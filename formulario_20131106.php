<!--Estilos-->
<!--<link rel="stylesheet" type="text/css" media="screen" href="js/css/jquery-ui-1.10.3.custom.min.css" />-->
<link rel="stylesheet" type="text/css" media="screen" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<!--<link rel="stylesheet" type="text/css" media="screen" href="js/css/ui.datepicker.css"/>-->
<link rel="stylesheet" type="text/css" media="screen" href="js/css/ui.jqgrid.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="js/css/jquery.notice.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/view.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="js/shadowbox/shadowbox.css"/>

<script type="application/javascript" src="js/jquery-1.10.0.min.js"></script>
<script type="application/javascript" src="js/jquery.blockUI.js"></script>
<script type="application/javascript" src="js/plugins/jquery.notice.js"></script>
<script type="application/javascript" src="js/i18n/grid.locale-es.js"></script>
<script type="application/javascript" src="js/plugins/jquery.jqGrid.min.js"></script>
<script type="application/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="application/javascript" src="js/view.js"></script>
<script type="application/javascript" src="js/jquery.editinplace.js"></script>
<script type="application/javascript" src="js/shadowbox/shadowbox.js"></script>

<!--<script type="application/javascript" src="js/annyang.min.js"></script>-->
<!--<script type='application/javascript' src='http://jqueryui.com/themeroller/themeswitchertool/'></script>-->


<script type="application/javascript">

Shadowbox.init();

jQuery.noConflict = true;

jQuery(function(){
// Themes switcher jquery UI
//jQuery('#switcher').themeswitcher();

/*
*  Manipulation voice commands, future implements, Don't touch!
*/
/*
if (annyang) {
  // Definir el comando a ejecutar.
  var commands = {
    'comando': function() {
			jQuery('#algo').show();....
			}
  };

  // Inicializar annyang con sus comandos
  annyang.init(commands);

  // Iniciar la escucha de los comandos.
  annyang.start();
}
*/

/*
 * ----------------------------- Select Objects --------------------------------
 */

jQuery("select[name='usuarioreloj']").change( function() {
    jQuery("#grilla1").jqGrid("setGridParam",( {
	   'url':'empl/reloj_lista_usuario_consulta.php?usuarioreloj='+jQuery('select option:selected').val()
    })).trigger("reloadGrid");
});

jQuery("#requerimiento").change( function() {
var opcion=jQuery('#requerimiento option:selected').val();
//alert(opcion);
switch(opcion) {
	case 'o':
		jQuery("#tipoCP").hide();
		jQuery("#tipoV").hide();
		jQuery.noticeAdd({
			text: 'Por favor elija una opci&oacute;n!',
			stay: false,
			type: 'error'
		});
		break;
	case 'v':
		jQuery("#tipoCP").hide('fast');
		jQuery("#tipoV").show('slow');
		jQuery("#acordion").hide();
		break;
	default:
		jQuery("#tipoV").hide('fast');
		jQuery("#tipoCP").show('slow');
		break;
}// End switch

});

jQuery("#vehiculo").change( function() {
var opc=jQuery('#vehiculo option:selected').val();

switch(opc){
	case 'auto':
		jQuery("#acordion").hide();
	break;
	default:
	  jQuery("#acordion").show('fast');
		  break;
	}
});

//-------------- Accordion------------------
jQuery("#acordion").accordion({
	autoHeight: false,
	collapsible: true,
	minHeight: 90
});
// ------------------------------------------

//* -------------- Dates Objects -------------
//Browsers
var IE6 = false /*@cc_on || @_jscript_version < 5.7 @*/
var IE7 = (document.all && !window.opera && window.XMLHttpRequest && navigator.userAgent.toString().toLowerCase().indexOf('trident/4.0') == -1) ? true : false;
var IE8 = (navigator.userAgent.toString().toLowerCase().indexOf('trident/4.0') != -1);
var IE9 = navigator.userAgent.toString().toLowerCase().indexOf("trident/5")>-1;
var IE10 = navigator.userAgent.toString().toLowerCase().indexOf("trident/6")>-1;
var SAFARI = (navigator.userAgent.toString().toLowerCase().indexOf("safari") != -1) && (navigator.userAgent.toString().toLowerCase().indexOf("chrome") == -1);
var FIREFOX = (navigator.userAgent.toString().toLowerCase().indexOf("firefox") != -1);
var CHROME = (navigator.userAgent.toString().toLowerCase().indexOf("chrome") != -1);
var MOBILE_SAFARI = ((navigator.userAgent.toString().toLowerCase().indexOf("iphone")!=-1) || (navigator.userAgent.toString().toLowerCase().indexOf("ipod")!=-1) || (navigator.userAgent.toString().toLowerCase().indexOf("ipad")!=-1)) ? true : false;
if(!CHROME){
	//alert(CHROME);
	var pickerOpts = {dateFormat:"yy-mm-dd"};
	jQuery("input[type=date]").datepicker(pickerOpts);
}
//-------------------------------------------

// -------------- Input Objects -------------
/*
 * Autocomplete Inputs
 */

jQuery("#apenom").autocomplete({
source: function( request, response )
{
    jQuery.ajax(
    {
        url: "empl/buscar.php",
        data: { term: request.term },
        type: "POST",
        dataType: "json",
        error: function ( data ){ alert(data);},
        success: function( data )
        {
            response( jQuery.map( data, function( item )
            {
                return{
                        label: item.codigo,
                        value: item.nombre,
                        mail: item.mail,
                        usuario: item.codigo

                        }

            } ) );
        }
    });// End $.ajax
},
select: function( event,ui )
{
     jQuery(this).val(ui.item.value);
     jQuery("#email").val(ui.item.mail);
     jQuery("#compras").append( "<input type='hidden' name='Codigo_usuario' id='Codigo_usuario' value='"+ui.item.usuario+"'/> " )
     jQuery('#interno').focus();

    }
});
///////////20131002///////////////
jQuery("#revendedor_pe").autocomplete({
source: function( request, response )
{
    jQuery.ajax(
    {
        url: "empl/buscar_pe.php",
        data: { term: request.term },
        type: "POST",
        dataType: "json",
        error: function ( data ){ alert(data);},
        success: function( data )
        {
            response( jQuery.map( data, function( item )
            {
                return{
                         label: item.nombre+" - ("+item.codigo+")",
                        value: item.nombre,

                        mail: item.mail,
                        usuario: item.codigo,
                        apellido: item.apellido,
                        nombres: item.nombres,
                        nro_doc: item.nro_doc,
                        provincia: item.provincia,
                        direccion: item.direccion,
                        celular: item.celular,
                        area_celu: item.area_celu,
                        cateusu: item.cateusu
                        }

            } ) );
        }
    });// End $.ajax
},
select: function( event,ui )
{
     jQuery(this).val(ui.item.value);
     jQuery("#email").val(ui.item.mail);

     jQuery("#apellido").val(ui.item.apellido);
     jQuery("#nombres").val(ui.item.nombres);
     jQuery("#nro_doc").val(ui.item.nro_doc);
     jQuery("#direccion").val(ui.item.direccion);
     jQuery("#provincia").val(ui.item.provincia);
     jQuery("#area_celu").val(ui.item.area_celu);
     jQuery("#celular").val(ui.item.celular);
     jQuery("#cod_us").val(ui.item.usuario);

     if (ui.item.cateusu =='R'){
         ui.item.cateusu = 'Revendedor'
     }
     if (ui.item.cateusu =='CF'){
         ui.item.cateusu = 'Cliente Final'
     }
          if (ui.item.cateusu =='A' || ui.item.cateusu =='E'){
         ui.item.cateusu = 'Staff'
     }

     jQuery("#cateusu").val(ui.item.cateusu);

//alert("codigo");
    }
});

////////////////////////////////////////
jQuery("#revendedor").autocomplete({
source: function( request, response )
{
	var n=(jQuery("input[name='nivel']").length)?jQuery("input[name='nivel']").val():0;
    jQuery.ajax(
    {
        url: "empl/buscar_usuario.php",
        data: { term: request.term,nivel: n },
        type: "POST",
        dataType: "json",
        error: function ( data ){
					jQuery.noticeAdd({
              text: data,
              stay: false
           });
				},
        success: function( data )
        {
            response( jQuery.map( data, function( item )
            {
                return{
                        label: item.usuario+" - ("+item.cod_us+")",
                        value: item.cod_us
                }

            } ) );
        }
    });// End $.ajax
},
select: function( event,ui )
{
     jQuery(this).val(ui.item.value);
    }
});

//-----------------------------------------------------------
/*
 * Buttons Object Actions & Setter
 */

//-------------------------------------------------------------

/*
 * ------------------ Uploads Input --------------------
 */

//-------------------------------------------------------------
/*
 * ------------------ Grids Objects ---------------------------
 */

jQuery("#grilla1").jqGrid({
	url: 'empl/reloj_lista_usuario_consulta.php?usuarioreloj='+jQuery('select option:selected').val(),
	datatype : 'json',
	colNames : ['Empleado(s)','Fecha/Hora Ing.','Fecha/Hora Egr.','Comentario'],
	colModel : [{
		name : 'cod_us',
		index:'cod_us',
		width : 290,
		align: 'left'
	},{
		name : 'ing',
		index:'ing',
		width : 130,
		align: 'left'
	},{
		name : 'sale',
		index:'sale',
		width : 130,
		align: 'left'
	},{
		name : 'comentario',
		index:'comentario',
		width : 300,
		align: 'left'
	}
	],
	height: 'auto',
	pager: '#pager1',
	rowNum: 25,
	rowList: [10,20,31,50],
	sortname: "cod_us",
	viewrecords: true,
	sortorder: "desc",
	caption: "LISTADO DE FICHADAS"
});
jQuery("#grilla1").jqGrid('navGrid','#pager1', {
	search:false,
	edit:false,
	add:false,
	del:false
});
//----------------------------------------------------------

/*
 * -------------- Submit Forms -----------------------------
 */

jQuery("#compras").submit( function() {
	/*
	 * Si no esta seleccionado el requerimiento, no se envian los datos
	 */
	if(jQuery('#requerimiento option:selected').val()=='o') {
		jQuery.noticeAdd({
			text: '<center>Por favor complete todos los datos solicitados!<center>',
			stay: false
		});
        return false;
	}
	else {
	   var inputs=jQuery("#compras").serialize();

	// Guardar los datos en la base
	jQuery.ajax({
	    url: 'empl/compras_pagos_guardar.php',
	    type: 'POST',
	    dataType: 'json',
	    data: inputs,
	    success:function(data){
           jQuery.noticeAdd({
              text: data,
              stay: false
           });
	    }// End success
	   });// End $.ajax

	}// end else submmit
	// Prevenir la acción por defecto del form
	return false;
});




/*
 * Eliminar el div con id=tipo al presionar cancelar
 */
jQuery("input:reset").click( function() {
	jQuery("#tipoV").hide();
	jQuery("#tipoCP").hide();
});

//------------------------------------------------------------
/*
 * ------------- Dialog Objects -------------------------------
 */

/*
$("#dialog").dialog({
        resizable: false,
        draggable: false,
        closeOnEscape: true,
        bgiframe: true,
        autoOpen: false,
        modal: true,
        position: 'center'
 });

jQuery('#alta_cf').click(function(){
     jQuery.ajax({
             url: 'empl/buscador_revr_pe_vendedores_abm.php',
             type: 'POST',
             dataType: 'json',
             data:{term:'codigousuario=-1',usuario: '<?echo $usuario;?>'},
             success: function(resp){ alert(resp);
                       jQuery("#dialog").html(resp).dialog('open');
             }
     }); // End $.ajax
    return false;
}); // End click method
*/

/* Tooltip
-------------------------*/
jQuery("#ayuda").tooltip({
			track:true,
			 position: {
					my: "center bottom-20",
					at: "center top",
					using: function( position, feedback ) {
									jQuery( this ).css( position );
						jQuery( "<div>" )
					.addClass( "arrow" )
					.addClass( feedback.vertical )
					.addClass( feedback.horizontal )
					.appendTo( this );
			}
	}
});

});// End ready

	function imprSelec(seleccion)
	{
		var ficha=document.getElementById(seleccion);
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		ventimp.close();
	}


	function nuevoAjax() {
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		 lo que se puede copiar tal como esta aqui */
		var xmlhttp=false;
		try {
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				// Creacion del objeto AJAX para IE
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			} catch(E) {
				xmlhttp=false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
			xmlhttp=new XMLHttpRequest();
		}

		return xmlhttp;
	}

	function visibilidad(nombre,imgnom) {

		// ocultamos o mostramos segun la variable valor
		if(document.getElementById(nombre).style.visibility!='visible') {
			document.getElementById(imgnom).src='images_gif/carpeta_a.gif';
			//document.getElementById(imgnom).width=20;
			document.getElementById(nombre).style.visibility='visible';
			document.getElementById(nombre).style.display='';
		} else {
			document.getElementById(imgnom).src='images_gif/carpeta_c.gif';
			//document.getElementById(imgnom).width=20;
			document.getElementById(nombre).style.visibility='hidden';
			document.getElementById(nombre).style.display='none';
		}
	}

	function comparacodigo(Campo,Nro) {
		if (Campo.value==Nro) {
			alert("ERROR: El " + Campo + " es igual");
			Campo.focus();
			return true;
		} else
			alert("ERROR: El " + Campo + " diferente a " + Campo);
		Campo.focus();
		return false;
	}

	function AbrirVentana(imagen) {
		ventana=open("","MiPropiaVentana","width=300, height=200, toolbar=no,directories=no,menubar=no,status=no");
		ventana.document.write("<HTML>");
		ventana.document.write('<HEAD><TITLE>Foto</TITLE></HEAD>');
		ventana.document.write('<BODY bgcolor=#b8b4d8 link="#cc6600" vlink="#66cc00" alink="#66cc00">');
		ventana.document.write('<p align=center><img border="0" src=' + imagen + '></p>');
		ventana.document.write('<p align="center"><font face="arial" size="3" color="#cc6600"><b><a href="javascript:close()"><b>cerrar</b></a></font></p>')
		ventana.document.write("</BODY>");
		ventana.document.write("</HTML>");
	}

	// Funciones varias con formularios
	function llenar_campo(campo,valor) {
		campo = valor
	}

	// Funciones de Validacion de datos

	function esNumerico(chr) {
		if (chr >= '0' && chr <= '9')
			return true;
		else
			return false;
	}

	function Stringmenor(CampoNom,Campo,largo) {
		if ((Campo==null) || (Campo.value==null) || (Campo.value.length < largo)) {
			alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
			Campo.focus();
			return true;
		} else
			return false;
	}

	function Stringmenorovacio(CampoNom,Campo,largo) {
		if ((Campo.value.length > 0) && (Campo.value.length < largo)) {
			alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
			Campo.focus();
			return true;
		} else
			return false;
	}

	function StringVacio(CampoNom,Campo) {
		if ((Campo==null) || (Campo.value==null) || (Campo.value.length==0)) {
			alert("ERROR: El " + CampoNom + " no puede estar vacio");
			Campo.focus();
			return true;
		} else
			return false;
	}

	function AnioValido(CampoNom,Campo,Desde,Hasta) {
		if (Campo.value.length!=4) {
			alert("ERROR: El a&ntilde;o no es correcto");
			Campo.focus();
			return false;
		}
		for (var i=0;i<Campo.value.length;i++) {
			chr=Campo.value.substring(i,i+1);
			if (!esNumerico(chr)) {
				alert("ERROR: El a&ntilde;o no es correcto");
				Campo.focus();
				return false;
			}
		}
		if (Campo.value < Desde || Campo.value > Hasta) {
			alert("ERROR: El a&ntilde;o no es correcto");
			Campo.focus();
			return false;
		}

		return true;
	}

	function PrecioValido(CampoNom,Campo,Desde,Hasta) {
		var puntosa=0;

		if (Campo.value.length==0) {
			alert("ERROR: Debe ingresar " + CampoNom);
			Campo.focus();
			return true;
		}

		for (var i=0;i<Campo.value.length;i++) {
			chr=Campo.value.substring(i,i+1);
			if ( (chr==".") )
				puntosa=puntosa+1;

			if (puntosa > 1) {
				alert("ERROR:" + CampoNom + " No es un monto valido " );
				Campo.focus();
				return true;
			}

			if (!((esNumerico(chr)) || (chr == "."))) {
				alert("ERROR: Debe ingresar un n&uacute;mero");
				Campo.focus();
				return true;
			}
		}
		if (Campo.value < Desde || Campo.value > Hasta) {
			alert("ERROR: El n&uacute;mero debe estar entre " + Desde + " y " + Hasta);
			Campo.focus();
			return true;
		}

		return false;

	}

	function NumeroValido(CampoNom,Campo,Desde,Hasta) {
		if (Campo.value.length==0) {
			alert("ERROR: Debe ingresar " + CampoNom);
			Campo.focus();
			return true;
		}
		for (var i=0;i<Campo.value.length;i++) {
			chr=Campo.value.substring(i,i+1);
			if (!esNumerico(chr)) {
				alert("ERROR: Debe ingresar un n&uacute;mero");
				Campo.focus();
				return true;
			}
		}
		if (Campo.value < Desde || Campo.value > Hasta) {
			alert("ERROR: El n&uacute;mero debe estar entre " + Desde + " y " + Hasta);
			Campo.focus();
			return true;
		}

		return false;
	}

	function MailValido(CampoNom,Campo) {
		var chr;
		var arrobas=0;
		var puntos=0;
		var blancos=0;
		if  ((Campo!=null) && (Campo.value.length!=0)) {
			for (var i=0;i<Campo.value.length;i++) {
				chr=Campo.value.substring(i,i+1);
				if ( (chr=="@") )
					arrobas=arrobas+1;
				if ( (chr==".") )
					puntos=puntos+1;
				if ( (chr==" ") )
					blancos=blancos+1;
			}

			if ( (blancos!=0) || (arrobas!=1) || (puntos<1) ) {
				alert("ERROR: La direcci\363n de correo no es correcta.Recuerda que no se adminten espacios en blanco y que debe contener el car\341cter arroba (@) y el car\341cter punto (.)");
				Campo.focus();
				return false;
			}
		}
		return true;
	}
	function validNumInt(txt) {
		var texto = txt.value;
		var textoS = "";
		var c;
		var notModified = true;

		for(var i=0; i< texto.length; i++) {

			c = texto.substring(i, i+1);

			if(c >= '0' && c <= '9') {
				textoS = textoS + c;
			}else {
				notModified = false;
			}
		}
		if ( notModified == false ) {
			txt.value = textoS;
		}
	}


	//calcular la edad de una persona
	//recibe la fecha como un string en formato español
	//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual
	function calcular_edad(fecha) {

		//calculo la fecha de hoy
		hoy=new Date()
		//alert(hoy)

		//calculo la fecha que recibo
		//La descompongo en un array
		var array_fecha = fecha.split("-");
		//si el array no tiene tres partes, la fecha es incorrecta
		if (array_fecha.length!=3)
			return false;

		//compruebo que los ano, mes, dia son correctos
		var ano
		ano = parseInt(array_fecha[0]);
		if (isNaN(ano))
			return false

		var mes
		mes = parseInt(array_fecha[1]);
		if (isNaN(mes))
			return false

		var dia
		dia = parseInt(array_fecha[2]);
		if (isNaN(dia))
			return false

			//resto los años de las dos fechas
		edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año

		//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
		if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
			return edad
		if (hoy.getMonth() + 1 - mes > 0)
			return edad+1

			//entonces es que eran iguales. miro los dias
			//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
		if (hoy.getUTCDate() - dia >= 0)
			return edad + 1

		return edad
	}

	function EdadMenor(CampoNom,Campo,Anios) {

		if (calcular_edad(Campo.value)<18) {
			alert("ERROR: " + CampoNom + " es menor de " + Anios + ". ");
			Campo.focus();
			return true;
		} else
			return false;
	}

	function CaracterValido(control) {
		var chr;
		var novalido=0;
		if ((control.value!=null) && (control.value.length!=0)) {
			for (var i=0;i<control.value.length;i++) {
				chr=control.value.substring(i,i+1);
				if ( (chr==';') || (chr==',') || (chr=='%') || (chr=='\"') || (chr=='\`') || (chr=='\ ') || (chr=='\'') || (chr=='\\') )
					novalido=novalido+1;
			}
			if ( (novalido!=0) ) {
				alert('ERROR: No se adminten los siguientes caracteres(  ;  ,  %  \`  Espacio  \\  \'  \"  ), Por favor reemplace por caracteres validos o quitelos ');
				control.focus();
				return false;
			}
		}
		return true;
	}
</script>

<?php
function advertencia($adv)
{ global $stylefield;
global $link;
global $c_database;
global $agregadv;
if (!$agregadv) $agregadv="";
$qrystr = "select * from advertencia where adv = $adv";
$qry = mysql_db_query($c_database,$qrystr ,$link);
$row = mysql_fetch_array($qry);
if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
<tr height='19'>
<td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
<td height='19' background='img/tabla_02.gif'><br>
</td>
<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
</tr>
<tr>
<td width='23' background='img/tabla_04.gif'><br>
</td>
<td>
<table border=0>
<tr>
<td width='4%' rowspan='2' align='center'>
<img border='0' width='20' height='94'src='images_gif/advertencia.gif'></td>
<td width='47%'><font size='4'>Informacion:</font></td>
</tr>
<tr>
<td width='47%' align='justify'><font face='Arial'>$row[texto] $agregadv</td>
</tr>
</table>
</td>
<td width='22' background='img/tabla_06.gif'><br>
</td>
</tr>
<tr height='22'>
<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
<td height='22' background='img/tabla_08.gif'><br>
</td>
<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
</tr>
</table>
";}
}

function advertencia_informes($filtro1,$filtro2)
{ global $stylefield;
global $link;
global $c_database;
global $agregadv;
if (!$agregadv) $agregadv="";
$qrystr = "select * from adv_filtro where filtro1 = '$filtro1' AND filtro2= '$filtro2'";
$qry = mysql_db_query($c_database,$qrystr ,$link);
$row = mysql_fetch_array($qry);
if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
<tr height='19'>
<td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
<td height='19' background='img/tabla_02.gif'><br>
</td>
<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
</tr>
<tr>
<td width='23' background='img/tabla_04.gif'><br>
</td>
<td>
<table border=0>
<tr>
<td width='4%' rowspan='2' align='center'>
<img border='0' width='20' height='94'src='images_gif/advertencia.gif'></td>
<td width='47%'><font size='4'>Informacion:</font></td>
</tr>
<tr>
<td width='47%' align='justify'><font face='Arial'>$row[texto] $agregadv</td>
</tr>
</table>
</td>
<td width='22' background='img/tabla_06.gif'><br>
</td>
</tr>
<tr height='22'>
<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
<td height='22' background='img/tabla_08.gif'><br>
</td>
<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
</tr>
</table>
";}
}

function advertenciaswf($adv)
{ global $stylefield;
global $link;
global $c_database;
global $agregadv;

if (!$agregadv) $agregadv="";
$qrystr = "select * from advertencia where adv = $adv";
$qry = mysql_db_query($c_database,$qrystr ,$link);
$row = mysql_fetch_array($qry);
if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
<tr height='19'>
<td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
<td height='19' background='img/tabla_02.gif'><br>
</td>
<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
</tr>
<tr>
<td width='23' background='img/tabla_04.gif'><br>
</td>
<td>
<table border=0>
<tr>
<td width='4%' rowspan='2' align='center'>
<img border='0' width='20' height='94' src='images_gif/advertencia.gif'></td>
<td width='47%'><font size='4'>Informacion:</font></td>
<td width='300' HEIGHT='180' rowspan='2'>
<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' WIDTH='400' HEIGHT='180'>
<param NAME='movie' VALUE='flash3.swf'>
<param NAME='quality' VALUE='high'>
<param NAME='bgcolor' VALUE='#FFFDF2'><embed src='flash3.swf' quality='high' bgcolor='#FFFDF2' WIDTH='400' HEIGHT='180' TYPE='application/x-shockwave-flash' PLUGINSPAGE='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
</object>
</td>
</tr>
<tr>
<td width='47%' align='justify'><font face='Arial'>$row[texto]
</td>
</tr>
<tr>
<td width='51%' align='center' colspan='3'>
$agregadv</td>
</tr>
</table>
</td>
<td width='22' background='img/tabla_06.gif'><br>
</td>
</tr>
<tr height='22'>
<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
<td height='22' background='img/tabla_08.gif'><br>
</td>
<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
</tr>
</table>
";}
}
function poner_combo_a_medida($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida)
{
global $stylefield;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_a_medida_1selected($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$selected=null)
{
global $stylefield;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry)){
$sel = '';
if($row[$campo_clave] == $selected)
$sel = 'selected';
echo "<option value=$row[$campo_clave] $sel>$row[$campo_mostrar]</option>";
}
echo "</select>";
}

function poner_combo_a_medida_1blanco($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida)
{
global $stylefield;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
echo "<option value=0></option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_a_medida_1texto($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$_campo_id_html='')
{
global $stylecombo;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
if($_campo_id_html<>'')
$_campo_id_html1="id='$_campo_id_html'";
echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
echo "<option value=$valor>$texto</option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}

// Para la campaña de cta cte
function poner_combo_a_medida_1texto_ctacte($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$_campo_id_html='')
{
global $stylecombo;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
if($_campo_id_html<>'')
$_campo_id_html1="id='$_campo_id_html'";
echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
echo "<option value=$valor>$texto</option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "<option value=0 >Campaña 0</option>";
echo "</select>";
}

// Para selecciones multiples
function poner_combo_a_medida_multiple($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$selected=null)
{
global $stylefield;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select size='6' name='" . $nom_combo . "' multiple>";
while ($row = mysql_fetch_array($qry)){
	$sel = '';
	if($row[$campo_clave] == $selected){$sel = 'selected';}
echo "<option value='$row[$campo_clave]' $sel>$row[$campo_mostrar]</option>";
}
echo "</select>";
}





function poner_combo_1texto($nom_combo,$tabla,$campo_mostrar,$campo_clave,$texto,$valor)
{
global $estilo;
global $link;
global $c_database;
$qrystr = "select * from $tabla order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
echo "<option value=$valor>$texto</option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_1blanco($nom_combo,$tabla,$campo_mostrar,$campo_clave)
{
global $estilo;
global $link;
global $c_database;
$qrystr = "select * from $tabla order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
echo "<option value=0></option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_filtro_1texto($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro,$texto,$valor)
{
global $estilo;
global $link;
global $c_database;
$qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $estilo. ">";
echo "<option value=$valor>$texto</option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_filtro_1blanco($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
global $estilo;
global $link;
global $c_database;

$qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
echo "<option value=0></option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_a_medida_b($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$esp,$bus)
{
global $stylefield;
global $link;
global $c_database;
$qry = mysql_db_query($c_database,$qrystramedida ,$link);
$espacio=str_pad($espacio,$esp, "_", STR_PAD_LEFT);
if (mysql_affected_rows()==0) { echo " <INPUT TYPE=hidden name=$nom_combo   value='0'>
<input  type='text' onfocus='this.blur()' name='pepe' size='50' value='No hay resultados en la busqueda:$bus'>";}
if (mysql_affected_rows()==1) {$row = mysql_fetch_array($qry);
echo " <INPUT TYPE=hidden name=$nom_combo   value='$row[$campo_clave]'>
<input  type='text' onfocus='this.blur()' name='pepe' size='52' value='$row[$campo_mostrar]'>";}
if (mysql_affected_rows()>1) {echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry))
{$muestra=$row[$campo_mostrar];
echo "<option value=$row[$campo_clave]>$muestra</option>";}
echo "<option value='0'>$espacio</option>";
echo "</select>";}
}
function poner_combo($nom_combo,$tabla,$campo_mostrar,$campo_clave,$_campo_id_html='')
{
global $stylecombo;
global $link;
global $c_database;
$qrystr = "select * from $tabla order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
if($_campo_id_html<>'')
$_campo_id_html1="id='$_campo_id_html'";

echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_str($nom_combo,$tabla,$campo_mostrar,$campo_clave)
{
global $stylefield;
global $link;
global $c_database;
$qrystr = "select * from $tabla order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry))
echo "<option value='$row[$campo_clave]'>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_filtro($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
global $stylefield;
global $link;
global $c_database;
$qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
//echo $qrystr;
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_filtro_str($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
global $stylefield;
global $link;
global $c_database;
$qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
$qry = mysql_db_query($c_database,$qrystr ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
while ($row = mysql_fetch_array($qry))
echo "<option value='$row[$campo_clave]'>$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_a_medida_noeditable($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$editable)
{
global $stylefield;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . " disabled='".$editable."'>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_a_medida_1texto_x($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$extra)
{
global $stylecombo;
global $link;
global $c_database;

$qry = mysql_db_query($c_database,$qrystramedida ,$link);
echo "<select $extra size='1' name=" . $nom_combo . " " . $stylecombo . ">";
echo "<option value=$valor selected=selected >$texto</option>";
while ($row = mysql_fetch_array($qry))
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}

function poner_combo_nro($nom_combo,$desde,$hasta)
{
global $stylefield;
echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
for ($i=$desde;$i<=$hasta;$i++)
echo "<option value=$i>$i</option>";
echo "</select>";
}

function poner_combo_array($nom_combo,$campo_mostrar,$campo_clave,$arr_param,$_campo_id_html='')
{
global $stylecombo;

if($_campo_id_html<>'')
$_campo_id_html1="id='$_campo_id_html'";
echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
foreach ($arr_param AS $row)
echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
echo "</select>";
}
// Funciones de fechas
function poner_combo_hora($nom,$j)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=$j>$j</option>";
$p='';
for ($i=0;$i<=23;$i++)
{
$p='0'.$i;
$p=substr($p,-2);
echo "<option value=$p>$p</option>";
}
echo "</select>";
}
function poner_combo_min_sec($nom,$j)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=$j>$j</option>";
$p='';
for ($i=0;$i<=59;$i++)
{
$p='0'.$i;
$p=substr($p,-2);
echo "<option value=$p>$p</option>";
}
echo "</select>";
}
function invertir_fecha($fecha)
{
$anio = substr($fecha,0,4);
$mes = substr($fecha,5,2);
$dia = substr($fecha,8,2);
return $dia . "/" . $mes . "/" . $anio;
}

function poner_combo_dia($nom)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
for ($i=1;$i<=31;$i++)
echo "<option value=$i>$i</option>";
echo "</select>";
}

function poner_combo_mes($nom)
{
global $stylefield;
global $ar_meses;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
for ($i=1;$i<=12;$i++)
echo "<option value=$i>$ar_meses[$i]</option>";
echo "</select>";
}

function poner_combo_anio($nom,$desde,$hasta)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
for ($i=$desde;$i<=$hasta;$i++)
echo "<option value=$i>$i</option>";
echo "</select>";
}
function poner_combo_diah($nom,$j)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=$j>$j</option>";
for ($i=1;$i<=31;$i++)
echo "<option value=$i>$i</option>";
echo "</select>";
}

function poner_combo_mesh($nom,$j)
{
global $stylefield;
global $ar_meses;
$j=intval($j);
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=$j>$ar_meses[$j]</option>";
for ($i=1;$i<=12;$i++)
echo "<option value=$i>$ar_meses[$i]</option>";
echo "</select>";
}

function poner_combo_anioh($nom,$desde,$hasta,$j)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=$j>$j</option>";
for ($i=$desde;$i<=$hasta;$i++)
echo "<option value=$i>$i</option>";
echo "</select>";
}
function poner_combo_sino($nom)
{
global $stylefield;
echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
echo "<option value=1>Si</option>";
echo "<option value=0>No</option>";
echo "</select>";

}
function llenar_campo($form,$campo,$valor)
{
echo "<script language='JavaScript'>";
echo " $form.$campo.value = '$valor'; ";
echo "</script>";
}
function llenar_area($form,$campo,$valor)
{
echo "<script language='JavaScript'>";
echo " $form.$campo.text = '$valor'; ";
echo "</script>";
}
function poner_datos_lid($codigo)
{
global $link;
global $c_database;
$qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_lider",$link);
while ($row = mysql_fetch_array($qry))
{
return $row[paq];
}
}
function poner_datos_dis($codigo)
{
global $link;
global $c_database;
$qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_distrib",$link);
while ($row = mysql_fetch_array($qry))
{
return $row[paq];
}
}
function poner_datos_cod($nom_concat,$codigo)
{
global $link;
global $c_database;

$qry = mysql_db_query($c_database,"select cod_us,concat(apellido,' ',nombres,' (',cod_us,')') as paq FROM usuario WHERE cod_us = '$codigo' ORDER by paq" ,$link);
while ($row = mysql_fetch_array($qry))
$nom_concat=$row[$paq];
}
function cambia_usuario($idus){
    $aa = strlen($idus);
   if($aa==1)
    $aa='00000';
   elseif($aa==2)
	$aa='0000';
   elseif($aa==3)
	$aa='000';
   elseif($aa==4)
	$aa='00';
   elseif($aa==5)
	$aa='0';
	else
	$aa='';
    return $aa.$idus;
}
function cambia_usuario_dist($idus){
    $aa = strlen($idus);
   if($aa==1)
    $aa='00';
   elseif($aa==2)
	$aa='0';
	else
	$aa='';
   return $aa.$idus;
}


function cambia_importe($importe)
{
	$imp=explode('.',$importe);
    $aa = strlen($imp[0]);
    $bb = strlen($imp[1]);
   if($aa==1)
    $aa='0000';
   elseif($aa==2)
	$aa='000';
   elseif($aa==3)
	$aa='00';
   elseif($aa==4)
	$aa='0';
   else
	$aa='';
   if($bb==0)
    $bb='00';
   elseif($bb==1)
    $bb='0';
   else
	$bb='';
	return $aa.$imp[0].$imp[1].$bb;
}
function cambia_fecha($fecha){
	$fcha1vto=explode('-',$fecha);
    $aa = substr($fcha1vto[0],2,3);
    return $aa.$fcha1vto[1].$fcha1vto[2];
}
function verificar_digito($codigo,$secuencia){

	$array_codigo = array();
    for ($i = 0; $i < strlen($codigo); $i++)
    {
        $array_codigo[] = $codigo[$i];
    }
   // print_r($array_codigo);
   // print_r($secuencia);
	$suma=0;
	$array_size = count($array_codigo);
	for($j = 0; $j < $array_size; $j++)
	{
		$suma=$suma+($array_codigo[$j]*$secuencia[$j]);
	}
	//echo ":::::::::::::suma:::::::::::::::".$suma."<br>";
	$divSumaE=$suma/2;
	$divSuma=(int)$divSumaE;

	//echo ":::::::::::::divSuma:::::::::::::::".$divSuma."<br>";
	$modulo=$divSuma%10;
	//echo ":::::::::::::modulo:::::::::::::::".$modulo."<br>";
	return $modulo;
}
function datos_distr($codigo)
{
	global $link;
	global $c_database;
	$qry = mysql_db_query($c_database,"select id_distribuidor FROM distribuidor WHERE cod_us='$codigo'",$link);
	$row = mysql_fetch_array($qry);
	return $row[id_distribuidor];
}
function poner_datos_reg($codigo)
{
global $link;
global $c_database;
$qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_regional",$link);
while ($row = mysql_fetch_array($qry))
return $row[paq];
}
function cliente_en_circuito($cliente,$nivel=6)
{
global $c_database;
global $link;
switch ($nivel)
{
case 6:
$campo1="comp_pedidos.id_vendedor";
break;
case 5:
$campo1="comp_pedidos.id_lider";
break;
case 4:
$campo1="comp_pedidos.id_distrib";
break;
case 3:
$campo1="usuario.id_regional";
break;
}
$qrystr = "SELECT id_pedido FROM campanias ORDER BY id_campania DESC LIMIT 2";
$qry = mysql_db_query($c_database,$qrystr,$link);
while ($row = mysql_fetch_array($qry))
{
$pedido=$row[id_pedido];
}
$qrystr1 = "SELECT comp_pedidos.id_distrib AS dis,comp_pedidos.id_lider AS lid,comp_pedidos.id_vendedor AS ven,id_pedido,estado_descrip
FROM comp_pedidos
INNER JOIN pedido ON id_pedidos=id_pedido
INNER JOIN estado_pedido ON estado=cod_estado
INNER JOIN usuario ON usuario.cod_us=comp_pedidos.id_vendedor
WHERE id_pedido = $pedido
AND estado IN(3,4,5,6,9,11,12,15,16,23)
AND pedido.anexo NOT IN('cambio_cred','acc','grab_rein','relojes','meda')
AND $campo1='$cliente'
GROUP BY id_pedido,comp_pedidos.id_vendedor";
$qry1=mysql_db_query($c_database,$qrystr1,$link);
//	echo $qrystr1;
verificar($qry,$qrystr);
$arr_pedidos=array();
while ($row1=mysql_fetch_array($qry1))
{
$arr_pedidos[]	= $row1;
}
return $arr_pedidos;
}
function ayuda($par_texto,$snapx=0,$snapy=0,$bgcolor='#800000',$fgcolor='#C4D3DB',$textcolor='#800000')
{
// return " onmouseover=\"pmaTooltip('". $par_texto ."'); return false;\" onmouseout=\"swapTooltip('default'); return false;\" ";
return " onmouseover=\"return overlib('$par_texto',SNAPX,$snapx,SNAPY,$snapy,BGCOLOR,'$bgcolor',FGCOLOR,'$fgcolor',TEXTCOLOR,'$textcolor');\" onmouseout=\"return nd();\" ";
}

function llenar_campo_id($id,$valor)
{
echo "<script language='JavaScript'>";
echo "document.getElementById('$id').value = '$valor'; ";
echo "</script>";
}

function calcular_tiempo_trasnc($hora1,$hora2){
	$separar[1]=explode(':',$hora1);
	$separar[2]=explode(':',$hora2);
	$total_minutos_trasncurridos[1] = ($separar[1][0]*60)+$separar[1][1];
	$total_minutos_trasncurridos[2] = ($separar[2][0]*60)+$separar[2][1];
	$total_minutos_trasncurridos = $total_minutos_trasncurridos[1]-$total_minutos_trasncurridos[2];
	return($total_minutos_trasncurridos);
}

function calcular_minutos($hora1){
	$separar[1]=explode(':',$hora1);
	$total_minutos_trasncurridos= ($separar[1][0]*60)+$separar[1][1];
		return($total_minutos_trasncurridos);
}
function sumaDia($fecha,$dia)
{
	list($year,$mon,$day) = explode('-',$fecha);
	$fecha=date('Y-m-d', mktime(0,0,0,$mon,$day+$dia,$year));
	return $fecha;
}
function buscarNombre($codigo)
{
	global $link;
	global $c_database;
	$qry = mysql_db_query($c_database,"select concat(apellido,' ',nombres) as paq FROM usuario WHERE cod_us='$codigo' ",$link);
	while ($row = mysql_fetch_array($qry))
	return $row[paq];
}
?>
