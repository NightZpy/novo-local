<?php
	class Ventan_Emerg extends Principal{
		function ventana_success($comentario){
	?>
			<div class="well hide" id="ventana_success" style="top:50%; left:50%; position:fixed; margin-left:-100px; height:100px; width:200px; margin-top:-100px; z-index: 1050;">
				<p><?php echo utf8_decode($comentario);?></p>
				<div style="bottom:5%; position:absolute; right:5%;">	
					<input type="button" id="success" class="btn btn-medium" value="Aceptar">
				</div>
			</div>
		<?php
		}
		
		function ventana_info($comentario){
		?>
			<div id="ventana_info" class="well hide" style="top:50%; left:50%; position:fixed; margin-left:-100px; height:100px; width:200px; margin-top:-100px; z-index: 1055;">
				<div id="comentario">
					<p><?php echo utf8_decode($comentario);?></p>
				</div>
				<div style="bottom:5%; position:absolute; right:5%;">	
					<input type="button" id="info" class="btn btn-medium" value="Aceptar">
				</div>
				<input type="hidden" id="ventan_info_foco">
			</div>
		<?php
		}
		
		function ventana_error($comentario){
		?>
			<div id="ventana_error" class="well hide" style="top:50%; left:50%; position:fixed; margin-left:-100px; height:100px; width:200px; margin-top:-100px; z-index: 1050; display:none;">
				<p><?php echo utf8_decode($comentario);?></p>
				<div style="bottom:5%; position:absolute; right:5%;">	
					<input type="button" id="error" class="btn btn-medium" value="Aceptar">
				</div>
			</div>
		<?php
		}
		
		function marco(){
		?>
			<div id="marco" class="modal-backdrop hide"></div>
		<?php
		}
	}
?>