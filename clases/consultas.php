<?php
	class Consultas extends Principal{
		
		//precios
		function precios_por_rubro(){
			$query = $this->consultar("SELECT * FROM precios_por_rubro");
			return $query;
		}
		
		function precios_por_rubro_especifico($id){
			$query = $this->consultar("SELECT * FROM precios_por_rubro WHERE id = $id");
			return $query;
		}
		
		//insertar precios
		function insertar_precios($rubro, $descrip, $precio){
			$query = $this->consultar("INSERT INTO precios_por_rubro (rubro_alfa, descrip_rubro, precio_gral) VALUES (UPPER('".$rubro."'),'".$descrip."',$precio)");
		}
		
		//actualizar precios
		function actualizar_precios($id,$rubro, $descrip, $precio){
			$query = $this->consultar("UPDATE precios_por_rubro SET rubro_alfa = UPPER('".$rubro."'), descrip_rubro = '".$descrip."', precio_gral = $precio WHERE id = $id");
		}
		
		//eliminar precios
		function eliminar_precios($id){
			$query = $this->consultar("DELETE FROM precios_por_rubro WHERE id = $id");
		}
		
		//validar rubro alfa
		function validar_rubro_alfa($rubro){
			$query = $this->consultar("SELECT * FROM precios_por_rubro WHERE rubro_alfa = UPPER('".$rubro."')");
			return $query;
		}
		
		//comprobar pedido campaña actual
		function validar_pedido_camp_actual(){
			$query = $this->consultar("SELECT campanias.id_pedido AS pedido_campania,
					MAX(pedido.id_pedidos) AS id_pedido
				FROM pedido
					INNER JOIN campanias ON pedido.id_pedidos = campanias.id_pedido AND campanias.id_campania = (SELECT MAX(campanias.id_campania) id_campania FROM campanias)
					INNER JOIN comp_pedidos ON pedido.id_pedidos = comp_pedidos.id_pedido
				WHERE pedido.anexo IN ('Sventas','cambio_cred')");
			return $query;
		}
	}
?>