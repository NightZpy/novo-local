<?php
require_once $paso . '../clases/grupos/classes_grupos.php';
//******************************************* CLASE pedido *************************************************
//*********************************************************************************************************
class Pedido
{
	var $idPedido;
	var $tipoPedido;
	var $cambio;              //[true|false]
	var $debug;
	var $qrystrBase;          //qrystr con los datos m�nimos de pedido.
	var $qrystrCompPedido;    //qrystr con los datos de los comps de pedido.
	var $qrystrCompPedidoDefi;//qrystr con los datos de los comps defi de pedido.
	var $qrystrPedidoDefi;    //qrystr con los datos m�nimos de pedido defi.
	var $compPedido;          //Array con los comp_pedidos
	var $compPedidoDefi;      //Array con los comp_pedidos_defi
	var $pedidoDefi;          //Array con los pedidos_defi
	var $resumenPedido;       //Array con los datos de resumen del pedido, como las un_com_s o el pvp_com_s.
	var $errores;
	var $data;                //Los datos del pedido, es una copia del row obtenido en la consulta base.
	var $dataExtraCargados;   //Array que contiene cu�les consultas para datos extra fueron realizadas. Esto
	//es porque no siempre es necesario saber por ej. cu�ntos Accesorios tiene un pedido,
	//pero puede haber 2 descriptores que necesiten recorrer los comp_pedidos y as� se consulta s�lo una vez la BD.
	//Puede contener los ss. elementos: 'comp_pedidos','pedidos_defi','comp_pedidos_defi'
	var $idGrupoAcc;          //Grupo de Accesorios.
	var $acciones;            //Array con las acciones a aplicar.

	// ************** Constructor ****************
	function Pedido($parIdPedido,$parDebug=false)
	{
		$this->debug=$parDebug;
		$this->idPedido = $parIdPedido;
		$this->compPedido=array();
		$this->compPedidoDefi=array();
		$this->pedidoDefi=array();
		$this->resumenPedido=array();
		$this->dataExtraCargados=array();
		$this->data=array();
		$this->acciones=array();
		$this->idGrupoAcc = 52;         //CAMBIAR A UN GRUPO DIN�MICO!!!!!!!!!!!!!!!!!!!!!!!!!
		$this->qrystrBase="SELECT p.*,c.campania,c.fecha_entrega FROM pedido AS p INNER JOIN cronograma AS c ON p.id_presentacion=c.id_presentacion WHERE id_pedidos=" . $this->idPedido;
		$this->qrystrCompPedido="SELECT * FROM comp_pedidos WHERE id_pedido=" . $this->idPedido;
		$this->qrystrCompPedidoDefi="SELECT * FROM comp_pedidos_defi WHERE id_pedido=" . $this->idPedido;
		$this->qrystrPedidoDefi="SELECT * FROM pedidos_defi WHERE id_pedidos=" . $this->idPedido;
		$this->obtenerPersistencia();
	}
	// ************ Fin Constructor **************
	// ************* Verifica si hubo errores sql **************
	function verificarSQL($qrystr,$metodo='')
	{
		$err = mysql_error();
		if($err<>'')
		{
			echo "Error en clase PedidoRev -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
		}
	}
	// ************************************************
	// ************ Obtiene datos B�sicos de la BD **************
	function obtenerPersistencia()
	{
		global $link;
		global $c_database;
		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>ObtenerPersistencia: Param:<BR>qrystr: " . $this->qrystrBase ."</font><BR>" ;
		  }
		$qry = mysql_db_query($c_database,$this->qrystrBase,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>LoadData: Error: " . $this->errores . "</font><BR>" ;
		  }

		$this->data = mysql_fetch_array($qry);
		if ($this->debug)
		  {
			echo "<BR><font color=#FF0000>LoadData: Data cargado: </font><BR>";
			var_dump($data);
		  }
	}
	// ************************************************
	// ######################################################################################################
	// ############################### Manejo de Datos Extra ################################################
	// ######################################################################################################
	// ************ Obtiene los Componentes del pedido ************
	function obtenerCompPedidos()
	{
		global $link;
		global $c_database;
		if(in_array('comp_pedidos',$this->dataExtraCargados)) return;

		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>obtenerCompPedidos: Param:<BR>qrystr: " . $this->qrystrCompPedido ."</font><BR>" ;
		  }
		$qry = mysql_db_query($c_database,$this->qrystrCompPedido,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>LoadData: Error: " . $this->errores . "</font><BR>" ;
		  }

		while($row=mysql_fetch_array($qry))
			$this->compPedido[] = $row;
		if ($this->debug)
		  {
			echo "<BR><font color=#FF0000>LoadData: Data cargado: </font><BR>";
			var_dump($this->compPedido);
		  }
		$this->dataExtraCargados[]='comp_pedidos';
	}
	// ************************************************
	// ************ Obtiene los Componentes Defi del pedido ************
	function obtenerCompPedidosDefi()
	{
		global $link;
		global $c_database;
		if(in_array('comp_pedidos_defi',$this->dataExtraCargados)) return;

		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>obtenerCompPedidosDefi: Param:<BR>qrystr: " . $this->qrystrCompPedidoDefi ."</font><BR>" ;
		  }
		$qry = mysql_db_query($c_database,$this->qrystrCompPedidoDefi,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		  {
		  	echo "<BR><font color=#FF0000>LoadData: Error: " . $this->errores . "</font><BR>" ;
		  }

		while($row=mysql_fetch_array($qry))
			$this->compPedidoDefi[] = $row;
		if ($this->debug)
		  {
			echo "<BR><font color=#FF0000>LoadData: Data cargado: </font><BR>";
			var_dump($this->compPedidoDefi);
		  }
		$this->dataExtraCargados[]='comp_pedidos_defi';
	}
	// ************************************************
	// ************ Obtiene los Pedidos Defi del pedido ************
	function obtenerPedidosDefi()
	{
		global $link;
		global $c_database;
		if(in_array('pedidos_defi',$this->dataExtraCargados)) return;

		if ($this->debug)
		{echo "<BR><font color=#FF0000>obtenerPedidosDefi: Param:<BR>qrystr: " . $this->qrystrPedidoDefi ."</font><BR>" ;}
		$qry = mysql_db_query($c_database,$this->qrystrPedidoDefi,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		{echo "<BR><font color=#FF0000>LoadData: Error: " . $this->errores . "</font><BR>" ;}

		while($row=mysql_fetch_array($qry))
		$this->PedidoDefi[] = $row;
		if ($this->debug)
		{
			echo "<BR><font color=#FF0000>LoadData: Data cargado: </font><BR>";
			var_dump($this->PedidoDefi);
		}
		$this->dataExtraCargados[]='pedidos_defi';
	}
	// ************************************************
	// ######################################################################################################
	// ############################## Manejo de Descriptores ################################################
	// ######################################################################################################
	// ************ Obtiene un valor para un descriptor **************
	function getDescriptor($parNombreDescriptor,$arr_param=array())
	{
		if(isset($this->data[$parNombreDescriptor]))  //Si es un descriptor que sale directo de la consulta base, devolverlo.
		return $this->data[$parNombreDescriptor];
		else                                          //No es un descriptor que sale directo de la consulta base => invocar al m�todo que lo devuelve.
		return $this->{'get_' . $parNombreDescriptor}($arr_param);
}
// ************************************************
// ************************************************
// ######################################################################################################
// ############################### Manejo de Condiciones ################################################
// ######################################################################################################
// ************ validar Condiciones ************
function validarCondiciones($parPuntoAplicacion,$parTipoPedido='',$parCampania='')
{
	global $link;
	global $c_database;
	$filtro='';

	if ($this->debug)
	{echo "<BR><font color=#FF0000>validarCondiciones: Param:<BR>parPuntoAplicacion: $parPuntoAplicacion parTipoPedido: $parTipoPedido parCampania: $parCampania</font><BR>" ;}

	//Buscar los criterios aplicables al pedido
	//     if($parTipoPedido<>'')
	//       $filtro .=" AND tipo_pedido='$parTipoPedido' ";
	//     if($parCampania<>'')
	//       $filtro .=" AND campania='$parTipoPedido' ";
	$qrystr="SELECT * FROM criterios WHERE activo=1 AND punto_aplicacion='$parPuntoAplicacion' $filtro ORDER BY orden";
	$qry = mysql_db_query($c_database,$qrystr,$link);
	$this->errores .= mysql_error();
	if ($this->debug)
	{echo "<BR><font color=#FF0000>validarCondiciones Error: " . $this->errores . "</font><BR>" ;}

	//Para cada criterio, verificar si se cumplen sus condiciones.
	while($row=mysql_fetch_array($qry))	{
		//Obtener las condiciones del criterio.
		$qrystr1="SELECT * FROM crit_condiciones WHERE id_criterio='$row[id_criterio]' ORDER BY  orden_condicion ";
		$qry1 = mysql_db_query($c_database,$qrystr1,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		{echo "<BR><font color=#FF0000>validarCondiciones Error: " . $this->errores . "</font><BR>" ;}

		$resulCondiciones=array();
		//Verificar cada condici�n.
		while($row1=mysql_fetch_array($qry1))
		{
			//Preparo el arr_param que se va a usar en el get_descriptor.
			$arr_param=array();
			if($row1['arr_param']<>'')
			eval('$arr_param =array(' . $row1['arr_param'] . ');');
			//Llama al m�todo de la clase que aplica el operador que viene desde  la bd.
			$resulCondiciones[$row1['id_cc']."-".$row1['descriptor']] = $this->{'operar_' . $row1['operador']}($this->getDescriptor($row1['descriptor'],$arr_param),$row1['valor']);
	} //Fin Verificar cada condici�n.
	if ($this->debug)
	echo"<br>resulCondiciones:".var_export($resulCondiciones,true)."<br>";

	$falla=false;
	//Forma b�sica de resoluci�n de condiciones: todas deben cumplirse.
	foreach ($resulCondiciones AS $r)
	if($r==false)
	$falla=true;
	//Si se cumplieron todas las condiciones, buscar las acciones del criterio y ponerlas en la 'bolsa' de acciones a realizar.
	if($falla==false)
	{
		//Obtener las Acciones del criterio.
		$qrystr2="SELECT * FROM crit_acciones WHERE id_criterio='$row[id_criterio]' ORDER BY  orden_accion";
		$qry2 = mysql_db_query($c_database,$qrystr2,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		{echo "<BR><font color=#FF0000>validarCondiciones Error: " . $this->errores . "</font><BR>" ;}

		//Almacenar cada Acci�n encontrada.
		while($row2=mysql_fetch_array($qry2))
		{
			//Guardo el id_criterio porque la acci�n puede querer usar la tabla de comunicaci�n de acciones.
			//Despu�s puede haber una acci�n que inhabilite a otra poniendo 'ejecutar' a false.
			$this->acciones[]=array('id_criterio'=>$row2['id_criterio'],'nombre'=>$row2['accion'],'arr_param'=>$row2['arr_param'],'ejecutar'=>true);
		}
	}
}//Fin Para cada criterio.
}
// ************************************************
// ************************************************
// ######################################################################################################
// ################################## Manejo de Acciones ################################################
// ######################################################################################################
// ************ Aplicar Acciones ************
function aplicarAcciones()
{
	global $link;
	global $c_database;
	$filtro='';

	if ($this->debug)
	{echo "<BR><font color=#FF0000>aplicarAcciones: Param:<BR>" ;}

	//Para cada Acci�n, aplicarla.
	foreach ($this->acciones AS $accion)
	{
		//Preparo el arr_param que se va a usar en el get_descriptor.
		$arr_param=array();
		if($accion['arr_param']<>'')
		eval('$arr_param =array(' . $accion['arr_param'] . ');');

		//Llama al m�todo de la clase que ejecuta la acci�n vino desde la bd.
		$this->{'do_' . $accion['nombre']}($accion['criterio'],$arr_param);
}
}
// ************************************************
// ######################################################################################################
// ##################################### Descriptores  ##################################################
// ######################################################################################################
// **************************************************************************************************************
//Todos los descriptores deben comenzar con 'get_' y a continuaci�n el nombre normal como si reci�n empezara.
//$arr_param contiene el array que se grab� en la tabla crit_condiciones para aplicar a la condici�n actual del criterio actual.
// **************************************************************************************************************
// ************ Tipo Faltante **************
function get_esFaltante($arr_param=array())
{
	return (substr($this->data['anexo'], 0, 8)=='FALTANTE');
}
// ************************************************
// ************ Tipo de Accesorios **************
function get_esAccesorios($arr_param=array())
{
/*
	global $sesion;
	global $link;
	global $c_database;
	//Obtener los comp_pedidos
	$this->obtenerCompPedidos();
	//Obtengo los productos tipo Accesorios.
	$g= new Grupo($this->idGrupoAcc);
	$g->InsertarElementosTemp($sesion);
	$acc=$g->GetElementosTemp($sesion,2,'');   //$acc guarda un array con los productos tipo accesorios.
	$g->BorrarElementosTemp($sesion);

	//Para cada compPedido, verifica si es un accesorio.
	$cant_acc=0;
	foreach($this->compPedido AS $prod)
	 {
    if($prod['id_vd_orig']<>'')
      $id_vd_usar=$prod['id_vd_orig'];
    else
      $id_vd_usar=$prod['id_vd'];
    if(in_array($id_vd_usar, $acc))
	    $cant_acc++;
	 }
	//Retorna verdadero si la cantidad de l�neas de componentes de pedido es igual a la cantidad de l�neas de accesorios encontrados.
  return ($cant_acc==sizeof($this->compPedido));
*/
  return ($this->data['anexo']=='acc');
}
// ************************************************
// ************ Tipo Internacional **************
function get_esInternacional($arr_param=array())
{
	return ($this->data['id_lista_precios']<>1);
}
// ************************************************
// ************ Tipo Relojes **************
function get_esRelojes($arr_param=array())
{
	return ($this->data['anexo']=='relojes');
}
// ************************************************
// 
// ************ Tipo Salon de ventas*********
function get_esSventas($arr_param=array())
{
	return ($this->data['anexo']=='Sventas');
}
// ************************************************
//*************Tipo Promo Meda**************************
function get_esMeda($arr_param=array())
{
	return ($this->data['anexo']=='meda');
}
//*******************************************************
//*************Tipo ddlm **************************
function get_esDdlm($arr_param=array())
{
	return ($this->data['anexo']=='ddlm');
}
//*******************************************************
//*************Tipo Promo Acc **************************
function get_esPromoAcc($arr_param=array())
{
	return ($this->data['anexo']=='promo_acc');
}
//*******************************************************
//*************Tipo esGrabRein **************************
function get_esGrabRein($arr_param=array())
{
	return ($this->data['anexo']=='grab_rein');
}
//*******************************************************
// ************ Tipo cambio_cred **************
function get_esCambioCred($arr_param=array())
{
	return ($this->data['anexo']=='cambio_cred');
}
// ************************************************

// ************ id_regional **************
function get_gerenteRegional($arr_param=array())
{
	global $link;
	global $c_database;

	//Obtener los Puntos en Promo de la promo $id_promo.
	$qrystr = "SELECT u.id_regional FROM pedido AS p INNER JOIN usuario AS u ON p.id_lider=u.cod_us WHERE p.id_pedidos='" . $this->getDescriptor('id_pedidos') . "'";
	if ($this->debug)
	{echo "<BR><font color=#FF0000>get_gerenteRegional: Param:<BR>qrystr: $qrystr. arr_param: " . var_export($arr_param, true) ."</font><BR>" ;}

	$qry = mysql_db_query($c_database,$qrystr,$link);
	//verificar($qry,$qrystr);
	$this->errores .= mysql_error();
	if ($this->debug)
	{echo "<BR><font color=#FF0000>get_gerenteRegional: Error: " . $this->errores . "</font><BR>" ;}

	//Almacenar los elementos del conjunto en un array autoasociativo.
	if(mysql_num_rows($qry)==0)
	return '';
	else
	{
		$row=mysql_fetch_array($qry);
		return $row[0];
	}
}
// ************************************************
// ************ Puntos Pedido**************
function get_puntosPedido($arr_param=array())
{
	global $link;
	global $c_database;

	// ******** Par�metros obligatorios ******
	$id_promo = $arr_param['id_promo'];
	// ****** Fin Par�metros obligatorios *****

	//Obtener los Puntos del pedido $id_pedido de la Promo $id_promo.
	$qrystr = "SELECT SUM(puntos) FROM crit_promos_puntos_log WHERE id_promo='$id_promo' AND id_pedido='" . $this->idPedido . "'";
	if($this->debug)
	{echo "<BR><font color=#FF0000>get_puntosPedido: Param:<BR>qrystr: $qrystr. arr_param: " . var_export($arr_param, true) ."</font><BR>" ;}
	$qry = mysql_db_query($c_database,$qrystr,$link);
	//verificar($qry,$qrystr);
	$this->errores .= mysql_error();
	if($this->debug)
	{echo "<BR><font color=#FF0000>get_puntosPedido: Error: " . $this->errores . "</font><BR>" ;}
	if(mysql_num_rows($qry)==0)
	return 0;
	else
	{
		$row=mysql_fetch_array($qry);
		return $row[0];
	}
}
// ************************************************
// ************ Fecha Presentacion**************
function get_fechaPresentacion($arr_param=array())
{
  return ($this->data['fecha_entrega']);
}
// ************************************************
// ######################################################################################################
// ##################################### Operadores #####################################################
// ######################################################################################################
// **************************************************************************************************************
//Todos los operadores deben comenzar con 'operar_' y a continuaci�n el nombre normal como si reci�n empezara.
//Todos reciben como par�metros $parValorClase y $parValorExt en ese orden.
// **************************************************************************************************************
// ************ Mayor **************
function operar_mayor($parValorClase,$parValorExt)
{
	return ($parValorClase>$parValorExt);
}
// ************************************************
// ************ Menor **************
function operar_menor($parValorClase,$parValorExt)
{
	return ($parValorClase<$parValorExt);
}
// ************************************************
// ************ mayor O Igual **************
function operar_mayorOIgual($parValorClase,$parValorExt)
{
  return ($parValorClase>=$parValorExt);
}
// ************************************************
// ************ menor O Igual **************
function operar_menorOIgual($parValorClase,$parValorExt)
{
  return ($parValorClase<=$parValorExt);
}
// ************************************************
function operar_txt_mayorOIgual($parValorClase,$parValorExt)
{
  global $link;
	global $c_database;
  $qrystr = "SELECT '$parValorClase'>='$parValorExt'";
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $row = mysql_fetch_array($qry);
  return $row[0];
}
// ************************************************
// ************ menor O Igual **************
function operar_txt_menorOIgual($parValorClase,$parValorExt)
{
  global $link;
	global $c_database;
  $qrystr = "SELECT '$parValorClase'<='$parValorExt'";
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $row = mysql_fetch_array($qry);
  return $row[0];
}
// ************ Igual **************
function operar_igual($parValorClase,$parValorExt)
{
	return ($parValorClase==$parValorExt);
}
// ************************************************
// ************ Distinto **************
function operar_distinto($parValorClase,$parValorExt)
{
	return ($parValorClase<>$parValorExt);
}
// ************************************************
// ************ In **************
function operar_in($parValorClase,$parValorExt)
{

	$ValorExt=explode(',',$parValorExt);
	return (in_array($parValorClase, $ValorExt));
}
// ************************************************
// ************ Not In **************
function operar_notIn($parValorClase,$parValorExt)
{
	$ValorExt=explode(',',$parValorExt);
	return (!in_array($parValorClase, $ValorExt));
}
// ************************************************
// ************ Like **************
function operar_like($parValorClase,$parValorExt)
{
	global $link;
	global $c_database;

	if ($this->debug)
	{echo "<BR><font color=#FF0000>operar_like: Param:<BR>qrystr: $parValorClase,$parValorExt</font><BR>" ;}
	$qrystr="SELECT '$parValorClase' LIKE('$parValorExt')";
	$qry = mysql_db_query($c_database,$qrystr,$link);
	$this->errores .= mysql_error();
	if ($this->debug)
	{echo "<BR><font color=#FF0000>operar_like: Error: " . $this->errores . "</font><BR>" ;}

	$row=mysql_fetch_array($qry);
	return($row(0));
}
// ************************************************
// ************ notLike **************
function operar_notLike($parValorClase,$parValorExt)
{
	global $link;
	global $c_database;

	if ($this->debug)
	{echo "<BR><font color=#FF0000>operar_notLike: Param:<BR>qrystr: $parValorClase,$parValorExt</font><BR>" ;}
	$qrystr="SELECT '$parValorClase' NOT LIKE('$parValorExt')";
	$qry = mysql_db_query($c_database,$qrystr,$link);
	$this->errores .= mysql_error();
	if ($this->debug)
	{echo "<BR><font color=#FF0000>operar_notLike: Error: " . $this->errores . "</font><BR>" ;}

	$row=mysql_fetch_array($qry);
	return($row(0));
}
// ************************************************

// ######################################################################################################
// ##################################### Acciones #######################################################
// ######################################################################################################
// **************************************************************************************************************
//Todas las Acciones deben comenzar con 'do_' y a continuaci�n el nombre normal como si reci�n empezara.
//Todas reciben como par�metros $parCriterio,$arr_param en ese orden. El $parCriterio sirve para buscar los par�metros
//que se pudieron enviar desde la validaci�n de condiciones. En la tabla crit_comunic se guardan par�metros con los campos
//clave_ses,criterio, accion, arr_param.
//$arr_param contiene el array que se grab� en la tabla crit_acciones para aplicar a la acci�n actual del criterio actual.
// **************************************************************************************************************
// ************ Escribir Accion **************
function do_escribirAccion($parCriterio,$arr_param)
{
	echo"escribirAccion $parCriterio";
	var_dump($arr_param);
}
// ************************************************
// ************ Agrega texto a un Comentario  **************
function do_agregarComentario($parCriterio,$arr_param)
{
	global $comentario_pedido;                  //A esta variable global se le modifica el texto.
	// ******** Par�metros obligatorios ******
	$texto = $arr_param['texto']; //El Texto en cuesti�n.
        // ******** Fin Par�metros obligatorios **
	if($comentario_pedido=='')
	$comentario_pedido=$texto;
	else
	$comentario_pedido .="<br>".$texto;
}
// ************************************************
//require_once 'clases_vd/acciones.inc';
}

//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************
?>
