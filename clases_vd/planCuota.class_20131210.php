<?php
	class Cuota	{
	var $totalCuotas;
	var $totalCuotasEspecial;
	var $idPedido;
	var $c_database;
	var $link;
	var $campania;

		public function Cuota($c_database,$link,$pedido,$campania)
		{
			$this->totalCuotas = 3;
			$this->totalCuotasEspecial=2;
			$this->idPedido = $pedido;
			$this->c_database = $c_database;
			$this->link = $link;
			$this->campania=$campania;
		}

		public function generarCuota()
		{
			/* Seleccionar el pedido de catalogo agrupado por lider,
			 * solo para saber el lider del pedido
			 ------------------------------------------------------*/
		 	$sql="SELECT p.*
				  FROM pedido as p
				  WHERE p.id_pedidos=".$this->idPedido."
				  AND p.anexo=''
				  AND p.id_lider<>''
				  GROUP BY p.id_lider";
			//echo $sql;
			//die();


			$qryV=mysql_db_query($this->c_database,$sql,$this->link);
			$resV=mysql_fetch_array($qryV);

			$lider=$resV['id_lider'];
			/*-------------------------------------------------------*/

			/* Seleccionar todos los vendedores de ese lider
			 --------------------------------------------------------*/
			$sql="SELECT cod_us as id_vendedor,id_lider,id_regional,id_distrib
					FROM usuario
					WHERE id_lider='".$lider."'";
			//echo $sql;
			//die();

			$qryVend=mysql_db_query($this->c_database,$sql,$this->link);
			/*----------------------------------------------------------*/

			while ($resVend=mysql_fetch_array($qryVend)){
				/* Seleccionar todas las cuotas de ese vendedor para la campania vigente y con estado abierta*/
				$sql="SELECT * FROM cuotas WHERE vendedor='".$resVend['id_vendedor']."' AND campania=".$this->campania." and estado = 'Abierto'";
				//echo $sql;
				$qry_camp=mysql_db_query($this->c_database,$sql,$this->link);
				$num_camp=mysql_num_rows($qry_camp);
				/*---------------------------------------------------------------------------------------------*/
				// Si no hay cuotas para ese vendedor, debería ser la primer cuota?
				if($num_camp==0){
						$sql="SELECT * FROM comp_pedidos WHERE id_vendedor='".$resVend['id_vendedor']."' AND id_pedido=".$this->idPedido;

						//echo $sql;
						//die();
						$qry=mysql_db_query($this->c_database,$sql,$this->link);
						$num=mysql_num_rows($qry);
						/* Si el vendedor tiene componentes para el id_pedido, buscar los productos que coincidan
						 * con los de la tabla productos_cuotas
						 -----------------------------------------------------------------------------------------*/
						if($num>0){
							$sql="SELECT cp.id_vd,cp.id_comp_ped,cp.id_vendedor,cp.cantidad FROM comp_pedidos as cp
							INNER JOIN productos_cuotas as pc ON pc.id_cuota=cp.id_vd WHERE cp.id_vendedor='".$resVend['id_vendedor']."'
							AND cp.id_pedido=".$this->idPedido." AND (cp.comentario NOT LIKE '%/3' OR cp.comentario NOT LIKE '%/2')";


							//echo $sql;
							//die();
							$qry1=mysql_db_query($this->c_database,$sql,$this->link);
							$num1=mysql_num_rows($qry1);
							/* Si hay componentes que coinciden con los de la tabla de productos_cuotas,crear plan de cuotas nuevo*/
							if($num1>0){
									while($res1=mysql_fetch_array($qry1)){
										for($k=1;$k<=$res1['cantidad'];$k++){
											$this->nuevoPlan($res1['id_vendedor'],$res1['id_vd'],$res1['id_comp_ped']);
										}
									}

									$sql="SELECT COUNT(cant_cuotas) as cuotas,id_vd,id_comp_ped,plan FROM cuotas WHERE vendedor='".$resVend['id_vendedor']."' AND estado='Abierto' AND campania NOT IN (".$this->campania.") GROUP BY plan";

									$qry_nnn=mysql_db_query($this->c_database,$sql,$this->link);

									$num_nnn=mysql_num_rows($qry_nnn);

									//echo $sql;
									//	die();
									if ($num_nnn>0){
										while($res_nnn=mysql_fetch_array($qry_nnn)){
											//echo $res_n['id_vendedor'];
											//die();
											$sigCuota=$res_nnn['cuotas']+1;
											$this->nuevaCuota($resVend['id_vendedor'],$res_nnn['id_vd'],$res_nnn['id_comp_ped'],$sigCuota,$res_nnn['plan'],$resVend['id_lider'],$resVend['id_distrib'],0);

										}
									}
							 }
							 else
							 {
										$sql="SELECT COUNT(cant_cuotas) as cuotas,id_vd,id_comp_ped,plan FROM cuotas WHERE vendedor='".$resVend['id_vendedor']."' AND estado='Abierto' AND campania NOT IN (".$this->campania.") GROUP BY plan";

										$qry_nn=mysql_db_query($this->c_database,$sql,$this->link);
										$num_nn=mysql_num_rows($qry_nn);

										//echo $sql;
											//die();
										if ($num_nn>0){
											while($res_nn=mysql_fetch_array($qry_nn)){
												//echo $res_n['id_vendedor'];
												//die();
												$sigCuota=$res_nn['cuotas']+1;
												$this->nuevaCuota($resVend['id_vendedor'],$res_nn['id_vd'],$res_nn['id_comp_ped'],$sigCuota,$res_nn['plan'],$resVend['id_lider'],$resVend['id_distrib'],0);
											}
										}
								}
						}
						else
						{
							/* No tiene componentes, entonces contar las cuotas del vendedor, si tiene */
								$sql="SELECT COUNT(cant_cuotas) as cuotas,id_vd,id_comp_ped,plan FROM cuotas WHERE vendedor='".$resVend['id_vendedor']."' AND estado='Abierto' AND campania NOT IN (".$this->campania.") GROUP BY plan";
								//echo $sql;
								//die();
								$qry_n=mysql_db_query($this->c_database,$sql,$this->link);
								$num_n=mysql_num_rows($qry_n);

								if ($num_n>0){
									while($res_n=mysql_fetch_array($qry_n)){

										$sigCuota=$res_n['cuotas']+1;
										//echo $sigCuota ."<br>";
										//echo $resVend['id_vendedor'];
										//die();
										$this->nuevaCuota($resVend['id_vendedor'],$res_n['id_vd'],$res_n['id_comp_ped'],$sigCuota,$res_n['plan'],$resVend['id_lider'],$resVend['id_distrib'],1);
									}
								}
						}// End if line: 70
				}// End if line: 51
			}// End while
		}// End generarCuota()

		public function nuevaCuota($vendedor,$id_vd,$idCompPed,$sigCuota,$plan,$lider,$distrib,$tipo)
		{
			/* Insertar la cuota nueva */
			$sql="INSERT INTO cuotas
				  (id_comp_ped,cant_cuotas,plan,id_vd,vendedor,estado,campania)
				  VALUES
				  (".$idCompPed.",".$sigCuota.",".$plan.",'".$id_vd."','".$vendedor."','Abierto',".$this->campania.")";

			//echo $sql . "<br>";
			//die();

			$qryinsA=mysql_db_query($this->c_database,$sql,$this->link);
			/* --------------------------------------------------------------------------------------------------------*/

			// Seleccionar el/los producto/s que esten en la lista de precios vigente
			$qrystr2 = "SELECT p.id_producto,p.id_vd,p.nombreproducto,l.precio,l.id_atrib FROM productos as p
			INNER JOIN listas_precios_productos as l ON l.id_vd=p.id_vd
			INNER JOIN listas_precios as ll ON ll.id_lista=l.id_lista
			WHERE p.id_vd = '".$id_vd."' AND ll.predet=1";
			//echo $qrystr2;
			$qry2 = mysql_db_query($this->c_database,$qrystr2,$this->link);
			$product = mysql_fetch_object($qry2);
			/*-----------------------------------------------------------------------------------------------------------*/

			// Si el id_vd es del producto Exibidor, setear el comentario
			if($id_vd=="CVA0402"){$cuota_hasta='/2';}
			else{ $cuota_hasta='/3';}
			//------------------------------------------------------------

			if ($tipo==1)// La cuota esta dentro del pedido personal del lider
			{
				$sqlA = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$product->id_producto."','".$product->id_vd."','".$product->nombreproducto."','COMPRADO EN CUOTAS X (".$vendedor.") ".$sigCuota.$cuota_hasta."',".$product->precio.",1,'".$distrib."','".$lider."','".$lider."','CURRENT()',".$product->id_atrib.",'A')";
    	 //echo $sqlA;
    	    $qryinscp1 = mysql_db_query($this->c_database,$sqlA,$this->link);
			}
			else
			{
			$sqlB = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$product->id_producto."','".$product->id_vd."','".$product->nombreproducto."','COMPRADO EN CUOTAS ".$sigCuota.$cuota_hasta."',".$product->precio.",1,'".$distrib."','".$lider."','".$vendedor."','CURRENT()',".$product->id_atrib.",'A')";
			//echo $sqlB;
			$qryinscp2 = mysql_db_query($this->c_database,$sqlB,$this->link);
			}
			//die();

			//------------------------------------ Cierre del Plan ---------------------------------
			if ($sigCuota>=$this->totalCuotas && $id_vd<>"CVA0402")
			{
				$precioNeg=$product->precio*3;
				$this->cerrarPlan($plan,$id_vd,$idCompPed,$vendedor,$lider,$distrib,$tipo,$precioNeg);
			}
			elseif($sigCuota>=totalCuotasEspecial && $id_vd=="CVA0402"){
				$precioNeg=$product->precio*2;
				$this->cerrarPlan($plan,$id_vd,$idCompPed,$vendedor,$lider,$distrib,$tipo,$precioNeg);
		  }
			//----------------------------------------------------------------------------------------

		}

		public function getProductOriginal($id_vd)
    {
    	//Obtenemos el producto original a agregar en el pedido una vez cumplido el plan de cuotas
    	$qrystr1 = "SELECT id_producto FROM productos_cuotas WHERE id_cuota = '".$id_vd."'";
    	//echo $qrystr1;
		$qry1 = mysql_db_query($this->c_database,$qrystr1,$this->link);
    	$result1 = mysql_fetch_array($qry1);
    	$rr = $result1['id_producto'];
    	$id_vd_orig = $rr;

    	$qrystr2 = "SELECT p.id_producto,p.id_vd,p.nombreproducto,l.precio,l.id_atrib FROM productos as p
		INNER JOIN listas_precios_productos as l ON l.id_vd=p.id_vd
		INNER JOIN listas_precios as ll ON ll.id_lista=l.id_lista
		WHERE p.id_vd = '".$id_vd_orig."' AND ll.predet=1";
    	//echo $qrystr2;
		$qry2 = mysql_db_query($this->c_database,$qrystr2,$this->link);
    $product = mysql_fetch_object($qry2);

    	return $product;
    }

		public function cerrarPlan($plan,$id_vd,$idCompPed,$vendedor,$lider,$distrib,$tipo,$precioNeg)
		{
			$sql="UPDATE cuotas SET estado='Cerrado' WHERE plan=".$plan;
			//echo $sql;
			$qryupB=mysql_db_query($this->c_database,$sql,$this->link);

			$product =$this->getProductOriginal($id_vd);

			$qrystrNeg = "SELECT p.id_producto,p.id_vd,p.nombreproducto,l.precio,l.id_atrib FROM productos as p
			INNER JOIN listas_precios_productos as l ON l.id_vd=p.id_vd
			INNER JOIN listas_precios as ll ON ll.id_lista=l.id_lista
			WHERE p.id_vd = 'CRD0000' AND ll.predet=1";
			//echo $qrystr2;
			$qryNeg = mysql_db_query($this->c_database,$qrystrNeg,$this->link);

			$productNeg = mysql_fetch_object($qryNeg);

			if ($tipo==1){
			$sql = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$product->id_producto."','".$product->id_vd."','".$product->nombreproducto."','COMPRADO EN CUOTAS X (".$vendedor.")',".$product->precio.",1,'".$distrib."','".$lider."','".$lider."','CURRENT()',".$product->id_atrib.",'A')";
    	    $qryinsR = mysql_db_query($this->c_database,$sql,$this->link);

			$sql = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$productNeg->id_producto."','".$productNeg->id_vd."','".$productNeg->nombreproducto."','COMPRADO EN CUOTAS X (".$vendedor.")',".-$precioNeg.",1,'".$distrib."','".$lider."','".$lider."','CURRENT()',".$productNeg->id_atrib.",'A')";
    	    $qryinsR1 = mysql_db_query($this->c_database,$sql,$this->link);

			}
			else
			{
			$sql = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$product->id_producto."','".$product->id_vd."','".$product->nombreproducto."','COMPRADO EN CUOTAS',".$product->precio.",1,'".$distrib."','".$lider."','".$vendedor."','CURRENT()',".$product->id_atrib.",'A')";
    	    $qryinsT = mysql_db_query($this->c_database,$sql,$this->link);

			$sql = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha,id_atrib,orden)
    	                VALUES ('".$this->idPedido."','".$productNeg->id_producto."','".$productNeg->id_vd."','".$productNeg->nombreproducto."','COMPRADO EN CUOTAS X (".$vendedor.")',".-$precioNeg.",1,'".$distrib."','".$lider."','".$vendedor."','CURRENT()',".$productNeg->id_atrib.",'A')";
    	    $qryinsT1 = mysql_db_query($this->c_database,$sql,$this->link);

			}
		}

		public function nuevoPlan($idVendedor,$idVd,$idCompPed)
		{
			$sql="SELECT cro.campania,u.id_regional FROM comp_pedidos as cp INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor WHERE cp.id_comp_ped=".$idCompPed."";

			$qry_campania=mysql_db_query($this->c_database,$sql,$this->link);

			$res_campania=mysql_fetch_object($qry_campania);
			//echo $sql;
			// Aqui se tiene en cuenta las campañas para permitir la creacion de nuevas cuotas
			if ($res_campania->campania>1105 || ($res_campania->id_regional=='MEXVAN1' || $res_campania->id_regional=='VANCHI1' || $res_campania->id_regional=='BARMAR28' || $res_campania->id_regional=='COMEXT1'))
			{
				$sql="DELETE FROM comp_pedidos WHERE id_comp_ped=".$idCompPed."";

				$del=mysql_db_query($this->c_database,$sql,$this->link);

					if ($res_campania->id_regional!='MEXVAN1' || $res_campania->id_regional!='VANCHI1' || $res_campania->id_regional!='BARMAR28' || $res_campania->id_regional!='COMEXT1') {
							//auditar
							$audit="Eliminacion automatica de Cuota $idVd por encontrarse fuera de campaña - Rev $idVendedor -Ped $this->idPedido";
							$qrystr="INSERT INTO auditoria (cod_us,fecha, area, accion, detalle)
							VALUES ('$this->usuario',CURRENT_TIMESTAMP,'Coordinacion', 'Elimina','$audit')";
						 // echo "------------------------------------<br> $qrystr <br>-------------------";
							 $qry = mysql_db_query($this->c_database,$qrystr ,$this->link);
							echo"<font size='2' color='#252f8f' face='Verdana, Arial, Helvetica, sans-serif'><strong> - $audit <br></strong></font>";
					}
			}
			else
			{
				$sql="SELECT MAX(c.plan) as ultPlan	FROM cuotas as c";
				$qry2=mysql_db_query($this->c_database,$sql,$this->link);
				$res2=mysql_fetch_array($qry2);

				// Obtener el último ID de plan
				$sigPlan=($res2['ultPlan'])+1;

				$sql="INSERT INTO cuotas
					(id_comp_ped,cant_cuotas,plan,id_vd,vendedor,estado,campania)
					VALUES
					(".$idCompPed.",1,".$sigPlan.",'".$idVd."','".$idVendedor."','Abierto',".$this->campania.")";
				$qryinsB=mysql_db_query($this->c_database,$sql,$this->link);

				// Puesto de pecho por ahora, para cuando la cantidad de cuotas es menor - Javier 20131106
				if($idVd=='CVA0402'){ $comentario='COMPRADO EN CUOTAS 1/2';}
				else { $comentario='COMPRADO EN CUOTAS 1/3';}
				// -------------------------------------------------

				$sql="UPDATE comp_pedidos SET comentario='".$comentario."', orden = 'A'
						WHERE id_comp_ped=".$idCompPed."";
				//echo "$sql <br>";
				//die();
				$qry_upA=mysql_db_query($this->c_database,$sql,$this->link);
			}
		}
	}// End Class Cuotas
?>
