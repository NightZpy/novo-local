<?php
//******************************************* CLASE PEDIDO *************************************************
//*********************************************************************************************************
class GestorLogCambio
{
	var $debug;
	var $objetoInicial;
	var $objetoFinal;
  var $accion;
  var $campos_excluidos;
  var $diferencias;
  var $tabla;
  var $tipo_objeto;
  var $campo1;
  var $campo2;
  var $campo3;
  var $campo4;
  var $campo5;
  
	// ******************************************************************************************* //
	// ******************************************************************************************* //
	// ************** Constructor ****************
	function GestorLogCambio($tabla,$par_tipoObjeto='')
	{
  	$this->objetoInicial=array();
  	$this->objetoFinal=array();
  	$this->diferencias=array();
  	$this->campos_excluidos=array();
  	$this->tabla=$tabla;
  	if($par_tipoObjeto=='')
  	  $this->tipoObjeto=$tabla;
  	else  
      $this->tipoObjeto=$par_tipoObjeto;

  	$this->campo1='';
  	$this->campo2='';
  	$this->campo3='';
  	$this->campo4='';
  	$this->campo5='';

		$this->debug=false;
	}
	// ************ Fin Constructor **************
  // ************* Verifica si hubo errores sql **************
  function verificarSQL($qrystr,$metodo='')
   {
    $err = mysql_error();
    if($err<>'')
     {
      echo "Error en clase GestorOperaciones -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
     }
   }
  // ************************************************

	// *********************************************************
	function obtenerObjeto($filtro_unico)
	{
  	global $link;
  	global $c_database;
  	global $sesion;
  	global $usuario;

    //Obtener el registro requerido.
		$qrystr = " SELECT *
    	          FROM $this->tabla 
    	          WHERE $filtro_unico
    	        ";

		$qry = mysql_db_query($c_database,$qrystr,$link);
    $this->verificarSQL($qrystr,'do_promoModulos');
    if(mysql_num_rows($qry)>0)
      $row = mysql_fetch_assoc($qry);
    else
      $row=array();  
    return $row;
	}
	// ************************************************

	// *********************************************************
	function obtenerInicial($filtro_unico)
	{
    $this->objetoInicial = $this->obtenerObjeto($filtro_unico);
	}
	// ************************************************

	// *********************************************************
	function obtenerFinal($filtro_unico)
	{
    $this->objetoFinal = $this->obtenerObjeto($filtro_unico);
    $this->obtenerDiferencias();
	}
	// ************************************************

	// *********************************************************
	function obtenerDiferencias()
	{
    if(sizeof($this->objetoInicial)==0)//Si el objeto inicial es nulo, no hay diferencias v�lidas.
      $this->diferencias=array();
    else                               //El objeto inicial es nulo => comparar campo a campo el inicial con el final.
    {
      foreach($this->objetoInicial AS $campo => $valor)
      {
      if(!in_array($campo,$this->campos_excluidos))
        {
        if($valor<>$this->objetoFinal[$campo])
          $this->diferencias[$campo]=array('i'=>$valor,'f'=>$this->objetoFinal[$campo]);
        }
      }
    }
	}
	// ************************************************

	// *********************************************************
	function loguearDiferencias($id_objeto,$campo1='',$campo2='',$campo3='',$campo4='',$campo5='')
	{
  	global $link;
  	global $c_database;
  	global $sesion;
  	global $usuario;
  	
//   	switch ($this->tabla)
//     {
//       case 'productos':
//    	    if(sizeof($this->objetoInicial)==0)
//           $tipo_log=1;  //Alta Producto
//         else  
//      	    if(sizeof($this->objetoFinal)==0)
//             $tipo_log=6;  //Baja Producto
//           else  
//             $tipo_log=2;  //Modif Producto
//      	break;
//       case 'productos':
//    	    if(sizeof($this->objetoInicial)==0)
//           $tipo_log=1;  //Alta Producto
//         else  
//      	    if(sizeof($this->objetoFinal)==0)
//             $tipo_log=6;  //Baja Producto
//           else  
//             $tipo_log=2;  //Modif Producto
//      	break;
//     }

    if($campo1<>'')
      $this->campo1=$campo1;
    if($campo2<>'')
      $this->campo2=$campo2;
    if($campo3<>'')
      $this->campo3=$campo3;
    if($campo4<>'')
      $this->campo4=$campo4;
    if($campo5<>'')
      $this->campo5=$campo5;
    

    if($this->accion==''); 
    {
      if(sizeof($this->objetoInicial)==0)
        $this->accion=1;  //Alta
      else  
   	    if(sizeof($this->objetoFinal)==0)
          $this->accion=2;  //Baja
        else  
          $this->accion=3;  //Modif
    }

    //Almacenar Log.
		$qrystr = "INSERT INTO `vanesaduran_catalogo_paralelo`.`log_objetos` (
                  `tipo_objeto` ,`id_objeto` ,accion,
                  `detalle` ,
                  `fecha` ,`momento` ,`usuario` ,
                  `campo1` ,`campo2` ,`campo3` ,
                  `campo4` ,`campo5`
                  )
                  VALUES (
                  '$this->tipoObjeto', '$id_objeto','$this->accion',
                  '". str_replace("'","\'",var_export($this->diferencias,true)) ."', 
                  NOW(), NOW(), '$usuario',
                  '$this->campo1', '$this->campo2', '$this->campo3',
                  '$this->campo4', '$this->campo5'
                  );
    	        ";

		$qry = mysql_db_query($c_database,$qrystr,$link);
    $this->verificarSQL($qrystr,'do_promoModulos');

	}
	// ************************************************

}
// ************************************************}//end of class
//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************
/*
Ejemplo:
$gc= new GestorLogCambio('productos');
...
...
...
...
$gc->obtenerInicial("id_vd='$id_vd'");
...
...
...
$gc->obtenerFinal("id_vd='$id_vd'");
$gc->loguearDiferencias($id_vd);
...
...

*/

?>
