<?php
//******************************************* CLASE PEDIDO *************************************************
//*********************************************************************************************************
class GestorPuntaje
{
	var $debug;

	// ******************************************************************************************* //
	// ******************************************************************************************* //
	// ************** Constructor ****************
	function  GestorPuntaje()
	{
		$this->debug=false;
	}
	// ************ Fin Constructor **************

	// ************* Verifica si hubo errores sql **************
	function verificarSQL($qrystr,$metodo='')
	{
		$err = mysql_error();
		if($err<>'')
		{
			echo "Error en clase GestorPuntaje -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
		}
	}
	// ************************************************

	// ************ Sumar los puntos Promo **************
	function sumarPuntos($id_promo,$cant_puntos,$id_vendedor,$id_pedido=0,$tipo='A')
	{
		global $link;
		global $c_database;

		// ************** Grabar los datos de puntos. *********
		//echo "id_promo $id_promo";
		//Hago un insert en el LOG siempre.
		$qrystrlog="INSERT INTO `crit_promos_puntos_log` (`id_pedido` , `id_promo` , `usuario` , `puntos` , `fecha`, `tipo` ) VALUES (
                      '$id_pedido', '$id_promo', '$id_vendedor', '". (1*$cant_puntos) ."', NOW(),'$tipo')";
		$qrylog = mysql_db_query($c_database,$qrystrlog,$link);
		$this->verificarSQL($qrystrlog,'sumarPuntos');
		//echo $qrystrlog;
 
		//Hago un insert siempre. Si ya existe el registro usuario-promo, no lo inserta pero no muestra error.
		$qrystr10="INSERT INTO crit_promos_puntos( id_promo,usuario,puntos ) VALUES ('$id_promo','$id_vendedor','0')";
		$qry10 = mysql_db_query($c_database,$qrystr10,$link);
		//echo $qrystr10;
		//Hago un UPDATE de los puntos.
		$qrystr11="UPDATE crit_promos_puntos SET puntos = puntos + ". (1*$cant_puntos) ." WHERE id_promo='$id_promo' AND usuario='$id_vendedor'";
//		echo $qrystr11;
		$qry11 = mysql_db_query($c_database,$qrystr11,$link);
		$this->verificarSQL($qrystr11,'sumarPuntos');
		// ************** Fin Grabar los datos de puntos. *********
	}
	// ************************************************
	// ************ Borrar los puntos Promo **************
	function borrarPuntos($id_promo,$id_vendedor,$id_pedido=0)
	{
		global $link;
		global $c_database;

		$puntos = $this->getPuntosPromo($id_promo,$id_vendedor);
		$this->sumarPuntos($id_promo,((-1)*$puntos),$id_vendedor,$id_pedido);
		// ************** Fin Grabar los datos de puntos. *********
	}
	// ************************************************
	// ************ Puntos Promo **************
	function getPuntosPromo($id_promo,$id_vendedor)
	{
		global $link;
		global $c_database;

		//Obtener los Puntos en Promo de la promo $id_promo.
		$qrystr = "SELECT puntos FROM crit_promos_puntos WHERE id_promo='$id_promo' AND usuario='$id_vendedor'";
		if ($this->debug)
		echo "<BR><font color=#FF0000>getPuntosPromo: Param:<BR>qrystr: $qrystr. id_promo: $id_promo, id_vendedor: $id_vendedor</font><BR>" ;

		$qry = mysql_db_query($c_database,$qrystr,$link);
		$this->errores .= mysql_error();
		if ($this->debug)
		echo "<BR><font color=#FF0000>getPuntosPromo: Error: " . $this->errores . "</font><BR>" ;

		//Almacenar los elementos del conjunto en un array autoasociativo.
		if(mysql_num_rows($qry)==0)
		return 0;
		else
		{
			$row=mysql_fetch_array($qry);
			return $row[0];
		}
	}
	// ************************************************


	// ************ Puntos Promo del PedidoPR **************
	function getPuntosPedidoPR($id_promo,$id_vendedor,$id_pedido,$tipo='')
	{
		global $link;
		global $c_database;

		if($tipo<>'')
		$filtro = " AND tipo=='$tipo' ";
		//Obtener los Puntos del pedido $id_pedido de la Promo $id_promo.
		$qrystr = "SELECT IFNULL(SUM(puntos),0) FROM crit_promos_puntos_log WHERE id_promo='$id_promo' AND id_pedido='$id_pedido' AND usuario='$id_vendedor' $filtro";
		if($this->debug)
		{
			echo "<BR><font color=#FF0000>get_puntosPedidoPR: Param:<BR>qrystr: $qrystr. param: id_promo: $id_promo, id_vendedor: $id_vendedor, id_pedido: $id_pedido</font><BR>" ;
		}
		$qry = mysql_db_query($c_database,$qrystr,$link);
		//verificar($qry,$qrystr);
		$this->errores .= mysql_error();
		if($this->debug)
		{
			echo "<BR><font color=#FF0000>get_puntosPedidoPR: Error: " . $this->errores . "</font><BR>" ;
		}
		if(mysql_num_rows($qry)==0)
		return 0;
		else
		{
			$row=mysql_fetch_array($qry);
			return $row[0];
		}
	}
}
// ************************************************}//end of class
//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************
?>
