<?php
class Cuota
{
    public function Cuota($pedido,$c_databse,$link)
    {
    	$this->cantCuotas = 3;
    	$this->idPedido = $pedido;
    	$this->c_database = $c_databse;
    	$this->link = $link;
    }	
    
    public function hasCuotas()
    {
    	$qrystr = "SELECT * FROM comp_pedidos WHERE id_pedido = ".$this->idPedido." AND LEFT(id_vd,3) = 'CRE'";
    	$qry = mysql_db_query($this->c_database,$qrystr,$this->link);
    	
    	if(mysql_num_rows($qry) > 0)
    	    return true;
    	    
    	return false;    
    }
    
    public function generateCuotas()
    {
    	$qrystr = "SELECT * FROM comp_pedidos WHERE id_pedido = ".$this->idPedido." AND LEFT(id_vd,3) = 'CRE'";
    	$qry = mysql_db_query($this->c_database,$qrystr,$this->link);

        if(mysql_num_rows($qry) > 0){
    		while($row = mysql_fetch_array($qry)){
    			$y = 0;
    			for($x=1;$x<=$row['cantidad'];$x++){
    				//Verificamos que ya existan cuotas para el articulo
    				$e = $this->existenCuotas($row['id_vd'],$row['id_vendedor'],$x-$y);
    				if($e == null){
    					//Generamos el nuevo plan de cuotas
    					$newPlan = $this->getNewPlan();
    					$z = 1;
    					$qrystr1 = "INSERT INTO cuotas (id_comp_ped,cant_cuotas,plan,id_vd,vendedor)
    					            VALUES ('".$row['id_comp_ped']."','".$this->cantCuotas."','".$newPlan."','".$row['id_vd']."','".$row['id_vendedor']."')";
    					$plan = $newPlan;
    				}else{
    					//Continuamos con el plan de cuotas
    					$qrystr1 = "INSERT INTO cuotas (id_comp_ped,cant_cuotas,plan,id_vd,vendedor)
    					            VALUES ('".$row['id_comp_ped']."','".$e['cant_cuotas']."','".$e['plan']."','".$row['id_vd']."','".$row['id_vendedor']."')";
    					$z = $e['cuotas'];
    					$plan = $e['plan'];
    				}
                                $qry1 = mysql_db_query($this->c_database,$qrystr1,$this->link);
    				//Verificamos si ya se cumplio el plan
    				if($z == $this->cantCuotas){
                                    $this->finalizarPlan($plan,$row['id_vd'],$row['id_vendedor']);    
    				    $y++;
    				}    
    			}
    		}
    	}
    }
    
    protected function finalizarPlan($plan,$id_vd,$vendedor)
    {
    	//$qrystr = "SELECT COUNT(plan) AS cuotas FROM cuotas WHERE
    	//           plan = '".$plan."' AND id_vd = '".$id_vd."' AND vendedor = '".$vendedor."' AND estado = 'Abierto'";
    	$qrystr = "SELECT COUNT(*) AS cuotas FROM comp_pedidos WHERE id_vendedor = '".$vendedor."' AND id_vd = '".$id_vd."'";
    		
        $qry = mysql_db_query($this->c_database,$qrystr,$this->link);
    	$result = mysql_fetch_array($qry);
    	
    	if($result['cuotas'] == $this->cantCuotas){
    		//Plan finalizado. Se cierra el plan y se hace efectivo el articulo
    		$product = $this->getProductOriginal($id_vd);
    	    $qrystr3 = "SELECT cp.* FROM comp_pedidos AS cp
    	                INNER JOIN cuotas AS cuo ON(cuo.id_comp_ped=cp.id_comp_ped)
    	                WHERE cuo.plan = '".$plan."' ORDER BY cp.id_comp_ped DESC";
    	    $qry3 = mysql_db_query($this->c_database,$qrystr3,$this->link);
    	    $result3 = mysql_fetch_object($qry3);
    	    //Insertamos el producto original al pedido
    	    $qrystr4 = "INSERT INTO comp_pedidos (id_pedido,id_producto,id_vd,nombreproducto,comentario,preciounidad,cantidad,id_distrib,id_lider,id_vendedor,fecha)
    	                VALUES ('".$result3->id_pedido."','".$product->id_producto."','".$product->id_vd."','".$product->nombreproducto."','COMPRADO EN CUOTAS',0,1,'".$result3->id_distrib."','".$result3->id_lider."','".$result3->id_vendedor."','CURRENT()')";
    	    $qry4 = mysql_db_query($this->c_database,$qrystr4,$this->link);
    	    //Cerramos el plan
    	    $qrystr5 = "UPDATE cuotas SET estado = 'Cerrado' WHERE plan = '".$plan."'";
    	    $qry5 = mysql_db_query($this->c_database,$qrystr5,$this->link);
    	    
    	    return true;
    	}
    	
    	return false;
    }
    
    protected function existenCuotas($id_vd,$vendedor,$iteration)
    {
    	//Este metodo verifica si ya existe un plan de cuotas para este $id_vd y este vendedor
    	$qrystr = "SELECT c.*,cp.id_pedido AS pedido FROM cuotas AS c
    	           INNER JOIN comp_pedidos AS cp ON(cp.id_comp_ped=c.id_comp_ped)
    	           WHERE c.vendedor = '".$vendedor."' AND c.id_vd = '".$id_vd."' AND c.estado = 'Abierto' AND cp.id_pedido != '".$this->idPedido."'
    	           GROUP BY c.plan";
    	//echo $qrystr;
        $qry = mysql_db_query($this->c_database,$qrystr,$this->link);
    	
    	$rows = mysql_num_rows($qry);
    	if($rows > 0 && $iteration <= $rows){
    		mysql_data_seek($qry,$iteration-1);
    		$result = mysql_fetch_array($qry,MYSQL_ASSOC);
    		$array['plan'] = $result['plan'];
    		$array['cant_cuotas'] = $result['cant_cuotas'];
    		$qrystr1 = "SELECT COUNT(*) AS cuotas FROM comp_pedidos WHERE id_vendedor = '".$vendedor."' AND id_vd = '".$id_vd."'";
    		$qry1 = mysql_db_query($this->c_database,$qrystr1,$this->link);
    		$row1 = mysql_fetch_array($qry1);
    		$array['cuotas'] = $row1['cuotas'];
    		
    		return $array;
    	}
    	
    	return false;
    }
    
    protected function getNewPlan()
    {
    	$newPlan = '00001';
    	
    	$qrystr = "SELECT * FROM cuotas GROUP BY plan";
    	$qry = mysql_db_query($this->c_database,$qrystr,$this->link);
    	
    	$rows = mysql_num_rows($qry);
    	if($rows > 0){
    		$len = strlen($rows);
    		$newPlan = $rows+1;
    		for($x=0;$x<(5-$len);$x++){
    			$newPlan = '0'.$newPlan;
    		}
    	}
    	
    	return $newPlan;
    }
    
    protected function getProductOriginal($id_vd)
    {
    	//Obtenemos el producto original a agregar en el pedido una vez cumplido el plan de cuotas
    	$qrystr1 = "SELECT descr_txt FROM productos WHERE id_vd = '".$id_vd."'";
    	$qry1 = mysql_db_query($this->c_database,$qrystr1,$this->link);
    	$result1 = mysql_fetch_array($qry1);
    	$rr = explode('-',$result1['descr_txt']);
    	$id_vd_orig = trim($rr[0]);
    	
    	$qrystr2 = "SELECT * FROM productos WHERE id_vd = '".$id_vd_orig."'";
    	$qry2 = mysql_db_query($this->c_database,$qrystr2,$this->link);
    	$product = mysql_fetch_object($qry2);
    	
    	return $product;
    }
    
    public function deleteCuotas()
    {
    	$qrystr = "SELECT id_comp_ped FROM comp_pedidos WHERE id_pedido = '".$this->idPedido."' AND LEFT(id_vd,3) IN('CRE')";
    	$qry = mysql_db_query($this->c_database,$qrystr,$this->link);
    	while($row = mysql_fetch_object($qry)){
    		$qrystr1 = "DELETE FROM cuotas WHERE id_comp_ped = '".$row->id_comp_ped."'";
    		$qry1 = mysql_db_query($this->c_database,$qrystr1,$this->link);
    	}
    	
    	return true;
    }
    
}
?>
