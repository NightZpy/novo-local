<?php
class ArmadoControl
{
	function LoadData($qrystr)
	{
	// echo $qrystr;
	  
	  $qry = mysql_query($qrystr);
	  // Si hay algun error, lo muestro
	  $err=mysql_error();
	  if($err<>'')
		echo "Consulta: $qrystr<br>Error: $err";
		
	  $data=array();
	  while ($row = mysql_fetch_array($qry)){
		$line=utf8_encode($row[c]);
		$data[]=explode(';',chop($line));
	  }
	//  echo "data:::::".$data;
		//var_dump($data);
		return $data;
	}
		
	function creaTabla($data,$header,$titulo,$subtitulo,$color,$boton) 
	{
		$tabla.="<table class='lista' >";
		$gr=count($header);
		if($boton) { $gr=count($header)+1; }
		if($titulo) {	$tabla.="<tr><th colspan=".$gr." align='center'>$titulo</th></tr>"; }
		if($subtitulo) {	$tabla.="<tr><th colspan=".$gr." >$subtitulo</th></tr>"; }
		if($header){
			$tabla.="<tr class='enc'>";
			for($i=0;$i<count($header);$i++){
				$tabla.="<td>".$header[$i]."</td>";
			}
			 if($boton) {$tabla.="<td>&nbsp;</td>";}
			$tabla.="</tr>";	
		}
		if($color) $colorstyle="style='background-color:".$color."'"; else $colorstyle="";	
		$cantidad=count($data);
		for($i=0;$i<$cantidad;$i++){
			 $tabla.="<tr $colorstyle >";
			 $cantidad2=count($data[$i]);
			 for($j=0;$j<$cantidad2;$j++){
				 if($boton) { 
					if ($j==$cantidad2-1) {	
							$tabla.="<td><a class='opc' style='margin:0px;' href='#' onclick=metodoClick('".$data[$i][0]."')></a><p id='".$data[$i][0]."'>".$data[$i][$j]."</p></td>";
					 }else{ 
							$tabla.="<td><p>".$data[$i][$j]."</p></td>";	
					}
				}else{ 
					$tabla.="<td><p style='padding:3px'>".$data[$i][$j]."</p></td>";	
				}
			 }
			 $tabla.="</tr>";
		}
		$tabla.="</table>";
		return $tabla;
	}

	function traerListaArmadores(){

			$query = "SELECT usuario 
			   FROM armadoras ORDER BY usuario ASC";
			$resultSet = mysql_query($query) or die("Error de SQL select  $query");
			
			$armadores = array();
			while($temp = mysql_fetch_assoc($resultSet)){
				$armadores[] = new Armadores($temp['usuario']);
			}
			return $armadores;
	}

	function traerListaFaltantes($atrib,$color,$func){

//p.id_vd, p.stk1, p.stk1_min, p.stk2, p.stk3, p.stk4, p.stk5, p.aux_acc, p.dep_acc1, p.dep_acc2, p.stk_pedido, p.stk_en_transito, p.stk_en_recepcion, p.stk_en_proceso 	, p.stk_faltante, p.stk_excedente, p.stock_min, p.stk_activo, p.stock
		$query = "SELECT  CONCAT(p.id_vd,';',p.id_producto,';',p.stk1,';',p.stk4,';',p.dep_acc1,';',p.dep_acc2,';',p.stk5,';',p.stk6,';',lpp.opcional) as c
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		  WHERE p.id_vd not in
				(
					SELECT gl.elemento
					from grupos_lista as gl 
					Where  gl.id_grupo=135  
				) 
		AND p.id_vd not in
				(
					SELECT gl.elemento
					from grupos_lista as gl 
					Where  gl.id_grupo=242
				)
		  AND lp.activo=1 AND lp.predet=1
		  AND lpp.id_atrib IN (" . implode(",", $atrib) . ") 
		  GROUP BY p.id_producto
	  	HAVING sum( p.stk1+p.stk3+p.stk4+p.dep_acc1+p.dep_acc2+p.stk5+p.stk6)<=0 ";
			  // echo $query."<br>";
		
		$header=array("Id Vd","Id Producto","Gondola Joyas","Deposito joyas","Gondola Acc","Deposito Acc","Gondola Grabado","Deposito Grabado");
		$data = $this->LoadData($query);
		$faltante= $this->creaTabla($data,$header, "Listado de Faltantes","",$color,$func);
		echo $faltante;
		
	}
	
	function traerListaCasiFaltantes($atrib,$color,$func){

//p.id_vd, p.stk1, p.stk1_min, p.stk2, p.stk3, p.stk4, p.stk5, p.aux_acc, p.dep_acc1, p.dep_acc2, p.stk_pedido, p.stk_en_transito, p.stk_en_recepcion, p.stk_en_proceso 	, p.stk_faltante, p.stk_excedente, p.stock_min, p.stk_activo, p.stock
		$query = "SELECT sum( p.stk1+p.stk4+p.stk3+p.dep_acc1+p.dep_acc2+p.stk5+p.stk6) as total, CONCAT(p.id_vd,';',p.id_producto,';',p.stk1,';',p.stk4,';',p.dep_acc1,';',p.dep_acc2,';',p.stk5,';',p.stk6,';',lpp.opcional) as c
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		   WHERE lp.activo=1 and lp.predet=1  
		    	AND p.id_vd not in
				(
					SELECT gl.elemento
					from grupos_lista as gl 
					Where  gl.id_grupo=135  
				) 
			AND p.id_vd not in
				(
					SELECT gl.elemento
					from grupos_lista as gl 
					Where  gl.id_grupo=242
				)
		   AND lpp.id_atrib IN (" . implode(",", $atrib) . ") 
		   GROUP BY p.id_producto
	HAVING sum( p.stk1+p.stk4+p.stk3+p.dep_acc1+p.dep_acc2+p.stk5+p.stk6)<=40 AND sum( p.stk1+p.stk3+p.stk4+p.dep_acc1+p.dep_acc2+p.stk5+p.stk6)>=1";
	//	echo $query;
		
		$header=array("Id Vd","Id Producto","Gondola Joyas","Deposito joyas","Gondola Acc","Deposito Acc","Gondola Grabado","Deposito Grabado");
		$data = $this->LoadData($query);
		$faltante= $this->creaTabla($data,$header,"","",$color,$func); 
		echo $faltante;
		
	}
	
	function CompruebaFaltante($prod){ 
		$query = "SELECT p.id_vd
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		  WHERE p.stk1<=0 AND p.stk4<=0  AND p.stk3<=0 AND p.dep_acc1<=0 AND p.dep_acc2<=0 AND lp.activo=1 and lp.predet=1 
		  AND  p.id_vd='".$prod."'";
	//	echo $query."<br />";
		$result = mysql_query($query) or die("Error de SQL select  $query");
		$cant = mysql_num_rows($result);
		if($cant>0)
			return true;
		else
			return false;
	}
	
	function CompruebaFaltanteGrabado($prod){ 
		$query = "SELECT p.id_vd
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		  WHERE p.stk5<=0 AND p.stk6<=0 AND lp.activo=1 and lp.predet=1 
		  AND  p.id_vd='".$prod."'";
	//	echo $query."<br />";
		$result = mysql_query($query) or die("Error de SQL select  $query");
		$cant = mysql_num_rows($result);
		if($cant>0)
			return true;
		else
			return false;
	}
	
	function compruebaProducto($prod){ 
		$query = "SELECT p.id_vd
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		  WHERE lp.activo=1 and lp.predet=1 
		  AND  p.id_vd='".$prod."'";
	//echo $query."<br />";
		$result = mysql_query($query) or die("Error de SQL select  $query");
		$cant = mysql_num_rows($result);
		if($cant>0)
			return true;
		else
			return false;
	}
	 
	function CompruebaOpcional($prod){
		$queryCO = "SELECT lpp.opcional AS opcional
		   FROM productos AS p
			INNER JOIN  listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
		   INNER JOIN  listas_precios AS lp ON lp.id_lista=lpp.id_lista
		  WHERE p.stk1<=0 AND p.stk4<=0  AND p.stk3<=0 AND p.dep_acc1<=0 AND p.dep_acc2<=0 AND lp.activo=1 and lp.predet=1 
		  AND  p.id_vd='".$prod."'";
		  
		$resultCO = mysql_query($queryCO) or die("Error de SQL select  $queryCO"); 
	//	echo $queryCO;
	if(mysql_num_rows($resultCO)>0){
		$row = mysql_fetch_array($resultCO);
			if($row[opcional]<>""){
				$opc=$row[opcional];
				$queryID = "SELECT id_producto
				   FROM productos
				  WHERE id_vd='".$opc."'";
				//  echo $queryID."<br /><br />";
			   $resultID = mysql_query($queryID) or die("Error de SQL select  $queryID"); 
			   $rowID = mysql_fetch_array($resultID);
				$id=$rowID[id_producto];
				return $id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	function BuscaId($prod){
		$query = "SELECT id_producto
		   FROM productos 
		  WHERE id_vd='".$prod."'";
		//    ECHO $query;
		$result = mysql_query($query) or die("Error de SQL select  $query"); 
		$row = mysql_fetch_array($result);
		$id=$row[id_producto];
		return $id;
	}
	
	function BuscaId_VD($prod){
		$query = "SELECT id_vd
		   FROM productos 
		  WHERE id_producto='".$prod."'";
		//  ECHO $query;
		$result = mysql_query($query) or die("Error de SQL select  $query"); 
		$row = mysql_fetch_array($result);
		$id_vd=$row[id_vd];
		return $id_vd;
	}
	function BuscaNombre($prod){
		$query = "SELECT CONCAT( '+(', id_vd, ') ', nombreproducto ) AS nombreproducto
		   FROM productos 
		  WHERE id_producto='".$prod."'";
		//  ECHO $query;
		$result = mysql_query($query) or die("Error de SQL select  $query"); 
		$row = mysql_fetch_array($result);
		$nombreproducto=$row[nombreproducto];
		return $nombreproducto;
	}

	function cambiaOpcional($pedido, $opc, $id, $prod){
		$qrystr_ped = " select id_lista_precios FROM pedido WHERE id_pedidos = '".$pedido."'";
		$qry_ped = mysql_query($qrystr_ped) or die("Error de SQL select  $qrystr_ped");
		$row_ped = mysql_fetch_array($qry_ped);
		
		$qrystr ="select p.*,lpp.precio AS precio_moneda
			 FROM productos AS p
			   LEFT JOIN listas_precios_productos AS lpp ON p.id_vd = lpp.id_vd AND lpp.id_lista='$row_ped[id_lista_precios]'
			 WHERE p.id_producto = '$opc'";
		//echo $qrystr;
		$qry = mysql_query($qrystr) or die("Error de SQL select  $qrystr");
		
		if(mysql_num_rows($qry)>0){
			$resul = mysql_fetch_array($qry);
			
			if($row_ped["id_lista_precios"]<>1)  //La lista 1 es la por defecto y debe tomar los valores de la tabla productos.
			   $precio = "precio_moneda";                     //Precio moneda destino
			 else
			   $precio = "preciounidad";                      //Precio de la Intranet
			   
			 if($resul[$precio]=='')//Si no existe el precio, no es válido
			   $precio_valido = false;
			 else
			   $precio_valido = true;

			 if (mysql_affected_rows()==1 and $precio_valido)
			 {	
			
				$aud=$prod;
				$qrystrU ="UPDATE comp_pedidos
				SET id_producto=$opc,  cantidad=cantidad_orig, id_vd='$resul[id_vd]', nombreproducto='$resul[nombreproducto]', 
				preciounidad='$resul[$precio]', obs='$aud'
				WHERE  id_comp_ped = '$id'";
			//	echo $qrystrU;
				$qryU = mysql_query($qrystrU) or die("Error de SQL select  $qrystrU");
				
				$qrystrI ="INSERT INTO `det_modif_pedido` (id_pedido, id_comp_pedido, detalle, donde) 
				VALUES ($pedido, $id, ' $prod ',2) ";
				//echo $qrystrI;
				$qryI = mysql_query($qrystrI) or die("Error de SQL select  $qrystrI");
				return true;
			}
			return false;
	 }else{
		return false;
	 }	
	 
	}
	function cambiaFaltante($id){
			/* Faltante  */
		$qrystrF=" SELECT id_comp_ped FROM comp_pedidos  WHERE det=0  and id_comp_ped = '$id'";
	//	echo $qrystrF."<br />";
		$qryF = mysql_query($qrystrF) or die("Error de SQL select  $qrystrF");
		$qryF = mysql_num_rows($qryF);
		if($qryF>0){
				/* Faltante  */
			$qrystr=" UPDATE comp_pedidos  SET cantidad='0', obs=' FALTANTE!' WHERE  id_comp_ped = '$id'";
		//	echo $qrystr;
			$qry = mysql_query($qrystr) or die("Error de SQL select  $qrystr");
		}
	}
	
	
	function tiempoBreak($ini){
		$fecha_actual= date("Y-m-d"); 
		$queryA = "SELECT COUNT(ini) as cantidadArmo
		   FROM armados 
		  WHERE  ini='$ini' and tipo=1
		  AND  fecha='$fecha_actual'";
		//echo $queryA."<br />";
		$resultA = mysql_query($queryA) or die("Error de SQL select  $queryA");
		$rowA = mysql_fetch_array($resultA);
		
		$queryB = "SELECT COUNT(ini_control) as cantidadControlo
		   FROM armados 
		  WHERE  ini_control='$ini' and tipo=1
		  AND  fecha='$fecha_actual'";
		//echo $queryB."<br />";
		$resultB = mysql_query($queryB) or die("Error de SQL select  $queryB");
		$rowB = mysql_fetch_array($resultB);
		
		$queryC ="SELECT nyape FROM armadoras WHERE ini='$ini'";
		//echo $sql_ini."<br />";
		$resultC = mysql_query($queryC) or die("Error de SQL select  $queryC");
		$row_ini = mysql_fetch_array($resultC);

		$cantidad=$rowA[cantidadArmo]+$rowB[cantidadControlo];
		
		
		if ($cantidad==35)
			$msje=$row_ini[nyape]." ya puedes comenzar a disfrutar tu break de 15'";
		elseif($cantidad==70)
			$msje=$row_ini[nyape]." ya puedes comenzar a disfrutar su break de 7'";
		elseif($cantidad==105)
			$msje=$row_ini[nyape]." ya puedes comenzar a disfrutar su break de 7'"; 
		elseif($cantidad==140)
			$msje=$row_ini[nyape]." ya puedes comenzar a disfrutar su break de 7'"; 
		else
			$msje="Cantidad de pedidos: ".$cantidad;
				
		return $msje;

	}	
	
	/*function modificado($inn,$pedido,$usuario2,$fil){
	 $qrystr3 = "select cp.id_comp_ped AS cpid, cp.id_vd_orig AS cpid_vd_orig, cp.id_producto AS cpid_producto, cp.cantidad AS cpcantidad, cp.id_vd AS cpid_vd,  cp.id_vd_orig AS cpid_vd_orig, 
	concat(IF(ISNULL(p2.id_vd),'',CONCAT('+(',p2.id_vd,') ')),cp.nombreproducto) AS cpnombreproducto, cp.comentario AS cpcomentario, cp.obs AS cpobs, lpp.opcional AS lppopc, cp.det AS cpdet
            FROM comp_pedidos AS cp
            INNER JOIN  listas_precios_productos as lpp on lpp.id_vd=cp.id_vd_orig
			INNER JOIN  pedido as p on p.id_pedidos=cp.id_pedido 
			$inn
              LEFT JOIN productos AS p1 ON cp.id_vd = p1.id_vd
              LEFT  JOIN productos AS p2 ON p1.id_producto_empaque = p2.id_producto
            WHERE  cp.id_pedido = $pedido
			  AND cp.id_vendedor = '$usuario2'
			  AND lpp.id_lista=p.id_lista_precios
			  $fil
             ORDER BY concat(cp.orden,cp.id_vd,cp.id_comp_ped)";
			//echo $qrystr3;
			$qry3 = mysql_query($qrystr3) or die("Error de SQL select  $qrystrI");
			while ($row3 = mysql_fetch_array($qry3))
			{
				$id=$row3[cpid];
				$id_vd=$row3[cpid_vd];

				$faltante=$this->CompruebaFaltante($id_vd);
				if($faltante){
					  if (!empty($row3[cpobs]) AND ($row3[cpobs]<>" FALTANTE!")){
						  $faltanteweb=$this->CompruebaFaltante($row3[cpobs]);
						  if($faltanteweb){
								 $opcionalvd=$this->CompruebaOpcional($row3[cpid_vd]);
								if($opcionalvd){
									$id_vd=$this->BuscaId_VD($opcionalvd);
									$faltantevd=$this->CompruebaFaltante($id_vd);
									if($faltantevd){
										$this->CambiaFaltante($id);
									}else{
										$prod="OPC CLIENTE FALTANTE:(".$row3[cpobs].") OPC VD:(".$row3[cpobs].") FALTANTE: $row3[cpid_vd]";
										$this->cambiaOpcional($pedido,$opcionalvd,$row3[cpid],$prod);
									}
								}
						  }else{
							  $prod="OPC CLIENTE:(".$row3[cpobs].") FALTANTE: ".$row3[cpid_vd];
							  $id_prod=$this->BuscaId($row3[cpobs]);
							  $this->cambiaOpcional($pedido,$id_prod,$row3[cpid],$prod);
						  }
					}else{  
						$opcionalvd=$this->CompruebaOpcional($row3[cpid_vd]);
						if($opcionalvd){
							$id_vd=$this->BuscaId_VD($opcionalvd);
							$faltantevd=$this->CompruebaFaltante($id_vd);
								if($faltantevd){
										$this->CambiaFaltante($id);
										$estilo="class='faltante'";
								}else{
										$prod="OPC VD:(".$id_vd.") FALTANTE: $row3[cpid_vd]";
										$this->cambiaOpcional($pedido,$opcionalvd,$row3[cpid],$prod);
								}
						}else{
								$this->CambiaFaltante($id);
						}
					 }
				}
			}

	}*/
}
?>
