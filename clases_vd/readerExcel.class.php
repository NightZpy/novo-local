<?php
class readerExcel extends Spreadsheet_Excel_Reader
{
	public function getObjects($rows, $cols, $type)
	{
		if($type == 'Catalogo')
		    $arr = $this->getObjectsCatalogo($rows, $cols);
		elseif($type == 'Lista')
		    $arr = $this->getObjectsLista($rows, $cols);
		    
		return $arr;    
	}
	
	protected function getObjectsCatalogo($rows, $cols)
	{
		$c = 0;
		$first = 9999;
		$arr = array();
		$fields = array('stk_activo','stock','control_web','orden','id_producto_empaque','tipo_garantia','id_vd','nombreproducto','ubi','id_vd_fact','preciounidad');
		
		for($i=1;$i<=$rows;$i++){
	        //calculamos la primera fila de datos validos
	        if($this->getFirstRow($this->sheets[0]['cells'][$i], 'Catalogo'))
	            $first = $i;    
			for($j=1;$j<=$cols;$j++){
		        if($i > $first){
				    $object = new stdClass();
		        	if(isset($fields[$j-1]))
		        	    $objArr[$fields[$j-1]] = $this->sheets[0]['cells'][$i][$j];
		        }
	        }
	        if($i > $first){
	            $arr[$c] = (object)$objArr;
	            $c++;
	        }    
        }
        
        return $arr;
	}
	
	protected function getObjectsLista($rows, $cols)
	{
		$c = 0;
		$first = 9999;
		$arr = array();
		$fields = array('id_vd','preciounidad','atributo','usuario');
		
		for($i=1;$i<=$rows;$i++){
	        //calculamos la primera fila de datos validos
	        if($this->getFirstRow($this->sheets[0]['cells'][$i], 'Lista'))
	            $first = $i;    
			for($j=1;$j<=$cols;$j++){
		        if($i > $first){
				    $object = new stdClass();
		        	if(isset($fields[$j-1]))
		        	    $objArr[$fields[$j-1]] = $this->sheets[0]['cells'][$i][$j];
		        }
	        }
	        if($i > $first){
	            $arr[$c] = (object)$objArr;
	            $c++;
	        }    
        }
        
        return $arr;
	}
	
	protected function getFirstRow($row, $type)
	{
		if($type == 'Catalogo' AND ($row[1] == 'stk_activo' && $row[2] == 'stock' && $row[3] == 'control_web' && $row[4] == 'orden' && $row[5] == 'id_producto_empaque' && $row[6] == 'tipo_garantia' && $row[7] == 'id_vd'))
		    return true;
		elseif($type == 'Lista' AND ($row[1] == 'id_vd' && $row[2] == 'preciounidad' && $row[3] == 'atributo'))
		    return true;    
		    
		return false;    
	}
	
	public function exist($id_vd, $type, $listaId = null)
	{
		global $c_database;
		global $link;
		
		if($type == 'Catalogo')
		    $qrystr = "SELECT id_vd FROM productos WHERE id_vd = '".$id_vd."'";
		elseif($type == 'Lista')
		    $qrystr = "SELECT id_vd FROM listas_precios_productos WHERE id_vd = '".$id_vd."' AND id_lista = $listaId";
		    
		$qry = mysql_db_query($c_database,$qrystr,$link);
		
		if(mysql_num_rows($qry) > 0)
		    return true;
		    
		return false;    
	}
	
	public function update($obj, $type, $listaId)
	{
		global $c_database;
		global $link;
		
	
		if($type == 'Catalogo') {
			if ($listaId[0]=="on")
				$qrystr1=$qrystr1."stk_activo=".$obj->stk_activo."";  			
			if ($listaId[1]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."stock=".$obj->stock."";  
				else	
					$qrystr1=$qrystr1.",stock=".$obj->stock."";  
			if ($listaId[2]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."control_web=".$obj->control_web."";  
				else	
					$qrystr1=$qrystr1.",control_web=".$obj->control_web."";  
			if ($listaId[3]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."orden='".$obj->orden."'";  
				else	
					$qrystr1=$qrystr1.",orden='".$obj->orden."'";  
			if ($listaId[4]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."tipo_garantia='".$obj->tipo_garantia."'";  
				else	
					$qrystr1=$qrystr1.",tipo_garantia='".$obj->tipo_garantia."'";  
			if ($listaId[5]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."id_producto_empaque=".$obj->id_producto_empaque."";  
				else	
					$qrystr1=$qrystr1.",id_producto_empaque=".$obj->id_producto_empaque."";  
			if ($listaId[6]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."id_vd_fact='".$obj->id_vd_fact."'";  
				else	
					$qrystr1=$qrystr1.",id_vd_fact='".$obj->id_vd_fact."'";  
			if ($listaId[7]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."ubi='".$obj->ubi."'";  
				else	
					$qrystr1=$qrystr1.",ubi='".$obj->ubi."'";  
			if ($listaId[8]=="on")			  
				if (qrystr1=="") 			  
					$qrystr1=$qrystr1."preciounidad=".$obj->preciounidad."";  
				else	
					$qrystr1=$qrystr1.",preciounidad=".$obj->preciounidad."";  
			$qrystr = "UPDATE productos SET ".$qrystr1." WHERE id_vd='".$obj->id_vd."'";
			//$qrystr = "UPDATE productos SET stk_activo=".$obj->stk_activo.",stock=".$obj->stock.",control_web=".$obj->control_web.",orden='".$obj->orden."',id_producto_empaque=".$obj->id_producto_empaque.",tipo_garantia='".$obj->tipo_garantia."',ubi='".$obj->ubi."',id_vd_fact='".$obj->id_vd_fact."',preciounidad=".$obj->preciounidad." WHERE id_vd='".$obj->id_vd."'";
		}elseif($type == 'Lista')
		    $qrystr = "UPDATE listas_precios_productos SET preciounidad=".$obj->preciounidad.",atributo=".$obj->atributo.",usuario='".$obj->usuario."' WHERE id_vd='".$obj->id_vd."' AND id_lista = $listaId";
		    
		$qry = mysql_db_query($c_database,$qrystr,$link);
		
		return true;
	}
	
	public function insert($obj, $type, $listaId = null)
	{
		global $c_database;
		global $link;
		
		if($type == 'Catalogo'){
		    $qrystr = "INSERT INTO productos (stk_activo,stock,control_web,orden,id_producto_empaque,tipo_garantia,ubi,id_vd_fact,preciounidad,id_vd,nombreproducto) VALUES('".$obj->stk_activo."','".$obj->stock."','".$obj->control_web."','".$obj->orden."','".$obj->id_producto_empaque."','".$obj->tipo_garantia."','".$obj->ubi."','".$obj->id_vd_fact."','".$obj->preciounidad."','".$obj->id_vd."','".$obj->nombreproducto."')";
		}elseif($type == 'Lista'){
		    $qrystr = "INSERT INTO listas_precios_productos (id_lista,id_vd,precio,usuario,momento,id_atrib) VALUES('".$listaId."','".$obj->id_vd."','".$obj->preciounidad."','".$obj->usuario."',NOW(),'".$obj->atributo."')";    
		}
		$qry = mysql_db_query($c_database,$qrystr,$link);
		
		return true;
	}
	
}//fin class
