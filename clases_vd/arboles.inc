<?php
$paso_images_gif="/vanesdur/images_gif";
// *******************************************************************************************
// ***************************************************** CLASE Nodo **************************
class Nodo
 {
  var $arbolId;
	var $_id;
	var $_nivel;
	var $_padre;
	var $_hijos;
  var $_datosNodo;
	var $manejador;

  // ************************************ CONSTRUCTOR ************************************************
  function Nodo($parId,&$par_padre,$parArbolId,&$parManejador,$par_datosNodo=null)
   {

   	if ($par_datosNodo===null)
     {
   		// this is a root node
   		$this->_nivel=0;
   	 }
   	$this->_datosNodo=$par_datosNodo;
   	$this->_id=$parId;
   	$this->arbolId = $parArbolId;
		$this->_hijos=array(); //initialize with no children

		// for the root node parent will be null
		$this->_padre=$par_padre;
		//$this->BuscarDescendencia();

		$this->manejador=$parManejador;

	 }
	// *************************************************************************************************

	// *************************** verificarSQL ********************************************************
  // *************************** Verifica si hubo errores sql ****************************************
  function verificarSQL($qrystr,$metodo='')
   {
    $err = mysql_error();
    if($err<>'')
     {
      echo "Error en clase Nodo -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
     }
   }
	// *************************************************************************************************

	// *************************** BuscarDescendencia **************************************************
	function BuscarDescendencia($nivel_tope=0,$nivel_actual=0)
	 {
	  global $sitio,$link,$usuario,$sesion;
	  global $c_database;

	  $nivel_actual++;
	  if($nivel_tope<>0)
	   {
	    if($nivel_actual>$nivel_tope)
	      return;
	   }

	 	$query = $this->construirConsultaHijos();

	 	//echo "<br>$query";
	  $qry = mysql_db_query($c_database,$query,$link);
	  $this->verificarSQL($query,'BuscarDescendencia');

    $filas_rev=mysql_num_rows($qry);
    //echo "<br>Hijos: $filas_rev";
	  while ($row = mysql_fetch_array($qry))
  	  $this->_hijos[]= &new Nodo($row['cod_us'],&$this,$this->arbolId,&$this->manejador,array('rowdatos'=>$row));

  	if(sizeof($this->_hijos)==0)									//Si no tiene hijos es hoja, de lo contrario es carpeta.
  	  $this->_datosNodo["tipo_nodo"]='hoja';
		else
  	  $this->_datosNodo["tipo_nodo"]='carpeta';

	  //Almacenar la cantidad de hijos.
  	$this->_datosNodo["cant_hijos"]=sizeof($this->_hijos);

  	for($i=0;$i<sizeof($this->_hijos);$i++)
  	 {
  	 	//echo"<br>padre: $this->_id - hijo: $nodoActual->_id";
  	  $this->_hijos[$i]->BuscarDescendencia($nivel_tope,$nivel_actual);
  	 }
	 }
	// *************************************************************************************************

	// *************************** BuscarRowDatosNodo **************************************************
	//Un nodo puede buscar sus propios datos. Sirve cuando es el ra�z, ya que en tal caso no hay un padre que entregue el RowDatos.
	function BuscarRowDatosNodo()
  {
		global $sitio,$link,$usuario,$sesion;
	  global $c_database;

	  $query = $this->construirConsultaRaiz($parNodoId);

	 	//echo "<br>$query";
	  $qry = mysql_db_query($c_database,$query,$link);
	  $this->verificarSQL($query,'BuscarDatosNodo');

		$row = mysql_fetch_array($qry);
		$this->_datosNodo['rowdatos'] = $row;
  }
	// *************************************************************************************************

	// *************************** mostrarNodo *********************************************************
	function mostrarNodo()
	 {
	  global $paso_images_gif;

    /*  ************************ Estrategia de construcci�n de interfaz **********************
    -----------------------------
    ini td contenedor nodo ra�z (se imprime desde afuera del m�todo) *
    ini tabla nodo
    ini tr nodo
      td nombre nodo y controles
    fin tr nodo
    ini tr hijos nodo
    -----------------------------------
    td espacio | ini td hijos nodo
    	   | ini tabla hijos nodo
               |  ini tr hijo 1 | td contenedor hijo 1 * | fin tr hijo 1
               |  ini tr hijo 2 | td contenedor hijo 2 * | fin tr hijo 2
               |  ini tr hijo 3 | td contenedor hijo 3 * | fin tr hijo 3
               |  ini tr hijo 4 | td contenedor hijo 4 * | fin tr hijo 4
               |  ini tr hijo 5 | td contenedor hijo 5 * | fin tr hijo 5
    	   | fin tabla hijos nodo
    -----------------------------------
      fin td hijos nodo
    fin tr hijos nodo
    fin tabla nodo
    fin td contenedor nodo ra�z (se imprime desde afuera del m�todo)
    -----------------------------
        ********************** FIN Estrategia de construcci�n de interfaz ********************
    */

	 	$ancho_control= "width='15'";
		$eventoDrag  = " onmousedown=\"inicioArrastre(event,'" . $this->_id . "','" . $this->crearIdVisual($this->_id, "_cont") . "');\"";
		$eventoDrag .= " onmouseup=\"finArrastre('" . $this->arbolId . "','" . $this->_id . "','" . $this->crearIdVisual($this->_id, "_cont") . "','" . $this->manejador->prefIdVisual . "');\"";

	 	switch ($this->_datosNodo["tipo_nodo"])
	 	 {
	 		case 'hoja':
			 	$control_rama="<img id='" . $this->crearIdVisual($this->_id, "_ctrl") . "' $ancho_control border=0 src='$paso_images_gif/arbol_nada.gif'>";	//El control mostrado a la izquierda del nodo.
			 	$control_rama.="<a onClick=\"abrirNodo('" . $this->arbolId . "','" . $this->crearIdVisual($this->_id, "_cont") . "','" . $this->_id . "','" . $this->manejador->prefIdVisual . "'); return false;\"><img id='" . $this->crearIdVisual($this->_id, "_cons") . "' $ancho_control border=0 src='$paso_images_gif/arbol_desconocido.gif'></a>";	//El control mostrado a la izquierda del nodo.
	  	 	$icono_rama="<img id='" . $this->crearIdVisual($this->_id, "_icon") . "' $ancho_control border='0' $eventoDrag src='$paso_images_gif/hoja.gif'>";		//El control mostrado a la izquierda del nodo.

	 			break;
	 		case 'carpeta':
			 	$control_rama="<a onmousedown=\"javascript:visibilidad('" .  $this->crearIdVisual($this->_id, "_trh") . "','" . $this->crearIdVisual($this->_id, "_ctrl") . "','" . $this->crearIdVisual($this->_id, "_icon") . "');\"><img id='" . $this->crearIdVisual($this->_id, "_ctrl") . "' $ancho_control border=0 src='$paso_images_gif/arbol_menos.gif'></a>";	//El control mostrado a la izquierda del nodo.
			 	$control_rama.="<a onClick=\"abrirNodo('" . $this->arbolId . "','" . $this->crearIdVisual($this->_id, "_cont") . "','" . $this->_id . "','" . $this->manejador->prefIdVisual . "'); return false;\"><img id='" . $this->crearIdVisual($this->_id, "_cons") . "' $ancho_control border=0 src='$paso_images_gif/arbol_desconocido.gif'></a>";	//El control mostrado a la izquierda del nodo.
			 	$icono_rama="<img id='" . $this->crearIdVisual($this->_id, "_icon") . "' $ancho_control border=0 $eventoDrag src='$paso_images_gif/carpeta_a.gif'>";		//El control mostrado a la izquierda del nodo.

	 			break;

	 		default:

			 	$control_rama="<a onClick=\"abrirNodo('" . $this->arbolId . "','" . $this->crearIdVisual($this->_id, "_cont") . "','" . $this->_id . "','" . $this->manejador->prefIdVisual . "'); return false;\"><img id='" . $this->crearIdVisual($this->_id, "_cons") . "' $ancho_control border=0 src='$paso_images_gif/arbol_desconocido.gif'></a>";	//El control mostrado a la izquierda del nodo.
			 	$icono_rama="<img id='" . $this->crearIdVisual($this->_id, "_icon") . "' $ancho_control border=0 $eventoDrag src='$paso_images_gif/carpeta_c.gif'>";		//El control mostrado a la izquierda del nodo.
	 			break;
	 	 }

	 	//Construir lo que se mostrar� en el nodo.
	 	$visual = $this->construirVisualizacion();

    echo "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
	  echo "<tr id='" . $this->crearIdVisual($this->_id) . "'>\n";    											//Inicia tr nodo.
	  echo "<td align=\"center\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tr><td>$control_rama</td><td>$icono_rama</td></tr></table></td>\n";									//Td del signo + o -.
	  echo "<td >" . $visual . "</td>\n";	    //Td que muestra el valor del nodo.
	  echo "</tr>\n";												   												//Fin del tr nodo.
	  echo "<tr id='" . $this->crearIdVisual($this->_id, "_trh") . "' style=\"visibility:visible\">\n";  											//Inicia tr hijos nodo (generalmente vac�o).
	  echo "<td ></td>\n";								  						//Td abajo de la del signo + o -. Va vac�o.
	  echo "<td id='" . $this->crearIdVisual($this->_id, "_tdh") . "' >\n";				//Td para los hijos.

	  if(sizeof($this->_hijos)>0)
      echo "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";				  //Tabla para los hijos.
  	for($i=0;$i<sizeof($this->_hijos);$i++)
  	 {
		  echo "<tr>\n";    																						//Tr de cada hijo.
		  echo "<td id='" . $this->crearIdVisual($this->_hijos[$i]->_id, "_cont") . "'><!ini !> \n";				//Td contenedor de cada hijo.
  	 	$this->_hijos[$i]->mostrarNodo();
		  echo "</td><!fin !>\n";								    														//Fin Td contenedor de cada hijo.
		  echo "</tr>\n";								    														//Fin Tr de cada hijo.
  	 }
	  if(sizeof($this->_hijos)>0)
      echo "</table>\n";																							//Fin Tabla para los hijos.

    echo "</td>\n";								    //Fin td para los hijos.
	  echo "</tr>\n";								    //Fin tr hijos nodo (generalmente vac�o).
    echo "</table>\n";								//Fin tabla del nodo.
	 }
	// *************************************************************************************************

	// *************************** buscarNodoPorId *****************************************************
	function buscarNodoPorId($id_nodo_a_buscar)
	 {
  	for($i=0;$i<sizeof($this->_hijos);$i++)
  	 {
      //echo"hijo $i: ". $this->_hijos[$i]->_id . " - buscado: " . $id_nodo_a_buscar . "<br />";
      if($this->_hijos[$i]->_id==$id_nodo_a_buscar)
        return true;
      else
       {
        $encontrado = $this->_hijos[$i]->buscarNodoPorId($id_nodo_a_buscar);
        if($encontrado==true)
         {
          return true;
         }
       }
     }
    return false;
   }
	// *************************************************************************************************

	// *************************** crearIdVisual *******************************************************
	function crearIdVisual($parId,$par_sufijo='')
  {
  	return $this->manejador->prefIdVisual . '_' . $parId . $par_sufijo;
  }
	// *************************************************************************************************

	// *************************************************************************************************
	// *************************************************************************************************
	// *************************** construirVisualizacion **********************************************
  // ****** Este m�todo puede ser heredado para luego mostrar lo que se necesite en cada caso ******
  function construirVisualizacion()
  {

  	$visual = " <b>" . $this->_id  . "</b> " .
 							" (" . $this->_datosNodo["cant_hijos"] . ") " .
 							$this->_datosNodo["rowdatos"][apellido] . " " .
 							$this->_datosNodo["rowdatos"][nombres];
 		return $visual;
  }
	// *************************************************************************************************

	// *************************** construirConsultaHijos **********************************************
	// ****** Este m�todo puede ser heredado para luego mostrar lo que se necesite en cada caso ******
	function construirConsultaHijos()
  {
	 	$query="SELECT au.cod_us, u.apellido, u.nombres
	 					FROM arboles_usuarios_avz as au
	 					  LEFT JOIN usuario AS u ON au.cod_us=u.cod_us
	          WHERE au.cod_us_padre = '" . $this->_id . "'
	            AND au.arbol_id='" . $this->arbolId . "'
	         ";
 		return $query;
  }
	// *************************************************************************************************

	// *************************** construirConsultaRaiz ***********************************************
	// ****** Este m�todo puede ser heredado para luego mostrar lo que se necesite en cada caso ******
	function construirConsultaRaiz()
  {
	 	$query="SELECT au.cod_us, u.apellido, u.nombres
	 					FROM arboles_usuarios_avz as au
	 					  LEFT JOIN usuario AS u ON au.cod_us=u.cod_us
	          WHERE au.cod_us = '" . $this->_id . "'
	            AND au.arbol_id='" . $this->arbolId . "'
	         ";
 		return $query;
  }
	// *************************************************************************************************

 }
// *************************************************** FIN CLASE Nodo ************************
// *******************************************************************************************


// **************************************************************************************************
// ***************************************************** CLASE ArbolDatos ***************************
//Clase abstracta.
class ArbolDatos
 {
  var $arbolId;			      	//�rbol que puede ser 1 para geneal�gico, 2 para log�stico, etc.
  var $estructura;					//�rbol a gestionar.
  var $respuesta;					  //Buffer para las respuestas de error.

	// ************************************ CONSTRUCTOR  **********************************************
  function ArbolDatos($parArbolId='')
   {
   	$this->arbolId = $parArbolId;
	 }

	 // ************* Verifica si hubo errores sql **************
  function verificarSQL($qrystr,$metodo='')
   {
    $err = mysql_error();
    if($err<>'')
     {
      echo "Error en clase ArbolDatos -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
     }
   }
  // ************************************************


  // ************************************ InsertarNodo *********************************
  function InsertarNodo()
   {
   }
  // ***********************************************************************************

  // ********************************* EliminarNodo ************************************
  function EliminarNodo()
   {
   }
  // ***********************************************************************************

  // ************************************* PodarEInjertar ******************************
  function PodarEInjertar($parNodoId,$parNuevoPadre)
   {
		global $c_database;
		global $link;
		global $usuario;

   	//Llamar a la funci�n de validaci�n general para verificar que se pueda hacer la operaci�n.
    $this->PodarEInjertarValidacionesPrevias($parNodoId,$parNuevoPadre);

   	//Verificar que el nuevo padre no sea descendiente del nodo actual.
   	$null = null;
		$this->estructura=new Nodo($parNodoId,$null,$this->arbolId,$null);
		$this->estructura->BuscarDescendencia(0);//Busca hasta el final.
    $encontrado = false;
    $encontrado = $this->estructura->buscarNodoPorId($parNuevoPadre);
    if($encontrado==true)
     {
      $this->respuesta = "<b>ERROR: El Nodo $parNuevoPadre no puede ser padre de $parNodoId porque $parNuevoPadre es un descendiente de$parNodoId. No se realizaron cambios.</b>";
      return false;
     }

   	//Realizar PodarEInjertar.
   	//Buscar padre anterior para la auditor�a.
   	$qrystr = "SELECT cod_us_padre FROM  arboles_usuarios_avz WHERE cod_us='$parNodoId' AND arbol_id='" . $this->arbolId . "'";
		$qry = mysql_db_query($c_database,$qrystr,$link);
	  $this->verificarSQL($qrystr,'PodarEInjertar');
		$row = mysql_fetch_array($qry);
    $anteriorPadre = $row[0];

   	$qrystr = "REPLACE INTO arboles_usuarios_avz (arbol_id,cod_us,cod_us_padre) VALUES ('$this->arbolId','$parNodoId','$parNuevoPadre')";
		$qry = mysql_db_query($c_database,$qrystr,$link);
	  $this->verificarSQL($qrystr,'PodarEInjertar');

	  //Auditar cambio.
		auditar($usuario,'CARRERA DE CRECIMIENTO','CAMBIO PADRE',"�rbol: $this->arbolId, usu: $parNodoId,Anterior Padre: $anteriorPadre,NuevoPadre: $parNuevoPadre");

		//Realizar actividades posteriores.
    $this->PodarEInjertarAccionesPosteriores($parNodoId,$parNuevoPadre);

    //Todo fue bien=> retornar true.
    return true;
   }
  // ***********************************************************************************

  // ************* M�todo abstracto a ser implementado por la clase hija ***************
  function PodarEInjertarValidacionesPrevias($parNodoId,$parNuevoPadre)
  {
  }
  // ***********************************************************************************

  // ************* M�todo abstracto a ser implementado por la clase hija ***************
  function PodarEInjertarAccionesPosteriores($parNodoId,$parNuevoPadre)
  {
  }
  // ***********************************************************************************
}
// *************************************************** FIN CLASE ArbolDatos *************************
// **************************************************************************************************


// **************************************************************************************************
// ************************************************** CLASE ArbolInterfaz ***************************
//Clase abstracta.
class ArbolInterfaz
 {
  var $arbolId;							//El id correspondiente a la tabla.
  var $prefIdVisual;				//Un id rand�mico  de prefijo para evitar que se pisen ids visuales en el con JS.
  var $estructura;					//�rbol a gestionar.
  var $pasoAbrirNodo;				//Paso para llamar al m�todo que abre un hijo.
  var $pasoPodar;						//Paso para llamar al m�todo que poda e injerta.
  var $profundidad;					//Profundidad m�xima a mostrar. Si es 0 no hay l�mite.
  var $imgEsperando;
  var $imgControlMenos;
  var $imgControlMas;
  var $imgControlNada;
  var $imgControlDesconocido;
  var $imgIconoAbierto;
  var $imgIconoCerrado;
  var $imgIconoHoja;

	/**
	* default constructor
	*/
  function ArbolInterfaz($pararbolId=1,$parNodoId,$parPrefIdVisual,$parProfundidad=2)
   {
   	global $paso_images_gif;

    $this->arbolId = $pararbolId;
    if($parPrefIdVisual<>'')
      $this->prefIdVisual = $parPrefIdVisual;
    else
      $this->prefIdVisual  = $this->crearPrefIdVisual();
    $this->profundidad=$parProfundidad;
    $this->imgEsperando = "http://www.iddelsur.com/googlemaps1/cargando.gif";
    $this->imgControlMenos = "$paso_images_gif/arbol_menos.gif";
    $this->imgControlMas = "$paso_images_gif/arbol_mas.gif";
    $this->imgControlNada = "$paso_images_gif/arbol_nada.gif";
    $this->imgControlDesconocido = "$paso_images_gif/arbol_desconocido.gif";
    $this->imgIconoAbierto = "$paso_images_gif/carpeta_a.gif";
    $this->imgIconoCerrado = "$paso_images_gif/carpeta_c.gif";
    $this->imgIconoHoja = "$paso_images_gif/arbol_hoja.gif";

   	if($parNodoId<>'')
		  $this->crearEstructura($parNodoId);
	 }

  // ************* Verifica si hubo errores sql **************
  function verificarSQL($qrystr,$metodo='')
   {
    $err = mysql_error();
    if($err<>'')
     {
      echo "Error en clase ArbolInterfaz -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
     }
   }
  // ************************************************

  function crearEstructura($parNodoId)
   {
   	$null = null;
		$this->estructura=new Nodo($parNodoId,$null,$this->arbolId,&$this);
		$this->estructura->BuscarRowDatosNodo();			//El nodo ra�z Busca los datos propios ya que no tuvo un Padre que lo haga.
		$this->estructura->BuscarDescendencia($this->profundidad);
   }

   function mostrarArbol($modo='completo')
    {
   	 //Seg�n el modo en que debe mostrarse, agregar funciones javascript.
     switch ($modo)
   	 	{
   	 	 case 'completo':
	   	   $this->ponerFuncVisibilidad();
	   	   $this->ponerFuncAjax();
	   	 	 $this->ponerFuncDragDrop();
	   	 	 $this->ponerPregunta();

		     echo "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
			   echo "<tr>\n";
			   echo "<td id='" . $this->prefIdVisual . '_' . $this->estructura->_id . "_cont'><!ini !> \n";
	  	   $this->estructura->mostrarNodo();
			   echo "</td><!fin !>\n";
			   echo "</tr>\n";
			   echo "</table>\n";

       break;
   	 	 case 'resumido':
				 $this->estructura->mostrarNodo();
   	 	 break;

   	 	 default:
   	 	 break;
   	 	}


    }

	function crearPrefIdVisual()
	{
		 global $_prefijos_creados;
		 if(!is_array($_prefijos_creados))
		   $_prefijos_creados=array();
		 do
		 {
		   $letras = "abcdefghlmopqwrz";
		   srand((double)microtime()*1000000);
		   $clave = strtoupper (substr($letras,rand(0,6),1));
		   $clave .= substr($letras,rand(0,6),1);
		   $clave .= strtoupper (substr($letras,rand(0,6),1));
		   $clave .= substr($letras,rand(0,6),1);
		 }
		 while (in_array($clave,$_prefijos_creados));
		 $_prefijos_creados[]=$clave;
	   return $clave;
	}

  // ************************************************************************************************************
  // *********************************** FUNCIONES JS ***********************************************************
  // ************************************************************************************************************


  // ********************************************************************
  // ********************* ponerFuncVisibilidad ***********************
   function ponerFuncVisibilidad()
   {
?>

<script type="text/javascript">

function visibilidad(id_nodo,id_control,id_icono)
{

  // ocultamos o mostramos segun la variable valor
  if(document.getElementById(id_nodo).style.visibility!="visible")
    {
     //alert("Paso a visible");
     document.getElementById(id_control).src="<?php echo $this->imgControlMenos; ?>";
     document.getElementById(id_icono).src="<?php echo $this->imgIconoAbierto; ?>";
     document.getElementById(id_nodo).style.visibility="visible";
     document.getElementById(id_nodo).style.display="";
    }
  else
    {
     //alert("Paso a invisible");
     document.getElementById(id_control).src="<?php echo $this->imgControlMas; ?>";
     document.getElementById(id_icono).src="<?php echo $this->imgIconoCerrado; ?>";
     document.getElementById(id_nodo).style.visibility="hidden";
     document.getElementById(id_nodo).style.display="none";
    }
}
</script>
<?php
   }

  // **************************************************************
  // *********************  ponerFuncAjax ***********************
  function ponerFuncAjax()
   {
?>
<script type='text/javascript' src='ajax.js'></script>

<script type="text/javascript">
var ajax = new sack();
function whenLoading()
 {
	var e = document.getElementById(ajax.element);
	e.innerHTML = "<img border=0 src='<?php echo $this->imgEsperando; ?>'><p>Enviando Datos...</p>";
 }

function whenLoaded()
 {
	var e = document.getElementById(ajax.element);
	e.innerHTML = "<img border=0 src='<?php echo $this->imgEsperando; ?>'><p>Datos Enviados...</p>";
 }

function whenInteractive()
 {
	var e = document.getElementById(ajax.element);
	e.innerHTML = "<img border=0 src='<?php echo $this->imgEsperando; ?>'><p>Obteniendo Respuesta...</p>";
 }

function whenCompletedAbrirNodo()
 {
	var e = document.getElementById('sackdata');
	if (ajax.responseStatus)
	 {
		var string = "<p>Status Code: " + ajax.responseStatus[0] + "</p><p>Status Message: " + ajax.responseStatus[1] + "</p><p>URLString Sent: " + ajax.URLString + "</p>";
		//var string = "<p>response: " + ajax.response + "</p>";
	 }
	else
	 {
		var string = "<p>URLString Sent: " + ajax.URLString + "</p>";
	 }
	e.innerHTML = string;
 }

// *************** PARA EL ABRIR_NODO
function abrirNodo(arbol_id,id_contenedor,id_nodo,pref_id_visual)//El contenedor es el <TD> que contiene al nodo que se debe expandir. El id_nodo es el identificador del nodo que se debe expandir.
 {
	ajax.requestFile = "<?php echo $this->pasoAbrirNodo; ?>&id_nodo="+id_nodo+"&arbol_id="+arbol_id+"&pref_id_visual="+pref_id_visual;  //La URL llamada. Podr�a enviarse por par�metro.
	ajax.method = 'POST';
	ajax.element = id_contenedor;            //El id del elemento que cambia.
	ajax.onLoading = whenLoading;
	ajax.onLoaded = whenLoaded;
	ajax.onInteractive = whenInteractive;
	ajax.onCompletion = whenCompletedAbrirNodo;
	ajax.runAJAX("passedvar=somedata");
 }
// *************** FIN  PARA EL ABRIR_NODO

// *************** PARA EL PODAR_E_INJERTAR
//function whenCompletedPodarNodo(id_nodo_visual,id_nuevo_padre,id_nuevo_padre_visual)
function whenCompletedPodarNodo()
 {
	var e = document.getElementById('mensajero');
	if (ajax.responseStatus)	//Si no hubo error de comunicaci�n viene por ac�.
	 {
		var respuesta = ajax.response;

		//Si hubo un error, mostrarlo en el mensajero.
    if(respuesta.toString().substring(0, 12) != "**STATUSOK**")
     {
			var cadena = respuesta;
	  	e.innerHTML = cadena;
     }
    else //Si no hubo error, eliminar el nodo visual y llamar a un refresco del nuevo padre.
     {
    	//alert("entr�");
    	e.innerHTML = '';
    	//Quitar el comienzo que es **STATUSOK**
      var cadena = respuesta.substring(12, respuesta.length);

      //Buscar las variables necesarias en el string de resuesta
      //Viene as�:
      //arbol_id=$arbol_id&id_nodo_visual=$id_nodo_visual&id_nuevo_padre=$id_nuevo_padre&id_nuevo_padre_visual=$id_nuevo_padre_visual
      var variables = cadena.split("&");

      for (i=0; i<variables.length; i++)
        eval("var " + variables[i].split("=")[0] + "='" + variables[i].split("=")[1] + "';");

    	//eliminarElementoHTML(id_nodo_visual);
    	eliminarElementoHTML(id_nodo_visual);

      //alert("arbol_id " + arbol_id + " id_nuevo_padre_visual " + id_nuevo_padre_visual + " id_nuevo_padre " + id_nuevo_padre);

    	abrirNodo(arbol_id,id_nuevo_padre_visual,id_nuevo_padre,pref_id_visual);

      //DESCOMENTAR ESTO PARA HACER DEBUG.
      //       var e1 = document.getElementById('sackdata');
      // 		  var string = "<p>URLString Sent: " + ajax.URLString + "</p>";
      //     	e1.innerHTML = string;

		 }
	 }
	else											//Si hubo error de comunicaci�n entra por ac�.
	 {
		var cadena = "<p>URLString Sent: " + ajax.URLString + "</p>";
		var cadena = "<p>Response: " + ajax.response+ "</p>";
  	e.innerHTML = cadena;
	 }
 }

 function PodarNodo(arbol_id,id_nodo,id_nodo_visual,id_nuevo_padre,id_nuevo_padre_visual,pref_id_visual)//El contenedor es el <TD> que contiene al nodo que se debe expandir. El id_nodo es el identificador del nodo que se debe expandir.
 {
  $str_conexion = "<?php echo $this->pasoPodar; ?>&id_nodo="+id_nodo+"&id_nuevo_padre="+id_nuevo_padre+"&id_nodo_visual="+id_nodo_visual+"&id_nuevo_padre_visual="+id_nuevo_padre_visual+"&arbol_id="+arbol_id+"&pref_id_visual="+pref_id_visual;
	ajax.requestFile = $str_conexion;  //La URL llamada. Podr�a enviarse por par�metro.
	ajax.method = 'POST';
	ajax.element = 'mensajero';            //El id del elemento que cambia.
	ajax.onLoading = whenLoading;
	ajax.onLoaded = whenLoaded;
	ajax.onInteractive = whenInteractive;
	ajax.onCompletion = whenCompletedPodarNodo;
	ajax.runAJAX("passedvar=somedata");

 }
// *************** FIN PARA EL PODAR_E_INJERTAR
</script>

<div id="sackdata" style="width:100%;display:none"></div>
<div id="mensajero" style="left:100;top:0;position:absolute;width: 100%;"></div>


<?php
   }

  // *******************************************************************
  // *********************   ponerFuncDragDrop ***********************
  function ponerFuncDragDrop()
   {
?>

<script type="text/javascript">
var elemento_arrastrado="";
var elemento_arrastrado_visual="";
function inicioArrastre(event,id,id_visual)
 {
	var e = document.getElementById('divMovible');
	var e1 = document.getElementById(id_visual);
	e.style.left =event.clientX+1;
	e.style.top =event.clientY+1;
	e.innerHTML = e1.innerHTML ;

	elemento_arrastrado = id;
	elemento_arrastrado_visual = id_visual;
	dragStart(event, 'divMovible');
 }

function finArrastre(arbol_id,id_nuevo_padre,id_nuevo_padre_visual,pref_id_visual)
 {
 	if(elemento_arrastrado!='')
 	  if(elemento_arrastrado != id_nuevo_padre)
 	 	  if(preguntar("�Desea Pasar " + elemento_arrastrado + " bajo el padre " + id_nuevo_padre + "?")==true)
 	 	   {
 	 	   	PodarNodo(arbol_id,elemento_arrastrado,elemento_arrastrado_visual,id_nuevo_padre,id_nuevo_padre_visual,pref_id_visual);
 	 	   }
 	 	  //else
 	 	  //  alert('No se Realizaron Cambios');
 }
function eliminarElementoHTML(id)
 {
	//Eliminar el nodo contenedor del nodo eliminado.
	var ancla = document.getElementById(id);
	var padre = ancla.parentNode;
	var hijoRemovido = padre.removeChild(ancla);
	//alert('ACEPTADO');
 }
</script>


<script type="text/javascript" src="drag_drop.js"></script>

</script>

<?php
    echo"<DIV id=divMovible style=\"POSITION: absolute;LEFT: 0px; TOP: 0px;-moz-opacity:0.70;filter:alpha(opacity=70);\"></DIV>";

   }
  // *******************************************************************

  // *******************************************************************
  // *********************** ponerPregunta ***********************
  function ponerPregunta()
   {
?>

<script language="javascript">
function preguntar(texto)
{
 if(!confirm(texto))
  {
  	return false;
  }
 else
 {
  return true;
 	//location.href = direccion;
 }
}
</script>

<?php

   }
  // *******************************************************************

  // ************************************************************************************************************
  // *********************************** FIN FUNCIONES JS *******************************************************
  // ************************************************************************************************************

 }
// ************************************************ FIN CLASE ArbolInterfaz *******************
// **************************************************************************************************

?>
