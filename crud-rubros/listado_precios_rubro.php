<?php 
	$direc = "../";
	include($direc . 'clases/principal.php');
	include($direc . 'clases/consultas.php');
	$principal = new Principal();
	$principal->conex();
	$principal->cabecera($direc,"Listado de Rubros");
	$nueva_consulta = new Consultas();
	$query = $nueva_consulta->precios_por_rubro();
	$query1 = $nueva_consulta->validar_pedido_camp_actual();
	$band = 0;
	if($principal->total_filas($query1) > 0){
		$band = 1;
	}
?>
	<form id="form1" name="form1" method="post">
		<input type = "hidden" name="opcion" id="opcion">
		<div class="container">
			<div align="center"><p><h4>Listado de Precios por Rubros</h4></p></center>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-2"></div>
						<div class="col-xs-8">
							<table class="table-striped table-bordered table-condensed table-hover display" align="center" cellpadding="0" cellspacing="0" id="example">
								<thead>
									<tr class="odd gradeU">
										<td align="center" width="120px">Rubro Alfa</td>
										<td align="center" width="400px">Descripci&oacute;n</td>
										<td align="center" width="100px">Precio</td>
										<td width="20px"></td>
										<td width="20px"></td>
									</tr>
								</thead>
								<input type="hidden" name="actualizar" id="actualizar">
								<input type="hidden" name="eliminar" id="eliminar">
								<tbody class="upper">
									<?php
									$i = 0;
									while($row = $principal->extraer($query)){
									?>
									<tr class="odd gradeU">
										<td><?php echo $row->rubro_alfa;?></td>
										<td><?php echo $row->descrip_rubro;?></td>
										<td class="text-right"><?php echo $row->precio_gral;?></td>
										<td><input type="image" name="actualizar<?php echo $i;?>" id="actualizar<?php echo $i;?>" src="<?php echo $direc;?>b_edit.PNG" value="<?php echo $row->id;?>" onclick="redireccion(this,'actualizar')"></td>
										<td><input type="image" name="eliminar<?php echo $i;?>" id="eliminar<?php echo $i;?>" src="<?php echo $direc;?>b_drop.PNG" value="<?php echo $row->id;?>" onclick="redireccion(this,'eliminar')"></td>
									</tr>
									<?php
										if($band == 0){
											$hab_actualizar = "#actualizar" . $i;
											echo "<script>$('$hab_actualizar').show()</script>";
											$hab_eliminar = "#eliminar" . $i;
											echo "<script>$('$hab_eliminar').show()</script>";
										}
										$i++;
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th><input type="text" id="search_rubro" name="search_rubro" value="Rubro Alfa" class="search_init" /></th>
										<th><input type="text" id="search_descrip" name="search_descrip" value="Descripci&oacute;n" class="search_init" /></th>
										<th colspan="3"><input type="text" id="search_precio" name="search_precio" value="Precio" class="search_init" /></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		</br>
		<div class="row-fluid">
			<div class="span12">
				<div align="center"><input type="submit" name="enviar" id="enviar" class="btn btn-primary" Value="Agregar" onclick="redireccion(this,'agregar')">&nbsp;&nbsp;<input type="button" class="btn" Value="Salir" onclick="location.href='<?php echo $direc;?>administracion.php'">
				</div>
			</div>
		</div>
	</form>
	<?php
	$principal->cerrar_conexion();
	$principal->footer();
	?>
	<script> 
		function redireccion(valor, opc){
			$("#opcion").val(opc)
			if (opc == "agregar"){
				valor.form.action='precio_rubro.php'
			}
			if (opc == "actualizar"){
				$("#actualizar").val(valor.value)
				valor.form.action='precio_rubro.php'
			}
			if (opc == "eliminar"){
				$("#eliminar").val(valor.value)
				valor.form.action='eliminar_precio_rubro.php'
			}
		}
	</script>