<?php
$direc = "../";
include($direc . 'clases/principal.php');
include($direc . 'clases/consultas.php');
include($direc . 'clases/ventan_emerg.php');
$principal = new Principal();
$nueva_consulta = new Consultas();
$principal->conex();
$principal->cabecera($direc,ucwords("eliminar información"));
$ventana = new Ventan_Emerg();
$eliminar = $_POST["eliminar"];
$ventana->marco();
echo "<script>$('#marco').show()</script>";
$nueva_consulta->eliminar_precios($eliminar);
if ($principal->filas_afectadas() == -1){
	$ventana->ventana_error(ucwords("no se ha eliminado ning&uacuten registro"));
	echo "<script>$('#ventana_error').show()</script>";
}
else{
	$ventana->ventana_success(ucwords("registro eliminado con &eacute;xito"));
	echo "<script>$('#ventana_success').show()</script>";
}
$principal->cerrar_conexion();
$principal->footer();
?>
<script>
	$(function(){
		$("#success").click(function(){
			location.href='listado_precios_rubro.php'
		})
		
		$("#error").click(function(){
			location.href='listado_precios_rubro.php'
		})
	})
</script>