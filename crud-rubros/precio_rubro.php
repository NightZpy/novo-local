<?php
$direc = "../";
include($direc . 'clases/principal.php');
include($direc . 'clases/consultas.php');
include($direc . 'clases/ventan_emerg.php');
$principal = new Principal();
$principal->conex();
$actualizar = "";
$opcion = $_POST["opcion"];
$rubro="";
$descrip="";
$precio="";
$nueva_ventana = new Ventan_Emerg();
$nueva_ventana->marco();
$nueva_ventana->ventana_info("");
if($opcion == "actualizar"){
	$actualizar = $_POST["actualizar"];
	$principal->cabecera($direc,"Actualizar Rubro");
	$nueva_consulta = new Consultas();
	$query = $nueva_consulta->precios_por_rubro_especifico($actualizar);
	$row = $principal->extraer($query);
	$rubro=$row->rubro_alfa;
	$descrip=$row->descrip_rubro;
	$precio=$row->precio_gral;
}
else{
	$principal->cabecera($direc,"Agregar Rubro");
}
?>
	<form id="forma1" name="forma1" method="post" enctype="multipart/form-data" action="guardar_precio_rubro.php">
	<div class="container">
		<div align="center"><p><h4>Gestor de Precios por Rubros</h4></p></center>
		<input type="hidden" name="opcion" id="opcion" value="<?php echo $opcion;?>">
		<input type="hidden" name="actualizar" id="actualizar" value="<?php echo $actualizar;?>">
		<table align="center" border="0">
			<tr>
				<th width="100px">Rubro Alfa</th>
				<td width="200px"><input type="text" style="text-transform:uppercase;" id="rubro" name="rubro" minlength="3" maxlength="3" value="<?php echo $rubro;?>"></td>
				<script>$("#rubro").focus()</script>
			</tr>
			<tr>
				<th width="100px">&nbsp;</th>
				<td  width="200px">&nbsp;</td>
			</tr>
			<tr>
				<th width="100px">Descripci&oacute;n</th>
				<td  width="200px"><input type="text" id="descrip" name="descrip" value="<?php echo $descrip;?>"></td>
			</tr>
			<tr>
				<th width="100px">&nbsp;</th>
				<td  width="200px">&nbsp;</td>
			</tr>
			<tr>
				<th  width="100px">Precio</th>
				<td  width="200px"><input type="text" id="precio" name="precio" value="<?php echo $precio;?>"></td>
			</tr>
			<tr>
				<th  width="100px">&nbsp;</th>
				<td  width="200px">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="siguiente" id="siguiente" class="btn btn-primary btn-medium" value="Guardar">&nbsp;&nbsp;<input type="button" class="btn btn-medium" name="cancelar" id="cancelar" Value="Cancelar">&nbsp;&nbsp;<input type="button" class="btn btn-medium" name="salir" id="salir" Value="Salir"></td>
			</tr>
		</table>
	</div>
	</form>
<?php
$principal->cerrar_conexion();
$principal->footer();
?>
<script>
	$.validator.setDefaults({
		submitHandler: function(){
			if($("#opcion").val() != "actualizar"){
				$.post("ajax_precio_rubros.php", { criterio : $("#rubro").val() }, function(data){
					if(data[0].opc == 1){
						$("#marco").show()
						$("#comentario").empty()
						$("#rubro").val("")
						$("#comentario").append("<p>Ya existe el rubro alfa</p>")
						$("#ventan_info_foco").val("#rubro")
						$("#ventana_info").show()
					}
					else{
						forma1.submit()
					}
				}, "json")
			}
			else{
				forma1.submit()
			}
		}
	});
	$().ready(function(){
		jQuery.validator.addMethod("letras", function(value, element) {
			return this.optional(element) || /^[a-zA-ZñÑ]+$/.test(value);
		}, "Solo letras mayúsculas");
		
		jQuery.validator.addMethod("numeros", function(value, element) {
			value=value.replace(/\,/i, '.');
			return this.optional(element) || /^(\d+)([.]\d{1,2})$/.test(value);
		}, "Solo números ejm: xxx.xx");
		
		$("#forma1").validate({
			rules: {
				rubro: {
					required: true,
					minlength: 3,
					letras: true
				},
				descrip: {
					required: true
				},
				precio: {
					required: true,
					//numeros: true
					number: true
				}
			},
			messages: {
				rubro: {
					required: "Ingrese el Rubro Alfa",
					minlength: "Debe tener tres caracteres"
				},
				descrip: {
					required: "Ingrese la descripción"
				},
				precio: {
					required: "Ingrese el Precio"
				}
			}
		});
		
		$('#info').click(function(){
			$("#marco").hide()
			$("#ventana_info").hide()
			$($("#ventan_info_foco").val()).focus()
		})
		
		$('#salir').click(function(){
			location.href = "listado_precios_rubro.php";
		})
		
		$('#cancelar').click(function(){
			if($("#opcion").val() == "agregar"){
				$("#rubro").val("")
				$("#precio").val("")
			}
			$("#descrip").val("")
		})
	})
</script>