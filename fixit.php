<?php
//Script para corregir el cobro de accesorios
include('conexion.php');

$id_vd_acc = 'AVA0162';
$cuota = 3;
$agosto = 1054;
$septiembre = 1055;
$octubre = 1056;

$qrystr = "SELECT cp.id_distrib, SUM(cp.cantidad) AS cantidad FROM `comp_pedidos` cp
           INNER JOIN pedido p ON p.id_pedidos = cp.id_pedido
           INNER JOIN cronograma cro ON cro.id_presentacion = p.id_presentacion
           WHERE cro.campania = $agosto AND cp.id_vd = '$id_vd_acc'
           GROUP BY cp.id_distrib";
//echo $qrystr;
$qry = mysql_db_query($c_database, $qrystr, $link);
$u1 = 0;
$u2 = 0;
$c1 = 0;
$c2 = 0;
while($result = mysql_fetch_object($qry)){
    $distrib = $result->id_distrib;
    $cantidad = $result->cantidad;
    $precio_nuevo = $cantidad*$cuota;    

    //Actualizamos el precio de la cuota del mes de Septiembre, dependiendo de la cantidad pedida en Agosto
    $qrystr1 = "SELECT cp.id_comp_ped,cp.id_pedido FROM `comp_pedidos` cp
                INNER JOIN pedido p ON p.id_pedidos = cp.id_pedido
                INNER JOIN cronograma cro ON cro.id_presentacion = p.id_presentacion
                WHERE cro.campania = $septiembre AND cp.id_vd = '$id_vd_acc' AND cp.id_vendedor = '$distrib' AND LEFT(cp.comentario,5) = 'Cuota'";
    $qry1 = mysql_db_query($c_database, $qrystr1, $link);
    if(mysql_num_rows($qry1) > 0){
        $result1 = mysql_fetch_object($qry1);
        $qrystr2 = "UPDATE `comp_pedidos` SET preciounidad = ".$precio_nuevo." WHERE id_comp_ped = ".$result1->id_comp_ped." AND id_vendedor = '$distrib' AND id_vd = '$id_vd_acc'";
        $qrystr5 = "UPDATE `comp_pedidos_defi` SET preciounidad = ".$precio_nuevo." WHERE id_comp_ped = ".$result1->id_comp_ped." AND id_vendedor = '$distrib' AND id_vd = '$id_vd_acc'";
        if($qry2 = mysql_db_query($c_database, $qrystr2, $link) && $qry5 = mysql_db_query($c_database, $qrystr5, $link)){
            //Actualizamos la cta cte
            $qrystr7 = "SELECT id_movcon,debe FROM `ctacte` WHERE id_cod_us = '$distrib' AND pedido = ".$result1->id_pedido." AND ref LIKE '%R:$distrib%'";
            $qry7 = mysql_db_query($c_database, $qrystr7, $link);
            if(mysql_num_rows($qry7) > 0){
                $result7 = mysql_fetch_object($qry7);
                $debe_nuevo = ($result7->debe - 3) + $precio_nuevo;
                $qrystr8 = "UPDATE ctacte SET debe = $debe_nuevo WHERE id_movcon = ".$result7->id_movcon;
                if($qry8 = mysql_db_query($c_database, $qrystr8, $link))
                    $c1++;
            }
            $u1++;
        }
    }

    //Actualizamos el precio de la cuota del mes de Octubre, dependiendo de la cantidad pedida en Agosto
    $qrystr3 = "SELECT cp.id_comp_ped,cp.id_pedido FROM `comp_pedidos` cp
                INNER JOIN pedido p ON p.id_pedidos = cp.id_pedido
                INNER JOIN cronograma cro ON cro.id_presentacion = p.id_presentacion
                WHERE cro.campania = $octubre AND cp.id_vd = '$id_vd_acc' AND cp.id_vendedor = '$distrib' AND LEFT(cp.comentario,5) = 'Cuota'";
    $qry3 = mysql_db_query($c_database, $qrystr3, $link);
    if(mysql_num_rows($qry3) > 0){
        $result3 = mysql_fetch_object($qry3);
        $qrystr4 = "UPDATE `comp_pedidos` SET preciounidad = ".$precio_nuevo." WHERE id_comp_ped = ".$result3->id_comp_ped." AND id_vendedor = '$distrib' AND id_vd = '$id_vd_acc'";
        $qrystr6 = "UPDATE `comp_pedidos_defi` SET preciounidad = ".$precio_nuevo." WHERE id_comp_ped = ".$result3->id_comp_ped." AND id_vendedor = '$distrib' AND id_vd = '$id_vd_acc'";
        if($qry4 = mysql_db_query($c_database, $qrystr4, $link) && $qry6 = mysql_db_query($c_database, $qrystr6, $link)){
            //Actualizamos la cta cte
            $qrystr9 = "SELECT id_movcon,debe FROM `ctacte` WHERE id_cod_us = '$distrib' AND pedido = ".$result3->id_pedido." AND ref LIKE '%R:$distrib%'";
            $qry9 = mysql_db_query($c_database, $qrystr9, $link);
            if(mysql_num_rows($qry9) > 0){
                $result9 = mysql_fetch_object($qry9);
                $debe_nuevo = ($result9->debe - 3) + $precio_nuevo;
                $qrystr10 = "UPDATE ctacte SET debe = $debe_nuevo WHERE id_movcon = ".$result9->id_movcon;
                if($qry10 = mysql_db_query($c_database, $qrystr10, $link))
                    $c2++;
            }
            $u2++;
        }
    }
}

echo "Se actualizaron $u1 registros en comp_pedidos para el mes de Septimebre.<br>";
echo "Se actualizaron $c1 registros en ctacte para el mes de Septimebre.<br>";
echo "Se actualizaron $u2 registros en comp_pedidos para el mes de Octubre.<br>";
echo "Se actualizaron $c2 registros en ctacte para el mes de Octubre.";
?>
