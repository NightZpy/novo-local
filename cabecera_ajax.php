<?php
echo"

	<style type='text/css'>

	/* START CSS NEEDED ONLY IN DEMO */
	html{height:100%;}
	body
  {
		/*background-color:#E2EBED;*/
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;	
		width:100%;
		height:100%;		
		margin:0px;
		text-align:center;
	}
	#mainContainer
  {
		/*width:660px;*/
		margin:0 auto;
		text-align:left;
		/*height:100%;*/
		/*background-color:#FFF;*/
		/*border-left:3px double #000;*/
		/*border-right:3px double #000;*/
	}
	#formContent{padding:5px;}
	/* END CSS ONLY NEEDED IN DEMO */
	/* Big box with list of options */
	#ajax_listOfOptions
  {
		position:absolute;	/* Never change this one */
		width:300px;	/* Width of box */
		height:250px;	/* Height of box */
		overflow:auto;	/* Scrolling features */
		border:1px solid #8000A1;	/* Dark green border */
		background-color:#FFFFE6;/*#FFFFC6;	/* White background color */
		text-align:left;
		font-size:1.0em;
		z-index:100;
	}
	#ajax_listOfOptions div
  {	/* General rule for both .optionDiv and .optionDivSelected */
		margin:1px;		
		padding:1px;
		cursor:pointer;
		font-size:1.0em;
	}
	#ajax_listOfOptions .optionDiv
  {	/* Div for each item in list */
		
	}
	#ajax_listOfOptions .optionDivSelected
  { /* Selected item in the list */
		background-color:#BBBB00;
		color:#8000A1;
	}
	#ajax_listOfOptions_iframe
  {
		background-color:#F00;
		position:absolute;
		z-index:5;
	}
	form
  {
		display:inline;
	}
	
	</style>
<script type='text/javascript' src='ajax.js'></script>
<script type='text/javascript' src='ajax-dynamic-list.js'></script>
";
?>
