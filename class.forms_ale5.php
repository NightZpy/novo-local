<?


class Form {
	  
	  var $name;  // Form Variable Name
	  var $title;  // Form Title
	  var $css;  // Form CSS File
	  var $action;  // Form Action
	  var $method;  // Form Method
	  var $enctype;  // Form Enctype
	  var $submit;  // Form Submit
	  var $tables;  // array of table classes to be used
	  var $fields;  // array of final form fields
	  var $ids;  // array of final form field ID's
	  var $details;  // array of final field details
	  var $equal;  // array of values that use the same form input but have different names
	  var $temp; // TEMP HTML
	  var $form; // FINAL HTML
	  var $result;  // FINAL HTML RESULT
    var $validaciones;
    var $combos;
    var $usuario;
    var $sesion;
    var $hiddensform;

	  // Form Constructor Function <
	  // ---------------------------
	  function Form ($name,$title,$action,$method,$enctype,$submit,$css,$usuario="",$sesion="")
     {
	  	if ($action == "") { $method = $PHP_SELF; }
	  	if ($method == "") { $method = "POST"; }
	  	if ($enctype == "") { $enctype = "text"; }
	  	if ($submit == "") { $submit = "submit"; }
			$this->name = $name;
			$this->title = $title;
	  	$this->css = $css;
	  	$this->action = $action;
	  	$this->method = $method;
	  	$this->enctype = $enctype;
	  	$this->submit = $submit;
			$this->tables = array();
			$this->fields = array();
			$this->ids = array();
			$this->combos = array();
			$this->hiddensform = array();
	  	$this->details = array();
	  	$this->equal = array();
	  	$this->temp = "";
	  	$this->form = "";
	  	$this->result = "";
	  	$this->usuario = $usuario;
	  	$this->sesion = $sesion;
		 }


	  // Add a new table to the form <
	  // -----------------------------
	  function addtable ($db,$name,$ignore,$passwd,$hide,$action,$where,$order,$limit,$campos="") {

	  		   array_push($this->tables,$name);
			   $GLOBALS[$name] = new MySQLTable($db,$name,$ignore,$passwd,$hide,$action,$where,$order,$limit,$this->combos,$campos);
			   }


	  // Add custom descriptions to the form fields <
	  // --------------------------------------------
	  function describe ($details) {

	  		   $this->details = explode("/:/",$details);
			   }


	  // Add custom descriptions to the form fields <
	  // --------------------------------------------
	  function equal ($equal) {

	  		   $this->equal = $equal;
			   }


	  // Build an array of form fields <
	  // -------------------------------
	  function makefields () {

			   for ($i = 0; $i < sizeof($this->tables); $i++) {
			   	   $table = $this->tables[$i];

				   $temp = $GLOBALS[$table]->fields;

                   $final = array();
				   $ids = array();
				   for ($j = 0; $j < sizeof($temp); $j++) {
					   $fieldn = $temp[$j];
					   $name = $GLOBALS[$fieldn]->varname;
					   $id = $GLOBALS[$fieldn]->ID;
					   $ieq = array_flip($this->equal);
					   if (in_array($name,$this->equal)) { $name = $ieq[$name]; }
					   array_push($ids,$id);
					   array_push($final,$name);
					   }
				   $merged1 = array_merge($this->fields,$final);
				   $merged2 = array_merge($this->ids,$ids);
				   $this->fields = $merged1;
				   $this->ids = $merged2;
				   }
			   $this->fields = array_unique($merged1);
			   $this->ids = array_unique($merged2);
			   }

			   
	  // Build the final form html code <
	  // --------------------------------
	  function build ($c1width,$c2width,$c3width,$rows,$cols) {
			   $width = $c1width + $c2width + $c3width;
			   
			   $this->form .= "<center><table class=\"formtable\" width=\"$width\">\n<tr>\n";
			   $this->form .= "<th align=center colspan=3><h3>" . $this->title . "</h3>\n";
			   $this->form .= "<form name=\"ff".$this->name."\" method=\"$this->method\" action=\"$this->action\" >\n";
			   $this->form .= "</th>\n</tr>\n";
//			    echo "combos: ".implode(",",$this->combos)."<br>";
			   $total = count($this->fields);
	  		  for ($i = 0; $i < $total; $i++)
           {
			   	  $temp = $this->fields[$i];
			   	  if ($temp == "")
             {
              $total++;
             }
				    else
             {
						  $tmpid = $this->ids[$i];
//						  echo "tmpid: $tmpid<br>";
					    $nomcampo = $GLOBALS["$tmpid"]->name;
  				    if ($this->details[$i] == "") { $detailz = $tmpid . " - " . $temp; }
					    else { $detailz = $this->details[$i]; }
						  if(in_array($nomcampo,$this->combos)) //Si el campo es un Combo, mostrar el combo.
				   	   {
//				   	  	echo "encontrado $nomcampo--".var_dump($GLOBALS);
                $this->form .= $GLOBALS["cmb$nomcampo"]->build($detailz,$c1width,$c2width,$c3width,$rows,$cols,$GLOBALS["$tmpid"]->value);
               }
              else                               //No es un combo, mostrar normalmente.
               {
				   	    if ($GLOBALS[$tmpid]->hidden == "0")
                 {
				   	  	  $this->form .= $GLOBALS[$tmpid]->build($detailz,$c1width,$c2width,$c3width,$rows,$cols);
					  	   }
						    else
                 {
							    $this->temp .= $GLOBALS[$tmpid]->build($detailz,$c1width,$c2width,$c3width,$rows,$cols);
							   }
               }
				   	 }	
				   }//Del for ($i = 0; $i < $total; $i++)

			   $this->form .= "<tr>\n<td width=\"$c1width\" align=right></td><td width=\"$c2width\"></td>" . $this->temp . "<td width=\"$c3width\"></td>\n</tr>\n";
			   $this->form .= "<tr>\n<td align=right width=\"$c1width\"><input class=\"formsubmit\" type=button name=\"Enviar\" ";
			   $this->form .= "onclick=\"javascript: if (ValidarDatos(ff".$this->name.")) submitear(ff".$this->name.");\" ";
			   $this->form .= "value=\"Enviar\"></input></td>\n<td width=\"$c2width\"></td>";
			   $this->form .= "<td width=\"$c2width\" align=left><input type=reset value=\"Reset\"></input></td></tr>\n";
			   $this->form .= "<tr><td colspan=3></td></tr></table></center>\n";
         if($this->usuario<>"")
  			   $this->form .= "<input type='hidden' name='usuario' value='$this->usuario'></input>\n";
         if($this->sesion<>"")
  			   $this->form .= "<input type='hidden' name='sesion' value='$this->sesion'></input>\n";

	  		 //Muestra todos los HiddensForm que se hayan agregado.
         for ($i = 0; $i < sizeof($this->hiddensform) ; $i++)
          {
			   	 $temph = $this->hiddensform[$i];
  			   $this->form .= "<input type='hidden' name='$temph[0]' value='$temph[1]'></input>\n";
          }

			   //Este input es el de env�o. Al presionar el bot�n pone la variable hacerenvio='S'.
         $this->form .= "<input type='hidden' name='hacerenvio' value=''></input></form>\n";
         //El input hacerenvio es una variable que luego de inicializar el formulario se
         //consulta para saber si hay que mostrar los input o si hay que actualizar la BD.
			   $this->form .= "
         <script language='javascript'>

         function ValidarDatos(Form)
          {
           $this->validaciones
           return true;
          }

         function submitear(ff)
          {
           ff.hacerenvio.value = 'S';
           ff.submit();
          }
         </script> ";
			   return $this->form;
			   }

	  // Submit the form fields <
	  // ------------------------
	  function submit () {

			   $GLOBALS[error] = "";
			   for ($j = 0; $j < sizeof($this->tables); $j++) {
			   	   $table = $this->tables[$j];
				   if ($GLOBALS[$table]->submit($this->name)) { }
				   else { $GLOBALS[error] .= "Error in $table<br>\n"; }
				   }
			   if ($GLOBALS[error] == "") {
			   	  return True;
				  }
			   else { 
			   		return False;
					}
			   }

			   
	  // Build the final form html code <
	  // --------------------------------
	  function Result ($c1width,$c2width,$c3width) {
	  		   
			   $width = $c1width + $c2width + $c3width;
			   
			   $this->result .= "<center><table class=\"formtable\" width=\"$width\">\n<tr>\n";
			   $this->result .= "<th align=center colspan=3><h3>" . $this->title . " - Results</h3></th>\n</tr>\n";
			   
			   $total = count($this->fields);
	  		   for ($i = 0; $i < $total; $i++) {
			   	   $temp = $this->fields[$i];
			   	   if ($temp == "") { $total++; }
				   else {
				   		//if (in_array($temp,$this->select)) { }
						$tmpid = $this->ids[$i];
						if ($this->details[$i] == "") { $detailz = $tmpid . " - " . $temp; }
						else { $detailz = $this->details[$i]; }
						$this->result .= $GLOBALS[$tmpid]->Result($detailz,$c1width,$c2width,$c3width);
				   		}
				   }

			   $this->result .= "<tr><td colspan=3></td></tr>\n";
			   $this->result .= "<tr><td align=center colspan=3><i>Updated Succesfully!</i></td></tr></table></center>\n";
			   
			   return $this->result;
			   }

	  // Asigna las Validaciones que vienen como un string de javascript<
	  // --------------------------------
    function setValidaciones ($parValidaciones)
     {
      $this->validaciones = $parValidaciones;
     }

	  // Agrega Validaciones que vienen como un string de javascript<
	  // --------------------------------
    function addValidaciones ($parValidaciones)
     {
      $this->validaciones .= $parValidaciones;
     }	

	  // Add a new combo to the form <
	  // -----------------------------
	function addCombo ($db1,$nom_combo,$tabla,$campo_mostrar,$campo_clave)
     {
      array_push($this->combos,"$nom_combo");
			$GLOBALS["cmb$nom_combo"] = new MySqlCombo($db1,$nom_combo,$tabla,$campo_mostrar,$campo_clave);
	   }

	  // Add a new combo to the form <
	  // -----------------------------
    function addComboFiltro ($db1,$nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
     {
      array_push($this->combos,"$nom_combo");
			$GLOBALS["cmb$nom_combo"] = new MySqlComboFiltro($db1,$nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro);
	   }

	  // Add a new hidden independiente al form <
	  // -----------------------------
	function addHiddenForm ($nombreHidden,$valorHidden)
     {
      array_push($this->hiddensform,array($nombreHidden,$valorHidden));
	   }

	  // Devuelve el valor de un campo <
	  // -----------------------------
	function getCampo ($nombrecampo)
     {
	    for ($i = 0; $i < sizeof($this->tables); $i++)
       {
        $table = $this->tables[$i];
		    $temp = $GLOBALS[$table]->fields;
				for ($j = 0; $j < sizeof($temp); $j++)
         {
					if($GLOBALS[$temp[$j]]->varname == $nombrecampo)
           {
            return $GLOBALS[$temp[$j]]->value;
           }
				 }
       }
	   }
 }
	
//###############################################################################3
//###############################################################################3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           Clase MySQLCombo - The MySQL Form Generator   #
#           Alejandro Baz�n - 2004                        #
#           abazan@iddelsur.com.ar                        #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MySQLCombo
 {
  var $db;
  var $name;
  var $nombreHtml;
  var $tabla;
  var $campo_mostrar;	
  var $campo_clave;

  function MySQLCombo ($db,$name,$tabla,$campo_mostrar,$campo_clave)
   {
    $this->db = $db;
    $this->name = "cmb.$name";
    $this->nombreHtml = "$name";
    $this->tabla = $tabla;
    $this->campo_mostrar = $campo_mostrar;
    $this->campo_clave = $campo_clave;
   }

  function ObtenerFilas()
   {
    $db = $this->db;
    $qrystr = "select * from $this->tabla order by $this->campo_mostrar";
    $raw = $db->get($qrystr,"2");
    return $raw;
   }

  function Build ($details,$c1width,$c2width,$c3width,$rows,$cols,$seleccion)
   {
    $raw = $this->ObtenerFilas();
    $resultado = "";
    $resultado .= "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\">$details</td><td width=\"$c2width\"></td><td align=left width=\"$c3width\" class=\"formvalue\">";
    $resultado .= "<select size='1' name=" . $this->nombreHtml . " class=\"forminput\" >";
    for ($i = 0 ; $i < sizeof($raw); $i++)
     {
   	  $row = $raw[$i];
      if ($row[$this->campo_clave] == $seleccion) { $selected = " selected"; }
      else { $selected = " "; }
      $resultado .= "<option $selected value='".$row[$this->campo_clave]."'>".$row[$this->campo_mostrar]."</option>";
   	 }
    $resultado .= "</select>";
    $resultado .= "</td>\n</tr>\n";

    return $resultado ;
   }
 }	
 
//###############################################################################3
//###############################################################################3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           Clase MySQLCombo - The MySQL Form Generator   #
#           Alejandro Baz�n - 2004                        #
#           abazan@iddelsur.com.ar                        #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MySQLComboFiltro extends MySQLCombo
 {
  var $filtro;

  function MySQLComboFiltro ($db,$name,$tabla,$campo_mostrar,$campo_clave,$filtro)
   {
    parent::MySQLCombo($db,$name,$tabla,$campo_mostrar,$campo_clave);
    $this->filtro = $filtro;
   }

  function ObtenerFilas()
   {
    $db = $this->db;
    $qrystr = "select * from $this->tabla where $this->filtro order by $this->campo_mostrar";
    $raw = $db->get($qrystr,"2");
    return $raw;
   }


 }	
 
//###############################################################################3
//###############################################################################3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           MySQLTable Class - The MySQL Form Generator   #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MySQLTable {
	
	  var $db;
	  var $name;
	  var $ignore;  // array of dbfield names that should be ignored
	  var $passwd;  // array of dbfields that should be passworded
	  var $hide;  // array of dbfields that should be hidden
	  var $action;  // Action (1 for Insert, 2 for Edit)
	  var $where;  // SQL Where Clause
	  var $order;  // SQL Order Clause
	  var $limit;  // SQL Limit Clause
	  var $fields;  // array of fieldnames in table
	  var $sql;     // sql query
	  	
	  function MySQLTable ($db,$name,$ignore,$passwd,$hide,$action,$where,$order,$limit,$combos,$campos="") {
	  		
			   if ($hide == "") { $hide = array(); }
			   else {
			   		if (strlen($hide) <= "1") { $hide = array($hide); }
					else { $hide = explode(":",$hide); }
					}
			   if ($ignore == "") { $ignore = array(); }
			   else {
			   		if (strlen($ignore) <= "1") { $ignore = array($ignore); }
					else { $ignore = explode(":",$ignore); }
					}
			   if ($passwd == "") { $passwd = array(); }
			   else {
			   		if (strlen($passwd) <= "1") { $passwd = array($passwd); }
					else { $passwd = explode(":",$passwd); }
					}
			   if ($campos == "") { $campos = array(); }
			   else {
			   		if (strlen($campos) <= "1") { $campos = array($campos); }
					else { $campos = explode(":",$campos); }
					}
	  		 $this->name = $name;
	  		 $this->db = $db;
	  		 $this->sql = "show fields from " . $this->name;
			   $this->hide = $hide;
			   $this->action = $action;
			   if ($where != "") { $this->where = " where " . $where; }
			   else { $this->where = ""; }
			   if ($order != "") { $this->order = " order by " . $order; }
			   else { $this->order = ""; }
			   if ($limit != "") { $this->limit = " limit " . $limit; }
			   else { $this->limit = ""; }
			   $this->ignore = $ignore;
			   $this->passwd = $passwd;
			   $this->hide = $hide;
	  		 $this->fields = array();
		 	   $result0 = mysql_list_fields($db->name,$this->name);
//               echo "<br>".implode(",",$campos);
               $raw = $db->get($this->sql,"2");
			   if ($this->action == "2") {
               if (sizeof($campos)>0)
			   	   $getval = "select ". implode(",",$campos) ." from " . $this->name . $this->where;
			   else
			   	   $getval = "select * from " . $this->name . $this->where;
			   $raw2 = $db->get($getval,"1");
				  }

          for ($i = 0 ; $i < sizeof($raw); $i++)
           {
			   	  $len = mysql_field_len($result0, $i);
			   	  $row = $raw[$i];
			   	  if (in_array($row[0],$campos) or sizeof($campos)==0)
             {
              if (in_array($i,$this->passwd) || in_array($row[0],$this->passwd)) { $passwd = "1"; }
				      else { $passwd = "0"; }
			   	    if (in_array($i,$this->hide) || in_array($row[0],$this->hide) || in_array($row[0],$combos)) { $hidden = "1"; }
				      else { $hidden = "0"; }
			   	    if (in_array($i,$this->ignore) || in_array($row[0],$this->ignore)) { }
				      else
               {
  				  	  if ($this->action == "2") { $value = $raw2[$row[0]]; }
						    else { $value = ""; }
                $field[id] = $this->name . "f" . $i;
						    array_push($this->fields,$field[id]);
						    $temp = $field[id];
						    $GLOBALS[$temp] = new MySQLField($field[id],$row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$len,$passwd,$hidden,$value);
//						    echo "<br><b>CAMPO:</b> ID=$field[id] <br>value=$value row0= $row[0] contcamp=$contcamp i=$i<br> name=$row[0]";
               }
             }
           }
			   }

			
	  // Submit the fields to DB <
	  // -------------------------
	  function submit ($form) {
	  		   $db = $this->db;
			   $string = "";
//	       $tempid = $GLOBALS[$form]->ids;
//			   $tempid = array_flip($tempid);
//		       $tempde = $GLOBALS[$form]->details;
			   // Start Setting Query
		       // -------------------
		       if ($this->action == "2") {
		 	      $strstart = "Update ";
			      $strtemp = " set ";
			      $bodyend = "";
			      $bodyend2 = "";
		 	      }
		       else {
		 	        $strstart = "Insert into ";
			        $strtemp = "";
			        $bodyend = "values (";
			        $bodyend2 = ")";
			        }

		       // Get Table Details and Add Query Body
		       // ------------------------------------
		       if ($this->action == "2") { $body = ""; $bodyend = "";}
		       else { $body = "("; $bodyend = ") values (";}
		       $ac0 = "0";
		       $ac1 = "0";
		       $ac2 = "0";
			   for ($i = 0; $i < sizeof($this->fields); $i++) {
			   	   $temp = $this->fields[$i];
				   $GLOBALS[$temp]->GetValue();
				   $fid = $GLOBALS[$temp]->ID;
//				   $number = $tempid[$fid];
//				   $defield = $tempde[$number];
				   $field = $GLOBALS[$temp]->name;
				   $value = $GLOBALS[$temp]->value;
//				   if ($value != "") {
				   	  if ($this->action != "2") {
				   	  	 $body .= $this->add_body($field,$value,$ac0,"0");
					  	 $ac0++;
					  	 $bodyend .= $this->add_body($field,$value,$ac1,"1");
					  	 $ac1++;
					  	 }
					  else {
					  	   $body .= $this->add_body($field,$value,$ac2,"2");
						   $ac2++;
						   }
				   	  }
		       $bodyend .= $bodyend2;
		       $body .= $bodyend;

		       // Compile Query End
		       // -----------------
		       if (($this->action != "") && ($this->where != "")) {
		 	      $this->where = " " . $this->where;
			      $stringend = $this->where;
			      }
		       else { $stringend = ""; }

		       // Make Query
		       // -----------------
		       $string .= $strstart;
		       $string .= $this->name;
		       $string .= $strtemp;
		       $string .= $body;
		       $string .= $stringend;
//           echo $string."<br>";
		       // Execute Query, return result if successful, return error if failed
		       // ------------------------------------------------------------------
		       if ($GLOBALS[input_error] == "") {
		 	      if ($db->put($string,"1")) {
				  	 return True;
					 }
				  else {
				  	   echo "Unknown Error!";
					   return False;
					   }
				  }
		       else {
			   		echo $GLOBALS[input_error];
					return False;
 			        }
		       }

			
	  // Turn the field object's variable into a SQL update/insert query
	  // ---------------------------------------------------------------
	  function add_body($field,$value,$ac,$action) {
		
		 	   $out = "";
			   if ($ac != "0") { $out .= ","; }
			   else { $out .= ""; }
			   switch ($action) {
			   		  case 0:
					  	   $out .= "$field";
						   break;
		 			  case 1:
					  	   $out .= " \"$value\"";
						   break;
		 			  case 2:
					  	   $out .= " $field = \"$value\"";
						   break;
		 			  }
			   return $out;
			   }
	  }

//###############################################################################3
//###############################################################################3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           MySQLField Class - The MySQL Form Generator   #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MySQLField {
	
	  var $ID;  // Field ID
	  var $name;  // Name of field
	  var $type;  // Field type
	  var $null;  // Null or Not Null
	  var $key;  // Field index or key value in table
	  var $def;  // Default field value
	  var $extra;  // Extra field info
	  var $len;  // Length of field
	  var $passwd;  // Is this field a password field?
	  var $value;  // Value from DB, and later new value to be put into DB
	  var $varname;  // Varname of field, used to find value after submit
	  var $hidden;  // IF hidden, used to generate as hidden
	  var $formtype;  // What type of forminput this object will generate
	

	  function MySQLField ($ID,$name,$type,$null,$key,$def,$extra,$len,$passwd,$hidden,$value) {

			   $this->ID = $ID;
			   $this->name = $name;
			   $this->type = $type;
			   $this->null = $null;
			   $this->key = $key;
			   $this->def = $def;
			   $this->extra = $extra;
			   $this->len = $len;
			   $this->passwd = $passwd;
			   $this->value = $value;
	  		 $this->varname = $name;
	  		 $this->hidden = $hidden;
	  		 $this->formtype = "";
			   }

	
	  function SetValue ($value) {

			   $this->value = $value;
			   }


	  function SetVar ($varname) {

			   $this->varname = $varname;
			   }


	  function Build ($details,$c1width,$c2width,$c3width,$rows,$cols) {

			   $this->value = stripslashes($this->value);
//         $len=50;
			   if ($this->len >= 40) { $maxsize = 40; }
			   else { $maxsize = $this->len; }
			   if ($this->passwd == "1") { $type = "password"; }
			   elseif ($this->hidden == "1") { $type = "hidden"; }
			   else { $type = "text"; }

			   if ($select != "") {
			   	  $array1 = $select;
				  $keys = array_keys($array1);
				  $output = "";
				  $output .= "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\">$details</td><td width=\"$c2width\"></td><td align=left width=\"$c3width\">\n";
				  $output .= "<select name=\"" . $this->name . "\" class=\"forminput\">\n";
				  for ($i = 0; $i < sizeof($array1); $i++) {
				  	  $valz = $keys[$i];
					  if ($array1[$valz] == $this->value) { $selected = " selected"; }
					  else { $selected = ""; }
					  	   $output .= "<option value=\"$valz\"$selected>$array1[$valz]</option>\n";
					  	   }
					  $output .= "</select></td>\n</tr>\n";
				  	  }
			   else {
			   		
					if ($this->hidden == "1") {
				 	   $output .= "<input type=\"$type\" name=\"" . $this->name . "\" value=\"" . $this->value . "\" size=\"$maxsize\" maxlength=\"$len\" class=\"forminput\">";
				 	   $output .= "</input>\n";
					   }
					else {
			
			   		     // Check if type is Integer, Real, Date/Time, Char/Varchar or Year
		 	  		     // ---------------------------------------------------------------
		 	  		     if ((eregi("int",$this->type)) || (eregi("double",$this->type)) || (eregi("float",$this->type)) || (ereg("decimal",$this->type)) || (ereg("date",$this->type)) || (ereg("time",$this->type)) || (ereg("year",$this->type)) || (ereg("char",$this->type))) {
		 	  	 	        $output = "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\">$details</td><td width=\"$c2width\"></td><td align=left width=\"$c3width\" class=\"formvalue\">";
				 	        $output .= "<input type=\"$type\" name=\"" . $this->name . "\" value=\"" . $this->value . "\" size=\"$maxsize\" maxlength=\"$len\" class=\"forminput\">";
				 	        $output .= "</input></td>\n</tr>\n";
				 	        }

					     // Check if type is and if len = 1, make checkbox, else make combobox
					     // ------------------------------------------------------------------
					     elseif (eregi("enum",$this->type)) {
						 		$selvalue = str_replace("enum(","",$this->type);
								$selvalue = str_replace("'","",$selvalue);
								$selvalue = str_replace("(","",$selvalue);
								$selvalue = str_replace(")","",$selvalue);
								$selvalue = explode(",",$selvalue);
								$output = "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\">$details</td><td width=\"$c2width\"></td><td align=left width=\"$c3width\" class=\"formvalue\">";
								$output .= "<select name=\"" . $this->name . "\" class=\"formcombobox\">";
								for ($i = 0; $i < sizeof($selvalue); $i++) {
									if ($this->value == $selvalue[$i]) { $selected = " selected"; }
									else { $selected = ""; }
									$output .= "<option value=\"$selvalue[$i]\"$selected>$selvalue[$i]</option>";
									}
								$output .= "</select></td>\n</tr>\n";
			  		            }
			             // Check if type set and make combo box
			             // ------------------------------------
			             elseif (eregi("set",$this->type)) {
			  		            $selvalue = str_replace("set","",$this->type);
					            $selvalue = str_replace("'","",$selvalue);
					            $selvalue = str_replace("(","",$selvalue);
					            $selvalue = str_replace(")","",$selvalue);
					            $selvalue = explode(",",$selvalue);
					            $output = "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\">$details</td><td width=\"$c2width\"></td><td align=left width=\"$c3width\" class=\"formvalue\">";
					            $output .= "<select name=\"" . $this->name . "\" class=\"formcombobox\">";
					            for ($i = 0; $i < sizeof($selvalue); $i++) {
					 	            if ($selvalue[$i] == $this->value) { $selected = " selected"; }
									else { $selected = ""; }
						            $output .= "<option value=\"$selvalue[$i]\"$selected>$selvalue[$i]</option>";
						            }
					            $output .= "</select></td>\n</tr>\n";
					            }
			             // Check if type is Text and make textbox
			             // --------------------------------------
			             elseif (eregi("text",$this->type)) {
			  		            $output = "<tr><td align=center colspan=3 class=\"formdetails\">$details</td></tr>\n<tr><td align=center colspan=3 class=\"formvalue\">";
					            $output .= "<textarea name=\"" . $this->name . "\" class=\"formtextbox\" cols=\"$cols\" rows=\"$rows\">" . $this->value . "</textarea></tr>\n";
					            }
			             // Check if type is Blob, Set or Enum and set output to ""
			             // -------------------------------------------------------
			             elseif (eregi("blob",$this->type)) {
			  		            $output = "";
					            }
			             // If anything else, set output to ""
			             // ----------------------------------
			             else {
			  	              $output = "";
				              }
		 	             }
					}

		       // Return Final String
		       // -------------------
		       return $output;
			   }
			

	  function GetValue () {

			   $var = $this->varname;
			   $temp = $GLOBALS[HTTP_POST_VARS];
			   $this->value = $temp[$this->varname];
			   $this->value = addslashes($this->value);
			   return true;
			   }
			

	  function Result ($details,$c1width,$c2width,$c3width) {

			   $value = stripslashes($this->value);
			   $value = nl2br($value);
			
			   if ($this->passwd != "1" || $this->hidden != "1") {
			   	  // Check if type is Integer, Real, Date/Time, Char/Varchar or Year
				  // ---------------------------------------------------------------
				  if ((eregi("int",$this->type)) || (eregi("double",$this->type)) || (eregi("float",$this->type)) || (ereg("decimal",$this->type)) || (ereg("date",$this->type)) || (ereg("time",$this->type)) || (ereg("year",$this->type)) || (ereg("char",$this->type)) || (eregi("enum",$this->type)) || (eregi("set",$this->type))) {
				  	 $output = "<tr>\n<td align=right width=\"$c1width\" class=\"formdetails\"><b>$details</b></td><td width=\"$c2width\"></td>";
					 $output .= "<td align=left width=\"$c3width\" class=\"formvalue\">" . $value . "</td>\n</tr>\n";
					 }

				  // Check if type is Text and make textbox
				  // --------------------------------------
				  elseif (eregi("text",$this->type)) {
				  		 $output = "<tr><td align=right width=\"$c1width\" class=\"formdetails\"><b>$details</b></td><td width=\"$c2width\"></td><td width=\"$c3width\"></td></tr>\n";
						 $output .= "<tr><td align=center colspan=3 class=\"formvalue\">" . $value . "</tr>\n";
						 $output .= "<tr><td align=center colspan=3 class=\"formvalue\"></tr>\n";
						 }

				  // Check if type is Blob, Set or Enum and set output to ""
				  // -------------------------------------------------------
				  elseif (eregi("blob",$this->type)) {
				  		 $output = "";
						 }

				  // If anything else, set output to ""
				  // ----------------------------------
				  else {
				  	   $output = "";
					   }
				  }
			   else { $output = ""; }
			   		

		       // Return Final String
		       // -------------------
		       return $output;
			   }
	  }

//###############################################################################3
//###############################################################################3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           MySQLDB Class - The MySQL Form Generator      #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MySQLDB {
	
	  var $name;
	  var $login;
	  var $passwd;
	  var $host;
	  var $conn;
	  var $error;
	
	  // MySQLDB Constructor <
	  // ---------------------
	  function MySQLDB ($name,$login,$passwd,$host) {

			   if ($host == "") { $host = "localhost"; }
	  		   $this->name = $name;
	  		   $this->type = $type;
	  		   $this->login = $login;
	  		   $this->passwd = $passwd;
			   $this->host = $host;
			   $this->error = "";
			   $this->conn = mysql_connect($this->host,$this->login,$this->passwd);
			   }

	  // MySQL Select Database <
	  // -----------------------
   	  function select () {

	  		   mysql_select_db($this->name,$this->conn);
			   }


	  // MySQL Insert and Update <
	  // -------------------------
	  function put ($string) {

	  		   if (mysql_query($string,$this->conn)) {
			   	  return true;
				  }
		 	   else {
			   		$this->error = mysql_error($this->conn);
					return false;
			   		}
	  		   }

	  //  MySQL Select <
	  // ---------------
	  function get($string,$mode) {

			   if ($mode == "") { $mode = "1"; }
			   	  $result = mysql_query($string,$this->conn);
				  $this->error = mysql_error($this->conn);
				  switch ($mode) {
				  		 case 0:
						 	  return $result;
							  break;
				  		 case 1:
						 	  if (mysql_num_rows($result) != "0") {
							  	 $array = mysql_fetch_array($result);
								 return $array;
								 }
							  else {
							  	   $array = array("0");
								   return $array;
								   }
							  break;
				  		 case 2:
						 	  $rows = mysql_num_rows($result);
							  for ($i = 0; $i < $rows; $i++) {
							  	  $array[$i] = mysql_fetch_array($result);
							  }
							  return $array;
							  break;
						 }
				  }
	  		   }


//###############################################################################3
//###############################################################################3

//######################## CLASES DE ONFICGURACI�N MEDIANTE ARCHIVO EXTERNO #####3
//###############################################################################3
 class ConfCombo
  {
   var $name;
   var $tabla;
   var $campo_mostrar;	
   var $campo_clave;

   function ConfCombo ($name,$tabla,$campo_mostrar,$campo_clave)
    {
     $this->name = $name;
     $this->tabla = $tabla;
     $this->campo_mostrar = $campo_mostrar;
     $this->campo_clave = $campo_clave;
    }

   //Esta funci�n agrega este ConfCombo a un Form que venga como par�metro.
   function ponerEnForm($db1,&$form)
    {
     $form->addCombo($db1,$this->name,$this->tabla,$this->campo_mostrar,$this->campo_clave);
    }
  }

 class ConfComboFiltro extends ConfCombo
  {
   var $filtro;
   function ConfComboFiltro($name,$tabla,$campo_mostrar,$campo_clave,$filtro)
    {
     parent::ConfCombo($name,$tabla,$campo_mostrar,$campo_clave);
     $this->filtro = $filtro;
    }

   //Esta funci�n agrega este ConfComboFiltro a un Form que venga como par�metro.
   function ponerEnForm($db1,&$form)
    {
     $form->addComboFiltro($db1,$this->name,$this->tabla,$this->campo_mostrar,$this->campo_clave,$this->filtro);
    }
  }

 class conf_tabla
  {
   var $campos_usar;
   var $titulos_campos;
   var $campos_hidden;
   var $validaciones;
   var $combos;

   function conf_tabla()
    {
     $this->campos_usar = array();
     $this->titulos_campos = array();
     $this->campos_hidden = array();
     $this->validaciones = array();
     $this->combos = array();
    }
  }

//######################## CLASES DE ONFICGURACI�N MEDIANTE ARCHIVO EXTERNO #####3
//###############################################################################3


?>

