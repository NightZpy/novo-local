// Menus desplegables
// Angel Chiang G.
//CAMBIADO POR ALE EN L�NEAS 233 y 252 de -1 a -10.
// Variables globales

var ids_timeout_oculta = new Array();
var ids_timeout_muestra = new Array();

var submenu_init = new Array();

// ancho del menu en pixeles
var ancho_menu = 760;

// para bordes y sombras de los submenus
var imagen_arriba = new Array();
var imagen_izquierda = new Array();
var imagen_derecha = new Array();
var imagen_abajo = new Array();
var imagen_esquina_izq = new Array();
var imagen_esquina_der = new Array();
var imagen_esquina_sup_izq = new Array();
var imagen_esquina_sup_der = new Array();

// Funciones de inicializacion del menu
// --------------------------------------------------------

// Inicializa el menu
function inicializa_menu()
	{
	
// 	var t0;
// 	t0 = new Date().getTime();
	
	var menu = document.getElementById("menu");
  //menu.offsetWidth = 10;
	ancho_menu = menu.offsetWidth;

	// ajusta algunos estilos
	
	var lis = menu.getElementsByTagName("li");

	var lis_length = lis.length;

	if (lis_length)
		{
		ajusta_estilos_menu(menu);
		}
	
	// agrega los manejadores de eventos
	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		li.id = "item_menu" + i;	
			
		submenu_init[li.id]	= 0;
			
		var uls = li.getElementsByTagName("ul");
		if(uls.length)
			{
			li.onmouseover = muestra_submenu;
			li.onmouseout = oculta_submenu;
			
			ids_timeout_oculta[li.id] = null;
			ids_timeout_muestra[li.id] = null;
			}
		}

	// justifica el primer nivel
	if (lis_length)
		{
		ajusta_ancho_primer_nivel(menu);
		}
		
	// ajusta el ancho de los submenus
/*	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		ajusta_ancho_submenu(li);
		}*/
		
	// reajusta el ancho de los submenus
/*	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		reajusta_ancho_submenu(li);
		}	*/
	
	// imagenes para los bordes y sombras
	// alinea los submenus horizontalmente
	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		var uls = li.getElementsByTagName("ul");
		
		var primero = 1;
		var id_primero;
		if (uls.length)
			{
			if (primero)
				{
				primero = 0;
				id_primero = li.id;
				imagen_arriba[id_primero]          = new Image();
				imagen_izquierda[id_primero]       = new Image();
				imagen_derecha[id_primero]         = new Image();
				imagen_abajo[id_primero]           = new Image();
				imagen_esquina_izq[id_primero]     = new Image();
				imagen_esquina_der[id_primero]     = new Image();
				imagen_esquina_sup_izq[id_primero] = new Image();
				imagen_esquina_sup_der[id_primero] = new Image();
				
				imagen_arriba[id_primero].src          = "/images/html/arriba.png";
				imagen_izquierda[id_primero].src       = "/images/html/izquierda.png";
				imagen_derecha[id_primero].src         = "/images/html/derecha.png";
				imagen_abajo[id_primero].src           = "/images/html/abajo.png";
				imagen_esquina_izq[id_primero].src     = "/images/html/esquina_izq.png";
				imagen_esquina_der[id_primero].src     = "/images/html/esquina_der.png";	
				imagen_esquina_sup_izq[id_primero].src = "/images/html/esquina_sup_izq.png";
				imagen_esquina_sup_der[id_primero].src = "/images/html/esquina_sup_der.png";	
				}
			else
				{
				var li_id = li.id
				imagen_arriba[li_id]          = imagen_arriba[id_primero].cloneNode(false);
				imagen_izquierda[li_id]       = imagen_izquierda[id_primero].cloneNode(false);
				imagen_derecha[li_id]         = imagen_derecha[id_primero].cloneNode(false);
				imagen_abajo[li_id]           = imagen_abajo[id_primero].cloneNode(false);
				imagen_esquina_izq[li_id]     = imagen_esquina_izq[id_primero].cloneNode(false);
				imagen_esquina_der[li_id]     = imagen_esquina_der[id_primero].cloneNode(false);
				imagen_esquina_sup_izq[li_id] = imagen_esquina_sup_izq[id_primero].cloneNode(false);
				imagen_esquina_sup_der[li_id] = imagen_esquina_sup_der[id_primero].cloneNode(false);				
				}
			
// 			alinea_submenu(li);		
			}
		}
	}
	
function inicializa_submenu(li)
	{
	var uls = li.getElementsByTagName("ul");
	
	if (uls.length)
		{
		ajusta_ancho_submenu(li);
		reajusta_ancho_submenu(li);
		alinea_submenu(li);
		}
	
	submenu_init[li.id] = 1;
	}
	
// Ajusta algunos estilos (primeros y ultimos items del menu, etc)
function ajusta_estilos_menu(menu)
	{
	var nodos_menu = menu.childNodes;

	// items primer nivel
	var lis = new Array();

	for (var i = 0; i < nodos_menu.length; i++)
		{
		var nodo = nodos_menu[i];
		if (nodo.nodeName == "LI")
			{
			lis[lis.length] = nodo;
			}
		}
	
	// quita el separador del ultimo item		
	lis[lis.length-1].style.backgroundImage = "none";
	var as = lis[lis.length-1].getElementsByTagName("a");
	as[0].style.marginRight = "0px";

	// items submenus
	for (var k = 0; k < lis.length; k++)
		{
	
		var uls = lis[k].getElementsByTagName("ul");
		
		for (var i = 0; i < uls.length; i++)
			{
			var ul = uls[i];
			var nodos_ul = ul.childNodes;
			
			var sub_lis = new Array();
			
			for (var j = 0; j < nodos_ul.length; j++)
				{
				var nodo = nodos_ul[j];
				if (nodo.nodeName == "LI")
					{
					sub_lis[sub_lis.length] = nodo;
					}
				}		
			
			// agrega margenes arriba y abajo y quita separador del ultimo item
			var as = sub_lis[0].getElementsByTagName("a");
			as[0].style.marginTop = "5px";
			
			var as = sub_lis[sub_lis.length-1].getElementsByTagName("a");
			as[0].style.border = "0";		
			as[0].style.marginBottom = "8px";
			}
		}
	}
	
// Justifica los items del primer nivel para que ocupen el ancho de la pagina
function ajusta_ancho_primer_nivel(menu)
	{
	var nodos_menu = menu.childNodes;

	var lis = new Array();

	var nodos_menu_length = nodos_menu.length;

	for (var i = 0; i < nodos_menu_length; i++)
		{
		var nodo = nodos_menu[i];
		if (nodo.nodeName == "LI")
			{
			lis[lis.length] = nodo;
			}
		}

	var ancho_lis = 0;

	var lis_length = lis.length;

	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		ancho_lis = ancho_lis + li.offsetWidth - 1;
		}
	
	// li.offsetWidth - 1 => menos un px por borde izquierdo
	
	var diferencia = ancho_menu - ancho_lis;
	var diferencia_li = diferencia / lis_length;

 	diferencia_li = Math.round(diferencia_li);

 	var nuevo_ancho_lis = 0;
 	
	for (var i = 0; i < lis_length; i++)
		{
		var li = lis[i];
		
		if (i < (lis_length - 1))
			{
			li.style.width = (li.offsetWidth + diferencia_li) + "px";
			nuevo_ancho_lis = nuevo_ancho_lis + li.offsetWidth - 1;
			}
		else
			{
			li.style.width = (ancho_menu - nuevo_ancho_lis) + "px";
			}
		var as = li.getElementsByTagName("a");
		var a = as[0];
		a.style.width = (li.offsetWidth) + "px";			
		}
	}

// Ajusta el ancho de los submenus al ancho del item mas largo
function ajusta_ancho_submenu(li)	
	{
	var uls = li.getElementsByTagName("ul");
	if (uls.length)
		{
		var ul = uls[0];
		var sub_lis = li.getElementsByTagName("li");
		
		var max_width = 0;
		var sub_lis_length = sub_lis.length;
		
		for (var i = 0; i < sub_lis_length; i++)
			{
			var sub_li = sub_lis[i];
			if (sub_li.parentNode.parentNode.id == li.id)
				{
				if (max_width < sub_li.offsetWidth)
					{
					max_width = sub_li.offsetWidth;
					}
				}
			}
		
		for (var i = 0; i < sub_lis_length; i++)
			{
			var sub_li = sub_lis[i];
			if (sub_li.parentNode.parentNode.id == li.id)
				{
				sub_li.style.width = max_width + "px";
				var as = sub_li.getElementsByTagName("a");
				var a = as[0];
				a.style.width = (max_width - 8 - 5) + "px";
				}
			}
			
		ul.style.width = max_width + "px";		
		}	
	}
	
// Aumenta el ancho de los submenus al ancho del padre si corresponde
function reajusta_ancho_submenu(li)	
	{
	var uls = li.getElementsByTagName("ul");
	if (uls.length)
		{
		var ul = uls[0];

		// si el ancho del padre es mayor...
		if (ul.offsetWidth < li.offsetWidth && li.parentNode.id == 'menu')
			{
			var max_width = li.offsetWidth;
			
			var sub_lis = li.getElementsByTagName("li");
			
			for (var i = 0; i < sub_lis.length; i++)
				{
				var sub_li = sub_lis[i];
				if (sub_li.parentNode.parentNode.id == li.id)
					{
					sub_li.style.width = max_width + "px";
					var as = sub_li.getElementsByTagName("a");
					var a = as[0];
					a.style.width = (max_width - 8 - 5) + "px";
					}
				}
				
			ul.style.width = max_width + "px";				
			}				
		}	
	}

// Alinea los submenus
function alinea_submenu(li)
	{
	var uls = li.getElementsByTagName("ul");


	if (uls.length)
		{
		var menu = document.getElementById("menu");
// 		var limite_menu = findPosX(menu) + ancho_menu;
		var posx_menu = findPosX(menu);
		var posx_li = findPosX(li);

		var limite_menu = posx_menu + menu.offsetWidth;
		var ul = uls[0];
		var ul_style = ul.style;
		var ul_offsetwidth = ul.offsetWidth;
		var li_offsetwidth = li.offsetWidth;
		

		// primer nivel
		if (li.parentNode.id == "menu")
			{
			ul_style.left = (posx_li - posx_menu) + "px";
			
			// si submenu se pasa se alinea con el limite derecho del padre
			var limite_ul = findPosX(ul) + ul_offsetwidth;
			if (limite_ul > limite_menu)
				{
				ul_style.left = (((posx_li + li_offsetwidth) - ul_offsetwidth) - posx_menu) + "px";
				}
			
			}
		// niveles inferiores
		else 
			{
			var posicion_li = posx_li;
			var limite_ul = posicion_li + li_offsetwidth + ul_offsetwidth;
	
// 			if (limite_ul > limite_menu)
			if (submenus_izquierda(li.parentNode))
				{
				ul_style.left = - (ul_offsetwidth + 7) + "px";
				}
			else
				{
				ul_style.left = (li_offsetwidth + 7) + "px";
				}
			ul_style.top = (ul.offsetTop - 27) + "px";
			}
			
		// aqui se agregan los bordes bonitos
		add_borders(li, ul);
		}
	}
	
// Calcula si el submenu debe ir a la izquierda o no
function submenus_izquierda(ul)
	{
	var menu = document.getElementById("menu");
	var limite_menu = findPosX(menu) + menu.offsetWidth;
	var lis = ul.getElementsByTagName('li');
	var uls = ul.getElementsByTagName('ul');
	
	var li = lis[0];
	var posicion_li = findPosX(li);
	
	var limite_ul = 0;
	
	for (var i = 0; i < uls.length; i++)
		{
		var ul = uls[i];
		
		if (limite_ul < posicion_li + li.offsetWidth + ul.offsetWidth)
			{
			var limite_ul = posicion_li + li.offsetWidth + ul.offsetWidth;
			}
		}
	
	if (limite_ul > limite_menu)
		{
		return 1;
		}	
	else
		{
		return 0;
		}
	}
	
// Agrega bordes y sombras (no los muestra)
function add_borders(li, ul)
	{
	var offset_left   = ul.offsetLeft;
	var offset_top    = ul.offsetTop;
	var offset_height = ul.offsetHeight;
	var offset_width  = ul.offsetWidth;

	var css;
	li.appendChild(imagen_izquierda[li.id]);
	
	css = imagen_izquierda[li.id].style;
	css.position     = "absolute";
	css.zIndex       = 4;
	css.left         = (offset_left - 7) + "px";
	css.top          = (offset_top + 1) + "px";
	css.height       = (offset_height - 6) + "px";
	css.width        = "7px";
	css.visibility   = "hidden";
	
	css = imagen_derecha[li.id].style;
	li.appendChild(imagen_derecha[li.id]);
	css.position       = "absolute";
	css.zIndex         = 4;
	css.left           = (offset_left + offset_width) + "px";
	css.top            = (offset_top + 1) + "px";
	css.height         = (offset_height - 6) + "px";
	css.width          = "7px";
	css.visibility     = "hidden";

	css = imagen_abajo[li.id].style;
	li.appendChild(imagen_abajo[li.id]);
	css.position         = "absolute";
	css.zIndex           = 4;
	css.left             = (offset_left + 11) + "px";
	css.top              = (offset_top + offset_height) + "px";
	css.height           = "13px";
	css.width            = (offset_width - 23) + "px";
	css.visibility       = "hidden";
	
	css = imagen_esquina_izq[li.id].style;
	li.appendChild(imagen_esquina_izq[li.id]);
	css.position   = "absolute";
	css.zIndex     = 4;
	css.left       = (offset_left -7) + "px";
	css.top        = (offset_top + offset_height - 5) + "px";
	css.height     = "18px";
	css.width      = "18px";
	css.visibility = "hidden";
	
	css = imagen_esquina_der[li.id].style;
	li.appendChild(imagen_esquina_der[li.id]);
	css.position   = "absolute";
	css.zIndex     = 4;
	css.left       = (offset_left + offset_width - 12) + "px";
	css.top        = (offset_top + offset_height - 5) + "px";
	css.height     = "18px";
	css.width      = "18px";
	css.visibility = "hidden";

	if (li.parentNode.id != 'menu')
		{
		css = imagen_esquina_sup_izq[li.id].style;
		li.appendChild(imagen_esquina_sup_izq[li.id]);
		css.position   = "absolute";
		css.zIndex     = 4;
		css.left       = (offset_left -7) + "px";
		css.top        = (offset_top - 13) + "px";
		css.height     = "14px";
		css.width      = "18px";
		css.visibility = "hidden";
		
		css = imagen_esquina_sup_der[li.id].style;
		li.appendChild(imagen_esquina_sup_der[li.id]);
		css.position   = "absolute";
		css.zIndex     = 4;
		css.left       = (offset_left + offset_width - 12) + "px";
		css.top        = (offset_top - 13) + "px";
		css.height     = "14px";
		css.width      = "18px";
		css.visibility = "hidden";
	
		css = imagen_arriba[li.id].style;
		li.appendChild(imagen_arriba[li.id]);
		css.position     = "absolute";
		css.zIndex       = 4;
		css.left         = (offset_left + 11) + "px";
		css.top          = (offset_top - 13) + "px";
		css.height       = "14px";
		css.width        = (offset_width - 23) + "px";
		css.visibility   = "hidden";
		}
		
	// Hack para canal alfa de png's en Internet Explorer
	if (document.all)
		{
		imagen_izquierda[li.id].runtimeStyle.filter =
			"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
			imagen_izquierda[li.id].src + "', sizingMethod='scale')" ;
		
		imagen_derecha[li.id].runtimeStyle.filter =
			"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
			imagen_derecha[li.id].src + "', sizingMethod='scale')" ;
		
		imagen_abajo[li.id].runtimeStyle.filter =
			"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
			imagen_abajo[li.id].src + "', sizingMethod='scale')" ;
		
		imagen_esquina_izq[li.id].runtimeStyle.filter =
			"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
			imagen_esquina_izq[li.id].src + "', sizingMethod='scale')" ;
		
		imagen_esquina_der[li.id].runtimeStyle.filter =
			"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
			imagen_esquina_der[li.id].src + "', sizingMethod='scale')" ;
		
		imagen_izquierda[li.id].src   = "/images/html/imagen1x1.gif";
		imagen_derecha[li.id].src     = "/images/html/imagen1x1.gif";
		imagen_abajo[li.id].src       = "/images/html/imagen1x1.gif";
		imagen_esquina_izq[li.id].src = "/images/html/imagen1x1.gif";
		imagen_esquina_der[li.id].src = "/images/html/imagen1x1.gif";

		
		if (li.parentNode.id != 'menu')
			{
			imagen_esquina_sup_izq[li.id].runtimeStyle.filter =
				"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
				imagen_esquina_sup_izq[li.id].src + "', sizingMethod='scale')" ;
			
			imagen_esquina_sup_der[li.id].runtimeStyle.filter =
				"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
				imagen_esquina_sup_der[li.id].src + "', sizingMethod='scale')" ;
			
			imagen_arriba[li.id].runtimeStyle.filter =
				"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
				imagen_arriba[li.id].src + "', sizingMethod='scale')" ;
			
			imagen_arriba[li.id].src   = "/images/html/imagen1x1.gif";
			imagen_esquina_sup_izq[li.id].src = "/images/html/imagen1x1.gif";
			imagen_esquina_sup_der[li.id].src = "/images/html/imagen1x1.gif";
			}
		}
	}

	
// Las siguientes funciones muestran y ocultan los submenus
// --------------------------------------------------------
	
function muestra_submenu(evento)
	{
	// inicializa el submenu si es que no se ha inicializado
	if (!submenu_init[this.id])
		{
		inicializa_submenu(this);
		}
	
	// oculta todos menos el actual
	oculta_submenus(this);
	
	window.clearTimeout(ids_timeout_oculta[this.id]);
 	var id_timeout = window.setTimeout('set_clase("' + this.id + '", "hover")', 30);
 	ids_timeout_muestra[this.id] = id_timeout;
	}

// Oculta todos los submenus menos el correspondiente al id
function oculta_submenus(li_activo)
	{

	var menu = li_activo.parentNode;
// 	var menu = document.getElementById("menu");
	var nodos_menu = menu.childNodes;
	
	// items primer nivel
	var lis = new Array();

	for (var i = 0; i < nodos_menu.length; i++)
		{
		var nodo = nodos_menu[i];
		if (nodo.nodeName == "LI")
			{
			lis[lis.length] = nodo;
			}
		}

	
	for (var i = 0; i < lis.length; i++)
		{
		var li = lis[i];
		if (li.id != li_activo.id)
			{
/*			window.clearTimeout(ids_timeout_muestra[li.id]);
			window.clearTimeout(ids_timeout_oculta[li.id]);
			var id_timeout = window.setTimeout('set_clase("' + li.id + '", "")', 30);
			ids_timeout_oculta[li.id] = id_timeout;*/
			oculta_submenu_recursivo(li);
			}
		}
	}

// Oculta submenus en forma "recursiva"
function oculta_submenu_recursivo(li)
	{
	var subuls = li.getElementsByTagName('ul');
	
	for (var i = 0; i < subuls.length; i++)
		{
		var subli = subuls[i].parentNode;
		
		window.clearTimeout(ids_timeout_muestra[subli.id]);
		window.clearTimeout(ids_timeout_oculta[subli.id]);
		var id_timeout = window.setTimeout('set_clase("' + subli.id + '", "")', 30);
		ids_timeout_oculta[subli.id] = id_timeout;
		}
	}

function oculta_submenu(evento)
	{
	window.clearTimeout(ids_timeout_muestra[this.id]);
	var id_timeout = window.setTimeout('set_clase("' + this.id + '", "")', 500);
	ids_timeout_oculta[this.id] = id_timeout;
	}

// Cambia la clase del li (oculta/muestra submenu)
function set_clase(id_li, clase)
	{
	var li = document.getElementById(id_li);
	
	if (li.className != clase)
		{
		li.className = clase;
		
		var uls = li.getElementsByTagName("ul");
		
		if(uls.length)
			{
			// iframe para Internet Explorer
			if (document.all)
				{
				if (clase == 'hover') add_iframe(li);
				else
					{
					remove_iframe(li);
					}
				}
			
			// bordes y sombras
			if (clase == 'hover') show_borders(li);
			else hide_borders(li);
			}
		}
	}
	
// Muestra los bordes
function show_borders(li)
	{
	imagen_izquierda[li.id].style.visibility   = "visible";
	imagen_derecha[li.id].style.visibility     = "visible";	
	imagen_abajo[li.id].style.visibility       = "visible";
	imagen_esquina_izq[li.id].style.visibility = "visible";
	imagen_esquina_der[li.id].style.visibility = "visible";
	
	if (li.parentNode.id != 'menu')
		{
		imagen_arriba[li.id].style.visibility      = "visible";
		imagen_esquina_sup_izq[li.id].style.visibility = "visible";
		imagen_esquina_sup_der[li.id].style.visibility = "visible";
		}
	}
	
// Oculta los bordes
function hide_borders(li)
	{
	imagen_izquierda[li.id].style.visibility   = "hidden";
	imagen_derecha[li.id].style.visibility     = "hidden";	
	imagen_abajo[li.id].style.visibility       = "hidden";
	imagen_esquina_izq[li.id].style.visibility = "hidden";
	imagen_esquina_der[li.id].style.visibility = "hidden";
	if (li.parentNode.id != 'menu')
		{
		imagen_arriba[li.id].style.visibility      = "hidden";
		imagen_esquina_sup_izq[li.id].style.visibility = "hidden";
		imagen_esquina_sup_der[li.id].style.visibility = "hidden";
		}
	}
	
// Agrega un iframe para que el menu no aparezca bajo los selects en Internet Explorer	
function add_iframe(li)
	{
	var uls = li.getElementsByTagName('ul');
	if (uls.length)
		{
		var ul = uls[0];
		var iframes = li.getElementsByTagName('iframe');

		if (!iframes.length)
			{
			var iframe = document.createElement('iframe');
			iframe.setAttribute("frameborder", "0");
			iframe.src = "/images/html/imagen1x1.gif";
			li.appendChild(iframe);
			iframe.style.width = ul.offsetWidth + "px";
			iframe.style.height = (ul.offsetHeight + 5) + "px";
			iframe.style.zIndex = 2;
			iframe.style.position = "absolute";
			iframe.style.left = ul.offsetLeft + "px";
			iframe.style.top = ul.offsetTop + "px";
			ul.style.zIndex = 3;
			}
		}	
	}

// Quita el iframe
function remove_iframe(li)
	{
	var uls = li.getElementsByTagName('ul');
	if (uls.length)
		{
		var ul = uls[0];
		var iframes = li.getElementsByTagName('iframe');
		
		while (iframes.length)
			{
			var iframe = iframes[0];
			iframe.parentNode.removeChild(iframe);
			}
		}	
	}


// --------------------------------------------------------
		
	
// Posicion absoluta del objeto
function findPosX(obj)
	{
	var curleft = 0;
	if (obj.offsetParent)
		{
		while (obj.offsetParent)
			{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
			}
		}
	else if (obj.x)
		curleft += obj.x;
		
	if (!document.all)
		{
		curleft -= 8;
		}
	return curleft;
	}

// No se usa
/*function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}*/
	




