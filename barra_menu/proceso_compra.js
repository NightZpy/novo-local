<!--
var W3C = document.getElementById? true : false; // IE5+, Netscape 6+, Opera 5+, Konqueror 2.1+, Mozilla and various Mozilla/Gecko-based browsers.
var NN4 = document.layers? true : false; //Netscape Navigator 4.x.
var IE4 = document.all? true : false; // IE version 4 (and above).

var current_calendar_layer;
var current_calendar_image;
var current_calendar_month;
var current_calendar_year;
var current_calendar_num;
var g_fecha_min, g_fecha_max;


function getElemById(id)
	{
	var elem = document.all ? document.all[id] : document.getElementById(id);

	if (!elem)
		{
		return;
		}

	return elem;
	}

//////////////////////////////////////////////////////////////////////////////
// This functions allow temporary deactivation of buttons
//
// You have to configure disabledButtonTimeout and disabledButton[]
// before you can use these functions, on the relevant javascript
// portion in your HTML

var disabledButtonTimeout = 20000;
var disabledButtonColor = '#CCCCCC';
var disabledButton = new Array(10);
var tid = new Array(10);
var pfgc = new Array(10);

function reenableButton(index)
	{
	if (tid[index])
		{
		clearTimeout(tid[index]);
		tid[index] = 0;
		}
	if (disabledButton[index])
		{
		// If it's more than 1 button associated with this index....
		if (disabledButton[index].length)
			{
			for (var i = 0; i < disabledButton[index].length; i++)
				{
				if (disabledButton[index][i].style)
					{
					disabledButton[index][i].style.color = pfgc[index];
					}
				}
			}
		else if (disabledButton[index].style)
			{
			disabledButton[index].style.color = pfgc[index];
			}
		}
	}

function buttonDisabled(index)
	{
	if (tid[index])
		{
		return true;
		}

	tid[index] = setTimeout("reenableButton(" + index + ")",disabledButtonTimeout);

	if (disabledButton[index])
		{
		// If it's more than 1 button associated with this index....
		if (disabledButton[index].length)
			{
			if (disabledButton[index][0].style)
				{
				pfgc[index] = disabledButton[index][0].style.color;
//				alert("ohoh0:" + pfgc[index]);
				}
			else
				{
//				alert("ohoh1");
				}
			for (var i = 0; i < disabledButton[index].length; i++)
				{
				if (disabledButton[index][i].style)
					{
//					alert("ohoh2["+i+"]:" + disabledButton[index][i].style.color);
					disabledButton[index][i].style.color = disabledButtonColor;
					}
				else
					{
//					alert("ohoh3:" + i);
					}
				}
			}
		else if (disabledButton[index].style)
			{
			pfgc[index] = disabledButton[index].style.color;
			disabledButton[index].style.color = disabledButtonColor;
			}
		}
	return false;
	}

//////////////////////////////////////////////////////////////////////////////


function just_refresh_paso_n(n, paso_actual, session_id)
	{
	// Revisar el indice del buttonDisabled
	if ((paso_actual <= n) || buttonDisabled(9))
		{
		return;
		}
	document.location = 'paso' + n + '.cgi?session_id=' + session_id + '&just_refresh=1';
	}

function just_refresh_paso_n_reserva(n, paso_actual, session_id)
	{
	// Agregar la parte el button disabled
	if (paso_actual <= n)
		{
		return;
		}
	document.location = 'paso' + n + 'r.cgi?session_id=' + session_id + '&just_refresh=1';
	}

function acciones_combo_origen_destino(f, combo, num_segmento, tipo)
	{
	var x;
	x = check_select_for_otros_destinos(combo, num_segmento, tipo);

	if (!x && tipo == 'destino')
		{
		pre_select_next_combo(f, combo, num_segmento);
		}
	}

function pre_select_next_combo(f, combo, num_segmento)
	{
	var pre_selected, origen, seg;

	seg = parseInt(num_segmento, 10);

	if (!combo || combo.type != 'select-one')
		{
		return;
		}

	pre_selected = combo.options[combo.selectedIndex].value;
	pre_select_by_value(f, 'from_city' + (seg + 1), pre_selected);

	origen = f.elements['from_city1'];
	if (!origen)
		{
		return;
		}
	pre_selected = origen.options[origen.selectedIndex].value;
	pre_select_by_value(f, 'to_city' + (seg + 1), pre_selected);
	}

function activate_calendar(num, ano, mes)
	{
	var obj, dim, today;
	if (current_calendar_layer)
		{
		hide_layer(current_calendar_layer);
		}

	today = new Date();
	today.setDate(today.getDate() + 14);

	if (!ano)
		{
		ano = new String(today.getFullYear ? today.getFullYear() : today.getYear());
		while (ano.length < 4)
			{
			ano = '0' + ano;
			}
		}
	if (!mes)
		{
		mes = new String(today.getMonth() + 1);
		while (mes.length < 2)
			{
			mes = '0' + mes;
			}
		}
	show_layer('layer_calendario' + num);
	obj = MM_findObj('calendario' + num);
	dim = getDim(obj);
	if (num == 1)
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x - 15, dim.y - 44);
		}
	else if (num == 2)
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x - 15, dim.y - 78);
		}
	else
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x,dim.y - 40);
		}
	current_calendar_num = num;
	current_calendar_layer = 'layer_calendario' + num;
	current_calendar_image = 'imagen_calendario' + num;
	current_calendar_year = ano;
	current_calendar_month = mes;
	set_calendario('imagen_calendario' + num, current_calendar_month, current_calendar_year);
	}

function hide_current_calendar()
	{
	hide_layer(current_calendar_layer);
	}

function selmes(x)
	{
	var m, y, mt, yt;

	m = parseInt(current_calendar_month, 10) + parseInt(x, 10);
	y = parseInt(current_calendar_year, 10);

	while (m > 12)
		{
		m -= 12;
		y += 1;
		}

	while (m < 1)
		{
		m += 12;
		y -= 1;
		}

	mt = new String(m);
	yt = new String(y);

	while (mt.length < 2)
		{
		mt = '0' + mt;
		}

	while (yt.length < 4)
		{
		yt = '0' + yt;
		}


	if ((yt + '-' + mt + '-32') < g_fecha_min)
		{
		return;
		}

	if ((yt + '-' + mt + '-01') > g_fecha_max)
		{
		return;
		}

	current_calendar_month = mt;
	current_calendar_year = yt;

	MM_swapImage(current_calendar_image,'','/images/html/calendarios/cal_' + current_calendar_month + current_calendar_year + '.png');
	}

function selday(x)
	{
	var my_date, first_week_day, days_in_month;

	my_date = new Date(current_calendar_year, current_calendar_month - 1, 1);
	first_week_day = my_date.getDay();
	if (first_week_day == 0)
		{
		first_week_day = 7;
		}
	my_date.setMonth(my_date.getMonth() + 1);
	my_date.setDate(0);
	days_in_month = my_date.getDate();

	x = x - first_week_day + 1;

	if (x > days_in_month)
		{
		return;
		}
	set_fecha_from_calendar(current_calendar_num, current_calendar_year, current_calendar_month, x);
	hide_current_calendar();
	}

function set_calendario(id_calendario, mes, ano)
	{
	MM_swapImage(id_calendario,'','/images/html/calendarios/cal_' + mes + ano + '.png');
	}

function show_layer(layerName)
	{
	if (W3C)
		{
		document.getElementById(layerName).style.visibility = "visible";
		}
	else if (IE4)
		{
		document.all[layerName].style.visibility = "visible";
		}
	else if(NN4)
		{
		document.layers[layerName].visibility = "show";
		}
	}


function hide_layer(layerName)
	{
	if (W3C)
		{
		document.getElementById(layerName).style.visibility = "hidden";
		}
	else if (IE4)
		{
		document.all[layerName].style.visibility = "hidden";
		}
	else if(NN4)
		{
		document.layers[layerName].visibility = "hidden";
		}
	}

function activar_paso(paso)
	{
	var elem;

	if (!paso)
		{
		return;
		}

	if (document.getElementById)
		{
		elem = document.getElementById('td_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.td_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'paso-activo';
		}

	if (document.getElementById)
		{
		elem = document.getElementById('a_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.a_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'link-paso-activo';
		}

	if (document.getElementById)
		{
		elem = document.getElementById('txt_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.txt_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'txt-paso-activo';
		}
	}

function desactivar_paso(paso)
	{
	var elem;

	if (!paso)
		{
		return;
		}

	if (document.getElementById)
		{
		elem = document.getElementById('td_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.td_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'paso-inactivo';
		}

	if (document.getElementById)
		{
		elem = document.getElementById('a_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.a_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'link-paso-inactivo';
		}

	if (document.getElementById)
		{
		elem = document.getElementById('txt_paso' + paso);
		}
	else if (document.all)
		{
		elem = eval("document.all.txt_paso" + paso);
		}
	else
		{
		return
		}
	if (elem)
		{
		elem.className = 'txt-paso-inactivo';
		}
	}

function change_class_for_id(id, clase)
	{
	var elem = document.all ? document.all[id] : document.getElementById(id);

	if (!elem)
		{
		return;
		}

	elem.className = clase;
	}

function change_bg_for_id(id, tipo_bg)
	{
	var elem = document.all ? document.all[id] : document.getElementById(id);

	if (!elem)
		{
		return;
		}
	if (!elem.style)
		{
		alert("Trato de cambiar style inexistente de : " + id);
		return;
		}
	if (tipo_bg == 'ok')
		{
		elem.style.backgroundColor = '#ffffff';
		}
	else
		{
		elem.style.backgroundColor = '#dd9999';
		}
	}

function pre_select_by_value(f,nombre_combo, valor)
	{
	var elemento, i;

	if (valor.length == 0)
		{
		return;
		}
	elemento = eval("f.elements['" + nombre_combo + "']");
	if (!elemento)
		{
		return;
		}
	if (elemento.type == 'select-one')
		{
		for (i = 0; i< elemento.options.length; i++)
			{
			if (elemento.options[i].value == valor)
				{
				elemento.options.selectedIndex = i;
				return;
				}
			}
		}
	else if (elemento.type == 'hidden' || elemento.type == 'text')
		{
		elemento.value = valor;
		}
	}

function pre_select_combo(f, combo, previous)
	{
	var pre_selected, master;

	master = eval("f.elements['to_city" + previous + "']");
	if (!master)
		{
		return;
		}

	pre_selected = master.options[master.selectedIndex].value;
	pre_select_by_value(f, 'to_city' + previous, pre_selected);
	}

function txt2amadeus_txt(text_input)
	{
	var newStr = "";
	var oldStr = String(text_input.value);
	var theLength = oldStr.length;
	for(pos = 0; pos < theLength; pos++)
		{
		switch( oldStr.charAt(pos) )
			{
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '0':
			newStr += ''; break;
			case ' ':
			newStr += ''; break;
			case '\'':
			case '"':
			case '��':
			newStr += ''; break;
			case '#':
			case '�':
			newStr += ' '; break;
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			newStr += 'a';  break;
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			newStr += 'e';  break;
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			newStr += 'i';  break;
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			newStr += 'o';  break;
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			case '�':
			newStr += 'u';  break;
			case '�':
			case '�':
			newStr += 'n';  break;
			default:
			newStr += oldStr.charAt(pos);
			}
		}
	//text_input.value = newStr.toUpperCase();
	text_input.value = newStr;
	return;
	}

function getDim(el)
	{
	for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
	return{x:lx,y:ly};
	}


function add_select_item(combo, item)
	{
	var i;
	if (!combo || combo.type != 'select-one')
		{
		return;
		}
	for (i=0; i<combo.options.length; i++)
		{
		if (combo.options[i].value == item.value)
			{
			combo.options.selectedIndex = i;
			return;
			}
		}
	combo.options[combo.options.length] = item;
	}

function real_add_city(codigo, nombre, num_segmentos, tipo, num)
        {
	var elem, item, i, f;

	f = eval("document.paso1");

	elem = f.otras_ciudades;
	if (elem)
                {
		if (elem.value.length == 0)
			{
			elem.value = codigo;
			}
		else
			{
			elem.value = elem.value + ',' + codigo;
			}
                }
	
	elem = f.to_city1;
	if (elem)
                {
		item = new Option(nombre, codigo);
		add_select_item(elem, item);
                }
	for (i = 2; i <= num_segmentos; i++)
		{
		elem = eval("document.paso1.from_city" + i);
		if (elem)
                	{
			item = new Option(nombre, codigo);
			add_select_item(elem, item);
                	}
		elem = eval("document.paso1.to_city" + i);
		if (elem)
                	{
			item = new Option(nombre, codigo);
			add_select_item(elem, item);
                	}
		}
	elem = f.to_city1;
	if (elem && num == 1 && tipo == 'destino')
		{
		pre_select_by_value(f, elem.name, item.value)
		pre_select_next_combo(f, elem, num);
		}
	for (i = 2; i <= num_segmentos; i++)
		{
		elem = eval("document.paso1.from_city" + i);
		if (elem && num == i && tipo == 'origen')
			{
			pre_select_by_value(f, elem.name, item.value)
			}
		elem = eval("document.paso1.to_city" + i);
		if (elem && num == i && tipo == 'destino')
			{
			pre_select_by_value(f, elem.name, item.value)
			pre_select_next_combo(f, elem, num);
			}
		}
	}

function fecha_valida_iso(fecha_iso)
	{
	var dia, mes, ano, fecha_test;

	if (fecha_iso.length != 10)
		{
		return false;
		}
		
	ano = fecha_iso.substring(0,4);
	mes = fecha_iso.substring(5,7);
	dia = fecha_iso.substring(8,10);

	// nueva validacion
	return isDate(fecha_iso);
	
	// validacion antigua
	fecha_test = new Date(ano,mes - 1, dia);
	if (dia != fecha_test.getUTCDate())
		{
		return false;
		}
	return true;
	}
	
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 * Fixed & enhanced by Horacio Gonzalez (hgonzalezl@lanchile.cl)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh = "-";
var minYear = 1901;
var maxYear = 2070;

function isInteger(s)
	{
	var i;
	for (i = 0; i < s.length; i++)
		{
     // Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9")))
			{
			return false;
			}
   	}
	// All characters are numbers.
	return true;
	}

function stripCharsInBag(s, bag)
	{
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++)
		{
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1)
			{
			returnString += c;
			}
		}
	return returnString;
	}

function daysInFebruary (year)
	{
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
	
function DaysArray(n)
	{
	for (var i = 1; i <= n; i++)
		{
		this[i] = 31;
		if (i == 4 || i == 6 || i == 9 || i == 11)
			{
			this[i] = 30;
			}
		if (i == 2)
			{
			this[i] = 29;
			}
   	}
   return this;
	}

function isDate(dtStr)
	{
	var daysInMonth = new DaysArray(12);
	var pos1 = dtStr.indexOf(dtCh);
	var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
	var strYear = dtStr.substring(0, pos1);
	var strMonth = dtStr.substring(pos1 + 1, pos2);
	var strDay = dtStr.substring(pos2 + 1);
	strYr = strYear;
	if (strDay.charAt(0) == "0" && strDay.length>1)
		{
		strDay=strDay.substring(1);
		}
	if (strMonth.charAt(0)=="0" && strMonth.length>1)
		{
		strMonth=strMonth.substring(1);
		}
	for (var i = 1; i <= 3; i++)
		{
		if (strYr.charAt(0)=="0" && strYr.length>1)
			{
			strYr=strYr.substring(1);
			}
		}
	month = parseInt(strMonth);
	day = parseInt(strDay);
	year = parseInt(strYr);
	if (pos1 == -1 || pos2 == -1)
		{
		//formato de fecha incorrecto
		return false;
		}
	if (strMonth.length < 1 || month < 1 || month > 12)
		{
		//mes invalido o fuera de rango
		return false;
		}
	if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month])
		{
		//dia invalido o fuera de rango
		return false;
		}
	if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear)
		{
		//formato de ano incorrecto o invalido o fuera de rango
		return false;
		}
	if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false)
		{
		//formato de fecha incorrecto
		return false;
		}
	return true;
}
//-->
