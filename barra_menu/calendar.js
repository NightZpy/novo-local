var current_calendar_layer;
var current_iframe_layer;
var current_calendar_image;
var current_calendar_month;
var current_calendar_year;
var current_calendar_num;
var g_fecha_min, g_fecha_max;

function activate_calendar(num, ano, mes)
	{
	var obj, dim, today;
	if (current_calendar_layer)
		{
		hide_layer(current_calendar_layer);
		DivSetinVisible();
		}

	today = new Date();
	today.setDate(today.getDate() + 14);

	if (!ano)
		{
		ano = new String(today.getFullYear ? today.getFullYear() : today.getYear());
		while (ano.length < 4)
			{
			ano = '0' + ano;
			}
		}
	if (!mes)
		{
		mes = new String(today.getMonth() + 1);
		while (mes.length < 2)
			{
			mes = '0' + mes;
			}
		}
	show_layer('layer_calendario' + num);
	obj = MM_findObj('calendario' + num);
	dim = getDim(obj);
	if (num == 1)
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x - 15, dim.y - 44);
		}
	else if (num == 2)
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x - 15, dim.y - 78);
		}
	else
		{
		dd.elements['layer_calendario' + num].moveTo(dim.x,dim.y - 40);
		}
	current_calendar_num = num;
	current_calendar_layer = 'layer_calendario' + num;
	current_calendar_image = 'imagen_calendario' + num;
	current_calendar_year = ano;
	current_calendar_month = mes;
	current_iframe_layer = 'iframelayer' + num;
	set_calendario('imagen_calendario' + num, current_calendar_month, current_calendar_year);
	}

function set_fecha_from_calendar(num, ano, mes, dia)
  {
  pre_select_calendar('fecha' + num + '_anomes', ano + '-' + mes);
  pre_select_calendar('fecha' + num + '_dia', dia);
  }

function pre_select_calendar(nombre_combo, valor)
	{
	var elemento, i;

	if (valor.length == 0)
		{
		return;
		}
	elemento = MM_findObj(nombre_combo);
	if (!elemento)
		{
		return;
		}
	if (elemento.type == 'select-one')
		{
		for (i = 0; i< elemento.options.length; i++)
			{
			if (elemento.options[i].value == valor)
				{
				elemento.options.selectedIndex = i;
				return;
				}
			}
		}
	else if (elemento.type == 'hidden' || elemento.type == 'text')
		{
		elemento.value = valor;
		}
	}

function getDim(el)
	{
	for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
	return{x:lx,y:ly};
	}

function is_explorer(version)
	{
	var nVer = navigator.appVersion;
	var nAgt = navigator.userAgent;
	var nApp = navigator.appName;

	var fullVersion = parseFloat(nVer);
	var majorVersion = parseInt(nVer);

	if(nApp != "Microsoft Internet Explorer")
		{
		return false;
		}

	if ((verOffset=nAgt.indexOf("Opera"))!=-1)
		{
		return false;
		}
	
	if ((verOffset=nAgt.indexOf("MSIE"))!=-1)
		{
		fullVersion = parseFloat(nAgt.substring(verOffset+5,nAgt.length));
		majorVersion = parseInt(''+fullVersion);
		}
	
	if(fullVersion >= version)
		{
		return true;
		}

	return false;
	}


function hide_current_calendar()
	{
	hide_layer(current_calendar_layer);
	DivSetinVisible();
	}

function DivSetVisible(num)
  {
	if(is_explorer("5.5") &&  MM_findObj('iframelayer'+num))
		{
	  MM_findObj('iframelayer'+num).style.visibility = "visible";
		}
	}

function DivSetinVisible()
  {
	if(is_explorer("5.5") &&  MM_findObj(current_iframe_layer))
		{
   	MM_findObj(current_iframe_layer).style.visibility = "hidden";
		}
  }

function selmes(x)
	{
	var m, y, mt, yt;

	m = parseInt(current_calendar_month, 10) + parseInt(x, 10);
	y = parseInt(current_calendar_year, 10);

	while (m > 12)
		{
		m -= 12;
		y += 1;
		}

	while (m < 1)
		{
		m += 12;
		y -= 1;
		}

	mt = new String(m);
	yt = new String(y);

	while (mt.length < 2)
		{
		mt = '0' + mt;
		}

	while (yt.length < 4)
		{
		yt = '0' + yt;
		}


	if ((yt + '-' + mt + '-32') < g_fecha_min)
		{
		return;
		}

	if ((yt + '-' + mt + '-01') > g_fecha_max)
		{
		return;
		}

	current_calendar_month = mt;
	current_calendar_year = yt;

	MM_swapImage(current_calendar_image,'','/images/html/calendarios/cal_' + current_calendar_month + current_calendar_year + '.png');
	}

function selday(x)
	{
	var my_date, first_week_day, days_in_month;

	my_date = new Date(current_calendar_year, current_calendar_month - 1, 1);
	first_week_day = my_date.getDay();
	if (first_week_day == 0)
		{
		first_week_day = 7;
		}
	my_date.setMonth(my_date.getMonth() + 1);
	my_date.setDate(0);
	days_in_month = my_date.getDate();

	x = x - first_week_day + 1;

	if (x > days_in_month)
		{
		return;
		}
	set_fecha_from_calendar(current_calendar_num, current_calendar_year, current_calendar_month, x);
	hide_current_calendar();
	}

function set_calendario(id_calendario, mes, ano)
	{
	MM_swapImage(id_calendario,'','/images/html/calendarios/cal_' + mes + ano + '.png');
	}

