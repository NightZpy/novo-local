<?php
/*
 * cronograma_pagos.php
 *
 * Copyright 2013 Javier Rios <javier.rios@joyasvanesaduran.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>
<style>
.ui-widget{ font-size: 0.8em;}
</style>
<div class="clear"></div>
<div id="toolbar" class="ui-widget-header ui-corner-all">
 <button id="ayuda" name="back" title="Click aqu&iacute; para volver al listado de Pagos">Volver</button>
</div><br>
<div class="clear"></div>
<div id='calendar'></div>

<script type="application/javascript">
jQuery(function(){

var fecha= new Date();
var mes=<?php echo date("m");?>;

/*---- Actions Buttons ----*/
jQuery("button:first").button({
      icons: {
        primary: "ui-icon-arrowthick-1-w"
      },
      text: true
});

jQuery("button[name='back']").click(function(){

	document.location.href="administracion.php?pagina=empl/gestor_pagos_listado.php&usuario=<?php echo $usuario;?>&sesion=<?php echo $sesion;?>";

});
/*------------------------------*/

// Calendario
jQuery('#calendar').fullCalendar({
        height : 450,
        width  : 650,
        contentHeight: 600,
        aspectRatio: 1.8,
        header: {
				left: 'today,prev,next',
				center: 'title',
				right: 'month,basicWeek,basicDay'
		},
		editable: true,
        theme: true,
        selectHelper: true,
		buttonText: {
						prev:     '&lsaquo;', // <
						next:     '&rsaquo;', // >
						prevYear: '&laquo;',  // <<
						nextYear: '&raquo;',  // >>
						today:    'Hoy',
						month:    'Mes',
						week:     'Semana',
						day:      'D�a'
				},
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesshort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
		eventClick: function(calEvent, jsEvent, view){

				jQuery.blockUI({
					theme: true,              				// true para habilitar el soporte jQuery UI CSS
					title:   "Aviso" , 								// Solo es usado cuando  theme == true
					message:  '<p>Editando el pago '+calEvent.id+'</p>',  //  Mensaje a mostrar
					timeout:   2000,
					showOverlay: false,
					centerY: 0,
					css: { top: '10px', left: '', right: '10px' }
		  });

			calEvent.title="Pago actualizado!";
			jQuery('#calendar').fullCalendar('updateEvent', calEvent);
					jQuery(this).css('border-color','#F00');
		},
		eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
					// Actualizamos la base de datos
					jQuery.post( "empl/editar_evento.php", {
						dias: dayDelta, // Dias que desplazas el evento
						id_evento: event.id // id del evento para poder buscarlo en la bbdd
				});
		},
    events: {
				url: 'empl/recuperar_pagos_con_vencimiento.php',
				type: 'POST',
				error: function(jqXHR, textStatus, errorThrown) {
					jQuery.blockUI({
						theme: true,              			// true para habilitar el soporte jQuery UI CSS
						draggable: false,   				// Opcion que hace arrastrable el dialogo requiere jquery UI
						title:   jqXHR.responseText , 		// Solo es usado cuando  theme == true
						message:  '<p>'+errorThrown+'</p>', //  Mensaje a mostrar
						timeout:   2000,
						showOverlay: false,
						centerY: 0,
						css: { top: '10px', left: '', right: '10px' }
					});
				},
				color: 'white',
				textColor: 'black'
		}
});// End fullcalendar

});// End ready
</script>
