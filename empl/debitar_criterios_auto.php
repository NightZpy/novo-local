<?
require_once "empl/clases_interfaz.php";
require_once 'clases_vd/pedido_rev.inc';

$precio_nota=0.23;
$cta="1.2.2.12";

$precio_gasto=0.50;
$cta_gasto="1.2.2.20";

// ##################################### DEBITO NOTA DE PEDIDO ############################################################ //
	//	echo "<div class='msje info'>DÉBITO NOTA DE PEDIDO</div>";
	//	echo"<input type=hidden name=step value=$step>";
		$filtro="";
		$qcamp="SELECT id_pedido,id_ped_fin,concat(id_campania,' (',fecha,')- ',obs) AS paq from campanias where id_campania='$campania'";
	//	echo $qcamp;
       	$qrycamp = mysql_db_query($c_database,$qcamp,$link);
       	$rcamp = mysql_fetch_array($qrycamp);
       	$filtro="AND ped.id_pedidos >='$rcamp[id_pedido]'";
       	if($rcamp[id_ped_fin]<>0)
         	{
          		$filtro=$filtro." AND ped.id_pedidos <= '$rcamp[id_ped_fin]'";
         	}
		$qrystr=" SELECT crit.id_pedido AS idped , SUM( crit.puntos ) AS tot, ped.id_distrib
					FROM crit_promos_puntos_log AS crit
					RIGHT JOIN pedidos_defi AS ped ON ped.id_pedidos=crit.id_pedido AND ped.id_vendedor=crit.usuario
					WHERE ped.id_distrib = '$id_distrib' AND crit.id_promo=10
						$filtro
					GROUP BY crit.id_pedido
					HAVING tot >0";
		//echo "<br />$qrystr";			
		$qry=mysql_db_query($c_database,$qrystr,$link);
		$ref="N.D.por Papelería Comercial ";
		$monto_deb=0;
		while ($row=mysql_fetch_array($qry))
		{	
			$monto_deb=$monto_deb+($precio_nota*$row[1]);
			$ref = $ref . ", $row[0]";
		//	echo "<div class='msje info'>Pedido: ".$row[0]." Puntos: ".$row[1]." Distrib: ".$row[2]." Pesos: ". $monto_deb."</div>";
			//aca revierto los puntos para que no pase de nuevo
			$qrystrr = "SELECT * FROM crit_promos_puntos_log WHERE id_pedido = '$row[0]' AND id_promo='10'";
		//	echo $qrystrr;
	           $qryr = mysql_db_query($c_database,$qrystrr,$link);
	           $cont=0;
	           while($rowr = mysql_fetch_array($qryr))
	           {
	                $pr=new PedidoRev($row[0],$rowr[usuario]);
	                $pr->do_sumarPuntos(0,array('id_promo_puntos'=>$rowr[id_promo],'cant_puntos'=>(-1*$rowr[puntos]),'tipo'=>'M'));
	           }
		}
		//aca inserto en ctacte el debito de las notas de pedido al distrib
		if($monto_deb<>0)
		{
			$qrystrmov = "SELECT MAX(id_movcon) AS maxmov FROM ctacte";
           	$qrymov = mysql_db_query($c_database,$qrystrmov ,$link);
           	$rowmov = mysql_fetch_array($qrymov);
          		$mov=$rowmov[0];
           	$mov1=$mov+1;
			$qrystr10 = " INSERT INTO ctacte (id_cod_us,id_cta,ref,debe,haber,pedido,fecha,t,fecha_gene,campania)
                					VALUES ('$id_distrib','$cta','$ref','$monto_deb','0','$mov',NOW(),'M',NOW(),$campania)";
           // echo $qrystr10;
           	$qry10 = mysql_db_query($c_database,$qrystr10,$link);
           // echo "<div class='msje info'>Movimiento: ".$mov1." Cta: ".$cta." Ref: ".$ref." Pesos: ". $monto_deb."</div>";
		   //  echo " <a target='_blank'". ayuda('Imprimir el Informe del Movimiento') ."href='empl/pdf/pdf_comprobante.php?movcon=$mov1&usuario=$usuario'><img border=0 src=images_gif/pdf_normal.gif></a>";
		}
		else 
		{
			echo "<div class='msje error'>EL DISTRIBUIDOR NO TIENE NOTAS DE PEDIDO O YA FUE GENERADO EL DEBITO</div>";
		}


// ##################################### DEBITO GASTOS ADMINISTRATIVOS #################################################### //
		echo "<div class='msje info'>DÉBITO GASTOS ADMINISTRATIVOS</div>";
		$qrystr=" SELECT crit.id_pedido AS idped , SUM( crit.puntos ) AS tot, ped.id_distrib
					FROM crit_promos_puntos_log AS crit
					RIGHT JOIN pedidos_defi AS ped ON ped.id_pedidos=crit.id_pedido AND ped.id_vendedor=crit.usuario
					WHERE ped.id_distrib = '$id_distrib' AND crit.id_promo=45
						$filtro
					GROUP BY crit.id_pedido
					HAVING tot >0";
					//echo $qrystr;
		$qry=mysql_db_query($c_database,$qrystr,$link);
		$ref="N.D.por Gasto Admin. ";
		$monto_deb=0;
		while ($row=mysql_fetch_array($qry))
		{	
			$monto_deb=$monto_deb+($precio_gasto*$row[1]);
			$ref = $ref . ", $row[0]";
			//echo "<div class='msje info'>Pedido: ".$row[0]." Puntos: ".$row[1]." Distrib: ".$row[2]." Pesos: ". $monto_deb."</div>";
			//aca revierto los puntos para que no pase de nuevo
			$qrystrr = "SELECT * FROM crit_promos_puntos_log WHERE id_pedido = '$row[0]' AND id_promo='45'";
		//echo $qrystrr;
	           $qryr = mysql_db_query($c_database,$qrystrr,$link);
	           $cont=0;
	           while($rowr = mysql_fetch_array($qryr))
	           {
	                $pr=new PedidoRev($row[0],$rowr[usuario]);
	                $pr->do_sumarPuntos(0,array('id_promo_puntos'=>$rowr[id_promo],'cant_puntos'=>(-1*$rowr[puntos]),'tipo'=>'M'));
	           }
		}
		if($monto_deb<>0)
		{
			$qrystrmov = "SELECT MAX(id_movcon) AS maxmov FROM ctacte";
           	$qrymov = mysql_db_query($c_database,$qrystrmov ,$link);
           	$rowmov = mysql_fetch_array($qrymov);
          		$mov=$rowmov[0];
           	$mov1=$mov+1;
			$qrystr10 = " INSERT INTO ctacte (id_cod_us,id_cta,ref,debe,haber,pedido,fecha,t,fecha_gene,campania)
                					VALUES ('$id_distrib','$cta','$ref','$monto_deb','0','$mov',NOW(),'M',NOW(),$campania)";
         // echo $qrystr10;
           	$qry10 = mysql_db_query($c_database,$qrystr10,$link);
           //	echo "<div class='msje info'>Movimiento: ".$mov1." Cta: ".$cta." Ref: ".$ref." Pesos: ". $monto_deb."</div>";
           //	echo " <a target='_blank'". ayuda('Imprimir el Informe del Movimiento') ."href='empl/pdf/pdf_comprobante.php?movcon=$mov1&usuario=$usuario'><img border=0 src=images_gif/pdf_normal.gif></a>";
		}
		else 
		{
			//echo "<div class='msje error'>EL DISTRIBUIDOR NO TIENE GASTOS ADMINISTRATIVOS O YA FUE GENERADO EL DEBITO</div>";
		}

?>
