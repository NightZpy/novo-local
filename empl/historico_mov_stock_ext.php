<?php
require_once "../clases/interfaz/classes_listas4.php";
require_once "clases_interfaz.php";
require_once "estetica.css";

echo "
<b>Registro Histórico de Movimientos de Stock</b>
<table border=0 width='100%'>
  <form method='POST' action='administracion.php'>
  <INPUT TYPE=hidden name=usuario  value=$usuario>
  <INPUT TYPE=hidden name=sesion  value=$sesion>
  <INPUT TYPE=hidden name=pagina  value='empl/historico_mov_stock_ext.php' >
  <tr>
    <td width='10%'><b>Id_Vd:</b>
    </td>
    <td width='90%'>
      <input type='text' name='par_id_vd' value='$par_id_vd'>
      <input type='submit' value='Aceptar' name='B1' $stylebutton>
      <input type='reset' value='Restablecer' name='B2' $stylebutton>
      <a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/historico_mov_stock_ext.php&par_id_vd=todos&orden='>
      <img border=0 align='absmiddle' src='images_gif/b_ver_todo.gif'>
      </a>
    </td>
  </tr>
  <tr width=0 bgcolor='#FF9999'>
    <td colspan=2>
    </td>
  </tr>
  </form>
</table>";
if (!isset($desde)) $desde=0;
if (!isset($cantidad_paginacion)) $cantidad_paginacion=60;

if($orden=="")
  $orden_final = " fecha_mov desc";
else
  $orden_final = $orden.", fecha_mov desc";
$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/historico_mov_stock_ext.php&par_id_vd=$par_id_vd&orden=$orden",$cantidad_paginacion);

$txt_lote = " AND (id_lote_prov=0 OR (dep_in='stk3' AND dep_out='stk_en_proceso') OR (dep_in='dep4' AND dep_out='stk_en_proceso'))";

//$txt_tipo =  " IF(id_lote_prov=0,IF(dep_out='','Ingreso','Movim.'),concat('Ingr.Lote:',id_lote_prov)) ";

$txt_tipo =  " IF(tipo=0,'Movim.',IF(tipo=1,CONCAT('Ingr.Lote:',id_lote_prov),CONCAT('Ped.Int:',id_pedido_int)))";

include "empl/historico_mov_stock.inc";

?>
