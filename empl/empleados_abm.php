<?php
?>
<script language="javascript">

function StringVaciouno(CampoNom,Campo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length==0))
  {
      alert("ERROR: Si no posee " + CampoNom + " debe seleccionar 'No Posee'.");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function ComboVaciouno(CampoNom,Campo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length==0) || (Campo.value==0))
  {
      alert("ERROR: Elija " + CampoNom + ". ");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function ValidarDatos(Form)
{ if(StringVacio("campo C�digo Usuario",Form.codempl))return false;
  if(StringVacio("campo Legajo N�",Form.codleg))return false;
  if(StringVacio("campo Cod Panel N�",Form.codpanel))return false;
  if(StringVacio("campo Fecha de Ingreso",Form.fechaing))return false;
  if (StringVacio("campo Apellido",Form.apellido)) return false;
  if (StringVacio("campo Nombres",Form.nombres)) return false;
  if (StringVacio("campo Direcci�n",Form.direccion)) return false;
  if (NumeroValido("campo Nro. Doc.",Form.nro_doc,100000,9999999999999999)) return false;
  if (StringVacio("campo Localidad",Form.localidad)) return false;
  if (StringVacio("campo C.P",Form.codpos)) return false;
  if (Form.notienete.checked==0)
   {
    if (StringVaciouno("TE",Form.telefono)) return false;
   }
  if (Form.asignafunc.checked==0){
  	
  } 
  if (Form.notienemail.checked==0)
   {
  	if (StringVaciouno("Mail",Form.mail)) return false;
   }
  	if (!MailValido("Mail",Form.mail)) return false;

 return true;
}

function MostrarMsg(text)
 {
 	alert(text);
 }

function WindowPopup(Url)
{

	msgWindows=window.open(Url, "msgPopup", "directories=no, menubar =no,status=no,toolbar=no,location=no,scrollbars=no,fullscreen=no,height=350,width=500,left=50,top=10")
}
</script>

<?php
# Seteo del titulo del Marco de Encabezado
if($accion != "A")
	$titulo="Modificaci�n de Datos del Empleado";
else
    $titulo="Alta de Nuevo Empleado";

include "clases_interfaz.php";

# Encabezado de Tabla
$marco=new Marco("tabla_bordeaux","tabla_bordeaux_","#990000");
$marco->setAnchoMarco(40);
$marco->setAlineamientoMarco("center");
$marco->setAlineamientoCelda("center");
$marco->abrirMarco("San Serif","#FFFFFF",3);
echo "<b>$titulo</b>";// Alta o Modificaci�n
$marco->cerrarMarco();

$file = "documentos/fotos/".$codempl.".jpg";  

$marco->setAnchoMarco(20);
if (!file_exists($file))
{$marco->abrirMarco();
echo"<img border='2' width='84' height='115'><font face=Arial><b>No posee Fotografia Personal</b></font></td>";
$marco->cerrarMarco();}
else
{$marco->abrirMarco();
echo"<img border='2' src='$file' width='84' height='115'></td>";
$marco->cerrarMarco();}

# Si hubo algun error con el codigo mostrar Msg.
if ($err_usu<>0)
{?>
   <script>MostrarMsg("Error: El Empleado que quiere ingresar ya fue dado de Alta.");</script>
<?}
# Si hubo algun error con el legajo mostrar Msg.
if ($err_leg<>0)
{
    echo "<font size='3' face='Arial' color='red'><b>Atenci�n: 'NO SE GUARDARON LOS DATOS'</b></font>";?>
	<script>MostrarMsg("Error: El N�mero de legajo que ingreso ya pertenece a otro Empleado.");</script>
<?}
# Si hubo algun error con el documento de acuerdo al error devuelto mostrar Msg.
if ($err_doc<>0)
{
    echo "<font size='3' face='Arial' color='red'><b>Atenci�n: 'NO SE GUARDARON LOS DATOS'</b></font>";?>
	<script>MostrarMsg("Error: El Documento ingresado ya se ha cargado para otro Empleado.");</script>
<?}

# Marco con los datos de Empleado
$marco->setDirectorio("tabla");
$marco->setNombreArchivos("tabla_");
$marco->setBgColor("#FFFDF0");
$marco->setAnchoMarco(60);
$marco->setAlineamientoMarco("center");
$marco->setAlineamientoCelda("left");
$marco->cargarImagenes();
$marco->abrirMarco("Arial","#990000",1);
?>
<FORM name="ff" ENCTYPE="multipart/form-data" action="empl/empleados_grabar.php" method="POST">
<p>
  <INPUT TYPE=hidden name=usuario value=<?php echo $usuario;?>>
  <INPUT TYPE=hidden name=sesion value=<?php echo $sesion;?>>
  <INPUT TYPE=hidden name=accion value=<?php echo $accion;?>>

  <font size="2" face="Arial"><i>Los campos marcados con un ( * ) son obligatorios</i></font><br><br>
  <!--hr color="#990000" height="0" width="73%"-->
  <font size="2" face="Arial"><b>C&oacute;digo Usuario:</b>&nbsp;
  <input type='text' name='codempl' size='20'<?php $stylefield; ?>>*<br>
  <font size="2" face="Arial"><b>Fecha de Ingreso:</b></font>&nbsp;
  <input type="text" name='fechaing' value='<?php echo $dia; ?>' size='12' onfocus='javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.ff.fechaing);return false;'><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.ff.fechaing);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a>&nbsp;*&nbsp;&nbsp;
  <font size="2" face="Arial"><b>Legajo N�:</b></font>
  <input type="text" name="codleg" size="5"<?php echo $stylefield; ?>>*<br><br>
  <font size="2" face="Arial"><b>Cod Panel N�:</b></font>
  <input type="text" name="codpanel" size="5"<?php echo $stylefield; ?>>*<br><br>
  <font size="2" face="Arial"><b>Apellidos:</b></font>&nbsp;
  <input type="text" name="apellido" size="50" <?php echo $stylefield;?>>*<br>
  <font size="2" face="Arial"><b>Nombres:</b></font>&nbsp;
  <input type="text" name="nombres" size="50" <?php echo $stylefield;?>>*<br>
  <font size="2" face="Arial"><b>Direccion:</b></font>
  <input type="text" name="direccion" size="50" <?php echo $stylefield;?>>*<br>
  <font size="2" face="Arial"><b>Tipo Doc.:</b></font>&nbsp;
  <?php poner_combo_str("tipo_doc","tipo_doc","descri_tipo_doc","tipo_doc_id"); ?>*&nbsp;&nbsp;
  <font size="2" face="Arial"><b>Nro. Doc.:</b></font>&nbsp;
  <input type="text" name="nro_doc" size="17" <?php echo $stylefield;?>>*<br><br>
  <font size="2" face="Arial"><b>Localidad</b><br>
  <input type="text" name="localidad" size="40" <?php echo $stylefield; ?>>*&nbsp;
  </font><font size="2" face="Arial"><b>C.P</b></font>&nbsp;
  <input type="text" name="codpos" size="10" <?php echo $stylefield; ?>>&nbsp;*<br>
  <font size="2" face="Arial"><b>Fecha de nacimiento</b></font><br>
  <input type="text" name='fechanac' value='<?php echo $dia; ?>' size='12' onfocus='javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.ff.fechanac);return false;'><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.ff.fechanac);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a><br>
  <font size="2" face="Arial"><b>Tel�fono</b></font>
  <input type="checkbox" name="notienete" value="1" <?php echo $stylefield; ?> onclick="javascript: if(ff.notienete.checked==1) eval(ff.telefono.value='');" >No Posee<br>
  <input type="text" name="telefono" size="20" <?php echo $stylefield; ?>onfocus="javascript: eval(ff.notienete.checked=0);"><br>
  <font size="2" face="Arial"><b>E-mail</b></font>
  <input type="checkbox" name="notienemail" value="1" <?php echo $stylefield; ?>onclick="javascript: if(ff.notienemail.checked==1) eval(ff.mail.value='');" >No Posee<br>
  <input type="text" name="mail" size="60" <?php echo $stylefield; ?> onfocus="javascript: eval(ff.notienemail.checked=0);"><br>
  <font size="2" face="Arial"><b>Imagen</b></font><br>
  <input type="file" name="foto" value="*.jpg" size="44"<?php echo $stylefield; ?>><br>
  <input type="checkbox" name="asignafunc" value="1" <?php echo $stylefield; ?>>Asignar Func. Reloj, Consulta Reloj y Cambiar Contrase&ntilde;a?<br>
  
</p>
  <p align=center>
  <input type="button" value="Aceptar" name="enviar" <?php echo $stylebutton; ?>  onClick="javascript:if (ValidarDatos(ff)) submit();">
  </p>
</form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<?
$marco->cerrarMarco();

# Si es una Modificacion...mostrar los datos
if ($accion <> "A")
  {
    $qrystr = "select u.*,r.cod_panel FROM usuario AS u, reloj_horarios AS r where u.cod_us = '$codempl' AND u.cod_us = r.cod_us";

     $qry = mysql_db_query($c_database,$qrystr ,$link);
     $row = mysql_fetch_array($qry);

	 llenar_campo("ff","codempl",$row[cod_us]);
	 llenar_campo("ff","fechaing",substr($row[fecha_alta],0,10));
	 llenar_campo("ff","codleg",$row[cod_local]);
	 llenar_campo("ff","codpanel",$row[cod_panel]);
	 llenar_campo("ff","apellido",$row[apellido]);
     llenar_campo("ff","nombres",$row[nombres]);
     llenar_campo("ff","direccion",$row[direccion]);
     llenar_campo("ff","tipo_doc",$row[tipo_doc_id]);
     llenar_campo("ff","nro_doc",$row[nro_doc]);
     llenar_campo("ff","fechanac",$row[fechanac]);
     llenar_campo("ff","localidad",$row[localidad]);
     llenar_campo("ff","codpos",$row[cp]);
     llenar_campo("ff","telefono",$row[telefono]);
     llenar_campo("ff","mail",$row[mail]);
   }
   else
   {
   	 llenar_campo("ff","apellido",$row[apellido]);
     llenar_campo("ff","nombres",$row[nombres]);
   }

# Marco para el pie de pagina
$marco->setAnchoMarco(35);
$marco->setalineamientoMarco("center");
$marco->setalineamientoCelda("center");
$marco->abrirMarco("","","");
echo "<a href='administracion.php?pagina=empl/func_emp_lista.php&usuario=$usuario&sesion=$sesion'><img border=0 src=$bpinicio></a>
	  <a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/arbol_lista_emp.php'><img border=0 src=$bpvolver></a>";
$marco->cerrarMarco();
?>
