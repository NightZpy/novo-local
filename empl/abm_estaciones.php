<div id="tabs">
	<ul>
    <li><a href="#est">Gesti&oacute;n de Estaciones</a></li>
    <li><a href="#listas">Gesti&oacute;n de Posiciones en Listas</a></li>
 </ul>

<div id="est">
	<table id="estaciones" ></table>
	<div id="pager_est" ></div>
</div>

<div id="listas">
	<div>
	<form id="frm_lista" method="post" name="frm_lista" accept-charset="utf-8">
	 <div>
      <label class="ui-state-highlight pagos">( Ingrese Parte del Nombre o ID de la Lista de Precios )</label>
          <input id="lista" type="search" name="id_lista" required />
          <input id="buscador" type="submit" name="update" value="Actualizar" />
  </div>
	</form>
	</div>
	<table id="listas_precios"></table>
	<div id="pager_listas" ></div>
</div>
</div>
<div class="clearfix">&nbsp;</div>

<div>
<span><a id="ayuda" class="btn center" href="	administracion.php?pagina=empl/func_emp_lista.php&usuario=<?php echo $usuario;?>&sesion=<?php echo $sesion;?>" title="Click aqu&iacute; para volver al men&uacute; principal">Men&uacute; Principal</a></span>
</div>
<script type="text/javascript">
//<![CDATA[
jQuery(function() {

	/* Var Obligatorias
	--------------------*/
	var empleado="<?php echo $usuario;?>";
	var emp_ath=["JRIOS","LUCIANAV","ROXANAC","MATIASS","MARCOS","MARTINC"]
	var pos=emp_ath.indexOf(empleado);
	var idlista=0;
	var usuario="<?php echo $usuario;?>";

		// TABS Settings ************************
		jQuery("#tabs").tabs();
		if(pos==-1){
			jQuery("#tabs").tabs({disabled:[0]});
			jQuery("#tabs").tabs({active: 1});
		}
		//***************************************

		// Search Button Settings
    jQuery('#buscador').button();
		// Button search action
		jQuery("#buscador").click(function(event){
				event.preventDefault();
				idlista	=jQuery("#lista").val();
				jQuery("#listas_precios").jqGrid('setGridParam',{postData:{ idlista:idlista}}).trigger('reloadGrid');
		});

		// ABM Estaciones
    jQuery("#estaciones").jqGrid({
				url:'empl/listar_estaciones.php',
				datatype: "json",
				shrinkToFit: true,
				colNames:['Nombre', 'Posici&oacute;n', 'Hilo','Estado'],
				colModel:[
									{name:'est_nombre',index:'est_nombre', width:90,sorttype:'string',search:true,editable:true,editoptions:{size:10,maxlength:2},editrules:{ required:true}},
									{name:'est_pos',index:'est_pos', width:90,sorttype:'integer',search:true,editable:true,editoptions:{size:10,maxlength:3},editrules:{ required:true}},
									{name:'est_hilo',index:'est_hilo', width:90, align:"center",search:true,editable:true,editoptions:{size:10,maxlength:4},editrules:{ required:true}},
									{name:'est_estado',index:'est_estado', width:110, align:"left",search:false,editable:true,edittype:"select",editoptions:{value:'Disponible:Disponible;En Preparacion:En Preparacion;En Espera:En Espera;Inhabilitada:Inhabilitada',defaultValue: 'Disponible',size:1},editrules:{ required:true}}
				],
				height:'auto',
				width:700,
				rowNum:20,
				rowList:[10,20,30,50,70],
				pager: '#pager_est',
				sortname: 'est_nombre',
				viewrecords: true,
				sortorder: "asc",
				caption:"Administraci&oacute;n de Estaciones",
				editurl:"empl/abm_estaciones_grabar.php"
		});
	jQuery("#estaciones").jqGrid('navGrid','#pager_est',
		{del:false,search:false},//Gral options
		{
			closeAfterEdit:true,afterSubmit: function(response) {
					jQuery.blockUI({theme: true,title: "Info",message:"<p>"+response.responseText+"</p>",timeout:3000});
					return [true,"",""];
       }
    },// Edit options
		{
			height:270,
			widht:350,
			addCaption:"Alta de Nueva Estaci&oacute;n",
			bSubmit:"Guardar",
			bCancel:"Cancelar",
			processData:"Procesando...",
			checkOnUpdate:true,
			topinfo: "Complete todos los campos.",
			closeAfterAdd:true,
			reloadAfterSubmit:true,
			afterSubmit: function(response) {
					jQuery.blockUI({theme: true,title: "Info",message:"<p>"+response.responseText+"</p>",timeout:3000});
					return [true,"",""];
       }
		},// add options
		{},// Del options
		{clearSearch:true}// Search options
	);
	jQuery("#estaciones").jqGrid('filterToolbar',{stringResult: false,searchOnEnter: false,enableClear: true});

// Listas de precios productos
jQuery("#listas_precios").jqGrid({
				url:'empl/listar_listas_precios.php',
				datatype: "json",
				data:{ idlista: (jQuery("#lista").val()>0)?jQuery("#lista").val():idlista},
				colNames:['IDVD','Descripci&oacute;n','Posici&oacute;n'],
				colModel:[
							{name:'id_vd',index:'id_vd', width:40,align:"left",sorttype:'string',search:true,editable:false},
							{name:'nombreproducto',index:'nombreproducto', width:130,align:"left",sorttype:'string',search:false,editable:false},
							{name:'posicion',index:'posicion', width:40,align:"left",sorttype:'string',search:false,editable:true,edittype: 'select',editoptions:{dataUrl: 'empl/mostrar_estaciones_edit.php',size:1,maxlength:5}}
				],
				height:'auto',
				width:700,
				rowNum:20,
				rowList:[10,20,30,50,70],
				pager: '#pager_listas',
				sortname: 'id_vd',
				viewrecords: true,
				sortorder: "asc",
				caption:"Administraci&oacute;n de Posiciones en Lista de Precios",
				forceFit : true,
				cellEdit: true,
				cellsubmit: 'remote',
				cellurl:"empl/cambiar_pos_lista_grabar.php",
				beforeSubmitCell:function(rowid,celname,value,iRow,iCol){
					//var IDLista=(jQuery("#lista").val()>0)?jQuery("#lista").val():0;
					return {idlista:idlista,usu:usuario};
				},
				afterSubmitCell: function(serverStatus, aPostData){
				var response=serverStatus.responseText;
				console.log("Respuesta del Server: "+response+"\nData: "+aPostData);
				jQuery.blockUI({theme:true,title:"Info",message:response,timeout:3000});
				jQuery("#listas_precios").jqGrid('setGridParam',{postData:{ idlista:idlista}}).trigger('reloadGrid');

		return [true];

	}
		});
	jQuery("#listas_precios").jqGrid('navGrid','#pager_listas',{del:false,search:false,add:false,edit:false});
	jQuery("#listas_precios").filterToolbar({searchOnEnter:false});
});
//]]>
</script>
