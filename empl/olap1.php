<?php
include "../conexion.php";
require_once "../../clases/interfaz/classes_listas4.php";
require_once "clases_interfaz.php";
require_once "../estetica.css";


//##########################################################################################################
class ListasImp extends Listas
{
 //M�todo Abstracto a implementar en la clase hija.
 function OnEnterCell(&$contenido,&$columna,&$color_fondo,&$color_texto,&$resalte,&$fin_resalte,&$tipo_fila,&$clase)
  {
   global $offv;
   global $offh;
   if($this->columna<$offh)  //Si es un grabado tomado el Valor del checkbox viene vac�o.
     $clase='class=listtitle';                                         //En consecuencia NO DEBO MOSTRAR EL CHECKBOX
   if($this->fila<$offv)  //Si es un grabado tomado el Valor del checkbox viene vac�o.
     $clase='class=listtitle';                                         //En consecuencia NO DEBO MOSTRAR EL CHECKBOX
  }

function BasicTable($data,$tot=0)
{
 global $spanes;
 $span=0;
    $this->fila =0;
    $this->columna =0;
	//Datos
    $totales=array();
    if ($this->debug) {echo "<BR><font color=#FFFFFF>BasicTable: Param:<BR> Tabla:";
                       print_r($data);
                       echo"</font><BR><BR>";}
//    $this->html .="<table width=".$this->t_ancho." border=".$this->t_borde." bgcolor='".$this->t_color."' bordercolor='".$this->t_b_color."' >\n";
    $this->html .="<table width=".$this->t_ancho." class=list >\n";

    //Muestra el encabezado si no est� vac�o.
    $i = 0;
    if(sizeof($this->titulos)>0)
     {
      $this->html .="<tr>\n";

      foreach($this->titulos as $col)
       {
        if($this->mostrarChecks and $i==0) //Si debe mostrar checkboxes los muestra ac�.
          $this->Celda("<INPUT TYPE=checkbox NAME='". $this->checkTitulo ."' ". $this->eventosCheckTitulo .">",$i,$this->c_b_color_titulo,$this->l_color_titulo,"<b>","</b>","t�tulo","class=listtitle");
        elseif(!in_array($this->titulos[$i],$this->camposHidden))
          $this->Celda($col,$i,$this->c_b_color_titulo,$this->l_color_titulo,"<b>","</b>","t�tulo","class=listtitle");
        $i++;
       }
      $this->html .="</tr>\n";
     }
  //    $this->MuestraLinea();
	//Empieza a mostrar la tabla de Datos.
    $this->fila=0;
    foreach($data as $row)
    {
     $this->OnEnterRow($row);
     $i=0;
     $this->columna=0;
     $this->html .="<tr>\n";

     if($this->usarGrupos)
      {
       if($this->CambioGrupo($row)) //Si Cambia el grupo, mostrar una celda del ancho de la tabla.
        {
         $this->html .="<td class=group colspan=". (sizeof($row) - sizeof($this->camposHidden)) .">". $this->grupoActual ."</td>";
         $this->html .="</tr>\n";
         $this->html .="<tr>\n";
        }
      }
     foreach($row as $col)
      {
       // ***** C�lculo de Totales ***** //
       if (sizeof($this->total<>0))
        {
         switch ($this->total[$i])
          {
           case 1:
             $this->totales[$i]=$this->totales[$i]+$col;
             break;
           case 2:
             $this->totales[$i]=$this->totales[$i]+1;
             break;
           case 3:
             $this->totales[$i]=$col;
             break;
           case 4:
             $this->totales[$i]=$this->totales[$i]+$col;
             $col=$this->totales[$i];
           break;
          }
        }
       // ***** Fin C�lculo de Totales ***** //

			 if($this->fila % 2 ==0)
			   $clase="class=listwrap";
			 else
			   $clase="class=listwrap2";

       if($this->mostrarChecks and $i==0)  //Si debe mostrar checkboxes los muestra ac�.
	       $this->Celda("<INPUT TYPE=checkbox VALUE='". $col ."' NAME='". $this->checkItem . ($this->fila + 1) ."' >",$i,"","","","","",$clase);
			 elseif(!in_array($this->titulos[$i],$this->camposHidden))
			  {
         $pone_span="";
         $f=$this->fila;
         $c=$this->columna;
         if((1*$spanes[$f][$c])<>0)
          {
           $span = $spanes[$f][$c]+1;
           $pone_span=" colspan=$span ";
           $this->Celda($col,$i,"","","","","",$clase,$pone_span);
          }
           
         if($span==0)
           $this->Celda($col,$i,"","","","","",$clase,$pone_span);
         else
           $span--;  
        }
       $i+=1;
       $this->columna++;
      }
     $this->html .="</tr>\n";
     $this->fila++;
     }

    // ***** Muestra Totales ***** //
    //$clase="class=group";
    $this->html .="<tr>\n";
    if (sizeof($this->total<>0))
     {
      for($i=0;$i<count($this->titulos);$i++)
       {
        if(!in_array($this->titulos[$i],$this->camposHidden))
         {
          switch ($this->total[$i])
           {
            case 1:
              $this->Celda($this->totales[$i],$i,'','','<b>','</b>','total',$clase);
              break;
            case 2:
              $this->Celda($this->totales[$i],$i,'','','<b>','</b>','total',$clase);
              break;
            case 4:
              $this->Celda($col,$i,'','','<b>','</b>','total',$clase);
              break;
            default:
              $this->Celda($this->total[$i],$i,'','','<b>','</b>','total',$clase);
            break;
           }
         }
       }
     $this->html .="</tr>\n";
     }
    // ***** Fin Muestra Totales ***** //

     $this->html .="</table>\n";
}


function Celda($contenido,$columna,$color_fondo="",$color_texto="",$resalte="",$fin_resalte="",$tipo_fila="normal",$clase="",$pone_span="")
{
	  $this->valor = $contenido;
	  //Invoca al M�todo/Evento OnEnterCell;
	  $this->OnEnterCell($contenido,$columna,$color_fondo,$color_texto,$resalte,$fin_resalte,$tipo_fila,$clase);

      if($color_fondo=="")
        $color_fondo = $this->c_b_color;
      if($color_texto=="")
        $color_texto = $this->l_color;

      if(!$this->soloClase)    //S�lo usa las opciones de formato de la clase.
       {
        $inicio_formatos = "<font bgcolor='".$color_fondo."' face='".$this->l_font."' size='".$this->l_size."' color='".$color_texto."'>";
        $fin_formatos = "</font>";
       }

      //Pregunta si es un link.
      if (($this->align[$columna]=='Link' or $this->align[$columna]=='LinkN') and $tipo_fila=='')
       {
        if($this->align[$columna]=='LinkN')//�Es un link a mostrarse en una nueva ventana?
          $target=" target='_blank'";
        else
          $target='';
          $links = explode("::", $contenido);
          $this->html .="<td width='".$this->porc[$columna]."%' align='".$this->align[$columna]."' $clase >\n";
          $this->html .="$resalte $inicio_formatos <a $target href='$links[1]'>$links[0]</a>$fin_formatos $fin_resalte";
//          $this->html .="<font face='".$this->l_font."' size='".$this->l_size."' color='".$this->l_color."'><a $target href='$links[1]'>$links[0]</a></font>";
          $this->html .="</td>\n";
         }
       else    //NO es un Link
        {
         $this->html .="<td $pone_span bgcolor='".$color_fondo."' width='".$this->porc[$columna]."%' align='".$this->align[$columna]."' $clase>\n";
         $this->html .="$resalte $inicio_formatos $contenido $fin_formatos $fin_resalte";
         $this->html .="</td>\n";
        }
}
}
//##########################################################################################################




if($qrystr=='')
 {
  $qrystr = "SELECT pd.id_regional, u.provincia,pd.obs, sum(total) AS pvp,sum(un_com_s) AS com
             FROM pedidos_defi AS pd
               INNER JOIN usuario AS u ON pd.id_vendedor=u.cod_us
             WHERE id_pedidos>=131293  	
               AND id_pedidos<=133977 
             GROUP BY pd.id_regional,u.provincia,pd.obs  
             WITH ROLLUP
            ";
 }
//echo $qrystr;
$qry = mysql_db_query($c_database,$qrystr,$link);
echo mysql_error();
$mat=array();
$elem_dim_v=array();
$elem_dim_h=array();
if($dimv=='')
  $dimv=array('id_regional');      //Dimensiones Horizontales.
if($dimh=='')
  $dimh=array('obs','provincia');  //Dimensiones Verticales.
if($medidas=='')
  $medidas=array('pvp');           //Medidas a usar.
$comb_pos_v=array();         //Combinaciones posibles de elementos de las dimensiones verticales.
$comb_pos_h=array();         //Combinaciones posibles de elementos de las dimensiones horizontales.

//Extraigo las filas y se acomodan en un array autoasociativo.
while($row = mysql_fetch_array($qry))
 {
  //Para cada dimensi�n vertical, obligo a que los campos sean [NULL] o [En Blanco] seg�n corresponda.
  foreach($dimv as $dvi) 
   {
    if(is_null($row[$dvi]))
      $row[$dvi]='�[TODO]';//echo "nulo";
    elseif($row[$dvi]=='')
      $row[$dvi]='[En Blanco]';//echo "en blanco";}
   }
  //Para cada dimensi�n horizontal, obligo a que los campos sean [NULL] o [En Blanco] seg�n corresponda.
  foreach($dimh as $dhi) 
   {
    if(is_null($row[$dhi]))
      $row[$dhi]='�[TODO]';//echo "nulo";
    elseif($row[$dhi]=='')
      $row[$dhi]='[En Blanco]';//echo "en blanco";}
   }

  //Para cada dimensi�n vertical, inserto el elemento de dimensi�n de la fila actual.
  foreach($dimv as $dvi) 
   {
    if(!$elem_dim_v[$dvi])
      $elem_dim_v[$dvi]=array();
    if(!in_array($row[$dvi],$elem_dim_v[$dvi]))
      $elem_dim_v[$dvi][]=$row[$dvi];
   }
  //Para cada dimensi�n horizontal, inserto el elemento de dimensi�n de la fila actual.
  foreach($dimh as $dhi) 
   {
    if(!$elem_dim_h[$dhi])
      $elem_dim_h[$dhi]=array();
    if(!in_array($row[$dhi],$elem_dim_h[$dhi]))
      $elem_dim_h[$dhi][]=$row[$dhi];
   }
  //$mat[$dim1][$dim2]=$row['pvp'];

  //Construir la matriz de datos.
  $t='';
  foreach($dimv as $dvi) //Agrega una dimensi�n por cada dimensi�n vertical.
  	$t .="['" . $row[$dvi] . "']";
  foreach($dimh as $dhi) //Agrega una dimensi�n por cada dimensi�n horizontal.
  	$t .="['" . $row[$dhi] . "']";
	$t = '$mat' . $t . "=number_format(" . $row[$medidas[0]] . ",2);";  //Arma la instrucci�n de asignaci�n del valor de la celda.
	//echo "$t<br>";
	eval($t);                          //Ejecuta la instrucci�n de asignaci�n del valor de la celda.
  //El select no totaliza por la primera dimensi�n=>totalizo a mano. (Totales de columna).
  //$tot[$dim2]+=$row['pvp'];
 }


foreach($dimv as $dvi) 
  sort($elem_dim_v[$dvi]);
foreach($dimh as $dhi) 
  sort($elem_dim_h[$dhi]);

$combv=array();//Posibles combinaciones de elementos de las dimensiones verticales.
$combh=array();//Posibles combinaciones de elementos de las dimensiones horizontales.
$camino=array();//Camino hasta cad hoja del �rbol.
//Obtener todas las combinaciones de dimensiones verticales posibles.
arma_arbol(-1,'v');
//Obtener todas las combinaciones de dimensiones horizontales posibles.
arma_arbol(-1,'h');
//var_dump($comb);

$offv=sizeof($dimh);//Calculo el offset vertical para la zona de datos.
$offh=sizeof($dimv);//Calculo el offset horizontal para la zona de datos.
//Setear en algo ('') a las celdas de la esquina del offset.
for($i=0;$i<$offv;$i++)
  for($j=0;$j<$offh;$j++)
    $data[$i][$j]='';

//Imprime los t�tulos de cada elemento de la dimensi�n horizontal.
for($i=0;$i<sizeof($combh);$i++)
 {
  for($j=0;$j<sizeof($combh[$i]);$j++)
    $data[$j][$i+$offh]=$combh[$i][$j];//Transpone i y j en $data.
 }

//Para cada combinaci�n de dimensiones verticales imprimir los t�tulos de las dimv corresp y la fila de datos.
for($i=0;$i<sizeof($combv);$i++)
 {
  //Imprimir los t�tulos de las dimv corresp
  for($j=0;$j<sizeof($combv[$i]);$j++)
    $data[$i+$offv][$j]=$combv[$i][$j];//Transpone i y j en $data.
    
  //Imprimir la fila de datos.  
  for($j=0;$j<sizeof($combh);$j++)
   {
    //Buscar el elemento de la matriz de datos.
    $t='';
    for($k=0;$k<sizeof($combv[$i]);$k++)
    	$t .="['" . $combv[$i][$k] . "']";
    for($k=0;$k<sizeof($combh[$j]);$k++)
    	$t .="['" . $combh[$j][$k] . "']";
  	$t = '$data[' . ($i+$offv) . '][' . ($j+$offh) . ']=' . '$mat' . $t . ";";  //Arma la instrucci�n de asignaci�n del valor de la celda.
  	//echo "$t<br>";
  	eval($t);                          //Ejecuta la instrucci�n de asignaci�n del valor de la celda.
   }
 }

$ultimo='inicio de spanes';
$spanes=array();
//Obtener las columnas en los t�tulos para hacer colspan.
for($i=0;$i<sizeof($dimh);$i++)
  for($j=0;$j<sizeof($data[$i]);$j++)
    if($data[$i][$j]==$ultimo)
     {
      $spanes[$i][$colu_span_actual]=$spanes[$i][$colu_span_actual]+1;
     }
    else
     {
      $ultimo=$data[$i][$j];
      $colu_span_actual=$j;
     } 
//var_dump($spanes);
echo "<b>Dimensiones Verticales: </b>" . implode(',',$dimv);
echo "<br>";
echo "<b>Dimensiones Horizontales: </b>" . implode(',',$dimh);
echo "<br>";

// **************************************************************** //
$lista = new ListasImp();
//$lista->debug=1;
$tit=array();
$lista->titulos=array_pad ($tit, sizeof($combh)+$offh, '');
//array("id_lote","n_ctrl","n_fac","Proveedor","Generado","Usuario","Recibido","Mod.","+Mov","Ver","pdf","Obs");
$lista->AlineacionColumnas(array_pad ($tit, sizeof($combh)+$offh, 'R'));
//$lista->MuestraTabla($qrystr);
$lista->BasicTable($data);
$lista->OutPut();

// ********************************************************************************************* //
// ********************************************************************************************* //

function arma_arbol($dim,$orient)
{
 global ${'elem_dim_'.$orient};
 global ${'comb'.$orient};
 global ${'dim'.$orient};
 global $camino;

 if($dim==sizeof(${'dim'.$orient})-1)
  {
   ${'comb'.$orient}[]=$camino;
   array_pop($camino);
   return;
  }
 
 //Buscar hijos del nodo actual(son todos los elem del array de dimension[$dim+1])
 $hijos=${'elem_dim_'.$orient}[${'dim'.$orient}[$dim+1]];
 
 //Para cada hijo
 $off=0;    
 while($off<sizeof($hijos))
  {
   array_push($camino,$hijos[$off]);
   arma_arbol($dim+1,$orient);
   $off++;
  }
 array_pop($camino);
}
?>
