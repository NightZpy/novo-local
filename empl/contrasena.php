<html>
	<head>
		<meta http-equiv='content-type' content='text/html;charset=utf-8'>
		<title>Vanes Duran, Joyas</title>
		<link href='vanesa12.css' rel='stylesheet' media='screen'>
		<script type="application/javascript">
     function InvalidMsg(textbox) {

     if(textbox.validity.patternMismatch){
        textbox.setCustomValidity('Debe tener al menos 8 caracteres, ser Alfanumerica, tener al menos una letra Mayuscula y NO repetir parte o todos los caracteres de una anterior.');
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}
			$(function(){
					$("#clave").submit(function(){
						var psw0,psw1,psw2;
						psw0=$("input[name='password0']").val();
						psw1=$("input[name='password1']").val();
						psw2=$("input[name='password2']").val();

					if(psw0.length==0 || psw1.length==0 || psw2.length==0){
						alert("Los campos no pueden estar vacios!");
						return false;
					}
					else if(psw1.length<8 || psw2.length<8){
						alert("La cantidad de caracteres no puede ser inferior a 8!");
						return false;
					}
					else if(psw1!=psw2){
						alert("Las contraseñas nuevas no coinciden!, corrija este error por favor.");
						return false;
					}
					else if(psw1==psw0){
						alert("La contraseña nueva es igual a la anterior, por favor cambiela!");
						return false;
					}
					else { return true;}
				});

		});//End ready

function capLock(e){
  kc=e.keyCode?e.keyCode:e.which;
  sk=e.shiftKey?e.shiftKey:((kc==16)?true:false);
  if(((kc>=65&&kc<=90)&&!sk)||((kc>=97&&kc<=122)&&sk))
    document.getElementById('caplock').style.visibility = 'visible';
  else document.getElementById('caplock').style.visibility = 'hidden';
}
</script>
	</head>
	<body>
		<div align='center' style='width: 762; height: 317'>
			<br>
			<table border=0 cellpadding=0 cellspacing=0 bgcolor='#fffdf0'>
				<tr height='19'>
					<td width='23' height='19'><img src='tabla/tabla_01.gif' width=23 height=19 alt=''></td>
					<td height='19' background='tabla/tabla_02.gif'><font size='-7'>&nbsp;&nbsp;</font></td>
					<td width='22' height='19'><img src='tabla/tabla_03.gif' width=22 height=19 alt=''></td>
				</tr>
				<tr>
					<td width='23' background='tabla/tabla_04.gif'><br>
					</td>
					<td valign='top' bgcolor=#FFFDF0>
						<div align='center' style='width: 395; height: 263'>
							Para Cambiar su contrase&ntilde;a en el primer casillero
                            ingrese la&nbsp;
							<br>
							contrase&ntilde;a actual y en los dos casilleros restantes
                            la nueva<br>IMPORTANTE: Recuerde que el sistema respeta mayusculas y minusculas,<br>
                            la nueva contrase&ntilde;a NO puede ser igual a alguna de las anteriores.

<form id="clave" name="ff" ENCTYPE=multipart/form-data action='administracion.php?pagina=empl/cambia_contras.php' method='post' >
						  <INPUT TYPE=hidden name=usuario value=<?php echo $_REQUEST['usuario']?>>
                              <INPUT TYPE=hidden name=sesion value=<?php echo $_REQUEST['sesion']?>>
                            	<div align='center'>
						 			<TABLE WIDTH=294 BORDER=0 CELLPADDING=0 CELLSPACING=0>
										<TR height='19'>
											<TD width='23' height='19'><IMG SRC='tabla_bordeaux/tabla_bordeaux_01.gif' WIDTH=23 HEIGHT=19 ALT=''></TD>
											<TD height='19' background='tabla_bordeaux/tabla_bordeaux_02.gif' width='249'><br>
											</TD>
											<TD width='22' height='19'><IMG SRC='tabla_bordeaux/tabla_bordeaux_03.gif' WIDTH=22 HEIGHT=19 ALT=''></TD>
										</TR>
										<TR>
											<TD width='23' background='tabla_bordeaux/tabla_bordeaux_04.gif'><br>
											</TD>
											<TD BGCOLOR=#990000 width='249'>
												<div align='center' style='width: 306; height: 161'>
													<table border='0' cellspacing='2' cellpadding='0'>
														<tr>
															<td class='cremita' align='right' valign='top'>
                                                            Contrase&ntilde;a actual</td>
															<td width='10'>&nbsp;</td>
															<td>
                                                            <input type='password' name='password0' size='16' border='0'></td>
														</tr>
														<tr>
															<td class='cremita' align='right' valign='top'>&nbsp;</td>
															<td width='10'>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td class='cremita' align='right' valign='top'>
                                                            Nueva Contrase&ntilde;a:</td>
															<td width='10'></td>
															<td><input type='password' name='password1' pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" oninvalid="InvalidMsg(this);" size='16' oninput="InvalidMsg(this);" onKeyPress="capLock(event)" border='0'></td>
                                                                                                                        <!--<td><input type='text' name='password1' pattern="(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" oninvalid="setCustomValidity('Debe ser Alfanumerica y tener al menos una letra Mayuscula')" title="Debe ser alfanumerica sin caracteres especiales" size='16' border='0'></td>-->
														</tr>
														<tr>
															<td class='cremita' align='right' valign='top'>
                                                            Repita nueva Contrase&ntilde;a:<br>
															</td>
															<td width='10'></td>
															<td><input type='password' name='password2' pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" oninvalid="InvalidMsg(this);"  size='16' oninput="InvalidMsg(this);" onKeyPress="capLock(event)"border='0'></td>
														</tr>
														<tr>
                                                                                                                    <td align='left'><div id="caplock" style="visibility:hidden;background-color:#E6E6E6"> <img width="16" height="16" alt="May&uacute;scula Activada!" id="pwcapsicon" src="empl/img/warning16.png">May&uacute;scula Activada!</div></td>
															<td width='10'></td>
															<td><br>
																<table border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td><input type='submit' name='submitButtonName' value='Cambiar' border='0'></td>
																		<td width='10'></td>
																		<td><input type='reset' value='Borrar' border='0'></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br>
													<span class='cremita'>
                                                    Mantenga su contrase&ntilde;a en un
                                                    lugar seguro !<br>
													</span>

                                                                                                </div>
											</TD>
											<TD width='22' background='tabla_bordeaux/tabla_bordeaux_06.gif'><br>
											</TD>
										</TR>
										<TR height='22'>
											<TD width='23' height='22'><IMG SRC='tabla_bordeaux/tabla_bordeaux_07.gif' WIDTH=23 HEIGHT=22 ALT=''></TD>
											<TD height='22' background='tabla_bordeaux/tabla_bordeaux_08.gif' width='249'><br>
											</TD>
											<TD width='22' height='22'><IMG SRC='tabla_bordeaux/tabla_bordeaux_09.gif' WIDTH=22 HEIGHT=22 ALT=''></TD>
										</TR>
									</TABLE>
								</div>
							</form>
						</div>
					</td>
					<td bgcolor='#fffdf0' width='22' background='tabla/tabla_06.gif'><br>
					</td>
				</tr>
				<tr height='22'>
					<td width='23' height='22'><img src='tabla/tabla_07.gif' width=23 height=22 alt=''></td>
					<td height='22' background='tabla/tabla_08.gif'><br>
					</td>
					<td width='22' height='22'><img src='tabla/tabla_09.gif' width=22 height=22 alt=''></td>
				</tr>
			</table>
		</div>
		<?include("empl/menu_pedidos_data_n1.php");?>
	</body>
</html>
