<?php
//$link = mysql_connect($c_conexion,$c_usuario,$c_password);
//$dias=4;

/*$sale_de = "stk_en_transito";
$entra_a = "";
$mostrar_id_prod_prov = true;
*/
require_once "../clases/interfaz/classes_listas4.php";
//require_once "empl/clases_interfaz.php";
require_once "estetica.css";

$color_sel = '#CCFFCC';
$color_no_sel = '#FFFFFF';
$color_cant_orig = array();
$colores["rojo"]["color"] = '#EEDBD7';
//$colores["verde"]["color"] = '#E0EEE0';
$colores["amarillo"]["color"] = '#FFFFF0';
//$colores["azul"]["color"] = '#FF9FFF';
$colores["rojo"]["ref"] = "El Producto NO existe en la Lista de Precios elegida.";
//$colores["verde"]["ref"] = "Nada se ha transferido al destino de lo ingresado en el origen.";
$colores["amarillo"]["ref"] = "El Producto YA TIENE UN PRECIO en la Lista de Precios elegida.";
//$colores["azul"]["ref"] = "Se ha transferido al destino una cantidad MAYOR del total de lo ingresado en el origen. (No se registrar�n movimientos negativos)";

$qrystr_atr = "select id_atrib,abrev,descri from atributos order by  id_atrib";
$qry_atr = mysql_db_query($c_database,$qrystr_atr ,$link);
$arr_atrib = array();
while ($row_atr = mysql_fetch_array($qry_atr))
  $arr_atrib[]=array('id_atrib'=>$row_atr['id_atrib'],'descri'=>$row_atr['id_atrib'].'-'.$row_atr['abrev'].'-'.$row_atr['descri']);
function poner_combo_atrib($nombre,$valor_elegido)
 {
  global $stylecombo;
  global $arr_atrib;

  if($valor_elegido==0)
  {
    $sel = "selected";
  }
  else
  {
    $sel = "";
  }

  $buf='';
  $buf .= "<select size='1' name=" . $nombre . " " . $stylecombo . ">";
  $buf.="<option $sel value=0>Auto</option>";
  foreach($arr_atrib AS $atr)
   {
    if($valor_elegido==$atr['id_atrib'])
      $sel = "selected";
    else
      $sel = "";
    $buf .= "<option $sel value=$atr[id_atrib]>$atr[descri]</option>";
   }
  $buf .= "</select>";
  return $buf;
 }

/**
 * Nueva implementaci�n para dar soporte a la ubicaci�n en cada estaci�n de cada producto
 *@author Rios Javier - 2014-05-14
 * */

$qryUbica="SELECT id_estacion,est_nombre,est_pos,est_hilo FROM estaciones ORDER BY est_hilo ASC";

$resUbi = mysql_db_query($c_database,$qryUbica ,$link);
$arr_ubica = array();
while ($rowUbi = mysql_fetch_array($resUbi)){
	$pos_completa=cambia_pos_estacion($rowUbi['est_pos']);// La posicion se pasa a string y devuelve rellenado con ceros si hace falta
	// Concatenar la pos y nombre de la estaci�n
	$estacion=$rowUbi['est_nombre'].$pos_completa;
  $arr_ubica[]=array('id_estacion'=>$rowUbi['id_estacion'],'ubicacion'=>$estacion);
}  //var_dump($arr_ubica);
function poner_combo_ubicacion($nombre,$valor_elegido)
 {
  global $stylecombo;
  global $arr_ubica;

  if($valor_elegido==0)
  {
    $sel = "selected";
  }
  else
  {
    $sel = "";
  }

  $buff='';
  $buff .= "<select size='1' name=" . $nombre . " " . $stylecombo . ">";
  $buff.="<option $sel value=0>Sin Ubicaci�n</option>";
  foreach($arr_ubica AS $ubi)
   {
    if($valor_elegido==$ubi['id_estacion'])
      $sel = "selected";
    else
      $sel = "";
    $buff .= "<option $sel value=$ubi[id_estacion]>$ubi[ubicacion]</option>";
   }
  $buff .= "</select>";
  return $buff;
 }
// ++++++++++++++++++++++++ End ubicaci�n por estaci�n +++++++++++++++++++++++++++++++++++


class ListasImp extends Listas
{
 function OnEnterRow(&$parFila)
 {
  global $colores;
  global $color_cant_orig;

  if($parFila[1]=='')
    $color_cant_orig[2] = $colores["rojo"]["color"];
  else
    $color_cant_orig[2] = $colores["amarillo"]["color"];

  $this->soloClase = false;

  $parFila[1]="<input align=right size=6 type=text value='" . $parFila[1] . "' name=prod" . ($this->fila + 1). ">";
  $parFila[3]=poner_combo_ubicacion("ubica". ($this->fila + 1), $parFila[3]);// Ubicaci�n en la estaci�n
  $parFila[4]=poner_combo_atrib("atrib" . ($this->fila + 1) ,$parFila[4]);// Atributo Comercial
 }

 //M�todo Abstracto implementado en la clase hija.
 function OnEnterCell(&$contenido,&$columna,&$color_fondo,&$color_texto,&$resalte,&$fin_resalte,&$tipo_fila,&$clase)
  {
   global $color_sel;
   global $color_no_sel;
   global $color_cant_orig;
   global $colores;
   $contenido = str_replace("TYPE=checkbox VALUE","TYPE=checkbox ONCLICK=\"if(this.checked==true) prod" . ($this->fila + 1) . ".style.backgroundColor='" . $color_sel ."';else prod" . ($this->fila + 1) . ".style.backgroundColor='$color_no_sel'\" VALUE",$contenido);
   if($columna==2 and $tipo_fila=='')
    {
     $color_fondo = $color_cant_orig[$columna];
     $clase='';
    }

//   $clase .= " ONCLICK=\"if(document.ff.item" . ($this->fila + 1) . ".checked==true) document.ff.item" . ($this->fila + 1) . ".checked=false; else document.ff.item" . ($this->fila + 1) . ".checked=true;\" ";
  }

}//FIN CLASE
//##################################################################################################################
//##################################################################################################################

/*if (!isset($desde)) $desde=0;
if (!isset($cantidad_paginacion)) $cantidad_paginacion=40;
$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/lote_prov_lista.php",$cantidad_paginacion);
*/
if($orden=='')
  $orden="ORDER BY p.id_vd ASC";
//$paso="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/historico_mov_stock_lote_prov.php&id_lote=$id_lote&par_id_vd=";
//$img="<img border=0 src=images_gif/buscar.gif>";
if($calcular_cantidad==true)
  $calculo = "sum(IF(dep_in='$sale_de',mov_stock.cantidad,0))-sum(IF(dep_out='$sale_de',mov_stock.cantidad,0))";
else
  $calculo = "0";


if (!isset($desde)) $desde=0;
if (!isset($cantidad_paginacion)) $cantidad_paginacion=40;
$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/stock_precios_listas.php&mostrar=$mostrar&id_lista=$id_lista&par_filtro=$par_filtro&r1=$r1&fil_mostrar=$fil_mostrar",$cantidad_paginacion);

if ($r1=="P")
  $filtro .= " and p.nombreproducto like '%$par_filtro%' ";
elseif($r1=="C")
  $filtro .= " and p.id_vd like '%$par_filtro%' ";
else
	$filtro.= " and lpp.precio = $par_filtro";

if($fil_mostrar<>'')
  $filtro .= " and NOT ISNULL(lpp.id_vd) ";

$qrystr = "SELECT CONCAT(p.id_vd,';',IFNULL(lpp.precio,''),';',IFNULL(lpp.precio,''),';',IFNULL(lpp.fkestacion,''),';',IFNULL(lpp.id_atrib,''),';',p.id_vd,';',p.id_producto,';',p.nombreproducto,';',IFNULL(lpp.usuario,''),';',IFNULL(lpp.momento,'')) AS c
           FROM productos AS p
             LEFT JOIN listas_precios_productos AS lpp ON p.id_vd=lpp.id_vd AND lpp.id_lista=$id_lista
             LEFT JOIN atributos AS at ON lpp.id_atrib=at.id_atrib
           WHERE 1
             $filtro
             $orden
            ".$paginador->limitar($desde);

verificar('',$qrystr);
//echo"CONSULTA ------>>>> $qrystr";

$t=$g_bgolor;
$marco1 = new Marco("img","tabla_",$t);

$lista = new ListasImp();
//$lista->debug=1;
$lista->MostrarChecks("check_titulo","item"," onclick=setear_checks();");
// if($mostrar_id_prod_prov==false)
//   $lista->camposHidden = array("Id_prod_prov");
$lista->titulos=array("check_titulo","Precio","Pr.Actual","Posici�n","A.C","Id_Vd","Id_prod","Nombre","Usuario","�ltima Actualizac.");
$lista->AlineacionColumnas(array('C','R','R','L','R','L','R','L','L','L','R'));

echo "<form method='get' action='administracion.php' name='ff'>
   <input type=hidden name=usuario value=$usuario>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=pagina value=''>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=id_lista value=$id_lista>
   <input type=hidden name=abm value=''>
   <input type=hidden name=par_filtro value='$par_filtro'></td>
   <input type=hidden name='r1' value='$r1' >
   <input type=hidden name='fil_mostrar' value=$fil_mostrar>
   ";

$marco1->abrirMarco();
$contador = $lista->MuestraTabla($qrystr);

echo"
<table width=100% border=0>
  <tr>
    <td  align='Left'>
      <img src='images_gif/arrow_ltr.png' border='0' width='38' height='22' alt='Con marca:' >
      <b>Con Marca: </b>
      <input type=button accesskey='a' name='Actualizar Precio' value='Actualizar Precio' ONCLICK=\"javascript:if (ValidarDatos(ff)) envia('empl/stock_precios_listas_grabar.php','m');\" $stylebutton>
      <input type=button accesskey='e' name='Eliminar de la Lista' value='Eliminar de la Lista' ONCLICK=envia('empl/stock_precios_listas_grabar.php','b') $stylebutton>
    </td>
  </tr>
</table>
<input type=hidden name='cantidad' value='$contador'>
</form>";

echo "<table width=100% border=0>
        <tr>
          <td >
            <b>Art�culos Mostrados:</b> $contador
          </td>
        <tr>
      </table >";

$marco1->cerrarMarco();
echo $paginador->escribirAccesos($desde);


//�������������������� REFERENCIAS ��������������������//
$marco1->abrirMarco();
echo "Referencias:<table border=0>";
foreach($colores as $color)
  echo"<tr><td><table border=1><tr><td height=20 width=20 bgcolor=" . $color["color"] . ">&nbsp;</td></tr></table></td>
       <td>" . $color["ref"] . "</td></tr>";
echo "</table>";
$marco1->cerrarMarco();
//������������������ FIN REFERENCIAS ������������������//


//echo $paginador->escribirAccesos($desde);

//*************** VALIDACIONES (deber�a hacerse m�s arriba pero hasta ac� no conozco la cantidada de filas) *******
echo" <script language='javascript'>
function ValidarDatos(Form)
{    ";
for ($i = 1; $i <= $contador; $i++) {
echo "  if (Form.item$i.checked==true)
          if (PrecioValido('Cantidad',Form.prod$i,0,1000000000))
            return false;";
                       }
echo "   return true;}";
echo "
</script>";
//************* FIN VALIDACIONES (deber�a hacerse m�s arriba pero hasta ac� no conozco la cantidada de filas) *****
echo "
     <input type=hidden name='cantidad' value='$contador'>
     </form>";

echo "
<script language='javascript' type='text/javascript'>
function envia(pag,par_abm)
{
 document.ff.abm.value= par_abm;
 document.ff.pagina.value= pag;
 document.ff.submit();
}
function setear_checks()
{
 for(i=1;i<=$contador;i++)
  {
   if(testIsValidObject(eval('document.ff.item'+i)))
     eval('document.ff.item'+i+'.checked = document.ff.check_titulo.checked');
   if(testIsValidObject(eval('document.ff.prod'+i)))
    {
     if(document.ff.check_titulo.checked==true)
       eval('document.ff.prod'+i+'.style.backgroundColor=\'$color_sel\'');
     else
       eval('document.ff.prod'+i+'.style.backgroundColor=\'$color_no_sel\'');
    }

  }
}
function testIsValidObject(objToTest)
{
 if (null == objToTest)
  {
	 return false;
	}
 if ('undefined' == typeof(objToTest) )
  {
	 return false;
	}
 return true;
}
</script>";

?>
