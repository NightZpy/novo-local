<?php
require_once "empl/clases_interfaz.php";
require_once "estetica.css";

if(isset($individual) && $individual = 1){
    $movimientos = $id_movcon;
}else{
    $movimientos = "";
    foreach($items as $item){
        $movimientos.= $item.',';
    }
    $movimientos = substr($movimientos,0,-1);
}

$t=$g_bgolor;
$marco1 = new Marco("img","tabla_",$t);
$marco1->abrirMarco();
echo "<font face=arial size=3><b>CTA CTE de $param</b></font>";
$marco1->cerrarMarco();
$marco1->alineamientoCelda="left";
$marco1->abrirMarco();
echo "Usted ha seleccionado los siguientes asientos de la cta cte para ser eliminados, si realmente desea eliminarlos presione Eliminar, en caso contrario presione Cancelar para volver a la pantalla anterior.<br/>
Asientos a eliminar:<br/><br/>";

$qrystr = "SELECT id_cta,ref,debe,haber,pedido,fecha_gene FROM ctacte WHERE (id_movcon IN(".$movimientos."))";
$qry = mysql_db_query($c_database,$qrystr,$link);

?>

<table border=0 width="90%" align="center">
  <tr>
    <td style="color:#900;font-weight:bold;border-bottom:1px solid #900;">Fecha</td>
    <td style="color:#900;font-weight:bold;border-bottom:1px solid #900;">Cuenta</td>
    <td style="color:#900;font-weight:bold;border-bottom:1px solid #900;">Referencia</td>
    <td style="color:#900;font-weight:bold;border-bottom:1px solid #900;">Debe</td>
    <td style="color:#900;font-weight:bold;border-bottom:1px solid #900;">Haber</td>
  </tr>
<?php
    while($row = mysql_fetch_array($qry)){
?>
    <tr>
      <td style="border-bottom:1px solid #900;"><?=$row[fecha_gene] ?></td>
      <td style="border-bottom:1px solid #900;"><?=$row[id_cta] ?></td>
      <td style="border-bottom:1px solid #900;"><?=$row[ref] ?></td>
      <td style="border-bottom:1px solid #900;"><?=$row[debe] ?></td>
      <td style="border-bottom:1px solid #900;"><?=$row[haber] ?></td>
    </tr>
<?php        
    }
?>
</table>
<br/><br/>
<p style="width:100%;text-align:center;">
  <input type="button" name="cancelar" value="Cancelar" onclick="javascript:history.back();" <?=$stylebutton ?>> &nbsp;&nbsp;
  <input type="button" name="eliminar" value="Eliminar" onclick="javascript:window.location.href='administracion.php?pagina=empl/lista_cta_cte_acum_admin.php&usuario=<?=$usuario ?>&sesion=<?=$sesion ?>&eliminar=1&movimientos=<?=$movimientos ?>&param=<?=$param ?>&i=<?=$i ?>'" <?=$stylebutton ?>>
</p>
<?php
$marco1->cerrarMarco();
?>
