<?php

$file = $_FILES['param']['tmp_name'];
require_once 'clases_vd/reader.php';
require_once 'clases_vd/readerExcel.class.php';

// ExcelFile($filename, $encoding);
//$data = new Spreadsheet_Excel_Reader();
$data = new readerExcel();
// Set output Encoding.
$data->setOutputEncoding('CP1251');

$data->read($file);

error_reporting(E_ALL ^ E_NOTICE);
$numRows = $data->sheets[0]['numRows'];
$numCols = $data->sheets[0]['numCols'];
$updated = 0;
$inserted = 0;

$objects = $data->getObjects($numRows, $numCols, 'Lista');

foreach($objects as $object){
	//verificamos si el producto ya existe en la base de datos
	$exist = $data->exist($object->id_vd, 'Lista', $id_lista);
	if($exist){   //Si ya existe actualizamos
		$data->update($object, 'Lista', $id_lista);
		$updated++;
	}else{        //Si no existe lo creamos
		$data->insert($object, 'Lista', $id_lista);
		$inserted++;
	}
}
?>