<?php
require('../clases/ChromePhp.php');
function debuggingPHP($devel,$tipo="log",$var){

/* Solo para activar el debug en desarrollo, no funciona en producción */
$desarrolladores=array("JRIOS","LUCIANAV","MARTINC");
//---------------------------------------------------

/* Solo activar para los desarrolladores */
if(in_array($devel,$desarrolladores)){
	switch($tipo){
		case "info":
			ChromePhp::info($var);
			break;
		case "warn":
			ChromePhp::warn($var);
			break;
		case "error":
		 ChromePhp::error($var);
		 break;
		case "table":
		ChromePhp::table($var);
		 break;
		case "log":
			ChromePhp::log($var);
			break;
	}
}

}// End function debuggingPHP ***********************************************
?>
