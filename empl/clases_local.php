<?php
//corre en LOCAL.
//########################## CLASE OBTENEDOR ##############################
class Obtenedor
 {
  var $archivo;
  var $bytes=4096;
  var $direccion;
  var $error = "";
  var $textoPlano = "";
  var $retorno = "variable";   //[pantalla|variable]
  
  //-------------------------------------------------
  function Obtenedor($parDireccion)
   {
    if($parDireccion <> "")
     {
      $parDireccion = str_replace(" ","%20",$parDireccion);
      $this->setDireccion($parDireccion);
     }
   }

  //-------------------------------------------------
  function setRetorno($parRetorno)
   {
    if($parRetorno=="variable")
      $this->retorno = $parRetorno;
    if($parRetorno=="pantalla")
      $this->retorno = $parRetorno;
   }

  //-------------------------------------------------
  function getRetorno()
   {
    return $this->retorno;
   }
  //-------------------------------------------------
  function setDireccion($parDireccion)
   {
    $parDireccion = str_replace(" ","%20",$parDireccion);
    $this->direccion = $parDireccion;
   }

  //-------------------------------------------------
  function getBytes()
   {
    return $this->bytes;
   }

  //-------------------------------------------------
  function setBytes($parBytes)
   {
    if(is_numeric($parBytes))
      if(is_int($parBytes) and ($parBytes > 0))
        $this->bytes = $parBytes;
   }

  //-------------------------------------------------
  function getDireccion()
   {
    return $this->direccion;
   }

  //-------------------------------------------------
  function obtenerDatos($parDireccion = "",$parBytes = 0, $parRetorno = "")
   {
    if ($parDireccion <> "")
      $url = $parDireccion;
    else
      $url = $this->getDireccion();
    $url = str_replace(" ","%20",$url);

    if ($parBytes <> 0)
      $bytes_a_leer = $parBytes;
    else
      $bytes_a_leer = $this->getBytes();

    if ($parRetorno == "pantalla")
      $retorno = $parRetorno;
    elseif($parRetorno == "variable")
      $retorno = $parRetorno;
    else
      $retorno = $this->getRetorno();

    if($this->direccionValida($url)==1)
     {
//      echo "entr�!! $url";
      $this->archivo = fopen ($url, "r");
      if($this->archivo == FALSE) //Abre el archivo. Si da error, lo registra y retorna.
       {
        $this->setError("ERROR AL ABRIR LA URL!!");
        return;
       }

      while (!feof($this->archivo))
       {
        if($retorno == "variable")
          $this->textoPlano = $this->textoPlano."".fgets($this->archivo, $bytes_a_leer);
        if($retorno=="pantalla")
          echo fgets($this->archivo, $bytes_a_leer);
       }
      if($retorno=="variable")
        return $this->textoPlano;
      if($retorno=="pantalla")
        return;
     }
    else
      if($retorno=="variable")
        return "";
      else
        return "";
   }

  //-------------------------------------------------
  function direccionValida($parDireccion = "",$parPuerto = 80, $parTiempoEspera = 30)
   {
    if ($parDireccion <> "")
      $url = $parDireccion;
    else
      $url = $this->direccion;
    //Obtengo el nombre del Servidor porque el fsockopen() s�lo funciona con el nombre pelado.
    $url = str_replace("http://","",$url);
    $pos_primera_barra = -1;
    if (is_integer(strpos($url, "/"))) // not found...
     {
      $pos_primera_barra = strpos($url, "/");
     }
    if($pos_primera_barra <> -1 )
     {
      $url = substr($url,0,$pos_primera_barra);
     }
    //echo "<br>direccionValida url $url <br>";

    $fp = fsockopen ($url, $parPuerto, $errno, $errstr, $parTiempoEspera);
    if (!$fp)
     {
      $this->setError("$errstr ($errno). \n");
      return 0;
     }
    return 1;
   }

  //-------------------------------------------------
  function setError($parError)
   {
    $this->error = $this->error.$parError;
   }

  //-------------------------------------------------
  function getError()
   {
    return $this->error;
   }
 }

//########################## FIN CLASE ######################################


//########################## EJEMPLO DE USO #################################
//Viene como par�metro por GET o POST la variable url.
//$url="http://192.168.0.2/vanesdur_arriba/bajar_datos.php?consulta=select%20*%20from%20estado_pedido";
//$url = "http://192.168.0.2/vanesdur_arriba/bajar_datos.php?consulta=select * from estado_pedido";
//$url = str_replace(" ","%20",$url);
//$url="http://www.vanesaduran.com/pedidos/bajar_datos.php?consulta=select * from estado_pedido";
//$pru = new Obtenedor("$url");
//$pru->obtenerDatos();


//########################## CLASE PARTICIONADOR ###########################
class Particionador
 {
  var $matriz_resultados = array();
  var $separadorCampos = "##";
  var $separadorRegistros = "||";
  var $cabecera = array();
  var $silencio = array();  //Si es 1 no escribe los tiepos que le toma particionar.

  function particionador($parTextoPlano="",$parSilencio=0)
   {
    $this->silencio = $parSilencio;
    if($parTextoPlano<>"")
      $this->particionar($parTextoPlano);
   }
  //-------------------------------------------------
  function setSeparadorCampos($parSeparadorCampos)
   {
    if ($parSeparadorCampos <> "")
      $this->separadorCampos = $parSeparadorCampos;
   }

  //-------------------------------------------------
  function getSeparadorCampos()
   {
    return $this->separadorCampos;
   }

  //-------------------------------------------------
  function setSeparadorRegistros($parSeparadorRegistros)
   {
    if ($parSeparadorRegistros <> "")
      $this->separadorRegistros = $parSeparadorRegistros;
   }

  //-------------------------------------------------
  function getSeparadorRegistros()
   {
    return $this->separadorRegistros;
   }

  function setCabecera($parCabecera)
   {
    $contador = 0;
    foreach($parCabecera as $campo)
     {
//      echo "<br>campo;".$campo;
      $this->cabecera[$campo] = $contador;
//      echo "<br>this->cabecera[campo]".$this->cabecera[$campo];
      $contador++;
     }
      
   }
   
  //-------------------------------------------------
  function getCelda($fila,$campo)
   {
    return $this->matriz_resultados[$fila][$this->cabecera[$campo]];
   }
  //-------------------------------------------------
  function particionar($parTextoPlano,$parSeparadorRegistros="",$parSeparadorCampos="")
   {
    if ($parSeparadorRegistros <> "")
      $sepReg = $parSeparadorRegistros;
    else
      $sepReg = $this->getSeparadorRegistros();

    if ($parSeparadorCampos <> "")
      $sepCam = $parSeparadorCampos;
    else
      $sepCam = $this->getSeparadorCampos();
    if($this->silencio==0)
      echo "<br>antes del primer explode: ".date("Ymd H:i:s");
    $matTemp = array();
    $matTemp = explode($sepReg,$parTextoPlano);

    if($this->silencio==0)
      echo "<br>antes del Segundo explode: ".date("Ymd H:i:s");
//    foreach($matTemp as $v)
//      $this->matriz_resultados[] = explode($sepCam,$v);
    $tama=sizeof($matTemp);
    for($i=0;$i < $tama;$i++)
      $this->matriz_resultados[] = explode($sepCam,$matTemp[$i]);
    if($this->silencio==0)
      echo "<br>Al obtener la matriz: ".date("Ymd H:i:s")."<br>";

   }
 }
//####################### FIN CLASE PARTICIONADOR ##########################


?>
