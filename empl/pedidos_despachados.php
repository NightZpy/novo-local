<?php
//$link = mysql_connect($c_conexion,$c_usuario,$c_password);
//$dias=4;
require_once "../clases/interfaz/classes_listas.php";
require_once "clases_interfaz.php";

if (!isset($desde)) $desde=0;
if (!isset($cantidad_paginacion)) $cantidad_paginacion=40;
$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/pedidos_despachados.php",$cantidad_paginacion);

$paso="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/visualiza_pedidos_despachados.php&id_envio=";
//$paso="empl/visualiza_pedidos_despachados.php&id_envio=";
$edit_bulto="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/edita_bultos.php&id_envio=";
$img="<img border=0 src=images_gif/c_previa.gif>";
//$paso_edit="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/pedidos_despachados_edit.php&par_id_envio=";
//$paso_etiq="empl/pdf/pdf_etiqueta_envio.php?usuario=$usuario&sesion=$sesion&id_envio=";
$paso_etiq="administracion.php?pagina=empl/etiqueta_envio.php&usuario=$usuario&sesion=$sesion&id_envio=";
$qrystr = "select concat(e.id_envio,';',usu2.cod_us,';',concat(usu2.apellido,', ',usu2.nombres),';',e.bultos,';',concat('Edit. Bulto::$edit_bulto',e.id_envio,'&bulto=',e.bultos),';',e.us_control,';',e.fecha_fin_control,';',concat('eti::$paso_etiq',e.id_envio),';',concat('$img::$paso',e.id_envio),';',IFNULL(t.nombre,''),';',e.nro_guia,';',e.fecha_envio) AS c
           FROM envios_distrib AS e
             LEFT JOIN transportes AS t ON e.id_empresa = t.id_empresa
             INNER JOIN pedido as ped ON e.id_envio=ped.id_envio
             INNER JOIN usuario as usu2 ON e.id_distrib = usu2.cod_us
             INNER JOIN estado_envio AS ee ON e.id_estado=ee.cod_estado
           WHERE NOT ISNULL(e.fecha_fin_control)
           GROUP by e.id_envio
           ORDER by e.fecha_gen DESC ".$paginador->limitar($desde);


$marco1 = new Marco("img","tabla_","$g_bgolor");
$marco1->abrirMarco();
echo "<font face='arial' size=3><b>Distribuciones Despachadas</b></font>";
$marco1->cerrarMarco();
$lista = new Listas();
//$lista->debug=1;
$lista->titulos=array("Id_envio","Distribuidor","Apellido, Nombres","Bultos","Edit. Bulto","Us.Ctrl","Fin Ctrl.Dist.","Et.","Ver","Empresa",utf8_decode("Nº Guía"),utf8_decode("Fecha Envío"));
$lista->AlineacionColumnas(array('L','L','L','L','K','L','L','K','K','L','L','L'));
$marco1->abrirMarco();
$lista->MuestraTabla($qrystr);
$marco1->cerrarMarco();

echo $paginador->escribirAccesos($desde);

include("menu_pedidos_data_n1.php");
?>
