<?php

//////// sample array to pass to class //////////
/*
$stuff = array("0" => array(
      		    "product_id" => "00001",
      		    "actual_unit_cost"  => "99.99",
      		    "retail_unit_cost" => "124.99",
      		    "quantity"   => "5"
      		   ),
      "1" => array(
      		    "product_id" => "00002",
      		    "actual_unit_cost" => "699.99",
      		    "retail_unit_cost" => "750.00",
      		    "quantity"   => "3"      		
      		  )
      );
*/


class display {
	var $widths = array();
	var $biggest = array();
	var $data = array();
	var $dis;
	var $divider;
	var $rows;
	var $emptyset=FALSE;
	var $write; //echo the output
	var $ascii_output;
	var $finalhtmltable;
	var $stylesheet;
	
	var $borderwidth = 0;
	var $bordercolor = "#000000";
	var $cellpadding = 2;
	var $cellspacing = 1;
	

	function calculate_widths($array){
		if(empty($array)){
			//check that there is some data to display
			$this->emptyset=TRUE;
			return(false);
		}
		//loop through each row
		$this->data = $array;
		$x=0;
		if(is_array($array)){
			foreach($array as $a){
				while(list($key, $val) = each($a)){ 
						$this->widths[$x][$key] = strlen($val);
				}
				++$x;
			}
		}
		$this->biggest = $this->get_longest_only();
		return($this->widths);
	}

	function get_longest_only(){
		$x=0;
		$array = $this->widths;
		foreach($array as $a){
			while(list($key, $val) = each($a)){ 
					if($val > $this->biggest[$key] || empty($this->biggest[$key])){	
						$this->biggest[$key] = $val;
					}
					if(strlen($key) > $this->biggest[$key]){
						$this->biggest[$key] = strlen($key);
					}
			}
			++$x;
		}
		return($this->biggest);
	}	

	function make_layout($write=1){
		$this->write = $write;
		if($this->emptyset){
			return("Empty set (0.00 hehehe)\n");
		}
			$first="+";	
			while(list($key, $val) = each($this->biggest)){ 

			$dis.="+";
				for($x=0;$x<$this->biggest[$key];$x++){
					$first .= "-";
				}
				$first.="+";
				$s="|".ucwords(str_replace("_", " ",$key));
				if(strlen($s)<= $this->biggest[$key]){
					for($x=strlen($s);$x<=$this->biggest[$key];$x++){
						$s.=" ";
					}
				}
				$second.=$s;
			}
		$this->divider = $first;
		$re = $first."\n".$second."|\n".$first."\n";
		$re.=$rows;	

		$this->rows = $this->make_body();
		$re.=$this->rows;
		if($this->write){
			//write the output to the webpage
			//echo "<pre>".$re."</pre>";
			echo "".$re."";
		}
		$this->ascii_out = $re;
		return($re);
	}

	function create_stylesheet($bg="ededed", $fontcol="000000", $fontsize="x-small", $bg2="444444", $fontcol2="ffffff", $fontsize2="x-small"){
		$this->stylesheet = "
			<STYLE type='text/css'> 
			<!--
				.column-data { background:$bg; color:$fontcol; font-size:$fontsize; }
				.table-header { background:$bg2; color:$fontcol2; font-weight:bold; text-align:center; font-size:$fontsize2 }
			//-->
			</style>";
	}

	function make_body(){
		if(is_array($this->data)){
			foreach($this->data as $row){	
				while(list($key, $val)=each($row)){

				if(is_array($val)){
					$out[0]=$val;
					$tr = new display($out);
					$tr->make_layout(0);
					$tr->set_borderwidth($this->borderwidth);
					$tr->set_cellpadding($this->cellpadding);
					$tr->set_cellspacing($this->cellspacing);
					$tr->set_bordercolor($this->bordercolor);		
					$val = "<Table><TR><TD> ".$tr->make_html_table()."</TD></tR></tAble>";
				}
					$r .= "|".$val;
					if(strlen($val)<= $this->biggest[$key]){
						for($x=strlen($val);$x < $this->biggest[$key]; $x++){	
							$r.=" ";
						}
					}
				}
				$r.="|\n";
			}	
		}	
		$r.=$this->divider."\n";
		return($r);	

	}

	function get_divider(){
		return($this->divider);
	}
	
	function display($stuff){
			//constructor function
			$this->widths = $this->calculate_widths($stuff);
	}

	function set_borderwidth($wid){
		$this->borderwidth = $wid;
	}

	function set_cellpadding($pad){
		$this->cellpadding=$pad;
	}
	
	function set_cellspacing($spac){
		$this->cellspacing=$spac;
	}

	function set_bordercolor($col){
		$this->bordercolor=$col;
	}
	function make_html_table(){
		//converts ascii display into a proper html table
		$text = $this->ascii_out;			

		$rows = explode("\n", $text);
		$x=0;
		foreach($rows as $row){
			$last = strlen($row);
			$class = "column-data";	
			if($x==1){	
				$class = "table-header";
			}
			if(!ereg("^\+\-*", $row) && strlen($row)>0){
				$row = "<TR>\n <td class='$class' align='center' valign='middle'>".$row;
				$row .= "</td>\n</TR>\n";
				$row = str_replace("+", "</td><td class='$class' align='center'>", $row);
				$row = str_replace("|", "</td><td class='$class' align='center'>", $row);
				$row = str_replace("<td class='$class' align='center'></td>", "", $row); //remove any blanks
				$row = str_replace("<td class='$class' align='center' valign='middle'></td>", "", $row); //remove any blanks
				$htmloutput.=$row;
			}
			$x++;
		}

		$style = $this->stylesheet;
		$htmloutput = $style."\n<TABLE border='".$this->borderwidth."' bordercolor='".$this->bordercolor."' cellpadding='".$this->cellpadding."' cellspacing='".$this->cellspacing."'>\n ".$htmloutput."\n</TABLE>";

		$this->finalhtmltable = $htmloutput;
		return($htmloutput);	
	}

	function returnhtml(){
		return($this->finalhtmltable);
	}
	function parsehtml(){
		echo $this->finalhtmltable;
	}
}
			
		

//$t = new display($stuff);
//$t->make_layout(0);
//echo $t->make_html_table();

/*
$d = new display($stuff);
$d->make_layout(0);
$d->set_borderwidth(0);
$d->set_cellspacing(1);
$d->set_cellpadding(2);
$d->make_html_table();
$d->parsehtml();
*/


?>
