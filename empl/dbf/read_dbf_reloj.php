<?php
/*
 *      read_dbf_reloj.php
 *
 *      Copyright 2008 Ríos Javier A. <soporte.softweb@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

if (sesion_ok($usuario,$sesion)=='0') { echo "La sesión ha caducado, presione la tecla F5 para actualizarla."; }

// Directorio de archivos a procesar
//$path = '/var/www/vanesdur/empl/dbf/archivos/';
$path = '/var/www/vanesdur_paralelo/empl/dbf/archivos/';



//$path = "/var/www/dbf/archivos/";

//abrimos el directorio
if(@$dir = opendir($path)){
echo '<!DOCTYPE html>
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>
<body bgcolor=#fffdf0>';
 echo '<form method="POST" name="personal" >
 			<div id="archivos">
 				<p><b>Seleccione el tipo de archivo a procesar ( DBF por Defecto ):</b>
 				<input type="radio" name="op" value=0 checked=checked>DBF&nbsp;
				<input type="radio" name="op" value=1 >CSV&nbsp;</p>';
 echo '<select name="filePersonal">';
 echo '<option selected>Seleccione un archivo</option>';

//arreglo de extensiones permitidas
$arr_exts=array("csv","dbf");
$extension=null;

//Mostramos los archivos del directorio
while (false !== ($filePersonal = readdir($dir))) {

   $ext=strtolower(substr($filePersonal,-3));
   if(($filePersonal!='.') && ($filePersonal!='..') && in_array($ext,$arr_exts)){
		echo '<option>'.$filePersonal.'</option>';
   }
}
echo '</select>&nbsp;&nbsp;<input type="submit" name="process" value="Procesar"></form></div>';
include("empl/menu_pedidos_data_n1.php");
echo '</body></html>';
 
 //Cerramos el directorio
 closedir($dir);
}
else { echo 'ERROR: No se ha localizado el directorio '.$path;}

if($_POST['process']=='Procesar')
{
	$arquivo=$_POST['filePersonal'];
	$extension=strtolower(substr($arquivo,-3));
	$opcion= $_REQUEST['op'];
	
	//echo "opción: ".$opcion." - extension: ".$extension;
	
	if(empty($arquivo) || $arquivo=='Seleccione un archivo'){
		echo "<script type='text/javascript'>
				alert('Debe seleccionar un archivo para realizar esta tarea, se cancela el proceso.');
			  </script>";
		exit;
	}
	elseif($opcion==0 && $extension=="dbf")
		include('read_dbf.php');
	elseif($opcion==1 && $extension=="csv")
		include('read_csv.php');
	else{
		echo "<script type='text/javascript'>
				alert('Por favor seleccione el archivo según el tipo elegido!');
			  </script>";
		exit;
	}
}
?>
