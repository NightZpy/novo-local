<?php
/*
 * Copyright 2008 Ríos Javier A. <soporte.softweb@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


//define("DIRFILES","/var/www/vanesdur/empl/dbf/archivos/");
//define("DIRLOG","/var/www/vanesdur/empl/dbf/logs/");
define("DIRFILES","/var/www/vanesdur_paralelo/empl/dbf/archivos/");
define("DIRLOG","/var/www/vanesdur_paralelo/empl/dbf/logs/");

// Data to connection
$c_database ="vanesaduran_catalogo_paralelo";

define("DB_USER","root");
define("DB_PSW","uysLY3tPtARKgYjkAVF6");
define("DB_NAME","vanesaduran_importaciones");
define("DB_SERVER","localhost");
define("TABLE","reloj_dbf");
/*
define("DB_USER","dbintranet");
define("DB_PSW","uysLY3tPtARKgYjkAVF6");
define("DB_NAME","vanesaduran_importaciones");
define("DB_SERVER","localhost");
define("TABLE","reloj_dbf");
*/
/*
* Luego vendra por parámetro el nombre deel archivo
*/
if (!defined("DIRFILES") || !defined("DIRLOG") || !defined("TABLE")) {
    echo "Fuera de aquí intruso!";
    exit;
}
echo "<p><b>Archivo:</b> ".$_POST['filePersonal']."</p><hr>";

include('/var/www/vanesdur_paralelo/empl/dbf/csv2mysql.inc');
//include('/var/www/vanesdur/empl/dbf/csv2mysql.inc');


    $timer = new timerClass();
    $timer ->start();
    $archivo = DIRFILES.$_POST['filePersonal'];
    $endexct = $timer->end();

    echo "<strong> Proceso generado el ".date("d-m-Y g:i a")." </strong><hr>";
		$q= new Import(); // Crear el objecto para importar los datos
		//$q->debug(4); // Usar solo en desarrollo para debuguear 
		$q->db_connect(DB_SERVER,DB_USER,DB_PSW,DB_NAME);// Conección a la base
    $q->specify_table(TABLE); // Tabla en la cual se insertaran las instrucciones sql
    $q->set_delimit_char(';');// Delimitador de campo del archivo

    // Leer el archivo csv
    $q->read_csv_file($archivo);
    // Alias de campos
    $q->add_field_alias("Hora","fichada");
    $q->add_field_alias("No. Emp.. (xxx)AC.No","legajo");
    $q->add_field_alias("Estado","evento");
    // Crear el array con los datos del archivo
    $q->create_csv_data_array();
    // Crear las sentencias sql a ejecutar en la base de importación y correrlas
    if($q->create_sql(1)){ $r= "Archivo procesado correctamente, revise las fichadas!";}
    else { $r= "No se proceso correctamente el archivo, hubo errores!";}

   // Mostrar las sentencias sql , SOLO PARA DESARROLLO!!!
   //echo "<font color=red>". $q->show_definitions(1) ."</font><br>";
   //die();

    // Ahora viene la inserccion en la base de los registros, previas verificaciones...
	include('/var/www/vanesdur_paralelo/empl/reloj_importacion_paneles.php');

echo("<br>Proceso ejecutado en: ".$endexct."<br>");
echo("<hr>$r<br>");

// Ahora elimino el archivo para evitar volcar en el .sql o la base los mismos datos , cuidado con los permisos!
	echo "Eliminando el archivo ( ".$archivo." ) para evitar errores...<br>";

  	if (file_exists($archivo))
	{
	    // cambiando los permisos
	    @chmod($archivo,0777);
	
	    if (!unlink($archivo)){
			echo "No se pudo eliminar el archivo: ".$archivo.", avise al &aacute;rea de Desarrollo por favor!<br><br>";
	    }
	    else{
			echo "El archivo fue eliminado satisfactoriamente!";
	    }
	}	
?>
