<?php
require_once "estetica.css";
require_once "../clases/interfaz/classes_calendario.php";
require_once "clases_interfaz.php";
if($pagina_llamadora=="" or $pagina_llamadora=="empl/cronograma_visita.php")
 {
  $perm["adm"]["ver"] = false;
  $perm["adm"]["modif"] = false;
  $perm["adm"]["alta"]= false;
  $perm["adm"]["baja"]= false;
  $perm["consulta"]["ver"] = false;
  $perm["consulta"]["modif"] = false;
  $pagina_llamadora="empl/cronograma_visita.php";
 }
//estado  de  la visita
//1 no visitada  2 visitada   3 cancelada
if(isset($accion) and $accion=='eliminar')
{
 $qrystr = "DELETE FROM visita_calendario WHERE id_calendario=$calendario";
 $qry = mysql_db_query($c_database,$qrystr,$link);
 $qrystr1 = "DELETE FROM visita_detalle WHERE id_calendario=$calendario";
 $qry1 = mysql_db_query($c_database,$qrystr1,$link);
 echo"<b>Registro $usuver Eliminado.</b>";
}
class CalendarioImp extends Calendario
{
	function OnEnterRow(&$parFila)
	{
		global $usuario;
		global $sesion;
		global $fecha_desde;
		global $fecha_hasta;
		global $perm;
    global $pagina_llamadora;
		switch($parFila[1])
    {
     case 1: //a visitar
          if($perm["adm"]["alta"]==true)
            $parFila[1]=" <a " . ayuda('Estado: A Visitar<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "href='administracion.php?usuario=$usuario&pagina_llamadora=$pagina_llamadora&sesion=$sesion&pagina=empl/cronograma_visita_alta.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&step=4'><img border=0 src=images_gif/amarillo.gif></a>";
          elseif($perm["consulta"]["modif"]==false)
            $parFila[1]=" <a " . ayuda('Estado: A Visitar<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "><img border=0 src=images_gif/amarillo.gif></a>";
     break;
     case 2: //visitada
          if($perm["adm"]["alta"]==true)
            $parFila[1]=" <a " . ayuda('Estado: Visitada<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "href='administracion.php?usuario=$usuario&pagina_llamadora=$pagina_llamadora&sesion=$sesion&pagina=empl/cronograma_visita_alta.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&step=4'><img border=0 src=images_gif/azul.gif></a>";
          elseif($perm["consulta"]["modif"]==false)
            $parFila[1]=" <a " . ayuda('Estado: Visitada<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "><img border=0 src=images_gif/azul.gif></a>";  
     break;
     case 3: //visitada
          if($perm["adm"]["alta"]==true)
            $parFila[1]=" <a " . ayuda('Estado: Cancelada<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "href='administracion.php?usuario=$usuario&pagina_llamadora=$pagina_llamadora&sesion=$sesion&pagina=empl/cronograma_visita_alta.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&step=4'><img border=0 src=images_gif/rojo.gif></a>";
          elseif($perm["consulta"]["modif"]==false)
            $parFila[1]=" <a " . ayuda('Estado: Cancelada<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "><img border=0 src=images_gif/rojo.gif></a>";  
     break;
     case 4: //en visita
          if($perm["adm"]["alta"]==true)  
            $parFila[1]=" <a " . ayuda('Estado: En Visita<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "href='administracion.php?usuario=$usuario&pagina_llamadora=$pagina_llamadora&sesion=$sesion&pagina=empl/cronograma_visita_alta.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&step=4'><img border=0 src=images_gif/verde.gif></a>";
          elseif($perm["consulta"]["modif"]==false)
            $parFila[1]=" <a " . ayuda('Estado: En Visita<br>Obs. Previa: '.$parFila[7].'<br> Obs. Posterior: '.$parFila[8].'') . "><img border=0 src=images_gif/verde.gif></a>";
     break;
    }
    if($perm["adm"]["ver"]==true or $perm["consulta"]["ver"]==true)
      $parFila[6]="<a" . ayuda('Ver Hora y Area de Visita') . "href='administracion.php?usuario=$usuario&pagina_llamadora=$pagina_llamadora&sesion=$sesion&pagina=empl/cronograma_visita_detalle_consulta.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&step=1&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta'><img border=0 width=12 src=images_gif/buscar.gif></a>";
    if($perm["adm"]["alta"]==true)
      $parFila[6]="<a" . ayuda('Ver Hora y Area de Visita') . "href='administracion.php?usuario=$usuario&sesion=$sesion&pagina_llamadora=$pagina_llamadora&pagina=empl/cronograma_visita_detalle_adm.php&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&step=1&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta'><img border=0 width=12 src=images_gif/buscar.gif></a>";
    if($perm["adm"]["baja"]==true)
      $parFila[6].="<a" . ayuda('Eliminar Visita') . "><img border=0 width=12 src=images_gif/c_eliminar.gif onclick=javascript:preguntar('$parFila[3]','administracion.php?usuario=$usuario&sesion=$sesion&pagina_llamadora=$pagina_llamadora&pagina=$pagina_llamadora&usuver=$parFila[3]&fecha=$parFila[4]&calendario=$parFila[5]&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&accion=eliminar');></a>";
	}
	function OnEnterCell(&$contenido,&$columna,&$color_fondo,&$color_texto,&$resalte,&$fin_resalte,&$tipo_fila,&$clase)
  {
    if($clase<>"class=listtitle")
      $clase="class=listchica";
  }
}
$t=$g_bgolor;
if(!isset($fecha_desde))
  $fecha_desde=date('Y-m-d');
if(!isset($fecha_hasta))
  $fecha_hasta=$diadelmes = date("Y-m-d",strtotime("+7 day"));
$marco1 = new Marco("img","tabla_",$t);
$marco1->abrirMarco();
echo "<font face='arial' size=3><b>$titulo</b></font><br><br>
<form method='POST' action='administracion.php' name='ff'>
   <input type=hidden name=usuario value=$usuario>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=pagina_llamadora value='$pagina_llamadora'>
   <input type=hidden name=pagina value='$pagina_llamadora'>
   <font size='2' face='Arial'><b>Fecha Desde </b></font>
   <input type='text' name='fecha_desde' value='$fecha_desde' size='12' onfocus=\"javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.ff.fecha_desde);return false;\"   ><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.ff.fecha_desde);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a>
   <font size='2' face='Arial'><b>Fecha Hasta </b></font>
   <input type='text' name='fecha_hasta' value='$fecha_hasta' size='12' onfocus=\"javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.ff.fecha_hasta);return false;\"   ><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.ff.fecha_hasta);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a>";
llenar_campo("ff","fecha_desde",$fecha_desde);
llenar_campo("ff","fecha_hasta",$fecha_hasta);
echo"
   <br><br><input type=submit name=Actualizar value=Actualizar>
</form>";
$marco1->cerrarMarco();
$marco1->abrirMarco();
$qrystr = " SELECT fecha,CONCAT('',';',vc.estado,';',CONCAT(u.apellido,' ',u.nombres),';',vc.usuario,';',fecha,';',id_calendario,';','',';',vc.obs_prev,';',vc.obs_post) AS c
            FROM visita_calendario AS vc
           	INNER JOIN usuario AS u ON vc.usuario = u.cod_us
           	WHERE fecha>='$fecha_desde' AND fecha<='$fecha_hasta'";
$calendario = new CalendarioImp();
$calendario->soloClase = true;
$calendario->titulos=array("dia","E","Visitas","usuario","fecha","calendario","","prev","post");
$calendario->camposHidden=array("dia","usuario","fecha","calendario","prev","post");
$calendario->AlineacionColumnas(array('L'));
if($perm["adm"]["alta"]==true)
  $calendario->encabezadoFecha = "<a href='administracion.php?pagina=empl/cronograma_visita_alta.php&step=1&usuario=$usuario&fecha_desde=$fecha_desde&fecha_hasta=$fecha_hasta&sesion=$sesion&pagina_llamadora=$pagina_llamadora&fecha_alta=**fecha**'><img src='images_gif/agregar_over.gif' border=0 width=13 ". ayuda("Agregar Visita a esta Fecha") ."></a>";
$calendario->MuestraTabla($qrystr,"HTML",true);
$marco1->cerrarMarco();
if($perm["adm"]["alta"]==true)
{
  $marco1->abrirMarco();
  $fecha_hoy=date('Y-m-d');
  echo "
      <form method='POST' action='administracion.php' name='gg'>
         <input type=hidden name=usuario value=$usuario>
         <input type=hidden name=sesion value=$sesion>
         <input type=hidden name=step value=1>
         <input type=hidden name=fecha_desde value='0000-00-00'>
         <input type=hidden name=fecha_hasta value='0000-00-00'>
         <input type=hidden name=pagina_llamadora value='$pagina_llamadora'>
         <input type=hidden name=pagina value='empl/cronograma_visita_alta.php'>
         <table>
           <tr>
             <td>
               <font size='2' face='Arial'><b>Otra Visita</b></font>
               <input type='text' name='fecha_alta' value='$fecha_hoy' size='12' onfocus=\"javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.gg.fecha_alta);return false;\"   ><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.gg.fecha_alta);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a>
               <input type=button name=Agregar value=Agregar " . ayuda("Agregar Visita") . " onClick=\"javascript:dar_alta();\">
             </td>
           </tr>
         </table>
      </form>";
  $marco1->cerrarMarco();
}
echo"<iframe width=174 height=189 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src='ipopeng.htm' scrolling='no' frameborder='0' style='visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;'></iframe>";
include "empl/menu_pedidos_data_n1.php";
?>
<script language="javascript">
function dar_alta()
{
  document.gg.fecha_desde.value=document.ff.fecha_desde.value;
  document.gg.fecha_hasta.value=document.ff.fecha_hasta.value;
  document.gg.submit();
}
function preguntar(reg_nom,direccion)
{
 if(!confirm('Seguro desea eliminar el registro '+reg_nom+'?')){return false;}
 else
 {
  location.href = direccion;
 }
}
</script>
