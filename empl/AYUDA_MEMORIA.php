
Buscar los pedidos_defi de la misma campa�a, rev, l�d, dist y ger con su bonif de rev <br>
Buscar los pedidos_defi de la misma campa�a, rev, l�d, dist y ger con su bonif de l�der <br> <br>

Los criterios de asignaci�n de comisi�n de rev actuales se calculan sumando los un_com_s del pedido anterior a los del actual <br>
Los criterios de asignaci�n de comisi�n de l�d actuales se calculan sumando los un_com_s del pedido anterior a los del actual <br> <br>

Si al sumar los un_com_s del pedido viejo m�s los del nuevo tienen >=10 un_com_s y la bonificaron de rev del anterior es 25% <br>
  =>Generar un pedido_defi con la diferencia de la bonificaci�n de rev al 30% y todas las unid. en 0 <br> <br>

Si al sumar los un_com_s del pedido viejo m�s los del nuevo tienen >=100 un_com_s y la bonificaron de l�d del anterior es 5% <br>
  =>Generar un pedido_defi con la diferencia de la bonificaci�n de l�der al 10% y todas las unid. en 0 <br> <br>

Si al sumar los un_com_s del pedido viejo m�s los del nuevo tienen >=160 un_com_s y la bonificaron de l�d del anterior es 10% y id_l�der<> id_rev  <br>
  =>Generar un pedido_defi con la diferencia de la bonificaci�n de l�der al 11% y todas las unid. en 0
