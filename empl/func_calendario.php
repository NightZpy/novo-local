<?php
require_once "../estetica.css";
class Calendario
{
 var $strCalen;
 var $minFecha;
 var $maxFecha;
 var $fechaActual;
 var $debug=false;
 var $titulos;    //Los t�tulos de cada columna en una celda de un d�a. Generalmente s�lo es una columna.

//---------------------------------- M�todos ----------------------------------------------------------//
 /**
  * Calendario::Calendario()
  *
  * {Description}
  *
  * @param
  *
  */
function Calendario()
 {
  $this->titulos=array();
 }

 /**
  * Calendario::LoadData()
  *
  * {Description}
  *
  * @param
  * @param integer
  *
  */
function LoadData($qrystr,$par_campo_fecha=0)       //$par_campo_fecha: Posici�n del campo que contiene las fechas.
 {
   global $link;
   global $c_database;
   if ($this->debug)
      {echo "<BR><font color=#FF0000>LoadData: Param:<BR>qrystr: $qrystr</font><BR>" ;}
   $qry = mysql_db_query($c_database,$qrystr,$link);
   if ($this->debug)
      {echo "<BR><font color=#FF0000>LoadData: Error: " . mysql_error() . "</font><BR>" ;}
   $data=array();
   while ($row = mysql_fetch_array($qry))
    {
     $fecha = date("Y-m-d",strtotime($row[$par_campo_fecha]));
     if($this->minFecha=='')
       $this->minFecha = $fecha;
     $this->maxFecha = $fecha;
     $line=$row[c];
     $data[$fecha][]=explode(';',chop($line));
    }
    if ($this->debug)
    {
     echo "<BR><font color=#FF0000>LoadData: Data cargado: </font><BR>";
     var_dump($data);
    }
   return $data;
 }

 /**
  * Calendario::completaf()
  *
  * {Description}
  *
  * @param
  * @param
  * @param
  *
  */
function completaf($des,$has,$dib)
{
 $des =date("w",strtotime("$des"));
 if ($des>0)
 for ($i = $des; $i <=$has ; $i++)
     {$this->html.="<td class='pequenia_gris' width='14%' bgcolor='#F2F2F2' align='center'>$dib</td>";}
 }

/**
    * Calendario::completa1()
    *
    * {Description}
    *
    * @param
    * @param
    * @param
    *
    */
function completa1($des,$has,$dib)
{
 $has =date("w",strtotime("$has"));
 if ($des>0)
 for ($i = $des; $i <=$has ; $i++)
     {$this->html .= "<td class='pequenia_gris' width='14%' bgcolor='#F2F2F2' align='center'>$dib</td>";}
}

/**
   * Calendario::cabecera1()
   *
   * {Description}
   *
   * @param
   *
   */
function cabecera1()
{
 $this->html .= "
                 <tr class='pequenia_gris' vAlign='center' align='middle' bgColor='#339999'>
                      <td width='14%'>Dom</td>
                      <td width='14%'>Lun</td>
                      <td width='14%'>Mar</td>
                      <td width='14%'>Mi�</td>
                      <td width='14%'>Jue</td>
                      <td width='14%'>Vie</td>
                      <td width='14%'>S�b</td>
                  </tr>
                 " ;
}

 /**
  * Calendario::BasicTable()
  *
  * {Description}
  *
  * @param
  *
  */
function BasicTable($data)
{

 $this->fechaActual = $this->minFecha;

 $this->html="
          <table border='0' cellpadding='1' cellspacing='1'>";

 $this->cabecera1();
 $this->html .= "<tr>";
 $this->completa1(1,$this->fechaActual,"NADA");

 while($this->fechaActual <= $this->maxFecha)
  {
   //Hacer un d�a.
   //Poner fechaActual
   $this->html .="<td class=listwrap $fondito align='center' vAlign='top'>
                         <table>
                           <tr>
                             <td class=listwrap2><font size='1'>* ".date("d M",strtotime($this->fechaActual))." *</font></td>
                           </tr>";

   //Poner Contenidos de la fechaActual
   $dia = $data[$this->fechaActual];
   if(sizeof($dia)<>0)
    {
     foreach($dia as $filaDia)
      {
       foreach($filaDia as $col)
        {
					   if($this->fila % 2 ==0)
						   $clase="class=listwrap";
						 else
						   $clase="class=listwrap2";

						 //if(!in_array($this->titulos[$i],$this->camposHidden))
           $this->html .= "<tr>
                           <td size='1' align=center> <p class='pequenia_gris'>$col</p></td>
                         </tr>";
         $i+=1;
         $this->columna++;
        }
      }
    }

   $this->html .= "      </table>
                  </td>" ;

   if (date("w",strtotime($this->fechaActual))==6)
     $this->html .= "</tr><tr>";

   $this->fechaActual = date("Y-m-d",strtotime($this->fechaActual ." +1 day"));
  }

 $this->completaf($this->fechaActual,6,"FINAL")."</tr></table>";
}

 /**
  * Calendario::MuestraTabla()
  *
  * {Description}
  *
  * @param
  * @param string
  * @param integer
  *
  */
function MuestraTabla($qrystr,$tipo_salida="html",$muestra_header=1,$par_campo_fecha=0)//$tipo_salida puede ser "html" o "excel". Por defecto es "html".
{ $data=$this->LoadData($qrystr,$par_campo_fecha=0);   //$par_campo_fecha: Posici�n del campo que contiene las fechas.
  $this->BasicTable($data);
  if(strtoupper($tipo_salida)=="EXCEL")
   {
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=excel.xls");
   }
  echo $this->html;
  return $this->fila;
}

}//FIN CLASE calendario.

//######################## EJEMPLO ##############################

include "../conexion.php";
//Como m�nimo Debe ir una columna con la fecha y la usual c con el contenido.
$qrystr = "SELECT fecha_gen,concat(id_distrib) AS c
           FROM envios_distrib
           WHERE fecha_envio<>'0000-00-00'
           ORDER BY fecha_gen
           LIMIT 1000";

$calendario = new Calendario();
//$calendario->debug=1;
//$calendario->titulos=array("Colu1");
$calendario->MuestraTabla($qrystr);

?>
