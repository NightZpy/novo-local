<?php
include ("../../conexion.php");

if($tipo_salida==1)
{
require_once "../../../clases/interfaz/classes_listas.php";
 $lista = new Listas();
       
    $qrystr ="SELECT CONCAT(u.id_regional,';',CONCAT(camp.obs,' - ',cro.campania),';',cro.distribuidor,';',concat(u.apellido,' ',u.nombres),';',cro.fecha_entrega,';',ep.estado_descrip,';',COUNT(DISTINCT(cp.id_vendedor)),';',SUM(cp.cantidad * (cp.id_vd='$producto'))) AS c
             FROM cronograma AS cro
             INNER JOIN usuario as u ON cro.distribuidor=u.cod_us
             LEFT JOIN pedido AS p ON cro.id_presentacion=p.id_presentacion
             INNER JOIN estado_pedido AS ep ON p.estado=ep.cod_estado
	           INNER JOIN comp_pedidos AS cp ON p.id_pedidos=cp.id_pedido
	           INNER JOIN campanias AS camp ON cro.campania=camp.id_campania
             WHERE cro.campania>='$c_menor'
                AND cro.campania<='$c_mayor'
                AND u.id_regional NOT IN('VANCHI1','MEXVAN1','COMEXT1','COMPER1','BAJWEB1','BARMAR28')
              $fil_estado
               AND p.anexo='' AND cp.id_vendedor!='CAMBIO'
             GROUP BY p.id_presentacion
             ORDER BY u.id_regional,fecha_entrega  
            ";
            
     
$lista->titulos=array('Id_Gerente','Campa�a','Id_Distrib','Nombre','Fecha Entrega','Estado','Cant.Ped.Rev','Cant/Prod');
$data=$lista->LoadData($qrystr);
$lista->BasicTable($data);
$lista->OutPut("EXCEL");
}
if($tipo_salida==2)
{
include("pdf_set5.php");
require_once('../../../clases/grupos/classes_grupos.php');
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
//instanciando... las variables
/*$qrystr = " SELECT texto FROM advertencia WHERE adv=1002";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);*/
//<A target='_blank' href='xls_usuarios_mail.php?tipo=excel'>Generar para planilla de c�lculo</A><br>
$nota="<br><B>Gener�:  <U>$usuario</U></B> <br>
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";
$reporte="Lista de Productos Ampliados";
$header=array("Id_Gerente","Campa�a","Id_Distrib","Nombre","Fecha Entrega","Estado","Cant.Ped.Rev","Cant/Prod"); //encabezados de columnas (en todos)
$anchos=array(25,15,25,95,22,30,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','C','L','L','L','L','C','C'); //L,R,C
$total=array('TOTAL','','','','','','1','1'); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur� para VD');
$pdf->SetFont('Arial','',10);
$pdf->header_si=0;
$pdf->SetFont('Arial','',9);

    $qrystr ="SELECT CONCAT(u.id_regional,';',CONCAT(camp.obs,' - ',cro.campania),';',cro.distribuidor,';',concat(u.apellido,' ',u.nombres),';',cro.fecha_entrega,';',ep.estado_descrip,';',COUNT(DISTINCT(cp.id_vendedor)),';',SUM(cp.cantidad * (cp.id_vd='$producto'))) AS c
             FROM cronograma AS cro
             INNER JOIN usuario as u ON cro.distribuidor=u.cod_us
             LEFT JOIN pedido AS p ON cro.id_presentacion=p.id_presentacion
             INNER JOIN estado_pedido AS ep ON p.estado=ep.cod_estado
	           INNER JOIN comp_pedidos AS cp ON p.id_pedidos=cp.id_pedido
	           INNER JOIN campanias AS camp ON cro.campania=camp.id_campania
             WHERE cro.campania>='$c_menor'
                AND cro.campania<='$c_mayor'
                AND u.id_regional NOT IN('VANCHI1','MEXVAN1','COMEXT1','COMPER1','BAJWEB1','BARMAR28')
              $fil_estado
             GROUP BY p.id_presentacion
             ORDER BY u.id_regional,fecha_entrega  
            ";
      //echo"$qrystr";
$pdf->AddPage('L');
$data=$pdf->LoadData($qrystr);
$pdf->BasicTable($header,$data,1);
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
}

?>
