<?php
include ("../../conexion.php");
//echo "proyeccion_dia $proyeccion_dia";
$sesion=crear_clave_sesion();
// *************************** Construcci�n de filtros
$reporte="Reporte: Promo -Al Cole con VD-  Campa�a A: $camp_a  Campa�a B: $camp_b ";//nombre del reporte (en todos)
// *************************** FIN Construcci�n de filtros


IF($modo_uso==0)
{
$ent_uno=",`ent3`";
$select_uno=",SUM((cp.cantidad_orig)*(cro.campania='$camp_a'))";
$campanias="cro.campania IN('$camp_a')";
$having="HAVING SUM((cp.cantidad_orig)*(cro.campania='$camp_a'))>='$unid_ca'";
$ent_dos=",`ent5`";
$sum_distrib="SUM(ent3+ent5)";
$sum_lider="SUM(ent3)";
$sum_rev="SUM(ent3)";
}
ELSE
{
$ent_uno=",`ent3`,`ent4`";
$select_uno=",SUM((cp.cantidad_orig)*(cro.campania='$camp_a')),SUM((cp.cantidad)*(cro.campania='$camp_b'))";
$campanias="cro.campania IN('$camp_a','$camp_b')";
	if($filtro='sumatoria')
	{$having="HAVING (SUM((cp.cantidad_orig)*(cro.campania='$camp_a'))+SUM((cp.cantidad)*(cro.campania='$camp_b'))) >='$unid_sum' AND SUM((cp.cantidad_orig)*(cro.campania='$camp_a')) >= 1 AND SUM((cp.cantidad)*(cro.campania='$camp_b')) >= 1";
	 $fil_ger=" AND usu.id_regional='$gerencia'";
	}
	else
	{$having="HAVING SUM((cp.cantidad_orig)*(cro.campania='$camp_a'))>='$unid_ca' AND SUM((cp.cantidad)*(cro.campania='$camp_b')) >='$unid_cb' ";}	
$ent_dos=",`ent5`,`ent6`";
$sum_distrib="SUM(ent3+ent4+ent5+ent6)";
$sum_lider="SUM(ent3+ent4)";

if($filtro='sumatoria')
 {$sum_rev="SUM(ent3),';',SUM(ent4)";}
else
 {$sum_rev="SUM(ent3+ent4)";}
}

$qrystr_sel="INSERT INTO reporte(`clave_ses`,`ref1`,`ref2`,`ref3`,`ref4`,`ent1`$ent_uno) 
             SELECT '$sesion',usu.id_regional,usu.id_distrib,usu.id_lider,cp.id_vendedor,COUNT(DISTINCT(cp.id_vendedor))
                    $select_uno
              FROM comp_pedidos AS cp
              INNER JOIN pedido AS ped ON ped.id_pedidos=cp.id_pedido
              INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion
              INNER JOIN usuario AS usu ON usu.cod_us=cp.id_vendedor
              WHERE $campanias
              AND LEFT(cp.id_vd,1) IN ('S','V','O','P')
              AND ped.id_lider<>ped.id_vendedor
              AND ped.anexo=''
              AND cp.id_vendedor<>'CAMBIO'
              AND usu.id_regional NOT IN('VANCHI1','MEXVAN1','COMEXT1','COMPER1','BAJWEB1')
              $fil_ger
              GROUP BY cp.id_vendedor,usu.id_lider,usu.id_distrib,usu.id_regional
              $having";
$qry_sel = mysql_db_query($c_database,$qrystr_sel,$link);
verificar("",$qrystr_sel);
//echo"$qrystr_sel";

$qrystr_sel2="INSERT INTO reporte(`clave_ses`,`ref1`,`ref2`,`ref3`,`ref4`,`ent2`$ent_dos) 
             SELECT '$sesion',usu.id_regional,usu.id_distrib,usu.id_lider,cp.id_vendedor,COUNT(DISTINCT(CONCAT(cp.id_pedido,'-',cp.id_vendedor)))
                    $select_uno
              FROM comp_pedidos AS cp
              INNER JOIN pedido AS ped ON ped.id_pedidos=cp.id_pedido
              INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion
              INNER JOIN usuario AS usu ON usu.cod_us=cp.id_vendedor
              WHERE $campanias
              AND LEFT(cp.id_vd,1) IN ('S','V','O','P')
              AND ped.id_lider<>ped.id_vendedor
              AND ped.anexo=''
              AND cp.id_vendedor<>'CAMBIO'
              AND usu.id_regional NOT IN('VANCHI1','MEXVAN1','COMEXT1','COMPER1','BAJWEB1')
              $fil_ger
              GROUP BY cp.id_vendedor,usu.id_lider,usu.id_distrib,usu.id_regional
              ";
$qry_sel2 = mysql_db_query($c_database,$qrystr_sel2,$link);
verificar("",$qrystr_sel2);
//echo"<br />$qrystr_sel2<br />";

switch($categoria)
{
case 'D': // DISTRIBUIDORES //
  /// PDF ///
  $header=array('Regional','Distribuidor','Cant/Unid','Acumulado Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  $anchos=array(40,40,40,50,30,30); //anchos de cada celda procurar que sumen aprox 190-
  $alig=array('L','L','L','L','L','L'); //L,R,C
  $total=array("Total",'',1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo)
  $sel="SELECT concat(ref1,';',ref2,';',$sum_distrib,';',SUM(ent1),';',SUM(ent2),';',(SUM(ent1)/SUM(ent2)) ) as c";
  $group="GROUP BY ref2";
  $order="ORDER BY ref1,ref2 ASC";
break;

case 'L': // LIDERES //
  /// PDF ///
  $header=array('Regional','Distribuidor','Lider','Cant/Unid','Acumulado Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  $anchos=array(40,40,40,40,50,30,30); //anchos de cada celda procurar que sumen aprox 190-
  $alig=array('L','L','L','L','L','L','L'); //L,R,C
  $total=array("Total",'','',1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo)
  $sel="SELECT concat(ref1,';',ref2,';',ref3,';',$sum_lider,';',SUM(ent1),';',SUM(ent2),';',(SUM(ent1)/SUM(ent2)) ) as c";
  $group="GROUP BY ref3";
  $order="ORDER BY ref1,ref2,ref3 ASC";
break;

case 'R': // REVENDEDORES //
  /// PDF ///
  if($filtro='sumatoria')
  {
  $header=array('Regional','Distribuidor','Lider','Revendedor','C-A','C-B','Acum/Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  $anchos=array(40,40,40,40,20,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
  $alig=array('L','L','L','L','L','L','L','L','L'); //L,R,C
  $total=array("Total",'','','',1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo)
  $sel="SELECT concat(ref1,';',ref2,';',ref3,';',ref4,';',$sum_rev,';',SUM(ent1),';',SUM(ent2),';',(SUM(ent1)/SUM(ent2)) ) as c";
  $group="GROUP BY ref4";
  $order="ORDER BY ref1,ref2,ref3,ref4 ASC";
  }
  else  
  {
  $header=array('Regional','Distribuidor','Lider','Revendedor','Cant/Unid','Acum/Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  $anchos=array(40,40,40,40,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
  $alig=array('L','L','L','L','L','L','L','L'); //L,R,C
  $total=array("Total",'','','',1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo)
  $sel="SELECT concat(ref1,';',ref2,';',ref3,';',ref4,';',$sum_rev,';',SUM(ent1),';',SUM(ent2),';',(SUM(ent1)/SUM(ent2)) ) as c";
  $group="GROUP BY ref4";
  $order="ORDER BY ref1,ref2,ref3,ref4 ASC";
  }
break;
}

$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B> <br>
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";
//$reporte1="Rep-URG, Reponer, Preparar,Faltantes, Sobre Stk y Normal";//nombre del reporte (en todos)
$notaalpie="  Documentaci�n exclusiva del �REA GERENCIA!!"; //nota al pie


$qrystr = "$sel
           FROM reporte
           WHERE clave_ses='$sesion'
					 $group  	
           $order
           ";

//echo "$qrystr";
verificar("",$qrystr);
// ---------------- fin variables ---------


switch($salida)
{
 case 'PDF':
	include("pdf_set6.php");
	//Recopilacion de Datos
	//$alto=4;
	//Iniciando PDF
	$pdf=new  PDF();
	$pdf->Open();
	$pdf->AliasNbPages();
	$pdf->SetTitle($reporte);
	$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
	$pdf->Setcreator('IDDelSur para VD');
	$data=$pdf->LoadData($qrystr);

	// ----------------------------------------
	$pdf->SetFont('Arial','',10);
	$pdf->AddPage('L');
	$pdf->SetFont('Arial','',8);
	$pdf->BasicTable($header,$data,1);
	//$pdf->ImprovedTable($header,$data);
	//$pdf->FancyTable($header,$data);
	

	$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
	$qry = mysql_db_query($c_database,$qrystr,$link);

	$pdf->Output();
 break;
 case 'EXCEL':
  require_once "../../../clases/interfaz/classes_listas.php";
 	$lista = new Listas();
//  $lista->titulos=array('Vd','Descripci�n','Ubi','Id-Prod-Prov','Estado','Ped','Env','G�n','B�v','Total','Incid','Proy.Arm','Prom.X Dia','Cant D.G�n','Reposici�n'); //encabezados de columnas (en todos)
  switch($categoria)
  {
  case 'D': // DISTRIBUIDORES // /// EXCEL /// 
    $lista->titulos=array('Regional','Distribuidor','Cant/Unid','Acumulado Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  break;
  
  case 'L': // LIDERES // /// EXCEL /// 
    $lista->titulos=array('Regional','Distribuidor','Lider','Cant/Unid','Acumulado Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
  break;

  case 'R': // REVENDEDORES // /// EXCEL /// 	
   if($filtro='sumatoria')
	  {$lista->titulos=array('Regional','Distribuidor','Lider','Revendedor','C-A','C-B','Acum/Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
	  }
	  else
	  {$lista->titulos=array('Regional','Distribuidor','Lider','Revendedor','Cant/Unid','Acum/Rev.','Pedidos','% Ped'); //encabezados de columnas (en todos)
	  }
	  break; 	
  }  
  $data=$lista->LoadData($qrystr);
  $lista->BasicTable($data);
  $lista->OutPut($salida);
  
  	$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
	$qry = mysql_db_query($c_database,$qrystr,$link);
	
 break;
}

?>
