<!-- Bootstrap 
<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">-->

<fieldset>

<!-- Form Name -->
<legend>Informe de Picking</legend>

<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Resumen por día de los funcionarios
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
			<form class="form-horizontal" method='POST' action='empl/pdf/informe_picking_pdf.php' name="ff">
				<input type="hidden" name="usuario" value="<?=$usuario?>" >
				<input type="hidden" name="sesion" value="<?=$sesion?>" >
				<input type="hidden" name="tipoinforme" value="resumen" >
				<!-- Fecha -->
				<div class="control-group">
				 <label class="control-label" for="fecha">Ingrese fecha:</label>
				  <div class="controls">
					<input id="fecha" name="fecha" placeholder="fecha" class="input-large" type="date" >
				  </div>
				</div>

				<!-- Funcionario
				<div class="control-group">
				  <label class="control-label" for="funcionario">Funcionario:</label>
				  <div class="controls">
					<input id="funcionario" name="funcionario" placeholder="Ingrese funcionario" class="input-large" type="search">
					 <p class="help-block">Ingrese funcionario si desea filtrar o deje el campo vacío para obtener un resumen de todos los funcionarios.</p>
				  </div>
				</div>-->
				
				<!-- Button -->
				<div class="control-group">
				  <label class="control-label" for="enviar"></label>
				  <div class="controls">
					<button id="enviar" name="enviar" class="btn btn-success">Enviar datos</button>
				  </div>
				</div>
				
				</form>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Detallado de cada funcionario por día o por pedido
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
      <div class="panel-body">
		  <form class="form-horizontal" method='POST' action='empl/pdf/informe_picking_pdf.php' name="ff">
				<input type="hidden" name="usuario" value="<?=$usuario?>" >
				<input type="hidden" name="sesion" value="<?=$sesion?>" >
				<input type="hidden" name="tipoinforme" value="detalle" >
				
       	<!-- Multiple Radios -->
		<div class="control-group">
		  <label class="control-label" for="radios"></label>
		  <div class="controls">
			<label class="radio" for="pedido">
			  <input name="filtro2" id="pedido" value="porpedido"  type="radio" >
			  Por Pedido
			</label> <input id="pedido" name="pedido" placeholder="Ingrese número de pedido" class="input-large" type="text" style="margin-left: 18px;">
		  </div>
		</div>


		<!-- Multiple Radios -->
		<div class="control-group">
		  <label class="control-label" for="radios"></label>
		  <div class="controls">
			<label class="radio" for="radios-1">
			  <input name="filtro2" id="radios-1" value="porfecha" type="radio" >
			  Por Fecha
			</label><input id="fecha" name="fecha" placeholder="fecha" class="input-medium" type="date" style="margin-left: 18px;">
		  </div>
		</div>

		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label" for="funcionario">Funcionario</label>
		  <div class="controls">
			<input id="funcionario" name="funcionario" placeholder="Ingrese funcionario" class="input-large" type="search" required>
		  </div>
		</div>

		<!-- Select Basic -->
		<div class="control-group">
		  <label class="control-label" for="actividad">Actividad</label>
		  <div class="controls">
			<select id="actividad" name="actividad" class="input-large">
			  <option value="1">Recolecta/Verifica</option>
			 <!-- <option value="2">Empaquetado</option>-->
			</select>
		  </div>
		</div>

		<!-- Button -->
		<div class="control-group">
		  <label class="control-label" for="enviar"></label>
		  <div class="controls">
			<button id="enviar" name="enviar" class="btn btn-success">Enviar datos</button>
		  </div>
		</div>

		</form>
      </div>
    </div>
  </div>
</div>


	
</fieldset>

