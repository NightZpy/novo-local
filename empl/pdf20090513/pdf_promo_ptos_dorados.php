<?php
include ("../../conexion.php");
require_once "../../../clases/interfaz/classes_listas.php";
include("pdf_set6.php");

$sesion=crear_clave_sesion();

////////////////////////////////////////////////////////////////////////////////
$qrystr = "SELECT CONCAT(CONCAT(usu.apellido,' , ',usu.nombres),';',CONCAT(td.descri_tipo_doc ,' - ',usu.nro_doc),';',usu.direccion,';',
           usu.telefono,';',usu.cp,';',usu.localidad,';',usu.provincia,';',usu.mail,';',ppd.puntaje) as c
           FROM promo_puntos_dorados AS ppd
           INNER JOIN usuario AS usu ON usu.cod_us=ppd.distrib
           INNER JOIN tipo_doc AS td ON usu.tipo_doc_id=td.tipo_doc_id
           WHERE ppd.pertenencia<>0
           ";
//echo"$qrystr";
//Iniciando PDF
$salida='EXCEL';
$lista = new Listas();
$lista->titulos=array('Apellido y Nombre','DNI','Domicilio',
                      'Telefono','CP','Localidad',
                      'Provincia','Mail','Puntaje acumulado'); //encabezados de columnas (en todos)
$data=$lista->LoadData($qrystr);
$lista->BasicTable($data);
$tipo=$salida;
$lista->OutPut($tipo);

////////////////////////////////////////////////////////////////////////////////
?>
