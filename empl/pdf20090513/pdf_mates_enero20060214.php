<?php
include("../../conexion.php");
include("inc_pdf/inc_function.php");
require_once "../../../clases/interfaz/classes_listas.php";
$sesion=crear_clave_sesion();
$lista = new Listas();
$lista->titulos=array('Vendedor','Lider','Distribuidor','Gerente','Unidades Pedidas'); //encabezados de columnas (en todos)
$qrystr = " SELECT CONCAT(CONCAT(u.apellido,' ',u.nombres,' (',cp.id_vendedor,')'),';',cp.id_lider,';',cp.id_distrib,';',cp.id_regional,';',sum(cp.cantidad_orig)) AS c,sum(cp.cantidad_orig) as comp
            FROM comp_pedidos_defi AS cp
            INNER JOIN usuario AS u ON u.cod_us = cp.id_vendedor
            WHERE cp.id_pedido >=133978  
                  AND cp.id_pedido <=135000  
                  AND cp.id_vendedor<>cp.id_lider  
                  AND LEFT(cp.id_vd_orig,1) IN('S','V','O','P','Y')  
                  AND cp.id_vendedor<>'CAMBIO'  
            GROUP BY cp.id_vendedor  
            HAVING comp>=22";
$data=$lista->LoadData($qrystr);
$lista->BasicTable($data);
$lista->OutPut("EXCEL");
?>
