<?php
/*Informaci�n de los par�mertro de Entrada:
$usuario          -- Si no viene toma 'FERSOTO'
$fecha_d          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$fecha_h          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$incremento       -- [hour|day|month|year] Es el incremento para el Resumen. Por defecto es un d�a (day) por Resumen.
*/
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();
if (!isset($usuario))
  $usuario="FERSOTO";

$qrystrFecha = " SELECT *
                 FROM fecha_manejo
                 WHERE usuario = '$usuario' ";

//echo $qrystrFecha;
$qryFecha = mysql_db_query($c_database,$qrystrFecha,$link);
$rowFecha = mysql_fetch_array($qryFecha);

if(!isset($fecha_d))
  $fecha_d = $rowFecha["fecha_desde"];
if(!isset($fecha_h))
  $fecha_h = $rowFecha["fecha_hasta"];

if(!isset($incremento))
  $incremento = "day";

$fecha_temp = $fecha_d;
$fecha_temp1 = date("Y-m-d h:i:s a", strtotime ("$fecha_temp +1 $incremento"));
if (strtotime($fecha_temp1) > strtotime($fecha_h))
  $fecha_temp1 =$fecha_h;

//*********************
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
//instanciando... las variables
$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$usuario=strtoupper($usuario);
$header=array('ID_VD','Descripci�n','Deposito','Ingresos','Egresos'); //encabezados de columnas (en todos)
$anchos=array(30,80,30,25,25); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','L','R','R'); //L,R,C
$total=array('Total:','','',1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$notaalpie="  Gracias por seguir confiando en Vanesa Dur�n !!"; //nota al pie
$reporte="Reporte: Informe de Armadoras desde $fecha_d hasta $fecha_h (no inclusive)";//nombre del reporte (en todos)
// ---------------- fin variables ---------
$pdf->SetTitle($reporte);

//Imprime el Resumen Total.
$pdf->SetFont('Arial','',9);
ImprimirSubInforme($fecha_d,$fecha_h,"Reporte: Informe de Movimiento de Stock");

//Imprime los Detalles Uno a Uno.
while(strtotime($fecha_temp) < strtotime($fecha_h))
 {
  ImprimirSubInforme($fecha_temp,$fecha_temp1,"Reporte: Detalle Grupo");
  $fecha_temp = $fecha_temp1 ;
  $fecha_temp1 = date("Y-m-d h:i:s a", strtotime ("$fecha_temp +1 $incremento"));
  if (strtotime($fecha_temp1) > strtotime($fecha_h))
    $fecha_temp1 =$fecha_h;
 }

$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B> <br>
Generador autom�tico de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:atencionalcliente@vanesaduran.com '>
soporte@iddelsur.com.ar</A>,<br>";
$pdf->WriteHTML($nota);
$pdf->Output();

//********************************************************
function ImprimirSubInforme($fecha_d,$fecha_h,$encab)
 {
  global $sesion;
  global $c_database;
  global $qrystr;
  global $link;
  global $pdf;	
  global $header;
  global $anchos;
  global $alig;
  global $total;
  global $reporte;	
  global $reporte1;
  global $orden;	
  //Recopilacion de Datos
  $qrystrdep_in = " INSERT INTO reporte (clave_ses,ref1,ref2,ent1)
                    SELECT '$sesion',p.id_vd,p.dep_in,p.cantidad
                    FROM mov_stock AS p
                    WHERE p.fecha_mov >= '".date("Y-m-d H:i:s", strtotime ($fecha_d))."'
                    AND p.fecha_mov < '".date("Y-m-d H:i:s", strtotime ($fecha_h))."'
                    GROUP BY id_vd,dep_in ";
  //LIMIT 100";
//  echo $qrystr;
  $qrydep_in = mysql_db_query($c_database,$qrystrdep_in,$link);

  $qrystrdep_out = " INSERT INTO reporte (clave_ses,ref1,ref2,ent2)
                     SELECT '$sesion',p.id_vd,p.dep_out,p.cantidad
                     FROM mov_stock AS p
                     WHERE p.fecha_mov >= '".date("Y-m-d H:i:s", strtotime ($fecha_d))."'
                     AND p.fecha_mov < '".date("Y-m-d H:i:s", strtotime ($fecha_h))."'
                     GROUP BY id_vd,dep_out ";
  //LIMIT 100";
//  echo $qrystr;
  $qrydep_out = mysql_db_query($c_database,$qrystrdep_out,$link);

  $qrystrupd = " UPDATE reporte SET ref2='proveedor'
                 WHERE clave_ses='$sesion' AND ref2=''";
//      echo $qrystrupd;
//      exit;
  $qryupd = mysql_db_query($c_database,$qrystrupd,$link);

  //Iniciando PDF
  // aca van los select del load data
  $qrystr = "SELECT concat(ref1,';',nombreproducto,';',ref2,';',sum(ent1),';',sum(ent2)) as c
             FROM reporte
             INNER JOIN productos ON ref1=id_vd
             WHERE clave_ses='$sesion'
             GROUP BY ref1,ref2
             ORDER BY $orden";
  // ---------------- fin variables ---------
  $reporte = "$encab desde $fecha_d ";//nombre del reporte (en todos)
  $reporte1 = "hasta $fecha_h (no inclusive)";//nombre del reporte (en todos)
  $data=$pdf->LoadData($qrystr);
  //print_r($data);
  // ----------- borramos recopilacion de datos ----------

  $qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
  $qry = mysql_db_query($c_database,$qrystr,$link);
  if(mysql_affected_rows()==0)
    return;
  // ----------------------------------------

  $pdf->SetFont('Arial','',10);
  $pdf->AddPage('P');
  $pdf->BasicTable($header,$data,1);
  //$pdf->ImprovedTable($header,$data);
  //$pdf->FancyTable($header,$data);
  $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
  $pdf->Ln();
 }

?>

