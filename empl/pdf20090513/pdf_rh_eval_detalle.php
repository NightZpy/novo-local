<?php
include ("../../conexion.php");
include("inc_pdf/inc_function.php");
include("pdf_set8.php");
//include("pdf_set5.php");

$qrystr1="SELECT CONCAT(rh_e.id_evaluacion,';',rh_ar.descrip,';',rh_pt.descrip,';',
                 CONCAT(usu.nombres,' , ',usu.apellido,' - ',rh_uxp.usuario),';',
                 rh_comp.competencia,';',rh_eval.metodo,' - ',rh_e_not.ident_nota,' - (',rh_e_not.porcentaje,')' 
                 ,';',rh_ed.valor_nota * rh_ed.incidencia_competencia/100
                 ) AS c
                FROM rh_evaluadores AS rh_e
                INNER JOIN rh_evaluaciones_det AS rh_ed ON rh_ed.id_evaluacion=rh_e.id_evaluacion
                INNER JOIN rh_usu_x_puesto AS rh_uxp ON rh_uxp.usu_x_puesto=rh_e.evaluado
                INNER JOIN rh_puesto AS rh_pue ON rh_pue.id_puesto=rh_uxp.id_puesto
                INNER JOIN rh_puesto_tipo AS rh_pt ON rh_pt.id_tipo_puesto=rh_pue.id_tipo_puesto
                INNER JOIN rh_area_x_dpto AS rh_axd ON rh_axd.id_area_x_dpto=rh_pue.area_x_dpto
                INNER JOIN rh_areas AS rh_ar ON rh_ar.id_area=rh_axd.id_area
                INNER JOIN rh_departamento AS rh_de ON rh_de.id_departamento=rh_axd.id_departamento
                INNER JOIN rh_comp_x_puesto AS rh_cxp ON rh_cxp.id_rh_puesto=rh_pue.id_puesto AND rh_cxp.id_competencia=rh_ed.competencia   
                INNER JOIN rh_competencias_aux AS rh_comp ON rh_comp.id_competencia=rh_ed.competencia                
                INNER JOIN rh_eval_metodo AS rh_eval ON rh_eval.id_rh_metodo=rh_cxp.id_rh_metodo
                INNER JOIN rh_eval_nota AS rh_e_not ON rh_e_not.id_rh_metodo=rh_eval.id_rh_metodo
                INNER JOIN usuario AS usu ON usu.cod_us=rh_uxp.usuario           
              WHERE rh_e.id_evaluacion='$id_evaluacion' AND rh_uxp.usu_x_puesto='$usu_x_puesto' AND rh_ed.evaluador='$evaluador'
              GROUP BY rh_comp.competencia,rh_uxp.usuario";
              
   //echo"$qrystr1";
$pdf=new PDF('L');
$pdf->Open();
$pdf->AliasNbPages();
// instanciando las variables
$qrystr2 = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr2,$link);
$row = mysql_fetch_array($qry);
$nota1="
<br />ACCIONES DE MEJORA:<br />
En funci�n de los aspectos a mejorar, identificados en la evaluaci�n realizada, proponemos algunas acciones tendientes a desarrollar las competencias de la persona evaluada, con el objeto de mejorar su desempe�o.<br />

<br />Ampliaci�n de tareas (consistente en la incorporaci�n de nuevas tareas al puesto).
........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
<br />Capacitaci�n (consistente en la adquisici�n de nuevos conocimientos y habilidades por parte de la persona evaluada)
........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
<br />Otros:<br />........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................

<br />SUGERENCIAS Y OPINIONES:
En este espacio, tanto evaluador como evaluado pueden dejar asentadas sus opiniones acerca de la actividad desarrollada, permitiendo a Recursos Humanos realizar las correcciones que sean necesarias para mejorar, de manera cont�nua, el sistema implementado para la evaluaci�n del desempe�o.
<br />
<br />El evaluador:
<br />Sugerencias:........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
<br />El evaluado
<br />Sugerencias:........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
";

$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B> <br><br>
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>soporte@iddelsur.com.ar</A><br>";
$header=array('Id-Eval','Area','Tipo Puesto','Usuario','Competencia','Metodo','Nota'); //encabezados de columnas (en todos)
$anchos=array(13,40,25,65,65,45,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','L','L','L','L','L','L'); //L,R,C
$total=array('','','','','','Nota Final',1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Reporte: Evaluaciones -Recursos Humanos- ";//nombre del reporte (en todos)
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
$pdf->header_si = 0;
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$pdf->camposHidden=array('Area','Tipo Puesto','Usuario');
$pdf->cambiaEnGrupo=array('Area','Tipo Puesto','Usuario');
$pdf->usarGrupos = true;
$pdf->encabezarGrupos = true;
$data=$pdf->LoadData($qrystr1);
$pdf->AddPage();
$pdf->BasicTable($header,$data,1);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Ln(2);

$qrystr4="SELECT CONCAT(rh_de.descrip,';',rh_pt.descrip,';',rh_ed.evaluador,';',CONCAT('D:  ',rh_e.periodo_desde,' - ','H:  ',rh_e.periodo_hasta),';',rh_e.fecha_alta) AS c
                FROM rh_evaluaciones AS rh_e
                INNER JOIN rh_evaluaciones_det AS rh_ed ON rh_ed.id_evaluacion=rh_e.id_evaluacion
                INNER JOIN rh_usu_x_puesto AS rh_uxp ON rh_uxp.usu_x_puesto=rh_ed.usu_x_puesto
                INNER JOIN rh_puesto AS rh_pue ON rh_pue.id_puesto=rh_uxp.id_puesto
                INNER JOIN rh_puesto_tipo AS rh_pt ON rh_pt.id_tipo_puesto=rh_pue.id_tipo_puesto
                INNER JOIN rh_area_x_dpto AS rh_axd ON rh_axd.id_area_x_dpto=rh_pue.area_x_dpto
                INNER JOIN rh_areas AS rh_ar ON rh_ar.id_area=rh_axd.id_area
                INNER JOIN rh_departamento AS rh_de ON rh_de.id_departamento=rh_axd.id_departamento     
              WHERE rh_e.id_evaluacion='$id_evaluacion' AND rh_uxp.usu_x_puesto='$usu_x_puesto' AND rh_ed.evaluador='$evaluador'
              GROUP BY rh_ed.usuario";

//echo"$qrystr2";
$header=array('Departamento','Tipo Puesto','Nombre Evaluador','Periodo Evaluado','Fecha Evaluacion'); //encabezados de columnas (en todos)
$anchos=array(28,25,40,55,55); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','C','C','L','L'); //L,R,C
$total=array('','','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$pdf->SetFont('Arial','',10);
$pdf->SetTitle($reporte);
$pdf->usarGrupos = false;
$pdf->encabezarGrupos = false;
$pdf->encabezarTabla();
$data=$pdf->LoadData($qrystr4);
$pdf->BasicTable($header,$data,0);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->WriteHTML($nota1);
$pdf->Ln(2);
$pdf->WriteHTML($nota);

$pdf->Output();
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// -----------------------------------------------------

?>

