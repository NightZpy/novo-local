<?php
include ("../../conexion.php");

define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
//Cargar los datos


function LoadData1($f)

{ global $link;
  global $tiempo;
  global $c_database;
  $link = mysql_pconnect($c_conexion,$c_usuario,$c_password);
  $qrystr = "SELECT concat(id_lider,';',id_vendedor,';',id_pedidos,';',un_total) as c FROM `pedidos_defi` WHERE 1 limit 40";
  $qry = mysql_db_query($c_database,$qrystr ,$link);
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

function LoadData($file)
{
    //Leer las l�neas del fichero
    $lines=file($file);
    $data=array();
    foreach($lines as $line)
      echo "$line <br>";
    //   $data[]=explode(';',chop($line));
    return $data;
}

//Tabla simple
function BasicTable($header,$data)
{
    //Cabecera
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    //Datos
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}

//Una tabla m�s completa
function ImprovedTable($header,$data)
{
    //Logo
    $this->Image('joy-isologo-ngro.jpg',10,8,70);
    //Arial bold 1510
    $this->SetFont('Arial','B',15);
    //Movernos a la derecha
    $this->Cell(80);
    //T�tulo
    $this->Cell(30,10,'Title',1,0,'C');
    //Salto de l�nea
    $this->Ln(20);
    //Anchuras de las columnas
    $w=array(40,35,40,25);
    //Cabeceras
    $header.=$header1;
    for($i=0;$i<count($header1);$i++)
        $this->Cell($w[$i],7,$header1[$i],1,0,'C');
    $this->Ln();
    //Datos
    foreach($data as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR');
        $this->Cell($w[1],6,$row[1],'LR');
        $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
        $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
        $this->Ln();
    }
    //L�nea de cierre
    $this->Cell(array_sum($w),0,'','T');
}

//Tabla coloreada
function FancyTable($header,$data)
{
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    //Cabecera
    $w=array(40,35,40,45);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',1);
    $this->Ln();
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=0;
    foreach($data as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
        $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
        $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
        $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
        $this->Ln();
        $fill=!$fill;
    }
    $this->Cell(array_sum($w),0,'','T');
}
}

$pdf=new PDF();
$pdf->Open();
//T�tulos de las columnas
$header1=array('Pa�s','Capital','Superficie (km2)','Pobl. (en miles)');
//Carga de datos
$data=$pdf->LoadData1('paises.txt');
//print_r($data);
$pdf->SetFont('Arial','',10);
//$pdf->AddPage();
//$pdf->BasicTable($header,$data);
$pdf->AddPage();
$pdf->ImprovedTable($header,$data);
//$pdf->AddPage();
//$pdf->FancyTable($header,$data);
$pdf->Output();
?>
