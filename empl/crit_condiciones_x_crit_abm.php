<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
 require_once('../clases/forms/class.forms_ale6.php');
 require_once('empl/clases_interfaz.php');

 //####### OBLIGATORIOS
 $tablasuper = "crit_condiciones";          //La tabla del Supertipo.
 $preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/crit_condiciones_x_crit_abm.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/crit_detalles_lista.php";//La p�gina a mostrar si graba exitosamente.

 $titulo = "Carga de Condiciones de Criterios - Criterio $par_id_criterio";   //El t�tulo a mostrar en el html.
 $clave_primaria = "id_cc";              //El nombre del campo que es Clave Primaria.

 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_cc","id_criterio","descriptor","operador","valor","arr_param","orden_condicion");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_cc","id_criterio","Descriptor","Operador","Valor","arr_param","orden_condicion");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_cc","id_criterio","orden_condicion");
 //Las validaciones de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (NumeroValido('Estado',Form.estado,0,1)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Desde',Form.desde)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Hasta',Form.hasta)) return false;");
 //Los COMBOS de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("condicion","crit_condiciones_pedido","descripcion","condicion"));
 $qrystrth = "SELECT propiedad,concat(propiedad,' - ', descripcion) AS paq FROM crit_descriptores_pedido  ORDER BY paq";
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("descriptor","paq","propiedad",$qrystrth));
 $qrystrth = "SELECT nombre_operador,concat(nombre_operador,' - ', descri_operador) AS paq FROM crit_operadores  ORDER BY paq";
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("operador","paq","nombre_operador",$qrystrth));
 //Las FECHAS de la tabla supertipo.
 //$conf_tablas["$tablasuper"]->fechas = array("desde","hasta");
//***********************************************************
//******** FIN ESTO VA A IR EN OTRO ARCHIVO .inc ************



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           DB Layer Example Page - Inserting             #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


 /*Variables de entrada (de control)
 par_id                -- El Producto a mostrar en el ABM. Si es "" es una ALTA.
 par_idh               -- El Hotel elegido si es una ALTA.
 */

 if(!isset(${"par_".$clave_primaria}))
  {
   $condicion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   //$nuevo = nuevo_id($tablasuper,$clave_primaria);
   //${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $condicion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.
   $form->addHiddenForm("estado",$estado);
   $form->addHiddenForm("cmb_punt_aplic",$cmb_punt_aplic);
   $form->addHiddenForm("promo",$promo);
   $form->addHiddenForm("combo",$combo);
//   $form->addHiddenForm("par_id_cc",$par_id_cc);
   if ($condicion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$condicion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     //var_dump($form);
     //$form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$condicion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }

   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //if ($condicion_abm == 1)         //1 = ALTA.
   //  $form->addHiddenForm("idh","$par_idh");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   $form->addHiddenForm("par_id_criterio",$par_id_criterio);      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   $form->addHiddenForm("id_criterio",$par_id_criterio);      //As� se setea un campo de pecho. No importa si se modifica por el usuario.

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
                $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
          {
           if ($condicion_abm == 1)
            {
             echo "<b>Se ha generado el elemento $condicion</b>";
             $temp=mysql_insert_id();
             //Obtengo el Orden m�s grande
             $nuevo_orden = nuevo_id('crit_condiciones','orden_condicion');
             //Hago un UPDATE del orden.
             $qrystr11="UPDATE crit_condiciones SET orden_condicion = $nuevo_orden, arr_param = REPLACE (arr_param,'\\\',''),valor = REPLACE (valor,'\\\','') WHERE id_cc='$temp' AND id_criterio='$par_id_criterio'";
             $qry11 = mysql_db_query($c_database,$qrystr11,$link);
             echo "$qrystr11<br>";
             $err=mysql_error();
             auditar($usuario,"Criterios","Alta de condiciones","Criterio: $par_id_criterio, Condicion: $temp , Orden=$nuevo_orden");
            }
           else
               {
                echo "<b>Se ha modificado el elemento $condicion</b>";
                $qrystr11="UPDATE crit_condiciones SET arr_param = REPLACE (arr_param,'\\\',''),valor = REPLACE (valor,'\\\','') WHERE id_cc=$par_id_cc AND id_criterio=$par_id_criterio";
             $qry11 = mysql_db_query($c_database,$qrystr11,$link);
             auditar($usuario,"Criterios","Modificacion de Condiciones ","Criterio: $par_id_criterio, Condicion: $par_id_cc");
               }

           include ($par_pag_sig);
          }
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
     $t=$g_bgolor;
     $marco1 = new Marco("img","tabla_",$t);
     $marco1->abrirMarco();
   	 echo $form->build("200","10","200","3","40");
     $marco1->cerrarMarco();
   	 echo"<a target=_blank href=administracion.php?pagina=empl/crit_descriptores_lista.php&usuario=$usuario&sesion=$sesion><img border='' src='images_gif/modificar_normal.gif'" . ayuda("Modificar Nombre") . ">[Ayuda de Descriptores]</a>";
     include "menu_pedidos_data_n1.php";
	  }


//################################################################################################
function nuevo_id($tablasuper,$campo)
 {
  global $link;
  global $c_database;

  $strConsulta = "SELECT MAX(`$campo`)+1
                  FROM $tablasuper " ;
  $qrynuevo = mysql_db_query($c_database,$strConsulta,$link);
  $rownuevo = mysql_fetch_array($qrynuevo);
  return $rownuevo[0];
 }
?>
