<script language="javascript">
function ValidarDatos(Form)
{
  if (StringVacio("Otra Fecha",Form.fecha)) return false;
  return true;
}
</script>
<?php
require_once "estetica.css";
require_once "../clases/interfaz/classes_calendario.php";
require_once "clases_interfaz.php";
require_once "empl/estados_coord.inc";
if($pagina_llamadora=="" or $pagina_llamadora=="empl/cronograma.php")
 {
  $perm["adm"]["ver"] = false;
  $perm["adm"]["modif"] = false;
  $perm["adm"]["alta"]= false;
  $perm["adm"]["baja"]= false;
  $perm["ctacte"]["ver"] = false;
  $perm["ctacte"]["modif"] = false;
  $perm["acc"]["ver"] = false;
  $perm["acc"]["modif"] = false;
  $perm["gra"]["ver"] = false;
  $perm["gra"]["modif"] = false;
  $pagina_llamadora="empl/cronograma.php";
 }


//##########################################################################################################
class CalendarioImp extends Calendario
{
//M�todo Abstracto a implementar en la clase hija.
function OnEnterRow(&$parFila)
 {
  global $usuario;
  global $sesion;
  global $perm;
  global $pagina_llamadora;
  global $estados;
  
//  if($parFila[2]=='GENMON62')
  //var_dump($parFila);
  
  $paso_ver="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/usuario_total.php&usuver=";
  $parFila[2] = "<a target=_blank " . ayuda($parFila[3]) . " href='$paso_ver" . $parFila[2] . "'>" . $parFila[2] . "</a>";

  // ************************************************ //

  //DE ACA RECUPERA EL VALOR DE LOS ESTADOS
  //estado_ctacte
  //estado_adm
  //estado_acc
  //estado_gra
  
  $pos_estado_adm = array_search("estado_adm",$this->titulos);
  $pos_estado_ctacte = array_search("estado_ctacte",$this->titulos);
  $pos_estado_acc = array_search("estado_acc",$this->titulos);
  $pos_estado_gra = array_search("estado_gra",$this->titulos);
  $pos_obs_adm1 = array_search("obs_adm1",$this->titulos);
  $pos_obs_ctacte1 = array_search("obs_ctacte1",$this->titulos);
  $pos_obs_acc1 = array_search("obs_acc1",$this->titulos);
  $pos_obs_gra1 = array_search("obs_gra1",$this->titulos);
  $pos_obs_glo = array_search("obs_glo",$this->titulos);

  // PREGUNTA POR LOS PERMISOS PARA MODIFICARLOS SI ES DISTINTO DE 3
  $semaf_adm = estado_adm($parFila[$pos_estado_adm]);
  if($semaf_adm<>9 and $perm["adm"]["modif"]==true)
    $paso_adm =  " href=administracion.php?pagina=empl/coord_cambiaestado.php&usuario=$usuario&sesion=$sesion&id_presentacion=$parFila[4]&paso=0&tipo=adm&pagina_llamadora=$pagina_llamadora";
  else
    $paso_adm =  "";

  $semaf_ctacte = estado_ctacte($parFila[$pos_estado_ctacte]);
  if($semaf_ctacte<>9 and $perm["ctacte"]["modif"]==true)
    $paso_ctacte =  " href=administracion.php?pagina=empl/coord_cambiaestado.php&usuario=$usuario&sesion=$sesion&id_presentacion=$parFila[4]&paso=0&tipo=ctacte&pagina_llamadora=$pagina_llamadora";
  else
    $paso_ctacte =  "";
  $semaf_acc = estado_acc($parFila[$pos_estado_acc]);
  if($semaf_acc<>9 and $perm["acc"]["modif"]==true)
    $paso_acc =  "
href=administracion.php?pagina=empl/coord_cambiaestado.php&usuario=$usuario&sesion=$sesion&id_presentacion=$parFila[4]&paso=0&tipo=acc&pagina_llamadora=$pagina_llamadora";
  else
    $paso_acc =  "";
	
  $semaf_gra = estado_gra($parFila[$pos_estado_gra]);
  if($semaf_gra<>9 and $perm["gra"]["modif"]==true)
    $paso_gra =  " href=administracion.php?pagina=empl/coord_cambiaestado.php&usuario=$usuario&sesion=$sesion&id_presentacion=$parFila[4]&paso=0&tipo=gra&pagina_llamadora=$pagina_llamadora";
  else
    $paso_gra =  "";
  $semaf_global = estado_global($parFila[$pos_estado_ctacte],$parFila[$pos_estado_adm]);

  // PREGUNTA POR LOS ESTADOS Y LE ASIGNA UN COLOR DE ACUERDO AL ARRAY

  if($perm["adm"]["ver"]==true)
    $estado_adm = "<a " . ayuda("<b>Adm: </b>" . $estados[$semaf_adm]["ref"] . "<br>" . $parFila[$pos_obs_adm1]) . " $paso_adm><img border=0 src='" . $estados[$semaf_adm]["img"] . "'></a>";
  if($perm["ctacte"]["ver"]==true)
    $estado_ctacte = "<a "  . ayuda("<b>CtaCte: </b>" . $estados[$semaf_ctacte]["ref"] . "<br>" . $parFila[$pos_obs_ctacte1]) . " $paso_ctacte><img border=0 src='" . $estados[$semaf_ctacte]["img"] . "'></a>";
  if($perm["acc"]["ver"]==true)
    $estado_acc = "<a "  . ayuda("<b>Acc: </b>" . $estados[$semaf_acc]["ref"] . "<br>" . $parFila[$pos_obs_acc1]) . " $paso_acc><img border=0 src='" . $estados[$semaf_acc]["img"] . "'></a>";
  if($perm["gra"]["ver"]==true)
    $estado_gra = "<a "  . ayuda("<b>Gra: </b>" . $estados[$semaf_gra]["ref"] . "<br>" . $parFila[$pos_obs_gra1]) . " $paso_gra><img border=0 src='" . $estados[$semaf_gra]["img"] . "'></a>";
    
  $estado_global = "<a " . ayuda($estados[$semaf_global]["ref"] . "<br>" . $parFila[$pos_obs_glo]) . " ><img border=0 src='" . $estados[$semaf_global]["img"] . "'></a>";

  // ************************************************ //
  $parFila[0] = $estado_global . $estado_adm . $estado_ctacte . $estado_acc . $estado_gra;
 }
}
//##########################################################################################################


$qrystr = "SELECT id_campania, fecha FROM campanias ORDER BY id_campania DESC LIMIT 0,1";
$qry = mysql_db_query($c_database,$qrystr ,$link);
$row = mysql_fetch_array($qry);
//Si no hay una campa�a elegida, busco la �ltima.
if($id_campania=="")
  $id_campania = $row["id_campania"];
$fecha_camp = $row["fecha"];
//echo "fecha_camp $fecha_camp";

$t=$g_bgolor;
$marco1 = new Marco("img","tabla_",$t);
$marco1->abrirMarco();
echo "<table width=100% border=0><tr>";
echo "<td align=center>
      <font face='arial' size=3><b>Cronograma de Campa�as - Fechas de Entrega</b></font>
      </td><tr></table >";

echo "<form method='POST' action='administracion.php' name='ff'>
   <input type=hidden name=usuario value=$usuario>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=pagina value='$pagina_llamadora'>
   ";
$qrystramedida = "SELECT id_campania, concat(id_campania,' (',fecha,')- ',obs) AS paq FROM campanias order by id_campania desc";
poner_combo_a_medida("id_campania","paq","id_campania",$qrystramedida);
llenar_campo("ff","id_campania",$id_campania);

echo "
   <input type=submit name=Actualizar value=Actualizar>
</form>";
$marco1->cerrarMarco();
$marco1->abrirMarco();

//El calendario agrega a los links un par�metro llamado fecha que contiene la fecha seleccionada &fecha=xxxx-xx-xx.
$paso_elim="administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/cronograma_eliminar_revendedor.php&id_presentacion=";
$img_elim="<img border=0 src=images_gif/eliminar_over.gif width=13 ALT=Quitar>";

if($perm["adm"]["baja"]==true)
  $elim = "concat('$img_elim::$paso_elim',id_presentacion)";
else
  $elim = "''";

//Como m�nimo Debe ir una columna con la fecha y la usual c con el contenido.
$qrystr = "SELECT fecha_entrega,concat('',';',$elim,';',distribuidor,';',concat(IFNULL(apellido,''),' ',IFNULL(nombres,'')),';',id_presentacion,';',
             estado_ctacte,';',
             estado_adm,';',
             estado_acc,';',
             estado_gra,';',
             IF(cronograma.obs='','',concat('<b>NOTA:</b>',LEFT(cronograma.obs,255))),';',
             IF(obs_adm='','',concat('<b>NOTA:</b>',LEFT(obs_adm,255))),';',
             IF(obs_ctacte='','',concat('<b>NOTA:</b>',LEFT(obs_ctacte,255))),';',
             IF(obs_acc='','',concat('<b>NOTA:</b>',LEFT(obs_acc,255))),';',
             IF(obs_gra='','',concat('<b>NOTA:</b>',LEFT(obs_gra,255)))
             ) AS c
           FROM cronograma
             INNER JOIN usuario on distribuidor=cod_us
           WHERE campania = $id_campania
           ";
//echo $qrystr;
$calendario = new CalendarioImp();
//$calendario->debug=1;
$calendario->titulos=array("estados","eliminar","cod_us","nombres","id_presentacion","estado_ctacte","estado_adm","estado_acc","estado_gra","obs_glo","obs_adm1","obs_ctacte1","obs_acc1","obs_gra1");
$calendario->camposHidden=array("nombres","id_presentacion","estado_ctacte","estado_adm","estado_acc","estado_gra","obs_glo","obs_adm1","obs_ctacte1","obs_acc1","obs_gra1");
//$calendario->encabezarMes = true;
//$calendario->saltarEnMes = true;
//$calendario->alturaMinima = 150;
//$calendario->colorDomingo = '#FF5555';
//$calendario->colorDiaHabil = '#555555';
//$calendario->minFecha = '2005-08-02';
//$calendario->maxFecha = '2005-08-20';
$calendario->AlineacionColumnas(array('L','K','K'));
if($perm["adm"]["alta"]==true)
  $calendario->encabezadoFecha = " <a href='administracion.php?id_campania=$id_campania&pagina=empl/cronograma_adm_cmb.php&paso=1&usuario=$usuario&sesion=$sesion&fecha=**fecha**'><img src='images_gif/agregar_over.gif' border=0 width=13 ". ayuda("Agregar Distribuci�n a esta Fecha") ."></a>" ;
else
  $calendario->encabezadoFecha = " " ;

$calendario->MuestraTabla($qrystr);


$marco1->cerrarMarco();
if($perm["adm"]["alta"])
 {
$marco1->abrirMarco();
echo "
<form method='POST' action='administracion.php' name='gg'>
   <input type=hidden name=usuario value=$usuario>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=paso value=1>
   <input type=hidden name=pagina value='empl/cronograma_adm_cmb.php'>
   <input type=hidden name=id_campania value='$id_campania'>
   <table>
     <tr>
       <td>
         <font size='2' face='Arial'><b>Otra Fecha</b></font>
         <input type='text' name='fecha' value='$fecha_camp' size='12' onfocus=\"javascript:blur();if(self.gfPop)gfPop.fPopCalendar(document.gg.fecha);return false;\"   ><a href='javascript:void(0);' onclick='if(self.gfPop)gfPop.fPopCalendar(document.gg.fecha);return false;' HIDEFOCUS><img name='popcal' align='absmiddle' src='calbtn.gif' width='34' height='22' border='0' alt=''></a>
         <input type=button name=Agregar value=Agregar onClick=\"javascript:if (ValidarDatos(gg)) submit();\">
       </td>
     </tr>
   </table>
</form>
 <iframe width=174 height=189 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src='ipopeng.htm' scrolling='no' frameborder='0' style='visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;'>
 </iframe>

";
$marco1->cerrarMarco();
 }

include "empl/menu_pedidos_data_n1.php";
?>
