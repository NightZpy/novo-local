<?php
require_once "../clases/interfaz/classes_listas4.php";
require_once "empl/clases_interfaz.php";
require_once "estetica.css";
$paginaesta="empl/solicitud_compra_lista.php";
echo"
 <form method='get' name='ff' action='administracion.php'>
 <input type=hidden name=usuario value='$usuario'>
 <input type=hidden name=sesion value='$sesion'>
 <input type=hidden name=pagina value=''>";

if($pagina_llamadora=="")
 {
  $perm["ver"] = false;
  $perm["modif"] = false;
  
 }

class ListasImp extends Listas
{
 function OnEnterRow(&$parFila)
 {
  global $usuario;
  global $sesion;
  global $c_database;
  global $link;
  global $dependencia;
  $parFila[6] = "<a " . ayuda("Modificar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_abm.php&par_id_sol_compra=" . $parFila[0] . "><img border=0 src=images_gif/editar.gif></a>";
  $parFila[7] = "<a " . ayuda("Agregar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/solicitud_compra_comp_lista.php&par_id_sol_compra=" . $parFila[0] . "><img border=0 src=images_gif/agregar_over.gif></a>";
  
  switch($parFila[4]){
  	case 1://generada
                  $parFila[8] = "<a " . ayuda("Enviar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=2&par_id_sol_compra=" . $parFila[0] . ">A Enviado</a>";
                  $parFila[9] = "<a " . ayuda("Eliminar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=99&par_id_sol_compra=" . $parFila[0] . ">Eliminar</a>";
                  $parFila[10] = "<a " . ayuda("PDF $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&par_id_sol_compra=" . $parFila[0] . ">PDF</a>";
                  break;
             case 2://en rectificacion
                  $parFila[8] = "<a " . ayuda("Rectificar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=3&par_id_sol_compra=" . $parFila[0] . ">A Rectificar</a>";
                  $parFila[9] = "<a " . ayuda("Eliminar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=99&par_id_sol_compra=" . $parFila[0] . ">Eliminar</a>";
                  $parFila[10] = "<a " . ayuda("PDF $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&par_id_sol_compra=" . $parFila[0] . ">PDF</a>";
             break;
             case 3://Enviada
                  $parFila[8] = "<a " . ayuda("Revisar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=4&par_id_sol_compra=" . $parFila[0] . ">A Revisar</a>";
             break;
             case 4:
                  $parFila[8] = "<a " . ayuda("Aceptar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=5&par_id_sol_compra=" . $parFila[0] . ">Aceptar</a>";
             break;
             case 5:
                  $parFila[8] = "<a " . ayuda("Rechazar $parFila[0]") . "href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/sol_compra_cambiaestado.php&estsig=6&par_id_sol_compra=" . $parFila[0] . ">Rechazar</a>";
             break;
            }
  
  }  
}
if(!isset($desde)) $desde=0;
if(!isset($cantidad_paginacion)) $cantidad_paginacion=40;
$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=$paginaesta",$cantidad_paginacion);
$qrystr = "
           SELECT CONCAT(id_sol_compra,';',CONCAT(a.id_area,'-',a.descri),';',CONCAT(u.cod_us,'-',u.apellido,',',u.nombres),';',sc.momento,';',CONCAT(sce.id_estado,'-',sce.descri_estado),';',IF(CHAR_LENGTH(sc.obs)>100,CONCAT(SUBSTRING(sc.obs,1,100),'...'),sc.obs),';','',';','',';','',';','',';','') AS c
           FROM sol_compra AS sc
           INNER JOIN usuario AS u ON sc.us_crea = u.cod_us
           INNER JOIN areas AS a ON sc.id_area = a.id_area
           INNER JOIN sol_compra_estados AS sce ON sce.id_estado = sc.estado
           ORDER BY sc.id_sol_compra
           ".$paginador->limitar($desde);
//echo $qrystr;
$t=$g_bgolor;
$marco1 = new Marco("img","tabla_",$t);
$marco1->abrirMarco();
echo "<font face='arial' size=3><b>Listado de Solicitudes de Compras </b></font>
      <input type=button name=docu value=Agregar" . ayuda("Dar de alta a una nueva solicitud de compra") . " onclick=\"javascript:nuevodoc();\" $stylebutton>";
$marco1->cerrarMarco();
$lista = new ListasImp();
$lista->titulos=array("ID","Area","Usuario","Momento","Estado","Obs"," "," "," "," "," ");
$lista->AlineacionColumnas(array('C','L','L','L','L','L','K','K','K','K','K'));
$lista->camposHidden=array();
echo $paginador->escribirAccesos($desde);
$marco1->abrirMarco();
$contador = $lista->MuestraTabla($qrystr);
$marco1->cerrarMarco();
echo $paginador->escribirAccesos($desde);

include("empl/menu_pedidos_data_n1.php");

echo "</form>
<script language='javascript' type='text/javascript'>
function nuevodoc()
 {
  pagina='empl/sol_compra_abm.php';
  ff.pagina.value=pagina;
  ff.submit();
 }
</script>";
?>
