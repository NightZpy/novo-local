<?
require_once('../clases/forms/class.forms_ale7.php');
require_once "estetica.css";
//####### OBLIGATORIOS
$tablasuper = "atributos";  //La tabla del Supertipo.
$estapagina = "empl/atributos_abm.php";  //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
$par_pag_sig = "empl/manejo_de_atributos.php";  //La p�gina a mostrar si graba exitosamente.

$titulo = "ABM de Atributos";  //El t�tulo a mostrar en el html.
$clave_primaria = "id_atrib";  //El nombre del campo que es Clave Primaria.
$conf_tablas = array();  //Array que contiene las opciones para las tablas a mostrar.
//####### OPCIONALES
//T�tulos de los campos.

//****** tabla tipo.
$conf_tablas["$tablasuper"] = new conf_tabla();  //La clase conf_tabla est� definida en el abm.
$conf_tablas["$tablasuper"]->campos_usar = array("abrev","descri","computable","env_cuenta_como","env_cuenta_como1","falt_cuenta_como","accion_boni_rev","accion_boni_lid","accion_boni_dist","accion_boni_ger","valor_boni_rev","valor_boni_lid","valor_boni_dist","valor_boni_ger","obs","pvp_comp");
$conf_tablas["$tablasuper"]->titulos_campos = array("Abreviatura","Descripcion","Computable","Enviado-Cta-Como","Enviado-Cta-Como1","Faltante-Cta-Como","Accion-Bonif-Rev","Accion-Bonif-Lider","Accion-Bonif-Dist","Accion-Bonif-Ger","Valor-Bonif-Rev","Valor-Bonif-Lider","Valor-Bonif-Dist","Valor-Bonif-Ger","Obs","Pvp Comp");
$conf_tablas["$tablasuper"]->campos_hidden = array("computable","env_cuenta_como","env_cuenta_como1","falt_cuenta_como","accion_boni_rev","accion_boni_lid","accion_boni_dist","accion_boni_ger","pvp_comp");

$qrystra = "SELECT id,concat(valor)as descri FROM enum_si_no WHERE 1 ORDER BY descri";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("computable","descri","id",$qrystra));

 $qrystrb = "SELECT como_cuenta, concat(como_cuenta) as descri FROM atributos_env_cuenta WHERE 1 ORDER BY descri";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("env_cuenta_como","descri","como_cuenta",$qrystrb));
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida1Texto("env_cuenta_como1","descri","como_cuenta",$qrystrb,"Ninguno"," "));

 $qrystrc = "SELECT como_cuenta, concat(como_cuenta) as descri FROM atributos_falt_cuenta WHERE 1 ORDER BY descri";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("falt_cuenta_como","descri","como_cuenta",$qrystrc));

////// COMBOS DE ACCIONES BONIFICACION ///////

 $qrystrac = "SELECT id_accion, concat(descri_accion) as descri FROM atributos_acciones_bonif WHERE 1 ORDER BY id_accion";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("accion_boni_rev","descri","id_accion",$qrystrac));

  $qrystrab = "SELECT id_accion, concat(descri_accion) as descri FROM atributos_acciones_bonif WHERE 1 ORDER BY id_accion";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("accion_boni_lid","descri","id_accion",$qrystrab));

  $qrystrad = "SELECT id_accion, concat(descri_accion) as descri FROM atributos_acciones_bonif WHERE 1 ORDER BY id_accion";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("accion_boni_dist","descri","id_accion",$qrystrad));

   $qrystrae = "SELECT id_accion, concat(descri_accion) as descri FROM atributos_acciones_bonif WHERE 1 ORDER BY id_accion";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("accion_boni_ger","descri","id_accion",$qrystrae));

 $qrystrpvp = "SELECT id_enum, concat(valor) as descri FROM enum_elementos WHERE grupo_enum=7 ORDER BY id_enum";
 //echo $qrystra;
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("pvp_comp","descri","id_enum",$qrystrpvp));

//***********************************************************

if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;  //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
//   $nuevo = nuevo_id($tablasuper,$clave_primaria);
 //  ${$clave_primaria} = $nuevo;
 //  echo "Id Area: ".$nuevo;
  }
else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;  //2=MODIFICACI�N.
  }
// Prepare Form
// ============

//************* Ac� se declara el objeto Base de Datos con sus campos ******************
$db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
$db1->select();

//************* Ac� se declara el objeto formulario con sus campos ******************
$form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
$form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

if($accion_abm == 1)  //1 = ALTA.
  {
//   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
//   $form->addHiddenForm($clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
//   $form->addHiddenForm($campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("pagina_llamadora",$pagina_llamadora);  //As� se setea un campo de pecho. No importa si se modifica por el usuario.
//   $form->addHiddenForm("par_".$campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
  }
else  //MODIFICACI�N.
  {
   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
   $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
   $form->addHiddenForm("pagina_llamadora",$pagina_llamadora);  //As� se setea un campo de pecho. No importa si se modifica por el usuario.
  }

//Los t�tulos de los campos separados con /:/
$form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$tablasub"]->titulos_campos)));
//$form->addHiddenForm("nombre_producto","de pecho");  //As� se setea un campo de pecho. No importa si se modifica por el usuario.

//Los combos de la tabla Supertipo.
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasuper"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//Los combos de la tabla Subtipo seleccionada.
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasub"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//FABRICAR LOS CAMPOS.
$form->makefields();
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasub"]->validaciones[$v]);

//************* Ac� se define si hay que mostrar los input **************************
//************* o si hay que actualizar los valores en la bd. ***********************
// Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
// ===================================================
if($hacerenvio=='S')
  {
   if(sesion_ok($usuario,$sesion)=='0')
     {
      echo "La sesi�n ha caducado";
      //header("location:error.php");
     }
   else
     {
      if($form->submit())
        {
		     if($renueva_sesion == 'S')
            $sesion = sesion_ok_ns($usuario,$sesion,4000);
         if($cierra_al_grabar == 'S')
            echo "<script language='javascript'>close();</script>";
         else
           {
            if($accion_abm == 1){
               echo "<br><b>Se ha generado un nuevo Atributo</b>";
               $nuevo=mysql_insert_id();
            }
            else
               echo "<br><b>Se ha modificado el Producto $par_id_sol_compra</b>";
            include($par_pag_sig);
           }
 	    	 }
	     else
         {
		      echo $error;
		      echo mysql_error();
		      echo "\n<br><a href=$PHP_SELF>Back</a>";
		     }
	    }
  }
else
  {
 	 echo $form->build("200","10","200","3","40");
 	 include "menu_pedidos_data_n1.php";
  }

?>

