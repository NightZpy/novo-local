<?php
include("../../../conexion.php");
$rowaparte['descuento'] = '0.00';
$rowaparte['filler'] = '';
$rowaparte['version'] = '900000';
$rowaparte['can_equi_v'] = '1.00';
$rowaparte['cod_clasif'] = '1';
$rowaparte['talon_ped'] = '1001';
$rowaparte['pen_rem_fc'] = '0.00';
$rowaparte['pen_fac_re'] = '0.00';
$ali_r=array();
$ali_l=array();
$ali_r['can_equi_v']= 9;
$ali_r['cant_a_des']= 9;
$ali_r['cant_a_fac']= 9;
$ali_r['cant_pedid']= 9;
$ali_r['cant_pen_d']= 9;
$ali_r['cant_pen_f']= 9;
$ali_l['cod_articu']= 15;
$ali_r['descuento']= 5;
$ali_r['filler']= 20;
$ali_r['n_renglon']= 4;
$ali_r['nro_pedido']= 13;
$ali_r['pen_rem_fc']= 9;
$ali_r['pen_fac_re']= 9;
$ali_r['precio']= 9;
$ali_r['talon_ped']= 4;
$ali_r['version']= 7;
$ali_l['cod_clasif']= 6;
foreach($ali_r as $nombre=>$valor)
	$rowaparte[$nombre]=substr(str_repeat(" ", $valor) . $rowaparte[$nombre], -$valor);
foreach($ali_l as $nombre=>$valor)
	$rowaparte[$nombre]=substr($rowaparte[$nombre] . str_repeat(" ", $valor), 0, $valor);
$i=0;
$linea1="";
$nombre_archivo = "cabeza_GVA03P.txt";
$gestor = fopen($nombre_archivo, "r");
$linea1 .= fread($gestor, filesize($nombre_archivo));
fclose($gestor);
$qrystr1 = " SELECT MIN(cpd.id_pedido) AS pedido, sum(cpd.cantidad) as cant,p.id_vd_fact,cpd.nombreproducto,
                    sum((cpd.cantidad*((100-(cpd.pb_id_revendedor+cpd.pb_id_lider+cpd.pb_id_distrib))*cpd.preciounidad)/100))/SUM(cpd.cantidad) AS precio_distrib
             FROM comp_pedidos_defi AS cpd
             INNER JOIN productos AS p ON cpd.id_vd = p.id_vd
             WHERE cpd.id_distrib='$id_distrib'
                   AND cpd.id_pedido >= '$pedido_desde'
                   AND cpd.id_pedido <= '$pedido_hasta'
                   AND cpd.neto<>0
                   AND cpd.id_vendedor<>'CAMBIO'
             GROUP BY p.id_vd_fact
             ORDER BY p.id_vd_fact";
$qry1 = mysql_db_query($c_database,$qrystr1,$link);
//echo $qrystr1;
$err=mysql_error();
if($err<>'')
{
	echo "Error: $err<br>$qrystr1";
	exit;
}
$i=0;
$row1[nro_pedido]=$row1[pedido];
foreach($ali_r as $nombre=>$valor)
	$row1[$nombre]=substr(str_repeat(" ", $valor) . $row1[$nombre], -$valor);
foreach($ali_l as $nombre=>$valor)
	$row1[$nombre]=substr($row1[$nombre] . str_repeat(" ", $valor), 0, $valor);
while($row1 = mysql_fetch_array($qry1))
{
	$i++;
	$row1[n_renglon]=$i;
	$row1[cant_a_des]=$row1[cant];
	$row1[cant_a_fac]=$row1[cant];
	$row1[cant_pedid]=$row1[cant];
	$row1[cant_pen_d]=$row1[cant];
	$row1[cant_pen_f]=$row1[cant];
	$row1[cant_ped_f]=$row1[cant];
	$row1[cod_articu]=$row1[id_vd_fact];
	//$row1[talon_ped]=$row1[cant];
	foreach($ali_r as $nombre=>$valor)
		$row1[$nombre]=substr(str_repeat(" ", $valor) . $row1[$nombre], -$valor);
	foreach($ali_l as $nombre=>$valor)
		$row1[$nombre]=substr($row1[$nombre] . str_repeat(" ", $valor), 0, $valor);
	$row1[cant_a_des]=substr('         ' . number_format(1*($row1[cant_a_des]),2,'.',''),-9);
	$row1[cant_a_fac]=substr('         ' . number_format(1*($row1[cant_a_fac]),2,'.',''),-9);
	$row1[cant_pedid]=substr('         ' . number_format(1*($row1[cant_pedid]),2,'.',''),-9);
	$row1[cant_pen_d]=substr('         ' . number_format(1*($row1[cant_pen_d]),2,'.',''),-9);
	$row1[cant_pen_f]=substr('         ' . number_format(1*($row1[cant_pen_f]),2,'.',''),-9);
	$row1[precio]=substr(number_format(1*($row1[precio_distrib]),9,'.',''),0,9);
	$row1[nro_pedido]=$row1[pedido];
	$row1[nro_pedido]=' ' . substr('000000000000' . (1*$row1[nro_pedido]),-12);
	$linea1.= "<z:row CAN_EQUI_V='" . $rowaparte[can_equi_v] . "' CANT_A_DES='" . $row1[cant_a_des] ."' CANT_A_FAC='" . $row1[cant_a_fac] . "' CANT_PEDID='" . $row1[cant_pedid] . "' CANT_PEN_D='" . $row1[cant_pen_d] . "' CANT_PEN_F='" . $row1[cant_pen_f] . "' COD_CLASIF='" . $rowaparte[cod_clasif]  . "'
    COD_ARTICU='" . $row1[cod_articu] ."' DESCUENTO='" . $rowaparte[descuento] .  "' FILLER='" . $rowaparte[filler] .  "' N_RENGLON='" . $row1[n_renglon] . "' NRO_PEDIDO='" . $row1[nro_pedido] . "' PEN_REM_FC='" . $rowaparte[pen_rem_fc] . "' PEN_FAC_RE='" . $rowaparte[pen_fac_re] . "'
     PRECIO='" . $row1[precio] . "' TALON_PED='" . $rowaparte[talon_ped] . "' Version='" . $rowaparte[version] .  "'/>";
}
$linea1.="</rs:data>
</xml>";
header('Content-Type: text/enriched');
header('Content-Disposition: inline; filename=GVA03P.xml');
header('Expires: 0');
header('Last-Modified: ' . $now);
header('Cache-Control:must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
echo $linea1;
?>
