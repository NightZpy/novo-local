<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly' rs:updatable='true'>
		<s:AttributeType name='FILLER' rs:number='1' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CANTIDAD' rs:number='2' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='9' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FEC_ENTREG' rs:number='3' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='TALON_PED' rs:number='4' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='4' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NRO_PEDIDO' rs:number='5' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='13' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='N_RENGLON' rs:number='6' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='4' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='Version' rs:number='7' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='7' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>