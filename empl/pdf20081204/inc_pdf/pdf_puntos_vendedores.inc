<?php
$header=array('Promo','Nombre','Puntos'); //encabezados de columnas (en todos)
$anchos=array(40,80,40); //anschos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','R'); //L,R,C
$total=array('Cantidad',2); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Reporte: Puntajes de Vendedores";//nombre del reporte (en todos)
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
// aca van los select del load data
$qrystr = " SELECT CONCAT(m.descri_promo,';',CONCAT(apellido,' ',nombres,'(',p.usuario,')'),';',puntos) AS c , CONCAT(apellido,' ',nombres,'(',p.usuario,')') as ref
			FROM promox_puntos_usu AS p
			INNER JOIN usuario AS u ON p.usuario = cod_us AND u.id_lider = '$lider'
			INNER JOIN promox AS m ON m.id_promo = p.id_promo 
            ORDER BY ref";
            
// ---------------- fin variables ---------
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$data=$pdf->LoadData($qrystr);
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->SetFont('Arial','',9);
$pdf->BasicTable($header,$data,1);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();

//$pdf->Output();
?>