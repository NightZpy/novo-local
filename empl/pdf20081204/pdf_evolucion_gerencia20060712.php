<?php
include("../../conexion.php");
include("inc_pdf/inc_function.php");
// ----------- borramos recopilacion de datos -------------
/*$qrystr = "DELETE FROM reporte WHERE clave_ses='CAMPANIA'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_A'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_B'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_C'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_D'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_E'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_DIS_A'";
$qry = mysql_db_query($c_database,$qrystr,$link);*/
// --------------------------------------------------------
$nombregerente = poner_datos_reg($gerencia);
//inserto en reporte las campanias a utilizar... $c_mayor es la ultima y obtengo las ultimas 4 
$qrystrcamp = " INSERT INTO reporte (clave_ses,ent1,ref1)
                SELECT 'CAMPANIA',id_campania,obs
                FROM campanias 
                WHERE id_campania<='$c_mayor'
                ORDER BY id_campania DESC
                LIMIT 4";
$qrycamp = mysql_db_query($c_database,$qrystrcamp,$link);
///////////////////////////////////////////////////////////////////////////////////////////////

//recorro campania por campania 
$qrystrcampania = " SELECT ent1,ref1
                    FROM reporte
                    WHERE clave_ses = 'CAMPANIA'
                    ORDER BY ent1";
$qrycampania = mysql_db_query($c_database,$qrystrcampania,$link);
$indi=0;
while($campania = mysql_fetch_array($qrycampania))
{
 //tomo la campania activa
 $indi++;
 $qmen = " SELECT id_pedido AS desde,id_ped_fin AS hasta,obs FROM campanias WHERE id_campania = '$campania[0]'";
 $qymen = mysql_db_query($c_database,$qmen,$link);
 $rmen = mysql_fetch_array($qymen);
 $filtro_campania = " AND cpd.id_pedido >= '$rmen[desde]' ";
 if($indi==4)
   {
    $filtro_ultima_campania = " AND cpd.id_pedido >= '$rmen[desde]' ";
    $nombre_ultima_campania = $rmen[obs];
    if($rmen[hasta]<>0)
      {
       $filtro_ultima_campania .= " AND cpd.id_pedido <= '$rmen[hasta]' ";
      }
   }
 if($indi==3)
   {
    $filtro_anteultima_campania = " AND cpd.id_pedido >= '$rmen[desde]' ";
    $nombre_anteultima_campania = $rmen[obs];
    if($rmen[hasta]<>0)
      {
       $filtro_anteultima_campania .= " AND cpd.id_pedido <= '$rmen[hasta]' ";
      }
   }  
 if($indi==2)
   {
    $filtro_segunda_campania = " AND cpd.id_pedido >= '$rmen[desde]' ";
    $nombre_segunda_campania = $rmen[obs];
    if($rmen[hasta]<>0)
      {
       $filtro_segunda_campania .= " AND cpd.id_pedido <= '$rmen[hasta]' ";
      }
   }  
 if($indi==1)
   {
    $filtro_primera_campania = " AND cpd.id_pedido >= '$rmen[desde]' ";
    $nombre_primera_campania = $rmen[obs];
    if($rmen[hasta]<>0)
      {
       $filtro_primera_campania .= " AND cpd.id_pedido <= '$rmen[hasta]' ";
      }
   }      
 if($rmen[hasta]<>0)
   {
    $filtro_campania .= " AND cpd.id_pedido <= '$rmen[hasta]' ";
   }

 //inserto en reporte valores para la tabla c mes x mes (campania x campania)
 $qrystr_tabla_c = " INSERT INTO reporte (clave_ses,ref1,ent1,val1,ent2,ent3,ent4,ent5,ent6,ent7,ent9,ref2)
                     SELECT 'TABLA_C','$campania[1]',$campania[0],SUM(cpd.cantidad*cpd.preciounidad),SUM(cpd.cantidad),
                            SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),COUNT(DISTINCT(cpd.id_vendedor)),
                            COUNT(DISTINCT(cpd.id_lider)),COUNT(DISTINCT(cpd.id_distrib)),$indi,cpd.id_distrib
                     FROM comp_pedidos_defi AS cpd
		     WHERE cpd.id_vendedor<>'CAMBIO' AND
		     LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_regional = '$gerencia' $filtro_campania GROUP BY cpd.id_regional";
 $qry_tabla_c = mysql_db_query($c_database,$qrystr_tabla_c,$link);
}
//fin recorrido campania x campania

//selecciono la tabla_c 
$select_tabla_c = " SELECT ref1 AS mes,ent1 AS camp,val1 AS pvp ,ent2 AS uenv,ent3 AS ufal,ent4 AS uped,ent5 AS ped,ent6 AS lid,ent7 AS dis
                    FROM reporte
                    WHERE clave_ses = 'TABLA_C'
                    ORDER BY ent1";
$qry_sel_tabla_c = mysql_db_query($c_database,$select_tabla_c,$link);
$indice=0;

//recorro la tabla_c para armar la tabla_a
while($tabla_c = mysql_fetch_array($qry_sel_tabla_c))
{
 $indice++;
 if($indice==4)
   {

    //aca calculo la fila de la media trimestral
    $qrystr_tabla_a3 = " SELECT 'Media Trimestral' AS mes,SUM(ent1)/3 AS ped,SUM(ent2)/3 AS lid,SUM(val1)/3 AS plid,SUM(val2)/3 AS uped,SUM(val3)/3 AS ulid,SUM(ent3)/3 AS dis,SUM(val5)/3 AS pdis,SUM(val6)/3 AS lxd,$indice
                         FROM reporte
                         WHERE clave_ses = 'TABLA_A'";
    $qry_tabla_a3 = mysql_db_query($c_database,$qrystr_tabla_a3,$link);
    $tabla_a3 = mysql_fetch_array($qry_tabla_a3);
 
    //aca inserto la fila calculada con la media trimestral
    $qrystr_tabla_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent3,val5,val6,ent9)
                        VALUES ('TABLA_A','$tabla_a3[mes]',$tabla_a3[ped],$tabla_a3[lid],$tabla_a3[plid],$tabla_a3[uped],
                                $tabla_a3[ulid],$tabla_a3[dis],$tabla_a3[pdis],$tabla_a3[lxd],$indice)";
    $qry_tabla_a = mysql_db_query($c_database,$qrystr_tabla_a,$link);
    $indice++;
   }

 //inserto mes x mes en la tabla_a
 $qrystr_tabla_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent3,val5,val6,ent9)
                     VALUES ('TABLA_A','$tabla_c[mes]',$tabla_c[ped],$tabla_c[lid],$tabla_c[ped]/$tabla_c[lid],
                             $tabla_c[uped]/$tabla_c[ped],$tabla_c[uped]/$tabla_c[lid],$tabla_c[dis],
                             $tabla_c[ped]/$tabla_c[dis],$tabla_c[lid]/$tabla_c[dis],$indice)";
 $qry_tabla_a = mysql_db_query($c_database,$qrystr_tabla_a,$link);
}

//obtengo filas de la tabla_a para su posterior calculo
$tabla_a_2 = " SELECT ref1 AS mes,ent1 AS ped,ent2 AS lid,val1 AS plid,val2 AS uped,val3 AS ulid,ent3 AS dis,val5 AS pdis,val6 AS lxd
               FROM reporte
               WHERE clave_ses = 'TABLA_A' AND ent9=2";
$qry_tabla_a_2 = mysql_db_query($c_database,$tabla_a_2,$link);
$tabla_a2 = mysql_fetch_array($qry_tabla_a_2);
$tabla_a_3 = " SELECT ref1 AS mes,ent1 AS ped,ent2 AS lid,val1 AS plid,val2 AS uped,val3 AS ulid,ent3 AS dis,val5 AS pdis,val6 AS lxd
               FROM reporte
               WHERE clave_ses = 'TABLA_A' AND ent9=3";
$qry_tabla_a_3 = mysql_db_query($c_database,$tabla_a_3,$link);
$tabla_a3 = mysql_fetch_array($qry_tabla_a_3);
$tabla_a_4 = " SELECT ref1 AS mes,ent1 AS ped,ent2 AS lid,val1 AS plid,val2 AS uped,val3 AS ulid,ent3 AS dis,val5 AS pdis,val6 AS lxd
               FROM reporte
               WHERE clave_ses = 'TABLA_A' AND ent9=4";
$qry_tabla_a_4 = mysql_db_query($c_database,$tabla_a_4,$link);
$tabla_a4 = mysql_fetch_array($qry_tabla_a_4);
$tabla_a_5 = " SELECT ref1 AS mes,ent1 AS ped,ent2 AS lid,val1 AS plid,val2 AS uped,val3 AS ulid,ent3 AS dis,val5 AS pdis,val6 AS lxd
               FROM reporte
               WHERE clave_ses = 'TABLA_A' AND ent9=5";
$qry_tabla_a_5 = mysql_db_query($c_database,$tabla_a_5,$link);
$tabla_a5 = mysql_fetch_array($qry_tabla_a_5);

//inserto las filas calculadas de la tabla_a
$qrystr_tabla_a6 = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent3,val5,val6,ent9)
                     VALUES ('TABLA_A',CONCAT('$tabla_a3[mes]',' - ','$tabla_a2[mes]'),$tabla_a3[ped]-$tabla_a2[ped],
                             $tabla_a3[lid]-$tabla_a2[lid],$tabla_a3[plid]-$tabla_a2[plid],$tabla_a3[uped]-$tabla_a2[uped],
                             $tabla_a3[ulid]-$tabla_a2[ulid],$tabla_a3[dis]-$tabla_a2[dis],$tabla_a3[pdis]-$tabla_a2[pdis],
                             $tabla_a3[lxd]-$tabla_a2[lxd],6)";
$qry_tabla_a6 = mysql_db_query($c_database,$qrystr_tabla_a6,$link);
$qrystr_tabla_a7 = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent3,val5,val6,ent9)
                     VALUES ('TABLA_A',CONCAT('$tabla_a5[mes]',' - ','$tabla_a3[mes]'),$tabla_a5[ped]-$tabla_a3[ped],
                             $tabla_a5[lid]-$tabla_a3[lid],$tabla_a5[plid]-$tabla_a3[plid],$tabla_a5[uped]-$tabla_a3[uped],
                             $tabla_a5[ulid]-$tabla_a3[ulid],$tabla_a5[dis]-$tabla_a3[dis],$tabla_a5[pdis]-$tabla_a3[pdis],
                             $tabla_a5[lxd]-$tabla_a3[lxd],7)";
$qry_tabla_a7 = mysql_db_query($c_database,$qrystr_tabla_a7,$link);
$qrystr_tabla_a8 = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent3,val5,val6,ent9)
                     VALUES ('TABLA_A',CONCAT('$tabla_a5[mes]',' - ','$tabla_a4[mes]'),$tabla_a5[ped]-$tabla_a4[ped],
                             $tabla_a5[lid]-$tabla_a4[lid],$tabla_a5[plid]-$tabla_a4[plid],$tabla_a5[uped]-$tabla_a4[uped],
                             $tabla_a5[ulid]-$tabla_a4[ulid],$tabla_a5[dis]-$tabla_a4[dis],$tabla_a5[pdis]-$tabla_a4[pdis],
                             $tabla_a5[lxd]-$tabla_a4[lxd],8)";
$qry_tabla_a8 = mysql_db_query($c_database,$qrystr_tabla_a8,$link);

//inserto en reporte la tabla_e
$qrystr_tabla_e = " INSERT INTO reporte (clave_ses,ref1,val1,ent1,ent2,ent3,ent4,ent5,ent9)
                    SELECT 'TABLA_E',CONCAT(u.apellido,' ',u.nombres),SUM(cpd.cantidad*cpd.preciounidad),SUM(cpd.cantidad),
                           SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),COUNT(DISTINCT(CONCAT(cpd.id_pedido,cpd.id_vendedor))),
                           COUNT(DISTINCT(cpd.id_pedido)),0
                    FROM comp_pedidos_defi AS cpd
		    INNER JOIN usuario AS u ON u.cod_us = cpd.id_distrib
                    WHERE cpd.id_vendedor<>'CAMBIO' AND
		    LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_regional = '$gerencia' $filtro_ultima_campania
                    GROUP BY cpd.id_distrib"; 
$qry_tabla_e = mysql_db_query($c_database,$qrystr_tabla_e,$link);

//inserto en reporte la tabla_e subtotales
$select_tabla_e = " SELECT sum(val1) AS pvp,SUM(ent1) AS env,SUM(ent2) AS fal,SUM(ent3) AS ped,SUM(ent4) AS rev,SUM(ent5) AS lid
                    FROM reporte
                    WHERE clave_ses = 'TABLA_E'";
$qry_sel_tabla_e = mysql_db_query($c_database,$select_tabla_e,$link);
$tabla_e = mysql_fetch_array($qry_sel_tabla_e);
$qrystr_tabla_e = " INSERT INTO reporte (clave_ses,ref1,val1,ent1,ent2,ent3,ent4,ent5,ent9)
                    VALUES ('TABLA_E','Totales',$tabla_e[pvp],$tabla_e[env],$tabla_e[fal],$tabla_e[ped],$tabla_e[rev],$tabla_e[lid],1)"; 
$qry_tabla_e = mysql_db_query($c_database,$qrystr_tabla_e,$link);
$select_tabla_e_tot = " SELECT ref1 AS dis,val1 AS pvp,ent1 AS env,ent2 AS fal,ent3 AS ped,ent4 AS rev,ent5 AS lid
                        FROM reporte
                        WHERE clave_ses = 'TABLA_E' AND ent9=0
                        ORDER BY ref1";
$qry_sel_tabla_e_tot = mysql_db_query($c_database,$select_tabla_e_tot,$link);
$ind=3;
while($tabla_e_tot = mysql_fetch_array($qry_sel_tabla_e_tot))
{
 $qrystr_tabla_e = " INSERT INTO reporte (clave_ses,ref1,val1,ent1,ent2,ent3,ent4,ent5,ent9)
                     VALUES ('TABLA_E','$tabla_e_tot[dis]',($tabla_e_tot[pvp]/$tabla_e[pvp])*100,
                             ($tabla_e_tot[env]/$tabla_e[env])*100,($tabla_e_tot[fal]/$tabla_e[fal])*100,
                             ($tabla_e_tot[ped]/$tabla_e[ped])*100,($tabla_e_tot[rev]/$tabla_e[rev])*100,
                             ($tabla_e_tot[lid]/$tabla_e[lid])*100,$ind)"; 
 $qry_tabla_e = mysql_db_query($c_database,$qrystr_tabla_e,$link);
 
 $ind++;
}

//selecciono la tabla_c 
$select_tabla_c = " SELECT ref1 AS mes,val1 AS pvp,ent2 AS env,ent3 AS fal,ent4 AS ped,ent5 AS rev,ent6 AS lid,ent7 AS dis
                    FROM reporte
                    WHERE clave_ses = 'TABLA_C'
                    ORDER BY ent1";
$qry_sel_tabla_c = mysql_db_query($c_database,$select_tabla_c,$link);
$indice=0;

//recorro la tabla_c para armar la tabla_b
while($tabla_c = mysql_fetch_array($qry_sel_tabla_c))
{
 $indice++;
 if($indice==4)
   {
    //aca calculo la fila de la media trimestral
    $qrystr_tabla_b3 = " SELECT 'Media Trimestral' AS mes,SUM(val1)/3 AS pvp,SUM(val2)/3 AS xrev,SUM(val3)/3 AS xlid,SUM(val4)/3 AS xuni,SUM(ent1)/3 AS env,SUM(ent2)/3 AS fal,SUM(ent3)/3 AS ped,SUM(val5)/3 AS xfal,$indice
                         FROM reporte
                         WHERE clave_ses = 'TABLA_B'";
    $qry_tabla_b3 = mysql_db_query($c_database,$qrystr_tabla_b3,$link);
    $tabla_b3 = mysql_fetch_array($qry_tabla_b3);
 
    //aca inserto la fila calculada con la media trimestral
    $qrystr_tabla_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
                        VALUES ('TABLA_B','$tabla_b3[mes]',$tabla_b3[pvp],$tabla_b3[xrev],$tabla_b3[xlid],$tabla_b3[xuni],
                                $tabla_b3[env],$tabla_b3[fal],$tabla_b3[ped],$tabla_b3[xfal],$indice)";
    $qry_tabla_b = mysql_db_query($c_database,$qrystr_tabla_b,$link);
    $indice++;
   }

 //inserto mes x mes en la tabla_b
 $qrystr_tabla_a = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
                     VALUES ('TABLA_B','$tabla_c[mes]',$tabla_c[pvp],$tabla_c[pvp]/$tabla_c[rev],$tabla_c[pvp]/$tabla_c[lid],
                             $tabla_c[pvp]/$tabla_c[env],$tabla_c[env],$tabla_c[fal],$tabla_c[ped],
                             ($tabla_c[fal]/$tabla_c[ped])*100,$indice)";
 $qry_tabla_a = mysql_db_query($c_database,$qrystr_tabla_a,$link);
}

//obtengo filas de la tabla_b para su posterior calculo
$tabla_b_2 = " SELECT ref1 AS mes,val1 AS pvp,val2 AS xrev,val3 AS xlid,val4 AS xuni,ent1 AS env,ent2 AS fal,ent3 AS ped,val5 AS xfal
               FROM reporte
               WHERE clave_ses = 'TABLA_B' AND ent9=2";
$qry_tabla_b_2 = mysql_db_query($c_database,$tabla_b_2,$link);
$tabla_b2 = mysql_fetch_array($qry_tabla_b_2);
$tabla_b_3 = " SELECT ref1 AS mes,val1 AS pvp,val2 AS xrev,val3 AS xlid,val4 AS xuni,ent1 AS env,ent2 AS fal,ent3 AS ped,val5 AS xfal
               FROM reporte
               WHERE clave_ses = 'TABLA_B' AND ent9=3";
$qry_tabla_b_3 = mysql_db_query($c_database,$tabla_b_3,$link);
$tabla_b3 = mysql_fetch_array($qry_tabla_b_3);
$tabla_b_4 = " SELECT ref1 AS mes,val1 AS pvp,val2 AS xrev,val3 AS xlid,val4 AS xuni,ent1 AS env,ent2 AS fal,ent3 AS ped,val5 AS xfal
               FROM reporte
               WHERE clave_ses = 'TABLA_B' AND ent9=4";
$qry_tabla_b_4 = mysql_db_query($c_database,$tabla_b_4,$link);
$tabla_b4 = mysql_fetch_array($qry_tabla_b_4);
$tabla_b_5 = " SELECT ref1 AS mes,val1 AS pvp,val2 AS xrev,val3 AS xlid,val4 AS xuni,ent1 AS env,ent2 AS fal,ent3 AS ped,val5 AS xfal
               FROM reporte
               WHERE clave_ses = 'TABLA_B' AND ent9=5";
$qry_tabla_b_5 = mysql_db_query($c_database,$tabla_b_5,$link);
$tabla_b5 = mysql_fetch_array($qry_tabla_b_5);
 
//inserto las filas calculadas de la tabla_b
$qrystr_tabla_b6 = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
                     VALUES ('TABLA_B',CONCAT('$tabla_b3[mes]',' - ','$tabla_b2[mes]'),$tabla_b3[pvp]-$tabla_b2[pvp],
                             $tabla_b3[xrev]-$tabla_b2[xrev],$tabla_b3[xlid]-$tabla_b2[xlid],$tabla_b3[xuni]-$tabla_b2[xuni],
                             $tabla_b3[env]-$tabla_b2[env],$tabla_b3[fal]-$tabla_b2[fal],$tabla_b3[ped]-$tabla_b2[ped],
                             $tabla_b3[xfal]-$tabla_b2[xfal],6)";
$qry_tabla_b6 = mysql_db_query($c_database,$qrystr_tabla_b6,$link);
$qrystr_tabla_b7 = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
                     VALUES ('TABLA_B',CONCAT('$tabla_b5[mes]',' - ','$tabla_b3[mes]'),$tabla_b5[pvp]-$tabla_b3[pvp],
                             $tabla_b5[xrev]-$tabla_b3[xrev],$tabla_b5[xlid]-$tabla_b3[xlid],$tabla_b5[xuni]-$tabla_b3[xuni],
                             $tabla_b5[env]-$tabla_b3[env],$tabla_b5[fal]-$tabla_b3[fal],$tabla_b5[ped]-$tabla_b3[ped],
                             $tabla_b5[xfal]-$tabla_b3[xfal],7)";
$qry_tabla_b7 = mysql_db_query($c_database,$qrystr_tabla_b7,$link);
$qrystr_tabla_b8 = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
                     VALUES ('TABLA_B',CONCAT('$tabla_b5[mes]',' - ','$tabla_b4[mes]'),$tabla_b5[pvp]-$tabla_b4[pvp],
                             $tabla_b5[xrev]-$tabla_b4[xrev],$tabla_b5[xlid]-$tabla_b4[xlid],$tabla_b5[xuni]-$tabla_b4[xuni],
                             $tabla_b5[env]-$tabla_b4[env],$tabla_b5[fal]-$tabla_b4[fal],$tabla_b5[ped]-$tabla_b4[ped],
                             $tabla_b5[xfal]-$tabla_b4[xfal],8)";
$qry_tabla_b8 = mysql_db_query($c_database,$qrystr_tabla_b8,$link);

//obtengo datos de la ultima campa�ia para la tabla_d
$qrystr_distrib_ult = " INSERT INTO reporte (clave_ses,ref1,ref2,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent4,ent5,val6,val7,val8,ent9)
                        SELECT 'TABLA_D',CONCAT(u.apellido,' ',u.nombres),'$nombre_ultima_campania',
                               SUM(cpd.cantidad*cpd.preciounidad),SUM(cpd.cantidad*cpd.preciounidad)/COUNT(DISTINCT(cpd.id_vendedor)),
                               SUM(cpd.cantidad*cpd.preciounidad)/COUNT(DISTINCT(cpd.id_lider)),SUM((cpd.cantidad*cpd.preciounidad)/cpd.cantidad),
                               SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),       
                               SUM(cpd.cantidad_orig - cpd.cantidad)*100/SUM(cpd.cantidad_orig),
                               COUNT(DISTINCT(cpd.id_vendedor)),COUNT(DISTINCT(cpd.id_lider)),
                               COUNT(DISTINCT(cpd.id_vendedor))/COUNT(DISTINCT(cpd.id_lider)),
                               SUM(cpd.cantidad)/COUNT(DISTINCT(cpd.id_vendedor)),SUM(cpd.cantidad)/COUNT(DISTINCT(cpd.id_lider)),2
                        FROM comp_pedidos_defi AS cpd
                        INNER JOIN usuario AS u ON u.cod_us = cpd.id_distrib
                        WHERE cpd.id_vendedor<>'CAMBIO' AND 
			LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_regional = '$gerencia' $filtro_ultima_campania
                        GROUP BY cpd.id_distrib";
$qry_distrib_ult = mysql_db_query($c_database,$qrystr_distrib_ult,$link);

//obtengo datos de la anteultima campa�ia para la tabla_d
$qrystr_distrib_ant = " INSERT INTO reporte (clave_ses,ref1,ref2,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent4,ent5,val6,val7,val8,ent9)
                        SELECT 'TABLA_D',CONCAT(u.apellido,' ',u.nombres),'$nombre_anteultima_campania',
                               SUM(cpd.cantidad*cpd.preciounidad),SUM(cpd.cantidad*cpd.preciounidad)/COUNT(DISTINCT(cpd.id_vendedor)),
                               SUM(cpd.cantidad*cpd.preciounidad)/COUNT(DISTINCT(cpd.id_lider)),SUM((cpd.cantidad*cpd.preciounidad)/cpd.cantidad),
                               SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),       
                               SUM(cpd.cantidad_orig - cpd.cantidad)*100/SUM(cpd.cantidad_orig),
                               COUNT(DISTINCT(cpd.id_vendedor)),COUNT(DISTINCT(cpd.id_lider)),
                               COUNT(DISTINCT(cpd.id_vendedor))/COUNT(DISTINCT(cpd.id_lider)),
                               SUM(cpd.cantidad)/COUNT(DISTINCT(cpd.id_vendedor)),SUM(cpd.cantidad)/COUNT(DISTINCT(cpd.id_lider)),1
                        FROM comp_pedidos_defi AS cpd
                        INNER JOIN usuario AS u ON u.cod_us = cpd.id_distrib
                        WHERE cpd.id_vendedor<>'CAMBIO' AND 
			LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_regional = '$gerencia' $filtro_anteultima_campania
                        GROUP BY cpd.id_distrib";
$qry_distrib_ant = mysql_db_query($c_database,$qrystr_distrib_ant,$link);

//obtengo los registros de la tabla_d parra calcular la variacion
$recorre_tabla_d =" SELECT * FROM reporte WHERE clave_ses = 'TABLA_D' ORDER BY ref1,ent9";
$qry_recorre_tabla_d = mysql_db_query($c_database,$recorre_tabla_d,$link);
$tabla_d_array=array();
$ind=0;
while($tabla_d = mysql_fetch_array($qry_recorre_tabla_d))
{
  if($tabla_d[ent9]==1)
    {
     //obtengo los resultados de la anteultima campania para calcular la variacion
     $a_val1=$tabla_d[val1];
     $a_val2=$tabla_d[val2];
     $a_val3=$tabla_d[val3];
     $a_val4=$tabla_d[val4];
     $a_ent1=$tabla_d[ent1];
     $a_ent2=$tabla_d[ent2];
     $a_ent3=$tabla_d[ent3];
     $a_val5=$tabla_d[val5];
     $a_ent4=$tabla_d[ent4];
     $a_ent5=$tabla_d[ent5];
     $a_val6=$tabla_d[val6];
     $a_val7=$tabla_d[val7];
     $a_val8=$tabla_d[val8];
    } 
  if($tabla_d[ent9]==2)
    {
     //obtengo los resultados de la anteultima campania para calcular la variacion
     $ind++;
     $v_ref1=$tabla_d[ref1];
     $v_ref2='Variaci�n';
     $v_val1=$tabla_d[val1]-$a_val1;
     $v_val2=$tabla_d[val2]-$a_val2;
     $v_val3=$tabla_d[val3]-$a_val3;
     $v_val4=$tabla_d[val4]-$a_val4;
     $v_ent1=$tabla_d[ent1]-$a_ent1;
     $v_ent2=$tabla_d[ent2]-$a_ent2;
     $v_ent3=$tabla_d[ent3]-$a_ent3;
     $v_ent4=$tabla_d[ent4]-$a_ent4;
     $v_ent5=$tabla_d[ent5]-$a_ent5;
     $v_val6=$tabla_d[val6]-$a_val6;
     $v_val7=$tabla_d[val7]-$a_val7;
     $v_val8=$tabla_d[val8]-$a_val8;
     $tabla_d_array[$ind]="INSERT INTO reporte (clave_ses,ref1,ref2,val1,val2,val3,val4,ent1,ent2,ent3,ent4,ent5,val6,val7,val8,ent9)
                           VALUES ('TABLA_D','$v_ref1','$v_ref2',$v_val1,$v_val2,$v_val3,$v_val4,$v_ent1,$v_ent2,$v_ent3,$v_ent4,$v_ent5,$v_val6,$v_val7,$v_val8,3)";
    } 
}
//inserto la variacion de cada distribuidor
for($i=1;$i<=$ind;$i++)
{
 $qrystr_tabla_d_array=$tabla_d_array[$i];
 $qry_tabla_d_array = mysql_db_query($c_database,$qrystr_tabla_d_array,$link);
}

$tabla_a = " SELECT CONCAT(ref1,';',ent1,';',ent2,';',val1,';',val2,';',val3,';',ent3,';',val5,';',val6) AS c
             FROM reporte
             WHERE clave_ses = 'TABLA_A' AND ent9 <= 5
             ORDER BY ent9";
$tabla_a_v = " SELECT CONCAT(ref1,';',ent1,';',ent2,';',val1,';',val2,';',val3,';',ent3,';',val5,';',val6) AS c
               FROM reporte
               WHERE clave_ses = 'TABLA_A' AND ent9 >= 6
               ORDER BY ent9";
$tabla_b = " SELECT CONCAT(ref1,';',val1,';',val2,';',val3,';',val4,';',ent1,';',ent2,';',ent3,';',CONCAT(val5,'%')) AS c
             FROM reporte
             WHERE clave_ses = 'TABLA_B' AND ent9 <= 5
             ORDER BY ent9";
$tabla_b_v = " SELECT CONCAT(ref1,';',val1,';',val2,';',val3,';',val4,';',ent1,';',ent2,';',ent3,';',CONCAT(val5,'%')) AS c
               FROM reporte
               WHERE clave_ses = 'TABLA_B' AND ent9 >= 6
               ORDER BY ent9";

$tabla_e = " SELECT CONCAT(ref1,';',IF(ent9>2,CONCAT(val1,'%'),val1),';',IF(ent9>2,CONCAT(ent1,'%'),ent1),';',IF(ent9>2,CONCAT(ent2,'%'),ent2),';',IF(ent9>2,CONCAT(ent3,'%'),ent3),';',IF(ent9>2,CONCAT(ent4,'%'),ent4),';',IF(ent9>2,CONCAT(ent5,'%'),ent5)) AS c
             FROM reporte
             WHERE clave_ses = 'TABLA_E' AND ent9 <= 1
             ORDER BY ent9,ref1";
$tabla_e_v = " SELECT CONCAT(ref1,';',IF(ent9>2,CONCAT(val1,'%'),val1),';',IF(ent9>2,CONCAT(ent1,'%'),ent1),';',IF(ent9>2,CONCAT(ent2,'%'),ent2),';',IF(ent9>2,CONCAT(ent3,'%'),ent3),';',IF(ent9>2,CONCAT(ent4,'%'),ent4),';',IF(ent9>2,CONCAT(ent5,'%'),ent5)) AS c
               FROM reporte
               WHERE clave_ses = 'TABLA_E' AND ent9 >= 2
               ORDER BY ent9,ref1";
$tabla_d = " SELECT CONCAT(ref1,';',ref2,';',val1,';',val2,';',val3,';',val4,';',ent1,';',ent2,';',ent3,';',val5,';',ent4,';',ent5,';',val6,';',val7,';',val8) AS c
             FROM reporte
             WHERE clave_ses = 'TABLA_D'
             ORDER BY ref1,ent9";

switch($salida)
{
 case 'EXCEL':
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=array('Periodo','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid','Distrib','Ped x Dist','Lid x Dist');
      $lista->Celda('INFORME GERENCIAL: '.$nombregerente,'L');
      $lista->Ln();
      $lista->Celda('ESTRUCTURA','L');
      $lista->Ln();
      $data=$lista->LoadData($tabla_a);
      $lista->BasicTable($data);
      $lista->Ln();
      $lista->Celda('VARIACION','L');
      $lista->Ln();
      $data=$lista->LoadData($tabla_a_v);
      $lista->BasicTable($data);
      $lista->titulos=array('Distribuidor','$ PVP','Uni Env','Uni Fal','Uni Ped','Pedidos','Lider');
      $lista->Ln();
      $lista->Celda('ULTIMA CAMPA�A','L');
      $lista->Ln();
      $data=$lista->LoadData($tabla_e);
      $lista->BasicTable($data);
      $lista->titulos=array('Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal');
      $lista->Ln();
      $lista->Celda('SITUACION COMERCIAL','L');
      $lista->Ln();
      $data=$lista->LoadData($tabla_b);
      $lista->BasicTable($data);
      $lista->Ln();
      $lista->Celda('VARIACION','L');
      $lista->Ln();
      $data=$lista->LoadData($tabla_b_v);
      $lista->BasicTable($data);
 break;
 case 'PDF':
      include("pdf_set5.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $pdf->header_si=0;
      $pdf->AddPage();
      $nota="<br><B>Gener�:  <U>$usuario</U></B> <br>Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>www.vanesaduran.com</A> | Consultas en <A href='mailto:consultasweb@vanesaduran.com'>consultasweb@vanesaduran.com</A>,<br>";
      $header=array('Periodo','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid','Distrib','Ped x Dist','Lid x Dist');
      $anchos=array(50,18,18,18,18,18,18,18,18); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('L','R','R','R','R','R','R','R','R'); //L,R,C
      $total=array('TOTALES',1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $reporte="INFORME GERENCIAL: $nombregerente ";//nombre del reporte (en todos)
      $notaalpie="Informacion Confidencial. (Exclusiva del area Direccion)"; //nota al pie
      $pdf->SetFont('Arial','',8);
      
      $data=$pdf->LoadData($tabla_a);
      $pdf->Cell(array_sum($anchos),$pdf->altoFila,'ESTRUCTURA',1,0,'C');
      $pdf->Ln();
      for($i=0;$i<count($header);$i++)
          $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      $pdf->Ln();
      $pdf->BasicTable($header,$data,0);
      
      $data=$pdf->LoadData($tabla_e);
      $header=array('Distribuidor','$ PVP','Uni Env','Uni Fal','Uni Ped','Pedidos','Lider');
      $anchos=array(70,20,20,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('L','R','R','R','R','R','R'); //L,R,C
      $total=array(); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $pdf->Ln();
      $pdf->Cell(array_sum($anchos),$pdf->altoFila,'ULTIMO MES',1,0,'C');
      $pdf->Ln();
      for($i=0;$i<count($header);$i++)
          $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      $pdf->Ln();
      $pdf->BasicTable($header,$data,0);
      $data=$pdf->LoadData($tabla_e_v);
      $pdf->Ln();
      $pdf->Cell(array_sum($anchos),$pdf->altoFila,'PORCENTAJES',1,0,'C');
      $pdf->Ln();
      for($i=0;$i<count($header);$i++)
          $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      $pdf->Ln();
      $pdf->BasicTable($header,$data,0);
            
      $data=$pdf->LoadData($tabla_b);
      $header=array('Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal');
      $anchos=array(50,18,18,18,18,18,18,18,18); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('L','R','R','R','R','R','R','R','R'); //L,R,C
      $total=array(); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $pdf->Ln();
      $pdf->Cell(array_sum($anchos),$pdf->altoFila,'SITUACION COMERCIAL',1,0,'C');
      $pdf->Ln();
      for($i=0;$i<count($header);$i++)
          $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      $pdf->Ln();
      $pdf->BasicTable($header,$data,0);
      $data=$pdf->LoadData($tabla_b_v);
      $pdf->Ln();
      $pdf->Cell(array_sum($anchos),$pdf->altoFila,'VARIACION',1,0,'C');
      $pdf->Ln();
      for($i=0;$i<count($header);$i++)
          $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      $pdf->Ln();
      $pdf->BasicTable($header,$data,0);
 break;
}
$qrystr_distrib = " SELECT cpd.id_distrib,CONCAT(u.apellido,' ',u.nombres)
                    FROM comp_pedidos_defi AS cpd
		    INNER JOIN usuario AS u ON u.cod_us = cpd.id_distrib
                    WHERE cpd.id_vendedor<>'CAMBIO' AND 
		    cpd.id_regional = '$gerencia' $filtro_ultima_campania
                    GROUP BY cpd.id_distrib
                    ORDER BY cpd.id_distrib";//echo $qrystr_distrib;
$qry_distrib = mysql_db_query($c_database,$qrystr_distrib,$link);
while($distrib = mysql_fetch_array($qry_distrib))
{
 
 $select_tabla_a = "SELECT '$nombre_primera_campania' AS mes,COUNT(DISTINCT(id_vendedor)) AS ped,
                           COUNT(DISTINCT(id_lider)) AS lid,COUNT(DISTINCT(id_vendedor))/COUNT(DISTINCT(id_lider)) AS pxl,
                           SUM(cantidad_orig)/COUNT(DISTINCT(id_vendedor)) AS uxp,SUM(cantidad_orig)/COUNT(DISTINCT(id_lider)) AS uxl,1 AS orden
                    FROM comp_pedidos_defi AS cpd
                    WHERE cpd.id_vendedor<>'CAMBIO' AND 
		    LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_primera_campania";
 $qry_tabla_a = mysql_db_query($c_database,$select_tabla_a,$link);
 $tabla_a_1 = mysql_fetch_array($qry_tabla_a);
 $select_tabla_a = "SELECT '$nombre_segunda_campania' AS mes,COUNT(DISTINCT(id_vendedor)) AS ped,
                           COUNT(DISTINCT(id_lider)) AS lid,COUNT(DISTINCT(id_vendedor))/COUNT(DISTINCT(id_lider)) AS pxl,
                           SUM(cantidad_orig)/COUNT(DISTINCT(id_vendedor)) AS uxp,SUM(cantidad_orig)/COUNT(DISTINCT(id_lider)) AS uxl,2 AS orden
                    FROM comp_pedidos_defi AS cpd
                    WHERE cpd.id_vendedor<>'CAMBIO' AND 
		    LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_segunda_campania";
 $qry_tabla_a = mysql_db_query($c_database,$select_tabla_a,$link);
 $tabla_a_2 = mysql_fetch_array($qry_tabla_a);
 $select_tabla_a = "SELECT '$nombre_anteultima_campania' AS mes,COUNT(DISTINCT(id_vendedor)) AS ped,
                           COUNT(DISTINCT(id_lider)) AS lid,COUNT(DISTINCT(id_vendedor))/COUNT(DISTINCT(id_lider)) AS pxl,
                           SUM(cantidad_orig)/COUNT(DISTINCT(id_vendedor))AS uxp,SUM(cantidad_orig)/COUNT(DISTINCT(id_lider)) AS uxl,3 AS orden
                    FROM comp_pedidos_defi AS cpd
                    WHERE cpd.id_vendedor<>'CAMBIO' AND 
		    LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_anteultima_campania";
 $qry_tabla_a = mysql_db_query($c_database,$select_tabla_a,$link);
 $tabla_a_3 = mysql_fetch_array($qry_tabla_a);
 $select_tabla_a = "SELECT '$nombre_ultima_campania' AS mes,COUNT(DISTINCT(id_vendedor)) AS ped,
                           COUNT(DISTINCT(id_lider)) AS lid,COUNT(DISTINCT(id_vendedor))/COUNT(DISTINCT(id_lider)) AS pxl,
                           SUM(cantidad_orig)/COUNT(DISTINCT(id_vendedor)) AS uxp,SUM(cantidad_orig)/COUNT(DISTINCT(id_lider)) AS uxl,4 AS orden
                    FROM comp_pedidos_defi AS cpd
                    WHERE cpd.id_vendedor<>'CAMBIO' AND 
		    LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_ultima_campania";
 $qry_tabla_a = mysql_db_query($c_database,$select_tabla_a,$link);
 $tabla_a_4 = mysql_fetch_array($qry_tabla_a);
 
 $fila_1_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_1[0]',$tabla_a_1[1],$tabla_a_1[2],$tabla_a_1[3],$tabla_a_1[4],$tabla_a_1[5],1)";
 $qry_fila_1_a = mysql_db_query($c_database,$fila_1_a,$link);
 
 $fila_2_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_2[0]',$tabla_a_2[1],$tabla_a_2[2],$tabla_a_2[3],$tabla_a_2[4],$tabla_a_2[5],2)";
 $qry_fila_2_a = mysql_db_query($c_database,$fila_2_a,$link);
 
 $fila_3_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_3[0]',$tabla_a_3[1],$tabla_a_3[2],$tabla_a_3[3],$tabla_a_3[4],$tabla_a_3[5],3)";
 $qry_fila_3_a = mysql_db_query($c_database,$fila_3_a,$link);
 
 $fila_4_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','Media Trimestral',($tabla_a_1[1]+$tabla_a_2[1]+$tabla_a_3[1])/3,
                       ($tabla_a_1[2]+$tabla_a_2[2]+$tabla_a_3[2])/3,($tabla_a_1[3]+$tabla_a_2[3]+$tabla_a_3[3])/3,
                       ($tabla_a_1[4]+$tabla_a_2[4]+$tabla_a_3[4])/3,($tabla_a_1[5]+$tabla_a_2[5]+$tabla_a_3[5])/3,4)";
 $qry_fila_4_a = mysql_db_query($c_database,$fila_4_a,$link);
 
 $fila_5_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_4[0]',$tabla_a_4[1],$tabla_a_4[2],$tabla_a_4[3],$tabla_a_4[4],$tabla_a_4[5],5)";
 $qry_fila_5_a = mysql_db_query($c_database,$fila_5_a,$link);
 
 $fila_6_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_3[0]-$tabla_a_2[0]',$tabla_a_3[1]-$tabla_a_2[1],$tabla_a_3[2]-$tabla_a_2[2],
                       $tabla_a_3[3]-$tabla_a_2[3],$tabla_a_3[4]-$tabla_a_2[4],$tabla_a_3[5]-$tabla_a_2[5],6)";
 $qry_fila_6_a = mysql_db_query($c_database,$fila_6_a,$link);
 
 $fila_7_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_4[0]-$tabla_a_3[0]',$tabla_a_4[1]-$tabla_a_3[1],$tabla_a_4[2]-$tabla_a_3[2],
                       $tabla_a_4[3]-$tabla_a_3[3],$tabla_a_4[4]-$tabla_a_3[4],$tabla_a_4[5]-$tabla_a_3[5],7)";
 $qry_fila_7_a = mysql_db_query($c_database,$fila_7_a,$link);
 
 $fila_8_a = " INSERT INTO reporte (clave_ses,ref1,ent1,ent2,val1,val2,val3,ent9)
               VALUES ('TABLA_DIS_A','$tabla_a_4[0]-Media Trimestral',$tabla_a_4[1]-(($tabla_a_1[1]+$tabla_a_2[1]+$tabla_a_3[1])/3),
                       $tabla_a_4[2]-(($tabla_a_1[2]+$tabla_a_2[2]+$tabla_a_3[2])/3),$tabla_a_4[3]-(($tabla_a_1[3]+$tabla_a_2[3]+$tabla_a_3[3])/3),
                       $tabla_a_4[4]-(($tabla_a_1[4]+$tabla_a_2[4]+$tabla_a_3[4])/3),$tabla_a_4[5]-(($tabla_a_1[5]+$tabla_a_2[5]+$tabla_a_3[5])/3),8)";
 $qry_fila_8_a = mysql_db_query($c_database,$fila_8_a,$link);
 
 $select_tabla_b_1 = " SELECT '$nombre_primera_campania',SUM(cpd.cantidad*cpd.preciounidad),SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_vendedor)),
                            SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_lider)),SUM(cpd.preciounidad/cpd.cantidad),
                            SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),
                            (SUM(cpd.cantidad_orig - cpd.cantidad)/SUM(cpd.cantidad_orig))*100
                     FROM comp_pedidos_defi AS cpd
                     WHERE cpd.id_vendedor<>'CAMBIO' AND 
		     LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_primera_campania"; 
 $qry_sel_tabla_b_1 = mysql_db_query($c_database,$select_tabla_b_1,$link);
 $tabla_b_1 = mysql_fetch_array($qry_sel_tabla_b_1);
 verificar($qry_sel_tabla_b_1,$select_tabla_b_1);
 $select_tabla_b_2 = " SELECT '$nombre_segunda_campania',SUM(cpd.cantidad*cpd.preciounidad),SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_vendedor)),
                            SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_lider)),SUM(cpd.preciounidad/cpd.cantidad),
                            SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),
                            (SUM(cpd.cantidad_orig - cpd.cantidad)/SUM(cpd.cantidad_orig))*100
                     FROM comp_pedidos_defi AS cpd
                     WHERE cpd.id_vendedor<>'CAMBIO' AND 
		     LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_segunda_campania"; 
 $qry_sel_tabla_b_2 = mysql_db_query($c_database,$select_tabla_b_2,$link);
 $tabla_b_2 = mysql_fetch_array($qry_sel_tabla_b_2);
 verificar($qry_sel_tabla_b_2,$select_tabla_b_2);
 $select_tabla_b_3 = " SELECT '$nombre_anteultima_campania',SUM(cpd.cantidad*cpd.preciounidad),SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_vendedor)),
                            SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_lider)),SUM(cpd.preciounidad/cpd.cantidad),
                            SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),
                            (SUM(cpd.cantidad_orig - cpd.cantidad)/SUM(cpd.cantidad_orig))*100
                     FROM comp_pedidos_defi AS cpd
                     WHERE cpd.id_vendedor<>'CAMBIO' AND 
		     LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_anteultima_campania"; 
 $qry_sel_tabla_b_3 = mysql_db_query($c_database,$select_tabla_b_3,$link);
 $tabla_b_3 = mysql_fetch_array($qry_sel_tabla_b_3);
 verificar($qry_sel_tabla_b_3,$select_tabla_b_3);
 $select_tabla_b_5 = " SELECT '$nombre_ultima_campania',SUM(cpd.cantidad*cpd.preciounidad),SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_vendedor)),
                            SUM((cpd.cantidad*cpd.preciounidad))/COUNT(DISTINCT(id_lider)),SUM(cpd.preciounidad/cpd.cantidad),
                            SUM(cpd.cantidad),SUM(cpd.cantidad_orig - cpd.cantidad),SUM(cpd.cantidad_orig),
                            (SUM(cpd.cantidad_orig - cpd.cantidad)/SUM(cpd.cantidad_orig))*100
                     FROM comp_pedidos_defi AS cpd
                     WHERE cpd.id_vendedor<>'CAMBIO' AND 
		     LEFT(id_vd,1) IN('S','V','O','P') AND cpd.id_distrib = '$distrib[0]' $filtro_ultima_campania"; 
 $qry_sel_tabla_b_5 = mysql_db_query($c_database,$select_tabla_b_5,$link);
 $tabla_b_5 = mysql_fetch_array($qry_sel_tabla_b_5);
 verificar($qry_sel_tabla_b_5,$select_tabla_b_5);
 $fila_1_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_1[0]',$tabla_b_1[1],$tabla_b_1[2],$tabla_b_1[3],$tabla_b_1[4],$tabla_b_1[5],$tabla_b_1[6],$tabla_b_1[7],$tabla_b_1[8],1)";
 $qry_fila_1_b = mysql_db_query($c_database,$fila_1_b,$link);
 verificar($qry_fila_1_b,$fila_1_b);
 $fila_2_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_2[0]',$tabla_b_2[1],$tabla_b_2[2],$tabla_b_2[3],$tabla_b_2[4],$tabla_b_2[5],$tabla_b_2[6],$tabla_b_2[7],$tabla_b_2[8],2)";
 $qry_fila_2_b = mysql_db_query($c_database,$fila_2_b,$link);
 verificar($qry_fila_2_b,$fila_2_b);
 $fila_3_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_3[0]',$tabla_b_3[1],$tabla_b_3[2],$tabla_b_3[3],$tabla_b_3[4],$tabla_b_3[5],$tabla_b_3[6],$tabla_b_3[7],$tabla_b_3[8],3)";
 $qry_fila_3_b = mysql_db_query($c_database,$fila_3_b,$link);
 verificar($qry_fila_3_b,$fila_3_b);
 $fila_4_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','Media Trimestral',($tabla_b_1[1]+$tabla_b_2[1]+$tabla_b_3[1])/3,($tabla_b_1[2]+$tabla_b_2[2]+$tabla_b_3[2])/3,
                       ($tabla_b_1[3]+$tabla_b_2[3]+$tabla_b_3[3])/3,($tabla_b_1[4]+$tabla_b_2[4]+$tabla_b_3[4])/3,($tabla_b_1[5]+$tabla_b_2[5]+$tabla_b_3[5])/3,
                       ($tabla_b_1[6]+$tabla_b_2[6]+$tabla_b_3[6])/3,($tabla_b_1[7]+$tabla_b_2[7]+$tabla_b_3[7])/3,($tabla_b_1[8]+$tabla_b_2[8]+$tabla_b_3[8])/3,4)";
 $qry_fila_4_b = mysql_db_query($c_database,$fila_4_b,$link);
 verificar($qry_fila_4_b,$fila_4_b);
 $fila_5_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_5[0]',$tabla_b_5[1],$tabla_b_5[2],$tabla_b_5[3],$tabla_b_5[4],$tabla_b_5[5],$tabla_b_5[6],$tabla_b_5[7],$tabla_b_5[8],5)";
 $qry_fila_5_b = mysql_db_query($c_database,$fila_5_b,$link);
 verificar($qry_fila_5_b,$fila_5_b);
 $fila_6_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_3[0] - $tabla_b_2[0]',$tabla_b_3[1]-$tabla_b_2[1],$tabla_b_3[2]-$tabla_b_2[2],$tabla_b_3[3]-$tabla_b_2[3],$tabla_b_3[4]-$tabla_b_2[4],$tabla_b_3[5]-$tabla_b_2[5],$tabla_b_3[6]-$tabla_b_2[6],$tabla_b_3[7]-$tabla_b_2[7],$tabla_b_3[8]-$tabla_b_2[8],6)";
 $qry_fila_6_b = mysql_db_query($c_database,$fila_6_b,$link);
 verificar($qry_fila_6_b,$fila_6_b);
 $fila_7_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_5[0] - $tabla_b_3[0]',$tabla_b_5[1]-$tabla_b_3[1],$tabla_b_5[2]-$tabla_b_3[2],$tabla_b_5[3]-$tabla_b_3[3],$tabla_b_5[4]-$tabla_b_3[4],$tabla_b_5[5]-$tabla_b_3[5],$tabla_b_5[6]-$tabla_b_3[6],$tabla_b_5[7]-$tabla_b_3[7],$tabla_b_5[8]-$tabla_b_3[8],7)";
 $qry_fila_7_b = mysql_db_query($c_database,$fila_7_b,$link);
 verificar($qry_fila_7_b,$fila_7_b);
 $fila_8_b = " INSERT INTO reporte (clave_ses,ref1,val1,val2,val3,val4,ent1,ent2,ent3,val5,ent9)
               VALUES ('TABLA_DIS_B','$tabla_b_5[0] - Media Trimestral',$tabla_b_5[1]-($tabla_b_1[1]+$tabla_b_2[1]+$tabla_b_3[1])/3,
                       $tabla_b_5[2]-($tabla_b_1[2]+$tabla_b_2[2]+$tabla_b_3[2])/3,$tabla_b_5[3]-($tabla_b_1[3]+$tabla_b_2[3]+$tabla_b_3[3])/3,
                       $tabla_b_5[4]-($tabla_b_1[4]+$tabla_b_2[4]+$tabla_b_3[4])/3,$tabla_b_5[5]-($tabla_b_1[5]+$tabla_b_2[5]+$tabla_b_3[5])/3,
                       $tabla_b_5[6]-($tabla_b_1[6]+$tabla_b_2[6]+$tabla_b_3[6])/3,$tabla_b_5[7]-($tabla_b_1[7]+$tabla_b_2[7]+$tabla_b_3[7])/3,
                       $tabla_b_5[8]-($tabla_b_1[8]+$tabla_b_2[8]+$tabla_b_3[8])/3,8)";
 $qry_fila_8_b = mysql_db_query($c_database,$fila_8_b,$link);
 
 $tabla_dis_a = " SELECT CONCAT(ref1,';',ent1,';',ent2,';',val1,';',val2,';',val3) AS c
                  FROM reporte
                  WHERE clave_ses = 'TABLA_DIS_A' AND ent9 <= 5
                  ORDER BY ent9";
 $tabla_dis_a_v = " SELECT CONCAT(ref1,';',ent1,';',ent2,';',val1,';',val2,';',val3) AS c
                    FROM reporte
                    WHERE clave_ses = 'TABLA_DIS_A' AND ent9 >= 6
                    ORDER BY ent9"; 
 $tabla_dis_b = " SELECT CONCAT(ref1,';',val1,';',val2,';',val3,';',val4,';',ent1,';',ent2,';',ent3,';',val5) AS c
                  FROM reporte
                  WHERE clave_ses = 'TABLA_DIS_B' AND ent9 <= 5
                  ORDER BY ent9";
 $tabla_dis_b_v = " SELECT CONCAT(ref1,';',val1,';',val2,';',val3,';',val4,';',ent1,';',ent2,';',ent3,';',val5) AS c
                   FROM reporte
                   WHERE clave_ses = 'TABLA_DIS_B' AND ent9 >= 6
                   ORDER BY ent9";
 switch($salida)
 {
  case 'EXCEL':
       $lista->titulos=array('Periodo','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid');
       $lista->Ln();
       $lista->Celda('ESTRUCTURA DISTRIBUIDOR '.$distrib[1],'L');
       $lista->Ln();
       $data=$lista->LoadData($tabla_dis_a);
       $lista->BasicTable($data);
       $lista->Ln();
       $lista->Celda('VARIACION','L');
       $lista->Ln();
       $data=$lista->LoadData($tabla_dis_a_v);
       $lista->BasicTable($data);
       $lista->titulos=array('Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal');
       $lista->Ln();
       $lista->Celda('EVOLUCION DE VENTAS EN PESOS Y UNIDADES','L');
       $lista->Ln();
       $data=$lista->LoadData($tabla_dis_b);
       $lista->BasicTable($data);
       $lista->Ln();
       $lista->Celda('VARIACION','L');
       $lista->Ln();
       $data=$lista->LoadData($tabla_dis_b_v);
       $lista->BasicTable($data);
  break;
  case 'PDF':
       $reporte="INFORME GERENCIAL";
       $reporte1="DISTRIBUCION: $distrib[1]";
       $header=array();
       $pdf->header_si=0;
       $pdf->AddPage();
       $data=$pdf->LoadData($tabla_dis_a);
       $header=array('Periodo','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid');
       $anchos=array(60,20,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
       $alig=array('L','R','R','R','R','R'); //L,R,C
       $total=array(); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
       $pdf->Ln();
       $pdf->Cell(array_sum($anchos),$pdf->altoFila,'ESTRUCTURA',1,0,'C');
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
           $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->BasicTable($header,$data,0);
       $data=$pdf->LoadData($tabla_dis_a_v);
       $pdf->Ln();
       $pdf->Cell(array_sum($anchos),$pdf->altoFila,'VARIACION',1,0,'C');
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
           $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->BasicTable($header,$data,0);
       $header=array('Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal');
       $anchos=array(60,18,15,15,18,12,12,12,12); //anchos de cada celda procurar que sumen aprox 190-
       $alig=array('L','R','R','R','R','R','R','R','R'); //L,R,C
       $total=array(); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
       $data=$pdf->LoadData($tabla_dis_b);
       $pdf->Ln();
       $pdf->Cell(array_sum($anchos),$pdf->altoFila,'EVOLUCION DE VENTAS EN PESOS Y UNIDADES',1,0,'C');
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
           $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->BasicTable($header,$data,0);
       $data=$pdf->LoadData($tabla_dis_b_v);
       $pdf->Ln();
       $pdf->Cell(array_sum($anchos),$pdf->altoFila,'VARIACION',1,0,'C');
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
           $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->BasicTable($header,$data,0);
  break;
 }
 
 $qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_DIS_A'";
 $qry = mysql_db_query($c_database,$qrystr,$link);
 $qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_DIS_B'";
 $qry = mysql_db_query($c_database,$qrystr,$link);
}
switch($salida)
 {
  case 'EXCEL':
       $lista->titulos=array('Distribuidor','Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid');
       $lista->Ln();
       $lista->Celda('EVOLUCION MENSUAL POR DISTRIBUIDORES','L');
       $lista->Ln();
       $data=$lista->LoadData($tabla_d);
       $lista->BasicTable($data);
       $lista->OutPut("EXCEL");
  break;
  case 'PDF':
       $reporte="EVOLUCION MENSUAL POR DISTRIBUIDORES";
       $reporte1="";
       $header=array();
       $pdf->header_si=0;
       $pdf->AddPage('L');
       $data=$pdf->LoadData($tabla_d);
       $header=array('Distribuidor','Periodo','$ PVP','$ x Rev','$ x Lid','$Prom x Uni','Env','Fal','Ped','% Fal','Pedidos','Lideres','Ped x Lid','Uni x Ped','Uni x Lid');
       $anchos=array(60,25,18,15,15,18,12,12,12,12,15,15,15,15,15); //anchos de cada celda procurar que sumen aprox 190-
       $alig=array('L','R','R','R','R','R','R','R','R','R','R','R','R','R','R'); //L,R,C
       $total=array(); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
           $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->BasicTable($header,$data,0);
       $pdf->WriteHTML($nota);
       $pdf->OutPut(); 
  break;
 }
/*
$qrystr = "DELETE FROM reporte WHERE clave_ses='CAMPANIA'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_A'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_B'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_C'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_D'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$qrystr = "DELETE FROM reporte WHERE clave_ses='TABLA_E'";
$qry = mysql_db_query($c_database,$qrystr,$link);*/
?>
