<?php
include ("../../conexion.php");
include("pdf_set_eti1.php");
require_once('pdfbarcode128.inc');
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para Vanesa Dur�n');
$pdf->SetAutoPageBreak(false,0);
$pdf->AddPage();
//Recopilacion de Datos

////// PARA EL SCRIPT CUANDO ESTE SUBIDO ////// - DESCOMENTAR!!!!!!!!
// $camp_mar="1037";
// $camp_abr="1038";
// $camp_may="1039";
// $campanias="'1037','1038','1039'";
/////////////////////////////////////////////////////////////////////

$camp_mar="1020";
$camp_abr="1021";
$camp_may="1022";
$campanias="'1020','1021','1022'";

$camp_mar="1037";
$camp_abr="1038";
$camp_may="1039";
$campanias="'1037','1038','1039'";

  $qrystr="SELECT CONCAT(usu.apellido,' - ',usu.nombres) AS usuario,IF(cpd.id_vendedor=cpd.id_lider,', LIDER','') as cat, usu.localidad AS loc, usu.provincia AS prov, usu.mail AS mail,
            CONCAT(cpd.id_vendedor,' - ',IF(usu.cod_us=usu.id_lider,' - Lider',' - Revendedor/a'))as id_vendedor,MAX(ped.id_pedidos*(cro.campania=$camp_mar)) AS id_p_mar,
            SUM(cpd.cantidad_orig*(cro.campania=$camp_mar)) AS ped_mar,SUM(cpd.cantidad*(cro.campania=$camp_mar)) AS envi_mar, SUM(cpd.cantidad*cpd.preciounidad*(cro.campania=$camp_mar)) AS tot_mar,
            MAX(ped.id_pedidos*(cro.campania=$camp_abr)) AS id_p_abr, SUM(cpd.cantidad_orig*(cro.campania=$camp_abr)) AS ped_abr,SUM(cpd.cantidad*(cro.campania=$camp_abr)) AS envi_abr, SUM(cpd.cantidad*cpd.preciounidad*(cro.campania=$camp_abr)) AS tot_abr,
            MAX(ped.id_pedidos*(cro.campania=$camp_may)) AS id_p_may,SUM(cpd.cantidad_orig*(cro.campania=$camp_may)) AS ped_may,SUM(cpd.cantidad*(cro.campania=$camp_may)) AS envi_may, SUM(cpd.cantidad*cpd.preciounidad*(cro.campania=$camp_may)) AS tot_may,
            CONCAT(usud.apellido,' - ',usud.nombres) AS distribuidor,
            CONCAT(usur.apellido,' - ',usur.nombres) AS regional            
            FROM comp_pedidos_defi AS cpd 
            INNER JOIN pedido AS ped ON ped.id_pedidos=cpd.id_pedido
            INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion AND cro.campania IN($campanias)
            INNER JOIN usuario AS usu ON usu.cod_us=cpd.id_vendedor
            INNER JOIN usuario AS usud ON usu.id_distrib=usud.cod_us
            INNER JOIN usuario AS usur ON usu.id_regional=usur.cod_us
            INNER JOIN pedidos_defi AS pd ON pd.id_pedidos=cpd.id_pedido AND pd.id_vendedor=cpd.id_vendedor AND pd.un_com_s<>0
            WHERE ped.anexo=''
              AND usu.id_regional NOT IN('BAJWEB1','MEXVAN1','VANCHI1','BARMAR28','EXTCOM1')
            GROUP BY cpd.id_vendedor
            HAVING tot_mar<>0 AND tot_abr<>0 AND tot_may<>0 AND SUM(cpd.id_pedido*(cpd.id_vendedor=cpd.id_distrib))=0
            ORDER BY cpd.id_vendedor";
           
  $qry = mysql_db_query($c_database,$qrystr,$link);
  verificar('',$qrystr);
//       ECHO"$qrystr";


$ancho_hoja=215;
$alto_hoja=278;
$ancho_eti=200;
$alto_eti=29;
$esp_eti=0;
//$alto_renglon=5;
$lmargin=0;
$rmargin=0;
$tmargin=-5;
$bmargin=0;

$row=0;
$x=0;
$y=0;
$x=$lmargin;
$y=$tmargin;


//  if ($x+$ancho_eti>$ancho_hoja)
//      {
//       $y=$y+$alto_eti;
//       $x=$lmargin;
//      }
//  if ($y+$alto_eti>$alto_hoja)
//               {
//                $x=$lmargin;
//                $y=$tmargin;
//               }
//  $x=$x+$ancho_eti+$esp_eti;


while($row = mysql_fetch_array($qry))
{
 if ($x+$ancho_eti>$ancho_hoja)
     {
      $y=$y+$alto_eti+$esp_eti_h;
      $x=$lmargin;
     }
 if ($y+$alto_eti>$alto_hoja)
              {
               $pdf->AddPage();
               $x=$lmargin;
               $y=$tmargin;
              }

//   $pdf->AddPage();
//   $x=$lmargin;
//   $y=$tmargin;

$tot_ped=$row['ped_abr']+$row['ped_mar']+$row['ped_may'];
$tot_env=$row['envi_abr']+$row['envi_mar']+$row['envi_may'];
$tot_pvp=$row['tot_abr']+$row['tot_mar']+$row['tot_may'];
//  $pdf->h=30;

 $pdf->x=$x+5;
 $pdf->y=$y+15;
 $pdf->SetFont('Arial','',9);
 $pdf->ClippedCell(145,3,'Cod-Us: '. $row['id_vendedor'],1);

 $pdf->x=$x+5;
 $pdf->y=$y+19;

  $pdf->ClippedCell(65,3,'Usuario: '.$row['usuario']);
 $pdf->x=$x+5;
 $pdf->y=$y+23;
  $pdf->ClippedCell(65,3,'Localidad: '.$row['loc']);
 $pdf->x=$x+5;
 $pdf->y=$y+27;
  $pdf->ClippedCell(65,3,'Provincia: '.$row['prov']);
  $pdf->x=$x+5;
 $pdf->y=$y+31;
  $pdf->ClippedCell(65,3,'e-mail: '.$row['mail']);
  $pdf->x=$x+5;
 $pdf->y=$y+35;
  $pdf->ClippedCell(65,3,'Dist: '.$row['distribuidor']);
  $pdf->x=$x+5;
 $pdf->y=$y+39;
  $pdf->ClippedCell(65,3,'Reg: '.$row['regional']);

$x=70;
 $pdf->x=$x+5;
 $pdf->y=$y+15;
 $pdf->SetFont('Arial','',9);
 $pdf->ClippedCell(19,3,'Id-Pedidos',1);
 $pdf->x=$x+5;
 $pdf->y=$y+19;
  $pdf->ClippedCell(65,3,''.$row['id_p_mar']);
 $pdf->x=$x+5;
 $pdf->y=$y+23;
  $pdf->ClippedCell(65,3,''.$row['id_p_abr']);
 $pdf->x=$x+5;
 $pdf->y=$y+27;
  $pdf->ClippedCell(65,3,''.$row['id_p_may']);
  $pdf->x=$x+5;
 $pdf->y=$y+31;
  $pdf->ClippedCell(65,3,'Totales');
   
$x=90;
 $pdf->x=$x+4;
 $pdf->y=$y+15;
 $pdf->SetFont('Arial','',9);
 $pdf->ClippedCell(15,3,'Pedidas',1);
 $pdf->x=$x+8;
 $pdf->y=$y+19;
  $pdf->ClippedCell(65,3,''.$row['ped_mar']);
 $pdf->x=$x+8;
 $pdf->y=$y+23;
  $pdf->ClippedCell(65,3,''.$row['ped_abr']);
 $pdf->x=$x+8;
 $pdf->y=$y+27;
  $pdf->ClippedCell(65,3,''.$row['ped_may']);
  $pdf->x=$x+8;
 $pdf->y=$y+31;
  $pdf->ClippedCell(65,3,$tot_ped);
      
$x=105;
 $pdf->x=$x+4;
 $pdf->y=$y+15;
 $pdf->SetFont('Arial','',9);
 $pdf->ClippedCell(17,3,'Enviadas',1);
 $pdf->x=$x+8;
 $pdf->y=$y+19;
  $pdf->ClippedCell(65,3,''.$row['envi_mar']);
 $pdf->x=$x+8;
 $pdf->y=$y+23;
  $pdf->ClippedCell(65,3,''.$row['envi_abr']);
 $pdf->x=$x+8;
 $pdf->y=$y+27;
  $pdf->ClippedCell(65,3,''.$row['envi_may']);
  $pdf->x=$x+8;
 $pdf->y=$y+31;
  $pdf->ClippedCell(65,3,$tot_env);            
 $x=$x+$ancho_eti+$esp_eti;
 
$x=120;
 $pdf->x=$x+6;
 $pdf->y=$y+15;
 $pdf->SetFont('Arial','',9);
 $pdf->ClippedCell(65,3,'Total PVP');
 $pdf->x=$x+8;
 $pdf->y=$y+19;
  $pdf->ClippedCell(65,3,''.$row['tot_mar']);
 $pdf->x=$x+8;
 $pdf->y=$y+23;
  $pdf->ClippedCell(65,3,''.$row['tot_abr']);
 $pdf->x=$x+8;
 $pdf->y=$y+27;
  $pdf->ClippedCell(65,3,''.$row['tot_may']);
  $pdf->x=$x+8;
 $pdf->y=$y+31;
  $pdf->ClippedCell(65,3,$tot_pvp);            
 $x=$x+$ancho_eti+$esp_eti; 
}
// ----------------------------------------

$pdf->Output();
?>
