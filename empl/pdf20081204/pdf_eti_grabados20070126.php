<?php
include ("../../conexion.php");
include("pdf_set_eti1.php");
require_once('pdfbarcode128.inc');
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para MKS');
$pdf->SetAutoPageBreak(false,0);
$pdf->AddPage();
//Recopilacion de Datos
$listado = "";
$separa = "";
$j=0;
for ($i = 1; $i <= $cantidad; $i++)
 {
  $d="item$i";
  $res = ${$d};
  if($res<>'')
   {
    $j++;
    $listado .= $separa . $res;
    $separa = "','";
   }
 }
$lis = "IN('" . $listado . "')";

$qrystr = "SELECT g.*, CONCAT(g.id_vd,' (',g.nombreproducto,')') AS prod,
											 CONCAT(u1.apellido,', ',u1.nombres,' (',u1.cod_us,')') AS rev,
											 CONCAT(u1.apellido,', ',u1.nombres,' (',u1.cod_us,')') AS dis
           FROM grabados AS g
           INNER JOIN usuario AS u1 ON g.id_vendedor=u1.cod_us
           INNER JOIN usuario AS u2 ON g.id_distrib=u2.cod_us
           WHERE g.id_comp_ped $lis
           ORDER BY g.id_distrib, g.id_pedido, g.id_vendedor,g.id_vd";
$qry = mysql_db_query($c_database,$qrystr,$link);
verificar('',$qrystr);
//$row = mysql_fetch_array($qry);

//informe($row);


$ancho_hoja=215;
$alto_hoja=280;
$ancho_eti=90;
$alto_eti=70;
$esp_eti=15;
//$alto_renglon=5;
$lmargin=0;
$rmargin=0;
$tmargin=0;
$bmargin=0;

$row=0;
$x=0;
$y=0;
$x=$lmargin;
$y=$tmargin;

for($i=0;$i<$blanco;$i++)
{
 if ($x+$ancho_eti>$ancho_hoja)
     {
      $y=$y+$alto_eti;
      $x=$lmargin;
     }
 if ($y+$alto_eti>$alto_hoja)
              {
               $pdf->AddPage();
               $x=$lmargin;
               $y=$tmargin;
              }
 $x=$x+$ancho_eti+$esp_eti;
}

while($row = mysql_fetch_array($qry))
{
// $cod=$row[env].$row[lid];
// $code = new pdfbarcode128($cod, 1);
// $code->set_pdf_document($pdf);
// $width = $code->get_width();
 if ($x+$ancho_eti>$ancho_hoja)
     {
      $y=$y+$alto_eti;
      $x=$lmargin;
     }
 if ($y+$alto_eti>$alto_hoja)
              {
               $pdf->AddPage();
               $x=$lmargin;
               $y=$tmargin;
              }

 $pdf->x=$x+5;
 $pdf->y=$y+5;
 $pdf->SetFont('Arial','',10);
 $pdf->ClippedCell(65,3,"Grabado por: " . $row['usuario']);


 $pdf->x=$x+70;
 $pdf->y=$y+5;
 $pdf->ClippedCell(25,5,'N� de Pedido:',0,0,'C');
 $pdf->x=$x+70;
 $pdf->y=$y+10;
 $pdf->ClippedCell(25,5,$row['id_pedido'],1,0,'C');

 $pdf->x=$x+5;
 $pdf->y=$y+17;
 $pdf->SetFont('Arial','',8);
 $pdf->ClippedCell(90,3,'Distribuidor: ');
 $pdf->x=$x+5;
 $pdf->y=$y+21;
 $pdf->ClippedCell(90,3,$row['dis']);

 $pdf->x=$x+7;
 $pdf->y=$y+27;
 $pdf->ClippedCell(86,0,' ');

 $pdf->x=$x+5;
 $pdf->y=$y+29;
 $pdf->ClippedCell(90,3,'C�digo VD: ');
 $pdf->x=$x+5;
 $pdf->y=$y+33;
 $pdf->ClippedCell(90,3,$row['prod']);

 $pdf->x=$x+5;
 $pdf->y=$y+37;
 $pdf->SetFont('Arial','B',6);
 $pdf->ClippedCell(20,3,"Grabar:");


 $pdf->SetFont('Arial','',8);
 $grab=array();
 $maxwidth=90;
 $wmax=$maxwidth*1000/$pdf->FontSize;
 $cw=&$pdf->CurrentFont['cw'];
 $s=str_replace("\r",'',$row['grabacion']);
 $nb=strlen($s);
 $i=0;$l=0;$j=0;$ii=0;
 while($i<$nb)
	{
		//Get next character
		$c=$s[$i];
	  $l+=$cw[$c];
		if($l>$wmax)
		 {
		 	$grab[] = substr($s,$j,$i-$j);
		  $j=$i;
		  $l=0;
		 }
  $i++;
	}
 $grab[] = substr($s,$j);
 foreach($grab as $grabacion)
  {
	 if($ii<5)
	  {
	   $pdf->x=$x+5;
		 $pdf->y=$y+40+(4*$ii);
		 $pdf->ClippedCell($maxwidth,4,$grabacion);
	  }
	 $ii++;
  }

 $pdf->SetFont('Arial','',8);
 $pdf->x=$x+5;
 $pdf->y=$y+60;
 $pdf->ClippedCell(90,3,'Revendedor: ');
 $pdf->x=$x+5;
 $pdf->y=$y+64;
 $pdf->ClippedCell(90,3, $row['rev']);

 //$code->draw_barcode($x+60,$y+21,3,false);

 $x=$x+$ancho_eti+$esp_eti;
}


// ----------- borramos recopilacion de datos ----------

$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);

// ----------------------------------------

$pdf->Output();
?>
