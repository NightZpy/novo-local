<?php
include ("../../conexion.php");
$sesion=crear_clave_sesion();

$qrystr="SELECT CONCAT(ppd.distrib,';',ppd.pertenencia,';',CONCAT(camp.id_campania,' - ',camp.obs),';',
          ppd.pvp_c9,';',ppd.pvp_c10,';',ppd.pvp_c11,';',ppd.pvp_c12,';',ppd.pvp_c13,';',ppd.pvp_c14,';',
          ppd.pun_c9,';',ppd.pun_c10,';',ppd.pun_c11,';',ppd.pun_c12,';',ppd.pun_c13,';',ppd.pun_c14,';',ppd.puntaje) AS c
         FROM promo_puntos_dorados AS ppd
         INNER JOIN campanias AS camp ON ppd.camp_pert=camp.id_campania
         WHERE 1
         ORDER BY ppd.distrib,camp.id_campania
        ";
    
switch($salida)
{
 case 'PDF':
      include("inc_pdf/inc_function.php");
      include("pdf_set6.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $header=array('Distrib','Perten','Campa�a','Pvp-C9','Pvp-C10','Pvp-C11','Pvp-C12','Pvp-C13','Pvp-C14','Punt-C9','Punt-C10','Punt-C11','Punt-C12','Punt-C13','Punt-C14','Puntaje'); //encabezados de columnas (en todos)
      $anchos=array(20,17,40,15,15,15,15,15,15,15,15,15,15,15,15,15); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('C','C','L','R','R','R','R','R','R','R','R','R','R','R','R','C'); //L,R,C
      $comment="TOTALES";
      $total=array($comment,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $reporte="PDF de Promocion de Puntos Dorados";//nombre del reporte (en todos)
      $pdf->AddPage('L');
      $data=$pdf->LoadData($qrystr);
      $pdf->SetFont('Arial','',8);
      $pdf->BasicTable($header,$data,1);
      $pdf->WriteHTML($nota);
      $pdf->Output();
 break;
     
 case 'EXCEL':
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=array('Distrib','Perten','Campa�a','Pvp-C9','Pvp-C10','Pvp-C11','Pvp-C12','Pvp-C13','Pvp-C14','Punt-C9','Punt-C10','Punt-C11','Punt-C12','Punt-C13','Punt-C14','Puntaje');
      $data=$lista->LoadData($qrystr);
      $lista->BasicTable($data);
      $lista->OutPut("EXCEL");
 break;
}
?>
