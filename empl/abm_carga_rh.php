<?php
//***********************************************************//
//***********************************************************//
require_once('../clases/forms/class.forms_ale6.php');
require_once "empl/clases_interfaz.php";
require_once "estetica.css";
//####### OBLIGATORIOS
$tablasuper = "";          //La tabla del Supertipo.
$preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
$estapagina = "empl/abm_carga_rh.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
$par_pag_sig = "empl/arbol_rh_recorre.php";//La p�gina a mostrar si graba exitosamente.
 
 switch($tipo_carga)
   { 
     case 0: //EMPRESA// NO POSEE ABM DE CARGA 
     break;
     
     case 1: // DEPARTAMENTO //                 
         $tablasuper = "rh_area_x_dpto";     
         $titulo = "Carga de Areas del Departamento $nombre_nivel";   //El t�tulo a mostrar en el html.
         $clave_primaria = "id_area_x_dpto";            //El nombre del campo que es Clave Primaria.                    
     break;
          
     case 2: // AREA //    
         $tablasuper = "rh_puesto";                            
         $titulo = "Carga de Puestos del Area $nombre_nivel";   //El t�tulo a mostrar en el html.
         $clave_primaria = "id_puesto";              //El nombre del campo que es Clave Primaria. 
     break;

     case 3: // PUESTO //
         $tablasuper = "rh_usu_x_puesto";        
         $titulo = "Carga de Usuarios del Puesto $nombre_nivel";   //El t�tulo a mostrar en el html.
         $clave_primaria = "usu_x_puesto";              //El nombre del campo que es Clave Primaria.
     break;

     case 4: // USUARIO //
//          $tablasuper = "campanias";                
//          $titulo = "Altas y Modificaciones de Campa�as";   //El t�tulo a mostrar en el html.
//          $clave_primaria = "id_campania";              //El nombre del campo que es Clave Primaria.
 
     break;
   }
   
//***********************************************************//
//***********************************************************//
 $conf_tablas = array();                                     //Array que contiene las opciones para las tablas a mostrar.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
//***********************************************************// 
//***********************************************************//
 switch($tipo_carga)
   { 
     case 0: //EMPRESA// NO POSEE ABM DE CARGA 
     break;
     
     case 1: // DEPARTAMENTO //                                  
         $conf_tablas["$tablasuper"]->campos_usar = array("id_area_x_dpto","id_departamento","id_area","estado_activo","obs");
         $conf_tablas["$tablasuper"]->titulos_campos = array("id_area_x_dpto","id_departamento","Area","estado_activo","Observacion");
         $conf_tablas["$tablasuper"]->campos_hidden = array("id_area_x_dpto","id_departamento","estado_activo");
         array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_area","rh_areas","descrip","id_area"));
     break;
          
     case 2: // AREA //      
         $conf_tablas["$tablasuper"]->campos_usar = array("id_puesto","area_x_dpto","id_tipo_puesto","descripcion","estado_activo");
         $conf_tablas["$tablasuper"]->titulos_campos = array("id_puesto","area_x_dpto","Puesto:","Descripcion:");
         $conf_tablas["$tablasuper"]->campos_hidden = array("id_puesto","area_x_dpto","estado_activo");
         array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_tipo_puesto","rh_puesto_tipo","descrip","id_tipo_puesto"));
     break;

     case 3: // PUESTO //
//          $conf_tablas["$tablasuper"]->campos_usar = array("usu_x_puesto","usuario","id_puesto","estado_activo","inicio","fin","obs");
//          $conf_tablas["$tablasuper"]->titulos_campos = array("usu_x_puesto","Usuario:","id_puesto","Estado:","inicio","fin","Observacion:");
//          $conf_tablas["$tablasuper"]->campos_hidden = array("usu_x_puesto","id_puesto","inicio","fin");
//          //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_area","rh_areas","descrip","id_area"));
//          $qrystrth = "SELECT cod_us as cod,CONCAT(apellido,'-',nombres) as descri FROM usuario WHERE cateusu NOT LIKE ('%R%') ORDER BY cod";
//          array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("usuario","descri","cod",$qrystrth));
          
     break;

     case 4: // USUARIO //
//          $tablasuper = "campanias";                
//          $titulo = "Altas y Modificaciones de Campa�as";   //El t�tulo a mostrar en el html.
//          $clave_primaria = "id_campania";              //El nombre del campo que es Clave Primaria.
     break;
   }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                                                  ///
///                                                                                                                  ///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = "";
   ${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }

   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //if ($accion_abm == 1)         //1 = ALTA.
   //  $form->addHiddenForm("idh","$par_idh");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   $form->addHiddenForm("tipo_carga","$tipo_carga"); 
   $form->addHiddenForm("abre_camino","$abre_camino");  
   $form->addHiddenForm("generacion","$gene"); 
   $form->addHiddenForm("estado_activo","1"); 
         
     switch($tipo_carga)
       { 
         case 0: //EMPRESA// NO POSEE ABM DE CARGA 
            //$form->addHiddenForm("id_departamento","$padre");
         break;
         
         case 1: // DEPARTAMENTO //       
            $gene=1;                           
            $form->addHiddenForm("id_departamento","$padre"); 
            //$form->addHiddenForm("par_pag_sig","$par_pag_sig");    
         break;
              
         case 2: // AREA //    
            $gene=2;
            $form->addHiddenForm("area_x_dpto","$padre");
         break;
    
         case 3: // PUESTO //
            //$gene=3;
            $form->addHiddenForm("id_puesto","$padre");
             //$form->addHiddenForm("usuario","$padre");
         break;
    
         case 4: // USUARIO //
        break;
      }    
   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
      
      switch($tipo_carga)
       { 
         case 0: //EMPRESA// NO POSEE ABM DE CARGA 
            //$form->addHiddenForm("id_departamento","$padre");
         break;
         
         case 1: // DEPARTAMENTO //  

              $qrystr_ver="SELECT id_area_x_dpto FROM rh_area_x_dpto WHERE id_area='$id_area' AND id_departamento='$id_departamento'";
              $qry_ver= mysql_db_query($c_database,$qrystr_ver,$link);
              $row_ver = mysql_fetch_array($qry_ver);
              if($row_ver[0]=="")     
              {
                     if ($form->submit())
                      {
              		     if($renueva_sesion == 'S')
                              $sesion = sesion_ok_ns($usuario,$sesion,4000);
              
                       if($cierra_al_grabar == 'S')
                         echo "<script language='javascript'>close();</script>";
                       else
                        {
                         if ($accion_abm == 1)
                          {
                           echo "<b>Se ha generado el Area</b>";
              
                          }
                         else
                             echo "<b></b>";
              
                         include ($par_pag_sig);
                        }
               	    	} 
                 	     else
                         {
                 		     echo $error;
                 		     echo mysql_error();
                 		     echo "\n<br><a href=$PHP_SELF>Back</a>";
                 		    }
                }
                else
                {
                $qrystr_act="UPDATE rh_area_x_dpto SET estado_activo='1' WHERE id_area='$id_area' AND id_departamento='$id_departamento'";
                $qry_act = mysql_db_query($c_database,$qrystr_act,$link); 
                echo "<b>Esta Area no puede ser dada de alta debido a que ya existia, se reactivo la existente </b>";
                include ($par_pag_sig);
                }
         break;
              
         case 2: // AREA //    
                     if ($form->submit())
                      {
              		     if($renueva_sesion == 'S')
                              $sesion = sesion_ok_ns($usuario,$sesion,4000);
              
                       if($cierra_al_grabar == 'S')
                         echo "<script language='javascript'>close();</script>";
                       else
                        {
                         if ($accion_abm == 1)
                          {
                           echo "<b>Se ha generado el Puesto</b>";
              
                          }
                         else
                             echo "<b></b>";
              
                         include ($par_pag_sig);
                        }
               	    	}
               	    	else
                        {
                		     echo $error;
                		     echo mysql_error();
                		     echo "\n<br><a href=$PHP_SELF>Back</a>";
                		    } 
         break;
       }


	    }
    }
   else
    {
    $t=$g_bgolor;
    $marco1 = new Marco("tabla_frente2","tabla_frente2_",$t);
    $marco1->abrirMarco();
    
 	 echo $form->build("200","10","200","3","40");

       	 
    $marco1->cerrarMarco();
     	 include "empl/menu_pedidos_data_n1.php";
     	 echo "<a href=administracion.php?usuario=$usuario&sesion=$sesion&abre_camino=$padre&generacion=$gene&pagina=empl/arbol_rh_recorre.php><font><b>Volver a la Estructura de Recursos Humanos</b></font></a><br><br>";

	  }

?>


