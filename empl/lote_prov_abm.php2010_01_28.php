<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
 require_once('../clases/forms/class.forms_ale6.php');
require_once "clases_interfaz.php";

 //####### OBLIGATORIOS
 $tablasuper = "lote_proveedor";          //La tabla del Supertipo.
 $preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/lote_prov_abm.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/lote_prov_lista.php";//La p�gina a mostrar si graba exitosamente.

 $titulo = "Alta y Modificaci�n de Lotes de Proveedor";   //El t�tulo a mostrar en el html.
 $clave_primaria = "id_lote";              //El nombre del campo que es Clave Primaria.
 
 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_lote","id_proveedor","obs","nro_control","nro_factura");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_lote","Proveedor","Observaciones","N� Ctrl","N� Fact");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_lote","id_proveedor");
 //Las validaciones de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (NumeroValido('Estado',Form.estado,0,1)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Desde',Form.desde)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Hasta',Form.hasta)) return false;");
 //Los COMBOS de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_proveedor","proveedor","proveedor_descrip","id_proveedor"));
$qrystramedida = "SELECT p.id_proveedor,CONCAT(p.id_proveedor, '  -',p.proveedor_descrip) AS paq
                      FROM proveedor AS p INNER JOIN proveedor_tipo AS pt ON p.id_tipo=pt.id_tipo
                      WHERE pt.id_tipo=1 ORDER BY p.proveedor_descrip";
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("id_proveedor","paq","id_proveedor",$qrystramedida));
 //Las FECHAS de la tabla supertipo.
 //$conf_tablas["$tablasuper"]->fechas = array("fecha_generado","fecha_recibido");
//***********************************************************
//******** FIN ESTO VA A IR EN OTRO ARCHIVO .inc ************



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           DB Layer Example Page - Inserting             #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


 /*Variables de entrada (de control)
 par_id                -- El Producto a mostrar en el ABM. Si es "" es una ALTA.
 par_idh               -- El Hotel elegido si es una ALTA.
 */

 if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = nuevo_id($tablasuper,$clave_primaria);
   ${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.


   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
    //$form->addHiddenForm("tax","$tax");
   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   if ($accion_abm == 1)         //1 = ALTA.
     $form->addHiddenForm("us_generado",$usuario);      //As� se setea un campo de pecho. No importa si se modifica por el usuario.

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
                $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if ($accion_abm == 1)   //Si es una alta actualizo los datos de usuario y momento de Generaci�n.
          {
           $qrystrup="UPDATE lote_proveedor
                      SET
                        fecha_generado = CURRENT_TIMESTAMP,
                        us_generado='$usuario'
                      WHERE id_lote = $nuevo ";
           $qryup = mysql_db_query($c_database,$qrystrup,$link);
          }

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
          {
           if ($accion_abm == 1)
               echo "<b>Se ha generado el lote de Proveedor $nuevo</b>";
           else
               echo "<b>Se ha modificado el lote de Proveedor $par_id</b>";
           
           include ($par_pag_sig);
          }
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
     $t=$g_bgolor;
     $marco1 = new Marco("img","tabla_",$t);
     $marco1->abrirMarco();
   	 echo $form->build("200","10","200","3","40");
     $marco1->cerrarMarco();
   	 include "menu_pedidos_data_n1.php";
	  }


//################################################################################################
function nuevo_id($tablasuper,$campo)
 {
  global $link;
  global $c_database;

  $strConsulta = "SELECT MAX(`$campo`)+1
                  FROM $tablasuper " ;
  $qrynuevo = mysql_db_query($c_database,$strConsulta,$link);
  $rownuevo = mysql_fetch_array($qrynuevo);
  return $rownuevo[0];
 }


?>
