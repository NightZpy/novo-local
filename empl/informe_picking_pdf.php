<?php
include("../../conexion.php");
	require "../../../clases/xls/PHPExcel.php";


	// Instanciar la clase
	$objPHPExcel= new PHPExcel();

	// Información para el Excel
	$objPHPExcel->getProperties()
	->setCreator("www.vanesaduranjoyas.com")
	->setLastModifiedBy("www.vanesaduranjoyas.com")
	->setTitle("Informe	de Picking")
	->setSubject("Informe para Excel");
 	
		if(empty($_POST[funcionario])){
				$filtro2="";
		}else{
			$qrystr = "SELECT ini
			   FROM armadoras WHERE usuario='".$_POST[funcionario]."'";
			$qry= mysql_db_query($c_database,$qrystr,$link);
			$row= mysql_fetch_array($qry);
			$ini=$row[ini];
			$func=$_POST[funcionario];
			$filtro2="AND ar.ini= '$ini'";	
			$ussu="AND (ini='".$ini."' OR ini_control='".$ini."')";
		}
	

				$estiloTituloReporte = array(
					'font' => array(
						'name'      => 'Verdana',
						'bold'      => true,
						'italic'    => false,
						'strike'    => false,
						'size' =>11,
						'color'     => array(
							'rgb' => 'FFFFFF'
						)
					),
					'fill' => array(
						'type'  => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array(
							'argb' => 'FF220835')
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_NONE
						)
					),
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'rotation' => 0,
						'wrap' => TRUE
					)
				);
				
		//$ussu2="AND us_control_lider='".$_POST[funcionario]."'";


if($_POST[tipoinforme]=='resumen'){
	
		$filtro= "AND fecha = '$_POST[fecha]'";
		
		$qrystrV0 = "SELECT aa.usuario as funcionario, COUNT(id_vendedor) AS cant_pedidos_rec, SUM(cantidad) as cant_unid_rec,  SEC_TO_TIME( SUM( TIMEDIFF( tiempo_fin_recolecta, momento ) ) ) AS tiempo_tardo_rec
							FROM armados as ar
							INNER JOIN armadoras AS aa ON aa.ini=ar.ini
							WHERE tipo =1
							$filtro
							$filtro2
							GROUP BY ar.ini";	
		
		$qryV0 = mysql_db_query($c_database,$qrystrV0,$link);
		$cantidad0=mysql_num_rows($qryV0);
		
			//	echo $qrystrV0;		
		$qrystrV1 = "SELECT aa.usuario as funcionario, COUNT(id_vendedor) as cant_pedidos_ver, SUM(cantidad) as cant_unid_ver, SEC_TO_TIME( SUM( TIMEDIFF( tiempo_fin_control, tiempo_ini_control ) ) ) AS tiempo_tardo_ver
							FROM armados as ar
							INNER JOIN armadoras AS aa ON aa.ini=ar.ini_control
							WHERE tipo =1
							$filtro
							$filtro2
							GROUP BY ar.ini_control";
		//	echo $qrystrV1;		

		$qryV1 = mysql_db_query($c_database,$qrystrV1,$link);
		$cantidad1=mysql_num_rows($qryV1);
				
			 if($cantidad0>0 or $cantidad1>0){
				 
				 if(empty($_POST[funcionario])){
					$sql = "CREATE TEMPORARY TABLE `temp_picking` (
					  `id_temp_picking` int(11) NOT NULL AUTO_INCREMENT,
					  `fecha` date NOT NULL DEFAULT '0000-00-00',
					  `funcionario` varchar(20) NOT NULL DEFAULT '', 
					  `cant_pedidos_rec` int(11) NOT NULL DEFAULT '0',
					  `cant_unid_rec` int(11) NOT NULL DEFAULT '0',
					  `tiempo_tardo_rec` time NOT NULL,
					  `cant_pedidos_ver` int(11) NOT NULL DEFAULT '0',
					  `cant_unid_ver` int(11) NOT NULL DEFAULT '0',
					  `tiempo_tardo_ver` time NOT NULL,
					  `total_pedidos`int(11) NOT NULL DEFAULT '0',
					  `total_unidades`int(11) NOT NULL DEFAULT '0',
					  `tiempo_total` time NOT NULL,
					  PRIMARY KEY (`id_temp_picking`))";
				
					  
					$qry = mysql_db_query($c_database, $sql, $link);
					if ($cantidad0>0){
						while ($rowV0=mysql_fetch_array($qryV0)){
							$qrystr="INSERT INTO temp_picking (fecha,funcionario,cant_pedidos_rec,cant_unid_rec,tiempo_tardo_rec,cant_pedidos_ver,cant_unid_ver,
							tiempo_tardo_ver,total_pedidos,total_unidades,tiempo_total)
								  VALUES ('$_POST[fecha]','$rowV0[funcionario]',$rowV0[cant_pedidos_rec],$rowV0[cant_unid_rec],'$rowV0[tiempo_tardo_rec]',0,0,
								  '00:00:00',$rowV0[cant_pedidos_rec],$rowV0[cant_unid_rec],'$rowV0[tiempo_tardo_rec]')";
						//	echo $qrystr;
							$qry = mysql_db_query($c_database, $qrystr,$link);
							$qrystr="";
						//	echo $qrystr;
							$qry = mysql_db_query($c_database, $qrystr, $link);
						}
					}
					if ($cantidad1>0){
						while ($rowV1=mysql_fetch_array($qryV1)){
							$qrystr="UPDATE temp_picking 
							SET cant_pedidos_ver=cant_pedidos_ver+$rowV1[cant_pedidos_ver],
								cant_unid_ver=cant_unid_ver+$rowV1[cant_unid_ver],
								tiempo_tardo_ver='$rowV1[tiempo_tardo_ver]',
								total_pedidos=total_pedidos+$rowV1[cant_pedidos_ver],
								total_unidades=total_unidades+$rowV1[cant_unid_ver],
								tiempo_total=ADDTIME(tiempo_total,'$rowV1[tiempo_tardo_ver]')
								WHERE funcionario='$rowV1[funcionario]'";
						//	echo $qrystr;
							$qry = mysql_db_query($c_database, $qrystr, $link);
						}
					}
				}
			//***************************************************************

				$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($estiloTituloReporte);


			
					// titulos
					$objPHPExcel->getActiveSheet()
					->setCellValue('A1',"Fecha")
					->setCellValue('B1',"Funcionario")
					->setCellValue('C1',"Cant de pedidos rec")
					->setCellValue('D1',"Cant de Unidades rec")
					->setCellValue('E1',"Tiempo total de recolecta")
					->setCellValue('F1',"Cant de pedidos ver")
					->setCellValue('G1',"Cantidad de unidades ver")
					->setCellValue('H1',"Tiempo total en verificar")
					->setCellValue('I1','Total de pedidos ')
					->setCellValue('J1','Total de unidades ')
					->setCellValue('K1','Tiempo total ');
						  
			 
				if(empty($_POST[funcionario])){
				 	    $qrystrV="SELECT * FROM temp_picking "; 
						$qryV = mysql_db_query($c_database,$qrystrV,$link);
						$cantidad=mysql_num_rows($qryV);

					 			
				$i=2;
				while($registros=mysql_fetch_object($qryV)){

					$objPHPExcel->setActiveSheetIndex(0)
				   
					->setCellValue('A'.$i,$registros->fecha)
					->setCellValue('B'.$i,$registros->funcionario)
					->setCellValue('C'.$i,$registros->cant_pedidos_rec)
					->setCellValue('D'.$i,$registros->cant_unid_rec)
					->setCellValue('E'.$i,$registros->tiempo_tardo_rec)
					->setCellValue('F'.$i,$registros->cant_pedidos_ver)
					->setCellValue('G'.$i,$registros->cant_unid_ver)
					->setCellValue('H'.$i,$registros->tiempo_tardo_ver)
					->setCellValue('I'.$i,$registros->total_pedidos) 
					->setCellValue('J'.$i,$registros->total_unidades)
					->setCellValue('k'.$i,$registros->tiempo_total);	
					
						$i++;
					}// End while
						
			}/*else{	
					$objPHPExcel->getActiveSheet()
				   
					->setCellValue('A2',$_POST[fecha])
					->setCellValue('B2',$func)
					->setCellValue('C2',$rowV0[cant_pedidos_rec])
					->setCellValue('D2',$rowV0[cant_unid_rec]);
					
					$objPHPExcel->getActiveSheet()->setCellValue('E2', PHPExcel_Shared_Date::PHPToExcel($rowV0[Tiempo_tardo_rec]));
					$objPHPExcel->getActiveSheet()->getStyle('E2')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
					
					$objPHPExcel->getActiveSheet()
					->setCellValue('F2',$rowV1[cant_pedidos_ver])
					->setCellValue('G2',$rowV1[cant_unid_ver])
					->setCellValue('H2',$rowV1[Tiempo_tardo_ver])
					->setCellValue('I2','=SUM(C2+F2)')
					->setCellValue('J2','=SUM(D2+G2)');
						
					$objPHPExcel->getActiveSheet()->setCellValue('K2', PHPExcel_Shared_Date::PHPToExcel('=SUM(E2+H2)'));
					$objPHPExcel->getActiveSheet()->getStyle('K2')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
					
					
				 }		*/
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment;filename="_'.date("Ymd_Hms").'.xls"');
					header('Cache-Control: max-age=0');


					$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5'); // Excel 97, 2000 xls format

					$objWriter->save('php://output');
					exit;
					
		  }else{
			  echo "<div class='error'> No hay registros para mostrar</div>";
		  }
	
}else{
		
	if ($_POST[filtro2]=='porpedido'){
		$filtro= "AND id_pedido = $_POST[pedido]";
	}else{
		$filtro= "AND fecha = '$_POST[fecha]'";
	}
		
	if ($_POST[actividad]=='1'){ // recolecta y verifica
		$muestra="ini as Recolector, momento as Fecha_inicio_recolecta, tiempo_fin_recolecta as Fecha_fin_recolecta, TIMEDIFF(tiempo_fin_recolecta, momento) as Tiempo_tardo_recolectar, SEC_TO_TIME(TIMEDIFF(tiempo_fin_recolecta,momento)/ar.cantidad) as Unidad_x_tiempo_rec,
		 ini_control as Verificador,fecha_control as Fecha_verifica,tiempo_ini_control as Fecha_inicio_verifica, tiempo_fin_control as Fecha_fin_verifica,TIMEDIFF(tiempo_fin_control, tiempo_ini_control) as Tiempo_tardo_verificar, SEC_TO_TIME(TIMEDIFF(tiempo_fin_control,momento)/ar.cantidad) as Unidad_x_tiempo";


		$qrystrV0 = "SELECT id_pedido as Pedido,id_vendedor as Revendedor,fecha as Fecha,cantidad as Cant_Unidades, $muestra
							FROM armados AS ar
							WHERE tipo =1
							$filtro
							$ussu";
	//	echo $qrystrV0;
		$qryV0 = mysql_db_query($c_database,$qrystrV0,$link);

			$sql = "CREATE TEMPORARY TABLE `temp_picking` (
			  `id_temp_picking` int(11) NOT NULL AUTO_INCREMENT,
			  `Pedido`  varchar(20) NOT NULL DEFAULT '',
			  `Revendedor`  varchar(20) NOT NULL DEFAULT '',
			  `Fecha` date NOT NULL DEFAULT '0000-00-00',
			  `Cant_Unidades` int(11) NOT NULL DEFAULT '0',
			  `Recolector` varchar(20) NOT NULL DEFAULT '',
			  `Fecha_inicio_recolecta` time NOT NULL,
			  `Fecha_fin_recolecta` time NOT NULL,
			  `Tiempo_tardo_recolectar` time NOT NULL,
			  `Unidad_x_tiempo_rec` time NOT NULL,
			  `Verificador` varchar(20) NOT NULL DEFAULT '',
			  `Fecha_verifica` date NOT NULL DEFAULT '0000-00-00',
			  `Fecha_inicio_verifica`time NOT NULL,
			  `Fecha_fin_verifica` time NOT NULL,
			  `Tiempo_tardo_verificar` time NOT NULL,
			  `Unidad_x_tiempo` time NOT NULL,
			  PRIMARY KEY (`id_temp_picking`))";
			  
			$qry = mysql_db_query($c_database, $sql, $link);
			while ($rowV0=mysql_fetch_array($qryV0)){

				if(($rowV0[Recolector]==$ini) and ($rowV0[Verificador]==$ini)){
						$qrystr="INSERT INTO temp_picking (Pedido,Revendedor,Fecha,Cant_Unidades,Recolector,Fecha_inicio_recolecta,
					Fecha_fin_recolecta,Tiempo_tardo_recolectar,Unidad_x_tiempo_rec,
					Verificador,Fecha_verifica, Fecha_inicio_verifica,Fecha_fin_verifica,Tiempo_tardo_verificar,Unidad_x_tiempo)
					  VALUES ($rowV0[Pedido],'$rowV0[Revendedor]','$rowV0[Fecha]',$rowV0[Cant_Unidades],'$func','$rowV0[Fecha_inicio_recolecta]',
					  '$rowV0[Fecha_fin_recolecta]','$rowV0[Tiempo_tardo_recolectar]','$rowV0[Unidad_x_tiempo_rec]',
					  '$func','$rowV0[Fecha_verifica]','$rowV0[Fecha_inicio_verifica]','$rowV0[Fecha_fin_verifica]','$rowV0[Tiempo_tardo_verificar]',
					   '$rowV0[Unidad_x_tiempo]')";
				}elseif($rowV0[Recolector]==$ini){ // borrar verificador
					$qrystr="INSERT INTO temp_picking (Pedido,Revendedor,Fecha,Cant_Unidades,Recolector,Fecha_inicio_recolecta,
					Fecha_fin_recolecta,Tiempo_tardo_recolectar,Unidad_x_tiempo_rec,
					Verificador,Fecha_verifica, Fecha_inicio_verifica,Fecha_fin_verifica,Tiempo_tardo_verificar,Unidad_x_tiempo)
					  VALUES ($rowV0[Pedido],'$rowV0[Revendedor]','$rowV0[Fecha]',$rowV0[Cant_Unidades],'$func','$rowV0[Fecha_inicio_recolecta]',
					  '$rowV0[Fecha_fin_recolecta]','$rowV0[Tiempo_tardo_recolectar]','$rowV0[Unidad_x_tiempo_rec]','','','','','','')";
				}else{ //borrar recolector
					$qrystr="INSERT INTO temp_picking (Pedido,Revendedor,Fecha,Cant_Unidades,Recolector,Fecha_inicio_recolecta,
					Fecha_fin_recolecta,Tiempo_tardo_recolectar,Unidad_x_tiempo_rec,
					Verificador,Fecha_verifica, Fecha_inicio_verifica,Fecha_fin_verifica,Tiempo_tardo_verificar,Unidad_x_tiempo)
					  VALUES ($rowV0[Pedido],'$rowV0[Revendedor]','$rowV0[Fecha]',$rowV0[Cant_Unidades],'','','','','',
					  '$func','$rowV0[Fecha_verifica]','$rowV0[Fecha_inicio_verifica]','$rowV0[Fecha_fin_verifica]','$rowV0[Tiempo_tardo_verificar]',
					   '$rowV0[Unidad_x_tiempo]')";
				  }
				//  echo $qrystr;
			 $qry = mysql_db_query($c_database,$qrystr, $link);
				 
			}
	}
		/*	$arr_tit = array("Pedido","Revendedor","Fecha","Cant Unidades","Recolector","Inicio recolecta",
			"Fin recolecta","Tiempo tardo recolectar","Unidad x tiempo Rec",
			"Verificador","Fecha verifica","Inicio verifica","Fin verifica","Tiempo tardo verificar",
			"Unidad x tiempo");

		   $qrystrV="SELECT CONCAT(Pedido,';',Revendedor,';',Fecha,';',Cant_Unidades,';',Recolector,';',Fecha_inicio_recolecta,';',
		   Fecha_fin_recolecta,';',Tiempo_tardo_recolectar,';',Unidad_x_tiempo_rec,';',
		   Verificador,';',Fecha_verifica,';',Fecha_inicio_verifica,';',Fecha_fin_verifica,';',Tiempo_tardo_verificar,';',
		   Unidad_x_tiempo) as c FROM temp_picking";

	}/*elseif($_POST[actividad]=='2'){ // empaquetado

			$qrystrV0 = "SELECT id_pedidos,us_control_lider,fecha_fin_control,fecha_control_lider,bultos,TIMEDIFF(fecha_control_lider, fecha_fin_control) as TiempoTardoEmpaquetado
							FROM pedido
							WHERE 1
							$filtro2
							$ussu2
							and us_control_lider<>''
							ORDER BY fecha_control_lider, us_control_lider";


			$sql = "CREATE TEMPORARY TABLE `temp_picking` (
			  `id_temp_picking` int(11) NOT NULL AUTO_INCREMENT,
			  `id_pedidos` varchar(20) NOT NULL DEFAULT '',
			  `us_control_lider`  varchar(20) NOT NULL DEFAULT '',
			  `fecha_fin_control` datetime NOT NULL,
			  `fecha_control_lider` datetime NOT NULL,
			  `TiempoTardoEmpaquetado` time NOT NULL,
			  `CantidadPedidosVendedores` int(11) NOT NULL,
			  `cantidadUnidades` int(11) NOT NULL,
			  PRIMARY KEY (`id_temp_picking`))";
			$qry = mysql_db_query($c_database, $sql, $link);


			while ($rowV0=mysql_fetch_array($qrystrV0)){
					$qrystrC = "SELECT count( pd.id_vendedor) as cantidadPedidos,sum(pd.un_total) as unidades
									FROM pedidos_defi as pd
									WHERE 1 and id_pedidos='$rowV0[id_pedidos]'";
				//	echo $qrystrC;
					$qryC = mysql_db_query($c_database,$qrystrC,$link);
					$rowC=mysql_fetch_array($qryC);
					$cantidadPedidos=$rowC[cantidadPedidos];
					$cantidadUnidades=$rowC[unidades];

				$qrystr="INSERT INTO temp_picking (id_pedidos,us_control_lider,fecha_fin_control,fecha_control_lider,TiempoTardoEmpaquetado,CantidadPedidosVendedores,cantidadUnidades)
				  VALUES ('$rowV0[id_pedidos]',$rowV0[us_control_lider],'$rowV0[fecha_fin_control]','$rowV0[fecha_control_lider]',$rowV0[TiempoTardoEmpaquetado],'$cantidadPedidos',
				  '$cantidadUnidades')";
				  $i++;
				 $qry = mysql_db_query($c_database, $qrystr, $link);
			}



		/*}elseif ($_POST[actividad]=='3'){ //Despacho
			$muestra= "us_control_distrib,';',fecha_control_lider,';',fecha_control_distrib,';',TIMEDIFF(fecha_control_distrib, fecha_control_lider)";
			   $arr_tit = array("Funcionario","fecha_control_lider","fecha_control_distrib");*/

		// $muestra= "id_pedidos,';',us_control_lider,';',fecha_fin_control,';',fecha_control_lider,';',$cantidadPedidos,';',$cantidadUnidades,';',bultos,';',TIMEDIFF(fecha_control_lider, fecha_fin_control)";
	/*
		$arr_tit = array("Pedido","Colaborador","FechaInicioEmpaquetado","FechaFinEmpaquetado","CantidadPedidosVendedores","CantidadUnidades","CantidadPaquetes","TiempoTardoEmpaquetado");

		$qrystrV="SELECT CONCAT(id_pedidos,';',us_control_lider,';',fecha_fin_control,';',fecha_control_lider,';',CantidadPedidosVendedores,';',cantidadUnidades,';',TiempoTardoEmpaquetado) as c FROM temp_picking";

	}
	*/

	   $qrystrV="SELECT Pedido,Revendedor,Fecha,Cant_Unidades,Recolector,Fecha_inicio_recolecta,
		   Fecha_fin_recolecta,Tiempo_tardo_recolectar,Unidad_x_tiempo_rec,
		   Verificador,Fecha_verifica,Fecha_inicio_verifica,Fecha_fin_verifica,Tiempo_tardo_verificar,
		   Unidad_x_tiempo FROM temp_picking ORDER BY CASE Fecha_inicio_recolecta WHEN '00:00:00' THEN Fecha_inicio_verifica ELSE Fecha_inicio_recolecta END ASC"; 
		   
		$qryV = mysql_db_query($c_database,$qrystrV,$link);
		$cantidad=mysql_num_rows($qryV);

		  if($cantidad>0){


					//***************************************************************
			$styleArray = array(
				'font' => array(
					'bold' => true,
				),
				
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($estiloTituloReporte);


				// titulos
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1',"Pedido")
				->setCellValue('B1',"Revendedor")
				->setCellValue('C1',"Fecha")
				->setCellValue('D1',"Cant Unidades")
				->setCellValue('E1',"Recolector")
				->setCellValue('F1',"Inicio recolecta")
				->setCellValue('G1',"Fin recolecta")
				->setCellValue('H1',"Tiempo tardo recolectar")
				->setCellValue('I1',"Unidad x tiempo rec")
				->setCellValue('J1',"Verificador")
				->setCellValue('K1',"Fecha verifica")
				->setCellValue('L1',"Inicio verifica")
				->setCellValue('M1',"Fin verifica")
				->setCellValue('N1',"Tiempo tardo verificar")
				->setCellValue('O1',"Unidad x tiempo");
			$i=2;
			  
			while($registros=mysql_fetch_object($qryV)){
				if($registros->Fecha_inicio_recolecta=='00:00:00')
					$registros->Fecha_inicio_recolecta="";
				if($registros->Fecha_fin_recolecta=='00:00:00')
					$registros->Fecha_fin_recolecta="";
				if($registros->Fecha_recolecta=='00:00:00')
					$registros->Fecha_recolecta="";
				if($registros->Tiempo_tardo_recolectar=='00:00:00')
					$registros->Tiempo_tardo_recolectar="";
				if($registros->Unidad_x_tiempo_rec=='00:00:00')
					$registros->Unidad_x_tiempo_rec="";
				if($registros->Fecha_verifica=='0000-00-00')
					$registros->Fecha_verifica="";
				if($registros->Fecha_inicio_verifica=='00:00:00')
					$registros->Fecha_inicio_verifica="";
				if($registros->Fecha_fin_verifica=='00:00:00')
					$registros->Fecha_fin_verifica="";
				if($registros->Tiempo_tardo_verificar=='00:00:00')
					$registros->Tiempo_tardo_verificar="";
				if($registros->Unidad_x_tiempo=='00:00:00')
					$registros->Unidad_x_tiempo="";
				
				//var_dump($registros->Tiempo_tardo_recolectar);
					  
				$objPHPExcel->setActiveSheetIndex(0)
			   
				->setCellValue('A'.$i,$registros->Pedido)
				->setCellValue('B'.$i,$registros->Revendedor)
				->setCellValue('C'.$i,$registros->Fecha)
				->setCellValue('D'.$i,$registros->Cant_Unidades)
				->setCellValue('E'.$i,$registros->Recolector)
				->setCellValue('F'.$i,$registros->Fecha_inicio_recolecta)
				->setCellValue('G'.$i,$registros->Fecha_fin_recolecta)
				->setCellValue('H'.$i,$registros->Tiempo_tardo_recolectar)
				/*->setCellValue('H'.$i, PHPExcel_Shared_Date::PHPToExcel($registros->Tiempo_tardo_recolectar));
				$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
				$objPHPExcel->setActiveSheetIndex(0)
	*/
				->setCellValue('I'.$i,$registros->Unidad_x_tiempo_rec)
				->setCellValue('J'.$i,$registros->Verificador)
				->setCellValue('K'.$i,$registros->Fecha_verifica)
				->setCellValue('L'.$i,$registros->Fecha_inicio_verifica)
				->setCellValue('M'.$i,$registros->Fecha_fin_verifica)
				->setCellValue('N'.$i,$registros->Tiempo_tardo_verificar)
				->setCellValue('O'.$i,$registros->Unidad_x_tiempo);
				$i++;
			}// End while
			
		
		
								
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="_'.date("Ymd_Hms").'.xls"');
				header('Cache-Control: max-age=0');


				$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5'); // Excel 97, 2000 xls format

				$objWriter->save('php://output');
				exit;
					/*  $lista = new Listas();
					  $lista->titulos=$arr_tit;
					  $data=$lista->LoadData($qrystrV);
					  $lista->BasicTable($data);

					  $lista->OutPut("EXCEL");*/
		  }else{
			  echo "<div class='error'> No hay registros para mostrar</div>";
		  }
}
?>

