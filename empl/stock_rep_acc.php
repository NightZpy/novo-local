<script>
$(document).ready(function(){
	$("#stock_min").editInPlace({
		url: "empl/stock_rep_grabar_acc_min.php",
		params: "usuario=<?=$usuario?>&sesion=<?=$sesion?>&param=<?=$param?>"
	});
		$("#stk1_min").editInPlace({
		url: "empl/stock_rep_grabar_acc_min.php",
		params: "usuario=<?=$usuario?>&sesion=<?=$sesion?>&param=<?=$param?>"
	});
});
</script>
<?php
/**
 * $R1: Depósito "Desde"
 * $R2: Depósito "Hasta"
 *
 * Referencia de Depósitos:
 * ------------------------
 * stk1: Góndola Semijoyas
 * stk2: Auxiliar
 * stk3: Bóveda Proceso
 * stk4: Bóveda terminada
 * stk5: Bóveda Grabados
 * stk6: Gondola Grabados
 * dep_acc1: Góndola. Achaval
 * dep_acc2: Bóveda Acc.
 * dep_acc3: Góndola Accesorios //cambiar nbre por dep_acc3
 * sv01 salon de venta cordoba
 * sv02 salon de venta resistencia
 * sv03 salon de venta bsas
 * sv04 salon de ventas Brasil
 * sv_muestra salon de venta muestra
 */
/* Cuando se use el deposito de Grabados adelantados
 * <tr > <td width='7%' align='center'><input type='radio' name='R1' value='stk6'></td>";
            echo "<td width='7%' align='center'><input type='radio' name='R2' value='stk6'";
							if(in_array($usuario,$usuariosGrabados)){ echo "checked='checked'";}
            echo "></td><td width='47%'><font face='Arial' size='3'>Grabados</font></td>
            <td width='7%' align='right'>$row[stk6]</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
   </tr>
 */

// Array con las letras de joyas y accesorios, no se puede usar atributos
// porque no estan todos los productos en la lista vigente


$letrasJoyas=array('S','V','O','M');
$letrasAcc=array('A','C','F','P');


// Array usuarios de Grabados
$usuariosGrabados=array('LUCIANAV','MONICA','ROSANA','MARTINC');
$usuariosSalon=array('LUCIANAV','ROXANAC','JPAUTASSO','MARTINC','JRIOS','MATIASS');

    


/* Para usar cuando se pueda con los atributos
$qrystr="SELECT *,CONCAT(p.nombreproducto,'-',p.id_vd,'-(',p.ubi,')') as paq,lpp.id_atrib
           FROM productos AS p
           RIGHT JOIN listas_precios_productos AS lpp ON lpp.id_vd=p.id_vd
           INNER JOIN listas_precios AS lp ON lp.id_lista=lpp.id_lista
          WHERE p.id_producto='$param' OR (p.id_producto='$param' AND lpp.predet=1)";
*/

$qrystr="SELECT *,CONCAT(p.nombreproducto,'-',p.id_vd,'-(',p.ubi,')') as paq
           FROM productos AS p
           WHERE p.id_producto='$param'";

//echo $qrystr; die();

$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$letraProducto=substr($row['id_vd'],0,1);
//echo $letraProducto;

// dep_acc3 *** Góndola grabado ***
//$stock=$row[stk1]+$row[stk2]+$row[stk3]+$row[stk4]+$row[stk5]+$row[stk6]+$row[dep_acc1]+$row[dep_acc2]+$row[dep_acc3];
$stock=$row[stk1]+$row[stk3]+$row[stk4]+$row[stk5]+$row[stk6]+$row[dep_acc1]+$row[dep_acc2]+$row[sv01]+$row[sv02]+$row[sv03]+$row[sv_muestra]+$row[sv04];

if(in_array($letraProducto,$letrasAcc)){// Accesorios
	if  ($row[dep_acc2]<($row[stk1_min]/2)) {$msg1= "Rep Urg.";$colormsg1="#FF0000";}
	if  (($row[dep_acc2]<$row[stk1_min]) AND ($row[dep_acc2]>=($row[stk1_min]/2)))  {$msg1= "Reponer";$colormsg1="#FFFF00";}
	if  (($row[dep_acc2]<($row[stk1_min]*1.3)) AND ($row[dep_acc2]>=$row[stk1_min]))  {$msg1=  "Preparar";$colormsg1="#CCFF66";}
	if  (($row[dep_acc2]>=($row[stk1_min]*1.3)) AND ($row[dep_acc2]<($row[stk1_min]*3)))  {$msg1=  "Normal";$colormsg1="#00FF00";}
	if  ($row[dep_acc2]>=($row[stk1_min]*3))  {$msg1="Sobre Stk"; $colormsg1="#D838FA";}
}
elseif(in_array($letraProducto,$letrasJoyas)){// Joyas
	if  ($row[stk1]<($row[stk1_min]/2)) {$msg1= "Rep Urg.";$colormsg1="#FF0000";}
	if  (($row[stk1]<$row[stk1_min]) AND ($row[stk1]>=($row[stk1_min]/2)))  {$msg1= "Reponer";$colormsg1="#FFFF00";}
	if  (($row[stk1]<($row[stk1_min]*1.3)) AND ($row[stk1]>=$row[stk1_min]))  {$msg1=  "Preparar";$colormsg1="#CCFF66";}
	if  (($row[stk1]>=($row[stk1_min]*1.3)) AND ($row[stk1]<($row[stk1_min]*3)))  {$msg1=  "Normal";$colormsg1="#00FF00";}
	if  ($row[stk1]>=($row[stk1_min]*3))  {$msg1="Sobre Stk"; $colormsg1="#D838FA";}
}
if  ($stock<($row[stock_min]/2)) {$msg2= "Pedir Urg."; $colormsg2="#FF0000";}
if  (($stock<$row[stock_min]) AND ($stock>=($row[stock_min]/2)))  {$msg2= "Pedir";$colormsg2="#FFFF00";}
if  (($stock<($row[stock_min]*1.3)) AND ($stock>=$row[stock_min]))  {$msg2=  "Prep. Pedido";$colormsg2="#CCFF66";}
if  (($stock>=($row[stock_min]*1.3)) AND ($stock<($row[stock_min]*3)))  {$msg2=  "Normal";$colormsg2="#00FF00";}
if  ($stock>=($row[stock_min]*3))  {$msg2=  "Sobre Stk";$colormsg2="#D838FA";}

$msg1="<table border='0' width='100%' cellspacing='2'>
        <tr>
          <td width='20%' bgcolor=$colormsg1>&nbsp;</td>
          <td width='80%' align='center'><font color=#FF0000 face='Arial' size='2'>$msg1</font></td>
        </tr>
      </table>";
$msg2="<table border='0' width='100%' cellspacing='2'>
        <tr>
          <td width='20%' bgcolor=$colormsg2>&nbsp;</td>
          <td width='80%' align='center'><font color=#FF0000 face='Arial' size='2'>$msg2</font></td>
        </tr>
      </table>";
echo "
<table border='0' width='100%'>
  <tr>
    <td width='100%' align=left>
    <font face=arial size=3 color=#800000><b>Movimiento de Art:</b></font></td>
  </tr>
</table> ";
// Stk2 suma aux_acc(auxiliar acc) + stk2 (auxiliar joyas)
echo" <form method='POST' action='administracion.php'>
<INPUT TYPE=hidden name=usuario  value=$usuario>
<INPUT TYPE=hidden name=sesion  value=$sesion>
<INPUT TYPE=hidden name=s1  value=$row[stk1]>
<INPUT TYPE=hidden name=s6  value=$row[stk2]>
<INPUT TYPE=hidden name=s7  value=$row[stk3]>
<INPUT TYPE=hidden name=s5  value=$row[stk4]>
<INPUT TYPE=hidden name=s8  value=$row[stk5]>
<INPUT TYPE=hidden name=s9  value=$row[stk6]>
<INPUT TYPE=hidden name=s10  value=$row[sv01]>
<INPUT TYPE=hidden name=s11  value=$row[sv02]>
<INPUT TYPE=hidden name=s12  value=$row[sv03]>
<INPUT TYPE=hidden name=s13  value=$row[sv_muestra]>
<INPUT TYPE=hidden name=s14  value=$row[sv04]>";

// dep_acc3 *** Góndola grabado ***
//<INPUT TYPE=hidden name=s2  value=$row[dep_acc3]>
echo "<INPUT TYPE=hidden name=s3  value=$row[dep_acc1]>
<INPUT TYPE=hidden name=s4  value=$row[dep_acc2]>
<INPUT TYPE=hidden name=sta value=$row[stk_activo]>
<INPUT TYPE=hidden name=id_pro value=$row[id_producto]>
<INPUT TYPE=hidden name=pagina  value='empl/stock_rep_grabar_acc.php'>
<p>  <table border='0' width='100%'>
    <tr>
      <td width='100%' colspan='2'>
        <table border='0' width='100%'>
          <tr>
            <td width='6%'>Art:</td>
            <td width='94%'>$row[paq]</td>
          </tr>
          </table>
         <table border='0' width='100%'>
          <tr>
            <td width='20%'>Cantidad:</td>
            <td width='30%'><input autofocus type='text' name='cantidad' size='4' value='0'></td>
            <td width='50%'>
            <table border='0' width='100%'>
               <tr>
                  <td width='6%'><input type='radio' value='1' ";if ($row[stk_activo]==1){echo "checked=checked";} echo " name='stad'></td>
                  <td width='44%'>Control Activo</td>
                  <td width='8%'><input type='radio' ";if ($row[stk_activo]<>1){echo "checked=checked";} echo " name='stad' value='0'></td>
                  <td width='42%'>Control Deshabilitado</td>
               </tr>
               </table>
               </td>
         </tr><tr><td width='100%' colspan='7' height='1' bgcolor=C85858></td></tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width='48%' >
             <table border='0' width='100%' cellspacing='0' cellpadding='3'>
           <tr>
            <td width='7%' align='center'><font face='Arial' size='2'>Desde</font></td>
            <td width='7%' align='center'><font face='Arial' size='2'>Hasta</font></td>
            <td width='47%'></td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
            <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='stk3'></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='stk3'></td>
            <td width='47%'><font face='Arial' size='3'>B&oacute;veda Proceso</font></td>
            <td width='7%' align='right'>$row[stk3]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
          <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
           <tr >
            <td width='7%' align='center'><input type='radio' name='R1' value='stk4'";
            if(in_array($letraProducto,$letrasJoyas)){ echo "checked='checked'";}
            echo "></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='stk4'></td>
            <td width='47%'><font face='Arial' size='3'>B&oacute;veda Terminada</font></td>
            <td width='7%' align='right'>$row[stk4]</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='stk1'></td>";
          echo "<td width='7%' align='center'><input type='radio' name='R2' value='stk1'";
						if(in_array($letraProducto,$letrasJoyas) || in_array($letraProducto,$letrasAcc)){
							echo "checked='checked'";
						}
          echo "></td><td width='47%'><font face='Arial' size='3'>G&oacute;ndola Semijoyas</font></td>
            <td width='7%' align='right'>$row[stk1]</td>
			<td width='7%' align='right'><font face='Arial' size='1'>Stock Min.</font></td>
            <td width='7%' align='right'><p id='stk1_min' style='background-color: transparent;' >$row[stk1_min]</p></td>
            <td width='18%'>$msg1</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
            <tr >
            <td width='7%' align='center'><input type='radio' name='R1' value='stk5'></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='stk5'></td>
            <td width='47%'><font face='Arial' size='3'>B&oacute;veda Grabado</font></td>
            <td width='7%' align='right'>$row[stk5]</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
           <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='stk6'></td>";
          echo "<td width='7%' align='center'><input type='radio' name='R2' value='stk6'>
          <td width='47%'><font face='Arial' size='3'>G&oacute;ndola Grabado</font></td>
            <td width='7%' align='right'>$row[stk6]</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>
          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='dep_acc2'";
            if(in_array($letraProducto,$letrasAcc)){
							echo "checked='checked'";
						}
            echo "></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='dep_acc2'></td>
            <td width='47%'><font face='Arial' size='3'>B&oacute;veda Accesorio</font></td>
            <td width='7%' align='right'>$row[dep_acc2]</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
          /*          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='aux_acc'></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='aux_acc'></td>
            <td width='47%'><font face='Arial' size='3'>G&oacute;ndola Accesorio</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[dep_acc3]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>*/
          echo "
          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='stk2'></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='stk2'></td>
            <td width='47%'><font face='Arial' size='3'>Auxiliar</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[stk2]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>

           <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='dep_acc1'></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='dep_acc1'></td>
            <td width='47%'><font face='Arial' size='3'>Dep. ACHAVAL</font></td>
            <td width='7%' align='right'>$row[dep_acc1]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
            </tr>
             <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
if (in_array($usuario,$usuariosSalon)){
   $control_habilitado=''; 
    
}else{
    $control_habilitado='disabled';
}

////////////////////// depositos salor de vta
          ///////////// cordoba
          echo "<tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='sv01' $control_habilitado></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='sv01' $control_habilitado></td>
            <td width='47%'><font face='Arial' size='3'>Sv C&oacute;rdoba</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[sv01]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
          /////////// Resistencia
          echo "          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='sv02' $control_habilitado></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='sv02' $control_habilitado></td>
            <td width='47%'><font face='Arial' size='3'>Sv Resistencia</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[sv02]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
          /////////////// Buenos aires
          echo "          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='sv03' $control_habilitado></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='sv03' $control_habilitado></td>
            <td width='47%'><font face='Arial' size='3'>Sv BsAs</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[sv03]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
                    /////////////// Brasil
          echo "          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='sv04' $control_habilitado></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='sv04' $control_habilitado></td>
            <td width='47%'><font face='Arial' size='3'>Sv Brasil</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[sv04]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
          
          ////////////////////////////Fin////////////////
          
          
          /////////////// muestra
          echo "          <tr>
            <td width='7%' align='center'><input type='radio' name='R1' value='sv_muestra' $control_habilitado></td>
            <td width='7%' align='center'><input type='radio' name='R2' value='sv_muestra' $control_habilitado></td>
            <td width='47%'><font face='Arial' size='3'>Sv Muestra</font><font face='Arial' size='1'></font></td>
            <td width='7%' align='right'>$row[sv_muestra]</td>
            <td width='7%' align='right'>&nbsp;</td>
             <td width='7%' align='right'>&nbsp;</td>
            <td width='18%' align='right'>&nbsp;</td>
          </tr>
           <tr bgcolor=#FFF8D8>
            <td colspan='7'></td>
          </tr>";
          
          ////////////////////////////Fin////////////////
          
          
           echo"<tr>
            <td width='7%' align='center'></td>
            <td width='7%' align='center'></td>
            <td width='47%' align='right'><font face='Arial' size='3' style='margin-right: 20px;'>Stock Total</font></td>
            <td width='7%' align='right'>$stock</td>
            <td width='7%' align='right'><font face='Arial' size='1'>Stock Min.</font></td>
            <td width='7%' align='right'>
            <p id='stock_min' style='background-color: transparent;'>$row[stock_min]</p></td>
            <td width='18%' align='right'>$msg2</td>
          </tr> <tr><td width='100%' colspan='7' height='1' bgcolor=C85858></td></tr>
       </table>
      </td>
    </tr>
  </table>
  <p align='center'><input type='submit' value='Enviar' name='B1'><input type='reset' value='Restablecer' name='B2'></p>
</form>
";
$relat="";
include ("menu_pedidos_stock_n2_acc.php");
?>
