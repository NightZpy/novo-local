<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
 require_once('../clases/forms/class.forms_ale7.php');

 //####### OBLIGATORIOS
 $tablasuper = "productos_medida";          //La tabla del Supertipo.
 $preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/producto_medida_abm.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/productos_medida_lista.php";//La p�gina a mostrar si graba exitosamente.

 $titulo = "ABM de Medidas de Productos";   //El t�tulo a mostrar en el html.
 $clave_primaria = "id_medida";              //El nombre del campo que es Clave Primaria.
 
 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_medida","descrip");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_medida","Descripcion");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_medida");
 //Las validaciones de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (NumeroValido('Estado',Form.estado,0,1)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Desde',Form.desde)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Hasta',Form.hasta)) return false;");
 //Los COMBOS de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("punto_aplicacion","crit_puntos_aplic","nombre_punto_aplic","id_punto_aplic"));
 //$qrystrth = "SELECT DISTINCT htl_hab_t.idth AS id, htl_hab_t.tipohab AS tipo FROM `a_htl_calendario`,htl_hab_t WHERE (htl_hab_t.idth = `a_htl_calendario`.idth OR htl_hab_t.idth=999) and idh ='$par_idh' ORDER BY(htl_hab_t.idth<>999)";
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("idth","tipo","id",$qrystrth));
 //Las FECHAS de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("punto_aplicacion","crit_puntos_aplic","nombre_punto_aplic","id_punto_aplic"));
//$qrystramedida = "SELECT  descri_promo,id_promo FROM crit_promos ORDER BY id_promo ASC";
// array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida1Texto("relacion_promo","descri_promo","id_promo",$qrystramedida,"NINGUNA",0));

 //$conf_tablas["$tablasuper"]->fechas = array("desde","hasta");
//***********************************************************
//******** FIN ESTO VA A IR EN OTRO ARCHIVO .inc ************



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           DB Layer Example Page - Inserting             #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


 /*Variables de entrada (de control)
 par_id                -- El Producto a mostrar en el ABM. Si es "" es una ALTA.
 par_idh               -- El Hotel elegido si es una ALTA.
 */

 if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
  // $nuevo = nuevo_id($tablasuper,$clave_primaria);
  // ${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.
   $form->addHiddenForm("estado",$estado);
   $form->addHiddenForm("cmb_punt_aplic",$cmb_punt_aplic);
   $form->addHiddenForm("promo",$promo);
   $form->addHiddenForm("combo",$combo);
   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
    
   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //if ($accion_abm == 1)         //1 = ALTA.
   //  $form->addHiddenForm("idh","$par_idh");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //$form->addHiddenForm("idtc","999");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
                $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
          {
           if ($accion_abm == 1)
            {
             echo "<b>Se ha generado la nueva medida </b>";
             //Obtengo el Orden m�s grande
             //$nuevo_orden = nuevo_id('criterios','orden');
             //Hago un UPDATE del orden.
             //qrystr11="UPDATE criterios SET orden = $nuevo_orden WHERE id_criterio='$nuevo'";
             //$qry11 = mysql_db_query($c_database,$qrystr11,$link);
             //$err=mysql_error();
            }
           else
             echo "<b>Se ha modificado la medida</b>";
           include ($par_pag_sig);
          }
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
   	 echo $form->build("200","10","200","3","40");
   	 echo "<TABLE width=100% BORDER=0><tr><td align=center>";
   	 //$qrystramedida = "SELECT  DISTINCT texto_agrup FROM criterios WHERE texto_agrup <> '' ORDER BY texto_agrup ASC";
   	 //poner_combo_a_medida_1texto_x("combo","texto_agrup","texto_agrup",$qrystramedida,"","","ONCHANGE=\"javascript:document.ffcriterios.texto_agrup.value=this.value;\"");
   	 echo "</td></tr></TABLE>";
   	 include "menu_pedidos_data_n1.php";
	  }


//################################################################################################

?>

