<form id="form_eti" method="post" name="form_eti" accept-charset="utf-8">
	<input id="nivel" type="hidden" name="nivel" value="2" />
  <div>
    <ul>
      <li>
          <h2><font color="red">*</font> Datos Obligatorios</h2>
      </li>
      <li>
        <label class="pagos" for="revendedor">Distribuidor ( Ingrese Parte de C&oacute;digo, Apellido o D.N.I ): </label>
        <div>
          <input id="revendedor" type="search" name="revendedor" required/>&nbsp;<font color="red">*</font>
        </div>
				<label class="pagos" for="transporte">Transporte: </label>
				<div>
				<?php
					$sqlTransporte="SELECT id_empresa,nombre FROM transportes ORDER BY nombre";
					poner_combo_1texto("transporte","transportes","nombre","id_empresa","Transporte Propio","0")
				?>
				</div>
				<label class="pagos" for="destino">Destino: </label>
				<div>
						Domicilio:<input type="radio" name="destino" value="D" checked/>&nbsp;
						Terminal:<input type="radio" name="destino" value="T" />
				</div>
        <label class="pagos" for="etiquetas">Cant. Etiquetas:</label>
        <div>
          <input id="etiquetas" type="number" name="etiquetas" value="0" required/>&nbsp;<font color="red">*</font>
          <button id="buscar" type="button" class="buttons">Emitir Etiq.</button>
        </div>
      </li>
    </ul>
  </div>
</form>
<div class="clear" >
<span><a id="ayuda" class="btn" href="administracion.php?pagina=empl/func_emp_lista.php&usuario=<?php echo $usuario;?>&sesion=<?php echo $sesion;?>" title="Click aqu&iacute; para volver al men&uacute; principal">Men&uacute; Principal</a></span>
</div>
<script type="application/javascript">
jQuery(function(){

// Param Required ---------------------
var codus	=jQuery("#revendedor").val();
var ceti	=jQuery("#etiquetas").val();
// -------------------------------------
jQuery.blockUI.defaults.theme= true;


jQuery("#buscar").click(function(){

	nivel	=jQuery("#nivel").val();
  codus	=jQuery("#revendedor").val();
  ceti	=jQuery("#etiquetas").val();
  transporte=jQuery("select[name='transporte']").val();
  destino= jQuery("input[name='destino']:checked").val();

 //alert(transporte+"-"+destino);

	if(codus==''){
		jQuery.blockUI({message: "<p>Por ingrese un Distribuidor para poder emitir las etiquetas!<p>", timeout: 2600});
		jQuery("#revendedor").val("");
	}else if(ceti<1){
		jQuery.blockUI({message: "<p>Por ingrese la cantidad de etiquetas para poder emitirlas!<p>", timeout: 2600});

		jQuery("#etiquetas").val("0");
	}else{
		window.open('empl/pdf/pdf_etiqueta_envio_manual.php?codus='+codus+'&ce='+ceti+'&t='+transporte+'&d='+destino,'_blank');
	}

});


}); // End load document
</script>
