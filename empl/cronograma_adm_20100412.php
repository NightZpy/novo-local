<?php
$perm["adm"]["ver"] = true;
$perm["adm"]["modif"] = true;
$perm["adm"]["alta"]= true;
$perm["adm"]["baja"]= true;
$perm["ctacte"]["ver"] = true;
$perm["ctacte"]["modif"] = false;
$perm["acc"]["ver"] = true;
$perm["acc"]["modif"] = $usuario=='CARINAB' ||
						$usuario=='ROBERTO' ||
						$usuario=='NICOLASMED' ||
						$usuario=='RICARDOJ' ||
						$usuario=='GUSTAVO' ||
						$usuario=='ALEJANDRO' ||
						$usuario=='MAURO' ||
						$usuario=='MARIALAURA' ||
						$usuario=='FERNANDA' ||
						$usuario=='IGNACIO' ||
						$usuario=='iddelsur';
$perm["gra"]["ver"] = true;
$perm["gra"]["modif"] = $usuario=='CARINAB' ||
						$usuario=='ROBERTO' ||
						$usuario=='NICOLASMED' ||
						$usuario=='RICARDOJ' ||
						$usuario=='MONICA' ||
						$usuario=='ROSANA' ||
						$usuario=="iddelsur";
$pagina_llamadora="empl/cronograma_adm.php";

include "empl/cronograma.php";
?>
