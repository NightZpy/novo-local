<?php
include "obtener_observados.inc";

$coef = coef_exponencial($xobs , $yobs);

$f = array();
$f[0] = "if(((\$xiobs + 0) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[1] = "if(((\$xiobs + 1) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[2] = "if(((\$xiobs + 2) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[3] = "if(((\$xiobs + 3) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[4] = "if(((\$xiobs + 4) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[5] = "if(((\$xiobs + 5) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[6] = "if(((\$xiobs + 6) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[7] = "if(((\$xiobs + 7) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[8] = "if(((\$xiobs + 8) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[9] = "if(((\$xiobs + 9) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[10] = "if(((\$xiobs + 10) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[11] = "if(((\$xiobs + 11) % 12)==0) \$fx = \$xiobs; else \$fx = 0;";
$f[12] = "\$fx = \$xiobs;";
//$f[13] = "\$fx = 1;";

$f[0] = "if(((\$xiobs + 0) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[1] = "if(((\$xiobs + 1) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[2] = "if(((\$xiobs + 2) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[3] = "if(((\$xiobs + 3) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[4] = "if(((\$xiobs + 4) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[5] = "if(((\$xiobs + 5) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[6] = "if(((\$xiobs + 6) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[7] = "if(((\$xiobs + 7) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[8] = "if(((\$xiobs + 8) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[9] = "if(((\$xiobs + 9) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[10] = "if(((\$xiobs + 10) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[11] = "if(((\$xiobs + 11) % 12)==0) \$fx = \$coef[0] * pow(\$coef[1],\$xiobs); else \$fx = 0;";
$f[12] = "\$fx = \$xiobs;";
$f[13] = "\$fx = 1;";


/*$f[0] = "if(((\$xiobs + 0) % 12)==0) \$fx = 1; else \$fx = 0;";
$f[1] = "if(((\$xiobs + 1) % 12)==0) \$fx = 1; else \$fx = 0;";
$f[2] = "if(((\$xiobs + 2) % 12)==0) \$fx = 1; else \$fx = 0;";
$f[3] = "\$fx = \$xiobs;";
*/

$mat = array();
//Genero la Matriz cuadrada.     fij
for($i=0;$i< sizeof($f);$i++)
  for($j=0;$j< sizeof($f);$j++)
   {
    for($k=0;$k<sizeof($xobs);$k++)
     {
      $xiobs = $xobs[$k];
      eval($f[$i]);
      $fi = $fx;
      eval($f[$j]);
      $fj = $fx;
      $mat[$i][$j] += $fi * $fj;
     }
   }
//Calculo la parte ampliada de la Matriz.  bi
for($i=0;$i< sizeof($f);$i++)
 {
  for($k=0;$k<sizeof($xobs);$k++)
   {
    $xiobs = $xobs[$k];
    eval($f[$i]);
    $fi = $fx;
    $mat[$i][sizeof($f)] += $fi * $yobs[$k];
   }
 }

mostrar_matriz($mat);

triangular_matriz(&$mat);

mostrar_matriz($mat);

$coef = sust_inv($mat);

for($i=0;$i<count($coef);$i++)
  echo "Coef$i: $coef[$i], ";

$mat_pru = array();
$mat_pru[0][0] = "xobs[k]";
$mat_pru[0][1] = "yobs[xk]";
$mat_pru[0][2] = "y'[xk]";
$mat_pru[0][3] = "error";

$errortot=0;
for($k=0;$k<sizeof($yobs);$k++)
 {
  $mat_pru[$k+1][0] = $k;
  $mat_pru[$k+1][1] = $yobs[$k];
  $mat_pru[$k+1][2] = intval(gran_funcion($f,$coef,$k));
  $error = pow(pow($mat_pru[$k+1][2]-$mat_pru[$k+1][1],2),0.5);
  $errortot += $error;
  $mat_pru[$k+1][3] = $error;
 }
mostrar_matriz($mat_pru);

echo "errortot = $errortot";

//##############################################################################################################
//*********************************************************************
function triangular_matriz($mat)
{
 for($k=0;$k<sizeof($mat[0]);$k++)                  //Recorro por columna.
  {
   for($i=$k+1;$i< sizeof($mat);$i++)               //Recorro las filas.
    {
     $m[$i] = -($mat[$i][$k]/$mat[$k][$k]);        //Modificador de fila.
     for($j=0;$j<sizeof($mat[0]);$j++)            //Recorro por columna.
      {
       $mat[$i][$j] = $mat[$i][$j] + $m[$i] * $mat[$k][$j];  //El nuevo valor de cada columna en la fila actual. Esto pone a 0 al elemento de la columna actual en k.
      }
    }
  }
}

//*********************************************************************
function mostrar_matriz($mat,$mostrar_dimesion=0)
{
 if($mostrar_dimesion<>0)
  {
   echo "f: ".count($mat);
   echo "c: ".count($mat[0]);
  }
 echo "<table border=1>\n";
 for($i=0;$i< count($mat);$i++)
  {
   echo "<tr>\n";
   for($j=0;$j< count($mat[0]);$j++)
     echo "<td>".$mat[$i][$j]."</td>\n";
   echo "</tr>\n";
  }
 echo "</table>\n";
}

//*********************************************************************
function sust_inv($mat)
{
 $coef = array();

 for($k=sizeof($mat)-1;$k>=0;$k--)                  //Recorro por fila.
  {
   $sumatoria = 0;
   for($j=$k+1;$j< sizeof($mat);$j++)               //Recorro las columnas no 0.
    {
     $sumatoria += $coef[$j] * $mat[$k][$j];
    }
//   echo "sumatoria=$sumatoria";
   $coef[$k] = ($mat[$k][sizeof($mat)] - $sumatoria) / $mat[$k][$k];
  }
// $coef[12] = 15;
 return $coef;
 
}

//*********************************************************************
function coef_exponencial($xobs;$yobs)
{
//Acumuladores.
 $Sx = 0;
 $Sy = 0;
 $Slogy = 0;
 $Sx2 = 0;
 $Sxlogy = 0;
 
 //Comienzo a acumular.
 for($i=0;$i< sizeof($xobs);$i++)
  {
   $Sx += $xobs[$i];
   $Sy += $yobs[$i];
   $Slogy += log($yobs[$i],10);
   $Sx2 += $xobs[$i] * $xobs[$i];
   $Sxlogy += $xobs[$i] * log($yobs[$i],10);
  }

 $loga = ($Slogy * $Sx2 - $Sx * $Sxlogy) / (count($xobs) * $Sx2 - $Sx * $Sx);
 $logb = (count($xobs) * $Sxlogy - $Sx * $Slogy)/(count($xobs) * $Sx2 - $Sx * $Sx);
 $coef[0] = pow(10,$loga);
 $coef[1] = pow(10,$logb);
 return $coef;
}
//*********************************************************************
function gran_funcion($f,$coef,$x)
{
 for($i=0;$i<count($f);$i++)
  {
   $xiobs = $x;
   eval($f[$i]);
   $fi = $fx;
   $gf += $coef[$i] * $fi;
  }
 return $gf;

}
?>

