<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

switch($categoria){
    case 'vendedor':
        $cat = 'id_vendedor';
        $cat_sup = 'id_lider';
        $title = 'Revendedores';
        $title_sup = 'Lideres';
        break;
    case 'lider':
        $cat = 'id_lider';
        $cat_sup = 'id_distrib';
        $title = 'Lideres';
        $title_sup = 'Distribuidores';
        break;
    case 'distrib':
        $cat = 'id_distrib';
        $cat_sup = 'id_regional';
        $title = 'Distribuidores';
        $title_sup = 'Regionales';
        break;
    default:
        $cat = 'id_vendedor';
        $cat_sup = 'id_lider';
        $title = 'Revendedores';
        $title_sup = 'Lideres';
        break;
}
//Buscamos los usuarios de la $categoria elegida para la $campaña elegida
$qrystr = "SELECT DISTINCT(cpd.$cat) AS elemento FROM comp_pedidos_defi AS cpd
           INNER JOIN pedido AS ped ON(ped.id_pedidos=cpd.id_pedido)
           INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
           WHERE cro.campania = $campania AND cpd.id_vendedor != 'CAMBIO' AND cpd.$cat != cpd.$cat_sup ORDER BY elemento";
$qry = mysql_db_query($c_database,$qrystr,$link);

$campania_anterior = $campania - 1;
$c = 1;
while($row = mysql_fetch_array($qry)){
    $qrystr1 = "SELECT DISTINCT(cpd.$cat_sup) AS userFoundIt FROM comp_pedidos_defi AS cpd
                INNER JOIN pedido AS ped ON(ped.id_pedidos=cpd.id_pedido)
                INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
                WHERE cro.campania = $campania_anterior AND cpd.id_vendedor != 'CAMBIO' AND cpd.$cat_sup = '".$row[elemento]."'";
    $qry1 = mysql_db_query($c_database,$qrystr1,$link);
    if(mysql_num_rows($qry1) > 0){
        $row1 = mysql_fetch_array($qry1);
        
        $qrystr2 = "INSERT INTO reporte (clave_ses,ref1,ent1)
                    SELECT '".$sesion."',CONCAT(usu.apellido,', ',usu.nombres,' (',usu.cod_us,')') AS c,$c
                    FROM usuario AS usu WHERE usu.cod_us = '".$row1[userFoundIt]."'";
        $qry2 = mysql_db_query($c_database,$qrystr2,$link);
        $c++;
    }
}

$qrystrAUX = "SELECT obs FROM campanias WHERE id_campania = $campania";
$qryAUX = mysql_db_query($c_database,$qrystrAUX,$link);
$rowAUX = mysql_fetch_array($qryAUX);

$reporte = "Reporte: Informe de usuarios que perdieron categoria - ".$rowAUX[obs];
//Recopilacion de Datos
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si=0;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('');

$pdf->SetFont('Arial','',9);

$nota="<br><B>Genero:  <U>$usuario</U></B> <br>
Generador automatica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$header=array('',$title); //encabezados de columnas (en todos)
$anchos=array(10,150); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','L'); //L,R,C
$total=array(2,''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie

 $qrystr3 = "SELECT CONCAT(ent1,';',ref1) AS c FROM reporte WHERE clave_ses = '".$sesion."' ORDER BY ref1 ASC";

 $pdf->SetFont('Arial','',7); 
 $msj = "Este informe muestra la cantidad de usuarios que perdieron categoria - Desde ".$title_sup." a ".$title.".";
 $pdf->WriteHTML($msj);
 $data=$pdf->LoadData($qrystr3);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(array_sum($anchos),$pdf->altoFila);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
    $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',7);
 $pdf->BasicTable($header,$data,1);
 $pdf->Ln(5);
 $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
 $pdf->Ln(10);
    
    // ----------- borramos recopilacion de datos ----------
    $qrystr4 = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
    $qry4 = mysql_db_query($c_database,$qrystr4,$link);
    // ----------------------------------------

$pdf->WriteHTML($nota);
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur® para VD');
$pdf->Output();
?>
