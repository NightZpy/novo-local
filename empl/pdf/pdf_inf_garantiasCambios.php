<?php 
include ("../../conexion.php");
include("pdf_set5.php");
require_once "../../../clases/interfaz/classes_listas.php";

$sesion=crear_clave_sesion();
extract($_POST);
$fil="1";
switch ($filtro)
{
	case "fecha":
		$fil.=" AND (cp.fecha BETWEEN '".$fecha_d."' AND '".$fecha_h."')";	
	break;
	case "campania":
		$fil.=" AND (c.campania>=".$c_menor." AND c.campania<=".$c_mayor.")";
	break;
	default:
	break;
}

if ($distribuidor!='todos')
{
	$fil.=" AND cp.".$nivel."='".$distribuidor."'";
}

if ($tipo_joya!='')
{
	$fil.=" AND cp.id_vd LIKE '".$tipo_joya."'";
}

$create="CREATE TEMPORARY TABLE temp (fecha varchar(20) not null,
									   id_vd varchar(20) not null,
									   descripcion varchar(100) not null,
									   ubicacion varchar(50) not null,
									   pvp float(6,2) not null,
									   ingresos float(6,0) not null,
									   egresos float(6,0) not null,
									   ventas float(6,0) not null,
									   relacion float(6,2) not null,
									   id_comp_ped INT(11))";
$qry_create=mysql_db_query($c_database,$create,$link);

$sql="INSERT INTO temp (fecha,id_vd,descripcion,ubicacion,pvp,ingresos,egresos,ventas,relacion,id_comp_ped)
	  SELECT cp.fecha,
			 cp.id_vd, 
			 pro.nombreproducto as Descripcion, 
			 pro.ubi as Ubicacion, 
			 IF(cp.cantidad>0,(cp.cantidad*cp.preciounidad),-1*(cp.cantidad*cp.preciounidad)) as PVP, 
			 SUM( ( -1 ) * cp.cantidad * ( p.anexo = 'cambio_cred' ) ) AS ingresos, 
			 SUM( cp.cantidad * ( p.anexo <> 'cambio_cred' ) * ( cp.id_vendedor = 'CAMBIO' ) ) AS egresos, 
			 SUM( cp.cantidad * ( cp.id_vendedor <> 'CAMBIO' ) ) AS ventas, 
			 ( SUM( ( -1 ) * cp.cantidad * ( p.anexo = 'cambio_cred' ) ) / SUM( cp.cantidad * ( cp.id_vendedor <> 'CAMBIO' ) ) ) *1 AS relacion,
			 cp.id_comp_ped 
	  FROM comp_pedidos_defi AS cp 
	  	INNER JOIN productos as pro ON pro.id_vd=cp.id_vd 
	  	INNER JOIN pedido AS p ON cp.id_pedido = p.id_pedidos 
	  	INNER JOIN cronograma AS c ON p.id_presentacion = c.id_presentacion 
	  	WHERE ".$fil." 
	  GROUP BY cp.id_vd 
	  HAVING ( ingresos <>0 OR egresos <>0 ) 
	  ORDER BY cp.id_vd
";
$qry_insert=mysql_db_query($c_database,$sql,$link);

$sql="SELECT *
		FROM temp";
$qry=mysql_db_query($c_database,$sql,$link);

while($res=mysql_fetch_object($qry)){
	$sql="SELECT * FROM cambio_producto_amp WHERE id_vd='".$res->id_vd."' AND id_comp_ped=".$res->id_comp_ped." AND ace_rch_rep='ACE'";
	$qry_cambio=mysql_db_query($c_database,$sql,$link);
	$row=mysql_num_rows($qry_cambio);
	if ($row==0)
	{
		$delete="DELETE FROM temp WHERE id_comp_ped=".$res->id_comp_ped."";
		$qry_delete=mysql_db_query($c_database,$delete,$link);
	}
}

$sql="SELECT CONCAT(fecha,';',id_vd,';',descripcion,';',ubicacion,';',pvp,';',ingresos,';',egresos,';',ventas,';',relacion) as c
	  FROM temp";

switch ($salida)
{
	case "PDF":
		$pdf=new PDF();
		$pdf->open();
		$pdf->AliasNbPages();
		$header=array("Fecha","id_vd","Descripci�n","Ubicaci�n","PVP","Ingresos","Egresos","Ventas","Relaci�n");
		$anchos=array(20,20,40,20,20,18,18,18,20);
		$alig=array("L","L","L","L","R","R","R","R","R");
		$total=array('Total','','','','','','','','');
		$reporte="Garant�as y Cambios a ventas X campa�a";
		$pdf->SetTitle($reporte);
		$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
		$pdf->Setcreator('IDDelSur para VD');
		$data=$pdf->LoadData($sql);


		$pdf->SetFont('Arial','',10);
		$pdf->AddPage('P');
		$pdf->SetFont('Arial','',9);
		$pdf->BasicTable($header,$data,1);
		
		$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
		$pdf->Ln();
		$pdf->WriteHTML($nota);
		$pdf->Output();

	break;
	case "EXCEL":
		$lista = new Listas();
     	$lista->titulos=array("Fecha","id_vd","Descripci�n","Ubicaci�n","PVP","Ingresos","Egresos","Ventas","Relaci�n");
     	$data=$lista->LoadData($sql);
     	$lista->BasicTable($data);
     	$lista->OutPut($salida);
	break;
	default:
	break;
}
?>