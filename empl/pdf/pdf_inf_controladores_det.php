<?php
/*Informaci�n de los par�mertro de Entrada:
$usuario          -- Si no viene toma 'FERSOTO'
$fecha_d          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$fecha_h          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$incremento       -- [hour|day|month|year] Es el incremento para el Resumen. Por defecto es un d�a (day) por Resumen.
*/
include ("../../conexion.php");
include("pdf_set.php");
$sesion=crear_clave_sesion();
if (!isset($usuario))
  $usuario="FERSOTO";

$qrystrFecha = " SELECT *
                 FROM fecha_manejo
                 WHERE usuario = '$usuario' ";

//echo $qrystrFecha;
$qryFecha = mysql_db_query($c_database,$qrystrFecha,$link);
$rowFecha = mysql_fetch_array($qryFecha);

if(!isset($fecha_d))
  $fecha_d = $rowFecha["fecha_desde"];
if(!isset($fecha_h))
  $fecha_h = $rowFecha["fecha_hasta"];

if(!isset($incremento))
  $incremento = "day";

$fecha_temp = $fecha_d;
$fecha_temp1 = date("Y-m-d", strtotime ("$fecha_temp +1 $incremento"));
if (strtotime($fecha_temp1) > strtotime($fecha_h))
  $fecha_temp1 =$fecha_h;

//*********************
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
//instanciando... las variables
$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$usuario=strtoupper($usuario);
$header=array('Cod_Us','Apellido y Nombres','Unidades','Pedidos'); //encabezados de columnas (en todos)
$anchos=array(40,80,35,35); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','R','R'); //L,R,C
$total=array('Total:',2,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('Martin C');
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
$reporte="Reporte: Informe de Armadoras desde $fecha_d hasta $fecha_h (no inclusive)";//nombre del reporte (en todos)
// ---------------- fin variables ---------
$pdf->SetTitle($reporte);

//Imprime el Resumen Total.
$pdf->SetFont('Arial','',9);
ImprimirSubInforme($fecha_d,$fecha_h,"Reporte: Informe de Controladores");

//Imprime el los Detalles Uno a Uno.
while(strtotime($fecha_temp) < strtotime($fecha_h))
 {
  ImprimirSubInforme($fecha_temp,$fecha_temp1,"Reporte: Detalle Grupo");
  $fecha_temp = $fecha_temp1 ;
  $fecha_temp1 = date("Y-m-d", strtotime ("$fecha_temp +1 $incremento"));
  if (strtotime($fecha_temp1) > strtotime($fecha_h))
    $fecha_temp1 =$fecha_h;
  
 }

$nota="<br>$row[texto]<br>
<B>Genero:  <U>$usuario</U></B> <br>
Generador automatico de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:sistemas@joyasvanesaduran.com'>
sistemas@joyasvanesaduran.com</A>,<br>";
$pdf->WriteHTML($nota);
$pdf->Output();

//********************************************************
function ImprimirSubInforme($fecha_d,$fecha_h,$encab)
 {
  global $sesion;
  global $c_database;
  global $qrystr;
  global $link;
  global $pdf;	
  global $header;
  global $anchos;
  global $alig;
  global $total;
  global $reporte;	
  //Recopilacion de Datos
  /*$qrystr = " INSERT INTO reporte (clave_ses,ref1,ref2,ent1,ent2)
              SELECT '$sesion',p1.us_control,concat(u.apellido,' ',u.nombres),sum(p2.un_env+p2.un_acc+p2.un_otr),COUNT(*)
              FROM pedido AS p1
                LEFT JOIN pedidos_defi AS p2 ON p1.id_pedidos= p2.id_pedidos
                LEFT JOIN usuario AS u ON u.cod_us = p1.us_control
              WHERE  fecha_fin_control >= '$fecha_d' AND fecha_fin_control < '$fecha_h'
              and p1.us_control <> ''
              GROUP BY 1,2";
  //LIMIT 100";
  */
// debo obtener los elementos del grupo a excluir (242) 
$strsqlgrup ="SELECT elemento  FROM `grupos_lista` WHERE `id_grupo` = 242";
$grupo_excl =array();
$qrygrup=mysql_db_query($c_database,$strsqlgrup,$link);
while ($rowgrup = mysql_fetch_array($qrygrup))
{array_push($grupo_excl,$rowgrup["elemento"]);}
$excluidos= implode("','",$grupo_excl);
//echo "<br>$excluidos";  
  
  
  
  
  
$qrystr = " INSERT INTO reporte (clave_ses,ref1,ref2,ent1,ent2)
SELECT '$sesion',p1.us_control,concat(u.apellido,' ',u.nombres),sum(p2.cantidad),COUNT(distinct p2.id_vendedor) 
FROM pedido AS p1 
LEFT JOIN comp_pedidos_defi AS p2 ON p1.id_pedidos= p2.id_pedido 
LEFT JOIN usuario AS u ON u.cod_us = p1.us_control 
WHERE fecha_fin_control >= '$fecha_d' AND fecha_fin_control < '$fecha_h' 
and p1.us_control <> '' 
and p2.cantidad >0 
and p2.id_vd not in ('$excluidos') 
GROUP BY 1,2";
  
  
  
  //echo "<br>------------------------------<br>$qrystr--------------------------------------<br>";
 
 $qry = mysql_db_query($c_database,$qrystr,$link);

  //Iniciando PDF
  // aca van los select del load data
  $qrystr = "SELECT concat(ref1,';',ref2,';',ent1,';',ent2) as c
             FROM reporte
             WHERE clave_ses='$sesion' and ref1 !=''
             ORDER BY ent1  DESC";
  // ---------------- fin variables ---------
  $reporte="$encab desde $fecha_d hasta $fecha_h (no inclusive)";//nombre del reporte (en todos)
  $data=$pdf->LoadData($qrystr);
  //print_r($data);
  // ----------- borramos recopilacion de datos ----------

  $qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
  $qry = mysql_db_query($c_database,$qrystr,$link);

  // ----------------------------------------

  $pdf->SetFont('Arial','',10);
  $pdf->AddPage('P');
  $pdf->BasicTable($header,$data,1);
  //$pdf->ImprovedTable($header,$data);
  //$pdf->FancyTable($header,$data);
  $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
  $pdf->Ln();
 }

?>

