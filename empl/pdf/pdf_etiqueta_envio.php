<?php
include ("../../conexion.php");
include("pdf_set_eti1.php");
require_once('pdfbarcode128.inc');
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('Vanesa Duran S.A.');
$pdf->SetAutoPageBreak(false,0);
$pdf->AddPage();

//Recopilacion de Datos
$qrystr = " SELECT id_envio,bultos,fecha_envio,concat(u.apellido,' ',u.nombres) AS paq,
                   u2.localidad as loc,u2.cp as cpl,u2.provincia as prov,u2.direccion as dir,u.telefono as tel,t.nombre as transp,u2.destinatario,u2.flete
            FROM envios_distrib
            LEFT JOIN usuario_envio AS u ON u.cod_us = envios_distrib.id_distrib
            INNER JOIN usuario_envio_dir AS u2 ON u2.cod_us = envios_distrib.id_distrib AND u2.predet=1
            LEFT JOIN transportes as t ON envios_distrib.id_empresa=t.id_empresa
			WHERE id_envio = $id_envio
           ";

//echo $qrystr;
//exit;
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
if($row[0]=="")
 {
  $pdf->SetFont('Arial','',15);

  $pdf->Cell(90,3,"���DESTINATARIO INEXISTENTE!!! Agr�guelo y actualice sus datos.");
 }
 else
 {
	if($row[1]==0)
 	{
  		$pdf->SetFont('Arial','',15);

  		$pdf->Cell(90,3,"���BULTOS INEXISTENTE!!! Agr�guelo y actualice sus datos.");
	 }
 	else
 	{
		if($row[transp]=="")
		{
 			$pdf->SetFont('Arial','',15);

 			 $pdf->Cell(90,3,"���TRANSPORTE INEXISTENTE!!! Agr�guelo y actualice sus datos.");

		}
	}
 }

//var_dump($row);
$ancho_hoja=210;
$alto_hoja=297;
$ancho_eti=200;
$alto_eti=50;
$esp_eti_h=5;
$esp_eti_v=0;
//$alto_renglon=5;
$lmargin=0;
$rmargin=0;
$tmargin=5;
$bmargin=0;
$x=0;
$y=0;
$x=$lmargin;
$y=$tmargin;
for($i=0;$i<$blanco;$i++)
{
 if ($x+$ancho_eti>$ancho_hoja)
     {
      $y=$y+$alto_eti+$esp_eti_h;
      $x=$lmargin;
     }
 if ($y+$alto_eti>$alto_hoja)
              {
               $pdf->AddPage();
               $x=$lmargin;
               $y=$tmargin;
              }
 $x=$x+$ancho_eti+$esp_eti_v;
}

if($row[transp]!="")
{
for($j=1;$j<=$row[bultos];$j++)
{
 $cod='000000000';
 $cod.=$j.$row[id_envio];
 $cod=substr($cod,-9);

 $k=$j-1;
 $sql="SELECT idBulto FROM bultos_envios WHERE id_envio=".$id_envio." LIMIT ".$k.",1";
 //echo $sql;
 $qry_b=mysql_db_query($c_database,$sql,$link);
 $res_b=mysql_fetch_object($qry_b);

 $sql="UPDATE bultos_envios SET codBarra='".$cod."' WHERE id_envio=".$id_envio." AND idBulto=".$res_b->idBulto."";
$res2=mysql_db_query($c_database,$sql,$link);

 $code = new pdfbarcode128($cod, 3);
 $code->set_pdf_document($pdf);
 $width = $code->get_width();
 if ($x+$ancho_eti>$ancho_hoja)
     {
      $y=$y+$alto_eti+$esp_eti_h;
      $x=$lmargin;
     }
 if ($y+$alto_eti>$alto_hoja)
              {
               $pdf->AddPage();
               $x=$lmargin;
               $y=$tmargin;
              }
 $pdf->SetFont('Arial','',15);
 $pdf->x=$x+5;
 $pdf->y=$y+3;
 $pdf->Cell(200,60,'',1,0,'T');
 $pdf->x=$x+10;
 $pdf->y=$y+7;
 $pdf->Cell(90,3,"Fecha: " . $row[fecha_envio]);
 $pdf->x=$x+80;
 $pdf->y=$y+7;
 $pdf->Cell(90,3,"N� Envio: " . $row[id_envio]);
 $pdf->x=$x+160;
 $pdf->y=$y+7;
 $pdf->Cell(90,3,"Bulto: " . $j ." de " . $row[bultos]);
 $pdf->x=$x+10;
 $pdf->y=$y+17;
 $row[paq] = ucwords(strtolower($row[paq]));
 //$pdf->ClippedCell(180,5,"Destinatario: " . $row[destinatario]);
 $pdf->WriteHTML("Destinatario: <b>" . $row[destinatario]."</b>");
 $pdf->x=$x+10;
 $pdf->y=$y+27;
 $pdf->Cell(90,3,"N� Tel: " . $row[tel]);
 $pdf->x=$x+10;
 $pdf->y=$y+37;
 $row[dir] = ucwords(strtolower($row[dir]));
 $row[loc] = ucwords(strtolower($row[loc]));
 //$pdf->ClippedCell(180,5,"Dir: " . $row[dir] . " - Loc: " . $row[loc]);
 $pdf->WriteHTML("Dir: <b>" . $row[dir] . "</b> - Loc: <b>" . $row[loc]."</b>");
 $pdf->x=$x+10;
 $pdf->y=$y+47;
 $row[prov] = ucwords(strtolower($row[prov]));
 //$pdf->Cell(90,3,"CP: " . $row[cpl] . " - Pcia: " . $row[prov]);
 $pdf->WriteHTML("CP: " . $row[cpl] . " - <b>Pcia</b>: " . $row[prov]."");
 $pdf->x=$x+10;
 $pdf->y=$y+57;
 $row[transp] = ucwords(strtolower($row[transp]));
 //$pdf->Cell(90,3,"Empresa de Transporte: " . $row[transp]);

 $pdf->WriteHTML("Empresa de Transporte:<b> " . $row[transp]."</b>");
 $pdf->x=$x+140;
 $pdf->y=$y+57;
 switch($row[flete])
 {
 	case "origen":
 		$row[flete] =ucwords(strtolower("Terminal"));
	break;

	case "destino":
		$row[flete] =ucwords(strtolower("Domicilio"));
	break;

	default:
	break;
 }
// $row[flete] = ucwords(strtolower($row[flete]));
 $pdf->WriteHTML("Destino: <b>" . $row[flete]."</b>");
 $code->draw_barcode($x+150,$y+23,8,true);
 $pdf->SetDrawColor(0,0,0);
 $x=$x+$ancho_eti+$esp_eti_v;
 $y=$pdf->y-20;
 }
 }
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
$pdf->Output();
?>
