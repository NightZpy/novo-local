<?php
include ("../../conexion.php");
include("pdf_set6.php");
//Recopilacion de Datos
$alto=4;

//ECHO"PROVEEDOR!!! $par_id_proveedor";

$qrystr_prov="SELECT id_proveedor 
              FROM orden_compra_comp AS occ 
              INNER JOIN orden_compra AS oc ON occ.id_orden=oc.id_orden
              WHERE occ.id_orden=$par_id_orden";
$qry_prov = mysql_db_query($c_database,$qrystr_prov,$link);
$row_prov = mysql_fetch_array($qry_prov);

//echo"$qrystr_prov";
$id_proveedor=$row_prov[0];


$qrystr_pres="SELECT scp.id_presupuesto,occ.id_sol FROM orden_compra_comp AS occ
              INNER JOIN sol_compra_presupuestos AS scp ON occ.id_sol=scp.id_sol_compra WHERE occ.id_orden=$par_id_orden";
$qry_p = mysql_db_query($c_database,$qrystr_pres,$link);
$row_pres = mysql_fetch_array($qry_p);

if($row_pres[0]<>'')
{$presupuesto=$row_pres[0];}
else
{$presupuesto="***SIN PRESUPUESTO***";}

$qrystr_prov="SELECT pro.razon_social,pro.cuit,pro.domicilio,pro.tel_fax,pro.email
              FROM orden_compra AS oc
              INNER JOIN proveedor AS pro ON oc.id_proveedor=pro.id_proveedor
              WHERE oc.id_orden=$par_id_orden AND oc.id_proveedor='$id_proveedor'";
$qry_pr = mysql_db_query($c_database,$qrystr_prov,$link);
$row_prov = mysql_fetch_array($qry_pr);
              
//echo"$qrystr_prov";

$qrystr = "INSERT INTO reporte (clave_ses,ref6,ref1,ref2,ent1,ref5,ref3,ref4,float2,float1)
            SELECT '$sesion',scc.id_prod_prov,IFNULL(occ.id_orden_comp,''),occ.producto,IFNULL(occ.cantidad,''),oc.momento,IFNULL(ee.valor,''),IFNULL(occ.detalle,''),ROUND(IFNULL(occ.precio,''),6),(occ.precio*occ.cantidad)
            FROM orden_compra_comp AS occ
            INNER JOIN orden_compra AS oc ON occ.id_orden=oc.id_orden
            INNER JOIN listas_precios_productos_prov AS lppp ON occ.id_lista=lppp.id_lista
            INNER JOIN sol_compra_comp AS scc ON scc.id_sol_compra_comp=occ.id_sol
            INNER JOIN enum_elementos AS ee ON scc.uni_medida=ee.id_enum
            WHERE occ.id_orden=$par_id_orden AND oc.id_proveedor=$id_proveedor
            GROUP BY id_orden_comp,occ.producto,occ.detalle"; //Poner esto si es necesario.
       
$qry = mysql_db_query($c_database,$qrystr,$link);

//echo"$qrystr";

//instanciando... las variables
$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
// $nota="<br>$row[texto]<br>
// <B>Gener�:  <U>$usuario</U></B> <br>
// Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
// www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
// soporte@iddelsur.com.ar</A>,<br>";

$qrystr_fech = "SELECT ref5 as momento
           FROM reporte
           WHERE clave_ses='$sesion'";
//echo $qrystr_fech;         
           
$qry_fech = mysql_db_query($c_database,$qrystr_fech,$link);
$row_fech = mysql_fetch_array($qry_fech);
$momento = $row_fech[momento];
          
// ---------------- fin variables ---------
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->Ln();
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$pdf->tipoHeader=3;
$pdf->tipoFooter=2;
//print_r($data);
$reporte=utf8_decode("Nº ORDEN DE COMPRA:" ."$par_id_orden");
$reporte1=utf8_decode("Nº DE PRESUPUESTO:") ."$presupuesto";//nombre del reporte (en todos)
$reporte2="FECHA: $momento";
$pdf->AddPage();
//$renglones[8]="";
$pdf->SetFont('Arial','',8);
  
  $header=array('DATOS DEL PROVEEDOR'); //encabezados de columnas (en todos)
   $anchos=array(190); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('C'); //L,R,C

   $pdf->SetFont('Arial','B',10);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7);
   ////////////////////////////////////////////////
   $header=array('RAZON SOCIAL:',$row_prov[0],utf8_decode('Nº-CUIT:'),$row_prov[1]); 
   $anchos=array(50,100,15,25); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L','L','L'); //L,R,C

   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7);
   ////////////////////////////////////////////////
     $header=array('DOMICILIO: ',$row_prov[2]); 
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C

   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7);   
   ////////////////////////////////////////////////
     $header=array('TELEFONO: ',$row_prov[3]); 
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C

   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7); 
   ////////////////////////////////////////////////
     $header=array('E-MAIL: ',$row_prov[4]); 
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C

   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7); 
   
////////////////////////////////////////////////
   $pdf->Ln();
   $pdf->Ln();
   
     $header=array('DETALLE'); 
   $anchos=array(190); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L'); //L,R,C

   $pdf->SetFont('Arial','B',10);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
   $pdf->Ln();
   $pdf->SetFont('Arial','',7); 

$header=array('Articulo','Cod Proov','Cant','Unid-Med','Detalle','Precio Unit','Importe'); //encabezados de columnas (en todos)
$anchos=array(55,20,15,18,42,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','C','C','L','C','C'); //L,R,C

$notaalpie=""; //nota al pie
// aca van los select del load data
$qrystr = "SELECT concat(ref2,';',ref6,';',ent1,';',ref3,';',ref4,';',ROUND(float2,6),';',float1) as c
           FROM reporte
           WHERE clave_ses='$sesion'
           ORDER BY ref1 asc";
           
$pdf->SetFont('Arial','B',10);        
$data=$pdf->LoadData($qrystr);
$pdf->EncabezarTabla();
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$data,0);

$qrystr11 = "SELECT ROUND(sum(float1),6) as subtot,ROUND(SUM(float1*0.21),2) as iva,ROUND(sum(float1*1.21),6) as tot
           FROM reporte
           WHERE clave_ses='$sesion'";
           
$qry11 = mysql_db_query($c_database,$qrystr11,$link);
$row_tot = mysql_fetch_array($qry11);
verificar('',$qrystr11);

   $subtotal_t=number_format($row_tot[0],2);
//$pdf->ImprovedTable($header,$data);
     $header=array('','SUBTOTAL',$subtotal_t); 
   $anchos=array(150,20); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','R'); //L,R,C
   $total=array('Total','1');

   $pdf->SetFont('Arial','',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
//    $pdf->Ln();
//    $pdf->SetFont('Arial','',7); 
// 
//      $header=array('','IVA',$row_tot[1]); 
//    $anchos=array(150,20,20); //anchos de cada celda procurar que sumen aprox 190-
   $total_t=number_format($row_tot[0],2);
   $alig=array('L','R'); //L,R,C
   $total=array('Total','1');

   $pdf->SetFont('Arial','',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
   $pdf->Ln();
   $pdf->SetFont('Arial','',7); 
   
     $header=array('','TOTAL',$total_t); 
   $anchos=array(150,20,20); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','R'); //L,R,C
   $total=array('Total','1');

   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7); 
    $pdf->Ln();
        $pdf->Ln();
///////////////////////////////////////////////////////////////////////////////////  
/////////////////////////////////////////////////////////////////////////////////// 

$qrystr_pago="SELECT contado,domicilio_envio,transporte,obs,ctacte FROM orden_compra WHERE id_orden=$par_id_orden";
$qry_p = mysql_db_query($c_database,$qrystr_pago,$link);
$row_pago = mysql_fetch_array($qry_p);

///////////////////////////////////////////////////////////////////////////////////  
/////////////////////////////////////////////////////////////////////////////////// 
$contado=$row_pago[0];
$ctacte=$row_pago[4];

if($contado==1)
  $cond_pago1=" SI";
else
  $cond_pago1=" NO";
if($ctacte==1)
  $cond_pago2=" SI";
else
  $cond_pago2=" NO";
 

  $header=array('CONDICIONES DE PAGO','CONTADO',$cond_pago1,'CTA.CTE',$cond_pago2); //encabezados de columnas (en todos)
   $anchos=array(130,20,10,20,10); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L'); //L,R,C
 
    $pdf->Ln();
   $data=array();
//     $data[1]=array('CTA.CTE','');
   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','B',7.5);
      $pdf->BasicTable($header,$data,0);

///////////////////////////////////////////////////////////////////////////////////   

  $header=array('DOMICILIO DE ENVIO',$row_pago[1]); //encabezados de columnas (en todos)
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C

   $pdf->Ln();     
   $data=array();
//    $data[0]=array('DOMICILIO','');
   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7);

///////////////////////////////////////////////////////////////////////////////////      
   
  $header=array('TRANSPORTE SUGERIDO',$row_pago[2]); //encabezados de columnas (en todos)
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C

   $pdf->Ln();     
   $data=array();
//    $data[0]=array('DOMICILIO','');
   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->SetFont('Arial','',7);
   $nota_obs ="$row_pago[3]";
///////////////////////////////////////////////////////////////////////////////////   
//  echo"$row_pago[3]";die();
  $header=array('OBSERVACIONES',); //encabezados de columnas (en todos)
   $anchos=array(50,140); //anchos de cada celda procurar que sumen aprox 190-
   $alig=array('L','L'); //L,R,C
     
   $pdf->Ln();
     
   $data=array();
//    $data[0]=array('DOMICILIO','');
   $pdf->SetFont('Arial','B',9);
   for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,$alig[$i]);
   $pdf->Ln();
   $pdf->WriteHTML($nota_obs);
   $pdf->SetFont('Arial','',7);

  
$pdf->Ln();

//$pdf->FancyTable($header,$data);
//$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();

$pdf->WriteHTML($nota);
$pdf->Output();

// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
?>
