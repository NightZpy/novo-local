<?php
	$pdf->Ln();
// TABLA ACUMULADO DE GARANTIAS - Javier 20130509 ###########################################################
/*
        $header_garantia=array('Garantia','Quantidade'); //encabezados de columnas (en todos)
		$total_garantia=0; // texto, 1 (suma) f %01.2f, 2 (cuenta), 3 (ultimo reg),5 (suma) f %01d, 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

		$qrystr_gar = " SELECT concat(p2.id_vd,';',sum(cp.cantidad)) AS c
               FROM comp_pedidos AS cp
               INNER JOIN productos AS p1 ON cp.id_vd = p1.id_vd
               INNER  JOIN productos AS p2 ON p1.tipo_garantia = p2.id_vd AND p1.tipo_garantia<>''
               WHERE 1 AND cp.id_pedido=".$pedido." AND cp.id_vendedor='".$usuario2."'
									AND cp.cantidad >= 0
									AND cp.id_vd NOT IN (SELECT gl.elemento
								FROM grupos_lista AS gl
								WHERE gl.id_grupo=135 or gl.id_grupo=242)
               GROUP BY p2.id_vd
               ORDER BY p2.id_vd";
		$data_garantia=$pdf->LoadData($qrystr_gar);
		$html_garantia=$pdf->creaTabla($header_garantia,$data_garantia,$total_garantia);
		$pdf->WriteHTML(utf8_encode($html_garantia));
		$pdf->Ln();
// #######################################################################################################
*/

	$header_pack=array('Packaging','Quantidade'); //encabezados de columnas (en todos)
	$total_pack=0;
	//$anchos=array(50,20); //anchos de cada celda procurar que sumen aprox 190-
	//$alig=array('L','L'); //L,R,C

	$qrystr_pack = " SELECT concat(concat('(',p2.id_vd,') ',CASE 
         WHEN p2.id_vd= 'CA0001' THEN 'ENVELOPE COMUN'
         WHEN p2.id_vd= 'CA0003' THEN 'CAIXA DE ANEL'
         WHEN p2.id_vd= 'CA0004' THEN 'CAIXA PEQUENHA'
         WHEN p2.id_vd= 'CA0005' THEN 'CAIXA MEDIANA'
         WHEN p2.id_vd= 'CA0006' THEN 'CAIXA GRANDE'
         WHEN p2.id_vd= 'CA0007' THEN 'CAIXA DE PULSEIRA PEQUENA'
         WHEN p2.id_vd= 'CA0008' THEN 'CAIXA DE PULSEIRA GRANDE'
         WHEN p2.id_vd= 'CA0018' THEN 'ENVELOPE INFANTIL'
         WHEN p2.id_vd= 'CA0014' THEN 'CAIXA DE BRACELETE'
         WHEN p2.id_vd= 'CA0019' THEN 'SACOLA CHICA OVERSIZE'
         WHEN p2.id_vd= 'CA0020' THEN 'SACOLA GRANDE OVERSIZE'
         WHEN p2.id_vd= 'CA0015' THEN 'PACKAGING RELOGIO VANESA DURAN'
         ELSE p2.nombreproducto
 END),';',sum(cp.cantidad)) AS c
			   FROM comp_pedidos as cp
			   INNER JOIN productos AS p1 ON cp.id_vd = p1.id_vd
			   INNER  JOIN productos AS p2 ON p1.id_producto_empaque = p2.id_producto
			   WHERE 1 AND cp.id_pedido=".$pedido." AND cp.id_vendedor='".$usuario2."'
		 AND cp.cantidad >= 0
		 AND cp.id_vd NOT IN (SELECT gl.elemento
								FROM grupos_lista AS gl
								WHERE gl.id_grupo=135 or gl.id_grupo=242)
			   GROUP BY p2.id_vd
			   ORDER BY p2.id_vd";
	//echo $qrystr;
	$data_pack=$pdf->LoadData($qrystr_pack);
	$pdf->SetFont('Arial','',7);
	$html_pack=$pdf->creaTabla($header_pack,$data_pack,$total_pack);
	$pdf->WriteHTML($html_pack);
	$pdf->Ln();
?>
