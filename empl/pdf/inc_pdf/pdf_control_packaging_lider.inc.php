<?php
$pdf->AddPage();
	$barcode = $id_ped_defi;
	$fecha=$today = date("d/m/y");
	$code = new pdfbarcode128($barcode, 2 );
	$code->set_pdf_document($pdf);
	$width = $code->get_width();
	$code->draw_barcode(150, 2, 7,true);
	$html = '
	<div style="margin-top: -15px;">
	<img src="vd_srl.jpg" class="left">
	<H4 class="center" style="margin: 0px; width: 450px;" >'.$tit.'</H4>
	<H4 class="center" style="font-size:24px;margin:0px 0px 0px 150px">'.$pedido.'</H4>
	<H1 style="font-size:24px;text-align: right;font-weight: bold;margin:-25px- 5px 0px 0px;">'.$cant_rev.'</H1>
	<H4 style="text-align: right;font-weight: bold;margin:0px- 5px 0px 0px;">'.$nombrerev.'</H4>
	<H4 style="text-align: right;font-weight: bold;margin:0px- 5px 0px 0px;"> '.$fecha.' </H4>
	</div>';
	$pdf->WriteHTML($html);
	$html="
		<p class='left' style='margin-top:-15px'>".$id_lider."</p>
		<p class='left'>".$id_distrib."</p>
		<p class='left'>".$id_regional."</p>";
	//echo $html;
	$pdf->WriteHTML($html);

	$header=array('Packaging','Cantidad'); //encabezados de columnas (en todos)
	$anchos=array(50,20); //anchos de cada celda procurar que sumen aprox 190-
	$alig=array('L','L'); //L,R,C

	$qrystr = " SELECT concat(concat('(',p2.id_vd,') ',p2.nombreproducto),';',sum(cp.cantidad)) AS c
			   FROM comp_pedidos as cp
			   INNER JOIN productos AS p1 ON cp.id_vd = p1.id_vd
			   INNER  JOIN productos AS p2 ON p1.id_producto_empaque = p2.id_producto
			   WHERE 1 AND cp.id_pedido=".$pedido." AND cp.cantidad >= 0
		 AND cp.id_vd NOT IN (SELECT gl.elemento
								FROM grupos_lista AS gl
								WHERE gl.id_grupo=135 or gl.id_grupo=242)
			   GROUP BY p2.id_vd
			   ORDER BY p2.id_vd";
	//echo $qrystr;
	$data=$pdf->LoadData($qrystr);
	$pdf->SetFont('Arial','',7);
	$html=$pdf->creaTabla($header,$data,$total);
	$pdf->WriteHTML($html);
	$pdf->Ln();
?>
