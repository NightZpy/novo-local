<?php
$c_camp=$c_menor;
$distribuidor=$distribuidor;

require_once "../calcula_puntos_conv2011.inc";
$calculado=calcula_puntos($c_camp,$distribuidor);

if($c_menor>1075 AND $c_mayor<1079){
	require_once "../calcula_puntos_anticipo2011.inc";
	$calculado2=calcula_puntos_ant($c_camp,$distribuidor);
}

$reporte="RESUMEN - PUNTOS LINGOTES ";
$reporte1="";

$pdf->header_si=0;
$pdf->AddPage('P');
$pdf->header_si=1;

//?	Mostrar un encabezamiento introductorio referente a puntos dorados
$qrystr_adv = " SELECT texto FROM advertencia WHERE adv=1104";
$qry_adv = mysql_db_query($c_database,$qrystr_adv,$link);
verificar('',$qrystr_adv);
$row_adv = mysql_fetch_array($qry_adv);
$pdf->WriteHTML($row_adv[0]);
//echo"perte:::::::::::::$acum_pvp";

$imagen_pts="../img/imagen_informe_Puntos_Dorados.1.jpg";

$pdf->Image($imagen_pts,50,140,110,50);


$text_promo='';
if(!$calculado){
	$text_promo.="<b>NO SE HA PODIDO CORRER EL <FONT COLOR=RED> PROCESO DE PUNTOS </FONT> DEBIDO A QUE NO ESTAN TODOS LOS PEDIDOS PROCESADOS</b><br />";
}else{
	$total=0;
	//CAMPAÑA ACTUAL
	$qrystr_camp = "SELECT obs
	   FROM campanias  
	   WHERE id_campania='$c_camp'";
   // echo $qrystr_ped;
	$qry_camp = mysql_db_query($c_database,$qrystr_camp,$link);
	$row_camp = mysql_fetch_array($qry_camp);

	if($c_camp>'1075'){
		//CAMPAÑA ANTERIOR
			$c_camp_ant=$c_camp-1;
			$qrystr_camp_ant = "SELECT obs
			   FROM campanias  
			   WHERE id_campania='$c_camp_ant'";
	   // echo $qrystr_ped;
		$qry_camp_ant = mysql_db_query($c_database,$qrystr_camp_ant,$link);
		$row_camp_ant = mysql_fetch_array($qry_camp_ant);

		// CANTIDAD TOTAL POR AJUSTE DE LA CAMPAÑA ANT
		$qrystr_ajustes2 = "SELECT SUM(puntos) as total 
				   FROM promo_lingotes2011  
				   WHERE id_distrib='$distribuidor'
					 AND id_campania='$c_camp_ant'
					 AND tipo_puntos=8";
	   // echo $qrystr_ped;
		$qry_ajustes2 = mysql_db_query($c_database,$qrystr_ajustes2,$link);
		$row_ajustes2 = mysql_fetch_array($qry_ajustes2);	
		if(empty($row_ajustes2[total])){$total2=0;}else{$total2=$row_ajustes2[total];}
		
		// CANTIDAD TOTAL MES ANTERIOR 
		$qrystr_total = "SELECT SUM(puntos) as total
				   FROM promo_lingotes2011  
				   WHERE id_distrib='$distribuidor'
					 AND id_campania='$c_camp_ant'";
	   // echo $qrystr_ped;
		$qry_total = mysql_db_query($c_database,$qrystr_total,$link);
		$row_total = mysql_fetch_array($qry_total);
		$total_ant=ceil($row_total[total]);
		
		// (-) MES ANTERIOR
		$qrystr_res = "SELECT * 
				   FROM promo_lingotes2011  
				   WHERE id_distrib='$distribuidor'
					 AND id_campania='$c_camp_ant'
					 AND tipo_puntos=4";
	   // echo $qrystr_ped;
		$qry_res = mysql_db_query($c_database,$qrystr_res,$link);
		
		// (+) MES ANTERIOR
		$qrystr_suma = "SELECT * 
				   FROM promo_lingotes2011  
				   WHERE id_distrib='$distribuidor'
					 AND id_campania='$c_camp_ant'
					 AND tipo_puntos=3";
	   // echo $qrystr_ped;
		$qry_suma = mysql_db_query($c_database,$qrystr_suma,$link);
		
			if(mysql_num_rows($qry_suma)<>0 ) {
					$text_promo.="<br />Se le imputo al total del mes anterior (".$row_camp_ant[obs].") 100 Lingotes por la Cancelación administrativa en tiempo";
			}elseif(mysql_num_rows($qry_res)<>0 ){
				$text_promo.="<br />Se le imputo al total del mes anterior (".$row_camp_ant[obs].") -100 Lingotes por Pago fuera de término";
			}
			if($total2>0) {
				$text_promo.="<br />Puntos por ajustes: ".$total2;
			}
			$text_promo.="<br /><b>Total de lingotes del mes anterior (".$row_camp_ant[obs]."): ".$total_ant." </b><br />";

	}
	
	// CANTIDAD TOTAL POR AJUSTE
	$qrystr_ajustes = "SELECT SUM(puntos) as total
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=8";
   // echo $qrystr_ped;
	$qry_ajustes = mysql_db_query($c_database,$qrystr_ajustes,$link);
	$row_ajustes = mysql_fetch_array($qry_ajustes);
	if(empty($row_ajustes[total])){$total3=0;}else{$total3=$row_ajustes[total];}
	
	// CANTIDAD DE PUNTOS POR PEDIDOS
	$qrystr_ped = "SELECT puntos as ped
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=1";
   // echo $qrystr_ped;
	$qry_ped = mysql_db_query($c_database,$qrystr_ped,$link);
	$row_ped = mysql_fetch_array($qry_ped);
	if(empty($row_ped[ped])){$ped=0;}else{$ped=ceil($row_ped[ped]);}
	
	$text_promo.="<br />Lingotes por Pedidos: ".$ped;
	
	// CANTIDAD DE PUNTOS POR INCORPORACIONES
	$qrystr_inc = "SELECT puntos as inc
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=2";
   // echo $qrystr_ped;
	$qry_inc = mysql_db_query($c_database,$qrystr_inc,$link);
	$row_inc = mysql_fetch_array($qry_inc);
	if(empty($row_inc[inc])){$inc=0;}else{$inc=ceil($row_inc[inc]);}
	
	$text_promo.="<br />Lingotes por Incorporaciones: ".$inc;
	
	// CANTIDAD DE PUNTOS POR LIDER
	$qrystr_lid = "SELECT SUM(puntos) as lid
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=5";
   // echo $qrystr_ped;
	$qry_lid  = mysql_db_query($c_database,$qrystr_lid,$link);
	$row_lid  = mysql_fetch_array($qry_lid);
	if(empty($row_lid[lid])){$lid=0;}else{$lid=ceil($row_lid[lid]);}
	if($lid>0) {
		$text_promo.="<br />Total de Lingotes por Nuevo Lider: ".$lid;
	}
		
	// CANTIDAD DE PUNTOS POR DISTRIBUIDOR
	$qrystr_dis = "SELECT SUM(puntos) as dis
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=6";
   // echo $qrystr_ped;
	$qry_dis = mysql_db_query($c_database,$qrystr_dis,$link);
	$row_dis = mysql_fetch_array($qry_dis);
	if(empty($row_dis[dis])){$dis=0;}else{$dis=ceil($row_dis[dis]);}
		$text_promo.="<br />Total de Lingotes por Nuevo Distribuidor: ".$dis;

	
	// CANTIDAD DE PUNTOS POR REGIONAL
	$qrystr_reg = "SELECT SUM(puntos) as reg
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania='$c_camp'
                 AND tipo_puntos=7";
   // echo $qrystr_ped;
	$qry_reg = mysql_db_query($c_database,$qrystr_reg,$link);
	$row_reg = mysql_fetch_array($qry_reg);
	if(empty($row_reg[reg])){$reg=0;}else{$reg=ceil($row_reg[reg]);}
		$text_promo.="<br />Total de Lingotes por nuevo Regional: ".$reg;

	
	$total=$reg+$dis+$lid+$ped+$inc;
	
		// CANTIDAD DE PUNTOS ACUMULADOS
	$qrystr_acum = "SELECT SUM(puntos) as acumulado
               FROM promo_lingotes2011  
			   WHERE id_distrib='$distribuidor'
                 AND id_campania IN (1075,1076,1077,1078,1079,1080,1081)";
   // echo $qrystr_ped;
	$qry_acum = mysql_db_query($c_database,$qrystr_acum,$link);
	$row_acum = mysql_fetch_array($qry_acum);
	
	$reporte1="RESUMEN PROMO LINGOTES";
	$text_promo.="<br /><b>Total de Lingotes Disponibles en el mes de ".$row_camp[obs].": $total </b><br /> 
	(No incluye en el total: la Cancelación administrativa en tiempo (100 puntos) o el pago fuera de término (-100 puntos).<br />
	El puntaje se imputara en la próxima campaña)<br />";
	if($total3>0) {
		$text_promo.="<br />Puntos por ajustes: ".$total3;
	}
	if($c_camp>'1074'){
		$text_promo.="<br /><b>Total de lingotes hasta el mes de ".$row_camp[obs].": ".$row_acum[acumulado]." </b><br />";
	}
 }     
 $pdf->WriteHTML(utf8_decode($text_promo));
?>
