<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si = 0;
$reporte= "Reporte de Dosis";//nombre del reporte (en todos)
$reporte1="";//nombre del reporte (en todos)
$notaalpie="   "; //nota al pie
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' MKS Argentina');
$pdf->Setcreator('IDDelSur para MKS');
$pdf->AddPage('L');
$usuario=strtoupper($usuario);
$nota="<br>$row[texto]<br>
<B>Generó:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.mks-cordoba.com'>
www.mks-cordoba.com</A> | Consultas en <A href='mailto:info@mks-cordoba.com'>
soporte@iddelsur.com.ar</A>,<br>";
switch($nivel)
{
 case 'G':
  if(isset($id_regional))
    {
     if(isset($id_regional))
       {
        if($id_regional<>'')
          {
           $filtro = " AND u.id_regional = '$id_regional'";
          }
       }
    }      
  else
    {
     $filtro = "ADVERTENCIA: NO SE PUEDE MOSTRAR INFORMACIÓN DE TODAS LAS GERENCIAS...";
     echo $filtro;
     exit;
    } 
 break;
 case 'D':
  if($id_distrib<>'')  
    {
     $filtro = " AND u.id_distrib = '$id_distrib'";
    }
 break; 
 case 'L':
  if($id_lider<>'')  
    {
     $filtro = " AND u.id_lider = '$id_lider'";
    }
 break;
 case 'R':
  if($id_revendedor<>'')
    {
     $filtro = " AND u.cod_us = '$id_revendedor'";
    }
 break;
} 
$qrystr1 = " SELECT u.id_lider AS lider,u.id_distrib AS dist,u.id_regional AS ger,
                    CONCAT(u1.apellido,' ',u1.nombres,' (',u1.cod_us,')') AS nlider
             FROM ped_rev AS p
             INNER JOIN usuario AS u ON p.id_vendedor = u.cod_us
             INNER JOIN usuario AS u1 ON u.id_lider = u1.cod_us
             WHERE 1 $filtro
             GROUP BY lider
             ORDER BY ger,dist,lider
           ";//echo $qrystr1;
$qry1 = mysql_db_query($c_database,$qrystr1,$link);
$ug='';
$ud='';
while ($row1 = mysql_fetch_array($qry1))
{
 $anchos=array(95,140,25); //anchos de cada celda procurar que sumen aprox 190-
 $pdf->SetFont('Arial','',10);
 if($ug<>$row1[ger])
    {
     $q = "SELECT CONCAT(apellido,' ',nombres,' (',cod_us,')') AS paq FROM usuario WHERE cod_us = '$row1[ger]'";

     $qy = mysql_db_query($c_database,$q,$link);
     $row2 = mysql_fetch_array($qy);
     $ug=$row1[ger];
     $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
     $pdf->Ln();
     $pdf->Cell(100,7,'Gerencia: ' . $row2["paq"]);
     $pdf->Ln();
    }
 if($ud<>$row1[dist])
    {
     $q = "SELECT CONCAT(apellido,' ',nombres,' (',cod_us,')') AS paq FROM usuario WHERE cod_us = '$row1[dist]'";
     $qy = mysql_db_query($c_database,$q,$link);
     $row2 = mysql_fetch_array($qy);
     $ud=$row1[dist];
     $pdf->Cell(100,7,'Distribución: ' . $row2["paq"]);
     $pdf->Ln();
    }

 $header=array('Rev. del Lider: '.$row1["nlider"],'Tipo Perfil','Valor'); //encabezados de columnas (en todos)
 $anchos=array(95,140,25); //anchos de cada celda procurar que sumen aprox 190-
 $alig=array('L','L','R'); //L,R,C
 $total=array('Cantidad',2,'','','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

 $pdf->SetFont('Arial','',10);
 $pdf->Cell(100,7,'Lider: '.$row1["nlider"]);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
     $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',8);
 $qrystr = " SELECT CONCAT(CONCAT(u.apellido,' ',u.nombres,' (',u.cod_us,')'),';',tp.descri_tipo_perfil,';',p.calif_txt) AS c
                 FROM ped_rev AS p
                 INNER JOIN usuario AS u ON p.id_vendedor = u.cod_us
                 INNER JOIN ped_rev_tipos_perfil AS tp ON p.tipo_perfil=tp.id_tipo_perfil
                 WHERE u.id_lider='$row1[lider]'
                 ORDER BY CONCAT(u.apellido,' ',u.nombres,' (',u.cod_us,')')
               ";
 $data=$pdf->LoadData($qrystr);
 $pdf->SetFont('Arial','',8);
 $pdf->header_si = 0;
 $pdf->BasicTable($header,$data,0);
 $pdf->header_si = 0;
 $pdf->Ln(10);
}
$anchos=array(95,140,25);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
?>
