<?php
/****************************************************************************
* Software: pdf_set                                                         *
* Version:  1.60                                                            *
* Date:     2002/08/03                                                      *
* Author:   Gustavo DE MIGUEL  gdemiguel@iddelsur.com.ar                                             *
* License:  Freeware                                                        *
*                                                                           *
* You may use and modify this software as you wish.                         *
****************************************************************************/
define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;

function PDF($orientation='P',$unit='mm',$format='A4')
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{   global $anchos;
    global $altos;
    global $notaalpie;
	$this->Cell(array_sum($anchos),0,'','T');
    //Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	if ($this->CurOrientation=='L')
		{
			$this->Image('pie2_ancho.jpg',10,190,278,5);
		}
		else
		{
			$this->Image('pie2.jpg',10,278,190,5);
		}
	//Arial italic 8
	$this->SetFont('Arial','I',$altos+3);
	//N�mero de p�gina
	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
}

function header_orig()
{global $header;
 global $anchos;
 global $altos;
 global $reporte;
 global $reporte1;

 //if (!$altos) {$altos=8;}
 $this->Image('vd_srl.jpg',10,8,65);
 //Arial bold 1510
 $this->SetFont('Arial','B',9);
 //Movernos a la derecha
 $this->Cell(80);
 //T�tulo
 $this->Cell(110,7,'H.Irigoyen 31 5� Piso - Ciudad de C�rdoba - CP X5000JHA',0,8,'L');
 $this->Cell(110,7,'Tel (54) 351-4115130 351-4113795',0,8,'L');
 $this->SetFont('Arial','B',8);
 $this->Cell(110,6,$reporte,0,8,'L');
 $this->Cell(110,6,$reporte1,0,8,'L');
 $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
 $this->Ln(10);
 $this->SetFont('Arial','B',$altos+3);
 for($i=0;$i<count($header);$i++)
        $this->Cell($anchos[$i],7,$header[$i],1,0,'C');
    $this->Ln();

}

function header()
{global $header;
 global $anchos;
 global $altos;
 global $reporte;
 global $reporte1;
 global $renglones;

 //if (!$altos) {$altos=8;}
 $this->Image('vd_srl.jpg',10,8,65);
 //Arial bold 1510
 $this->SetFont('Arial','B',9);
 //Movernos a la derecha
 $this->Cell(80);
 //T�tulo
 $this->Cell(110,7,'H.Irigoyen 31 5� Piso - Ciudad de C�rdoba - CP X5000JHA',0,8,'L');
 $this->Cell(110,7,'Tel (54) 351-4115130 351-4113795',0,8,'L');
 $this->SetFont('Arial','B',8);
 $this->Cell(110,6,$reporte,0,8,'L');
 $this->Cell(110,6,$reporte1,0,8,'L');
 $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
 $this->Ln(0);
  for($i=0;$i<count($renglones);$i++)
 {$this->Cell(190,6,$renglones[$i],0,8,'L');}
 $this->SetFont('Arial','B',$altos+3);
 for($i=0;$i<count($header);$i++)
        $this->Cell($anchos[$i],7,$header[$i],1,0,'C');
    $this->Ln();

}
function LoadData($qrystr)

{ global $link;
  global $c_database;
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

//Tabla simple

/// arreglado tema mantiza y ceros y negativos... de pdf_set1
function BasicTable( $header,$data,$tot)
{   global $anchos;
    global $altos;
    global $alig;
    global $total;
    //Datos
    if(!$altos){$altos=7;}
    $totales=array();
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
            {$formatted = $col;
             if ($tot==1) {
             switch ($total[$i]) {
                    case 1:
                          $totales[$i]=$totales[$i]+$col;  $formatted = sprintf("%01.2f", $col);
                    break;
                    case 2:
                          $totales[$i]=$totales[$i]+1;    $formatted = sprintf("%01.2f", $col);
                    break;
                    case 3:
                          $totales[$i]=$col;   $formatted = sprintf("%01.2f", $col);
                    break;
                    case 4:
                          $totales[$i]=$totales[$i]+$col;
                          $col=$totales[$i];
                          $col=round($col, 2);    $formatted = sprintf("%01.2f", $col);
                    break;
                    case 5:
                          $totales[$i]=$totales[$i]+$col;  $formatted = sprintf("%01d", $col);
                    break;

                    default:
                    break;
                                   }
                             }

             $this->Cell($anchos[$i],$altos,$formatted,1,0,$alig[$i]);
             $i=$i+1;
            }
        $this->Ln();

    }
    if ($tot==1)
    { for($i=0;$i<count($header);$i++)
      {  switch ($total[$i]) {
                    case 1:
                         $formatted = sprintf("%01.2f", $totales[$i]);
                   break;
                    case 4:
                          $formatted = sprintf("%01.2f", $col);
                    break;
                    case 5:
                         $formatted = sprintf("%01d", $totales[$i]);
                   break;
                    default:
                         $formatted = $total[$i];
                    break;
                               }
          $this->Cell($anchos[$i],$altos, $formatted,1,0,$alig[$i]);
         }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $altos;
    global $alig;
    global $total;
    //datos
    if(!$altos){$altos=6;}
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],$altos,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $altos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    if(!$altos){$altos=6;}
    $fill=1;
    foreach($data as $row)
     {  $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],$altos,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
     }
     $this->Ln();
     $fill=!$fill;
    }
}

}

?>
