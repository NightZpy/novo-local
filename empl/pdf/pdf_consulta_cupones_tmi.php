<?php
ini_set('memory_limit','50M');

$camp_1=$camp_desde;
$camp_2=$camp_1+1;
$camp_3=$camp_1+2;
$camp_4=$camp_1+3;
$camp_5=$camp_1+4;
$camp_6=$camp_1+5;
$campanias="'$camp_1','$camp_2','$camp_3','$camp_4','$camp_5','$camp_6'";

$campos=array();
$campos[]=array('calculo'=>"CONCAT(usu.apellido,' - ',usu.nombres)",'alias'=>"usuario");
$campos[]=array('calculo'=>"IF(usu.cod_us=usu.id_lider,', LIDER','')",'alias'=>"cat");
$campos[]=array('calculo'=>"usu.localidad",'alias'=>"loc");
$campos[]=array('calculo'=>"usu.provincia",'alias'=>"prov");
$campos[]=array('calculo'=>"usu.mail",'alias'=>"mail");
$campos[]=array('calculo'=>"CONCAT(pd.id_vendedor,' - ',IF(usu.cod_us=usu.id_lider,' - Lider',' - Revendedor/a'))",'alias'=>"id_vendedor");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_1))",'alias'=>"id_p_1");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_1))",'alias'=>"ped_1");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_1))",'alias'=>"envi_1");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_1))",'alias'=>"tot_1");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_1))",'alias'=>"tot_1");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_2))",'alias'=>"id_p_2");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_2))",'alias'=>"ped_2");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_2))",'alias'=>"envi_2");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_2))",'alias'=>"tot_2");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_2))",'alias'=>"tot_2");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_3))",'alias'=>"id_p_3");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_3))",'alias'=>"ped_3");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_3))",'alias'=>"envi_3");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_3))",'alias'=>"tot_3");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_3))",'alias'=>"tot_3");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_4))",'alias'=>"id_p_4");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_4))",'alias'=>"ped_4");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_4))",'alias'=>"envi_4");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_4))",'alias'=>"tot_4");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_4))",'alias'=>"tot_4");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_5))",'alias'=>"id_p_5");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_5))",'alias'=>"ped_5");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_5))",'alias'=>"envi_5");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_5))",'alias'=>"tot_5");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_5))",'alias'=>"tot_5");

$campos[]=array('calculo'=>"MAX(ped.id_pedidos*(cro.campania=$camp_6))",'alias'=>"id_p_6");
$campos[]=array('calculo'=>"SUM(pd.un_total*(cro.campania=$camp_6))",'alias'=>"ped_6");
$campos[]=array('calculo'=>"SUM((pd.un_env+pd.un_acc)*(cro.campania=$camp_6))",'alias'=>"envi_6");
$campos[]=array('calculo'=>"SUM(pd.total*(cro.campania=$camp_6))",'alias'=>"tot_6");
$campos[]=array('calculo'=>"SUM((cro.campania=$camp_6))",'alias'=>"tot_6");


$campos[]=array('calculo'=>"CONCAT(usud.apellido,' - ',usud.nombres)",'alias'=>"distribuidor");
$campos[]=array('calculo'=>"CONCAT(usur.apellido,' - ',usur.nombres)",'alias'=>"regional");
$campos[]=array('calculo'=>"ped.anexo",'alias'=>"anexo");
$campos[]=array('calculo'=>"usu.id_regional NOT IN('BAJWEB1','MEXVAN1','VANCHI1','BARMAR28','EXTCOM1')",'alias'=>"regional_valido");
$campos[]=array('calculo'=>"usu.cod_pais IN(2395,0)",'alias'=>"pais_valido");
$campos[]=array('calculo'=>"((ped.anexo='') AND (usu.id_regional NOT IN('BAJWEB1','MEXVAN1','VANCHI1','BARMAR28','EXTCOM1')) AND (usu.cod_pais IN(2395,0)) )",'alias'=>"incluido");
// $campos[]=array('calculo'=>"cro.campania",'alias'=>"campa");

  if($salida=='PDF')              //PDF
      {
        foreach($campos as $campo_actual)    
          {
          $campo1.=$separador.$campo_actual['calculo']." as ".$campo_actual['alias'];
          $separador=", ";
          $filtro_anexo=" AND ped.anexo='' ";
          $filtro_gerencias=" AND usu.id_regional NOT IN('BAJWEB1','MEXVAN1','VANCHI1','BARMAR28','EXTCOM1')";
          $filtro_pais="AND usu.cod_pais IN(2395,0)";
          }
      
      }
  else                            //XLS
      {
      $arr_tit=array();
      $separador="CONCAT(";
      
              foreach($campos as $campo_actual)    
              {
                $campo1.=$separador."IFNULL(".$campo_actual['calculo'].",'')";
                $separador=",';',";
                
                if($campo_actual['alias']=='tot_1' or $campo_actual['alias']=='tot_2' or $campo_actual['alias']=='tot_3' or $campo_actual['alias']=='tot_4' or $campo_actual['alias']=='tot_5' or $campo_actual['alias']=='tot_6')
                {
                  $campo2.=$separador2.$campo_actual['calculo']." as ".$campo_actual['alias'];
                  $separador2=", ";
                }
                $arr_tit[]=$campo_actual['alias'];
              }
              $campo1.=" ) as c, $campo2"; 
              
        $filtro_anexo="";
        $filtro_gerencias="";
        $filtro_pais="";
      }

$qrystr="SELECT $campo1         
            FROM pedidos_defi AS pd   
              INNER JOIN pedido AS ped ON ped.id_pedidos=pd.id_pedidos AND pd.un_com_s<>0
              INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion AND cro.campania IN($campanias)
              INNER JOIN usuario AS usu ON usu.cod_us=pd.id_vendedor
              INNER JOIN usuario AS usud ON usu.id_distrib=usud.cod_us
              INNER JOIN usuario AS usur ON usu.id_regional=usur.cod_us
              
            WHERE pd.id_vendedor<>'CAMBIO'
              $filtro_anexo
              $filtro_gerencias
              $filtro_pais
            GROUP BY pd.id_vendedor
            HAVING tot_1<>0 AND tot_2<>0 AND tot_3<>0 AND tot_4<>0 AND tot_5<>0 AND tot_6<>0 AND SUM(pd.id_pedidos*(pd.id_vendedor=pd.id_distrib))=0
            ORDER BY usu.cod_us=usu.id_lider DESC,pd.id_vendedor
            
            ";//LIMIT 100
// 
//    echo "$qrystr<br />";
//    exit;
//             
if($salida=='PDF')
      {
      include"pdf_cupones_moto.php";
      }
  else
      {
      include"xls_xupones_moto.php";
      }
?>
