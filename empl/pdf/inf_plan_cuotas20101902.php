<?php
include ("../../conexion.php");
require_once "../../../clases/interfaz/classes_listas.php";
$sesion=crear_clave_sesion();
//echo $ger;die();

include("inc_pdf/inc_function.php");
include("pdf_set6.php");
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$pdf->fuenteTitulos=8;
$reporte="Informe Plan de Cuotas";
$pdf->AddPage();

if(!empty($ger))
    $filtro = "usu.id_regional = '".$ger."'";
else
    $filtro = 1;

//Obtenemos la campa�a vigente
$qrystr = "SELECT id_campania FROM campanias WHERE id_ped_fin = 0 ORDER BY id_campania DESC";
$qry = mysql_db_query($c_database, $qrystr, $link);
$result = mysql_fetch_object($qry);
$campania = $result->id_campania;

$vtos = array('1055' => 'Septiembre','1056' => 'Octubre','1057' => 'Noviembre','1058' => 'Diciembre','1059' => 'Enero','1060' => 'Febrero','1061' => 'Marzo','1062' => 'Abril');

//Buscamos las gerencias que tengan vendedores con planes de cuotas
$qrystr1 = "SELECT cuo.*,usu.id_regional FROM cuotas AS cuo
            INNER JOIN comp_pedidos AS cp ON (cp.id_comp_ped=cuo.id_comp_ped)
            INNER JOIN usuario AS usu ON (usu.cod_us=cuo.vendedor)
            INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
            WHERE (cuo.estado = 'Abierto' OR (cuo.estado = 'Cerrado' AND cro.campania = '".$campania."')) AND $filtro
            GROUP BY cuo.vendedor ORDER BY usu.id_regional,cuo.vendedor";
$qry1 = mysql_db_query($c_database, $qrystr1, $link);

while($result1 = mysql_fetch_object($qry1)){
    $vendedor = $result1->vendedor;
	//Buscamos los planes de cuotas de cada vendedor
    $qrystr2 = "SELECT cuo.*,cro.campania FROM cuotas AS cuo             
                INNER JOIN comp_pedidos AS cp ON (cp.id_comp_ped=cuo.id_comp_ped)
                INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
                INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
                WHERE cuo.vendedor = '".$vendedor."' AND (cuo.estado = 'Abierto' OR (cuo.estado = 'Cerrado' AND cro.campania = '".$campania."'))
                GROUP BY cuo.plan ORDER BY id_comp_ped DESC,plan ASC";
    //echo $qrystr2;
	$qry2 = mysql_db_query($c_database, $qrystr2, $link);
    $planes = "";
    if(mysql_num_rows($qry2) > 0){
        while($result2 = mysql_fetch_object($qry2)){
            //$camp=$result2->campania;
			$planes.= "'".$result2->plan."',";
            $qrystr3 = "SELECT '$sesion',cuo.plan,cuo.id_vd,prod.descr_txt,COUNT(cuo.plan) AS cantcuotas,cuo.cant_cuotas
		        FROM cuotas AS cuo
		        INNER JOIN productos AS prod ON(prod.id_vd=cuo.id_vd)
		        WHERE cuo.plan = '".$result2->plan."' GROUP BY cuo.plan";
	    $qry3 = mysql_db_query($c_database, $qrystr3, $link);
            $result3 = mysql_fetch_object($qry3);
            $cuotasPagas = $result3->cantcuotas.'/'.$result3->cant_cuotas;
            $cuotas = $result3->cant_cuotas;
            
            if($cuotasPagas > 1 ){
                for($x=1;$x<=$cuotasPagas-1;$x++)
                    $cuotasAnteriores.= $x.'/'.$cuotas.' - ';
                    $cuotasAnteriores = substr($cuotasAnteriores,0,-3);
                }else{
                    $cuotasAnteriores = '';
            }

            if(substr($cuotasPagas,0,-2) == $cuotas){
                $pendientesVto = 'Entregado';
            }else{
                $pendientesVto = '';
                for($x=1;$x<=$cuotas-$cuotasPagas;$x++)
                    $pendientesVto.= ($cuotasPagas+$x)."/".$cuotas.' '.$vtos[$campania+$x].'\n';
            }

            $qrystrI = "INSERT INTO reporte (clave_ses,ref1,ref2,ref3,ref4,ref5,ref6)
                        SELECT '$sesion',cuo.plan,cuo.id_vd,prod.nombreproducto,'$cuotasAnteriores','$cuotasPagas','$pendientesVto'
		        FROM cuotas AS cuo
		        INNER JOIN productos AS prod ON(prod.id_vd=cuo.id_vd)
		        WHERE cuo.plan = '".$result3->plan."' GROUP BY cuo.plan";
	    $qryI = mysql_db_query($c_database, $qrystrI, $link);

        } //fin while $result2

        $planes = substr($planes, 0, -1);
        $header=array(utf8_decode('Plan N�'),'Cod Cuota','Detalle','Campa�as Anteriores','Campa�a Actual','Pendientes/Vto'); 
        $anchos=array(20,20,50,35,30,30); //anchos de cada celda procurar que sumen aprox 190-
        $alig=array('C','C','L','C','C','C'); //L,R,C
    
        // aca van los select del load data
        $qrystr = "SELECT concat(ref1,';',ref2,';',ref3,';',ref4,';',ref5,';',ref6) as c
                   FROM reporte
                   WHERE clave_ses='$sesion' AND ref1 IN($planes)
                   ORDER BY ref1 asc";

       $nota = "Planes de Cuotas - Regional: ".$result1->id_regional." - Vendedor: ".$result1->vendedor;
       $data=$pdf->LoadData($qrystr);
       $pdf->header_si=0;
       $pdf->SetFont('Arial','B',9);
       $pdf->Ln(); 
       $pdf->WriteHTML($nota);  
       $pdf->Ln();
       for($i=0;$i<count($header);$i++)
            $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
       $pdf->Ln();
       $pdf->SetFont('Arial','',8);
       $pdf->BasicTable($header,$data,0);
       $pdf->header_si=1; 

    }
}
             
      //$comen = "Para el grupo $grupo - Campa�a N� $campania - Regional $ger";
      /*$pdf->Ln();
      $nota="<br><B>Gener�: <U>$usuario</U></B><br>Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>www.vanesaduran.com</A> | Consultas en <A href='mailto:consultasweb@vanesaduran.com'>consultasweb@vanesaduran.com</A>,<br>";
      //$pdf->WriteHTML($comen);  
      //$pdf->Ln();
      //$pdf->header_si=1;
      //for($i=0;$i<count($header);$i++)
      //   $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
      //$pdf->Ln();
      //$pdf->SetFont('Arial','',7);
      //$pdf->BasicTable($header,$data,1);
      //$pdf->Ln(2);
      //$pdf->SetFont('Arial','',7);
      //$pdf->WriteHTML($nota);
      */
      
      $pdf->Output();
      
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
?>
