<?php
include ("conexion.php");
//********************************************************************

$cd			=$_REQUEST['campdesde'];//Campaña desde
$ch			=$_REQUEST['camphasta'];//Campaña hasta
$cate		=$_REQUEST['categoria'];//Categoria: Distrib. o Líder
$salida	=$_REQUEST['salida'];// Salida del informe 1:XLS, 2:PDF
$sesion	=$_REQUEST['sesion'];

//********************************************************************

if($cate=='d'){
	$campos="prod.id_regional, prod.id_distrib,CONCAT(u.apellido,',',u.nombres,'(',prod.id_distrib,')') AS ApeyNom, IF(prod.nivel_distrib=1,'Plata',IF(prod.nivel_distrib=2,'Oro','Platino')) AS nivel,u.mail AS Mail, CONCAT_WS( '-', u.area_tel, u.telefono ) AS Telefono, CONCAT_WS( '-', u.area_celu, u.celular ) AS Celular, u.provincia AS Provincia, u.localidad AS Localidad,CONCAT(camp.obs,'(',prod.id_campania,')') AS campania,prod.id_campania";
	$join =" ON prod.id_distrib=u.cod_us";
	$join.=" INNER JOIN campanias AS camp ON prod.id_campania=camp.id_campania";
	$where=" AND prod.nivel_distrib>0";
	$orderBy=" ORDER BY ApeyNom,id_campania";

}
else{
	$campos="prod.id_regional, prod.id_distrib,CONCAT(u.apellido,',',u.nombres,'(',prod.id_lider,')') AS ApeyNom, IF(prod.nivel_lider=1,'Plata',IF(prod.nivel_lider=2,'Oro','Platino')) AS nivel,u.mail AS Mail, CONCAT_WS( '-', u.area_tel, u.telefono ) AS Telefono, CONCAT_WS( '-', u.area_celu, u.celular ) AS Celular, u.provincia AS Provincia, u.localidad AS Localidad,CONCAT(camp.obs,'(',prod.id_campania,')') AS campania,prod.id_campania";
	$join=" ON prod.id_lider=u.cod_us";
	$join.=" INNER JOIN campanias AS camp ON prod.id_campania=camp.id_campania";
	$where=" AND u.id_distrib<>prod.id_lider AND prod.nivel_lider>0";
	$orderBy=" ORDER BY ApeyNom,id_campania";
}
$campania=$cd;
$sqlProd="";

while($campania<=$ch){

$sqlProd.="SELECT $campos 
FROM productividad AS prod 
LEFT JOIN usuario AS u $join 
WHERE prod.id_campania=$campania $where";

// Si es la ultima camp, no debo concatenar el UNION
if($campania<$ch){ $sqlProd.=" UNION ";}
//echo $campania."<br>";
$campania++;
}// End while $campania

$sqlProd.=$orderBy;


//echo $sqlProd."<br>";die();

$resInf=mysql_db_query($c_database,$sqlProd,$link);
$numCant=mysql_num_rows($resInf);
if(mysql_error()){ echo mysql_error();}

// Establecer propiedades y config. para la clase
if($numCant>0){//echo realpath($_FILE_);
    require "../../../clases/xls/PHPExcel.php";
    // Instanciar la clase
    $objPHPExcel= new PHPExcel();
 
    // Información para el Excel
    $objPHPExcel->getProperties()
    ->setCreator("www.vanesaduranjoyas.com")
    ->setLastModifiedBy("www.vanesaduranjoyas.com")
    ->setTitle("Listado de Productividad")
    ->setSubject("Informe para Excel");
 	
		// Fuente de los encabezados en negrita
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
 		if($categoria=='d'){ 
 			$ApeyNom="Apellido y Nombre";
 		}
 		else{
 			$ApeyNom="Líder";
 		}
 		
 		// Sheets properties
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1',"Regional")
    ->setCellValue('B1',"Distribuidor")
    ->setCellValue('C1',"$ApeyNom")
    ->setCellValue('D1',"Nivel")
    ->setCellValue('E1',"Campaña")
    ->setCellValue('F1',"Mail")
    ->setCellValue('G1',"Teléfono")
    ->setCellValue('H1',"Celular")
    ->setCellValue('I1',"Provincia")
    ->setCellValue('J1',"Localidad");
   
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="productividad_'.date("Ymd_Hms").'.'.$salida.'"');
header('Cache-Control: max-age=0');
 		
$i=2;// Para empezar en la segunda fila
// Recorrer el array para armar la planilla
while($obj=mysql_fetch_object($resInf)){
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i,utf8_encode($obj->id_regional))
    	->setCellValue('B'.$i,utf8_encode($obj->id_distrib))
    	->setCellValue('C'.$i,utf8_encode($obj->ApeyNom))
    	->setCellValue('D'.$i,$obj->nivel)
    	->setCellValue('E'.$i,$obj->campania)
    	->setCellValue('F'.$i,utf8_encode($obj->Mail))
    	->setCellValue('G'.$i,$obj->Telefono)
    	->setCellValue('H'.$i,$obj->Celular)
    	->setCellValue('I'.$i,$obj->Provincia)
    	->setCellValue('J'.$i,$obj->Localidad);
$i++;    	
}
// Salida
if($salida=='pdf'){
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'PDF');
}
else
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5'); // Excel 97, 2000 xls format
 
$objWriter->save('php://output');
exit;
mysql_close ();
}// End if $numCant>0

?>
