<?php
include ("../../conexion.php");
include("pdf_set6.php");
$sesion=crear_clave_sesion();
$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B> <br> $linkexcel
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

switch($orden)
{
 case 1:
    $ord="ua.cod_us ASC";
 break;
 case 2:
    $ord="ua.cod_us DESC";
 break;
 case 3:
    $ord="dc.vto ASC";
 break;
 case 4:
    $ord="dc.vto DESC";
 break;
 
}


$reporte="INFORME DE CONSTANCIAS VENCIDAS ";//nombre del reporte (en todos)
$reporte1="Constancias vencidas entre el $fecha_d y el $fecha_h";

$notaalpie=""; //nota al pie
// aca van los select del load data

$qrystr=" SELECT CONCAT(ua.cod_us,';',CONCAT(ua.apellido,' ',ua.nombres),';',dc.cuit,';',dc.vto) AS c
          FROM usuario_admin AS ua
	  INNER JOIN usuario AS u ON ua.cod_us = u.cod_us
	  INNER JOIN usuario AS u1 ON u.id_regional = u1.cod_us
	  INNER JOIN documento AS td ON ua.cod_us = td.id_distrib
	  INNER JOIN doc_constancia AS dc ON td.id_doc=dc.id_doc
	  WHERE ua.estado=1 
	  AND td.baja <> 1
	  AND u.cod_us<>u.id_regional
	  AND dc.vto >='$fecha_d' AND dc.vto <='$fecha_h'
	  ORDER BY $ord";
//echo $qrystr;
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');

$header=array('Distribuidor','CUIT','Vencimiento'); //encabezados de columnas (en todos)
$anchos=array(20,70,40,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','L','R'); //L,R,C
$total=array('Total',2,'',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

$data=$pdf->LoadData($qrystr);
$pdf->SetFont('Arial','',7);
$pdf->fuenteTitulos=8;
$pdf->altoFila=4;
$pdf->tipoHeader=1;
$pdf->AddPage('P');
$pdf->SetFont('Arial','',7);
$pdf->BasicTable($header,$data,1);
$pdf->Ln();
$pdf->Cell(($pdf->getAnchoTabla()),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
//$pdf->WriteHTML($nota);
$pdf->Output();
?>
