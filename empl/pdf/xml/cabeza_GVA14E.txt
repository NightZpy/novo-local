<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly' rs:updatable='true'>
		<s:AttributeType name='ADJUNTO' rs:number='1' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ALI_ADI_IB' rs:number='2' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ALI_FIJ_IB' rs:number='3' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='AL_FIJ_IB3' rs:number='4' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ALI_NO_CAT' rs:number='5' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='BMP' rs:number='6' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='C_POSTAL' rs:number='7' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CALLE' rs:number='8' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CALLE2_ENV' rs:number='9' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CAL_DEB_IN' rs:number='10' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CLAUSULA' rs:number='11' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CLAVE_IS' rs:number='12' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CLA_IMP_CL' rs:number='13' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COD_CLIENT' rs:number='14' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='GRUPO_EMPR' rs:number='15' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COD_PROVIN' rs:number='16' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COD_TRANSP' rs:number='17' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COD_VENDED' rs:number='18' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COD_ZONA' rs:number='19' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='COND_VTA' rs:number='20' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CUIT' rs:number='21' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CUMPLEANIO' rs:number='22' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='CUPO_CREDI' rs:number='23' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DESTINO_DE' rs:number='24' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DIAS_MI_IN' rs:number='25' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='3' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DIR_COM' rs:number='26' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='55' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DOMICILIO' rs:number='27' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DTO_ENVIO' rs:number='28' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='DTO_LEGAL' rs:number='29' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='E_MAIL' rs:number='30' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ENV_DOMIC' rs:number='31' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ENV_LOCAL' rs:number='32' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ENV_POSTAL' rs:number='33' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ENV_PROV' rs:number='34' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='EXPORTA' rs:number='35' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='EXP_SALDO' rs:number='36' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_ALTA' rs:number='37' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_ANT' rs:number='38' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_DESD' rs:number='39' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_HAST' rs:number='40' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_INHA' rs:number='41' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_MODI' rs:number='42' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FECHA_VTO' rs:number='43' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='8' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='FILLER' rs:number='44' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='IB_L' rs:number='45' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='IB_L3' rs:number='46' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ID_EXTERNO' rs:number='47' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='17' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ID_INTERNO' rs:number='48' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='17' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='II_D' rs:number='49' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='II_IB3' rs:number='50' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='II_L' rs:number='51' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='IVA_D' rs:number='52' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='IVA_L' rs:number='53' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='LIB' rs:number='54' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='LIMCRE_EN' rs:number='55' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='LOCALIDAD' rs:number='56' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='MON_CTE' rs:number='57' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='MON_MI_IN' rs:number='58' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='N_IMPUESTO' rs:number='59' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='160' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='N_ING_BRUT' rs:number='60' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='20' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='N_PAGOELEC' rs:number='61' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='14' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NOM_COM' rs:number='62' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NRO_ENVIO' rs:number='63' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='9' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NRO_INSCR' rs:number='64' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='12' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NRO_LEGAL' rs:number='65' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='9' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='NRO_LISTA' rs:number='66' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='OBSERVACIO' rs:number='67' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PARTIDOENV' rs:number='68' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PERMITE_IS' rs:number='69' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PISO_ENVIO' rs:number='70' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PISO_LEGAL' rs:number='71' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PORC_DESC' rs:number='72' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='5' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PORC_EXCL' rs:number='73' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PORCE_INT' rs:number='74' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='5' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PORC_L' rs:number='75' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PORC_RECAR' rs:number='76' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='6' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='PUNTAJE' rs:number='77' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='9' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='RAZON_SOCI' rs:number='78' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='60' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='RG_1361' rs:number='79' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SAL_AN_UNI' rs:number='80' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SALDO_ANT' rs:number='81' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SALDO_CC' rs:number='82' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SALDO_CC_U' rs:number='83' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SALDO_DOC' rs:number='84' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SALDO_D_UN' rs:number='85' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='11' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SOBRE_II' rs:number='86' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SOBRE_IVA' rs:number='87' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='SUCUR_ORI' rs:number='88' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='3' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='TELEFONO_1' rs:number='89' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='TELEFONO_2' rs:number='90' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='TIPO' rs:number='91' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='1' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='TIPO_DOC' rs:number='92' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='ZONA_ENVIO' rs:number='93' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='2' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:AttributeType name='Version' rs:number='94' rs:write='true'>
			<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='7' rs:precision='0' rs:fixedlength='true' rs:maybenull='false'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>