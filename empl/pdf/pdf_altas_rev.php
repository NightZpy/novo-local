<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

switch($nivel)
{
 case 'G'://Nivel de regional 
  if($id_regional<>'')  //con un regional elegido, agupar para sus distrib.
    {
     $filtro = " AND u.id_regional = '$id_regional'";
     $join= " u.id_distrib=u1.cod_us ";
     $group= " GROUP BY u.id_distrib ";
     $tit="id_regional: $id_regional";
    }
  else                 //con un regional elegido, agupar para cada regional.   
    {
     $filtro = " ";
     $join= " u.id_regional=u1.cod_us ";
     $group= " GROUP BY u.id_regional ";
     $tit="Todos los Gerentes";
    }
 break;
 case 'D':      
  if($id_distrib<>'')  //con un distrib elegido, agupar para sus l�deres.
    {
     $filtro = " AND u.id_distrib = '$id_distrib'";
     $join= " u.id_lider=u1.cod_us ";
     $group= " GROUP BY u.id_lider ";
     $tit="id_distrib: $id_distrib";
    }
 break; 
 case 'L':            //con un l�der elegido, agupar para sus rev.
  if($id_lider<>'')  
    {
     $filtro = " AND u.id_lider = '$id_lider'";
     $join= " u.cod_us=u1.cod_us ";
     $group= " GROUP BY u.cod_us ";
     $tit="id_lider: $id_lider";
    }
 break;
 case 'R':
   echo "No se pueden visualizar datos a nivel de Revendedor";
   exit;
 break;
} 

$tit .= " A�o $anio";

$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si = 0;
$reporte= "Altas de Revenderores";//nombre del reporte (en todos)
$reporte1="$tit";//nombre del reporte (en todos)
$notaalpie="   "; //nota al pie
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' MKS Argentina');
$pdf->Setcreator('IDDelSur para MKS');
$pdf->AddPage('L');
$usuario=strtoupper($usuario);
$nota="<B>Gener�:  <U>$usuario</U></B><br>Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>www.vanesaduran.com</A> | Consultas en <A href='mailto:sistemas@vanesaduran.com'>sistemas@vanesaduran.com</A>,<br>";

$anchos=array(95,13,13,13,13,13,13,13,13,13,13,13,13,13); //anchos de cada celda procurar que sumen aprox 190-
$pdf->SetFont('Arial','',10);

$header=array('Usuario','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic','TOT'); //encabezados de columnas (en todos)
//$anchos=array(95,140,25); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','R','R','R','R','R','R','R','R','R','R','R','R','R'); //L,R,C
$total=array(2,1,1,1,1,1,1,1,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

$pdf->SetFont('Arial','',10);
$pdf->Cell(100,7,'Lider: '.$row1["nlider"]);
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
for($i=0;$i<count($header);$i++)
   $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',8);

//$anio=2005;
$meses="";
for($a=1;$a<13;$a++)
  $meses .= ",';',IFNULL(sum((MONTH(u.fecha_alta)=$a) AND (YEAR(u.fecha_alta)=$anio)),0)";
$meses .= ",';',IFNULL(sum((YEAR(u.fecha_alta)=$anio)),0)";
$qrystr = " SELECT CONCAT(CONCAT(IFNULL(u1.apellido,''),' ',IFNULL(u1.nombres,''),' (',IFNULL(u1.cod_us,''),')')$meses) AS c
               FROM usuario AS u
               INNER JOIN usuario AS u1 ON $join
               WHERE 1 $filtro
               $group
               ORDER BY CONCAT(u1.apellido,' ',u1.nombres,' (',u1.cod_us,')')
             ";
//echo $qrystr;exit;
$data=$pdf->LoadData($qrystr);
$pdf->SetFont('Arial','',8);
$pdf->header_si = 0;
$pdf->BasicTable($header,$data,1);
$pdf->header_si = 0;
$pdf->Ln(10);

$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();


//SELECT CONCAT(CONCAT(u1.apellido,' ',u1.nombres,' (',u1.cod_us,')'),';',sum(((MONTH(fecha_alta)=1) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=2) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=3) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=4) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=5) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=6) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=7) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=8) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=9) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=10) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=11) AND (YEAR(fecha_alta)=2005)),';',sum(((MONTH(fecha_alta)=12) AND (YEAR(fecha_alta)=2005))) AS c FROM usuario AS u INNER JOIN usuario AS u1 ON u.id_vendedor = u.cod_us WHERE 1 GROUP BY u.id_regional ORDER BY CONCAT(u1.apellido,' ',u1.nombres,' (',u1.cod_us,')')
?>
