<?php
include ("../../conexion.php");
include("pdf_set6.php");
//Recopilacion de Datos
//Pedidos 'normales'
//echo "<br> pedidos  $pedidosdh <br> distribuidor $id_distrib";
$qrystr = " INSERT INTO reporte (clave_ses,ent1,ref1,ref2,val1,val2,val3,ref3)
            SELECT '$sesion',sum(cpd.cantidad),p.id_vd_fact,p1.nombreproducto,IFNULL(SUM(cpd.preciounidad*cpd.cantidad)/sum(cpd.cantidad),''),sum(cpd.cantidad*cpd.preciounidad),sum((cpd.cantidad*cpd.preciounidad)-cpd.boni_id_revendedor-cpd.boni_id_lider-cpd.boni_id_distrib),''
            FROM comp_pedidos_defi AS cpd
            INNER JOIN productos AS p ON cpd.id_vd = p.id_vd
   	        INNER JOIN productos AS p1 ON p.id_vd_fact = p1.id_vd
            WHERE cpd.id_distrib='$id_distrib'
              AND cpd.id_vendedor<>'CAMBIO'
              AND cpd.id_pedido in ($pedidosdh)
              AND cpd.neto<>0
            GROUP BY p.id_vd_fact";//El neto<>0 es para que no muestre los faltantes/agotados en stock.
//              AND neto>0
//echo $qrystr;
$qry = mysql_db_query($c_database,$qrystr,$link);
echo mysql_error();
//Pedidos de 'cambio'
$qrystr = " INSERT INTO reporte (clave_ses,ent1,ref1,ref2,val1,val2,val3,ref3)
            SELECT '$sesion',sum(cpd.cantidad),p.id_vd_fact,p1.nombreproducto,IFNULL(SUM(cpd.preciounidad*cpd.cantidad)/sum(cpd.cantidad),''),sum(cpd.cantidad*cpd.preciounidad),sum((cpd.cantidad*cpd.preciounidad)-cpd.boni_id_revendedor-cpd.boni_id_lider-cpd.boni_id_distrib),'CAMBIO'
            FROM comp_pedidos_defi AS cpd
            INNER JOIN productos AS p ON cpd.id_vd = p.id_vd
	        INNER JOIN productos AS p1 ON p.id_vd_fact = p1.id_vd
	        WHERE cpd.id_distrib='$id_distrib'
              AND cpd.id_vendedor='CAMBIO'
              AND cpd.id_pedido in ($pedidosdh)
              AND cpd.neto<>0
            GROUP BY p.id_vd_fact";//El neto<>0 es para que no muestre los faltantes/agotados en stock.
//              AND neto>0
//echo $qrystr;
$qry = mysql_db_query($c_database,$qrystr,$link);
echo mysql_error();
//instanciando... las variables
$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B>
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:consultasweb@vanesaduran.com'>
consultasweb@vanesaduran.com</A>,<br>";
$qrystr = " SELECT count(*) as sumpedidos,sum(val2) as totvp,sum(val3) as netodist FROM reporte WHERE clave_ses='$sesion' and ref3<>'CAMBIO'";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$header=array('Can.','Cod.','Descripcion','P.U.V.P','Total V.P','Neto Distr'); //encabezados de columnas (en todos)
$anchos=array(10,25,85,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('R','L','L','R','R','R'); //L,R,C
$total=array(1,2,'','',1,1); // texto, 1(suma), 2(cuenta), 3(ultimo reg), 4(saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Despachados Distrib: $id_distrib en los pedidos $pedidosdh.";//nombre del reporte (en todos)
$notaalpie="  informacion confidencial"; //nota al pie
// aca van los select del load data
$qrystr = "SELECT concat(ent1,';',ref1,';',ref2,';',val1,';',val2,';',val3) as c
           FROM reporte
           WHERE clave_ses='$sesion'
                 AND ref3<>'CAMBIO'
           ORDER BY ref1 ASC";
// ---------------- fin variables ---------
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$data=$pdf->LoadData($qrystr);
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$data,1);
$qrystr = "SELECT concat(ent1,';',ref1,';',ref2,';',val1,';',val2,';',val3) as c
           FROM reporte
           WHERE clave_ses='$sesion'
                 AND ref3='CAMBIO'
           ORDER BY ref1 ASC";
//print_r($data);
$data=$pdf->LoadData($qrystr);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(array_sum($anchos),$pdf->altoFila,"Despachados de Cambio",1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
for($i=0;$i<count($header);$i++)
  $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',7);
$pdf->BasicTable($header,$data,1);
$pdf->Ln();
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$qrystrsum = " SELECT concat('',';','Balance',';','',';',sum(val1),';',sum(val2),';',sum(val3)) as c
               FROM reporte
               WHERE clave_ses='$sesion'";
$data=$pdf->LoadData($qrystrsum);
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
$pdf->SetFont('Arial','B',10);
$pdf->Cell(array_sum($anchos),$pdf->altoFila,"Balance",1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
for($i=0;$i<count($header);$i++)
  $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',7);
$pdf->BasicTable($header,$data,0);
$pdf->Ln();
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln(2);
$pdf->WriteHTML($nota);
$pdf->Output();
?>
