<?php
include ("../../conexion.php");
include("pdf_set5.php");
require_once "../../../clases/interfaz/classes_listas.php";


extract($_POST);
extract($_GET);

$sql="SELECT CONCAT(u.id_regional,';',u.id_distrib,';',u.id_lider,';',CONCAT(u.apellido,', ',u.nombres,' (',u.cod_us,')'),';',c.id_vd,';',COUNT(c.vendedor),';',(lp.precio * COUNT(c.vendedor)),';',c.estado) as c
		FROM cuotas_cat_ant as c
		INNER JOIN usuario as u ON u.cod_us=c.vendedor
		INNER JOIN listas_precios_productos as lp ON lp.id_vd=c.id_vd
		INNER JOIN listas_precios as l ON l.id_lista=lp.id_lista
		WHERE c.campania=0
		AND c.estado IN ('Abierto','A Confirmar')
		AND l.predet=1
	GROUP BY c.plan
	ORDER BY c ASC";

switch($salida)
{
 case "PDF":
 		$pdf=new PDF();

		$pdf->Open();
		$pdf->AliasNbPages();

		$header=array("Regional","Distribuidor","Lider","Apellido y Nombre(Vendedor)","id_vd","C. Pagadas","Total","Estado");
		$anchos=array(20,20,20,50,20,15,20,15);
		$alig=array("L","L","L","L","C","R","R","L");
		$total=array("Total",'','','','',1,1,'');
		
		$data=$pdf->LoadData($sql);
		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->SetFont('Arial','',7);

		$pdf->BasicTable($header,$data,1);

		$pdf->Output();
 break;
 case "EXCEL":
     $lista = new Listas();
     $lista->titulos=array("Regional","Distribuidor","Lider","Apellido y Nombre(Vendedor)","id_vd","Cuotas Pagadas","Importe Total","Estado");//encabezados de columnas (en todos)
     $data=$lista->LoadData($sql);
     $lista->BasicTable($data);
     $lista->OutPut($salida);
 
 break;
}
?>
