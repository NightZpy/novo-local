<?php
define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;
var $altoFila;     //La Altura de la fila de datos
var $MaxRengFila;  //El tama�o m�ximo al que puede crecer un rengl�n.


function PDF($orientation='P',$unit='mm',$format='A4',$parAltoFila=5,$parMaxRengFila=0)
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
	$this->setAltoFila($parAltoFila);
	$this->MaxRengFila =  $parMaxRengFila;    //0=Ilimitado.
}

function setAltoFila($parAltoFila)
{
 $this->altoFila = $parAltoFila;
}

function getAltoFila()
{
 return $this->altoFila;
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{   global $anchos;
    global $notaalpie;
	$this->Cell(array_sum($anchos),0,'','T');
    //Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	//Arial italic 8
	$this->SetFont('Arial','I',8);
	//N�mero de p�gina
	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
}

function header()
{global $header;
 global $anchos;
 global $reporte;
 global $reporte1;
 global $renglones;
 $this->Image('vd_srl.jpg',10,8,65);
 //Arial bold 1510
 $this->SetFont('Arial','B',9);
 //Movernos a la derecha
 $this->Cell(80);
 //T�tulo
 $this->Cell(110,$this->altoFila,'Documentaci�n de uso interno',0,8,'L');
 $this->SetFont('Arial','B',8);
 $this->Cell(110,6,$reporte,0,8,'L');
 $this->Cell(110,6,$reporte1,0,8,'L');
 $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
 $this->Ln(0);
  for($i=0;$i<count($renglones);$i++)
   {$this->Cell(190,6,$renglones[$i],0,8,'L');}
 $this->SetFont('Arial','B',10);
 for($i=0;$i<count($header);$i++)
        $this->Cell($anchos[$i],$this->altoFila,$header[$i],1,0,'C');
    $this->Ln();

}

function LoadData($qrystr)

{ global $link;
  global $c_database;
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

//Tabla simple
function BasicTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Datos
    $totales=array();
    foreach($data as $row)
    {   $i=0;
        $paginaoriginal = $this->page;
        $max_renglones = 0;
        $columnas = array();
        foreach($row as $col)
            {if ($tot==1) {
             switch ($total[$i]) {
                    case 1:
                          $totales[$i]=$totales[$i]+$col;
                    break;
                    case 2:
                          $totales[$i]=$totales[$i]+1;
                    break;
                    case 3:
                          $totales[$i]=$col;
                    break;
                    case 4:
                          $totales[$i]=$totales[$i]+$col;
                          $col=$totales[$i];
                    break;
                                   }
                             }
             $yoriginal = $this->y;
             //Parte a una celda en renglones que se almacenan en cada elemento de la matriz columnas.
             array_push($columnas,$this->ParteCell($anchos[$i],$col));

             //Almaceno la cantidad m�xima de renglones en la que se haya desglosado una celda.
             if($max_renglones < sizeof($columnas[$i]))
               $max_renglones = sizeof($columnas[$i]);
             if($max_renglones > $this->MaxRengFila and $this->MaxRengFila > 0 )
               $max_renglones = $this->MaxRengFila;
             $i=$i+1;
            }
        //Ya se construy� la fila, ahora recorro las columnas imprimiendo de a un rengl�n a la vez.

        //Primero me fijo cu�ntos renglones entran en cada p�gina si es que hay un salto de p�gina.
        $renglones_p1 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 1.
        $renglones_p2 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 2 si hay salto de p�gina.
        $yfinal = $this->y;
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          if($yfinal + $this->altoFila < $this->PageBreakTrigger)
            $renglones_p1++;
          else
            {
             $renglones_p2 = $max_renglones - $renglones_p1;
             break;
            }
          $yfinal += $this->altoFila;
         }

        //Ahora imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //S�lo los de la p�gina 1.
        $xact = $this->lMargin;
        for($colu=0;$colu<sizeof($row);$colu++)
         {
          $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$this->y) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p1))* $this->k,'S'));
          $xact += $anchos[$colu];
         }
         
        //Imprimo un registro de la BD en los renglones que se requieran.
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          //Imprimo todas las columnas del rengl�n $reng del registro actual.
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            $rengtemp = $columnas[$colu];// echo "<br>".var_dump($rengtemp);
            $this->MultiCell($anchos[$colu],$this->altoFila,$rengtemp[$reng],0,$alig[$colu]);
           }
          $this->Ln();
         }

        //Imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //Los de la p�gina 2, si hubo salto de p�gina.
        $xact = $this->lMargin;
        if($renglones_p2>0)
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            $ydesde = $this->y - $renglones_p2 * $this->altoFila;//Le resto el alto de los renglones de la primera fila de celdas, ya que ya fueron impresas y se les hizo Ln() en el ciclo de arriba.
            $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$ydesde) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p2))* $this->k,'S'));
            $xact += $anchos[$colu];
           }
    }

    if ($tot==1)
    { for($i=0;$i<count($header);$i++)
      {  switch ($total[$i]) {
                    case 1:
                          $this->Cell($anchos[$i],$this->altoFila,$totales[$i],1,0,$alig[$i]);
                    break;
                    case 2:
                          $this->Cell($anchos[$i],$this->altoFila,$totales[$i],1,0,$alig[$i]);
                    break;
                    case 4:
                          $this->Cell($anchos[$i],$this->altoFila,$col,1,0,$alig[$i]);
                    break;
                    default:
                          $this->Cell($anchos[$i],$this->altoFila,$total[$i],1,0,$alig[$i]);
                    break;
                               }
       }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //datos
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=1;
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
         }
     $this->Ln();
     $fill=!$fill;
    }
}

}

?>
