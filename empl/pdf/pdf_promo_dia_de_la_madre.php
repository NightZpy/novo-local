<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

$reporte = "Reporte: Informe Promo Dia de la Madre";
//Recopilacion de Datos
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si=0;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('');

$pdf->SetFont('Arial','',9);

$nota="<br><B>Genero:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$header=array('Total de pedidos de campania','Pedidos con al menos 1(una) unidad del dia de la madre','Participacion'); //encabezados de columnas (en todos)
$anchos=array(70,80,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','C','C'); //L,R,C
$total=array('','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie

$total_campania=0;
$total_pedido=0;

/*$qrystr2="SELECT cp.cantidad_orig as rtdo
			 FROM `comp_pedidos` as cp
			INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
            WHERE (cro.campania = '".$campania."')";*/
$qrystr2 = "SELECT DISTINCT(cp.id_vendedor) as rtdo
	    FROM `comp_pedidos` as cp
	    INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
            WHERE (cro.campania = '".$campania."') AND cp.id_vendedor != 'CAMBIO'
            GROUP BY cp.id_vendedor";
//echo $qrystr2;
$qry2 = mysql_db_query($c_database,$qrystr2,$link);
/*while($row = mysql_fetch_array($qry2)){
	$total_campania=$total_campania + $row["rtdo"];
}*/
$total_campania = mysql_num_rows($qry2);


/*$qrystr3="SELECT cp.cantidad_orig as rtdo
			 FROM `comp_pedidos` as cp
			INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
             INNER JOIN grupos_lista AS g ON (g.id_grupo='96')
            WHERE (cro.campania = '1051' and g.elemento=cp.id_vd_orig)";*/
$qrystr3 = "SELECT DISTINCT(cp.id_vendedor) as rtdo
	    FROM `comp_pedidos` as cp
	    INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
            INNER JOIN grupos_lista AS g ON (g.id_grupo='".$grupo."')
            WHERE (cro.campania = '".$campania."' AND g.elemento=cp.id_vd_orig) AND cp.id_vendedor != 'CAMBIO'
            GROUP BY cp.id_vendedor";
$qry3 = mysql_db_query($c_database,$qrystr3,$link);
/*while($row = mysql_fetch_array($qry3)){
	$total_pedido=$total_pedido + $row["rtdo"];
}*/
$total_pedido = mysql_num_rows($qry3);

if ($total_pedido>0) {
	$participacion=($total_pedido * 100) / $total_campania;
	$participacion=number_format($participacion,2).'%';
}
else if ($total_pedido==0){
	$participacion = '0%';
}
else if ($total_pedido<0){
	$participacion = '0%';
	$total_pedido=0;
}

$qrystr = "INSERT INTO reporte (clave_ses,ent1,ent2,ref1) VALUES ('".$sesion."',$total_campania,$total_pedido,'".$participacion."')";
$qry = mysql_db_query($c_database,$qrystr,$link);
     
 $qrystr4 = "SELECT CONCAT(ent1,';',ent2,';',ref1) AS c FROM reporte WHERE clave_ses = '".$sesion."'"; 
$data=$pdf->LoadData($qrystr4);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(array_sum($anchos),$pdf->altoFila);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
    $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',7);
 $pdf->BasicTable($header,$data,1);
 $pdf->Ln(5);
 $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
 $pdf->Ln(10);
    
    // ----------- borramos recopilacion de datos ----------
    $qrystr5 = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
    $qry5 = mysql_db_query($c_database,$qrystr5,$link);
    //$qrystr6 = "DELETE FROM  reporte1 WHERE clave_ses='$sesion'";
    //$qry6 = mysql_db_query($c_database,$qrystr6,$link);
    // ----------------------------------------

$pdf->WriteHTML($nota);
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur® para VD');
$pdf->Output();
?>
