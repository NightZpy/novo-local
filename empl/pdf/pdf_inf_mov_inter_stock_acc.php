<?php
/*Informaci�n de los par�mertro de Entrada:
$usuario          -- Si no viene toma 'FERSOTO'
$fecha_d          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$fecha_h          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$incremento       -- [hour|day|month|year] Es el incremento para el Resumen. Por defecto es un d�a (day) por Resumen.
*/
include ("../../conexion.php");
include ("../../config.php");
include("pdf_set_mov_inter_stock.php");
$sesion=crear_clave_sesion();

if($dep_desde!="")//id_deposito: solo se usa para saber que tipo de informe se necesita (de ingreso de stock o de movimiento inter-deposito).
{
	$strConsulta = "SELECT id_deposito,nombre_deposito,nombre_columna
                  FROM  depositos
                  WHERE id_deposito = $dep_desde";
  $qryp = mysql_db_query($c_database,$strConsulta,$link);
  $rowp = mysql_fetch_array($qryp);
}

if($dep_hasta!="")//No se necesita el id.
{
	$strConsulta1 = "SELECT nombre_deposito, nombre_columna
                  FROM  depositos
                  WHERE id_deposito = $dep_hasta";
  $qryd = mysql_db_query($c_database,$strConsulta1,$link);
  $rowd = mysql_fetch_array($qryd);
}

//*********************
//instanciando... las variables
//$qrystr = " SELECT texto FROM advertencia WHERE adv=1003";
//$qry = mysql_db_query($c_database,$qrystr,$link);
//$row = mysql_fetch_array($qry);
//$usuario=strtoupper($usuario);

if($dep_desde!="") $reporte.= "desde el Deposito Origen: $rowp[nombre_deposito]";

if($dep_hasta!="") $reporte.= "Deposito destino: $rowp[nombre_deposito]";

if($fecha_desde!="")
{
	$reporte1= " Fecha desde $fecha_desde";
	$filtro_fecha=" and DATE_FORMAT( fecha_mov, '%Y-%m-%d' ) >='$fecha_desde'";
}

if($fecha_hasta!="")
{
	$reporte1.= " hasta $fecha_hasta";
	$filtro_fecha.=" and DATE_FORMAT( fecha_mov, '%Y-%m-%d' ) <='$fecha_hasta'";
}

// (*1) 

$deposito_desde=strtolower($rowp[nombre_columna]);

if($deposito_desde=='' && $rowp[id_deposito]==19) $tipo_informe='ingreso';
else $tipo_informe='movimiento';

$deposito_hasta=strtolower($rowd[nombre_columna]);

if(($dep_desde!='') && ($dep_hasta!=''))$filtro_desde="AND mov.dep_out='$deposito_desde' AND mov.dep_in='$deposito_hasta' ";
//OR (mov.dep_out='' AND mov.dep_in='$deposito_hasta') OR (mov.dep_out='$deposito_desde' AND mov.dep_in=''))";
if(($dep_desde!='') && ($dep_hasta==''))$filtro_desde="AND mov.dep_out='$deposito_desde'";
if(($dep_desde=='') && ($dep_hasta!=''))$filtro_desde="AND mov.dep_in='$deposito_hasta'";

  $qrystr = "SELECT mov.fecha_mov, mov.usuario, pro.nombreproducto, mov.cantidad, mov.id_vd,
						depout.nombre_deposito as sal, depin.nombre_deposito as ent
						from mov_stock as mov
						inner join productos as pro on mov.id_vd=pro.id_vd
						inner join depositos as depin on depin.nombre_columna=mov.dep_in
						inner join depositos as depout on depout.nombre_columna=mov.dep_out
						Where 1 AND depin.tipo_deposito IN (1) AND depout.tipo_deposito IN (1,50) AND mov.id_vd<>'' $filtro_desde $filtro_hasta $filtro_fecha 
						ORDER BY mov.fecha_mov";

$qry=mysql_db_query($c_database,$qrystr,$link);

//Veo si se encontraron registros...
$num_rows = mysql_num_rows($qry);

//echo 'Numero de filas de la consulta'.$num_rows;

/******************** Verifica si el informe es de ingreso de stock o de movimiento inter-depositos *******************/

	if($num_rows!=0){
		if($tipo_informe=='ingreso'){
	
			//Creo tabla temporal de ingreso de stock
			$sql="CREATE TEMPORARY TABLE tabla_temp (fecha VARCHAR(30),
					 usuario VARCHAR(50),
					 nombre VARCHAR(50),
					 id_vd VARCHAR(20),
					 cantidad INT(11),
					 ent VARCHAR(50))";
		
			$qry_create=mysql_db_query($c_database,$sql,$link);
		
			while($row=mysql_fetch_object($qry))
			{	
				$ins="INSERT INTO tabla_temp (fecha,usuario,nombre,id_vd,cantidad,ent) 
				VALUES ('".$row->fecha_mov."','".$row->usuario."','".$row->nombreproducto."','".$row->id_vd."',".$row->cantidad.",'".$row->ent."')";
				$qry_ins=mysql_db_query($c_database,$ins,$link);
			}
		
			$qrystr="SELECT CONCAT(fecha,';',usuario,';',nombre,';',id_vd,';',cantidad,';',ent) as c FROM tabla_temp";			
		
			$header=array('Fecha ingreso','Usuario','Producto','ID_VD','Cantidad','Dep. ingreso stock'); 
			$anchos=array(33,27,30,30,20,50); //anchos de cada celda procurar que sumen aprox 190-
			$alig=array('L','L','L','L','R','L'); //L,R,C
			$total=array('Total:','','2','',1,''); 
			// texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

		}	else{

			//Creo tabla temporal de movimiento entre depositos
			$sql="CREATE TEMPORARY TABLE tabla_temp (fecha VARCHAR(30),
					 usuario VARCHAR(50),
					 nombre VARCHAR(50),
					 id_vd VARCHAR(20),
					 cantidad INT(11),
					 sal VARCHAR(50),
					 ent VARCHAR(50))";

			$qry_create=mysql_db_query($c_database,$sql,$link);

			while($row=mysql_fetch_object($qry))
			{	
			 		$ins="INSERT INTO tabla_temp (fecha,usuario,nombre,id_vd,cantidad,sal,ent) 
						VALUES ('".$row->fecha_mov."','".$row->usuario."','".$row->nombreproducto."','".$row->id_vd."',".$row->cantidad.",'".$row->sal."','".$row->ent."')";
						$qry_ins=mysql_db_query($c_database,$ins,$link);
			}

			$qrystr="SELECT CONCAT(fecha,';',usuario,';',nombre,';',id_vd,';',cantidad,';',sal,';',ent) as c FROM tabla_temp";

			$header=array('Fecha_Mov','Usuario','Producto','ID_VD','Cantidad','Dep.Origen','Dep.Destino'); 
			$anchos=array(30,20,47,25,15,25,25); //anchos de cada celda procurar que sumen aprox 190-
			$alig=array('L','L','L','L','R','L','L'); //L,R,C
			$total=array('Total:','','2','',1,'',''); 
			// texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot

		}
	}

/***************************************************************************************************************/


$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();         

$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$notaalpie=$cfg['pie_informes_salientes'];

if($tipo_informe=='ingreso')$reporte = "Ingreso de Stock";
else $reporte = "Movimientos Inter-Dep�sitos ";          

$pdf->AddPage('P');	
$pdf->SetFont('Arial','',8);

if($num_rows!=0){ 
	$data=$pdf->LoadData($qrystr);
	$pdf->BasicTable($header,$data,1);
}
else{
	$anchos=array(30,20,47,25,15,25,25); //anchos de cada celda procurar que sumen aprox 190-
	$alig=array('L','L','L','L','R','L','L'); //L,R,C
	$total=array('Total:','','2','',1,'',''); 
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(80);
	//Texto centrado en una celda con cuadro  20*10 mm y salto de l�nea
	if($tipo_informe=='ingreso') $pdf->Cell(20,10,'No se registran ingresos',0,1,'C');
	if($tipo_informe=='movimiento') $pdf->Cell(20,10,'No se registran movimientos',0,1,'C');
}

$pdf->WriteHTML($nota);
$pdf->Output();

//echo $qrystr;
// ---------------- fin variables ---------
//ECHO $qrystr;
//print_r($data);
// ----------- borramos recopilacion de datos ----------
// ----------------------------------------
/* for($i=0;$i<count($header);$i++)
   $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
$pdf->Ln();*/

//$pdf->ImprovedTable($header,$data);
//$pdf->FancyTable($header,$data);
//$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-

//$pdf->Ln();

// $nota="<br>$row[texto]<br>
// <B>Gener�:  <U>$usuario</U></B> <br>
// Generador autom�tico de reportes de <A href='http://www.vanesaduran.com'>
// www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
// soporte@iddelsur.com.ar</A>,<br>";

?>

//(*1)
//$reporte1 = "hasta $rowd[nombre_deposito] (no inclusive)";//nombre del reporte (en todos)

// ---------------- fin variables ---------


 // $pdf->AddPage('P');
//encabezados de columnas (en todos)


 /* $strConsulta = "SELECT *
                  FROM depositos
                  WHERE tipo_deposito=1"; 
  $qryd = mysql_db_query($c_database,$strConsulta,$link); */

 // $pdf->SetFont('Arial','B',8);
 // $pdf->Cell(100,7,'Producto: '.$rowp["id_producto"]."- (".$rowp["id_vd"].") ".$rowp["nombreproducto"]);
  //$pdf->SetFont('Arial','',8);
  /*while($rowd = mysql_fetch_array($qryd))
   {
    $pdf->Cell(100,7,$rowd["nombre_deposito"].': '.$rowp[$rowd["nombre_columna"]]);
    $pdf->Ln();
    $totdep+=$rowp[$rowd["nombre_columna"]];
   }
  $pdf->Cell(100,7,'Total: '.$totdep);
  $pdf->Ln();

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(100,7,"MOVIMIENTOS INTER-�REAS" );
  $pdf->Ln();
  $pdf->SetFont('Arial','',8); */

