<?php
define('FPDF_FONTPATH','font/');
	require_once('font/barcodes.php');
//	require_once('pdf_fpdf.php');
	class pdfbarcode extends TCPDFBarcode {
		var $_pdf;
		var $_char_width;
		var $_bar_width;
		var $_data;
		var $_type;
		
		function pdfbarcode( $data, $char_width, $type) {
			$this->_bar_width = round($char_width/11, 1);
			$this->_char_width = $this->_bar_width * 11;
			$this->_data = $data;
			$this->_type = $type;
			$this->setBarcode($data, $type);
		}
		
		function get_width() {
			return TCPDFBarcode::get_width($this->_char_width);
		}
	
		function set_pdf_document( &$pdf ) {
			$this->_pdf = &$pdf;
		}
		
		function _draw_line( $x, $y, $nb, $height, $bar ) {
			if ($bar) {
				$this->_pdf->SetDrawColor(0, 0, 0);
				$this->_pdf->SetFillColor(0, 0, 0);
			} else {
				$this->_pdf->SetDrawColor( 255, 255, 255);
				$this->_pdf->SetFillColor( 255, 255, 255);
			}
		//	$this->_pdf->SetLineWidth(0.000);
			$this->_pdf->Rect( $x, $y, $nb*$this->_bar_width, $height, 'FD');
		}
	
		function draw_barcode( $x, $y, $bar_height, $print_text = false ) {
			$tdc = $this->_pdf->DrawColor;
			$tfc = $this->_pdf->FillColor;
      $start_x = $x;
			$start_y = $y;
			
			$this->_compute_pattern();
			
			// Quiet Zone
		//	$this->_pdf->SetLineWidth(0.0000);
			$this->_pdf->SetDrawColor(255, 255, 255);
			$this->_pdf->SetFillColor(255, 255, 255);
			$this->_pdf->Rect($x, $y, $this->_bar_width*10, $bar_height, 'FD');
			$x += $this->_bar_width*10;

			foreach($this->_pattern as $pos=>$digit) {
				$digit = split( ' ', $digit);
				$bar = true;
				foreach ($digit as $tmp=>$nb) {
					$this->_draw_line( $x, $y, $nb, $bar_height, $bar );
					$x += ($this->_bar_width*$nb)+0.05;
					$bar = ($bar==true?false:true);
				}
			}

			// Quiet Zone
			//$this->_pdf->SetLineWidth(0.0000);
			$this->_pdf->SetDrawColor(255, 255, 255);
			$this->_pdf->SetFillColor(255, 255, 255);
			$this->_pdf->Rect($x, $y, $this->_bar_width*10, $bar_height, 'FD');
			$x += $this->_bar_width*10;

			if ($print_text) {
				$font_size = floor($this->get_width() / strlen($this->_data)*1.5);
				$this->_pdf->SetFont('arial', '', $font_size);
        $this->_pdf->SetXY( $start_x, $start_y + $bar_height );
				$this->_pdf->Cell( $x - $start_x, $font_size/2, $this->_data, 0, 0, 'C' );
			}
			$this->_pdf->DrawColor = $tdc;
			$this->_pdf->FillColor = $tfc;
		}
  } // End of Class
?>

