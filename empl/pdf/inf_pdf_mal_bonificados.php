<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

//Buscamos los distribuidores del regional elegido
$qrystr1 = "SELECT cod_us,CONCAT(apellido,', ',nombres,' (',cod_us,')') AS nombre FROM usuario WHERE id_regional = '".$regional."' AND cod_us = id_distrib";
$qry1 = mysql_db_query($c_database,$qrystr1,$link);

while($row1 = mysql_fetch_array($qry1)){
    $distribuidor = $row1[cod_us];
    $nombre_distribuidor = $row1[nombre];

    //buscamos la cantidad de unidades pedidas (campo 'cantidad') que es como se estaba haciendo incorrectamente
    $qrystr2 = "SELECT SUM(cp.cantidad) AS cantidad FROM comp_pedidos AS cp 
                INNER JOIN pedido AS ped ON(ped.id_pedidos=cp.id_pedido)
                INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
                WHERE cro.campania = $campania AND cp.id_vendedor != 'CAMBIO' AND cp.comentario != 'PROMO'
                AND cp.id_distrib = '".$distribuidor."' AND LEFT(cp.id_vd,1) IN('S','V','O','P','L')";
    $qry2 = mysql_db_query($c_database,$qrystr2,$link);
    if(mysql_num_rows($qry2) > 0){
        $row2 = mysql_fetch_array($qry2);
        $cantidad = $row2['cantidad'];
        if($cantidad < 600)
            $bonifico = 5;
        elseif($cantidad >= 600 && $cantidad < 1500)
            $bonifico = 10;
        elseif($cantidad >= 1500)
            $bonifico = 12;
    }

    //Ahora buscamos la cantidad de unidades pedidas (campo 'cantidad_orig') que es como deberia calcularse
    $qrystr3 = "SELECT SUM(cp.cantidad_orig) AS cantidad_orig FROM comp_pedidos AS cp 
                INNER JOIN pedido AS ped ON(ped.id_pedidos=cp.id_pedido)
                INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
                WHERE cro.campania = $campania AND cp.id_vendedor != 'CAMBIO' AND cp.comentario != 'PROMO'
                AND cp.id_distrib = '".$distribuidor."' AND LEFT(cp.id_vd,1) IN('S','V','O','P','L')";
    $qry3 = mysql_db_query($c_database,$qrystr3,$link);
    if(mysql_num_rows($qry3) > 0){
        $row3 = mysql_fetch_array($qry3);
        $cantidad_orig = $row3['cantidad_orig'];
        if($cantidad_orig < 600)
            $debio_bonificar = 5;
        elseif($cantidad_orig >= 600 && $cantidad_orig < 1500)
            $debio_bonificar = 10;
        elseif($cantidad_orig >= 1500)
            $debio_bonificar = 12;
    }

    $obs = "";
    if($bonifico != $debio_bonificar)
        $obs = "Mal Bonificado";

    //Insertamos en la tabla reporte los datos temporales
    $qrystr5 = "INSERT INTO reporte (clave_ses,ref1,ref2,ref3,ref4) VALUES ('".$sesion."','".$nombre_distribuidor."','".$bonifico." %','".$debio_bonificar." %','".$obs."')";
    $qry5 = mysql_db_query($c_database,$qrystr5,$link);
    
}

$qrystr4 = "SELECT obs FROM campanias WHERE id_campania = $campania";
$qry4 = mysql_db_query($c_database,$qrystr4,$link); 
$row4 = mysql_fetch_array($qry4);

$reporte = "Reporte: Informe Mal Bonificados";
//Recopilacion de Datos
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si=0;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('');

$pdf->SetFont('Arial','',9);

$nota="<br><B>Generó:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$header=array('Distribuidor','Bonifico','Debio Bonificar','Observaciones'); //encabezados de columnas (en todos)
$anchos=array(60,25,25,50); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','C','C','C'); //L,R,C
$total=array('','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie

 $qrystr6 = "SELECT CONCAT(ref1,';',ref2,';',ref3,';',ref4) AS c FROM reporte WHERE clave_ses = '".$sesion."' ORDER BY ref1 ASC";

 $pdf->SetFont('Arial','',7); 
 $msj = "Mes: ".$row4['obs']." - Regional: ".$regional;
 $pdf->WriteHTML($msj);
 $data=$pdf->LoadData($qrystr6);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(array_sum($anchos),$pdf->altoFila);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
    $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',7);
 $pdf->BasicTable($header,$data,1);
 $pdf->Ln(5);
 $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
 $pdf->Ln(10);
    
    // ----------- borramos recopilacion de datos ----------
    $qrystr7 = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
    $qry7 = mysql_db_query($c_database,$qrystr7,$link);
    // ----------------------------------------

$pdf->WriteHTML($nota);
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur® para VD');
$pdf->Output();
?>
