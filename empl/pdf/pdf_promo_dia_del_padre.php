<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

$reporte = "Reporte: Informe Promo Dia del Padre";
//Recopilacion de Datos
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si=0;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('');

$pdf->SetFont('Arial','',9);

$nota="<br><B>Generó:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$header=array('Regional','Distribuidor','Lider','Vendedor','Puntaje','Cred.'); //encabezados de columnas (en todos)
$anchos=array(40,40,40,40,15,15); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','L','L','C','C'); //L,R,C
$total=array('','','','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie

 $qrystr = "SELECT *, cppl.usuario AS user, (SUM(cppl.puntos*(-1))) AS puntaje FROM crit_promos_puntos_log AS cppl
             INNER JOIN pedido AS ped ON (ped.id_pedidos=cppl.id_pedido)
             INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
             INNER JOIN usuario AS usu ON (usu.cod_us=cppl.usuario)
             WHERE (cppl.id_promo = '61' AND cro.campania = '1051')
             GROUP BY cppl.usuario";
 $qry = mysql_db_query($c_database,$qrystr,$link);

 while($row = mysql_fetch_array($qry)){
     $vendedor = "-";
     if($row[user] != $row[id_regional] && $row[user] != $row[id_distrib] && $row[user] != $row[id_lider])
         $vendedor = $row[user];

     //Calculamos los creditos
     $qrystr1 = "SELECT SUM(cp.preciounidad) AS credito FROM comp_pedidos AS cp
                 INNER JOIN pedido AS ped ON (ped.id_pedidos=cp.id_pedido)
                 INNER JOIN cronograma AS cro ON (cro.id_presentacion=ped.id_presentacion)
                 WHERE (cro.campania = '1052' AND cp.id_vd = 'JCR0001' AND cp.id_vendedor = '".$row[user]."')";
     $qry1 = mysql_db_query($c_database,$qrystr1,$link);
     $row1 = mysql_fetch_array($qry1);

     $qrystr2 = "INSERT INTO reporte (clave_ses,ref1,ref2,ref3,ref4,ent1,ref5) VALUES ('".$sesion."','".$row[id_regional]."','".$row[id_distrib]."','".$row[id_lider]."','".$vendedor."',$row[puntaje],'".$row1[credito]."')";
     $qry2 = mysql_db_query($c_database,$qrystr2,$link);
     //echo $row[id_regional].' - '.$row[id_distrib].' - '.$row[id_lider].' - '.$vendedor.' - '.$row[puntaje].' - '.$row1[credito].'<br>';
 } 

 $qrystr4 = "SELECT CONCAT(ref1,';',ref2,';',ref3,';',ref4,';',ent1,';',ref5) AS c FROM reporte WHERE clave_ses = '".$sesion."'";
 
 $data=$pdf->LoadData($qrystr4);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(array_sum($anchos),$pdf->altoFila);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
    $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',7);
 $pdf->BasicTable($header,$data,1);
 $pdf->Ln(5);
 $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
 $pdf->Ln(10);
    
    // ----------- borramos recopilacion de datos ----------
    $qrystr3 = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
    $qry3 = mysql_db_query($c_database,$qrystr3,$link);
    // ----------------------------------------

$pdf->WriteHTML($nota);
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur® para VD');
$pdf->Output();
?>
