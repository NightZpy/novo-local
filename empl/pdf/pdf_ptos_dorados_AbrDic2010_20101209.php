<?php

/**
 * @author Faian
 * @copyright 2009
 */

include ("../../conexion.php");
$sesion=crear_clave_sesion();


$qrystr="SELECT CONCAT(ppd.distrib,';',ppd.pertenencia,';',CONCAT(camp.id_campania,' - ',camp.obs),';',
          ppd.pvp_c28,';',ppd.pvp_c29,';',ppd.pvp_c30,';',ppd.pvp_c31,';',ppd.pvp_c32,';',ppd.pvp_c33,';',ppd.pvp_c34,';',ppd.pvp_c35,';',
          ppd.pun_c28,';',ppd.pun_c29,';',ppd.pun_c30,';',ppd.pun_c31,';',ppd.pun_c32,';',ppd.pun_c33,';',ppd.pun_c34,';',ppd.pun_c35,';',ppd.puntaje) AS c
         FROM promo_puntos_dorados AS ppd
         INNER JOIN campanias AS camp ON ppd.camp_pert=camp.id_campania
         ORDER BY ppd.distrib,camp.id_campania
        ";
      //echo $qrystr;
switch($salida)
{
 case 'PDF':
      include("inc_pdf/inc_function.php");
      include("pdf_set15.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $header=array('Distrib','Per','Campa�a','Pvp-C4','Pvp-C5','Pvp-C6','Pvp-C7','Pvp-C8','Pvp-C9','Pvp-C10','Pvp-C11','Punt-C4','Punt-C5','Punt-C6','Punt-C7','Punt-C8','Punt-C9','Punt-C10','Punt-C11','Puntaje'); //encabezados de columnas (en todos)
      $anchos=array(20,5,30,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('C','C','L','R','R','R','R','R','R','R','R','R','R','R','R','R','R','R','R','C'); //L,R,C
      $comment="TOTALES";
      $total=array($comment,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $reporte="PDF de Puntos Dorados ABR 2010 - DIC 2010";//nombre del reporte (en todos)
      $pdf->AddPage('L');
      $data=$pdf->LoadData($qrystr);
      $pdf->SetFont('Arial','',5);
      $pdf->BasicTable($header,$data,1);
      $pdf->WriteHTML($nota);
      $pdf->Output();
 break;
     
 case 'EXCEL':
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=array('Distrib','Per','Campa�a','Pvp-C4','Pvp-C5','Pvp-C6','Pvp-C7','Pvp-C8','Pvp-C9','Pvp-C10','Pvp-C11','Punt-C4','Punt-C5','Punt-C6','Punt-C7','Punt-C8','Punt-C9','Punt-C10','Punt-C11','Puntaje'); //encabezados de columnas (en todos)
      $data=$lista->LoadData($qrystr);
      $lista->BasicTable($data);
      $lista->OutPut("EXCEL");
 break;
}

?>