<?php

/**
 * @author Faian
 * @copyright 2009
 */

include ("../../conexion.php");
$sesion=crear_clave_sesion();


$qrystr="SELECT CONCAT(ppd.distrib,';',ppd.pertenencia,';',CONCAT(camp.id_campania,' - ',camp.obs),';',
          ppd.pvp_c22,';',ppd.pvp_c23,';',ppd.pvp_c24,';',ppd.pvp_c25,';',ppd.pvp_c26,';',ppd.pvp_c27,';',
          ppd.pun_c22,';',ppd.pun_c23,';',ppd.pun_c24,';',ppd.pun_c25,';',ppd.pun_c26,';',ppd.pun_c27,';',ppd.puntaje) AS c
         FROM promo_puntos_dorados AS ppd
         INNER JOIN campanias AS camp ON ppd.camp_pert=camp.id_campania
         ORDER BY ppd.distrib,camp.id_campania
        ";
      //echo $qrystr;
switch($salida)
{
 case 'PDF':
      include("inc_pdf/inc_function.php");
      include("pdf_set6.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $header=array('Distrib','Per','Campa�a','Pvp-C10','Pvp-C11','Pvp-C12','Pvp-C1','Pvp-C2','Pvp-C3','Punt-C10','Punt-C11','Punt-C12','Punt-C1','Punt-C2','Punt-C3','Puntaje'); //encabezados de columnas (en todos)
      $anchos=array(20,5,30,15,15,15,15,15,15,15,15,15,15,15,15,15); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('C','C','L','R','R','R','R','R','R','R','R','R','R','R','R','C'); //L,R,C
      $comment="TOTALES";
      $total=array($comment,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $reporte="PDF de Puntos Dorados OCT 2009 - ABRIL-2010";//nombre del reporte (en todos)
      $pdf->AddPage('L');
      $data=$pdf->LoadData($qrystr);
      $pdf->SetFont('Arial','',8);
      $pdf->BasicTable($header,$data,1);
      $pdf->WriteHTML($nota);
      $pdf->Output();
 break;
     
 case 'EXCEL':
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=array('Distrib','Per','Campa�a','Pvp-C10','Pvp-C11','Pvp-C12','Pvp-C1','Pvp-C2','Pvp-C3','Punt-C10','Punt-C11','Punt-C12','Punt-C1','Punt-C2','Punt-C3','Puntaje'); //encabezados de columnas (en todos)
      $data=$lista->LoadData($qrystr);
      $lista->BasicTable($data);
      $lista->OutPut("EXCEL");
 break;
}

?>