<?php
include ("../../conexion.php");
include("pdf_set5.php");
$sesion=crear_clave_sesion();

$filtrarRegional = "";
if($regional != "")
    $filtrarRegional = "AND usu.id_regional = '".$regional."'";

//Buscamos los lideres q presentaron pedido en la campaña
$qrystr1 = "SELECT cp.id_lider AS lider,cp.id_distrib AS distribuidor,usu.id_regional AS regional FROM comp_pedidos AS cp
            INNER JOIN pedido AS ped ON(ped.id_pedidos=cp.id_pedido)
            INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
            INNER JOIN usuario AS usu ON(usu.cod_us=cp.id_lider)
            WHERE(cro.campania = $campania AND LEFT(cp.id_vd,1) IN ('S','V','O','P')        
            AND cp.comentario != 'PROMO'
            AND ped.anexo=''
            AND cp.id_vendedor != 'CAMBIO' ".$filtrarRegional.")
            GROUP BY cp.id_lider";
$qry1 = mysql_db_query($c_database,$qrystr1,$link);

while($row1 = mysql_fetch_array($qry1)){
    $lider = $row1[lider];
    $distribuidor = $row1[distribuidor];
    $regional = $row1[regional];
    //buscamos los pedidos del lider
    $qrystr2 = "SELECT ped.id_pedidos AS pedido FROM pedido AS ped
                INNER JOIN cronograma AS cro ON(cro.id_presentacion=ped.id_presentacion)
                WHERE(cro.campania = $campania AND ped.id_lider = '".$lider."')";
    $qry2 = mysql_db_query($c_database,$qrystr2,$link);

    $rango1[$lider] = 0;$rango2[$lider] = 0;$rango3[$lider] = 0;$rango4[$lider] = 0;
    while($row2 = mysql_fetch_array($qry2)){
        $pedido = $row2[pedido];
        //Buscamos el PVP promedio de cada pedido
        $qrystr3 = "SELECT SUM((cp.cantidad_orig)*(cp.preciounidad))/SUM(cp.cantidad_orig) AS promedio,cp.id_lider FROM comp_pedidos AS cp
                    WHERE(cp.id_pedido = $pedido AND cp.id_vendedor != 'CAMBIO' AND cp.comentario != 'PROMO' AND LEFT(cp.id_vd,1) IN ('S','V','O','P','L'))
                    GROUP BY cp.id_vendedor";
        $qry3 = mysql_db_query($c_database,$qrystr3,$link);
        while($row3 = mysql_fetch_array($qry3)){

            $lider = $row3[id_lider];
            $promedio = floor($row3[promedio]);

            if($promedio >= 46 && $promedio <= 50)
                $rango1[$lider]++;
            if($promedio >= 51 && $promedio <= 55)
                $rango2[$lider]++;
            if($promedio >= 56 && $promedio <= 60)
                $rango3[$lider]++;
            if($promedio >= 61)
                $rango4[$lider]++;
        }
    }
    
    if($rango1[$lider] > 0 || $rango2[$lider] > 0 || $rango3[$lider] > 0 || $rango4[$lider] > 0){
        
        /*echo "Regional: ".$row4[regional]." - Distrib.: ".$row4[distribuidor]." - Lider: ".$lider." - R1: ".$rango1[$lider]." - R2: ".$rango2[$lider]." - R3: ".$rango3[$lider]." - R4: ".$rango4[$lider]."<br><br>";*/
        $qrystr5 = "INSERT INTO reporte (clave_ses,ref1,ref2,ref3,ent1,ent2,ent3,ent4) VALUES ('".$sesion."','".$regional."','".$distribuidor."','".$lider."','".$rango1[$lider]."','".$rango2[$lider]."','".$rango3[$lider]."','".$rango4[$lider]."')";
        $qry5 = mysql_db_query($c_database,$qrystr5,$link);
    }
}


$reporte = "Reporte: Informe PVPromedio";
//Recopilacion de Datos
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->header_si=0;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('');

$pdf->SetFont('Arial','',9);

$nota="<br><B>Generó:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$header=array('Regional','Distribuidor','Lider','JRE0111R01','JRE0111R02','JRE0111R03','JRE0111R04'); //encabezados de columnas (en todos)
$anchos=array(35,35,35,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','C','C','C','C','C','C'); //L,R,C
$total=array('Total','','',1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie

 $qrystr6 = "SELECT CONCAT(ref1,';',ref2,';',ref3,';',ent1,';',ent2,';',ent3,';',ent4) AS c FROM reporte WHERE clave_ses = '".$sesion."' ORDER BY ref1,ref2,ref3 ASC";

 $pdf->SetFont('Arial','',7); 
 $msj = "Este informe muestra la cantidad de vendedores que hay por rangos para cada lider.";
 $pdf->WriteHTML($msj);
 $data=$pdf->LoadData($qrystr6);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(array_sum($anchos),$pdf->altoFila);
 $pdf->Ln();
 $pdf->SetFont('Arial','B',8);
 for($i=0;$i<count($header);$i++)
    $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
 $pdf->Ln();
 $pdf->SetFont('Arial','',7);
 $pdf->BasicTable($header,$data,1);
 $pdf->Ln(5);
 $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
 $pdf->Ln(10);
    
    // ----------- borramos recopilacion de datos ----------
    $qrystr7 = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
    $qry7 = mysql_db_query($c_database,$qrystr7,$link);
    // ----------------------------------------

$pdf->WriteHTML($nota);
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur® para VD');
$pdf->Output();
?>
