<?php
include ("../../conexion.php");
$sesion=crear_clave_sesion();

$camp_1=$camp_desde;
$camp_m1=$camp_1-1;
$camp_m2=$camp_1-2;
$camp_2=$camp_1+1;
$camp_3=$camp_1+2;
$camp_4=$camp_1+3;
$camp_5=$camp_1+4;
$camp_6=$camp_1+5;
$camp_7=$camp_1+6;
if($can_camp==3)
  $campanias="'$camp_m2','$camp_m1','$camp_1','$camp_2','$camp_3'";
elseif($can_camp==6)
  $campanias="'$camp_m2','$camp_m1','$camp_1','$camp_2','$camp_3','$camp_4','$camp_5','$camp_6'";
else
  $campanias="'$camp_m2','$camp_m1','$camp_1','$camp_2','$camp_3','$camp_4','$camp_5','$camp_6','$camp_7'";

if($restringir==1)
{
  $restric_reg = "(usu.id_regional NOT IN('BAJWEB1','MEXVAN1','VANCHI1','BARMAR28','EXTCOM1'))";
  $restric_pais="(usu.cod_pais IN(2395,0))";
  $restric_nivel = "(usu.cod_us<>usu.id_distrib)"; 
}
else
{
  $restric_reg = "1";
  $restric_pais="1";
  $restric_nivel = "1"; 
}
//sesion,vendedor,campania,nombre,unidades_pedidas,unidades_enviadas,pvp_joy,pvp_acc,regional_valido,pais_valido,nivel_valido            
$qrystr1="INSERT INTO reporte1(
                clave_ses,ref1,ent1,ref2,
                ent3,ent4,val1,val2,
                ent5,ent6,ent7
                )                      
         SELECT '$sesion',pd.id_vendedor,cro.campania,CONCAT(usu.apellido,' - ',usu.nombres), 
                SUM(pd.un_com_s), SUM(pd.un_env_s), SUM(pd.pvp_comp), SUM(pd.pvp_ncomp),
                $restric_reg,$restric_pais,$restric_nivel 
         FROM pedidos_defi AS pd 
         INNER JOIN pedido AS ped ON ped.id_pedidos=pd.id_pedidos  
         INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion AND cro.campania IN($campanias) 
         INNER JOIN usuario AS usu ON usu.cod_us=pd.id_vendedor 
         WHERE pd.id_vendedor<>'CAMBIO'
           AND ped.anexo='' AND un_com_s<>0  
         GROUP BY pd.id_vendedor,cro.campania 
         ";
//  echo "$qrystr1<br />";
//  exit;

$qry = mysql_db_query($c_database,$qrystr1,$link);
verificar('',$qrystr1);

//C�lculo de vendedores por campa�a (campa�a, vendedores) 
//vendedores incluidos
//Excluidos por gerencia no v�lida//Excluidos por pa�s no v�lido//Excluidos por nivel no v�lido
//PVP joyas v�lido por campa�a//PVP acc v�lido por campa�a
//un_ped v�lido por campa�a//un_env v�lido por campa�a
$qrystr1="INSERT INTO reporte(
                clave_ses,ent1,ent2,
                ent8,
                ent3,ent4,ent5,
                val1,val2,
                ent6,ent7
                )                      
         SELECT '$sesion',ent1,COUNT(*),
         SUM(ent5<>0 AND ent6<>0 AND ent7<>0),
         SUM(ent5=0),SUM(ent6=0),SUM(ent7=0),
         SUM(val1 * (ent5<>0 AND ent6<>0 AND ent7<>0)),SUM(val2 * (ent5<>0 AND ent6<>0 AND ent7<>0)),
         SUM(ent3 * (ent5<>0 AND ent6<>0 AND ent7<>0)),SUM(ent4 * (ent5<>0 AND ent6<>0 AND ent7<>0))
         FROM reporte1 
         WHERE clave_ses='$sesion'
         GROUP BY ent1 
         ";
// echo "$qrystr1<br />";
// exit;
$qry = mysql_db_query($c_database,$qrystr1,$link);
verificar('',$qrystr1);

if($can_camp==3)
  $lim_sup=$camp_3;
elseif($can_camp==6)
  $lim_sup=$camp_6;
else
  $lim_sup=$camp_7;
if($restringir==1)
  $fil = "AND (ent5<>0 AND ent6<>0 AND ent7<>0)";
else  
  $fil = "";

//C�lculo de vendedores con continuidad de 1 campa�a
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_cont1="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_cont1 = mysql_db_query($c_database,$qrystr_cont1,$link);
  verificar('',$qrystr_cont1);
//   $row_cont1 = mysql_fetch_array($qry_cont1);
  $canti1=mysql_num_rows($qry_cont1);

  $qrystr_u_cont1="UPDATE reporte SET ent9='$canti1'                      
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_cont1 = mysql_db_query($c_database,$qrystr_u_cont1,$link);
  verificar('',$qrystr_u_cont1);
}


//C�lculo de vendedores con continuidad de 2 campa�as
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_cont2="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_ant,
             SUM(ent3*(ent1=$campa-2)) as ped_campa_ant_ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1 or ent1=$campa-2)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_ant<>0 AND ped_campa_ant_ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_cont2 = mysql_db_query($c_database,$qrystr_cont2,$link);
  verificar('',$qrystr_cont2);
//   $row_cont2 = mysql_fetch_array($qry_cont2);
  $canti2=mysql_num_rows($qry_cont2);

  $qrystr_u_cont2="UPDATE reporte SET ent10='$canti2'                     
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_cont2 = mysql_db_query($c_database,$qrystr_u_cont2,$link);
  verificar('',$qrystr_u_cont2);
}

//C�lculo de vendedores con continuidad de 3 campa�as
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_cont2="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_1ant,
             SUM(ent3*(ent1=$campa-2)) as ped_campa_2ant,
             SUM(ent3*(ent1=$campa-3)) as ped_campa_3ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1 or ent1=$campa-2 or ent1=$campa-3)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_1ant<>0 AND ped_campa_2ant<>0 AND ped_campa_3ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_cont2 = mysql_db_query($c_database,$qrystr_cont2,$link);
  verificar('',$qrystr_cont2);
//   $row_cont2 = mysql_fetch_array($qry_cont2);
  $canti2=mysql_num_rows($qry_cont2);

  $qrystr_u_cont2="UPDATE reporte SET ent11='$canti2'                     
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_cont2 = mysql_db_query($c_database,$qrystr_u_cont2,$link);
  verificar('',$qrystr_u_cont2);
}

//C�lculo de vendedores con continuidad de 4 campa�as
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_cont2="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_1ant,
             SUM(ent3*(ent1=$campa-2)) as ped_campa_2ant,
             SUM(ent3*(ent1=$campa-3)) as ped_campa_3ant,
             SUM(ent3*(ent1=$campa-4)) as ped_campa_4ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1 or ent1=$campa-2 or ent1=$campa-3 or ent1=$campa-4)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_1ant<>0 AND ped_campa_2ant<>0 AND ped_campa_3ant<>0 AND ped_campa_4ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_cont2 = mysql_db_query($c_database,$qrystr_cont2,$link);
  verificar('',$qrystr_cont2);
//   $row_cont2 = mysql_fetch_array($qry_cont2);
  $canti2=mysql_num_rows($qry_cont2);

  $qrystr_u_cont2="UPDATE reporte SET ent12='$canti2'                     
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_cont2 = mysql_db_query($c_database,$qrystr_u_cont2,$link);
  verificar('',$qrystr_u_cont2);
}

//C�lculo de vendedores con continuidad de 5 campa�as
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_cont2="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_1ant,
             SUM(ent3*(ent1=$campa-2)) as ped_campa_2ant,
             SUM(ent3*(ent1=$campa-3)) as ped_campa_3ant,
             SUM(ent3*(ent1=$campa-4)) as ped_campa_4ant,
             SUM(ent3*(ent1=$campa-5)) as ped_campa_5ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1 or ent1=$campa-2 or ent1=$campa-3 or ent1=$campa-4 or ent1=$campa-5)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_1ant<>0 AND ped_campa_2ant<>0 AND ped_campa_3ant<>0 AND ped_campa_4ant<>0 AND ped_campa_5ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_cont2 = mysql_db_query($c_database,$qrystr_cont2,$link);
  verificar('',$qrystr_cont2);
//   $row_cont2 = mysql_fetch_array($qry_cont2);
  $canti2=mysql_num_rows($qry_cont2);

  $qrystr_u_cont2="UPDATE reporte SET ent13='$canti2'                     
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_cont2 = mysql_db_query($c_database,$qrystr_u_cont2,$link);
  verificar('',$qrystr_u_cont2);
}

//Irregulares por campa�a
for($campa=$camp_1;$campa<=$lim_sup;$campa++)
{
  //Buscar los que tengan pedidos en la campa�a de la iteraci�n Y en la anterior.
  $qrystr_i="                      
           SELECT ref1, 
             SUM(ent3*(ent1=$campa)) as ped_campa,
             SUM(ent3*(ent1=$campa-1)) as ped_campa_ant,
             SUM(ent3*(ent1=$campa-2)) as ped_campa_ant_ant
           FROM reporte1 
           WHERE clave_ses='$sesion'
             AND (ent1=$campa or ent1=$campa-1 or ent1=$campa-2)
             $fil
           GROUP BY ref1 
           HAVING ped_campa<>0 and ped_campa_ant=0 AND ped_campa_ant_ant<>0
           ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_i = mysql_db_query($c_database,$qrystr_i,$link);
  verificar('',$qrystr_i);
//   $row_cont2 = mysql_fetch_array($qry_cont2);
  $cantii=mysql_num_rows($qry_i);

  $qrystr_u_i="UPDATE reporte SET ref5='$cantii'                     
                 WHERE clave_ses='$sesion'
                 AND ent1=$campa 
                 ";
  // echo "$qrystr1<br />";
  // exit;
  $qry_u_i= mysql_db_query($c_database,$qrystr_u_i,$link);
  verificar('',$qrystr_u_i);
}


//CONSULTA QUE MUESTRA LOS DATOS
$qrystr="SELECT CONCAT(CONCAT(r.ent1,'-',c.obs),';',r.ent2,';',
                r.ent8,';',
                r.ent3,';',r.ent4,';',r.ent5,';',
                r.val1,';',r.val2,';',
                r.ent6,';',r.ent7,';',
                r.ent9,';',r.ent10,';',r.ent11,';',r.ent12,';',r.ent13,';',
                r.ref5) as c
         FROM reporte as r
           INNER join  campanias as c ON r.ent1=c.id_campania
         WHERE r.clave_ses = '$sesion'
         ORDER BY r.ent1";

$arr_tit = array('Campa�a','Vendedores','Incluidos','ExcXger','ExcXpa�s','ExcXnivel','PVPjoyV�l','PVPaccV�l','Un_pedV�l','Un_envV�l','Cont1','Cont2','Cont3','Cont4','Cont5','Irreg');
if($salida=='PDF')
      {
      include("pdf_set6.php");
      //Recopilacion de Datos
      //Iniciando PDF
      $pdf=new PDF($orientation='P',$unit='mm',$format='legal');//PDF('L');
      $pdf->Open();
      $pdf->AliasNbPages();
      //instanciando... las variables

      $nota="<br>$row[texto]<br>
      <B>Gener�:  <U>$usuario</U></B> <br>
      Generador autom�tico de reportes de <A href='http://www.vanesaduran.com'>www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>soporte@iddelsur.com.ar</A>,<br><br>
      Campa�a: Campa�a<br>
      Vendedores: Cantidad de vendedores en la campa�a<br>
      Incluidos: Vendedores que cumplieron con los requisitos de la promoci�n calificados seg�n la campa�a del rengl�n<br>
      ExcXger: Vendedores Excluidos por estar en gerencias que no participaron de la promo <br>
      ExcXpa�s: Vendedores Excluidos por estar en pa�ses que no participaron de la promo<br>
      ExcXnivel: Vendedores Excluidos por tener nivel de distribuidor o regional<br>
      PVPjoyV�l: PVP enviado de joyas de los vendedores que entraron en la promo en el rengl�n actual<br>
      PVPaccV�l: PVP enviado de accesorios de los vendedores que entraron en la promo en el rengl�n actual<br>
      Un_pedV�l: Unidades Pedidas de joyas de los vendedores que entraron en la promo en el rengl�n actual<br>
      Un_envV�l: Unidades Enviadas de joyas de los vendedores que entraron en la promo en el rengl�n actual<br>
      Cont1: Vendedores que tienen continuidad en la campa�a del rengl�n visualizado y en la anterior tambi�n<br>
      Cont2: Vendedores que tienen continuidad en la campa�a del rengl�n visualizado y en las 2 anteriores tambi�n<br>
      Irreg: Vendedores que tienen pedido en la campa�a del rengl�n visualizado y en la anterior no tienen pero en la 2� anterior s�<br>
      Se excluyeron pedidos de cambio ing/egr y pedidos especiales";


      $header=$arr_tit; //encabezados de columnas (en todos)
      $anchos=array(40,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('L','R','R','R','R','R','R','R','R','R','R','R','R','R','R','R'); //L,R,C
      $total=array('','','','','','','','','','','','','','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      if($restringir<>1)
        $pdf->camposHidden=array('Incluidos','ExcXger','ExcXpa�s','ExcXnivel');
      $reporte="Reporte: An�lisis TMI";//nombre del reporte (en todos)
      $notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
      // aca van los select del load data

      // ---------------- fin variables ---------
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $data=$pdf->LoadData($qrystr);
      //print_r($data);
      // ----------- borramos recopilacion de datos ----------
      
      // ----------------------------------------
      
      $pdf->SetFont('Arial','',10);
      $pdf->AddPage('L');
      $pdf->SetFont('Arial','',9);
      $pdf->BasicTable($header,$data,0);
      //$pdf->ImprovedTable($header,$data);
      //$pdf->FancyTable($header,$data);
      $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
      $pdf->Ln();
      $pdf->WriteHTML($nota);
      $pdf->Output();
      }
  else
      {
//       include ("../../conexion.php");
//       include("inc_pdf/inc_function.php");
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=$arr_tit;
      $data=$lista->LoadData($qrystr);
      $lista->BasicTable($data);
      
      $lista->OutPut("EXCEL");//
      }

// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte1 WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------


?>
