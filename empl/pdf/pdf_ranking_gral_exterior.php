<?php
include ("../../conexion.php");
require_once "../../../clases/interfaz/classes_listas.php";
$sesion=crear_clave_sesion();

$niv_atribs = "";

$tit_atribs = array();
$alin_atribs = array();
$tot_atribs = array();
$tot_anchos = array();

function getCantPedidos($campania, $vendedor, $nivel)
{
    global $c_database;
    global $link;

    if($nivel == "id_regional"){
        $inner = "INNER JOIN usuario AS usu ON usu.cod_us=cp.id_vendedor";
        $join = "usu.id_regional = '".$vendedor."'";
        $groupBy = "usu.id_regional";
    }else{
        $inner = "";
        $join = "cp.".$nivel." = '".$vendedor."'";
        $groupBy = "cp.".$nivel;
    }

    $qrystr = "SELECT COUNT(DISTINCT(cp.id_vendedor)) AS pedidos FROM comp_pedidos AS cp
               INNER JOIN pedido AS ped ON ped.id_pedidos=cp.id_pedido
               INNER JOIN cronograma AS cro ON cro.id_presentacion=ped.id_presentacion $inner
               WHERE cro.campania = $campania AND cp.id_vendedor != 'CAMBIO' AND $join
               GROUP BY $groupBy";
    $qry = mysql_db_query($c_database,$qrystr,$link);
    if(mysql_num_rows($qry) > 0){
        $result = mysql_fetch_object($qry);
        return $result->pedidos;
    }

    return 0;
}
$campanias = $c_mayor - $c_menor;
//echo $campanias;
if ($campanias < 25){
$qrystr_camp = "SELECT * FROM campanias WHERE id_campania >= '$c_menor' AND id_campania <= '$c_mayor'";
$qrycamp=mysql_db_query($c_database,$qrystr_camp,$link);
verificar($qrystr_camp,'');

while($rowcamp=mysql_fetch_array($qrycamp)){
    $arr_atribs[] = array("id_camp"=>"$rowcamp[id_campania]","nombre"=>"$rowcamp[obs]");
}
    
$i=2;$y=2;
$separador=",';',";
$separa=",";
foreach ($arr_atribs as $atrib){
    $i2 = $i+1;
    $niv_atribs .=$separa." SUM((cp.cantidad_orig)*(cro.campania='$atrib[id_camp]')) AS ucamp$i,SUM((cp.cantidad_orig*cp.preciounidad)*(cro.campania='$atrib[id_camp]')) AS pvpcamp$i";
    $ent .=$separa."ent$i".$separa."val$y".$separa."ent$i2";
    $ent_muestra.=$separador."ent$i".$separador."val$y".$separador."ent$i2";
    $tit_atribs[] = 'Und('.$atrib['id_camp'].')';
    $tit_atribs[] = 'PVP('.$atrib['id_camp'].')';
    $tit_atribs[] = 'Ped.('.$atrib['id_camp'].')';
    $alin_atribs[] = 'R';
    $alin_atribs[] = 'R';
    $alin_atribs[] = 'R';
    $tot_atribs[] = 1;
    $tot_atribs[] = 1;
    $tot_atribs[] = 1;
    $tot_anchos[] = 18;
    $tot_anchos[] = 18;
    $tot_anchos[] = 18;
    $arr_in[] = $atrib['id_camp'];
    $i = $i + 2;
    $y++;
}
$niv_atribs = substr($niv_atribs,1);
$ent = substr($ent,1);
$ent_muestra = substr($ent_muestra,5);

if($como_mostrar == 'pedido')
  $tabla_prin="cp";
else
  $tabla_prin="usu";


if($ger<>'Todas'){
    $filtro_ger = " AND usu.id_regional='$ger'";
    $reporte.="Regional: ".$ger;
}else{
    $reporte.="Todas las Regionales";
} 
  
switch($cate)
{
    case '2': // GERENCIA //
        $ins_rep = "ref1";
        $sel_rep = "usu.id_regional AS ref1";
        if($como_mostrar=='pedido')
            $inner = "INNER JOIN usuario AS usu ON cp.id_distrib=usu.cod_us";
        else
            $inner = "INNER JOIN usuario AS usu ON cp.id_vendedor=usu.cod_us";
        $filtro_adicional = " AND usu.id_regional != 'DURVAN01' AND $tabla_prin.id_distrib != 'DURVAN01'";
        $group = "GROUP BY usu.id_regional";
        $join_rep = "ref1";
        $join_usu = "id_regional";
        $campos_hidden = array('DISTRIBUIDOR','LIDER','REVENDEDOR');
        break;

    case '3': // DISTRIBUIDOR //
        $ins_rep = "ref1,ref2";
        $sel_rep = "usu.id_regional AS ref1,$tabla_prin.id_distrib AS ref2";
        if($como_mostrar=='pedido')
            $inner="INNER JOIN usuario AS usu ON cp.id_distrib=usu.cod_us ";
        else
            $inner="INNER JOIN usuario AS usu ON cp.id_vendedor=usu.cod_us";
        $filtro_adicional = " AND ($tabla_prin.id_distrib  != 'DURVAN01') AND (usu.id_regional != $tabla_prin.id_distrib)";
        $group = "GROUP BY $tabla_prin.id_distrib";
        $join_rep = "ref2";
        $join_usu = "id_distrib";
        $campos_hidden=array('LIDER','REVENDEDOR');
        break;

    case '4': // LIDER //
        $ins_rep = "ref1,ref2,ref3";
        $sel_rep = "usu.id_regional AS ref1,$tabla_prin.id_distrib AS ref2,$tabla_prin.id_lider AS ref3";
        if($como_mostrar=='pedido')
            $inner="INNER JOIN usuario AS usu ON cp.id_lider=usu.cod_us";
        else
            $inner="INNER JOIN usuario AS usu ON cp.id_vendedor=usu.cod_us";
        $filtro_adicional = " AND $tabla_prin.id_distrib != $tabla_prin.id_lider";
        $group = "GROUP BY $tabla_prin.id_lider";
        $filtro_nivel = "AND usu.cod_us != $tabla_prin.id_distrib";
        $join_rep = "ref3";
        $join_usu = "id_lider";
        $campos_hidden=array('REVENDEDOR');
        break;

    case '5': // REVENDEDOR //
        $ins_rep = "ref1,ref2,ref3,ref4"; 
        $sel_rep = "usu.id_regional AS ref1,$tabla_prin.id_distrib AS ref2,$tabla_prin.id_lider AS ref3,cp.id_vendedor AS ref4";
        $inner = "INNER JOIN usuario AS usu ON cp.id_vendedor=usu.cod_us";
        $filtro_adicional = " AND cp.id_vendedor != $tabla_prin.id_distrib";
        $group = "GROUP BY cp.id_vendedor";
        $join_rep = "ref4";
        $join_usu = "id_vendedor";
        $campos_hidden = array('');
        break;
}//FIN SWITCH

if($rank==1)
    $orden=" ORDER BY ent1 DESC, val1 DESC";
else
    $orden=" ORDER BY val1 DESC, ent1 DESC";

if($limitar<>'')
    $limitar_a=" LIMIT 0,$limitar";
else
    $limitar_a="";

if($c_menor == $c_mayor)
    $filtro_campania = "cro.campania = $c_mayor";
else
    $filtro_campania = "cro.campania >= '$c_menor' AND cro.campania <= '$c_mayor'";


//inserta en reporte
$qrystr = "SELECT $sel_rep,$niv_atribs,SUM(cp.cantidad_orig) AS utotal,SUM(cp.cantidad_orig*cp.preciounidad) AS pvp_total
           FROM comp_pedidos AS cp
           INNER JOIN pedido AS ped ON cp.id_pedido = ped.id_pedidos
           INNER JOIN cronograma AS cro ON ped.id_presentacion=cro.id_presentacion 
           INNER JOIN atributos AS atr ON atr.id_atrib = cp.id_atrib
           $inner
           WHERE usu.id_regional NOT IN ('MEXVAN1','VANCHI1','BARMAR28','EXTCOM1')
           AND cp.id_vendedor != 'CAMBIO' $filtro_ger
           $filtro_adicional 
           AND ped.anexo=''
           AND ped.estado NOT IN(1,2,7,23)
           AND atr.computable = 1
           AND cp.cantidad_orig > 0
           AND $filtro_campania $filtro_nivel
	   $filtrostr 
           $group";
//echo "<br>$qrystr";
//die();
$qry = mysql_db_query($c_database,$qrystr,$link);

verificar('',$qrystr);

$coma = ",";
while($result = mysql_fetch_object($qry)){
    
    $sel_rep2 = "";
    $representantes = explode(',', $ins_rep);
    foreach($representantes as $repres){
        $sel_rep2 .= $coma."'".$result->{$repres}."'";
    }
    $sel_rep2 = substr($sel_rep2, 1);    

    $i = 2;$zero = 0;$entradas = "";
    foreach($arr_atribs as $atrib){
        $ucamp = 'ucamp'.$i;
        $pvpcamp = 'pvpcamp'.$i;
        $entradas .= $coma.$result->{$ucamp}.$coma.$result->{$pvpcamp}.$coma.$zero;
        $i = $i+2;
    }
    $entradas = substr($entradas, 1);

    $qrystr_ins = "INSERT INTO reporte (clave_ses,$ins_rep,$ent,ent1,val1,ent51) VALUES 
                   ('$sesion',$sel_rep2,$entradas,$result->utotal,$result->pvp_total,0)";
  //  echo "<br>$qrystr_ins";
//    die();
    $qry_ins = mysql_db_query($c_database,$qrystr_ins,$link);
    verificar('',$qrystr_ins);

    $x = 3;$totalPedidos = 0;
    foreach ($arr_atribs as $atrib){
        $cant_pedidos = getCantPedidos($atrib['id_camp'], $result->{$join_rep}, $join_usu);
        $qrystrU = "UPDATE reporte SET ent$x = $cant_pedidos WHERE clave_ses = '$sesion' AND $join_rep = '".$result->{$join_rep}."'";
        $totalPedidos = $totalPedidos + $cant_pedidos;
        $qryU = mysql_db_query($c_database,$qrystrU,$link);
        $x = $x+2;
    }
    //Actualizamos el total de pedidos
    $qrystrUT = "UPDATE reporte SET ent51 = $totalPedidos WHERE clave_ses = '$sesion' AND $join_rep = '".$result->{$join_rep}."'";
    $qryUT = mysql_db_query($c_database,$qrystrUT,$link);
}


//consulta que muestra
$qrystr1 = " SELECT concat(r.ref1,';',r.ref2,';',r.ref3,';',r.ref4,';',CONCAT(usu.apellido,' , ',usu.nombres),';',IFNULL(usu.localidad,'-'),';',IFNULL(usu.provincia,'-'),';',IFNULL(usu.fecha_alta,'-'),';',$ent_muestra,';',r.ent1,';',r.val1,';',
             ent51) as c
             FROM reporte AS r
             INNER JOIN usuario usu ON usu.cod_us=r.$join_rep
             WHERE r.clave_ses='$sesion'
             GROUP BY r.$join_rep 
             $orden
             $limitar_a";

//echo "hasta aqui -> revisar reporte <br> $qrystr1";
//die(); 

             
switch($salida)
{             
 case 'PDF':
      include("inc_pdf/inc_function.php");
      include("pdf_set6.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $pdf->fuenteTitulos=8;
      $reporte="Informe Convencion - Ranking de Ventas";
      $comen="Entre la Campaña Nº $c_menor y la Campaña Nº $c_mayor";
            $pdf->Ln();
      $nota="<br> ****  Todas las categorias son con al menos un producto con precio comprendido en los rangos especificados. Se exculyen cambios **** <br>
             <br><B>Genero: <U>$usuario</U></B><br>Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>www.vanesaduran.com</A> | Consultas en <A href='mailto:consultasweb@vanesaduran.com'>consultasweb@vanesaduran.com</A>,<br>";
      $header = array_merge(array('REGIONAL','DISTRIBUIDOR','LIDER','REVENDEDOR','NOMBRE','Localidad','Provincia', 'Fecha Alta'),$tit_atribs,
                                array('U-TOT','PVP','Ped.'));
      $anchos=array_merge(array(25,25,25,25,25,25,25,65),$tot_anchos,array(18,18,16));
      $alig=array_merge(array('L','L','L','L','L','L','L','L'),$alin_atribs,array('R','R','R'));
      $total=array_merge(array('Total',2,'',''),$tot_atribs,array(1,1,1));
      $pdf->camposHidden = $campos_hidden;
      $pdf->SetFont('Arial','',7);
      $pdf->AddPage('L');
      //echo"$qrystr1";
      $data=$pdf->LoadData($qrystr1);
      $pdf->BasicTable($header,$data,1);
      $pdf->Ln(2);
      $pdf->SetFont('Arial','',7);
      $pdf->WriteHTML($nota);
      $pdf->Output();
      break;

 case 'EXCEL':
 	$lista = new Listas();
		//cambio Ingresos
		$lista->titulos=array_merge(array('REGIONAL','DISTRIBUIDOR','LIDER','REVENDEDOR','NOMBRE','Localidad','Provincia', 'Fecha Alta'),$tit_atribs,
    array('U-TOT','PVP','Ped.')); //encabezados de columnas (en todos)
		$data=$lista->LoadData($qrystr1);
		$lista->Ln(1);
		$lista->BasicTable($data);
		$lista->Ln(1);
		//cambio aceptados

   $lista->OutPut('EXCEL');
   break;
}
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
}
else{
    Echo "<table width='624' border='1' cellspacing='2' cellpadding='0' align='center'>
			<tr>
				<td>
					<div align='center'>
						<font color='#1d2aa4'>Estimado $usuario este Informe est&aacute; Limitado a 24 Campa&ntilde;as. Por Favor cierre esta ventana y seleccione otro Rango de Campa&ntilde;as</font></div>
				</td>
			</tr>
		</table>";}
    
?>
