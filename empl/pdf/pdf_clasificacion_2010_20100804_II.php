<?php
include ("../../conexion.php");
include("pdf_set5.php");
require_once "../../../clases/interfaz/classes_listas.php";

extract($_POST);

if (isset($categoria))
{
	switch($categoria)
	{
		case "regional":
		{
			$tit="Clasificación Regionales";
			$sel="ur.id_regional,ur.id_distrib,ur.id_lider,CONCAT('(',ur.cod_us,') ',ur.apellido,', ',ur.nombres)";
			$inn=" INNER JOIN usuario as ur ON ur.cod_us=u.id_regional ";
			$fil="";
			$grp=" GROUP BY u.id_regional ";
		}
		break;
		case "RegDistrib":
		{
			$tit="Clasificación Regionales como Distribuidor";
			$sel="ud.id_regional,ud.id_distrib,ud.id_lider,CONCAT('(',ud.cod_us,') ',ud.apellido,', ',ud.nombres)";
			$inn=" INNER JOIN usuario as ud ON ud.cod_us=u.id_distrib ";
			$fil=" AND ud.cod_us=ud.id_regional ";
			$grp=" GROUP BY u.id_regional ";
		}
		break;
		case "distribuidor":
		{
			$tit="Clasificación Distribuidores";
			$sel="ud.id_regional,ud.id_distrib,ud.id_lider,CONCAT('(',ud.cod_us,') ',ud.apellido,', ',ud.nombres)";
			$inn=" INNER JOIN usuario as ud ON ud.cod_us=u.id_distrib ";
			$fil=" AND ud.cod_us<>ud.id_regional ";
			$grp=" GROUP BY u.id_distrib ";
		}
		break;
		case "lider":
		{
			$tit="Clasificación Líderes";
			$sel="ul.id_regional,ul.id_distrib,ul.id_lider,CONCAT('(',ul.cod_us,') ',ul.apellido,', ',ul.nombres)";
			$inn=" INNER JOIN usuario as ul ON ul.cod_us=u.id_lider ";
			$fil=" AND ul.id_lider<>ul.id_distrib ";
			$grp=" GROUP BY u.id_lider ";
		}
		break;
		case "vendedor":
		{
			$tit="Clasificación Vendedores";
			$sel="u.id_regional,u.id_distrib,u.id_lider,CONCAT('(',u.cod_us,') ',u.apellido,', ',u.nombres)";
			$fil=" AND u.cod_us<>u.id_lider ";
			$grp=" GROUP BY cp.id_vendedor ";
		}
		break;
		default:
		break;
	}
}

if (isset($orden))
{
	switch($orden)
	{
		case "pvp":
			$ord=" ORDER BY pvp DESC";
		break;
		case "unid":
			$ord=" ORDER BY unid DESC";
		break;
		default:
		break;
	}
}

$temp="CREATE TEMPORARY TABLE temp (ref1 VARCHAR(100) not null,ref2 VARCHAR(100) not null,ref3 VARCHAR(100) not null,
									cod_us VARCHAR(100) not null,unid1 INT(11) not null,ped1 INT(11) not null,pvp1 INT(11) not null,
                                    unid2 INT(11) not null,ped2 INT(11) not null,pvp2 INT(11) not null,
									unid3 INT(11) not null,ped3 INT(11) not null,pvp3 INT(11) not null,
									unid4 INT(11) not null,ped4 INT(11) not null,pvp4 INT(11) not null,
									unid5 INT(11) not null,ped5 INT(11) not null,pvp5 INT(11) not null,
									unid6 INT(11) not null,ped6 INT(11) not null,pvp6 INT(11) not null)";
//echo $temp;
$qtemp=mysql_db_query($c_database,$temp,$link);

$sql="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid1,ped1,pvp1)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1063
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql;
$qry=mysql_db_query($c_database,$sql,$link);

$sql1="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid2,ped2,pvp2)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1064
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql1;
$qry1=mysql_db_query($c_database,$sql1,$link);

$sql2="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid3,ped3,pvp3)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1065
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql2;
$qry2=mysql_db_query($c_database,$sql2,$link);

$sql3="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid4,ped4,pvp4)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1066
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql;
$qry3=mysql_db_query($c_database,$sql3,$link);

$sql4="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid5,ped5,pvp5)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1067
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql;
$qry4=mysql_db_query($c_database,$sql4,$link);

$sql5="INSERT INTO temp (ref1,ref2,ref3,cod_us,unid6,ped6,pvp6)
	  SELECT ".$sel.",SUM(cp.cantidad_orig) as unid,if(p.anexo='',COUNT(DISTINCT(cp.id_vendedor)),0),ROUND(SUM(cp.preciounidad*cp.cantidad_orig)) * ( cp.id_atrib <>20 ) as pvp
	  FROM comp_pedidos as cp
	  INNER JOIN usuario as u ON u.cod_us=cp.id_vendedor
	  ".$inn."
	  INNER JOIN pedido as p ON p.id_pedidos=cp.id_pedido
	  INNER JOIN cronograma as cro ON cro.id_presentacion=p.id_presentacion
	  WHERE cp.id_atrib IN (1,6,7,10,12,14,16,17,20,21)
	  ".$fil."
	  AND cp.id_vendedor<>'CAMBIO'
	  AND cro.campania=1068
	  ".$grp."
	  ".$ord."
	  LIMIT 0,500";
//echo $sql;
$qry5=mysql_db_query($c_database,$sql5,$link);
	  

$qrystr="SELECT CONCAT(ref1,';',ref2,';',ref3,';',cod_us,';',SUM(unid1),';',SUM(ped1),';',SUM(pvp1),';',SUM(unid2),';',SUM(ped2),';',SUM(pvp2),';',SUM(unid3),';',SUM(ped3),';',SUM(pvp3),';',SUM(unid4),';',SUM(ped4),';',SUM(pvp4),';',SUM(unid5),';',SUM(ped5),';',SUM(pvp5),';',SUM(unid6),';',SUM(ped6),';',SUM(pvp6),';',SUM(unid1)+SUM(unid2)+SUM(unid3)+SUM(unid4)+SUM(unid5)+SUM(unid6),';',SUM(ped1)+SUM(ped2)+SUM(ped3)+SUM(ped4)+SUM(ped5)+SUM(ped6),';',SUM(pvp1)+SUM(pvp2)+SUM(pvp3)+SUM(pvp4)+SUM(pvp5)+SUM(pvp6)) as c,SUM(pvp1)+SUM(pvp2)+SUM(pvp3)+SUM(pvp4)+SUM(pvp5)+SUM(pvp6) as pvp,SUM(unid1)+SUM(unid2)+SUM(unid3)+SUM(unid4)+SUM(unid5)+SUM(unid6) as unid
		FROM temp
		GROUP BY cod_us
		".$ord."
		";

switch($salida)
{
 case "PDF":
{
$pdf=new PDF();

$pdf->Open();
$pdf->AliasNbPages();

$pdf->AddPage('l');
   
$header=array('','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Acumulado'); //encabezados de columnas (en todos)
$anchos=array(100,24,24,24,24,24,24,24); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','C','C','C','C','C','C','C'); //L,R,C

$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,8,$tit,0,0,'C');
$pdf->Ln();

$pdf->SetFont('Arial','B',5);

for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
$pdf->Ln();
   
//$pdf->header_si=1;

$header=array("Reg","Distrib","Lid","COD_US","UNID.","PED.","PVP","UNID.","PED.","PVP","UNID.","PED.","PVP","UNID.","PED.","PVP","UNID.","PED.","PVP","UNID.","PED.","PVP","UNID.","PED.","PVP");
$anchos=array(25,25,25,25,7,7,10,7,7,10,7,7,10,7,7,10,7,7,10,7,7,10,7,7,10);
$alig=array('L','L','L','L','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C');
$total=array("Total",1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);


for($i=0;$i<count($header);$i++)
       $pdf->Cell($anchos[$i],$pdf->altoFila,$header[$i],1,0,'C');
//$pdf->AddPage();
$pdf->Ln();

$data=$pdf->LoadData($qrystr);
$pdf->BasicTable($header,$data,1);

$pdf->Output();
}
break;
 case "EXCEL":
 {
     $lista = new Listas();
     $lista->titulos=array("COD_US","UNIDADES MAYO","PEDIDOS MAYO","PVP MAYO","UNIDADES JUNIO","PEDIDOS JUNIO","PVP JUNIO","UNIDADES JULIO","PEDIDOS JULIO","PVP JULIO","UNIDADES AGOSTO","PEDIDOS AGOSTO","PVP AGOSTO","UNIDADES SEPTIEMBRE","PEDIDOS SEPTIEMBRE","PVP SEPTIEMBRE","UNIDADES OCTUBRE","PEDIDOS OCTUBRE","PVP OCTUBRE","UNIDADES ACUM","PEDIDOS ACUM","PVP ACUM");//encabezados de columnas (en todos)
     $data=$lista->LoadData($qrystr);
     $lista->BasicTable($data);
     $lista->OutPut($salida);
 }
 break;
}
?>