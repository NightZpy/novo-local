<?php
/*Informaci�n de los par�mertro de Entrada:
$usuario          -- Si no viene toma 'FERSOTO'
$fecha_d          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$fecha_h          -- Fecha con el formato yyyy-mm-dd . Si no viene toma la Predetermianda del Usuario.
$incremento       -- [hour|day|month|year] Es el incremento para el Resumen. Por defecto es un d�a (day) por Resumen.
*/
include ("../../conexion.php");
include("pdf_set.php");
$sesion=crear_clave_sesion();

if (!isset($usuario))
  $usuario="NICOLASR";
if (!isset($sesion))
  $sesion="SwFsWW";

$qrystrFecha = " SELECT *
                 FROM fecha_manejo
                 WHERE usuario = '$usuario' ";

//echo $qrystrFecha;
$qryFecha = mysql_db_query($c_database,$qrystrFecha,$link);
$rowFecha = mysql_fetch_array($qryFecha);

if(!isset($fecha_d))
  $fecha_d = $rowFecha["fecha_desde"];
if(!isset($fecha_h))
  $fecha_h = $rowFecha["fecha_hasta"];

if(!isset($incremento))
  $incremento = "day";

$fecha_temp = $fecha_d;
$fecha_temp1 = date("Y-m-d", strtotime ("$fecha_temp +1 $incremento"));
if (strtotime($fecha_temp1) > strtotime($fecha_h))
  $fecha_temp1 =$fecha_h;

//*********************
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
//instanciando... las variables

$usuario=strtoupper($usuario);
$header=array('Iniciales','Nombre','Pedidos','Unidades'); //encabezados de columnas (en todos)
$anchos=array(20,80,35,35); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('C','L','R','R'); //L,R,C
$total=array('Total:',2,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('Vanesa Duran S.A.');
$reporte="Reporte: Informe de Recolectores/Verificadores desde $fecha_d hasta $fecha_h";//nombre del reporte (en todos)
// ---------------- fin variables ---------
$pdf->SetTitle($reporte);

//Imprime el Resumen Total.
$pdf->SetFont('Arial','',9);
ImprimirSubInforme($fecha_d,$fecha_h,$reporte);

//Imprime el/los Detalles Uno a Uno.
while(strtotime($fecha_temp) < strtotime($fecha_h))
 {
  ImprimirSubInforme($fecha_temp,$fecha_temp1,"Reporte: Detalle Grupo");
  $fecha_temp = $fecha_temp1 ;
  $fecha_temp1 = date("Y-m-d", strtotime ("$fecha_temp +1 $incremento"));
  if (strtotime($fecha_temp1) > strtotime($fecha_h))
    $fecha_temp1 =$fecha_h;

 }

$pdf->Output();

//********************************************************
function ImprimirSubInforme($fecha_d,$fecha_h,$encab)
 {
  global $sesion;
  global $c_database;
  global $qrystr;
  global $link;
  global $pdf;
  global $header;
  global $anchos;
  global $alig;
  global $total;
  global $reporte;

	mysql_select_db($c_database,$link);

// Aca van los SELECT *****************************************************************************

//Recopilacion de Datos Recolectores Pedidos y Unidades
$qryReco ="INSERT INTO reporte(clave_ses,ref1,ref2,ent1,ent2)
				SELECT '$sesion',IFNULL(a1.ini,'SD'),ar.nyape,COUNT(*),SUM(a1.cantidad)
							FROM armados AS a1
								LEFT JOIN armadoras AS ar ON a1.ini = ar.ini
              WHERE  fecha >= '$fecha_d' AND fecha <= '$fecha_h'
              GROUP BY ar.ini";

//echo $qryReco."<br>";die();
  $resReco = mysql_query($qryReco,$link);
//  $rowReco=mysql_fetch_object($resReco);

// Recopilacion de datos Verificadores Pedidos y Unidades
$qryVerif = "INSERT INTO reporte(clave_ses,ref1,ref2,ent1,ent2)
				SELECT '$sesion',IFNULL(a1.ini_control,'SD'),ar.nyape,COUNT(*),SUM(a1.cantidad)
						FROM armados AS a1
							LEFT JOIN armadoras AS ar ON a1.ini_control = ar.ini
            WHERE  fecha_control >= '$fecha_d' AND fecha_control <= '$fecha_h'
            GROUP BY ar.ini";

//echo $qryVerif."<br>";
  $resVerif = mysql_query($qryVerif,$link);
//	$rowVerif=mysql_fetch_object($resVerif);
//*****************************************************************************************************

// Pedidos / Unidades
$qryDefi="SELECT CONCAT(ref1,';',ref2,';',SUM(ent1),';',SUM(ent2)) AS c
FROM reporte
WHERE clave_ses='$sesion'
GROUP BY ref1
ORDER BY SUM(ent1) DESC";

//echo $qryDefi; die();


//Iniciando PDF

  $reporte="$encab";//nombre del reporte (en todos)
  $data=$pdf->LoadData($qryDefi);

 // print_r($data);

  // ----------- borramos recopilacion de datos -------------

  $qrystr = "DELETE FROM reporte WHERE clave_ses='$sesion'";
  $qry = mysql_query($qrystr,$link);

  //---------------------------------------------------------

  $pdf->SetFont('Arial','',10);
  $pdf->AddPage('P');
  $pdf->BasicTable($header,$data,1);
  $pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
  $pdf->Ln();
 }

?>
