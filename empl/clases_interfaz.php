<?php
//******************************************* CLASE MARCO *************************************************
//*********************************************************************************************************
class Marco
{
 var $imagenes;
 var $tamanio;
 var $anchoMarco = "100%";
 var $alineamientoMarco = "center";
 var $alineamientoCelda = "center";
 var $directorio = ".";
 var $nombreArchivos;
 var $bgcolor="#990000";
 var $fontcolor="#990000";
 var $fontsize="1";
 var $fontface="Arial";
 
 /**
  * Marco::Marco()
  * 
  * @param $parDirectorio
  * @return 
  */
 function Marco($parDirectorio="",$parNombreArchivos="",$parBgColor="")
 {
	if($parDirectorio<>"")
	 {
  	  $this->setDirectorio($parDirectorio);
      if($parNombreArchivos<>"")
       {
	    $this->setNombreArchivos($parNombreArchivos);
	    $this->cargarImagenes();
       }
      else
        $this->nombreArchivos = $this->getDirectorio()."_";
	  $this->cargarImagenes();
	 }

    if ($parBgColor<>"")
      $this->setBgColor($parBgColor); 	
 }
 
 /**
  * Marco::setDirectorio()
  * 
  * @param $parDirectorio
  * 
  */
 function setDirectorio($parDirectorio)
 {
	$this->directorio = $parDirectorio;
 }
/**
  * Marco::getDirectorio()
  *
  * @return: directorio
  *
  */
 function getDirectorio()
 {
	return $this->directorio;
 }

 function setBgColor($parBgColor)
 {
  if($parBgColor<>"" )
    $this->bgcolor = $parBgColor;
 }

 function getBgColor()
 {
    return $this->bgcolor;
 }
  /**
  * Marco::setNombreArchivos()
  * 
  * @param $parNombreArchivos
  * Setea los nombres de archivos para las imagenes
  */
 function setNombreArchivos($parNombreArchivos)
 {
	$this->nombreArchivos = $parNombreArchivos;
 }
/**
  * Marco::getNombreArchivos()
  *
  * @return nombreArchivos
  *
  */

function getNombreArchivos()
 {
	return $this->nombreArchivos;
 }
 /**
  * Marco::cargarImagenes()
  * 
  * @return 
  */
 function cargarImagenes()
 {
	for($i=1;$i<10;$i++)
   {
	  if($i<>5)
	    {
       $this->imagenes[$i]=$this->getDirectorio()."/".$this->getNombreArchivos()."0$i.gif";
       $this->tamanio[$i] = GetImageSize($this->imagenes[$i],&$info);	
	    }
   }
 }
 
 /**
  * Marco :: abrirMarco()
  * 
  * @param: $par_FontFace,$par_FontColor,$par_FontSize
  */
  // Se modifico el metodo para poder setear la fuente de la celda
 function abrirMarco($par_FontFace="",$par_FontColor="",$par_FontSize="")
 {
  echo "<table width ='".$this->anchoMarco."' border='0' cellpadding='0' cellspacing='0' align='".$this->alineamientoMarco."'>
        <tr>
          <td>
              <img  src='".$this->imagenes[1]."' ".$this->tamanio[1][3]."></td>
          <td background='".$this->imagenes[2]."' ></td>
          <td>
              <img src='".$this->imagenes[3]."' ".$this->tamanio[3][3]."></td>
        </tr>
          
		<tr>
          <td background='".$this->imagenes[4]."' ></td>
          <TD bgcolor=".$this->bgcolor." width=100% align='".$this->getAlineamientoCelda()."'>
          <font ";// Desde aqui es nuevo todo
            if($par_FontFace<>""){
				$this->fontface=$par_FontFace;
				echo "face='".$this->fontface."'";
			}
			else{echo "face='".$this->fontface."'";}

			if($par_FontColor<>"")
			{
				$this->fontcolor=$par_FontColor;
				echo " color='".$this->fontcolor."'";
			}
			else{echo " color='".$this->fontcolor."'";}

			if($par_FontSize<>"")
			{
				$this->fontsize=$par_FontSize;
				echo " size='".$this->fontsize."'>";
			}
			else{echo " size='".$this->fontsize."'>";}
 }

 /**
  * Marco :: cerrarMarco()
  * 
  * @return 
  */
 function cerrarMarco()
 {
  echo "</font></TD>
            <td background='".$this->imagenes[6]."' ></td>
          </tr>
          <tr>
            <td>
              <img src='".$this->imagenes[7]."'  ".$this->tamanio[7][3]."></td>
            <td background='".$this->imagenes[8]."' ".$this->tamanio[8][3]."></td>
            <td >
              <img src='".$this->imagenes[9]."' ".$this->tamanio[9][3]."></td>
          </tr>
        </table>";
 }

 /**
  * Marco :: setAnchoMarco()
  * 
  * @return 
  */
 function setAnchoMarco($parAnchoMarco)
 {
	//Primero valido la entrada.
  if((!is_numeric($parAnchoMarco) and !is_numeric(substr($parAnchoMarco,0,strlen($parAnchoMarco)-1)))or(substr($parAnchoMarco,strlen($parAnchoMarco)-1,1)<>'%' and !is_numeric(substr($parAnchoMarco,strlen($parAnchoMarco)-1,1))))
		{
		 $this->mostrarError($parAnchoMarco." No es un Ancho v�lido","setAnchoMarco");
		 return;
		} 
	//Todo OK, sigo.
  $this->anchoMarco = $parAnchoMarco."%";
 }

 /**
  * Marco :: setAlineamientoMarco()
  * 
  * @return 
  */
 function setAlineamientoMarco($parAlineamientoMarco)
 {
	//Primero valido la entrada.
	$validos = array("CENTER","LEFT","RIGHT","JUSTIFY");
  if(!in_array(strtoupper($parAlineamientoMarco),$validos))
		{
		 $this->mostrarError($parAlineamientoMarco." No es un alineamiento v�lido","setAlineamientoMarco");
		 return;
		} 
	//Todo OK, sigo.
	$this->alineamientoMarco = $parAlineamientoMarco;
 }

function setAlineamientoCelda($parAlineamientoCelda)// Nuevo - alinea la celda
 {
	//Primero valido la entrada.
	$validos = array("CENTER","LEFT","RIGHT","JUSTIFY");
  if(!in_array(strtoupper($parAlineamientoCelda),$validos))
		{
		 $this->mostrarError($parAlineamientoCelda." No es un alineamiento v�lido","setAlineamientoCelda");
		 return;
		}
	//Todo OK, sigo.
	$this->alineamientoCelda = $parAlineamientoCelda;
}
// Devuelve el alineamiento de la Celda
function getAlineamientoCelda()
{
	return $this->alineamientoCelda;
}
 /**
  * Marco::mostrarError()
  * 
  * @param $parError
  * @param $fuente
  * @return 
  */
 function mostrarError($parError,$fuente="Indeterminado")
 {
  echo "<br>ERROR: Clase Marco->$fuente: ".$parError."<br>";
 }
}
//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************

//Texto de Prueba:
/*$a=new Marco("tabla_bordeaux");
//$a=new Marco("");
$a->setAnchoMarco("220000");
$a->setAlineamientoMarco("center");
//$a->setDirectorio("tabla_bordeaux");
//$a->setNombreArchivos("tabla_bordeaux_");
$a->cargarImagenes();

$a->abrirMarco();
echo " <span class=cremita align=center>
      <br> <br> Ingrese Un Comentario Para este Turno <br> <br> <br></span>";
$a->cerrarMarco();*/

//******************************************* CLASE PAGINADOR *********************************************
//*********************************************************************************************************

class Paginador
 {
  var $pagina;
  var $cantidadPaginacion;

  function Paginador($parPagina,$parCantPag)
   {
    $this->pagina = $parPagina;
    $this->cantidadPaginacion = $parCantPag;
   }

  function escribirAccesos($parDesde)
   {
    $accesos="";
    $desde = $parDesde;
    $desdeant = $desde - $this->cantidadPaginacion;
    if($desdeant>=0)
      $dirant = "<a ". ayuda("Ir a P�gina Anterior") . " href=".$this->pagina . "&desde=$desdeant>Anterior</a>";
    else
      $dirant = "Anterior";

    $desdeprox = $desde + $this->cantidadPaginacion;

    $accesos.= "
          <table width=760 CELLSPACING=0 >
              <tr>";
    $accesos.= "
              <td width='7%'>$dirant</td>";
    $accesos.= "
              <td width='10%'><a " . ayuda("Ir a P�gina Siguiente") . " href=".$this->pagina . "&desde=$desdeprox>Siguiente</a></td>";
    $accesos.= "
              <td width='83%' >";
    $hoja=0;
    $texto="Ir a P�gina: ";
    for($i=0;$i<20;$i++)
     {
      if (($i*$this->cantidadPaginacion) == $desde)
        {
         $hoja=$i+1;
         $ayuda = $texto.$hoja;
         $accesos.= "
             <a " . ayuda($ayuda) . " href=".$this->pagina."&desde=".$i*$this->cantidadPaginacion . "><b> ".($i+1)." </b></a>";
        }
      else
        {
         $hoja=$i+1;
         $ayuda = $texto.$hoja;
         $accesos.= "
             <a " . ayuda($ayuda) . " href=".$this->pagina."&desde=".$i*$this->cantidadPaginacion . "> ".($i+1)." </a>";
        }
     }
    $accesos.= "
             </td>";

    $accesos.= "
          </tr>
        </table>";
    return $accesos;
   }

  function limitar($parDesde)
   {
    $desde = $parDesde;
    return " limit $desde,".$this->cantidadPaginacion." ";
   }
 }

//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************

/*Ejemplo de uso

$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/alta_prov_ven.php",$cantidad_paginacion);
$qrystr = "select * from usuario
           where psw = 'pswpswpsw' ".$paginador->limitar($desde);
dfhghglkhjfghjgjh
fghfgh
ddhdh
echo $paginador->escribirAccesos($desde);*/

class pagination{
		/*Default values*/
		var $total_pages = -1;//items
		var $limit = null;
		var $target = ""; 
		var $page = 1;
		var $adjacents = 2;
		var $showCounter = false;
		var $className = "pagination";
		var $parameterName = "page";
		var $urlF = false;//urlFriendly

		/*Botones Anterior and Siguiente*/
		var $nextT = "Sig";
		var $nextI = "&#187;"; //&#9658;
		var $prevT = "Ant";
		var $prevI = "&#171;"; //&#9668;

		/*****/
		var $calculate = false;
		
		#Total items
		function items($value){$this->total_pages = (int) $value;}
		
		#how many items to show per page
		function limit($value){$this->limit = (int) $value;}
		
		#Page to sent the page value
		function target($value){$this->target = $value;}
		
		#Current page
		function currentPage($value){$this->page = (int) $value;}
		
		#How many adjacent pages should be shown on each side of the current page?
		function adjacents($value){$this->adjacents = (int) $value;}
		
		#show counter?
		function showCounter($value=""){$this->showCounter=($value===true)?true:false;}

		#to change the class name of the pagination div
		function changeClass($value=""){$this->className=$value;}

		function nextLabel($value){$this->nextT = $value;}
		function nextIcon($value){$this->nextI = $value;}
		function prevLabel($value){$this->prevT = $value;}
		function prevIcon($value){$this->prevI = $value;}

		#to change the class name of the pagination div
		function parameterName($value=""){$this->parameterName=$value;}

		#to change urlFriendly
		function urlFriendly($value="%"){
				if(eregi('^ *$',$value)){
						$this->urlF=false;
						return false;
					}
				$this->urlF=$value;
			}
		
		var $pagination;

		function pagination(){}
		function show(){
				if(!$this->calculate)
					if($this->calculate())
						echo "<div class=\"$this->className\">$this->pagination</div>\n";
			}
		function get_pagenum_link($id){
				if(strpos($this->target,'?')===false)
						if($this->urlF)
								return str_replace($this->urlF,$id,$this->target);
							else
								return "$this->target?$this->parameterName=$id";
					else
						return "$this->target&$this->parameterName=$id";
			}
		
		function calculate(){
				$this->pagination = "";
				$this->calculate == true;
				$error = false;
				if($this->urlF and $this->urlF != '%' and strpos($this->target,$this->urlF)===false){
						//Es necesario especificar el comodin para sustituir
						echo "Especificaste un wildcard para sustituir, pero no existe en el target<br />";
						$error = true;
					}elseif($this->urlF and $this->urlF == '%' and strpos($this->target,$this->urlF)===false){
						echo "Es necesario especificar en el target el comodin % para sustituir el número de página<br />";
						$error = true;
					}

				if($this->total_pages < 0){
						echo "It is necessary to specify the <strong>number of pages</strong> (\$class->items(1000))<br />";
						$error = true;
					}
				if($this->limit == null){
						echo "It is necessary to specify the <strong>limit of items</strong> to show per page (\$class->limit(10))<br />";
						$error = true;
					}
				if($error)return false;
				
				$n = trim($this->nextT.' '.$this->nextI);
				$p = trim($this->prevI.' '.$this->prevT);
				
				/* Setup vars for query. */
				if($this->page) 
					$start = ($this->page - 1) * $this->limit;             //first item to display on this page
				else
					$start = 0;                                //if no page var is given, set start to 0
			
				/* Setup page vars for display. */
				$prev = $this->page - 1;                            //previous page is page - 1
				$next = $this->page + 1;                            //next page is page + 1
				$lastpage = ceil($this->total_pages/$this->limit);        //lastpage is = total pages / items per page, rounded up.
				$lpm1 = $lastpage - 1;                        //last page minus 1
				
				/* 
					Now we apply our rules and draw the pagination object. 
					We're actually saving the code to a variable in case we want to draw it more than once.
				*/
				
				if($lastpage > 1){
						if($this->page){
								//anterior button
								if($this->page > 1)
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($prev)."\">$p</a>";
									else
										$this->pagination .= "<span class=\"disabled\">$p</span>";
							}
						//pages	
						if ($lastpage < 7 + ($this->adjacents * 2)){//not enough pages to bother breaking it up
								for ($counter = 1; $counter <= $lastpage; $counter++){
										if ($counter == $this->page)
												$this->pagination .= "<span class=\"current\">$counter</span>";
											else
												$this->pagination .= "<a href=\"".$this->get_pagenum_link($counter)."\">$counter</a>";
									}
							}
						elseif($lastpage > 5 + ($this->adjacents * 2)){//enough pages to hide some
								//close to beginning; only hide later pages
								if($this->page < 1 + ($this->adjacents * 2)){
										for ($counter = 1; $counter < 4 + ($this->adjacents * 2); $counter++){
												if ($counter == $this->page)
														$this->pagination .= "<span class=\"current\">$counter</span>";
													else
														$this->pagination .= "<a href=\"".$this->get_pagenum_link($counter)."\">$counter</a>";
											}
										$this->pagination .= "...";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($lpm1)."\">$lpm1</a>";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($lastpage)."\">$lastpage</a>";
									}
								//in middle; hide some front and some back
								elseif($lastpage - ($this->adjacents * 2) > $this->page && $this->page > ($this->adjacents * 2)){
										$this->pagination .= "<a href=\"".$this->get_pagenum_link(1)."\">1</a>";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link(2)."\">2</a>";
										$this->pagination .= "...";
										for ($counter = $this->page - $this->adjacents; $counter <= $this->page + $this->adjacents; $counter++)
											if ($counter == $this->page)
													$this->pagination .= "<span class=\"current\">$counter</span>";
												else
													$this->pagination .= "<a href=\"".$this->get_pagenum_link($counter)."\">$counter</a>";
										$this->pagination .= "...";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($lpm1)."\">$lpm1</a>";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($lastpage)."\">$lastpage</a>";
									}
								//close to end; only hide early pages
								else{
										$this->pagination .= "<a href=\"".$this->get_pagenum_link(1)."\">1</a>";
										$this->pagination .= "<a href=\"".$this->get_pagenum_link(2)."\">2</a>";
										$this->pagination .= "...";
										for ($counter = $lastpage - (2 + ($this->adjacents * 2)); $counter <= $lastpage; $counter++)
											if ($counter == $this->page)
													$this->pagination .= "<span class=\"current\">$counter</span>";
												else
													$this->pagination .= "<a href=\"".$this->get_pagenum_link($counter)."\">$counter</a>";
									}
							}
						if($this->page){
								//siguiente button
								if ($this->page < $counter - 1)
										$this->pagination .= "<a href=\"".$this->get_pagenum_link($next)."\">$n</a>";
									else
										$this->pagination .= "<span class=\"disabled\">$n</span>";
									if($this->showCounter)$this->pagination .= "<div class=\"pagination_data\">($this->total_pages Pages)</div>";
							}
					}

				return true;
			}
	}
?>
