<?php
require_once "clases_interfaz.php";
require_once "estetica.css";
?>
<script language="javascript">

function CheckEstado(est)
{
  if(est==1)
  {
     document.ff.activo.checked=true;
     document.ff.baja.checked=false;
  }
  else
  {
     document.ff.activo.checked=false;
     document.ff.baja.checked=true;
  }
}

function StringVaciouno(CampoNom,Campo)
{
  if((Campo==null) || (Campo.value==null) || (Campo.value.length==0))
  {
     alert("ERROR: Si no posee " + CampoNom + " debe seleccionar 'No Posee'.");
     Campo.focus();
     return true;
  }
  else
     return false;
}


function ComboVaciouno(CampoNom,Campo)
{
  if((Campo==null) || (Campo.value==null) || (Campo.value.length==0) || (Campo.value==0))
  {
     alert("ERROR: Elija " + CampoNom + ". ");
     Campo.focus();
     return true;
  }
  else
     return false;
}

function NumeroValido(CampoNom,Campo,Desde,Hasta)
{
        if (Campo.value.length==0)
        {
                alert("ERROR: Debe ingresar " + CampoNom);
                Campo.focus();
                return true;
        }
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if (!esNumerico(chr))
                {
                        alert("ERROR: Debe ingresar un n�mero");
                        Campo.focus();
                        return true;
                }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El n�mero debe estar entre " + Desde + " y " + Hasta);
              Campo.focus();
              return true;
          }


return false;
}

function ValidarDatos(Form)
{
  if(StringVacio("Nombre",Form.proveedor_descrip)) return false;
  if(StringVacio("Razon Social",Form.razon_social)) return false;
  if(NumeroValido("C.U.I.T",Form.cuit,1,999999999999)) return false;
  if(ComboVaciouno("Pa�s",Form.pais)) return false;
  if(ComboVaciouno("Provincia",Form.provincia)) return false;
  if(StringVacio("Localidad",Form.localidad)) return false;
  if(ComboVaciouno("Rubro",Form.id_rubro)) return false;


    if(Form.notienete.checked==0)
   {
     if (StringVaciouno("TE",Form.tel_fax)) return false;
   }
  if (Form.notienefax.checked==0)
   {
     if (StringVaciouno("Fax/Celular",Form.tel_celular)) return false;
   }
  if (Form.notienemail.checked==0)
   {
     if (StringVaciouno("Mail",Form.email)) return false;
   }
  if (!MailValido("Mail",Form.email)) return false;
  return true;
}
</script>
<?php
// $qrystr = "select * FROM proveedor where id_proveedor = '$par_id_proveedor'";
// $qry1= mysql_db_query($c_database,$qrystr ,$link);
// $row1 = mysql_fetch_array($qry1);
$t=$g_bgolor;
$marco1 = new Marco("img","tabla_",$t);
$marco1->abrirMarco();
echo "<font size='3' face='Arial'><b>Gestion de Proveedores</b></font> <a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/proveedores_abm_grabar.php.php&usuver=$codigousuario'></a></td>";
$marco1->cerrarMarco();

$marco1->setAlineamientoMarco("Left");
$marco1->abrirMarco();
echo "<table border=0><tr><td align='Left'>";
?>
<FORM name="ff" ENCTYPE=multipart/form-data ACTION=administracion.php METHOD=POST>
<p style="margin-left: 40">
  <INPUT TYPE=hidden name=usuario value=<?php echo $usuario;?>>
  <INPUT TYPE=hidden name=sesion value=<?php echo $sesion;?>>
  <INPUT TYPE=hidden name=par_id_proveedor value=<?php echo $par_id_proveedor;?>>
  <input type=hidden name=pagina value=empl/proveedores_abm_grabar.php>
  <font size="2" face="Arial"><b>Nombre</b></font><br>
  <input type="text" name="proveedor_descrip" size="50" <?php echo $stylefield;?>><br>
  <font size="2" face="Arial"><b>Tipo</b></font><br>
  <?php poner_combo("id_tipo","proveedor_tipo","descri_tipo","id_tipo")?><br>
  <font size="2" face="Arial"><b>Rubro</b></font><br>
  <?php poner_combo("id_rubro","sol_compra_producto","nombre","id_sol_comp_prod")?><br>
  <font size="2" face="Arial"><b>Razon Social</b></font><br>
  <input type="text" name="razon_social" size="30" <?php echo $stylefield;?>><br>  
  <font size="2" face="Arial"><b>C.U.I.T.</b></font><br>
  <input type="text" name="cuit" size="30" <?php echo $stylefield;?>><br>  
  <font size="2" face="Arial"><b>Condicion Fiscal</b></font><br>
  <?php poner_combo("id_condicion","condicion_fiscal","descrip","id_condicion")?><br> 
  <font size="2" face="Arial"><b>Domicilio</b></font><br>
  <input type="text" name="domicilio" size="30" <?php echo $stylefield;?>><br>
  <font size="2" face="Arial"><b>Pa�s</b></font><br>
  <?php // poner_combo_filtro_str("pais","lugar","lugar_nombre","lugar_cod","tab_tipo_lugar_cod = 2"); echo"<br>"; ?>
  <?php poner_combo_on_change("pais","lugar","lugar_nombre","lugar_cod","tab_tipo_lugar_cod = 2","cambia_provincia"); ?><br>

  <font size="2" face="Arial"><b>Provincia</b></font><br>
  <?php //poner_combo_filtro_str("provincia","lugar","lugar_nombre","lugar_nombre","tab_tipo_lugar_cod = 3"); echo "<br>"; ?>

  <select name=provincia>
  <option value="-">-
  </select><br>

  <font size="2" face="Arial"><b>Localidad</b></font><br>
  <input type="text" name="localidad" size="60" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>CP</b></font><br>
  <input type="text" name="cod_postal" size="20" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Tel�fono/Fax</b></font>
  <input type="checkbox" name="notienete" value="1" <?php echo $stylefield; ?> onclick="javascript: if(ff.notienete.checked==1) eval(ff.tel_fax.value='');" >No Posee<br>
  <input type="text" name="tel_fax" size="30" <?php echo $stylefield; ?>onfocus="javascript: eval(ff.notienete.checked=0);"><br>
  <font size="2" face="Arial"><b>Celular</b></font>
  <input type="checkbox" name="notienefax" value="1" <?php echo $stylefield; ?> onclick="javascript: if(ff.notienefax.checked==1) eval(ff.tel_celular.value='');" >No Posee<br>
  <input type="text" name="tel_celular" size="30" <?php echo $stylefield; ?>onfocus="javascript: eval(ff.notienefax.checked=0);"><br>
  <font size="2" face="Arial"><b>E-mail</b></font>
  <input type="checkbox" name="notienemail" value="1" <?php echo $stylefield; ?>onclick="javascript: if(ff.notienemail.checked==1) eval(ff.email.value='');" >No Posee<br>
  <input type="text" name="email" size="60" <?php echo $stylefield; ?> onfocus="javascript: eval(ff.notienemail.checked=0);"><br>
  <font size="2" face="Arial"><b>Web</b></font><br>
  <input type="text" name="web" size="60" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Contacto</b></font><br>
  <textarea rows='2' name="contacto" cols='60'></textarea><br>
  <font size="2" face="Arial"><b>Observaciones</b></font><br>
  <textarea rows='2' name="obs" cols='60'></textarea><br>

</p>
  <p align=center>
  <input type="button" value="Aceptar" name="Aceptar" <?php echo $stylebutton; ?>  onClick="javascript:if (ValidarDatos(ff)) submit();">&nbsp;&nbsp;
  <input type="reset" value="Restablecer" name="Restablecer" <?php echo $stylebutton; ?> >
  </p>
</form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<?php
 // echo "$codigousuario";

  if ($par_id_proveedor != '')
  {
     $qrystr = "select * FROM proveedor where id_proveedor = '$par_id_proveedor'";

     $qry = mysql_db_query($c_database,$qrystr ,$link);
     $row = mysql_fetch_array($qry);

     llenar_campo("document.ff","proveedor_descrip",$row[proveedor_descrip]);
     llenar_campo("document.ff","id_tipo",$row[id_tipo]);
     llenar_campo("document.ff","id_rubro",$row[rubro]);
     llenar_campo("document.ff","razon_social",$row[razon_social]);
     llenar_campo("document.ff","cuit",$row[cuit]);
     llenar_campo("document.ff","id_condicion",$row[condicion_fiscal]);
     llenar_campo("document.ff","domicilio",$row[domicilio]);
     llenar_campo("document.ff","localidad",$row[localidad]);
     llenar_campo("document.ff","pais",$row[pais]);
//     llenar_campo("document.ff","provincia",$row[provincia]);
     llenar_campo("document.ff","cod_postal",$row[cod_postal]);
     llenar_campo("document.ff","tel_fax",$row[tel_fax]);
     llenar_campo("document.ff","email",$row[email]);
     llenar_campo("document.ff","web",$row[web]);
     llenar_campo("document.ff","contacto",$row[contacto]);
     llenar_campo("document.ff","tel_celular",$row[tel_celular]);
          
     $row[obs] = str_replace("\x00", '\0', $row[obs]);
     $row[obs] = str_replace("\x08", '\b', $row[obs]);
     $row[obs] = str_replace("\x0a", '\n', $row[obs]);
     $row[obs] = str_replace("\x0d", '\r', $row[obs]);
     $row[obs] = str_replace("\x1a", '\Z', $row[obs]);
     $special_chars   = htmlspecialchars($row[obs]);
     $temp            = $row[obs];
     llenar_campo("document.ff","obs",$temp);
   }


function poner_combo_on_change($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro,$nom_funcion_change)
{
   global $stylefield;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . " onchange=\"$nom_funcion_change()\">";
   echo "<option value='0'>-- Elegir --</option>";    //Esto es s�lo para este sistema. Ac� ai el pa�s no est� informado, debe mostrar y grabar cero (a menos que le agregue la validaci�n de obligatoriedad).
   while ($row = mysql_fetch_array($qry))
   echo "<option value='$row[$campo_clave]'>$row[$campo_mostrar]</option>";
   echo "</select>";
}


?>


<script>
//defino una serie de variables Array para cada pa�s
<?php
   $qrystrpais = "select * from lugar where tab_tipo_lugar_cod = 2 ";
   $qrypais = mysql_db_query($c_database,$qrystrpais ,$link);
   while ($rowpais = mysql_fetch_array($qrypais))
    {
     echo "var provincias_". $rowpais[lugar_cod]."=new Array(\"\"";
     $qrystrprov = "select * from lugar AS lu INNER JOIN lugar_afinidad AS la ON lu.lugar_cod = la.campo_a where tab_tipo_lugar_cod = 3 AND la.campo_b=$rowpais[lugar_cod] ORDER BY lugar_nombre ASC";
     $qryprov = mysql_db_query($c_database,$qrystrprov ,$link);
     $enarray=0;
     while ($rowprov = mysql_fetch_array($qryprov))
      {
//       if($enarray>0) echo ",";
       echo ",";
       echo "\"$rowprov[lugar_nombre]\"";
       $enarray++;
      }
     echo ");";
    }
//var provincias_1=new Array("-","Andaluc�a","Asturias","Baleares","Canarias","Castilla y Le�n","Castilla-La Mancha","...")

   $qrystrpais = "select * from lugar where tab_tipo_lugar_cod = 2 ";
   $qrypais = mysql_db_query($c_database,$qrystrpais ,$link);
   while ($rowpais = mysql_fetch_array($qrypais))
    {
     echo "var id_provincias_". $rowpais[lugar_cod]."=new Array(\"\"";
     $qrystrprov = "select * from lugar AS lu INNER JOIN lugar_afinidad AS la ON lu.lugar_cod = la.campo_a where tab_tipo_lugar_cod = 3 AND la.campo_b=$rowpais[lugar_cod] ORDER BY lugar_nombre ASC";
     $qryprov = mysql_db_query($c_database,$qrystrprov ,$link);
     $enarray=0;
     while ($rowprov = mysql_fetch_array($qryprov))
      {
//       if($enarray>0) echo ",";
       echo ",";
       echo "\"$rowprov[lugar_cod]\"";
       $enarray++;
      }
     echo ");";
    }
//var provincias_1=new Array("-","Andaluc�a","Asturias","Baleares","Canarias","Castilla y Le�n","Castilla-La Mancha","...")

?>

//funci�n que cambia las provincias del select de provincias en funci�n del pa�s que se haya escogido en el select de pa�s.
function cambia_provincia(){
	//tomo el valor del select del pais elegido
	var pais
	pais = document.ff.pais[document.ff.pais.selectedIndex].value
	//miro a ver si el pais est� definido
	if (pais != 0) {
		//si estaba definido, entonces coloco las opciones de la provincia correspondiente.
		//selecciono el array de provincia adecuado
		mis_provincias=eval("provincias_" + pais)
		mis_id_provincias=eval("id_provincias_" + pais)
		//calculo el numero de provincias
		num_provincias = mis_provincias.length
		//marco el n�mero de provincias en el select
		document.ff.provincia.length = num_provincias
		//para cada provincia del array, la introduzco en el select
		for(i=0;i<num_provincias;i++){
		   document.ff.provincia.options[i].value=mis_id_provincias[i]
		   document.ff.provincia.options[i].text=mis_provincias[i]
		}	
	}else{
		//si no hab�a provincia seleccionada, elimino las provincias del select
		document.ff.provincia.length = 1
		//coloco un gui�n en la �nica opci�n que he dejado
		document.ff.provincia.options[0].value = ""
	    document.ff.provincia.options[0].text = ""
	}
	//marco como seleccionada la opci�n primera de provincia
	document.ff.provincia.options[0].selected = true
}
cambia_provincia();
</script>

<?php
  llenar_campo("document.ff","provincia",$row[provincia]);
echo "</td></tr></table>";
$marco1->cerrarMarco();
echo "<a href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/proveedores_lista.php><font><b>Volver a la Lista de Proveedores</b></font></a><br>";

?>
