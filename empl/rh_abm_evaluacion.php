<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
require_once "empl/clases_interfaz.php";
require_once "estetica.css";
 require_once('../clases/forms/class.forms_ale7.php');

 //####### OBLIGATORIOS
 $tablasuper = "rh_evaluaciones";          //La tabla del Supertipo.
 $preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/rh_abm_evaluacion.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/listado_rh_evaluaciones.php";//La p�gina a mostrar si graba exitosamente.

 $titulo = "ABM de Evaluacion";   //El t�tulo a mostrar en el html.
 $clave_primaria = "id_evaluacion";              //El nombre del campo que es Clave Primaria.
 
 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_evaluacion","sentido_eval","comentario","mes","anio","campania","usuario_alta","fecha_alta","momento_alta","estado","periodo_desde","periodo_hasta");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_evaluacion","Sentido","Comentario","mes","anio","Campa�a","usuario_alta","fecha_alta","momento_alta","estado","Periodo Evaluacion desde :","hasta:");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_evaluacion","usuario_alta","momento_alta","mes","anio","fecha_alta","estado");
 //Las validaciones de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (NumeroValido('Estado',Form.estado,0,1)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Desde',Form.desde)) return false;");
 //array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('Fecha Hasta',Form.hasta)) return false;");
 //Los COMBOS de la tabla supertipo.
 //array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("tipo_func","funcionalidad_tipo","descripcion_tipo_func","tipo_funcionalidad_id"));
 $qrystrth = "SELECT id_campania AS id, CONCAT(id_campania,' - ',obs) AS paq FROM `campanias` ORDER BY id DESC";
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("campania","paq","id",$qrystrth));
 //Las FECHAS de la tabla supertipo.
 $conf_tablas["$tablasuper"]->fechas = array("periodo_desde","periodo_hasta");
 
 $qrystrth1 = "SELECT id_enum AS id, valor AS paq FROM `enum_elementos` WHERE grupo_enum=6 ORDER BY id ASC";
 array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("sentido_eval","paq","id",$qrystrth1));
//***********************************************************
//******** FIN ESTO VA A IR EN OTRO ARCHIVO .inc ************



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           DB Layer Example Page - Inserting             #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


 /*Variables de entrada (de control)
 par_id                -- El Producto a mostrar en el ABM. Si es "" es una ALTA.
 par_idh               -- El Hotel elegido si es una ALTA.
 */

 if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = "";
   ${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
    }

   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //if ($accion_abm == 1)         //1 = ALTA.
   //  $form->addHiddenForm("idh","$par_idh");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.
   //$form->addHiddenForm("idtc","999");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
                $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
          {
           if ($accion_abm == 1)
            {
            $id=mysql_insert_id();
            
            $qrystr_up="UPDATE rh_evaluaciones SET fecha_alta=NOW(),usuario_alta='$usuario', momento_alta=NOW() WHERE id_evaluacion='$id' ";
            $qry_up = mysql_db_query($c_database,$qrystr_up,$link);
            
             echo "<b>Se ha generado el Evaluacion </b>";
            }
           else
               echo "<b>Se ha modificado la Evaluacion $par_id_departamento</b>";
           include ($par_pag_sig);
          }
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
    $t=$g_bgolor;
    $marco1 = new Marco("tabla_frente2","tabla_frente2_",$t);
    $marco1->abrirMarco();
    
 	 echo $form->build("200","10","200","3","40");

       	 
    $marco1->cerrarMarco();
     	 include "empl/menu_pedidos_data_n1.php";
        echo "<a href=administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/listado_rh_evaluaciones.php><font><b>Volver al Listado de Evaluaciones </b></font></a><br><br>";

	  }
	  
//################################################################################################
?>
