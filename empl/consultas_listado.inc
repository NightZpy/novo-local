<?php
//require_once "../clases/interfaz/classes_listas_ajax.php";
require_once "empl/ajax_lib2.inc";
require_once "clases_interfaz.php";
  $t=$g_bgolor;
  $marco1 = new Marco("img","tabla_",$t);
  $marcofrente = $marco1;
class ListasImp extends Listas
{
	function OnEnterRow(&$parFila)
	{
		global $usuario;
		global $sesion;
		global $c_database;
		global $link;
		global $paso_images_gif;
		global $nivel;
		global $modo_gc;
		$parFila[2]='';

    $paso="admin_vacia.php?pagina=empl/consultas_gestion.php&usuario=$usuario&sesion=$sesion&step_gestor=consulta_abm&id_consulta=$parFila[1]";

    if($modo_gc=='admin')
      $parFila[2] .= "<span class='bot_edicion' ONCLICK=\"javascript: setContenido('" . $this->idHtml . "_row". $this->fila ."','$paso&step_abm=&id_html=" . $this->idHtml . "_row". $this->fila ."&inicio_conteo_fila=". $this->fila ."&modo_gc=$modo_gc&id_contenido=$id_contenido&no_incluir_ajax=true');\"><li class='glyphicon glyphicon-edit'></li></span>";
	 	$parFila[2] .= "<br /><a style='cursor: pointer;' href=administracion.php?pagina=empl/consultas_gestion.php&usuario=$usuario&sesion=$sesion&step_gestor=formulario_informe&id_consulta=$parFila[1]><img border='' src='$paso_images_gif/actualizar_normal.gif'" . ayuda("Informe") . "></a>";

		$cons=new ConsultaSQL($parFila[1]);
		$parFila[4] = $cons->getQryStr2HTML();
	 	$parFila[5] = nl2br($parFila[5]);

    if($modo_gc<>'admin')
    {
      $img_ver="<img border=0 src=$paso_images_gif/modificar_over.gif>";
      $paso="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/asig_usuario_comentarios.php&usuver=$usuario&gran_tipo=4&asig_cod=" . $parFila[0];
      $paso_revisualizar="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/consultas_gestion.php&step_gestor=consultas_listado&fila_unica=$parFila[1]&inicio_conteo_fila=". $this->fila ."&modo_gc=$modo_gc&id_contenido=$id_contenido&desde=$desde&no_incluir_ajax=true";
      $img_ver_otros="<img border=0 src=$paso_images_gif/c_previa.gif>";
      $paso_ver_otros="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/asig_usuario_comentarios.php&asig_cod=$parFila[0]&gran_tipo=4&step=4&no_incluir_ajax=true";

      $parFila[6] = "<a style='cursor: pointer;' ONCLICK=\"javascript: var vent1 =new Ventana({titulo:'Comentario $parFila[3]',anchoInicial: '700px',altoInicial: '200px',onClose: function(){setContenido('" . $this->idHtml . "_row". $this->fila ."','$paso_revisualizar');}, className: '$class_name'});vent1.setAjaxContent('$paso&no_incluir_ajax=true&id_contenido='+vent1.capaContenido.div.id, {method: 'POST',asynchronous:false , evalScripts: true});vent1.capaContenedora.div.style.top=0;\">$img_ver</a>"
                  . " <a " . ayuda('Ver comentarios de otros usuarios') . " style='cursor: pointer;' ONCLICK=\"javascript: var vent1 =new Ventana({titulo:'Otros Comentarios',anchoInicial: '700px',altoInicial: '550px',onClose: function(){}, className: '$class_name'});vent1.setAjaxContent('$paso_ver_otros&no_incluir_ajax=true&id_contenido='+vent1.capaContenido.div.id, {method: 'POST',asynchronous:false , evalScripts: true});vent1.capaContenedora.div.style.top=10;\">$img_ver_otros</a>"
                  . " " .nl2br($parFila[6]);

    }

	}
}

if($fila_unica=='')
{
  if (!isset($desde)) $desde=0;
//   $paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&desde=$desde&pagina=empl/consultas_gestion.php&step_gestor=consultas_listado&modo_gc=$modo_gc",$cantidad_paginacion);
}

if($fila_unica<>'')
{
  $_modo_muestra = 'fila_unica';
  $filtro_unico = " AND c.id_consulta='$fila_unica' ";
}
$campos_asig = ",';',''";
if($modo_gc<>'admin')
{
  $filtro_admin= " AND c.id_consulta>'11' ";
  $inner_asig= " INNER JOIN asig_usuario AS au ON c.id_consulta =au.asig_cod AND gran_tipo='4' AND usuario_cod='$usuario' ";
  $campos_asig = ",';',IFNULL(comentario,'')";
}
$qrystr = "SELECT CONCAT(c.id_consulta,';',c.id_consulta,';',c.id_consulta,';',c.descripcion_consulta,';',c.qrystr_consulta,';',c.explicacion $campos_asig) AS c
           FROM consultas AS c
             $inner_asig
           WHERE 1
             $filtro_unico
             $filtro_admin
           ORDER BY c.id_consulta DESC
           ";


$lista = new ListasImp();
$lista->titulo = "<b>Elementos Asignables Encontrados</b>";

if($modo_gc=='admin')
{
$paso_nuevo="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/consultas_gestion.php&step_gestor=consulta_abm&estado=$estado&modo_gc=$modo_gc";
$lista->pasoNuevo ="<a ONCLICK=\"javascript:addFila('" . $id_contenido."_t_c" . "','$paso_nuevo');\"
         <img width=20 name='agregar' src=$paso_images_gif/agregar_chico.gif " . ayuda("Dar Alta Nueva Consulta") . " alt='Dar Alta Nueva Consulta' border=0>+</a>
              ";
}
else
$lista->pasoNuevo ="";


$lista->idHtml = $id_contenido."_t_c";

$lista->titulos=array("Check","Id","Acci&oacute;n","Nombre","Consulta","Explicaci&oacute;n","Comentario");
$lista->AlineacionColumnas(array('C','C','C','L','L','L','L'));

if($modo_gc=='admin')
  $lista->camposHidden=array("Check","Comentario");
else
  $lista->camposHidden=array("Check","Consulta");

$paso_paginacion = "admin_vacia.php?usuario=$usuario&sesion=$sesion&desde=$desde&pagina=empl/consultas_gestion.php&step_gestor=consultas_listado&modo_gc=$modo_gc&id_contenido=$id_contenido";
$lista->agregarPaginacion($paso_paginacion,50,$desde);
switch ($_modo_muestra)
{
 case '':
   $lista->recargarAlInicio=true;
 case 'estructura':
   include"estetica.css";
  if($id_contenido=='')
    $marcofrente->abrirMarco();

  echo "
  <STYLE type=text/css>.parametro_textbox {
  														 cursor: default;
                               BORDER-RIGHT: #AACCAA 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #AACCAA 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #AACCAA 2px solid;
                               COLOR: #005500;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #AACCAA 2px solid;
                               BACKGROUND-COLOR: #EEFFEE   }
  											.parametro_combobox {
  														 cursor: default;
                               BORDER-RIGHT: #AAAAFF 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #AAAAFF 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #AAAAFF 2px solid;
                               COLOR: #0000AA;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #AAAAFF 2px solid;
                               BACKGROUND-COLOR: #EEEEFF   }
  											.parametro_ajax {
  														 cursor: default;
                               BORDER-RIGHT: #FF9900 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #FF9900 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #FF9900 2px solid;
                               COLOR: #883300;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #FF9900 2px solid;
                               BACKGROUND-COLOR: #FFEEBB   }
  											.parametro_datebox {
  														 cursor: default;
                               BORDER-RIGHT: #996600 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #996600 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #996600 2px solid;
                               COLOR: #882200;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #996600 2px solid;
                               BACKGROUND-COLOR: #ffffcc   }
  											.sintaxis_sql {
                               COLOR: #cc0000;
                                  }
                        .bot_edicion {
  														 cursor: pointer;
                               BORDER-RIGHT: #CCAAAA 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #CCAAAA 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #CCAAAA 2px solid;
                               COLOR: #CC0000;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #CCAAAA 2px solid;
                               BACKGROUND-COLOR: #CCAAAA;
                               }
  											.bot_deshacer {
  														 cursor: pointer;
                               BORDER-RIGHT: #AACCAA 2px solid;
                               PADDING-RIGHT: 0px;
                               BORDER-TOP: #AACCAA 2px solid;
                               PADDING-LEFT: 0px;
                               PADDING-BOTTOM: 0px;
                               BORDER-LEFT: #AACCAA 2px solid;
                               COLOR: #009900;
                               PADDING-TOP: 0px;
                               BORDER-BOTTOM: #AACCAA 2px solid;
                               BACKGROUND-COLOR: #AACCAA;
                               }
                         div.resize-handle {
                              	cursor: e-resize;
                              	width: 2px;
                              	border-right: 1px dashed #1E90FF;
                              	position:absolute;
                              	top:0;
                              	left:0;
                              }

                        .sortcol {
                        	cursor: pointer;
                        	padding-right: 20px;
                        	background-repeat: no-repeat;
                        	background-position: right center;
                        }
                        .sortasc {
                        	background-color: #DDFFAC;
                        	background-image: url($paso_images_gif/up.gif);
                        }
                        .sortdesc {
                        	background-color: #B9DDFF;
                        	background-image: url($paso_images_gif/down.gif);
                        }
                        .nosort {
                        	cursor: default;
                        }

      </STYLE>";

   $contador = $lista->MuestraTabla($qrystr,'html',1,'estructura');


   if($id_contenido=='')
   {

      echo "
      <script language='javascript' type='text/javascript'>
      function enviagg(pag)
      {
       document.gg.step_gestor.value = pag;
       document.gg.submit();
      }

      function navegar(direccion, nueva_ventana)
         {
        if(direccion != '')
         {
          if (nueva_ventana)
           {
            window.open(direccion + '&nivel=' + ff.nivel.value + '&estado=' +ff.estado.value + '&promo=' +ff.promo.value +'&combo=' +ff.combo.value+'&desde=' +ff.desde.value + '&cmb_punt_aplic=' + ff.cmb_punt_aplic.value);
           }
          else
           {
            location.href = direccion + '&nivel=' + ff.nivel.value + '&estado=' +ff.estado.value + '&promo=' +ff.promo.value +'&combo=' +ff.combo.value+'&desde=' +ff.desde.value + '&cmb_punt_aplic=' + ff.cmb_punt_aplic.value;
           }
         }
         }
      </script>";



     $marcofrente->cerrarMarco();
     include("empl/menu_pedidos_data_n1.php");
   }
   break;
   case 'contenido':
     $contador = $lista->MuestraTabla($qrystr,'html',1,'contenido');
   break;
   case 'fila_unica':
     $lista->inicio_conteo_fila=$inicio_conteo_fila;
     $contador = $lista->MuestraTabla($qrystr,'html',1,'fila_unica');
   break;
   case 'completo':
     $marcofrente->abrirMarco();
     $contador = $lista->MuestraTabla($qrystr,'html',1,'completo');
     if($id_contenido=='')
     {
       $marcofrente->cerrarMarco();
       include("empl/menu_pedidos_data_n1.php");
     }
   break;
}
// if($fila_unica=='')
//   echo $paginador->escribirAccesos($desde);
// if($fila_unica=='')
//   $contador = $lista->MuestraTabla($qrystr);
// else
// {
//   $lista->inicio_conteo_fila=$inicio_conteo_fila;
//   $contador = $lista->MuestraTabla($qrystr,'html',1,$fila_unica);
// }
// if($fila_unica=='')
// {
//
//   $marcofrente->cerrarMarco();
//   echo $paginador->escribirAccesos($desde);
//   echo "
//   <script language='javascript' type='text/javascript'>
//   function enviagg(pag)
//   {
//    document.gg.step_gestor.value = pag;
//    document.gg.submit();
//   }
//
//   function navegar(direccion, nueva_ventana)
//      {
//     if(direccion != '')
//      {
//       if (nueva_ventana)
//        {
//         window.open(direccion + '&nivel=' + ff.nivel.value + '&estado=' +ff.estado.value + '&promo=' +ff.promo.value +'&combo=' +ff.combo.value+'&desde=' +ff.desde.value + '&cmb_punt_aplic=' + ff.cmb_punt_aplic.value);
//        }
//       else
//        {
//         location.href = direccion + '&nivel=' + ff.nivel.value + '&estado=' +ff.estado.value + '&promo=' +ff.promo.value +'&combo=' +ff.combo.value+'&desde=' +ff.desde.value + '&cmb_punt_aplic=' + ff.cmb_punt_aplic.value;
//        }
//      }
//      }
//   </script>";
//
//   include("empl/menu_pedidos_data_n1.php");
//}
?>
