<?php
/* Tratamiento para el cobro de cuotas acc
-----------------------------------------*/

// Variables obligatorios
	 $cta_plan="1.2.2.25";
	 $cta_ref="N.D. accesorios en cuotas";
	 $cuotas_disminuir=0;
//-----------------------

// Debo seleccionar todas las cuotas con estado Adeudada del mismo distribuidor acumulada por pedido
		$sqlCobroCuotasAcc="SELECT id_pedido,cuotas_adeudadas,vendedor,camp_inicio,camp_empieza,camp_fin,SUM(monto_cuota) AS monto
		FROM cuotas_acc
		WHERE estado ='Adeudada'
		GROUP BY id_pedido
		ORDER BY vendedor";
		$resCobroCuotasAcc=mysql_db_query($c_database,$sqlCobroCuotasAcc,$link);
		$resNumCobroCuotasAcc=mysql_num_rows($resCobroCuotasAcc);

if($resNumCobroCuotasAcc>0){

	while($rowCobroCuotasAcc=mysql_fetch_object($resCobroCuotasAcc)){

		if($camp>=$rowCobroCuotasAcc->camp_empieza && $camp<=$rowCobroCuotasAcc->camp_fin){
			$sqlInsertCobroCuotasAcc="INSERT INTO ctacte(id_cod_us,id_cta,ref,debe,pedido,fecha,t,fecha_gene,campania)
			VALUES('".$rowCobroCuotasAcc->vendedor."','".$cta_plan."','".$cta_ref."',".$rowCobroCuotasAcc->monto.",".$rowCobroCuotasAcc->id_pedido.",NOW(),'A',NOW(),".$camp.")";

			$resInsertCobroCuotasAcc=mysql_db_query($c_database,$sqlInsertCobroCuotasAcc,$link);

			// Si ya es la ultima cuota, cambiar el estado a 'Cancelada'
			if($camp==$rowCobroCuotasAcc->camp_fin){
				$estado=",estado='Cancelada'";
			}
			else{
				$estado="";
			}

			// Disminuir la cantidad de cuotas adeudadas del pedido
			$cuotas_disminuir=($rowCobroCuotasAcc->cuotas_adeudadas)-1;

			$sqlUpdCobroCuotasAcc="UPDATE cuotas_acc SET cuotas_adeudadas=".$cuotas_disminuir.$estado."
			WHERE id_pedido=".$rowCobroCuotasAcc->id_pedido." AND vendedor='".$rowCobroCuotasAcc->vendedor."'";

			$resUpdCobroCuotasAcc=mysql_db_query($c_database,$sqlUpdCobroCuotasAcc,$link);


		}// End if line 34
		//echo "<br>Insert Cta Cte:".$sqlInsertCobroCuotasAcc."<br>Actualizacion de Cuotas: ".$sqlUpdCobroCuotasAcc."<br>";
		$detalle="Cobro de cuota para el pedido: ".$rowCobroCuotasAcc->id_pedido.", vendedor: ".$rowCobroCuotasAcc->vendedor.", camp: ".$camp.",estado: ".$estado.",monto: ".$rowCobroCuotasAcc->monto;
		// Guardar auditoria
		$sqlAudita="INSERT INTO auditoria (cod_us,fecha,area,accion,detalle)	VALUES('SISTEMA','NOW()','CTA CTE','Mov. Automatico',".$detalle.")";
		$resAudita=@mysql_db_query($c_database,$sqlAudita,$link);
	}// End while
}

/* ------ Fin tratamiento cobro de cuotas acc
---------------------------------------------*/
?>
