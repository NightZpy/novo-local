<?
require_once('../clases/forms/class.forms_ale6.php');

//####### OBLIGATORIOS
$tablasuper = "documento";  //La tabla del Supertipo.
$tablasub = "doc_contrato";  //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
$estapagina = "empl/doc_contrato_abm.php";  //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
$par_pag_sig = "empl/listado_clientes_doc.php";  //La p�gina a mostrar si graba exitosamente.

$titulo = "Gesti�n de Documentos de Contrato";  //El t�tulo a mostrar en el html.
$clave_primaria = "id_doc";  //El nombre del campo que es Clave Primaria.
$campo_super_sub = 'tipo_doc';  //El campo que est� en la tabla supertipo, que indica el tipo del objeto.
$conf_tablas = array();  //Array que contiene las opciones para las tablas a mostrar.
//####### OPCIONALES
//T�tulos de los campos.
$qt="DELETE FROM reporte WHERE clave_ses='CONTRATO1'";
$qy=mysql_db_query($c_database,$qt,$link);
$qt="DELETE FROM reporte WHERE clave_ses='CONTRATO2'";
$qy=mysql_db_query($c_database,$qt,$link);
$qt="DELETE FROM reporte WHERE clave_ses='CONTRATO3'";
$qy=mysql_db_query($c_database,$qt,$link);

$qt="INSERT INTO reporte (clave_ses,ref1) VALUES ('CONTRATO1','GERENTE'),('CONTRATO1','DISTRIBUIDOR')";
$qy=mysql_db_query($c_database,$qt,$link);
$qt="INSERT INTO reporte (clave_ses,ref1) VALUES ('CONTRATO2','COMPLETO'),('CONTRATO2','INCOMPLETO')";
$qy=mysql_db_query($c_database,$qt,$link);
$qt="INSERT INTO reporte (clave_ses,ref1) VALUES ('CONTRATO3','ENVIADO POR CORREO'),('CONTRATO3','FIRMA EN EMPRESA'),('CONTRATO3','FIRMA EN RESISTENCIA'),('CONTRATO3','FIRMA EN BS AS'),('CONTRATO3','--NINGUN--')";
$qy=mysql_db_query($c_database,$qt,$link);

//****** tabla tipo.
$conf_tablas["$tablasuper"] = new conf_tabla();  //La clase conf_tabla est� definida en el abm.
$conf_tablas["$tablasuper"]->campos_usar = array("id_doc","area","tipo_doc","descri_documento","fec_cre","id_distrib","usuario","ubicacion","lectura","escritura");
$conf_tablas["$tablasuper"]->titulos_campos = array("id_doc","area","tipo_doc","descri_documento","fec_cre","id_distrib","usuario","Ubicaci�n","lectura","escritura");
$conf_tablas["$tablasuper"]->campos_hidden = array("id_doc","area","id_distrib","usuario","tipo_doc","fec_cre","lectura","escritura");

//****** tabla subtipo.
$conf_tablas["$tablasub"] = new conf_tabla();  //La clase conf_tabla est� definida en el abm.
$conf_tablas["$tablasub"]->campos_usar = array("id_doc","ger_dist","completo","tramite","fec_tramite","inicio","vto","certificado","firma");
$conf_tablas["$tablasub"]->titulos_campos = array("id_doc","Categoria","Estado","Tr�mite","Fecha Tr�mite","Fecha Inicio","Fecha Vencimiento","Fecha Certificado","Fecha Firma");
$conf_tablas["$tablasub"]->campos_hidden = array("id_doc","ger_dist","completo","tramite");

//Los COMBOS de la tabla tipo.
$qger_dist="SELECT ref1 FROM reporte WHERE clave_ses='CONTRATO1' ORDER BY ref1";
array_push($conf_tablas["$tablasub"]->combos, new ConfComboAMedida("ger_dist","ref1","ref1",$qger_dist));
$qcompleto="SELECT ref1 FROM reporte WHERE clave_ses='CONTRATO2' ORDER BY ref1";
array_push($conf_tablas["$tablasub"]->combos, new ConfComboAMedida("completo","ref1","ref1",$qcompleto));
$qtramite="SELECT ref1 FROM reporte WHERE clave_ses='CONTRATO3' ORDER BY ref1";
array_push($conf_tablas["$tablasub"]->combos, new ConfComboAMedida("tramite","ref1","ref1",$qtramite));

//Las FECHAS de la tabla subtipo.
$conf_tablas["$tablasub"]->fechas = array("fec_tramite","inicio","vto","certificado","firma");

//***********************************************************

if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;  //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = nuevo_id($tablasuper,$clave_primaria);
   ${$clave_primaria} = $nuevo;
   echo "Id Documento: ".$nuevo;
  }
else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;  //2=MODIFICACI�N.
  }
// Prepare Form
// ============

//************* Ac� se declara el objeto Base de Datos con sus campos ******************
$db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
$db1->select();

//************* Ac� se declara el objeto formulario con sus campos ******************
$form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
$form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

if($accion_abm == 1)  //1 = ALTA.
  {
   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
   $form->addHiddenForm($clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
   $form->addtable($db1,"$tablasub","","",implode(":",$conf_tablas["$tablasub"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasub"]->campos_usar),implode(":",$conf_tablas["$tablasub"]->fechas));
   $form->addHiddenForm($campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
   $form->addHiddenForm("par_".$campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
  }
else  //MODIFICACI�N.
  {
   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
   $form->addtable($db1,"$tablasub","","",implode(":",$conf_tablas["$tablasub"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasub"]->campos_usar),implode(":",$conf_tablas["$tablasub"]->fechas));
   $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
  }
$usuarioperm = "#," . $usuario . ",";
$form->addHiddenForm("tipo_doc","$tablasub");
$form->addHiddenForm("id_distrib",$codigousuario);
$form->addHiddenForm("codigousuario",$codigousuario);
$form->addHiddenForm("usuario",$usuario);
$form->addHiddenForm("lectura",$usuarioperm);
$form->addHiddenForm("escritura",$usuarioperm);
$form->addHiddenForm("area","CONTRATO");
$form->addHiddenForm("fec_cre",date('Y-m-d'));

//Los t�tulos de los campos separados con /:/
$form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$tablasub"]->titulos_campos)));
//$form->addHiddenForm("nombre_producto","de pecho");  //As� se setea un campo de pecho. No importa si se modifica por el usuario.

//Los combos de la tabla Supertipo.
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasuper"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//Los combos de la tabla Subtipo seleccionada.
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasub"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//FABRICAR LOS CAMPOS.
$form->makefields();
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasub"]->validaciones[$v]);

//************* Ac� se define si hay que mostrar los input **************************
//************* o si hay que actualizar los valores en la bd. ***********************
// Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
// ===================================================
if($hacerenvio=='S')
  {
   if(sesion_ok($usuario,$sesion)=='0')
     {
      echo "La sesi�n ha caducado";
      //header("location:error.php");
     }
   else
     {
      if($form->submit())
        {
		     if($renueva_sesion == 'S')
            $sesion = sesion_ok_ns($usuario,$sesion,4000);
         if($cierra_al_grabar == 'S')
            echo "<script language='javascript'>close();</script>";
         else
           {
            if($accion_abm == 1)
               echo "<br><b>Se ha generado la Rescisi�n $nuevo</b>";
            else
               echo "<br><b>Se ha modificado la Rescisi�n $par_id_doc</b>";
            include($par_pag_sig);
           }
 	    	 }
	     else
         {
		      echo $error;
		      echo mysql_error();
		      echo "\n<br><a href=$PHP_SELF>Back</a>";
		     }
	    }
  }
else
  {
 	 echo $form->build("200","10","200","3","40");
 	 include "menu_pedidos_data_n1.php";
  }

//################################################################################################
function nuevo_id($tablasuper,$campo)
{
 global $link;
 global $c_database;
 $strConsulta = "SELECT MAX(`$campo`)+1
                 FROM $tablasuper " ;
 $qrynuevo = mysql_db_query($c_database,$strConsulta,$link);
 $rownuevo = mysql_fetch_array($qrynuevo);
 return $rownuevo[0];
}
?>
