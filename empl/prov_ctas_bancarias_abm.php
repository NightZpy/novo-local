<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
 require_once('../clases/forms/class.forms_ale6.php');
 $qrystrban="SELECT proveedor_descrip FROM proveedor WHERE id_proveedor=$par_id_proveedor"; 
 $qryban = mysql_db_query($c_database,$qrystrban,$link);
 $rowban = mysql_fetch_array($qryban);   
  //####### OBLIGATORIOS
 $tablasuper = "ctas_x_prov";          //La tabla del Supertipo.
 //$preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/prov_ctas_bancarias_abm.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/prov_cuentas_bancarias.php";//La p�gina a mostrar si graba exitosamente.

 $titulo = "ALTA DE NUEVA CUENTA BANCARIA DEL PROVEEDOR <font face='Arial' color=red><b> $rowban[0] </b></font></font>";   //El t�tulo a mostrar en el html.
 $clave_primaria = "id_cuenta";              //El nombre del campo que es Clave Primaria.
 
 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_prov","id_banco","id_tipo_cuenta","nro_cuenta","cbu","titular_nombre","titular_apellido","obs");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_prov","BANCO","TIPO CTA","N� CUENTA","CBU","TITULAR NOMBRE","TITULAR APELLIDO","OBS");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_prov","banco","tipo_cuenta");
 //$qrystr = " INSERT INTO reporte (clave_ses,ref1,ent1) VALUES ('GRUPO','EST�TICO',0),('GRUPO','DIN�MICO',1)";
 //$qry= mysql_db_query($c_database,$qrystr,$link);
//  $qrystr1 = "SELECT ref1,ent1 FROM reporte WHERE clave_ses='GRUPO' ORDER BY ref1";
//  array_push($conf_tablas["$tablasuper"]->combos, new ConfComboAMedida("estodin","ref1","ent1",$qrystr1));
array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_banco","deposito","nombre_deposito","id_deposito"));
array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_tipo_cuenta","tipos_cuentas","cuenta_descri","id_tipo_cuenta"));
// array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("titular_tipo_doc","tipo_doc","descri_tipo_doc","tipo_doc_id"));
//Las validaciones de la tabla supertipo.
array_push($conf_tablas["$tablasuper"]->validaciones, "if (NumeroValido('CBU',Form.cbu,0,9999999999999999999999)) return false;");
 
 if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;                //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = nuevo_id($tablasuper,$clave_primaria);
   ${$clave_primaria} = $nuevo;
  }
 else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;                //2=MODIFICACI�N.
  }
   // Prepare Form
   // ============

   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.
     $form->addHiddenForm("id_prov",$id_proveedor);
   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm($clave_primaria,${$clave_primaria});      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("id_prov",$par_id_proveedor);
     $form->addHiddenForm("par_id_proveedor",$par_id_proveedor);
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));//
     $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("id_grupo",${$clave_primaria});
     $form->addHiddenForm("id_prov",$par_id_proveedor);
     $form->addHiddenForm("par_id_proveedor","$par_id_proveedor");
    }

   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }
   //Los combos de la tabla Subtipo seleccionada.
   for($v=0;$v<sizeof($conf_tablas["$preftablasub".$tipo]->combos);$v++)
    {
     $temp = $conf_tablas["$preftablasub".$tipo]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
     $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);

   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================

   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
                $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
          {
           if ($accion_abm == 1)
               {
                   if($nuevo=='')
                      $nuevo=1;
                   echo"<b>Se ha generado una nueva cuenta bancaria del proveedor $par_id_proveedor</b>";
                   $qrystr1 = " INSERT INTO grupos_usuarios (grupo_cod,usuario_cod)
                                VALUES ($nuevo,'$usuario')";
                   $qry1= mysql_db_query($c_database,$qrystr1,$link);
                   $qrystr2 = "DELETE FROM reporte WHERE clave_ses = 'GRUPO'";
                   $qry2= mysql_db_query($c_database,$qrystr2,$link);
                  //echo"ssssss   $qrystr1";
               }
           else
               echo "$qrystr1<b>Se ha modificado la cuenta bancaria del proveedor $par_id_proveedor</b>";
           include ($par_pag_sig);
          }
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
   	 echo $form->build("200","10","200","3","40");
	  }
echo "<a href=administracion.php?usuario=$usuario&par_id_proveedor=$par_id_proveedor&sesion=$sesion&pagina=empl/prov_cuentas_bancarias.php><font face=arial size=2 color=black><b>Volver a la Lista de Cuentas</b></font></a><br>";

//################################################################################################
function nuevo_id($tablasuper,$campo)
 {
  global $link;
  global $c_database;

  $strConsulta = "SELECT MAX(`$campo`)+1
                  FROM $tablasuper " ;
  $qrynuevo = mysql_db_query($c_database,$strConsulta,$link);
  $rownuevo = mysql_fetch_array($qrynuevo);
  return $rownuevo[0];
 }

?>
