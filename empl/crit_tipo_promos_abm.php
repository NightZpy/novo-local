<?
require_once('../clases/forms/class.forms_ale6.php');
//####### OBLIGATORIOS
$tablasuper = "crit_promos_tipo";  //La tabla del Supertipo.
$estapagina = "empl/crit_tipo_promos_abm.php";  //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
$par_pag_sig = "empl/crit_tipo_promo_lista.php";  //La p�gina a mostrar si graba exitosamente.

$titulo = "Gesti�n de Tipo de Promociones";  //El t�tulo a mostrar en el html.
$clave_primaria = "id_tipo";  //El nombre del campo que es Clave Primaria.
$conf_tablas = array();  //Array que contiene las opciones para las tablas a mostrar.
//####### OPCIONALES
//T�tulos de los campos.

//****** tabla tipo.
$conf_tablas["$tablasuper"] = new conf_tabla();  //La clase conf_tabla est� definida en el abm.
$conf_tablas["$tablasuper"]->campos_usar = array("id_tipo","desc_tipo");
$conf_tablas["$tablasuper"]->titulos_campos = array("id_tipo","Tipo Promoci�n");
$conf_tablas["$tablasuper"]->campos_hidden = array("id_tipo");

//***********************************************************
//array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_tipo","crit_promos_tipo","desc_tipo","id_tipo"));



if(!isset(${"par_".$clave_primaria}))
  {
   $accion_abm = 1;  //1=ALTA
   //$titulo .= " - ". datos_hotel($par_idh). "($par_idh)";
   $nuevo = nuevo_id($tablasuper,$clave_primaria);
   ${$clave_primaria} = $nuevo;
   echo "Id Promo: ".$nuevo;
  }
else
  {
   ${$clave_primaria} = ${"par_".$clave_primaria};
   $accion_abm = 2;  //2=MODIFICACI�N.
  }
// Prepare Form
// ============

//************* Ac� se declara el objeto Base de Datos con sus campos ******************
$db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
$db1->select();

//************* Ac� se declara el objeto formulario con sus campos ******************
$form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
$form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

if($accion_abm == 1)  //1 = ALTA.
  {
   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
   $form->addHiddenForm($clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
   $form->addHiddenForm($campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
   $form->addHiddenForm("par_".$campo_super_sub,${"par_".$campo_super_sub});  //As� deber�a setearse el ID de la relaci�n supertipo subtipo. Recordar poner como hidden al campo de la tabla.
  }
else  //MODIFICACI�N.
  {
   $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,$clave_primaria." = '".${$clave_primaria}."'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar),implode(":",$conf_tablas["$tablasuper"]->fechas));
   $form->addHiddenForm("par_".$clave_primaria,${$clave_primaria});  //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
  }

//Los t�tulos de los campos separados con /:/
$form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$tablasub"]->titulos_campos)));
//$form->addHiddenForm("nombre_producto","de pecho");  //As� se setea un campo de pecho. No importa si se modifica por el usuario.

//Los combos de la tabla Supertipo.
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasuper"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//Los combos de la tabla Subtipo seleccionada.
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->combos);$v++)
  {
   $temp = $conf_tablas["$tablasub"]->combos[$v];
   $temp->ponerEnForm($db1,$form);
  }
//FABRICAR LOS CAMPOS.
$form->makefields();
for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);
for($v=0;$v<sizeof($conf_tablas["$tablasub"]->validaciones);$v++)
    $form->addValidaciones($conf_tablas["$tablasub"]->validaciones[$v]);

//************* Ac� se define si hay que mostrar los input **************************
//************* o si hay que actualizar los valores en la bd. ***********************
// Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
// ===================================================
if($hacerenvio=='S')
  {
   if(sesion_ok($usuario,$sesion)=='0')
     {
      echo "La sesi�n ha caducado";
      //header("location:error.php");
     }
   else
     {
      if($form->submit())
        {
		     if($renueva_sesion == 'S')
            $sesion = sesion_ok_ns($usuario,$sesion,4000);
         if($cierra_al_grabar == 'S')
            echo "<script language='javascript'>close();</script>";
         else
           {
            if($accion_abm == 1)
               echo "<br><b>Se ha generado el Tipo Promo $nuevo</b>";
            else
               echo "<br><b>Se ha modificado el Tipo Promo $par_id_tipo</b>";
            include($par_pag_sig);
           }
 	    	 }
	     else
         {
		      echo $error;
		      echo mysql_error();
		      echo "\n<br><a href=$PHP_SELF>Back</a>";
		     }
	    }
  }
else
  {
 	 echo $form->build("200","10","200","3","40");
 	 include "empl/menu_pedidos_data_n1.php";
  }

//################################################################################################
function nuevo_id($tablasuper,$campo)
{
 global $link;
 global $c_database;
 $strConsulta = "SELECT MAX(`$campo`)+1
                 FROM $tablasuper " ;
 $qrynuevo = mysql_db_query($c_database,$strConsulta,$link);
 $rownuevo = mysql_fetch_array($qrynuevo);
 return $rownuevo[0];
}
?>
