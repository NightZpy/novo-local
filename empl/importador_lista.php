<?php
include "empl/clases_interfaz.php";
require_once "estetica.css";
switch ($step)
{
 //##################################################################################################################
 case '':
  require_once "cabecera_ajax.php";
  $marco=new Marco("tabla","tabla_","#FFFDF0");
  $marco->setAnchoMarco('100%');
  $marco->abrirMarco();
?>  
    <table border='0' >
      <tr><td >
        <h2><b>Importador de Lista de Precios</b></h1>
      </td ></tr>
    </table>

    <div align=center>
        <form method='POST' action='administracion.php' name='ff' enctype="multipart/form-data">
        <table border='0' width='80%'>
          <tr>
            <td>
              <input type=hidden name='usuario' value='<?=$usuario ?>'>
              <input type=hidden name='sesion' value='<?=$sesion ?>'>
              <input type=hidden name='pagina' value='empl/importador_lista.php'>
              <input type="hidden" name="step" value="1" />
              <input type="hidden" name="path" value="" />
<?php              
              $t=$g_bgolor;
              $marco1 = new Marco("img","tabla_",$t);
              $marco1->abrirMarco();
?>
              <h2><b>Lista de Precios</b></h2>
              <table border='0' width='71%'>
                <table border='0' width='100%'>
                  <tr>
                    <td>
	                  <font face=arial size='2'><br/><b>Lista:</b></font>
	                </td>
                    <td width='22%' align=center>
                       <?php $qrystramedida = "SELECT id_lista, concat(nombre_lista,' (',id_lista,')') AS paq FROM listas_precios order by id_lista desc";
                             poner_combo_a_medida("id_lista","paq","id_lista",$qrystramedida);
                       ?>
                    </td>
 	              </tr>
                  <tr>
                    <td>
	                  <font face=arial size='2'><br/><b>Archivo:</b></font>
	                </td>
	                <td>
                      Seleccione el archivo <strong>.xls</strong>:
                      <input type="file" id="param" name="param" value="" size=50 >
                      <br />
                    </td>
                  </tr>
                </table>
<?php
             $marco1->cerrarMarco();
?>             
             </td>
           </tr>
           <tr><td></td></tr>
         </table>
         <table border='0' width='80%'>
 	       <tr>
             <td width='22%' align=center>
               <input type="button" value='Aceptar' onclick="validar(this.form, this.form.param.value)" <?=$stylebutton ?>>
             </td>
 	       </tr>
         </table>
         </form>
         <script type="text/javascript">
             function validar(formulario, archivo)
             {
               permitir = new Array(".xls");
               if (!archivo){
                 //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
                 alert('Debes seleccionar un archivo .xls');
                 return 0;
               }else{
                 //recupero la extensión de este nombre de archivo
                 extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
                 //compruebo si la extensión está entre las permitidas
                 permitida = false;
                 for(var i = 0; i < permitir.length; i++){
                   if (permitir[i] == extension) {
                     permitida = true;
                     formulario.path.value = archivo;
                     formulario.submit();
                     break;
                   }
                 }
                 if(!permitida){
                   alert('Formato de archivo no permitido.!!');
                   return 0;
                 }
               }
             }        
         </script>
<?php
require_once "../clases/interfaz/classes_listas4.php";
class ListasImp extends Listas
{
 function OnEnterRow(&$parFila)
 {
  global $usuario;
  global $sesion;
  $parFila[3]="<a target=_blank href=empl/pdf/promo_crucero_externo.php?usuario=$usuario&sesion=$sesion&pedido=$parFila[0]><img border=0 src=images_gif/pdf.gif></a>";
 }
}
?>

<script language='javascript' type='text/javascript'>

function navegar(direccion, nueva_ventana)
 {
  if(direccion != '')
   {
    if (nueva_ventana)
      window.open(direccion);
    else
      location.href = direccion;
   }
 }
</script>
<?php
  $relat="";
  $marco->cerrarMarco();
  include('empl/menu_pedidos_data_n1.php');
 break;
 //##################################################################################################################
 case 1://Procesamiento del archivo
   $t=$g_bgolor;
   $marco1 = new Marco("img","tabla_",$t);
   $marco1->abrirMarco();
     echo "<h2><b>Procesando archivo...</b></h2>";
     echo "<img src='images_vd/procesando.gif' height='50' width='50' alt='Procesando XLS' />";
   $marco1->cerrarMarco();  
   flush();
   include('empl/importador_lista_procesar.php');
   echo "<script type='text/javascript'>
       location.href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/importador_lista.php&step=2&inserted=$inserted&updated=$updated';
   </script>";    
 break;
 case 2:
   $t=$g_bgolor;
   $marco1 = new Marco("img","tabla_",$t);
   $marco1->abrirMarco();
     echo "<h2><b>Importaci&oacute;n exitosa</b></h2>";
     echo "<p>Se actualizaron <strong>$updated</strong> registros.<br/>
     Se ingresaron <strong>$inserted</strong> nuevos registros</p>";
     echo "<p style='width:100%;text-align:center;'>
         <a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/importador_lista.php&step='>Volver</a>
     </p>";
   $marco1->cerrarMarco();
 break;	
}//fin switch
?>