<?php
define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;
var $altoFila;     //La Altura de la fila de datos
var $MaxRengFila;  //El tama�o m�ximo al que puede crecer un rengl�n.
var $header_si;
var $tipoHeader;
var $usuarioGenera;
var $tipoFooter;
var $cambiaEnGrupo;               //Campos en los que se hace cambio de grupo.
var $grupoActual;                 //Grupos para hacer cambio de grupo.
var $usarGrupos;                  //[True|False].
var $camposHidden;                //Columnas que no se muestran.
var $fuenteTitulos;               //Tama�o de la fuente en los t�tulos.
var $totales;                     //Array que contiene los totales.
var $encabezarGrupos;             //[True|False]. Muestra u oculta los t�tulos de columna al cambiar de grupo.
var $imprimirGrupos;              //[True|False]. Imprime los t�tulos del grupo que cambia.
var $subTotales;                     //Array de arrays que contienen los subtotales por cada columna agrupable.
var $mostrarSubtotales;           //[True|False]. Muestra u oculta los subtotales al cambiar de grupo.

function PDF($orientation='P',$unit='mm',$format='A4',$parAltoFila=5,$parMaxRengFila=0)
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
	$this->setAltoFila($parAltoFila);
	$this->MaxRengFila =  $parMaxRengFila;    //0=Ilimitado.
	$this->header_si = 1;
	$this->tipoFooter = 0;
  $this->cambiaEnGrupo=array();
  $this->camposHidden=array();
  $this->usarGrupos = false;
  $this->grupoActual = array();
  $this->fuenteTitulos=10;
  $this->totales=array();
  $this->encabezarGrupos=true;
  $this->imprimirGrupos=true;
  $this->subTotales=array();
  $this->mostrarSubtotales=true;
}

function setAltoFila($parAltoFila)
{
 $this->altoFila = $parAltoFila;
}

function getAltoFila()
{
 return $this->altoFila;
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{
 switch($this->tipoFooter)
 {
 	case 0:
	 	global $anchos;
    	global $notaalpie;
		//Posici�n: a 1,7 cm del final
		$this->SetY(-15);
//		$this->Cell($this->getAnchoTabla(),0,'','T');
//		$this->Ln();
		if ($this->CurOrientation=='L')
		{
			$this->Image('pie2_ancho.jpg',10,190,278,8);
		}
		else
		{
			$this->Image('pie2.jpg',10,275,190,8);
		}
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//N�mero de p�gina
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
	break;
	case 1:
	break;
  case 2:
          $this->SetY(-23);
        	global $anchos;
        	global $notaalpie;
    		//Posici�n: a 1,7 cm del final

    		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
	break;	
 }
}

function header()
{global $header;
 global $anchos;
 global $reporte;
 global $reporte1;
 global $renglones;
  switch($this->tipoHeader)
 {
     case 0:
            $this->Image('vd_srl.jpg',10,8,65);
            $this->SetFont('Arial','B',9);
            $this->Cell(80);
            $this->Cell(110,$this->altoFila,'DOCUMENTACION DE USO INTERNO',0,8,'L');
            $this->SetFont('Arial','B',8);
            $this->Cell(110,6,$reporte,0,8,'L');
            $this->Cell(110,6,$reporte1,0,8,'L');
            $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
            $this->Ln(0);
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            $this->SetFont('Arial','B',$this->fuenteTitulos);
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }
     break;
     case 1:

            $this->Image('vd_srl.jpg',10,4,30);
            $this->SetFont('Arial','B',9);
            $this->Cell(80);
            $this->Cell(110,$this->altoFila,'DOCUMENTACION DE USO INTERNO',0,8,'L');
            $this->SetFont('Arial','B',8);
            $this->Cell(110,6,$reporte,0,8,'L');
            $this->Cell(110,6,$reporte1,0,8,'L');
            $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
            $this->Ln(0);
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            $this->SetFont('Arial','B',8);
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;
     case 2:
     break;
     
     case 3:
            $temporal=$this->altoFila;
            $this->SetFillColor(220);
            $this->RoundedRect(10, 4, $this->w-20, 8, 2, 'FD', '1234');//MARCO-CHIQUITO
            $this->SetFillColor(255);
            $this->RoundedRect(10, 12, $this->w-20, 26, 4, 'FD', '1234');//MARCO-ENCABEZADO
            $this->SetFillColor(255);
            $this->RoundedRect(10, 38, $this->w-20, $this->h-60, 4, 'FD', '1234');//MARCO-TABLA
             $this->SetFillColor(220);
             $this->RoundedRect(10, $this->h-22, $this->w-20, 8, 2, 'FD', '1234');//MARCO-CHIQUITO-abajo
            $this->Image('vd_srl.jpg',23,16,35);
            $this->SetFont('Arial','B',9);
            $this->x=10;
            $this->y=6;
            $this->Cell(186,$this->altoFila,'ORDEN DE COMPRA',0,8,'C');            
            $this->x=10;
            $this->y=16;
            $this->Cell(115);
            $this->Cell(110,$this->altoFila,'DOCUMENTACION DE USO INTERNO',0,8,'L');
            $this->SetFont('Arial','',7);
            $this->altoFila=5;
            $this->Cell(110,$this->altoFila,$reporte,0,8,'L');
            $this->Cell(110,$this->altoFila,$reporte1,0,8,'L');
            $this->Cell(110,$this->altoFila,'FECHA :'.date("d/m/Y H:i"),0,8,'L');
            $this->altoFila=$temporal;  
            $this->x=$this->x-140;
            $this->y=28;
            $this->SetFont('Arial','',6);
            $this->altoFila=2;
            $this->Cell(110,$this->altoFila,'de Vanesa Duran S.A',0,8,'C');     
            $this->Cell(110,$this->altoFila,'Miguel Del Corro 317 -C.P.:X5000KTG -CORDOBA',0,8,'C');
            $this->Cell(110,$this->altoFila,'TEL./FAX: 0351-4280100/4280200',0,8,'C');  
            $this->Cell(110,$this->altoFila,'www.vanesaduran.com',0,8,'C');         
            $this->altoFila=$temporal;          
            $this->SetFont('Arial','B',8);
            $this->Ln(8);
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            $this->SetFont('Arial','B',$this->fuenteTitulos);
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;
 }

}

function encabezarTabla()
{
 global $header;
 global $anchos;
 for($i=0;$i<count($header);$i++)
   if(!in_array($header[$i],$this->camposHidden))
     $this->Cell($anchos[$i],$this->altoFila,$header[$i],1,0,'C');
 $this->Ln();
}

function LoadData($qrystr)

{ global $link;
  global $c_database;
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $err=mysql_error();
  if($err<>'')
    echo"$qrystr<br>$err";
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

//Tabla simple
function BasicTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    global $subtotal;
    if(!$subtotal)
      $subtotal=array();
    //Datos
    $this->totales=array();
    foreach($data as $row)
    {
       $this->OnEnterRow($row);
       // ****************** Manejo de Cambio de Grupos *************************** //
       if($this->usarGrupos)
        {
         $col_cambiadas=$this->CambioGrupo($row);
         if(sizeof($col_cambiadas)>0)
          {
           //Lanzar eventoOnGroupChange.
           $this->OnGroupChange($col_cambiadas);

           $header_si_temp=$this->header_si;
           $this->header_si=0;
           if($this->mostrarSubtotales)
            {
              if($vez++>0)//Para que no imprima subtotales en el inicio del informe.
               {
                $col_cambiadas_inv=array_reverse($col_cambiadas,true);
                foreach($col_cambiadas_inv as $col_cambiada)
                 {
                  if(sizeof($subtotal[$header[$col_cambiada]])<>0)
                   {
                    $this->MultiCell(100,$this->altoFila,'Subtotales ' . $header[$col_cambiada],0,'L');
                    $this->Ln();
                    $this->imprimirSubtotal($header[$col_cambiada]);
                   }
                 }
               }
             }

           if($this->imprimirGrupos)
            {
             foreach($col_cambiadas as $col_cambiada) //Si Cambia el grupo, mostrar una celda del ancho de la tabla.
              {
               $this->Ln();
               $this->MultiCell($this->getAnchoTabla(),$this->altoFila,$header[$col_cambiada] . ": " . $this->grupoActual[$col_cambiada],1,'C');
              }
             $this->Ln();
            }
           if($this->encabezarGrupos)
            {
             $font=$this->FontFamily;
             $est=$this->FontStyle;
             $tam=$this->FontSizePt;
             $this->SetFont('Arial','B',$this->fuenteTitulos);
             $this->encabezarTabla();
             $this->SetFont($font,$est,$tam);
            }
           $this->header_si=$header_si_temp;
          }
        }
       // ****************** Fin Manejo de Cambio de Grupos *************************** //

        $i=0;
        $paginaoriginal = $this->page;
        $max_renglones = 0;
        $columnas = array();
        foreach($row as $col)
            {if ($tot==1) {
             switch ($total[$i]) {
                    case 1:
                          $this->totales[$i]=$this->totales[$i]+$col;
                    break;
                    case 2:
                          $this->totales[$i]=$this->totales[$i]+1;
                    break;
                    case 3:
                          $this->totales[$i]=$col;
                    break;
                    case 4:
                          $this->totales[$i]=$this->totales[$i]+$col;
                          $col=$this->totales[$i];
                    break;
                                   }
                             }

             // ******** C�lculo de subtotales *********** //
             //Cada elemento de $subtotal es un array con banderas de totalizaci�n de columnas.
             foreach ($subtotal as $colagrupable=>$sub)
              {
               switch ($sub[$i])
                {
                  case 1:
                        $this->subTotales[$colagrupable][$i]=number_format($this->subTotales[$colagrupable][$i]+$col,2, '.', '');
                  break;
                  case 2:
                        $this->subTotales[$colagrupable][$i]=$this->subTotales[$colagrupable][$i]+1;
                  break;
                  case 3:
                        $this->subTotales[$colagrupable][$i]=$col;
                  break;
                  case 4:
                        $this->subTotales[$colagrupable][$i]=$this->subTotales[$colagrupable][$i]+$col;
                        $col=$this->subTotales[$colagrupable][$i];
                  break;
                }
              }
             // ****** Fin C�lculo de subtotales ********* //

             $yoriginal = $this->y;
             //Parte a una celda en renglones que se almacenan en cada elemento de la matriz columnas.
             if(!in_array($header[$i],$this->camposHidden))
               $columnas[$i]=$this->ParteCell($anchos[$i],$col);
             //var_dump($columnas);exit;
             //Almaceno la cantidad m�xima de renglones en la que se haya desglosado una celda.
             if($max_renglones < sizeof($columnas[$i]))
               $max_renglones = sizeof($columnas[$i]);
             if($max_renglones > $this->MaxRengFila and $this->MaxRengFila > 0 )
               $max_renglones = $this->MaxRengFila;

             $i=$i+1;
            }
        //Ya se construy� la fila, ahora recorro las columnas imprimiendo de a un rengl�n a la vez.

        //Primero me fijo cu�ntos renglones entran en cada p�gina si es que hay un salto de p�gina.
        $renglones_p1 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 1.
        $renglones_p2 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 2 si hay salto de p�gina.
        $yfinal = $this->y;
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          if($yfinal + $this->altoFila < $this->PageBreakTrigger)
            $renglones_p1++;
          else
            {
             $renglones_p2 = $max_renglones - $renglones_p1;
             break;
            }
          $yfinal += $this->altoFila;
         }

        //Ahora imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //S�lo los de la p�gina 1.
        $xact = $this->lMargin;
        for($colu=0;$colu<sizeof($row);$colu++)
         {
          if(!in_array($header[$colu],$this->camposHidden))
           {
            $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$this->y) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p1))* $this->k,'S'));
            $xact += $anchos[$colu];
           }
         }

        //Imprimo un registro de la BD en los renglones que se requieran.
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          //Imprimo todas las columnas del rengl�n $reng del registro actual.
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            if(!in_array($header[$colu],$this->camposHidden))
             {
              $rengtemp = $columnas[$colu];// echo "<br>".var_dump($rengtemp);
              $this->MultiCell($anchos[$colu],$this->altoFila,$rengtemp[$reng],0,$alig[$colu]);
             }
           }
          $this->Ln();
         }

        //Imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //Los de la p�gina 2, si hubo salto de p�gina.
        $xact = $this->lMargin;
        if($renglones_p2>0)
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            if(!in_array($header[$colu],$this->camposHidden))
             {
              $ydesde = $this->y - $renglones_p2 * $this->altoFila;//Le resto el alto de los renglones de la primera fila de celdas, ya que ya fueron impresas y se les hizo Ln() en el ciclo de arriba.
              $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$ydesde) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p2))* $this->k,'S'));
              $xact += $anchos[$colu];
             }
           }

    }
    if($this->usarGrupos)
        {
         foreach($this->cambiaEnGrupo as $agrupable)
          {
           $colAgrupable = array_search($agrupable,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
           //Si el contenido de la columna agrupable de la fila actual cambi� respecto al �ltimo grupo registrado, mostrar una celda todo a lo ancho.
           if($this->grupoActual[$colAgrupable] <> $parArrFila[$colAgrupable])
            {
             $this->grupoActual[$colAgrupable] = $parArrFila[$colAgrupable];
             $col_cambiadas[]=$colAgrupable;
            }
          }
         //var_dump($col_cambiadas);
         $header_si_temp=$this->header_si;
         $this->header_si=0;
          if($this->mostrarSubtotales)
            {
             if($vez++>0)//Para que no imprima subtotales en el inicio del informe.
               {
                $col_cambiadas_inv=array_reverse($col_cambiadas,true);
                foreach($col_cambiadas_inv as $col_cambiada)
                {
                 if(sizeof($subtotal[$header[$col_cambiada]])<>0)
                   {
                    $this->MultiCell(100,$this->altoFila,'Subtotales ' . $header[$col_cambiada],0,'L');
                    $this->Ln();
                    $this->imprimirSubtotal($header[$col_cambiada]);
                   }
                }
              }
            }
         $this->header_si=$header_si_temp;
        }
    if ($tot==1)
    { for($i=0;$i<count($header);$i++)
      {
        if(!in_array($header[$i],$this->camposHidden))
         {
          switch ($total[$i]) {
                      case 1:
                            $this->Cell($anchos[$i],$this->altoFila,$this->totales[$i],1,0,$alig[$i]);
                      break;
                      case 2:
                            $this->Cell($anchos[$i],$this->altoFila,$this->totales[$i],1,0,$alig[$i]);
                      break;
                      case 4:
                            $this->Cell($anchos[$i],$this->altoFila,$col,1,0,$alig[$i]);
                      break;
                      default:
                            $this->Cell($anchos[$i],$this->altoFila,$total[$i],1,0,$alig[$i]);
                      break;
                                 }
         }
       }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //datos
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=1;
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
         }
     $this->Ln();
     $fill=!$fill;
    }
}

function CambioGrupo($parArrFila)
{
 global $header;
 $col_cambiadas=array();

 foreach($this->cambiaEnGrupo as $agrupable)
  {
   $colAgrupable = array_search($agrupable,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
   //Si el contenido de la columna agrupable de la fila actual cambi� respecto al �ltimo grupo registrado, mostrar una celda todo a lo ancho.
   if($this->grupoActual[$colAgrupable] <> $parArrFila[$colAgrupable])
    {
     $this->grupoActual[$colAgrupable] = $parArrFila[$colAgrupable];
     $col_cambiadas[]=$colAgrupable;

    }
  }
 return $col_cambiadas;
}
function getAnchoTabla()
{
 global $header;
 global $anchos;
 $ancho=array_sum($anchos);
 foreach ($this->camposHidden as $co)
  {
   $col = array_search($co,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
   $ancho -= $anchos[$col];
  }
 return $ancho;
}

function imprimirSubtotal($colCambiada)
 {
  global $header;
  global $anchos;
  global $alig;
  global $subtotal;
  if(!$subtotal)
      $subtotal=array();

  for($i=0;$i<count($header);$i++)
   {
    if(!in_array($header[$i],$this->camposHidden))
     {
      switch ($subtotal[$colCambiada][$i])
       {
        case 1:
              $this->Cell($anchos[$i],$this->altoFila,$this->subTotales[$colCambiada][$i],1,0,$alig[$i]);
        break;
        case 2:
              $this->Cell($anchos[$i],$this->altoFila,$this->subTotales[$colCambiada][$i],1,0,$alig[$i]);
        break;
        case 4:
              $this->Cell($anchos[$i],$this->altoFila,$col,1,0,$alig[$i]);
        break;
        default:
              $this->Cell($anchos[$i],$this->altoFila,$subtotal[$colCambiada][$i],1,0,$alig[$i]);
        break;
       }
     }
    unset($this->subTotales[$colCambiada][$i]);
   }
  $this->Ln();
 }
function RoundedRect($x, $y, $w, $h, $r, $style = '', $angle = '1234')
{
	$k = $this->k;
	$hp = $this->h;
	if($style=='F')
		$op='f';
	elseif($style=='FD' or $style=='DF')
		$op='B';
	else
		$op='S';
	$MyArc = 4/3 * (sqrt(2) - 1);
	$this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));

	$xc = $x+$w-$r;
	$yc = $y+$r;
	$this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));
	if (strpos($angle, '2')===false)
		$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k,($hp-$y)*$k ));
	else
		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

	$xc = $x+$w-$r;
	$yc = $y+$h-$r;
	$this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
	if (strpos($angle, '3')===false)
		$this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-($y+$h))*$k));
	else
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

	$xc = $x+$r;
	$yc = $y+$h-$r;
	$this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
	if (strpos($angle, '4')===false)
		$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-($y+$h))*$k));
	else
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

	$xc = $x+$r ;
	$yc = $y+$r;
	$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
	if (strpos($angle, '1')===false)
	{
		$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$y)*$k ));
		$this->_out(sprintf('%.2f %.2f l',($x+$r)*$k,($hp-$y)*$k ));
	}
	else
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
	$this->_out($op);
}

function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
{
	$h = $this->h;
	$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
		$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
}
//************************** M�todos Abstractos *************************************
//M�todo Abstracto a implementar en la clase hija.
function OnEnterMultiCell(&$contenido,&$columna,&$color_fondo,&$color_texto)
 {
 }

//M�todo Abstracto a implementar en la clase hija.
function OnEnterRow(&$parFila)
 {
 }

//M�todo Abstracto a implementar en la clase hija.
function OnGroupChange(&$col_cambiadas)
 {

 }

}//Fin Clase.
?>
