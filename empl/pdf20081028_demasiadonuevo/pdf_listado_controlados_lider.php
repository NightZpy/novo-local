<?php
//************************* INICIO *********************************
include ("../../conexion.php");
include("pdf_set5.php");
//parametros (anular cuando se termina la prueba.)
//$usuario='NICOLAS';

/* Parámetros de Entrada Permitidos.
$fecha_d_    La fecha desde para filtrar el listado.
$fecha_h_    La fecha hasta para filtrar el listado.
$estado      El estado de la reserva para filtrar el Listado.
$agencia     La Agencia para filtrar el listado.
$vendedor    El Vendedor para filtrar el listado.
$orden       El campo para ordenar de modo ASC. [res.id_lider|est.estado_descrip]
$orden1      El campo para ordenar de modo ASC. [est.estado_descrip|res.id_lider]
*/

$qrystr = " INSERT INTO reporte (clave_ses,ref1,ref2,date1)
                   SELECT '$sesion',ped.id_lider,ped.id_pedidos,ped.fecha_fin_control
                   FROM pedido AS ped
                   WHERE ped.det=1 ";

if (isset($fecha_d))
  $qrystr = $qrystr." AND ped.fecha_fin_control >= '$fecha_d' ";
if (isset($fecha_h))
  $qrystr = $qrystr." AND ped.fecha_fin_control < '$fecha_h' ";

//$qrystr = $qrystr." GROUP BY ulid.id_distrib,ped.id_lider
//                    ORDER BY ulid.id_distrib,ped.id_lider ";

$qry = mysql_db_query($c_database,$qrystr,$link);

//echo $qrystr;
//exit;

$header=array('Distribuidor','Lider','Pedido','Fecha'); //encabezados de columnas (en todos)
$anchos=array(75,75,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','R','L'); //L,R,C
$total=array('TOTAL',2,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot


$nota="<br>$row[texto]<br>
<B>Generó:  <U>$usuario</U></B> <br>
Generador automática de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";
$reporte="Reporte: Listado de Pedidos Controlados desde $fecha_d hasta $fecha_h";//nombre del reporte (en todos)
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$pdf->SetFont('Arial','',10);
$pdf->header_si=1;
$pdf->AddPage();
$qrystr = "SELECT concat(concat('(',udis.id_distrib,')',udis.apellido,' ',udis.nombres),';',concat('(',ulid.id_distrib,')',ulid.apellido,' ',ulid.nombres),';',ref2,';',date1) as c
           FROM reporte
             INNER JOIN usuario AS ulid ON reporte.ref1 = ulid.cod_us
             INNER JOIN usuario AS udis ON ulid.id_distrib = udis.cod_us
             WHERE reporte.clave_ses='$sesion'
             ORDER BY ulid.id_distrib,ulid.id_lider ASC";

//echo $qrystr;
//exit;
$data=$pdf->LoadData($qrystr);


// ----------- borramos recopilacion de datos ----------

$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);

// ----------------------------------------

$pdf->SetFont('Arial','B',8);
$pdf->BasicTable($header,$data,0);
$pdf->SetFont('Arial','',8);

$pdf->Cell(190,0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln(2);
$pdf->SetFont('Arial','',10);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln(15);
$pdf->WriteHTML($nota);
$pdf->Output();
?>
