<?php
include ("../../conexion.php");
include("pdf_set5.php");
//Recopilacion de Datos
$sesion=crear_clave_sesion();
$alto=4;

/*$nota="<br><B><u>Referencia:</u></B>
Mod. Por: (Pedido Modificado por)";*/
$header=array("Fecha Fin Arm.","Mod. Por","Armador(a)","ID Pedido","ID Distrib.","ID Lider","ID Vendedor","ID vd","Cant. Ped.","Stock Disp."); //encabezados de columnas (en todos)
$anchos=array(30,25,50,20,25,25,25,30,20,20); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','L','L','L','L','L','L','L','L'); //L,R,C
$total=array('Total',2); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Reporte: Unidades Mal Modificadas en Armado.";//nombre del reporte (en todos)
$reporte1="Entre $fecha_desde y $fecha_hasta";
$notaalpie="  Documentacion exclusiva de AREA ARMADO.!!"; //nota al pie
// aca van los select del load data
$qrystr = "	SELECT concat(a.fecha,';',p.us_ini,';',ar.nyape,';',cp.id_pedido,';',cp.id_distrib,';',cp.id_lider,';',cp.id_vendedor,';',cp.id_vd,';',cp.cantidad_orig,';',(pr.stk4+pr.stk1)) AS c
									FROM comp_pedidos AS cp
											INNER JOIN productos AS pr ON pr.id_vd=cp.id_vd
											INNER JOIN armados AS a ON cp.id_pedido = a.id_pedido AND cp.id_vendedor = a.id_vendedor
											INNER JOIN armadoras AS ar ON a.ini = ar.ini
											INNER JOIN pedido AS p ON p.id_pedidos=cp.id_pedido
									WHERE a.fecha >= '$fecha_desde'
											AND a.fecha < '$fecha_hasta'
											AND cp.id_distrib = '$id_distrib'
											AND cp.cantidad < cp.cantidad_orig
											AND cp.cantidad_orig > 0
											AND (pr.stk4+pr.stk1)>0
									ORDER BY a.fecha";
	 //  echo$qrystr;exit;

// ---------------- fin variables ---------
//Iniciando PDF
$pdf=new PDF("L");
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$data=$pdf->LoadData($qrystr);
//print_r($data);


$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
//$pdf->lMargin=50;
$pdf->BasicTable($header,$data,0);
//$pdf->ImprovedTable($header,$data);
//$pdf->FancyTable($header,$data);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
?>
