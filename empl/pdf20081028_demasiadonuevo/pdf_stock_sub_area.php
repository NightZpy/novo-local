<?php
include ("../../conexion.php");
include("pdf_set5.php");
require_once "../../../clases/interfaz/classes_listas.php";
$sesion=crear_clave_sesion();
switch ($orden)
{
 case 1:
 	$ord="u.apellido,u.nombres ASC";
 break;
 case 2:
    $ord="u.apellido,u.nombres DESC";
 break;
 case 3:
    $ord="u.fec_last_acc ASC";
 break;
 case 4:
    $ord="u.fec_last_acc DESC";
 break;
 case 5:
 	$ord="u1.apellido,u1.nombres ASC";
 break;
 case 6:
    $ord="u1.apellido,u1.nombres DESC";
 break;
}
switch($datos)
{
	case 'C':
		$cond='AND';
		$qrystr1 = " AND (NOT ISNULL(u.apellido) $cond u.apellido<>'' $cond NOT ISNULL(u.nombres) $cond u.nombres<>''
				     $cond NOT ISNULL(u.direccion) $cond u.direccion<>'' $cond u.nro_doc<>0 $cond NOT ISNULL(u.nro_doc) 
					 $cond NOT ISNULL(u.fechanac) $cond u.fechanac<>'0000-00-00' $cond u.cant_acc<>0 $cond u.cod_pais<>0 
					 $cond u.cod_pais<>'' $cond u.localidad<>'' $cond NOT ISNULL(u.localidad) $cond NOT ISNULL(u.provincia) $cond u.provincia<>'')
				 ORDER BY $ord "; 
		$ndatos='Completos';
	break;
	case 'I':
		$cond='OR';
		$qrystr1 = " AND (ISNULL(u.apellido) $cond u.apellido='' $cond ISNULL(u.nombres) $cond u.nombres=''
					 $cond ISNULL(u.direccion) $cond u.direccion='' $cond u.nro_doc=0 $cond ISNULL(u.nro_doc) 
					 $cond ISNULL(u.fechanac) $cond u.fechanac='0000-00-00' $cond u.cant_acc=0 $cond u.cod_pais=0 
					 $cond u.cod_pais='' $cond u.localidad='' $cond ISNULL(u.localidad) $cond ISNULL(u.provincia) $cond u.provincia='')
				 ORDER BY $ord"; 
		$ndatos='Incompletos';
	break;
}
$qrystr = "	SELECT CONCAT(CONCAT(u1.apellido,' ',u1.nombres),';',IFNULL(u.cod_us,''),';',IFNULL(u.apellido,''),';',IFNULL(u.nombres,''),';',u.fec_last_acc) AS c
			FROM usuario AS u
			INNER JOIN usuario AS u1 ON u.id_distrib = u1.cod_us
			WHERE u.fec_last_acc >= DATE_SUB(curdate(),INTERVAL $nmes MONTH)
				AND u.cateusu='R' $qrystr1";
switch($salida)
{
	case 'PDF':
		$pdf=new PDF();
		$pdf->Open();
		$pdf->AliasNbPages();
		$nota="<B>Gener�:  <U>$usuario</U></B> <br>
		Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
		www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
		soporte@iddelsur.com.ar</A>,<br>";
		$header=array('Distribuidor','C�digo','Apellido','Nombres','Ultimo Acceso');
		$anchos=array(60,30,45,45,30);
		$alig=array('L','L','L','L','L');
		$total=array('Cantidad Usuarios',2,'','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
		$reporte="Usuarios con datos $ndatos";
		$reporte1="que ingresaron al sistema en los ultimos $nmes meses";
		$notaalpie=" INFORMACION CONFIDENCIAL!!"; //nota al pie
		$pdf->SetTitle($reporte);
		$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
		$pdf->Setcreator('IDDelSur para VD');
		$pdf->AddPage('L');
		$data=$pdf->LoadData($qrystr);
		$pdf->SetFont('Arial','',8);
		$pdf->BasicTable($header,$data,1);
		$pdf->Ln();
		$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
		$pdf->Ln();
		$pdf->WriteHTML($nota);
		$pdf->Output();
	break;
	case 'EXCEL':
		$lista = new Listas();
		$lista->titulos=array('Distribuidor','C�digo','Apellido','Nombres','Ultimo Acceso');
		//$lista->debug=1;
		$data=$lista->LoadData($qrystr);
		$lista->BasicTable($data);
		$lista->OutPut($salida);
	break;
}
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM  reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// -----------------------------------------------------
?>