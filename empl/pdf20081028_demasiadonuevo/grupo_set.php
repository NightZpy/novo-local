<?php
/*
ESTA FUNCION INSERTA ELEMENTOS DE UN GRUPO EN LA TABLA GRUPOS_MANEJO
PARAMETROS: $SESION=SESION
            $GRUPO=ID_GRUPO
*/
function inserta_elementos($sesion,$grupo)
{
    global $link;
    global $c_database;
    $qrystr0 = "
                INSERT INTO grupos_manejo (clave_ses,grupo,elemento)
                SELECT '$sesion',id_grupo,elemento
                FROM grupos_lista
                WHERE id_grupo = '$grupo'
               ";
    $qry0= mysql_db_query($c_database,$qrystr0 ,$link);
}
/*
ESTA FUNCION SELECIONA ELEMENTOS DE UN GRUPO Y TIENE 2 SALIDAS.
PARAMETROS: $SESION=SESION
            $GRUPO=ID_GRUPO
            $SALIDA: 1 LA VARIABLE DE RETURN ES UNA LISTA DE ELEMENTOS SEPARADOS POR ','
                     2 LA VARIABLE DE RETURN ES UN ARRAY DE ELEMENTOS
            $INCLUIR: 0 CONCATENA EL "NOT IN" AL COMIENZO DE LA VARIABLE DE SALIDA
                      1 CONCATENA EL "IN" AL COMIENZO DE LA VARIABLE DE SALIDA
*/
function get_grupos($sesion,$grupo,$salida=1,$incluir)
{
    global $link;
    global $c_database;
    $qrystr1 = "
                SELECT elemento
                FROM grupos_manejo
                WHERE grupo = '$grupo' AND clave_ses = '$sesion'
               ";
            //echo $qrystr1;
    $qry1= mysql_db_query($c_database,$qrystr1 ,$link);

    if($salida==1)
    {
        $separa = "";
        while ($row = mysql_fetch_array($qry1))
        {
            $distrib .= $separa . $row[0];
            $separa = "','";
        }

        if($distrib<>'')
          {
              switch($incluir)
              {
                  case 0:
                         return "NOT IN('" . $distrib . "')";
                  break;
                  case 1:
                         return "IN('" . $distrib . "')";
                  break;
              }
          }
    }
    else if($salida==2)
         {
             $distrib=array();
             $i=0;
             while ($row = mysql_fetch_array($qry1))
             {
                 $distrib[$i]=$row[0];
                 $i++;
             }
          return $distrib;
         }
}
/*
ESTA FUNCION ELIMINA LOS REGISTROS INGRESADOS
PARAMETROS: $SESION=SESION
            $GRUPO=ID_GRUPO
*/
function borra_seleccion($sesion,$grupo)
{
    global $link;
    global $c_database;
    $qrystr0 = " DELETE FROM grupos_manejo WHERE clave_ses = '$sesion' AND grupo = '$grupo'";
    $qry0 = mysql_db_query($c_database,$qrystr0,$link);
}
/*
ESTA FUNCION ARMA UNA LISTA DE ELEMENTOS DESDE UNA LISTA CON CHECKS
PARAMETROS: $CANTIDAD=CANTIDAD
*/
function borra_checked($cantidad,$res)
{
    global $link;
    global $c_database;
    for ($i = 1; $i <= $cantidad; $i++)
        {
            if ($res <> "")
               {
                   $qrystr0 = " DELETE FROM grupos_lista WHERE (grupo = '$grupo' AND elemento = '$res')";
                   $qry0 = mysql_db_query($c_database,$qrystr0,$link);
               }
        }
}
/*
ESTA FUNCION DEVUELVE LA CANTIDAD DE ELEMENTOS DE UN GRUPO
PARAMETROS: $GRUPO
*/
function cuenta_elementos($grupo)
{
    global $link;
    global $c_database;
    $q = "
           SELECT COUNT(*) AS paq2,g.nombre_grupo AS paq
           FROM grupos_lista AS gl
           INNER JOIN grupos AS g ON g.id_grupo = gl.id_grupo
           WHERE gl.id_grupo = '$grupo'
           GROUP BY gl.id_grupo
         ";
    $qr = mysql_db_query($c_database,$q,$link);
    $r = mysql_fetch_array($qr);
    $salida = "Grupo: " . $r[paq];
return $salida;
}
/*
ESTA FUNCION DEVUELVE UN ARRAY DE ELEMENTOS DE UN GRUPO DE GRUPO
PARAMETROS: $GRUPO
            $SALIDA: 1 LA VARIABLE DE RETURN ES UNA LISTA DE ELEMENTOS SEPARADOS POR ','
                     2 LA VARIABLE DE RETURN ES UN ARRAY DE ELEMENTOS
            $INCLUIR: 0 CONCATENA EL "NOT IN" AL COMIENZO DE LA VARIABLE DE SALIDA
                      1 CONCATENA EL "IN" AL COMIENZO DE LA VARIABLE DE SALIDA
*/
function elementos_gr_gr($sesion,$grupo,$salida=1,$incluir,$campo1,$campo2)
{
 global $link;
 global $c_database;
 if($salida==2)
    $elementos=array();
 else
    $elementos="";
 $separa="";
 $i=0;
 $q = " SELECT elemento
        FROM grupos_lista
        WHERE id_grupo = '$grupo'
        ORDER BY elemento
      ";
 $qr = mysql_db_query($c_database,$q,$link);
 while($r = mysql_fetch_array($qr))
 {
  $qrystr = " SELECT elemento
              FROM grupos_lista
              WHERE id_grupo = '$r[elemento]'
              ORDER BY elemento
            ";
  $qry = mysql_db_query($c_database,$qrystr,$link);               
  while($row = mysql_fetch_array($qry))
  {
   switch($salida)
   {
    case 1:
      $elementos .= $separa . $row[0];
      $separa = "','";
    break;
    case 2:
      $elementos[$i]=$row[0];
      $i++;
    break;
    case 3:
      $qrystrrep = " INSERT INTO reporte (clave_ses,ref1,$campo1,$campo2) VALUES('$sesion','$row[0]',$r[elemento],$grupo)";
      $qryrep = mysql_db_query($c_database,$qrystrrep,$link);               
    break;
   }     
  }
 }
if($salida==2)
  return $elementos;
else
  {  
   if($elementos<>'')
     {
      switch($incluir)
      {
       case 0:
             return "NOT IN('" . $elementos . "')";
       break;
       case 1:
             return "IN('" . $elementos . "')";
       break;
      }
     } 
  }
}
/*
ESTA FUNCION DEVUELVE LA CANTIDAD DE ELEMENTOS DE UN GRUPO
PARAMETROS: $GRUPO
*/
function verifica_elementos1()
{
    global $link;
    global $c_database;
    $q = " SELECT gl1.elemento,gl1.id_grupo,gl2.id_grupo,gru1.nombre_grupo,gru2.nombre_grupo,gru3.nombre_grupo
           FROM grupos_lista AS gl1
           INNER JOIN grupos AS gru1 ON gl1.id_grupo = gru1.id_grupo
           INNER JOIN grupos AS gru2 ON gl2.id_grupo = gru2.id_grupo
           INNER JOIN grupos AS gru3 ON gru3.id_grupo = 49
           INNER JOIN grupos_lista AS gl2 ON gl1.id_grupo = gl2.elemento AND gl2.id_grupo IN ( 48, 50 )
           WHERE gl1.elemento NOT IN ( SELECT gl3.elemento 
                                       FROM grupos_lista AS gl3 
                                       INNER JOIN grupos_lista AS gl4 ON gl3.id_grupo = gl4.elemento AND gl4.id_grupo IN ( 49 ))
         ";
    $qr = mysql_db_query($c_database,$q,$link);
    $salida="";
    while($r = mysql_fetch_array($qr))
    {
     $salida .= "El cliente: " . $r[0] . " se encuentra en (" . $r[1] . ") " . $r[3] . " perteneciente al grupo (" . $r[2] . ") " . $r[4] . " y no existe en ning�n grupo del grupo (49) " . $r[5] . ". <br><br>";
    }
    
return $salida;
}
/*
ESTA FUNCION DEVUELVE LA CANTIDAD DE ELEMENTOS DE UN GRUPO
PARAMETROS: $GRUPO
*/
function verifica_elementos2()
{
    global $link;
    global $c_database;
    $q = " SELECT gl1.elemento,gl1.id_grupo,gl2.id_grupo,gru1.nombre_grupo,gru2.nombre_grupo,gru3.nombre_grupo,gru4.nombre_grupo
           FROM grupos_lista AS gl1
           INNER JOIN grupos AS gru1 ON gl1.id_grupo = gru1.id_grupo
           INNER JOIN grupos AS gru2 ON gl2.id_grupo = gru2.id_grupo
           INNER JOIN grupos AS gru3 ON gru3.id_grupo = 48
           INNER JOIN grupos AS gru4 ON gru4.id_grupo = 50
           INNER JOIN grupos_lista AS gl2 ON gl1.id_grupo = gl2.elemento AND gl2.id_grupo IN ( 49 )
           WHERE gl1.elemento NOT IN ( SELECT gl3.elemento 
                                       FROM grupos_lista AS gl3 
                                       INNER JOIN grupos_lista AS gl4 ON gl3.id_grupo = gl4.elemento AND gl4.id_grupo IN ( 48,50 ))
         ";
    $qr = mysql_db_query($c_database,$q,$link);
    $salida="";
    while($r = mysql_fetch_array($qr))
    {
     $salida .= "El cliente: " . $r[0] . " se encuentra en (" . $r[1] . ") " . $r[3] . " perteneciente al grupo (" . $r[2] . ") " . $r[4] . " y no existe en ning�n grupo del grupo (48) " . $r[5] . " o del (50) " . $r[6] . ". <br><br>";
    }
    
return $salida;
}
function verifica_elementos3()
{
    global $link;
    global $c_database;
    $q = " SELECT gl1.elemento,gl1.id_grupo,gl2.id_grupo,gru1.nombre_grupo,gru2.nombre_grupo,gru3.nombre_grupo,gru4.nombre_grupo,count(*) AS cuenta
           FROM grupos_lista AS gl1
           INNER JOIN grupos_lista AS gl2 ON gl1.id_grupo = gl2.elemento AND gl2.id_grupo IN ( 48, 50 )
           INNER JOIN grupos AS gru1 ON gl1.id_grupo = gru1.id_grupo
           INNER JOIN grupos AS gru2 ON gl2.id_grupo = gru2.id_grupo
           INNER JOIN grupos AS gru3 ON gru3.id_grupo = 48
           INNER JOIN grupos AS gru4 ON gru4.id_grupo = 50
           GROUP BY gl1.elemento
           HAVING cuenta >1
         ";
    $qr = mysql_db_query($c_database,$q,$link);
    $salida="";
    while($r = mysql_fetch_array($qr))
    {
     $vez=$r[cuenta]-1;
     $salida .= "El cliente: " . $r[0] . " se encuentra en (" . $r[1] . ") " . $r[3] . " perteneciente al grupo (" . $r[2] . ") " . $r[4] . " y se repite " . $vez ." vez/veces en alg�n grupo del grupo (48) " . $r[5] . " o del (50) " . $r[6] . ". <br><br>";
    }
    
return $salida;
}
function verifica_elementos4()
{
    global $link;
    global $c_database;
    $q = " SELECT gl1.elemento,gl1.id_grupo,gl2.id_grupo,gru1.nombre_grupo,gru2.nombre_grupo,gru3.nombre_grupo,count(*) AS cuenta
           FROM grupos_lista AS gl1
           INNER JOIN grupos_lista AS gl2 ON gl1.id_grupo = gl2.elemento AND gl2.id_grupo IN ( 49 )
           INNER JOIN grupos AS gru1 ON gl1.id_grupo = gru1.id_grupo
           INNER JOIN grupos AS gru2 ON gl2.id_grupo = gru2.id_grupo
           INNER JOIN grupos AS gru3 ON gru3.id_grupo = 49
           GROUP BY gl1.elemento
           HAVING cuenta >1
         ";
    $qr = mysql_db_query($c_database,$q,$link);
    $salida="";
    while($r = mysql_fetch_array($qr))
    {
     $vez=$r[cuenta]-1;
     $salida .= "El cliente: " . $r[0] . " se encuentra en (" . $r[1] . ") " . $r[3] . " perteneciente al grupo (" . $r[2] . ") " . $r[4] . " y se repite " . $vez ." vez/veces en alg�n grupo del grupo (49) " . $r[5] . ". <br><br>";
    }
    
return $salida;
}
?>
