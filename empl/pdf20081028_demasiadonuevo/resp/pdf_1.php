<?php
include ("../../conexion.php");

define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;

function PDF($orientation='P',$unit='mm',$format='A4')
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{   global $anchos;
    global $notaalpie;
	$this->Cell(array_sum($anchos),0,'','T');
    //Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	//Arial italic 8
	$this->SetFont('Arial','I',8);
	//N�mero de p�gina
	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
}

function header()
{global $header;
 global $anchos;
 global $reporte;
 $this->Image('joy-isologo-ngro.jpg',10,8,70);
 //Arial bold 1510
 $this->SetFont('Arial','B',9);
 //Movernos a la derecha
 $this->Cell(80);
 //T�tulo
 $this->Cell(110,7,'H.Irigoyen 31 - Ciudad de C�rdoba - CP 5000',0,8,'L');
 $this->Cell(110,7,'Tel (54) 351-4115130 351-4113795',0,8,'L');
 $this->SetFont('Arial','B',8);
 $this->Cell(110,6,$reporte,0,8,'L');
 $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
 $this->Ln(10);
 $this->SetFont('Arial','B',10);
 for($i=0;$i<count($header);$i++)
        $this->Cell($anchos[$i],7,$header[$i],1,0,'C');
    $this->Ln();

}

function LoadData($qrystr)

{ global $link;
  global $c_database;
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

//Tabla simple
function BasicTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Datos
    $totales=array();
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
            {$this->Cell($anchos[$i],6,$col,1,0,$alig[$i]);
             if (($tot==1) and ($total[$i]==1)) {$totales[$i]=$totales[$i]+$col;}
             if (($tot==1) and ($total[$i]==2)) {$totales[$i]=$totales[$i]+1;}
             if (($tot==1) and ($total[$i]==3)) {$totales[$i]=$col;}
             $i=$i+1;
            }
        $this->Ln();
        
    }
    if ($tot==1)
    { for($i=0;$i<count($header);$i++)
      {if ($total[$i]==1) {$this->Cell($anchos[$i],7,$totales[$i],1,0,$alig[$i]);}
                     else {$this->Cell($anchos[$i],7,$total[$i],1,0,$alig[$i]);}
       }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //datos
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=1;
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
         }
     $this->Ln();
     $fill=!$fill;
    }
}

}
$nota='<br><B>Obs:  <I><U>PVP.</U></I></B> (precio de venta al publico) -- <B><I><U>Bon.Rev.</U></I></B> (Comision de Revendedor) -- <B><I><U>Neto Rev.</U></I></B> (Total a pagar del Revendedor) -- <B><I><U>Sem</U></I></B> (Cantidad de Semijoyas computadas para bonificacion) -- <B><I><U>Fal</U></I></B> (Faltantes) -- Acc. (Accesorios y Otros) -- <B><I><U>Bon.Lider</U></I></B> (Comision de lider por Revendedor) <br>
** Generacion automatica de reportes de <A HREF="http://www.vanesaduran.com">www.vanesaduran.com</A>,
<BR>';
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
//instanciando... las variables
$qrystr = "SELECT concat(id_lider,';',id_vendedor,';',id_pedidos,';',neto,';',un_total) as c FROM `pedidos_defi` WHERE 1 limit 600";
$qrystr = "SELECT (sum(debe)-sum(haber)) as tot,concat(usuario.apellido,' ' ,usuario.nombres,';',(sum(debe)-sum(haber))) as c
  FROM ctacte,usuario
 WHERE id_cod_us=usuario.cod_us
 GROUP BY id_cod_us
 ORDER BY tot DESC";
$anchos=array(120,70,); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','R'); //L,R,C
$total=array('Total',1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), funciona si se llama la tabla con 1 en tot
$header=array('Titula Cta Cte','Saldo'); //encabezados de columnas (en todos)
$reporte="Reporte: Saldos en Ctas Ctes.";//nombre del reporte (en todos)
$notaalpie="     Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
// ---------------- fin variables ---------
$pdf->SetTitle($reporte);
$pdf->SetAuthor('Gar.Adm - Vanesa Duran');
$pdf->Setcreator('IDDelSur');
//carga de datos
$data=$pdf->LoadData($qrystr);
//print_r($data);
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->BasicTable($header,$data,1);
//$pdf->ImprovedTable($header,$data);
//$pdf->FancyTable($header,$data);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
?>
