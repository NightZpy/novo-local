<?php
include ("../../conexion.php");
$sesion=crear_clave_sesion();
if($id_distrib<>'TODOS')
  {
   $reporte1="Del Distribuidor $id_distrib";
   $filtro = " AND id_cod_us = '$id_distrib'" ;
  }
else
  {
   $reporte1="De Todos Los Distribuidores";
   $filtro="";
  }  
$qrystr="SELECT CONCAT(cta.fecha,';',CONCAT(u.apellido,' ',nombres),';',CONCAT('(',plan.ctacompl,') ',plan.cta_desc),';',cta.ref,';',cta.debe,';',cta.haber) AS c
         FROM ctacte AS cta
         INNER JOIN ctacte_plan AS plan ON cta.id_cta = plan.ctacompl
         INNER JOIN usuario AS u ON u.cod_us = cta.id_cod_us
         WHERE cta.t = 'M'
               AND cta.fecha >= '$fecha_desde'
               AND cta.fecha <= '$fecha_hasta'
               $filtro
         ORDER BY cta.fecha,CONCAT(u.apellido,' ',nombres),CONCAT('(',plan.ctacompl,') ',plan.cta_desc),cta.ref
        ";
      
switch($salida)
{
 case 'PDF':
      include("inc_pdf/inc_function.php");
      include("pdf_set5.php");
      $pdf=new PDF();
      $pdf->Open();
      $pdf->AliasNbPages();
      $pdf->SetTitle($reporte);
      $pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
      $pdf->Setcreator('IDDelSur para VD');
      $header=array('Fecha','Cliente','Cuenta','Movimiento','Debe','Haber'); //encabezados de columnas (en todos)
      $anchos=array(20,80,60,90,15,15); //anchos de cada celda procurar que sumen aprox 190-
      $alig=array('L','L','L','L','R','R'); //L,R,C
      $comment="TOTALES ";
      $total=array($comment,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
      $reporte="Imputaciones Por Periodo $fecha_desde a $fecha_hasta";//nombre del reporte (en todos)
      $notaalpie="Informacion Confidencial. (Exclusiva del area Direccion)"; //nota al pie
      $pdf->AddPage('L');
      $data=$pdf->LoadData($qrystr);
      $pdf->SetFont('Arial','',8);
      $pdf->BasicTable($header,$data,0);
      $pdf->WriteHTML($nota);
      $pdf->Output();
 break;
     
 case 'EXCEL':
      require_once "../../../clases/interfaz/classes_listas.php";
      $lista = new Listas();
      $lista->titulos=array('Fecha','Cliente','Cuenta','Movimiento','Debe','Haber');
      $data=$lista->LoadData($qrystr);
      $lista->BasicTable($data);
      $lista->OutPut("EXCEL");
 break;
}
?>
