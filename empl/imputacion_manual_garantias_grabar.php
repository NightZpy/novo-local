<?php
include("../conexion.php");
mysql_select_db($c_database,$link);

// Variables ------------------------
$vendedor	=utf8_decode($_POST['r']);
$tipoMov	=$_POST['t'];
$camp			=$_POST['c'];
$ref			=$_POST['ref'];
$importe	=$_POST['i'];
$noValidarSaldo= $_POST['ck'];
$u = $_POST['usuario'];
// End variables ----------------------

// Validaciones --------------------------------------------------------------------------------
if($vendedor==''){ echo "<b>Por favor seleccione un vendedor!</b>";exit;}
if($camp==0){ echo "<b>Por favor seleccione una campa&ntilde;a!</b>";exit;}
if($ref==''){ echo "<b>Por favor ingrese una referencia!</b>";exit;}
if($importe==0 || $importe<0){ echo "<b>Por favor ingrese un monto v&aacute;lido!</b>";exit;}
// End validaciones -----------------------------------------------------------------------------

// Configuraciones adicionales ------------------------------------------------------------------
if($tipoMov=='c'){
	$id_cta="2.1.1.1";
	$campoMonto="creditos";
}
else{
	$id_cta="1.2.1.1";
	$campoMonto="debitos";
}
// End configuraciones adicionales ---------------------------------------------------------------

// Consultar la estructura del vendedor ----------------------------------------------------------
$sqlEstructura="SELECT id_regional,id_distrib,id_lider FROM usuario WHERE cod_us='".$vendedor."'";
$resEstructura=mysql_query($sqlEstructura,$link);
$rowEstructura=mysql_fetch_object($resEstructura);
// End Estructura --------------------------------------------------------------------------------

// Ahora debo saber el saldo que tiene para el canje -------------------------------------------------------
$sqlSaldo="SELECT (SUM(creditos) - SUM(debitos)) AS saldo FROM ctacte_garantia WHERE id_cod_us='".$vendedor."' GROUP BY id_cod_us";//echo $sqlSaldo;
//echo $sqlSaldo;
$resSaldo=mysql_query($sqlSaldo,$link);
$rowSaldo=mysql_fetch_object($resSaldo);
$numSaldo=mysql_num_rows($resSaldo);
// End obtener Saldo ----------------------------------------------------------------------------------------

if($numSaldo>0){
	if($tipoMov=='d'){
		if($rowSaldo->saldo < $importe && $noValidarSaldo==0){
			echo "<b>No se puedo realizar la imputaci&oacute;n porque el saldo disponible es insuficiente: ( ".$rowSaldo->saldo." )</b>";
			exit;
		}
	}
	// Ahora que todo es válido guardamos los datos del importe en la tabla de ctacte_garantia
	$sqlInsertMov="INSERT INTO ctacte_garantia(id_distrib, id_cod_us,id_cta,ref,".$campoMonto.",pedido,fecha,t,fecha_gene,campania)
	VALUES('".$rowEstructura->id_distrib."','".$vendedor."','".$id_cta."', '".$ref."',".$importe.",0,CURDATE(),'M',CURDATE(),".$camp.")";

	$resInsertMov=mysql_query($sqlInsertMov,$link);
	echo "<b>El movimiento se realizo correctamente!</b>";
}
else {// El vendedor no tiene registros en la tabla ctacte_garantia, no tiene saldo
		if($tipoMov=='d'){
			echo "<b>No se pudo realizar la imputaci&oacute;n porque el saldo disponible es insuficiente: ( 0 )</b>";
		}else{
			// Ahora que todo es válido guardamos los datos del importe en la tabla de ctacte_garantia
			$sqlInsertMov="INSERT INTO ctacte_garantia(id_distrib, id_cod_us,id_cta,ref,".$campoMonto.",pedido,fecha,t,fecha_gene,campania)
			VALUES('".$rowEstructura->id_distrib."','".$vendedor."','".$id_cta."', '".$ref."',".$importe.",0,CURDATE(),'M',CURDATE(),".$camp.")";

			$resInsertMov=mysql_query($sqlInsertMov,$link);
			echo "<b>El movimiento se realizo correctamente!</b>";
		}
}
$detalle="Mov. Tipo:$campoMonto | Emprenv.: $vendedor | Ref.: $ref | Importe: $importe";
// Auditar
$sqlAuditar="INSERT INTO auditoria VALUES ('$u',NOW(),'CTA CTE','Inputacion Manual','$detalle')";
$resAuditar=mysql_query($sqlAuditar,$link);
?>
