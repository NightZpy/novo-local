<?php
/*
 * reprocesador_manual_pedido_en_cuotas_salon.php
 *
 * Copyright 2014 Javier Rios <javier.rios@joyasvanesaduran.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
?>
<div id="msg" class="alert alert-info alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	<i class="fa fa-warning fa-2x"></i><p id="mensaje"></p>
</div>
<div class="container">
	<div class="row">
			<form id="form-procesar" class="form-horizontal">
				<input type="hidden" name="usuario" value="<?php echo $usuario?>">
			<fieldset>

			<!-- Form Name -->
			<legend>Pedidos en Cuotas de Sal�n de Vtas.</legend>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="txtpedidoID">N�mero de Pedido:</label>
				<div class="col-md-3">
				<input id="txtpedidoID" name="txtpedidoID" type="text" placeholder="ID PEDIDO" class="form-control input-md" required="true" autofocus="true">
				<span class="help-block">Por favor ingrese aqu� el ID del pedido del L�der que desea reprocesar.</span>
				</div>
			</div>

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="reprocesar"></label>
				<div class="col-md-4">
					<button id="reprocesar" name="reprocesar" class="btn btn-danger">Reprocesar</button>
				</div>
			</div>

			</fieldset>
			</form>
	</div>
</div>

<script type="text/javascript">

jQuery(function(){
	jQuery("#msg").hide();

		// Proccess form
		jQuery("#form-procesar").submit(function(e){

			jQuery.ajax({
				url: 'empl/reprocesar_pedidos_cuotas_salon2.php',
				data: {pedidoID:jQuery('#txtpedidoID').val(),usuario:jQuery("input[name='usuario']").val()},
				dataType: 'html',
				type: 'POST'
			}).
			done(function (data){//alert(data);
				jQuery("#mensaje").text(data);
				jQuery("#msg").show();
			}).
			always(function(data){
			jQuery("#mensaje").text(data);
			jQuery("#msg").show();
			
			});
			return false;
		});
});//End document ready
</script>
