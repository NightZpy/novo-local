<style>.contmenu {	margin: 0 40%;width: 80%;padding: 10px;}</style>
<div class="info">
<p class="titulo"><b>INFORMACI&Oacute;N IMPORTANTE!:</b><br>
<span>
	Con esta herramienta Ud. podr&aacute; cambiar individualmente o masivamente el Distribuidor o el L&iacute;der de la/las notas de canje seleccionadas.
</span>
</p>
</div>
<form method="post" name="form_canjes" accept-charset="utf-8">
<input id="nivel" type="hidden" name="nivel" value="2" />
  <div>
    <ul>
      <li>
          <h2>Gestor de Notas de Canjes</h2>
      </li>
      <li>
        <label class="pagos" for="lider">Lider ( Ingrese Todo o Parte de C&oacute;digo ): </label>
        <div>
          <input id="revendedor" type="search" name="revendedor" required />&nbsp;
          <input id="update" type="button" value="Buscar Canjes" />
        </div>
      </li>
    </ul>
  </div>
</form>
<div class="clearfix">&nbsp;</div>
<table id="canjes"></table>
<div id="pcanje"></div>
<div id="masivo" style="display:none;" title="Cambio Masivo de Canjes">
	<h2>Por favor verifique los datos antes de guardarlos!</h2>
	<input id="usuario" type="hidden" name="usuario" value="<?php echo $usuario;?>">
<form id="frm_canjes">
<label for="distribuidor">Distribuidor Nuevo:</label>
<div>
<input id="distrib_nuevo" type="text" name="distrib_nuevo" required="true" />
</div>
<label for="distribuidor">L&iacute;der Nuevo:</label>
<div>
<input id="lider_nuevo" type="text" name="lider_nuevo" required="true" />
</div>
</form>
</div>
<div class="contmenu">
<input id="menubtn" type="button" name="menup" value="Inicio" />
</div>
<script type="text/javascript">

jQuery(function(){
// Usar solo la nueva API de jqGrid
jQuery.jgrid.no_legacy_api = true;

// Param Required ---------------------
var nivel	= jQuery("#nivel").val();
var codus = jQuery("#revendedor").val();
var usu="";
var idsNC=0;
var idRef =0;
var oldDist="";
var oldLider="";
var oldValue="";

//-------------------------------------

// Button Settings
jQuery("input#menubtn:not(button)").button({icons:{ primary: "ui-icon-home"}}).
click(function(){
	location.href="administracion.php?pagina=empl/func_emp_lista.php&usuario=<?php echo $usuario;?>&sesion=<?php echo $sesion;?>";
});

// Button update autocomplete
jQuery("#update").click(function(){
	nivel	= jQuery("#nivel").val();
  codus	= jQuery("#revendedor").val();
	jQuery("#canjes").jqGrid('setGridParam',{postData:{ n:nivel,codigo:codus}}).trigger('reloadGrid');
});

// Table Settings (Canjes)
jQuery("#canjes").jqGrid({
  url:"empl/consultar_canjes.php",
	datatype: "json",
   	colNames:['Pedido','Distribuidor','L&iacute;der','D&eacute;bitos','Cr&eacute;ditos','Referencia','Fecha Canje', 'Campa&ntilde;a'],
   	colModel:[
   		{name:'pedido',index:'pedido', width:110, sorttype:'int'},
			{name:'id_distrib',index:'id_distrib',width:110,editable:true,edittype:'text', editrules:{required:true}},
   		{name:'id_cod_us',index:'id_cod_us',width:110,editable:true,edittype:'text',editrules:{required:true}},
   		{name:'debitos',index:'debitos', width:100,align:"right"},
   		{name:'creditos',index:'creditos', width:100,align:"right"},
   		{name:'referencia',index:'referencia', width:110,align:"left"},
   		{name:'fecha_gene',index:'fecha_gene', width:110, align: "center",sorttype:'date', formatter:'date', datefmt:'d/m/Y'},
   		{name:'campania',index:'campania',width:175,align: "right"},
   	],
   	rowNum:50,
   	rowList:[15,25,35,45,65],
   	height: 'auto',
   	pager: '#pcanje',
   	sortname: 'campania',
    viewrecords: true,
    multiselect:true,
    sortorder: "desc",
    caption:"MOVIMIENTOS POR NOTAS DE CANJE",
    forceFit : true,
	cellEdit: true,
	cellsubmit: 'remote',
	cellurl: "empl/gestor_canjes_guardar.php",
	beforeEditCell: function(rowid,celname,value,iRow,iCol){
		//console.log("Valores Originales - Nombre: "+celname+"\nValor: "+value+"\nRow: "+iRow+"\nCol: "+iCol+"\nID: "+rowid);
		// Save prev values
		oldDist=jQuery("#canjes").getCell(rowid,"id_distrib");
		oldLider=jQuery("#canjes").getCell(rowid,"id_cod_us");
	},
	beforeSubmitCell:function(rowid,celname,value,iRow,iCol){
		//console.log("Valores Modificados - Nombre: "+celname+"\nValor: "+value+"\nRow: "+iRow+"\nCol: "+iCol+"\nID: "+rowid);
		//var usuario="<?php echo $usuario;?>";

		if(celname=="id_distrib"){
			oldValue=oldDist;
		}else{
			oldValue=oldLider;
		}
		return {old1:oldValue,usu:jQuery("#usuario").val()}
	},
	afterSubmitCell: function(serverStatus, aPostData){
		var response=serverStatus.responseText;
		//console.log("Respuesta del Server: "+response+"\nData: "+aPostData);
		jQuery.blockUI({theme:true,title:"Info",message:response,timeout:3000});
		jQuery("#canjes").jqGrid('setGridParam',{postData:{ n:nivel,codigo:oldLider}}).trigger('reloadGrid');

		return [true];

	}
});
// Pager Settings
jQuery("#canjes").jqGrid('navGrid','#pcanje',{add:false,edit:false,del:false,search:false}).
navButtonAdd('#pcanje', {
  caption: "Editar Seleccionados",
  title: "Click aqui para editar todos los canjes seleccionados",
  buttonicon: "ui-icon-pencil",
  onClickButton: function() {
				idsNC=jQuery("#canjes").jqGrid("getGridParam","selarrrow");
				idRef =jQuery(".jqgrow").first().attr("id");
				oldDist=jQuery("#canjes").getCell(idRef,"id_distrib");
				oldLider=jQuery("#canjes").getCell(idRef,"id_cod_us");
				usu=jQuery("usuario").val();
				//console.log("ids_mov: "+idsNC+"\nDistrib:"+oldDist+"\nLider:"+oldLider+"\nID:"+idRef+"\nUsu: "+usuario);

     if(idsNC.length>0){
				jQuery("#masivo").dialog({
						autoOpen: true,
						open: function(){
								jQuery("#distrib_nuevo").val(oldDist);
								jQuery("#lider_nuevo").val(oldLider);
								jQuery("#usuario").val(usu);
						},
						position: 'center',
						draggable: false,
						resizable:true,
						modal: true,
						buttons: {
							"Guardar":function(){
								jQuery.ajax({
									url: "empl/gestor_canjes_guardar.php",
									//data: { id_distrib:jQuery("#distrib_nuevo").val(),id_cod_us:jQuery("#lider_nuevo").val(),oper:"edit_m",ids:idsNC,old1:oldDist,old2:oldLider,usu:usu},
									type: "post",
									dataType: "html"
								}).done(function(respuesta) {
										jQuery.blockUI({theme:true,title:"Info",message:respuesta,timeout:3000});
										jQuery("#canjes").jqGrid('setGridParam',{postData:{ n:nivel,codigo:oldLider}}).trigger('reloadGrid');

								}).fail(function(jqXHR,textStatus, errorThrown){
										jQuery.blockUI({theme: true, title:textStatus,message:errorThrown,timeout:3000});
								});
							},
							"Cerrar": function() {
									 jQuery(this).dialog("close");
							}
						}
				});
		 }else{// Items not selected
				jQuery.blockUI({theme:true,title: "Advertencia!",message:"Por favor seleccione por lo menos un canje!",timeout: 2600});
		}
  },
  position: "first"
});
});
</script>
