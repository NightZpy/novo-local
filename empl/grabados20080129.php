<?
require_once "../clases/interfaz/classes_listas4.php";
require_once "clases_interfaz.php";
require_once "estetica.css";

//##########################################################################################################
class ListasImp extends Listas
{
   //M�todo Abstracto a implementar en la clase hija.
  function OnEnterCell(&$contenido,&$columna,&$color_fondo,&$color_texto,&$resalte,&$fin_resalte,&$tipo_fila,&$clase)
    {
     if(substr_count($contenido,"TYPE=checkbox VALUE=''")>0)  //Si es un grabado tomado el Valor del checkbox viene vac�o.
       $contenido="";                                         //En consecuencia NO DEBO MOSTRAR EL CHECKBOX
     //   $contenido = str_replace("TYPE=checkbox VALUE","TYPE=checkbox ONCHANGE='checked=false;' VALUE",$contenido);
     //   $contenido = str_replace("TYPE=checkbox VALUE","TYPE=checkbox ONCHANGE='checked=false;' VALUE",$contenido);
    }
  function OnEnterRow(&$parFila)
   {
    global $usuario;
    global $sesion;
    global $c_database;
    global $link;
    global $paso_images_gif;
    $parFila[6] = " <a href=administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=2&id_cp=" . $parFila[0] . "><img border=0 src=images_gif/b_nograba.gif alt=No graba width=55 height=28></a>"; //NO GRABA
    $parFila[8] = " <a href=administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=3&id_cp=" . $parFila[0] . "><img border=0 src=images_gif/b_grinicial.gif alt=GRABADO DE SEMIJOYAS INICIAL width=55 height=28></a>"; //INICIAL
    $parFila[8] .= " <a href=administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=4&id_cp=" . $parFila[0] . "><img border=0 src=images_gif/b_grpalabra.gif alt=GRABADO DE SEMIJOYAS PALABRA width=55 height=28></a><br>"; //PALABRA 
    $parFila[8] .= " <a href=administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=7&id_cp=" . $parFila[0] . "><img border=0 src=images_gif/b_grambos.gif alt=GRABADO DE SEMIJOYAS AMBOS LADOS width=55 height=28></a>"; //AMBOS   
    $parFila[8] .= " <a href=administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=8&id_cp=" . $parFila[0] . "><img border=0 src=images_gif/b_logotipo.gif alt=GRABADO DE SEMIJOYAS LOGOTIPO width=55 height=28></a>"; //LOGOTIPO
  
   }
}
//##########################################################################################################

if($vista == "asignacion")  //Vista de Todo lo no cerrado con posibilidad de Tomar.
 {
  $paso_tomar="administracion.php?pagina=empl/grabados_cambiaestado.php&usuario=$usuario&sesion=$sesion&g=5&cantidad=1&item1=";
  $img_tomar="<img border=0 src=images_gif/b_tomar.gif alt=No graba width=55 height=28>";
  $opc1="usuario";
  $opc2="IF(usuario='',concat('$img_tomar::$paso_tomar',id_comp_ped),'')";
  $opc3="''";
  $opc4="''";
  $titulo = "<b>Listado de Asignaci�n Grabados</b>";
  $check = "IF(usuario='',id_comp_ped,'')";         //Si es el Listado de Asignaci�n s�lo le pongo el VALUE al chackBox si est� NO ASIGNADO.
 }
elseif($vista == "")        //Vista de Todo lo asignado al usuario actual con posibilidad de cerrar.
 {
//   $paso_nogr="administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=2&id_cp=";//NO GRABA
//   $img_nogr="<img border=0 src=images_gif/b_nograba.gif alt=No graba width=55 height=28>";
// 
//   $paso_ini="administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=3&id_cp=";//INICIAL
//   $img_ini="<img border=0 src=images_gif/b_grinicial.gif alt=GRABADO DE SEMIJOYAS INICIAL width=55 height=28>";
// 
//   $paso_pal="administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=4&id_cp=";//PALABRA
//   $img_pal="<img border=0 src=images_gif/b_grpalabra.gif alt=GRABADO DE SEMIJOYAS PALABRA width=55 height=28>";
// 
//   $paso_amb="administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=7&id_cp=";//AMBOS LADOS
//   $img_amb="<img border=0 src=images_gif/b_grambos.gif alt=GRABADO DE SEMIJOYAS AMBOS LADOS width=55 height=28>";
// 
//   $paso_log="administracion.php?pagina=empl/grabados_confirm.php&usuario=$usuario&sesion=$sesion&g=8&id_cp=";//LOGOTIPO
//   $img_log="<img border=0 src=images_gif/b_logotipo.gif alt=GRABADO DE SEMIJOYAS LOGOTIPO width=55 height=28>";
// 
// 
//   $opc1="concat('$img_nogr::$paso_nogr',id_comp_ped)";
//   $opc2="concat('$img_pal::$paso_pal',id_comp_ped)";
//   $opc3="concat('$img_ini::$paso_ini',id_comp_ped)";
//   //$opc4="concat('$img_amb::$paso_amb',id_comp_ped)";
//   $opc4="concat('$img_log::$paso_log',id_comp_ped)";

  $cond = " AND usuario='$usuario' ";
  $titulo = "<b>Listado de Grabados tomados por $usuario</b>";
  $check = "id_comp_ped";  //Si es el Listado de los Tomados por $usuario le pongo el VALUE al chackBox SIEMPRE.
 }

//Construye el select.
$qrystr = "select concat($check,';',id_pedido,';',cantidad,';',id_vd,';<b>P:</b>',nombreproducto,'<br><b>C:</b>',comentario,'<br><b>G:</b>',grabacion,';',concat(us.apellido,' ',us.nombres,' (',us.cod_us,')'),';',concat(us1.apellido,' ',us1.nombres,' (',us1.cod_us,')'),';',concat(us2.apellido,' ',us2.nombres,' (',us2.cod_us,')'),';','') AS c,TO_DAYS(fecha), id_vd,concat(us2.apellido,' ',us2.nombres,' (',us2.cod_us,')') as distri
              FROM grabados as gra, usuario as us, usuario as us1, usuario as us2
              WHERE gra.id_vendedor = us.cod_us
                AND gra.id_lider = us1.cod_us
                AND gra.id_distrib = us2.cod_us
                AND gra.estado=1
                $cond ";
if($orden=="")
  $qrystr .= " ORDER by TO_DAYS(fecha), id_vd";
elseif($orden == "distrib")
  $qrystr .= " ORDER by distri, TO_DAYS(fecha), id_vd";

$lista = new ListasImp();
$lista->MostrarChecks("check_titulo","item"," onclick=setear_checks();");
$lista->camposHidden=array("Distrib");
$lista->cambiaEnGrupo="Distrib";
if($orden == "distrib")
 {
  $lista->usarGrupos = true;
  $tit_orden = "Distribuidor";
 }
else
 {
  $tit_orden = "Tipo de Producto";
 }
//$lista->debug=1;
$lista->titulos=array("check_titulo","Pedido","Id_vd","Cant","GRABACION","Vendedor","L�der","Distrib"," Opciones ");
if($vista == "asignacion")  //Vista de Todo lo no cerrado con posibilidad de Tomar.
  $lista->AlineacionColumnas(array('L','R','R','C','L','L','L','L','C','K','K'));
elseif($vista == "")        //Vista de Todo lo asignado al usuario actual con posibilidad de cerrar.
  $lista->AlineacionColumnas(array('L','R','R','C','L','L','L','L','K','K'));

echo"<table width='100%'><tr>
<td  align='center' class=list width='25%'><a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/grabados.php&orden=$orden&vista=asignacion'>Listado de Asignaci�n</a></td>
<td  align='center' class=list2 width='25%'><a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/grabados.php&orden=$orden&vista='>Listado Tomados por $usuario</a></td>
<td  align='center' class=list width='25%'><a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/grabados.php&orden=distrib&vista=$vista'>Ordenar por Distribuci�n</a></td>
<td  align='center' class=list2 width='25%'><a href='administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/grabados.php&orden=&vista=$vista'>Ordenar por Tipo</a></td>
</tr><tr>
<td colspan=4 align='center' class=listtitle width='25%'>$titulo - Orden: $tit_orden</td>
</tr></table>";
echo "<form method='get' action='administracion.php' name='ff'>
   <input type=hidden name=usuario value=$usuario>
   <input type=hidden name=sesion value=$sesion>
   <input type=hidden name=pagina value=''> ";

$contador = $lista->MuestraTabla($qrystr);

echo"
     <table  CELLSPACING=0 border=0>
        <tr>
          <td class='list' align='Left'>";

if($vista == "asignacion")  //Vista de Todo lo no cerrado con posibilidad de Tomar.
 {
  echo"
            <img src='images_gif/arrow_ltr.png' border='0' width='38' height='22' alt='Con marca:' >
            <b>Con Marca: </b>
            <INPUT TYPE='button' name='Tomar Grabados' value='Tomar Grabados' ONCLICK=envia('empl/grabados_cambiaestado.php',5) $stylebutton>
            <INPUT TYPE='button' name='PDF' value='PDF' ONCLICK=enviaPDF('empl/pdf/pdf_eti_grabados.php') $stylebutton>";

 }
elseif($vista == "")        //Vista de Todo lo asignado al usuario actual con posibilidad de cerrar.
 {
  echo"
            <img src='images_gif/arrow_ltr.png' border='0' width='38' height='22' alt='Con marca:' >
            <b>Con Marca: </b>
            <INPUT TYPE='button' name='No Grabar Masivo' value='No Grabar Masivo' ONCLICK=envia('empl/grabados_confirm.php',2) $stylebutton>
            <INPUT TYPE='button' name='Grabado Inicial Masivo' value='Grabado Inicial Masivo' ONCLICK=envia('empl/grabados_confirm.php',3) $stylebutton>
            <INPUT TYPE='button' name='Grabado Palabra Masivo' value='Grabado Palabra Masivo' ONCLICK=envia('empl/grabados_confirm.php',4) $stylebutton>
      	    <INPUT TYPE='button' name='Grabado Ambos Lados' value='Grabado Ambos Lados' ONCLICK=envia('empl/grabados_confirm.php',7) $stylebutton>
            <INPUT TYPE='button' name='Liberar Grabados' value='Liberar Grabados' ONCLICK=envia('empl/grabados_cambiaestado.php',6) $stylebutton>
            <INPUT TYPE='button' name='PDF' value='PDF' ONCLICK=enviaPDF('empl/pdf/pdf_eti_grabados.php') $stylebutton>
	    ";
 }
echo "
          </td>
        </tr>
     </table>
     <input type=hidden name='cantidad' value='$contador'>
     <input type=hidden name='multiple' value='1'>
     <input type=hidden name='g' value=''>
     <input type=hidden name='orden' value='$orden'>
     </form>";

echo "
<script language='javascript' type='text/javascript'>
function envia(pag,g)
{
 document.ff.g.value= g;
 document.ff.pagina.value= pag;
 document.ff.submit();
}
function setear_checks()
{
 for(i=1;i<=$contador;i++)
  {
   if(testIsValidObject(eval('document.ff.item'+i)))
     eval('document.ff.item'+i+'.checked = document.ff.check_titulo.checked');
  }
}
function enviaPDF(pag)
{
 document.ff.target='_BLANK';
 document.ff.action = pag;
 document.ff.submit();
}
function testIsValidObject(objToTest)
{
 if (null == objToTest)
  {
	 return false;
	}
 if ('undefined' == typeof(objToTest) )
  {
	 return false;
	}
 return true;
}
</script>";

$relat="";
include("menu_1ra_linea.php");
?>
