<?php
/*
 * gestor_bonificaciones_especial.php
 *
 * Copyright 2014 Javier Rios <javier.rios@joyasvanesaduran.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

mysql_select_db($c_database,$link);

$sqlCampania="SELECT MAX(id_campania) AS id_campania	FROM campanias	WHERE 1";
		$resCampania=mysql_query($sqlCampania,$link);
		$rowCampania=mysql_fetch_array($resCampania);
//var_dump($rowCampania);
$campania=$rowCampania['id_campania'];
$sqlGroupProd="SELECT g.nombre_grupo, g.id_grupo FROM grupos_tipos AS gt, grupos AS g
 WHERE gt.id_tipo_grupo=g.tipo_grupo AND g.tipo_grupo=2
 ORDER BY g.id_grupo";

?>
<div class="well">
 <p><span class="text-info" style="font-family:arial, helvetica, sans-serif;">Con este Gestor Ud. podr� establecer la bonificaci�n a aplicar para un grupo de productos correspondientes a un pedido de Cat�logo.</span></p>
 <p class="bg-danger"><span>ATENCI�N: <strong class="text-danger">SOLO PODR� ESPECIFICAR UNA BONIFICACI�N POR CAMPA�A.</strong></span></p>
</div>
<!--
<form class="form-inline">
<fieldset>
-->
<!-- Form Name
<legend>Gestor de Bonificaciones</legend>
-->
<!-- Select Categor�a
<div class="form-group">
		  <label class="control-label" for="categoria">Categor�a</label>
    <select id="categoria" name="categoria">
      <option value="rev" selected="selected">Emprenvendedor</option>
      <option value="lid">L�der</option>
      <option value="dist">Distribuidor</option>
      <option value="reg">Regional</option>
    </select>
</div>
-->
<!-- Text input Bonif
<div class="form-group">
			<label class="control-label" for="bonificacion">Bonificaci�n( En n&uacute;mero )</label>
			<input id="bonificacion" name="bonificacion" type="text" placeholder="Bonificaci�n especial" size="5" required="">
			<!--<p class="help-block">Ingrese el valor de la bonificaci�n especial en n�meros</p>-->
<!--
</div>
-->
<!-- Select Prod
<div class="form-group">
  <label class="control-label" for="idgrupo">Grupo de Prod.</label>
-->
<?php
	//poner_combo_a_medida_1texto_x('idgrupo','nombre_grupo','id_grupo',$sqlGroupProd,'Seleccione un Grupo','Ninguno');
?>
</div>
<!-- Button
<div class="form-group">
    <button id="bonif" name="bonif" class="btn btn-primary">Guardar</button>
</div>

</fieldset>
</form>
-->
<table id="tb_bonif"></table>
<div id="pagerBonif"></div>

<script type="text/javascript">
jQuery(function(){// On ready
	var cate=jQuery('#categoria option:selected').val();
	var grupo=jQuery('#idgrupo option:selected').val();
	var boni=jQuery('#bonificacion').val();
	var campania="<?php echo $campania;?>";

	jQuery('#tb_bonif').jqGrid({
				url: 'empl/get_bonificaciones_especiales.php?camp='+campania,
				datatype : 'json',
				autowidth: true,
				shrinkToFit: false,
				colNames : ['Campa�a','Categor�a','Bonificaci�n','Grupo Prod.','Nombre Grupo'],
				colModel : [{
					name : 'obs',
					index: 'obs',
					width : 150,
					align: 'left'
				},{
					name : 'categoria',
					index: 'categoria',
					width : 180,
					align: 'left',
					editable:true,
					edittype:'select',formatter:'text' ,editoptions:{value:"rev:Emprenvendedor;lid:L�der;dist:Distribuidor;reg:Regional"}
				},{
					name : 'bonif',
					index: 'bonif',
					width : 120,
					align: 'left',
					editable:true
				},{
					name: 'id_grupo_prod',
					index:'id_grupo_prod',
					width: 100,
					align: 'left',
					editable:true
				},{
					name: 'nombre_grupo',
					index:'nombre_grupo',
					width: 290,
					align: 'left',
					editable:false,
					editoptions:{rules:'readonly'}
				}
				],
				height: 'auto',
				width: 720,
				pager: '#pagerBonif',
				rowNum: 5,
				rowList: [10,20],
				sortname: "obs",
				viewrecords: true,
				sortorder: "desc",
				editurl: "abm_bonificaciones_especiales.php",
				caption: "LISTADO DE BONIFICACIONES ESPECIALES EN PRODUCTOS"
});
jQuery("#tb_bonif").jqGrid('navGrid','#pagerBonif',
{search:false,del:false},
{closeAfterEdit:true,afterSubmit: function(response) {
					jQuery.blockUI({theme: true,title: "Info",message:"<p>"+response.responseText+"</p>",timeout:3000});
					return [true,"",""];
       },height:280,reloadAfterSubmit:false
}, // Edit options
{addCaption:"Alta de Nueva Bonificaci&oacute;n",
	bSubmit:"Guardar",
	bCancel:"Cancelar",
	processData:"Procesando...",
	checkOnUpdate:true,
	topinfo: "Complete todos los campos.",
	closeAfterAdd:true,
	reloadAfterSubmit:true,
	afterSubmit: function(response) {
					jQuery.blockUI({theme: true,title: "Info",message:"<p>"+response.responseText+"</p>",timeout:3000});
					return [true,"",""];
  },height:280,reloadAfterSubmit:false
},// Add options
{reloadAfterSubmit:false}, // Del options
{} // Search options
);
});
</script>
