<?
?>
<script language="javascript">

function ValidarDatos(Form)
{
  if (StringVacio("ID VD",Form.id_vd)) return false;
  if (StringVacio("Nombre Producto",Form.nombreproducto)) return false;
  if (StringVacio("Id Producto Proveedor",Form.id_prod_prov)) return false;
  if (PrecioValido("Precio",Form.preciounidad,0,100000000)) return false;
  if (PrecioValido("Precio Costo",Form.precio_costo,0,100000000)) return false;
  if (StringVacio("Orden",Form.orden)) return false;
  if (StringVacio("Ubi",Form.ubi)) return false;
  if (StringVacio("Descripcion",Form.descr_txt)) return false;
  if (StringVacio("Metal",Form.metal)) return false;
  if (StringVacio("Fact.V.D.",Form.id_vd_fact)) return false;
  if (NumeroValido("Bolsa",Form.bolsa,0,1000000)) return false;

  return true;
}
</script>
<center>
<table width=100%>
  <tr>
  </tr>
</table>
</center>

<?php
  if ($producto != -1){
    $inhibe_id_vd=' onfocus=blur(); ';
    $qrystr = "select * FROM productos as prod
                        WHERE id_producto = $producto";

     $qry = mysql_db_query($c_database,$qrystr ,$link);
		 $row = mysql_fetch_array($qry);
  }else
    $inhibe_id_vd='';
?>
<?php
$qrystr_producto="SELECT MAX(id_producto)
             FROM productos
              WHERE id_producto>600000 AND id_producto<800000";
$qry_producto= mysql_db_query($c_database,$qrystr_producto,$link);
$row_producto = mysql_fetch_array($qry_producto);

$id_producto_mostrar=$row_producto[0]+1;
?>
<FORM name="ff" ENCTYPE=multipart/form-data ACTION=administracion.php METHOD=POST>
<p style="margin-left: 40">
  <INPUT TYPE=hidden name=usuario value=<?php echo $usuario; ?>>
  <INPUT TYPE=hidden name=sesion value=<?php echo $sesion; ?>>
  <INPUT TYPE=hidden name=producto value=<?php echo $producto; ?>>
  <INPUT TYPE=hidden name=pagina value="empl/productos_grabar.php">
  <INPUT TYPE=hidden name=t1 value=<?php echo $t1; ?>>
  <INPUT TYPE=hidden name=r1 value=<?php echo $r1; ?>>
  <font size="2" face="Arial"><b>Id-Producto</b><b><font size="3" color=red> <?php echo $id_producto_mostrar; ?></font></b><font size="1">  - Este ID-Producto puede variar al grabarse . -  </font></font><br>
  <font size="2" face="Arial"><b>Ident. V.D.</b></font><br>
  <input type="text" name="id_vd" size="20" <?php echo $stylefield . ' ' . $inhibe_id_vd; ?>><br>
  <font size="2" face="Arial"><b>Nombre Articulo</b></font><br>
  <input type="text" name="nombreproducto" size="50" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Id Producto Proveedor</b></font><br>
  <input type="text" name="id_prod_prov" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Precio (obsoleto)</b></font><br>
  <input type="text" name="preciounidad" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Precio Costo</b></font><br>
  <input type="text" name="precio_costo" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Contribucion Marginal Unitaria</b></font><br>
  <input type="text" name="cmu" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Costo en Reales (R$)</b></font><br>
  <input type="text" name="precio_costo_r" size="10" <?php echo $stylefield; ?>><br> 
  <font size="2" face="Arial"><b>Costo en Dolares (U$S)</b></font><br>
  <input type="text" name="precio_costo_d" size="10" <?php echo $stylefield; ?>><br>   
  <font size="2" face="Arial"><b>Habilitado en Web</b></font><br>
  <select name="stock">
  <option value='0'>NO</option>
  <option value='1'>SI</option>
  </select><BR>
  <font size="2" face="Arial"><b>Control Stock Web</b></font><br>
  <select name="control_web">
  <option value='0'>NO</option>
  <option value='1'>SI</option>
  </select><BR>
  <font size="2" face="Arial"><b>Marca Temporal</b></font><br>
  <select name="temporal">
  <option value='0'>0</option>
  <option value='1'>1</option>
  <option value='2'>2</option>
  </select><BR>
  <font size="2" face="Arial"><b>Orden</b></font><br>
  <input type="text" name="orden" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Ubi</b></font><br>
  <input type="text" name="ubi" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Descripci&oacute;n</b></font><br>
  <textarea rows="4" name="descr_txt" cols="60" <?php echo $stylefield; ?>></textarea><br>
  <font size="2" face="Arial"><b>Tipo de Metal</b></font><br>
  <?php
  $qrystrmetal = "SELECT id_metal, concat(id_metal,'-',metal_descrip) AS paq FROM `productos_metales` ORDER BY id_metal";
  poner_combo_a_medida("metal","paq","id_metal",$qrystrmetal);
  ?><br>
  <font size="2" face="Arial"><b>Color</b></font><br>
  <?php
  $qrystrcolor = "SELECT id_color, concat(id_color,'- ',color_descrip) AS paq FROM `productos_colores` ORDER BY id_color";
  poner_combo_a_medida("color","paq","id_color",$qrystrcolor);
  ?><br>
  <font size="2" face="Arial"><b>Medida</b></font><br>
  <?php
  $qrystramedida = "SELECT id_medida, concat(id_medida,'- ',descrip) AS paq FROM `productos_medida` ORDER BY id_medida";
  poner_combo_a_medida("medida","paq","id_medida",$qrystramedida);
  ?><br>
  <font size="2" face="Arial"><b>Proveedor</b></font><br>
  <?php
  $qrystrproveedor = "SELECT p.id_proveedor,CONCAT(p.id_proveedor, '  -',p.proveedor_descrip) AS paq
                      FROM proveedor AS p INNER JOIN proveedor_tipo AS pt ON p.id_tipo=pt.id_tipo
                      WHERE pt.id_tipo=1 ORDER BY p.id_proveedor";//echo"$qrystramedida";
  poner_combo_a_medida("id_proveedor","paq","id_proveedor",$qrystrproveedor);
  ?><br>
  <font size="2" face="Arial"><b>Empaque</b></font><br>
  <?php

  $qrystrempaque = "SELECT id_producto, concat(id_vd, ' ', nombreproducto) AS paq FROM `productos` WHERE `id_vd` LIKE 'C%' ";
  poner_combo_a_medida_1texto("id_producto_empaque","paq","id_producto",$qrystrempaque,"SIN EMPAQUE",0);
  ?><br>
  <font size="2" face="Arial"><b>Tipo Garant&iacute;a</b></font><br>
  <?php
  $qrystrgarantia = "SELECT id_vd, concat(id_vd, ' ', nombreproducto) AS paq FROM `productos` WHERE `id_vd` LIKE 'GAR%' ";
  poner_combo_a_medida_1texto("tipo_garantia","paq","id_vd",$qrystrgarantia,"SIN GARANTIA","");

  ?><br>
  <font size="2" face="Arial"><b>Fact.V.D.</b></font><br>
  <input type="text" name="id_vd_fact" size="20" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Bolsa</b></font><br>
  <input type="text" name="bolsa" size="10" <?php echo $stylefield; ?>><br>
  <font size="2" face="Arial"><b>Cat&aacute;logos</b></font> - <span class="ui-state-error" style="padding: 0.7em; margin-left: 15%;"><small><?php if(!empty($row[catalogos])){echo $row[catalogos];}else{ echo "Sin Seleccionar";};?></small></span><br>
   <?php
  $qrystrcatalogos = "SELECT desc_catalogo FROM `tipos_catalogos` ORDER BY id_tipo_catalogo";
  poner_combo_a_medida_multiple("catalogos[]","desc_catalogo","desc_catalogo",$qrystrcatalogos,'Ninguno');

  ?>
  <br>
  </p>

  <p align=center>
  <input type="button" value="Aceptar" name="Aceptar" <?php echo $stylebutton; ?>  onClick="javascript:if (ValidarDatos(ff)) submit();">&nbsp;&nbsp;
  </p>
  <script type="text/javascript">
		var cat="<?php echo $row[catalogos];?>";
		jQuery("select[name='catalogos[]']").each(function(){
			jQuery(this).val(cat).prop("checked","true");
		});
  </script>
</form>
<?php
  if ($producto != -1)
  {

     $ii=mysql_affected_rows();
     //echo "$qrystr";
     $combo[1]= 0;
     $combo[2]= 0;
     $combo[3]= 0;
     $combo[4]= 0;
     for ($i = 1; $i <=$ii  ; $i++){

       if (is_null($row[id_combo])) {$combo[$i]= 0;}
                             else  {$combo[$i]= $row[id_combo];
                                    }
      }
     llenar_campo("ff","id_vd",$row[id_vd]);
     llenar_campo("ff","id_prod_prov",$row[id_prod_prov]);
     llenar_campo("ff","nombreproducto",$row[nombreproducto]);
     llenar_campo("ff","preciounidad",$row[preciounidad]);
     llenar_campo("ff","precio_costo",$row[precio_costo]);
     llenar_campo("ff","cmu",$row[cmu]);
     llenar_campo("ff","precio_costo_r",$row[precio_costo_r]);
     llenar_campo("ff","precio_costo_d",$row[precio_costo_d]);
     llenar_campo("ff","stock",$row[stock]);
     llenar_campo("ff","control_web",$row[control_web]);
     llenar_campo("ff","orden",$row[orden]);
     llenar_campo("ff","ubi",$row[ubi]);
     llenar_campo("ff","descr_txt",$row[descr_txt]);
     llenar_campo("ff","metal",$row[metal]);
     llenar_campo("ff","id_producto_empaque",$row[id_producto_empaque]);
     llenar_campo("ff","tipo_garantia",$row[tipo_garantia]);
     llenar_campo("ff","id_vd_fact",$row[id_vd_fact]);
     llenar_campo("ff","temporal",$row[temporal]);
     llenar_campo("ff","medida",$row[medida]);
     llenar_campo("ff","color",$row[cod_color]);
     llenar_campo("ff","id_proveedor",$row[id_proveedor]);
     llenar_campo("ff","bolsa",$row[bolsa]);
     llenar_campo("ff","catalogos",$row[catalogos]);
     }
echo "
<div class='contenido'>
	<table border=0>
     <tr>
        <td width='33%'>&nbsp;</td>
        <td width='33%'>
            <a href='administracion.php?pagina=empl/func_emp_lista.php&usuario=$usuario&sesion=$sesion'><img border=0 src=$bpinicio></a></td>
         <td width='34%'>&nbsp;</td>
     </tr>
  </table>
</div>";
?>
