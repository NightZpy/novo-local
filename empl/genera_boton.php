<?php
//Descomentar la l�nea siguiente para fijar m�rgenes.
//$margenes = 1;   //Tambi�n puede venir como par�metro.

if($margenes<>"")       //El par�metro m�rgenes se usa para decir al script que se est� queriendo obtener una vista previa.
  header ("Content-type: image/png");
//********* Setear estos m�rgenes seg�n el gr�fico. Definen la zona en la que puede haber texto. ******
$margenl = 13;          //Margen Izquierdo.
$margenr = 11;          //Margen Derecho.
$margent = 10;         //Margen Superior.
$margenb = 10;         //Margen Inferior.
//******* FIN Setear estos m�rgenes seg�n el gr�fico. Definen la zona en la que puede haber texto. ****

if($directorio_img == "")
  $directorio_img = "../images_gif";

if(!isset($texto_boton))
  $texto_boton = "Regeneraci�n de Libreta de Direcciones";     //Viene definido como par�metro.
$palabras = explode(" ",$texto_boton);

$fuente = "$directorio_img/ARIAL.TTF";
$tamanio = 9;
if(!isset($nombre_base))
  $nombre_base = "base.png";
$im  = imagecreatefrompng ("$directorio_img/$nombre_base");
$renglones = array();

$renglones[0] = $palabras[0];
for($p=1;$p<sizeof($palabras);$p++)
 {
  $coorde = imagettfbbox($tamanio,0 ,$fuente, $renglones[sizeof($renglones)-1] . $palabras[$p]);
//  var_dump($coorde);

  if($coorde[2] <= imagesx($im)-$margenl-$margenr)
    $renglones[sizeof($renglones)-1] .= " ".$palabras[$p];
  else
    $renglones[sizeof($renglones)] = $palabras[$p];
 }

if($margenes<>"")  //Esto es para dibujar 2 cuadrados, uno es el borde del bot�n y el otro la zona en la que puede haber texto.
 {
  imagerectangle ( $im, 0, 0, imagesx($im)-1,imagesy($im)-1, 548640);
  imagerectangle ( $im, $margenl, $margent, imagesx($im)-$margenr-1,imagesy($im)-$margenb-1, 548640);
 }
$text_color = imagecolorallocate ($im, 255, 255, 247);
$text_color1 = imagecolorallocate ($im, 255, 255, 247);
for($r=0;$r < sizeof($renglones);$r++)
 {
  $altorenglon = (imagesy($im)-$margent-$margenb)/sizeof($renglones);
  $coorde = imagettfbbox($tamanio,0 ,$fuente, $renglones[$r]);
  $posx = (imagesx($im)-($coorde[2]))/2;
//  imageline($im,0,  $margent + $altorenglon * ($r),50,$margent + $altorenglon * ($r),234354556);
  $posy = $margent + $altorenglon * ($r+0.5) - $coorde[5] * 0.5;
  imagettftext ($im, $tamanio, 0, $posx+1, $posy, $text_color1, $fuente, $renglones[$r]);
  imagettftext ($im, $tamanio, 0, $posx, $posy, $text_color, $fuente, $renglones[$r]);
 }
if($margenes<>"")  //Esto es para dibujar el bot�n en lugar de guardarlo.
  imagepng ($im);
else
  if (function_exists("imagepng"))
   {
    if($nombre_boton == "")
      $nombre_boton = $texto_boton.".gif";

    imagepng ($im,"$directorio_img/$nombre_boton");
   }
   
imagedestroy ($im);

?>
