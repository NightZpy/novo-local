<?php
include("../conexion.php");
mysql_select_db($c_database,$link) or die("Error al conectar a la base.");

$page = $_GET['page']; // get the requested page
$limit = $_GET['rows']; // get how many rows we want to have into the grid
$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction
$campania=$_GET['camp'];//La campaña vigente
if(!$sidx) $sidx =1;

// Por si vienen valores para filtrar
if($_GET['_search']==true){
	$filtro=(isset($_GET['campania']) || $_GET['campania']!="")?" AND campania =".$_GET['campania']:" AND 1";
}else{ $filtro=""; }

$resCantBonif = mysql_query("SELECT COUNT(id) AS cant FROM gestor_bonif_dif WHERE campania=".$campania);
$rowCant = mysql_fetch_object($resCantBonif);
$count = $rowCant->cant;
//var_dump($rowCant);
if( $count >0 ) {
	$total_pages = ceil($count/$limit);
} else{
	$total_pages = 0;
}
if($page > $total_pages) $page=$total_pages;
if($page>0){	$start = $limit*$page - $limit;} // do not put $limit*($page - 1)
else { $start=0;}

$sqlBonifTable="SELECT gbd.id,gbd.campania AS id_camp,c.obs AS campania,IF(gbd.categoria='rev','Emprenvendedor',IF(gbd.categoria='lid','Líder',IF(gbd.categoria='dist','Distribuidor','Regional'))) AS categoria,gbd.bonif,gbd.id_grupo_prod,g.nombre_grupo
		FROM gestor_bonif_dif AS gbd, campanias AS c,grupos_tipos AS gt, grupos AS g
		WHERE gbd.campania=c.id_campania
		AND gt.id_tipo_grupo=g.tipo_grupo AND g.tipo_grupo=2
		AND g.id_grupo=gbd.id_grupo_prod".$filtro." ORDER BY $sidx $sord LIMIT $start , $limit";
//echo $sqlBonifTable;
$result = mysql_query($sqlBonifTable) or die("No se pudo ejecutar la consulta.".mysql_error());

$responce->page = $page;
$responce->total = $total_pages;
$responce->records = $count;
$i=0;
while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
    $responce->rows[$i]['id']=$row['id'];
    $responce->rows[$i]['cell']=array(utf8_encode($row['id_camp']."-".$row['campania']),$row['categoria'],$row['bonif'],$row['id_grupo_prod'],$row['nombre_grupo']);
    $i++;
}
echo json_encode($responce);
?>
