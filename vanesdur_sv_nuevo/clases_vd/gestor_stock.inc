<?php

class GestorStock
{
  var $tipoControl;  //[ 'auto' | 0 | 1 ]
  var $arrDepositos;
  var $log;
  var $campoControl;
  var $tipoLog;    //[ 'todo' | 'errores' ]
  
  function GestorStock()
  {
    global $c_database;
    global $link;
    $this->tipoControl = 'auto';
    $this->log = '';
    //$this->campoControl="stk_activo";
    $this->campoControl="stk_activo";
    $this->tipo_log = "todo";
    
    //Guardar en un array los dep�sitos para luego poder hacer b�squedas m�s r�pidas.
    $qrystr = "SELECT * FROM depositos ";
    $qry= mysql_db_query($c_database,$qrystr ,$link);
    $this->verificarSQL($qrystr,'Constructor');
    while($row=mysql_fetch_assoc($qry))
      $this->arrDepositos[$row['id_deposito']] = $row;  
   
  }
  
  function registrarMovimiento($id_vd,$cantidad,$stock_origen,$stock_destino,$signo=1)
  {
    global $c_database;
    global $link;
    global $usuario;
    global $sesion;
//echo "id_vd,: $id_vd, cantidad: $cantidad,stock_origen: $stock_origen,stock_destino: $stock_destino,signo: $signo<br>";    

    //Obtener datos del Producto.
    $qrystrprod = "SELECT p.*,s.cantidad
                   FROM productos AS p
                     LEFT JOIN stock AS s ON p.id_vd=s.id_vd AND s.id_deposito='$stock_origen' 
                   WHERE p.id_vd = '$id_vd' ";
    $qryprod= mysql_db_query($c_database,$qrystrprod ,$link);
    $this->verificarSQL($qrystrprod,'registrarMovimiento');
    $rowprod = mysql_fetch_array($qryprod);
    
    //Todo esto se hace s�lo si el producto exige control de stock.
    if($rowprod[$this->campoControl]==1)
    {
      //Insertar los registros en stock_resta y stock_suma. Si ya exist�an falla pero no muestra. 
      $qrystr_ins_o = "INSERT IGNORE INTO `stock` ( `id_vd` , `id_deposito` , `cantidad` )
                       VALUES (
                         '$id_vd', '$stock_origen', '0'
                           ), (
                         '$id_vd', '$stock_destino', '0'  
                           )";
      $qry_ins_o= mysql_db_query($c_database,$qrystr_ins_o ,$link);
      //verificar($qrystr_ins_o);
      
      //Buscar el permiso para control. 
      $dep_origen = $this->arrDepositos[$stock_origen];
      $dep_destino = $this->arrDepositos[$stock_destino];
      //tipoControl 
      //auto: desautoriza el movimiento si el dep�sito destino es tipo 1 (interno) y no hay existencias sufifientes.
      //0: no desautoriza el movimiento.
      //1: desautoriza el movimiento si no hay existencias sufifientes sin importar el tipo de dep�sito.
      //En este if se decide si se agrega o no al update la condici�n de que haya existencias sufifientes.
      if((($dep_origen["tipo_deposito"]==1) and ($this->tipoControl === 'auto')) or ($this->tipoControl == 1))
      {
        $str_agregado = " AND (cantidad - ($cantidad * $signo))>=0 ";
//echo "str_agregado: $str_agregado<br />";
      }  
      
      // ************************************ //
      //Actualizar el stock origen
      $qrystr6b = "UPDATE stock
                   SET cantidad=cantidad - ".($cantidad * $signo)."
                   WHERE id_vd = '$rowprod[id_vd]'
                         AND id_deposito='$stock_origen'
                         $str_agregado";
      $qry6b = mysql_db_query($c_database,$qrystr6b,$link);
      $this->verificarSQL($qrystr6b,'registrarMovimiento');
//echo "qrystr6b: $qrystr6b<br />";
      
      //Si no hubo registros actualizados, entonces el control no lo permiti� => mostrar este error.
      if(mysql_affected_rows()==0)
      {
        $this->log .= "<b>Imposible ingresar $cantidad unidades a articulo: $rowprod[id_vd] $rowprod[nombreproducto] porque en el Origen existen ". (1 * $rowprod['cantidad']) ." unidades</b><br>";
        return false;
      }
      // ************************************ //
      
      // ************************************ //
      //Actualizar el stock destino
      $qrystr6b = "UPDATE stock
                   SET cantidad=cantidad+".($cantidad*$signo)."
                   WHERE id_vd = '$rowprod[id_vd]'
                         AND id_deposito='$stock_destino'";
      $qry6b = mysql_db_query($c_database,$qrystr6b,$link);
      $this->verificarSQL($qrystr6b,'registrarMovimiento');
      // ************************************ //
//echo "qrystr6b: $qrystr6b<br />";
      
      //Loguear el movimiento.
      if($this->tipoLog=='todo')
        $this->log .= "Ingresa $cantidad unidades a articulo: $rowprod[id_producto] - ($rowprod[id_vd]) $rowprod[nombreproducto]<br>";

      //Insertar registro en mov_stock
      $qrystrmov="INSERT INTO `mov_stock` ( `usuario` , `fecha_mov` , `id_producto` , `id_vd` , `cantidad` , `dep_in` , `dep_out` )
                  VALUES ('$usuario', CURRENT_TIMESTAMP, '$rowprod[id_producto]', '$rowprod[id_vd]', " . ($cantidad*$signo) . ", '$stock_destino', '$stock_origen')";
      $qrymov = mysql_db_query($c_database,$qrystrmov ,$link);
      $this->verificarSQL($qrystrmov,'registrarMovimiento');
//echo "qrystrmov: $qrystrmov<br />";
    }
    else
    {
      //Loguear .
      if($this->tipoLog=='todo')
       $this->log .= "<b>Imposible ingresar $can unidades a articulo: $id_pro - ($rowprod[id_vd]) $rowprod[nombreproducto] porque este producto no tiene activado el control de stock</b><br>";
    }
    
    return true;
  } 
  // ***************************************************************************************************** //

  // ***************************************************************************************************** //
  // ************* Verifica si hubo errores sql **************
	function verificarSQL($qrystr,$metodo='')
	{
		$err = mysql_error();
		if($err<>'')
		{
			echo "Error en clase GestorStock -> $metodo:" . $err . "<br><br>" . $qrystr . "<br><br>";
		}
	}
	// ************************************************
}

//******************VALIDA STOCK EXISTENTE******************
function verificarExistenciaStock($id_pedido){
	global $c_database;
    global $link;
    
	$qrystr="SELECT pic.id_vd, SUM(s.cantidad) AS nivel, SUM(pic.cant_e) AS req
	         FROM pedidos_int_comp AS pic
	         INNER JOIN stock AS s ON pic.id_vd = s.id_vd AND s.id_deposito = pic.dep_origen
	         WHERE pic.id_pedido = $id_pedido
	         GROUP BY pic.id_vd
	         HAVING req > nivel";
	
	$qry = mysql_db_query($c_database,$qrystr,$link);
//	echo $qrystr;
	
  
  if(mysql_num_rows($qry)<>0)
  { 
  	while($row=mysql_fetch_array($qry)){
	  echo "<b>Imposible ingresar unidades del articulo: $row[id_vd] porque en el Origen existen $row[nivel] unidades y se requieren $row[req] unidades</b><br>";        
    }
      	return false;     
  }
  else{
  	return true;
  }
}
//**********************************************************


?>
