//*****************************************************************************
// Do not remove this notice.
//
// Copyright 2001 by Mike Hall.
// See http://www.brainjar.com for terms of use.
//*****************************************************************************
// Determine browser and version.
function Browser() {
var ua, s, i;
this.isIE    = false;
this.isNS    = false;
this.version = null;
ua = navigator.userAgent;
s = "MSIE";
if ((i = ua.indexOf(s)) >= 0) {
this.isIE = true;
this.version = parseFloat(ua.substr(i + s.length));
return;
}
s = "Netscape6/";
if ((i = ua.indexOf(s)) >= 0) {
this.isNS = true;
this.version = parseFloat(ua.substr(i + s.length));
return;
}
// Treat any other "Gecko" browser as NS 6.1.
s = "Gecko";
if ((i = ua.indexOf(s)) >= 0) {
this.isNS = true;
this.version = 6.1;
return;
}
}
var browser = new Browser();
// Global object to hold drag information.
var dragObj = new Object();
dragObj.zIndex = 0;
function dragStart(event, id) {
var el;
var x, y;

// If an element id was given, find it. Otherwise use the element being
// clicked on.
if (id)
dragObj.elNode = document.getElementById(id);
else {
if (browser.isIE)
dragObj.elNode = window.event.srcElement;
if (browser.isNS)
dragObj.elNode = event.target;
// If this is a text node, use its parent element.
if (dragObj.elNode.nodeType == 3)
dragObj.elNode = dragObj.elNode.parentNode;
}

/* PARÁMETROS OPCIONALES */
var opciones = arguments[2] || {};
/* asignamos valores a los atributos de este objeto. */
dragObj.elNode.modo = opciones.modo	|| '';
dragObj.elNode.anchoMin = opciones.anchoMin	|| 150;
dragObj.elNode.altoMin = opciones.altoMin	|| 90;
dragObj.elNode.divAsociado = opciones.divAsociado	|| null;


// Get cursor position with respect to the page.
if (browser.isIE) {
x = window.event.clientX + document.documentElement.scrollLeft
+ document.body.scrollLeft;
y = window.event.clientY + document.documentElement.scrollTop
+ document.body.scrollTop;
}
if (browser.isNS) {
x = event.clientX + window.scrollX;
y = event.clientY + window.scrollY;
}
// Save starting positions of cursor and element.
dragObj.cursorStartX = x;
dragObj.cursorStartY = y;

// Resize
if (dragObj.elNode.modo=='resize') 
{
  dragObj.elNode.widthOrg = dragObj.elNode.offsetWidth;
  dragObj.elNode.heightOrg = dragObj.elNode.offsetHeight;
  if(dragObj.elNode.divAsociado)
  {
    dragObj.elNode.divAsociado.widthOrg = dragObj.elNode.divAsociado.offsetWidth; 
    dragObj.elNode.divAsociado.heightOrg = dragObj.elNode.divAsociado.offsetHeight;
  }

}

if(dragObj.elNode.style.left=='') dragObj.elNode.style.left= dragObj.elNode.offsetLeft;
if(dragObj.elNode.style.top=='') dragObj.elNode.style.top= dragObj.elNode.offsetTop;

dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);
if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;
// Update element's z-index.
//dragObj.elNode.style.zIndex = ++dragObj.zIndex;
alFrente(dragObj.elNode);
// Capture mousemove and mouseup events on the page.
if (browser.isIE) {
document.attachEvent("onmousemove", dragGo);
document.attachEvent("onmouseup",	dragStop);
window.event.cancelBubble = true;
window.event.returnValue = false;
}
if (browser.isNS) {
document.addEventListener("mousemove", dragGo,   true);
document.addEventListener("mouseup",   dragStop, true);
event.preventDefault();
}
}
function dragGo(event) {
var x, y;
// Get cursor position with respect to the page.
if (browser.isIE) {
x = window.event.clientX + document.documentElement.scrollLeft
+ document.body.scrollLeft;
y = window.event.clientY + document.documentElement.scrollTop
+ document.body.scrollTop;
}
if (browser.isNS) {
x = event.clientX + window.scrollX;
y = event.clientY + window.scrollY;
}

// Resize case, update width/height
if (dragObj.elNode.modo =='resize') 
{

   var nuevoAncho = 0;
   var nuevoAlto = 0;

   if(dragObj.elNode.widthOrg + x - dragObj.cursorStartX > dragObj.elNode.anchoMin)
     nuevoAncho = dragObj.elNode.widthOrg + x - dragObj.cursorStartX ;
   else
     nuevoAncho=dragObj.elNode.anchoMin;

   if(dragObj.elNode.heightOrg + y - dragObj.cursorStartY > dragObj.elNode.altoMin)
     nuevoAlto = dragObj.elNode.heightOrg + y - dragObj.cursorStartY;
   else
     nuevoAlto=dragObj.elNode.altoMin;
   
   dragObj.elNode.style.width = nuevoAncho + "px"; var dx = dragObj.elNode.widthOrg - nuevoAncho;
   dragObj.elNode.style.height = nuevoAlto + "px"; var dy = dragObj.elNode.heightOrg - nuevoAlto;

   if(dragObj.elNode.divAsociado)
   {
     dragObj.elNode.divAsociado.style.width = (dragObj.elNode.divAsociado.widthOrg - dx) + "px"; 
     dragObj.elNode.divAsociado.style.height = (dragObj.elNode.divAsociado.heightOrg - dy) + "px";
   }

  //document.getElementById('log').innerHTML='dragObj.elNode.offsetWidth ' + dragObj.elNode.offsetWidth + ', dragObj.elNode.offsetHeight ' + dragObj.elNode.offsetHeight ;    

}
else
{
  // Move drag element by the same amount the cursor has moved.
  dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
  dragObj.elNode.style.top  = (dragObj.elStartTop  + y - dragObj.cursorStartY) + "px";

}

if (browser.isIE) {
window.event.cancelBubble = true;
window.event.returnValue = false;
}
if (browser.isNS)
event.preventDefault();
}
function dragStop(event) {
// Stop capturing mousemove and mouseup events.
if (browser.isIE) {
document.detachEvent("onmousemove", dragGo);
document.detachEvent("onmouseup",	dragStop);
}
if (browser.isNS) {
document.removeEventListener("mousemove", dragGo,	true);
document.removeEventListener("mouseup",   dragStop, true);
}
}
function alFrente(elemento)
{
  elemento.style.zIndex = ++dragObj.zIndex;
}
