function menu_tooltip(id_div,id_div_cont)
{

  this.delay=500;
  this.solapamiento=5;
  this.id_div=id_div;
  this.id_div_cont = id_div_cont;
  this.parametros=null;
  this.objeto_ref=null;
  document.getElementById(this.id_div).permiso_para_ocultar=0;
  document.getElementById(this.id_div).id_timeout_actual=0;
  
  document.getElementById(this.id_div).manejador=this;
  document.getElementById(this.id_div).onmouseover=function(){this.permiso_para_ocultar=0;};
  document.getElementById(this.id_div).onmouseout=function(){this.manejador.prog_ocultar_opc();};


  // ********************************************** //
  this.mostrar_opc = function (objeto_ref)
  {
    this.objeto_ref=objeto_ref;
    this.parametros = arguments[1] || {};

    var div;
    div=document.getElementById(this.id_div);
    div.style.visibility='visible'; 
     
    var div_cont;
    div_cont=document.getElementById(this.id_div_cont);
  
    div_cont.innerHTML  = this.rellenarOpciones();
  
    coord = this.findPos(this.objeto_ref);  
    div.style.left=coord[0] + this.objeto_ref.offsetWidth -this.solapamiento ;
    div.style.top=coord[1];

    div.llamador=this.objeto_ref;
    div.permiso_para_ocultar=0;
  }
  
  this.prog_ocultar_opc = function ()
   {
    div=document.getElementById(this.id_div);
  
    if(div.id_timeout_actual!=0)
      clearTimeout(div.id_timeout_actual);
    div.id_timeout_actual = setTimeout('div.manejador.oculta_opc()', this.delay );
    div.permiso_para_ocultar=div.id_timeout_actual;
   }
  
  
  this.oculta_opc = function ()
  {
    var div;
    div=document.getElementById(this.id_div);
    if(div.permiso_para_ocultar==div.id_timeout_actual)
      div.style.visibility='hidden'; 
  }


  this.obligar_oculta_opc = function ()
  {
    var div;
    div=document.getElementById(this.id_div);
    div.style.visibility='hidden'; 
  }
  
  this.findPos =function (obj) 
  {
  	var curleft = curtop = 0;
  	if (obj.offsetParent) 
    {
  	  do 
      {
  			curleft += obj.offsetLeft;
  			curtop += obj.offsetTop;
  		} while (obj = obj.offsetParent);
  	  return [curleft,curtop];
    }
  }

  this.rellenarOpciones =function () 
  {
    var buf;
    buf  =' � Acci�n 1 para el objeto ' + this.objeto_ref.id + ' <br>';
    buf +=' � Acci�n 2 para el objeto ' + this.objeto_ref.id + ' <br>';
    buf +=' � Acci�n 3 para el objeto ' + this.objeto_ref.id + ' <br>';
    return buf;
  }
}
