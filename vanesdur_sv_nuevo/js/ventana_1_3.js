function Capa(idcapa){
  this.id=idcapa;

  var opciones = arguments[1] || {};
	/* asignamos valores a los atributos de este objeto. */
	this.texto = opciones.texto	|| '';
	this.className = opciones.clase_est	|| '';
	this.divPadre = opciones.divPadre	|| document.body;

  this.CrearCapa= function CrearCapa(){
  try{
    capa=document.createElement('div');    
    capa.setAttribute('id',this.id);        
    this.divPadre.appendChild(capa);
    capa = document.getElementById(this.id); 
  	capa.className = this.className;

    capa.innerHTML=this.texto; 
    }catch(e){
      alert(e.name + " - " + e.message);
    }
   this.div=capa;
   return(this); 
  }
  this.CrearCapa();
}

function Ventana(){
  this.id=null;
  this.capaContenedora =null;
  this.capaTitulo =null;
  this.capaContenido =null;
  this.capaEstado =null;
  
  var opciones = arguments[0] || {};
	/* asignamos valores a los atributos de este objeto. */

	this.id = opciones.id	|| '__v' + crearID()
	this.titulo = opciones.titulo	|| 'Ventana ' + this.id;
	this.html = opciones.html	|| '';
	this.className = opciones.className	|| 'dialog';

  this.CrearVentana= opciones.CrearVentana	|| function CrearVentana()
  {
//  try{
    className = this.className;
    id = this.id;
    tabla_grafica = "\
      <table id='"+ id +"_row1' class=\"top table_window\">\
        <tr>\
          <td class='"+ className +"_nw'></td>\
          <td class='"+ className +"_n'><div id='"+ id +"_top' class='"+ className +"_title title_window'></div></td>\
          <td class='"+ className +"_ne'></td>\
        </tr>\
      </table>\
      <table id='"+ id +"_row2' class=\"mid table_window\">\
        <tr>\
          <td class='"+ className +"_w'></td>\
            <td id='"+ id +"_table_content' class='"+ className +"_content' valign='top'></td>\
          <td class='"+ className +"_e'></td>\
        </tr>\
      </table>\
        <table id='"+ id +"_row3' class=\"bot table_window\">\
        <tr>\
          <td class='"+ className +"_sw'></td>\
            <td class='"+ className +"_s'><div id='"+ id +"_bottom' class='status_bar'><span style='float:left; width:1px; height:1px'></span></div></td>\
            <td class='" + className + "_sizer' id='"+ id +"_estado'></td>\
        </tr>\
      </table>\
    ";

     this.capaContenedora =new Capa(this.id + '_v',{texto: tabla_grafica,clase_est: 'dialog'});
     this.capaTitulo = new Capa(this.id+'_tit',{texto: '',divPadre: document.getElementById(id +'_top')});
     this.capaTextoTitulo = new Capa(this.id+'_txttit',{texto: this.titulo,divPadre: this.capaTitulo.div});
     this.capaCerrar = new Capa(this.id+'_cerrtit',{texto: '',clase_est: className + '_close',divPadre: this.capaTitulo.div});
     this.capaContenido = new Capa(this.id+'_cont',{texto: this.html,clase_est: className + '_content',divPadre: document.getElementById(id +'_table_content')});
//     this.capaEstado = new Capa(this.id+'_est',{texto: '',clase_est: 'barraest',divPadre: this.capaContenedora.div});
//     this.capaTextoEstado = new Capa(this.id+'_est',{texto: '',clase_est: 'barraest',divPadre: this.capaEstado.div});
     this.capaResize = new Capa(this.id+'_resize',{texto: '',clase_est: className + '_sizer',divPadre: document.getElementById(id +'_estado')});

     this.capaContenido.div.style.overflow='auto';
     this.capaContenido.div.ventana = this; 
     this.capaContenido.div.idContenedor = this.capaContenedora.div.id; 
     this.capaContenido.div.onmousedown = function (event){
                                          alFrente(document.getElementById(this.idContenedor));
                                       };

     this.capaTextoTitulo.div.ventana = this; 
     this.capaTextoTitulo.div.idContenedor = this.capaContenedora.div.id; 
     this.capaTextoTitulo.div.onmousedown = function ini(event){
                                         dragStart(event, this.idContenedor); 
                                       };

     this.capaResize.div.ventana = this; 
     this.capaResize.div.idContenedor = this.capaContenedora.div.id; 
     this.capaResize.div.onmousedown = function ini(event){
                                         dragStart(event, this.idContenedor,{modo: 'resize',divAsociado: this.ventana.capaContenido.div}); 
                                       };

     this.capaCerrar.div.idContenedor = this.capaContenedora.div.id; 
     this.capaCerrar.div.ventanaAsociada = this; 
     this.capaCerrar.div.onclick = function destruir(event){
       this.ventanaAsociada.onClose();
       Effect.Fade(document.getElementById(this.idContenedor),{duration: 0.5, afterFinish: function(effect) { Element.remove(effect.element) }});
       };
 

//     }catch(e){
//       alert(e.name + " - " + e.message);
//     }
  }
  
  //Posicionar al medio del la pantalla visible actual.
  
  this.onClose = opciones.onClose	|| function (){alert('Cerrando ventana ' + this.id);};

  this.setAjaxContent= function(url, options) {
    options = options || {asynchronous:false,evalScripts: true};
    // Clear HTML (and even iframe)
    this.capaContenido.div.innerHTML = "";
    new Ajax.Updater(this.capaContenido.div, url,{asynchronous:false,evalScripts: true});
  };
  
  function crearID()
      {
        inferior=1000;superior=99999;
        numPosibilidades = superior - inferior;
        aleat = Math.random() * numPosibilidades;
        aleat = Math.round(aleat);
        return parseInt(inferior) + aleat;
      }

  this._center= function(top,left) {
    var windowScroll = WindowUtilities.getWindowScroll();    
    var pageSize = WindowUtilities.getPageSize();    
    if (typeof top == "undefined")
      top = (pageSize.windowHeight - (this.capaContenedora.div.scrollHeight))/2;
    top += windowScroll.top
    
    if (typeof left == "undefined")
      left = (pageSize.windowWidth - (this.capaContenedora.div.scrollWidth))/2;
    left += windowScroll.left      
//     this.setLocation(top, left);
    this.capaContenedora.div.style.left = left; 
    this.capaContenedora.div.style.top = top; 
//     this.toFront();
//    alert('centrando');
  };

  this.CrearVentana();
  this.anchoInicial= opciones.anchoInicial;
  if(this.anchoInicial!=null)
    this.capaContenedora.div.style.width = this.anchoInicial;
//  this.capaContenedora.div.style.opacity= 0;
  this.altoInicial= opciones.altoInicial;
//   alert(this.altoInicial);
//   if(this.altoInicial!=null)
//     this.capaContenedora.div.style.height = this.altoInicial;
  if(this.altoInicial!=null)
  {
    //alert('aaaa');
    this.capaContenido.div.style.height = this.altoInicial;
  }
  this._center();
  alFrente(this.capaContenedora.div);
  //Effect.MoveBy( this.capaContenedora.div, 5, -10 );
  //Effect.Appear(this.capaContenedora.div,{duration: 0.5, afterFinish: function(effect) {  }});
  //Effect.Highlight(this.capaContenedora.div.id, {startcolor:'#ff99ff', endcolor:'#999999'});
  
}

// ******************************************************************************** //
// ******************************************************************************** //
// ******************************************************************************** //
// ******************************************************************************** //
var WindowUtilities = {  
  // From dragdrop.js
  getWindowScroll: function(parent) {
    var T, L, W, H;
    parent = parent || document.body;              
    if (parent != document.body) {
      T = parent.scrollTop;
      L = parent.scrollLeft;
      W = parent.scrollWidth;
      H = parent.scrollHeight;
    } 
    else {
      var w = window;
      with (w.document) {
        if (w.document.documentElement && documentElement.scrollTop) {
          T = documentElement.scrollTop;
          L = documentElement.scrollLeft;
        } else if (w.document.body) {
          T = body.scrollTop;
          L = body.scrollLeft;
        }
        if (w.innerWidth) {
          W = w.innerWidth;
          H = w.innerHeight;
        } else if (w.document.documentElement && documentElement.clientWidth) {
          W = documentElement.clientWidth;
          H = documentElement.clientHeight;
        } else {
          W = body.offsetWidth;
          H = body.offsetHeight
        }
      }
    }
    return { top: T, left: L, width: W, height: H };
  }, 
  //
  // getPageSize()
  // Returns array with page width, height and window width, height
  // Core code from - quirksmode.org
  // Edit for Firefox by pHaez
  //
  getPageSize: function(parent){
    parent = parent || document.body;              
    var windowWidth, windowHeight;
    var pageHeight, pageWidth;
    if (parent != document.body) {
      windowWidth = parent.getWidth();
      windowHeight = parent.getHeight();                                
      pageWidth = parent.scrollWidth;
      pageHeight = parent.scrollHeight;                                
    } 
    else {
      var xScroll, yScroll;

      if (window.innerHeight && window.scrollMaxY) {  
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
      } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
      } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
      }


      if (self.innerHeight) {  // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
      } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
      } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
      }  

      // for small pages with total height less then height of the viewport
      if(yScroll < windowHeight){
        pageHeight = windowHeight;
      } else { 
        pageHeight = yScroll;
      }

      // for small pages with total width less then width of the viewport
      if(xScroll < windowWidth){  
        pageWidth = windowWidth;
      } else {
        pageWidth = xScroll;
      }
    }             
    return {pageWidth: pageWidth ,pageHeight: pageHeight , windowWidth: windowWidth, windowHeight: windowHeight};
  },

  disableScreen: function(className, overlayId, overlayOpacity, contentId, parent) {
    WindowUtilities.initLightbox(overlayId, className, function() {this._disableScreen(className, overlayId, overlayOpacity, contentId)}.bind(this), parent || document.body);
  },

  _disableScreen: function(className, overlayId, overlayOpacity, contentId) {
    // prep objects
    var objOverlay = $(overlayId);

    var pageSize = WindowUtilities.getPageSize(objOverlay.parentNode);

    // Hide select boxes as they will 'peek' through the image in IE, store old value
    if (contentId && Prototype.Browser.IE) {
      WindowUtilities._hideSelect();
      WindowUtilities._showSelect(contentId);
    }  
  
    // set height of Overlay to take up whole page and show
    objOverlay.style.height = (pageSize.pageHeight + 'px');
    objOverlay.style.display = 'none'; 
    if (overlayId == "overlay_modal" && Window.hasEffectLib && Windows.overlayShowEffectOptions) {
      objOverlay.overlayOpacity = overlayOpacity;
      new Effect.Appear(objOverlay, Object.extend({from: 0, to: overlayOpacity}, Windows.overlayShowEffectOptions));
    }
    else
      objOverlay.style.display = "block";
  },
  
  enableScreen: function(id) {
    id = id || 'overlay_modal';
    var objOverlay =  $(id);
    if (objOverlay) {
      // hide lightbox and overlay
      if (id == "overlay_modal" && Window.hasEffectLib && Windows.overlayHideEffectOptions)
        new Effect.Fade(objOverlay, Object.extend({from: objOverlay.overlayOpacity, to:0}, Windows.overlayHideEffectOptions));
      else {
        objOverlay.style.display = 'none';
        objOverlay.parentNode.removeChild(objOverlay);
      }
      
      // make select boxes visible using old value
      if (id != "__invisible__") 
        WindowUtilities._showSelect();
    }
  },

  _hideSelect: function(id) {
    if (Prototype.Browser.IE) {
      id = id ==  null ? "" : "#" + id + " ";
      $$(id + 'select').each(function(element) {
        if (! WindowUtilities.isDefined(element.oldVisibility)) {
          element.oldVisibility = element.style.visibility ? element.style.visibility : "visible";
          element.style.visibility = "hidden";
        }
      });
    }
  },
  
  _showSelect: function(id) {
    if (Prototype.Browser.IE) {
      id = id ==  null ? "" : "#" + id + " ";
      $$(id + 'select').each(function(element) {
        if (WindowUtilities.isDefined(element.oldVisibility)) {
          // Why?? Ask IE
          try {
            element.style.visibility = element.oldVisibility;
          } catch(e) {
            element.style.visibility = "visible";
          }
          element.oldVisibility = null;
        }
        else {
          if (element.style.visibility)
            element.style.visibility = "visible";
        }
      });
    }
  },

  isDefined: function(object) {
    return typeof(object) != "undefined" && object != null;
  },
  
  // initLightbox()
  // Function runs on window load, going through link tags looking for rel="lightbox".
  // These links receive onclick events that enable the lightbox display for their targets.
  // The function also inserts html markup at the top of the page which will be used as a
  // container for the overlay pattern and the inline image.
  initLightbox: function(id, className, doneHandler, parent) {
    // Already done, just update zIndex
    if ($(id)) {
      Element.setStyle(id, {zIndex: Windows.maxZIndex + 1});
      Windows.maxZIndex++;
      doneHandler();
    }
    // create overlay div and hardcode some functional styles (aesthetic styles are in CSS file)
    else {
      var objOverlay = document.createElement("div");
      objOverlay.setAttribute('id', id);
      objOverlay.className = "overlay_" + className
      objOverlay.style.display = 'none';
      objOverlay.style.position = 'absolute';
      objOverlay.style.top = '0';
      objOverlay.style.left = '0';
      objOverlay.style.zIndex = Windows.maxZIndex + 1;
      Windows.maxZIndex++;
      objOverlay.style.width = '100%';
      parent.insertBefore(objOverlay, parent.firstChild);
      if (Prototype.Browser.WebKit && id == "overlay_modal") {
        setTimeout(function() {doneHandler()}, 10);
      }
      else
        doneHandler();
    }    
  },
  
  setCookie: function(value, parameters) {
    document.cookie= parameters[0] + "=" + escape(value) +
      ((parameters[1]) ? "; expires=" + parameters[1].toGMTString() : "") +
      ((parameters[2]) ? "; path=" + parameters[2] : "") +
      ((parameters[3]) ? "; domain=" + parameters[3] : "") +
      ((parameters[4]) ? "; secure" : "");
  },

  getCookie: function(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
      begin = dc.indexOf(prefix);
      if (begin != 0) return null;
    } else {
      begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
      end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
  },
    
  _computeSize: function(content, id, width, height, margin, className) {
    var objBody = document.body;
    var tmpObj = document.createElement("div");
    tmpObj.setAttribute('id', id);
    tmpObj.className = className + "_content";

    if (height)
      tmpObj.style.height = height + "px"
    else
      tmpObj.style.width = width + "px"
  
    tmpObj.style.position = 'absolute';
    tmpObj.style.top = '0';
    tmpObj.style.left = '0';
    tmpObj.style.display = 'none';

    tmpObj.innerHTML = content;
    objBody.insertBefore(tmpObj, objBody.firstChild);

    var size;
    if (height)
      size = $(tmpObj).getDimensions().width + margin;
    else
      size = $(tmpObj).getDimensions().height + margin;
    objBody.removeChild(tmpObj);
    return size;
  }  
}
