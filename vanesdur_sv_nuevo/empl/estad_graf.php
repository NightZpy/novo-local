<?php

/************************************************************
*  Esta rutina Muestra Estad�sticas en dos Gr�ficos en Flash.

Puede Recibir y Tratar los Siguientes Par�metros.
$desde: L�mite Inferior de Fecha para la consulta. Si existe un campo llamado 
				fdesde en la consulta, se reemplaza por este valor. Si no viene como 
				par�metro pero est� definido en la consulta, se la reemplaza con la 
				Fecha Desde Predeterminada del Usuario de la Sesi�n Actual.
$hasta: L�mite Superior de Fecha para la consulta. Si existe un campo llamado 
				fhasta en la consulta, se reemplaza por este valor. Si no viene como 
				par�metro pero est� definido en la consulta, se la reemplaza con la 
				Fecha Hasta Predeterminada del Usuario de la Sesi�n Actual.
$i: Es el Identificador de la Estad�stica a mostrar.
$usuario: Es el Usuario de la Sesi�n Actual.
************************************************************/

//Buscar las fechas Desde y Hasta Predeterminadas del Usuario.
$qrystrfechas="SELECT * FROM fecha_manejo WHERE usuario='$usuario'";
$qryfechas = mysql_db_query($c_database,$qrystrfechas,$link);
$rowfechas = mysql_fetch_array($qryfechas);
if (!isset($desde))
	$desde=$rowfechas[fecha_desde];
if (!isset($hasta))
	$hasta=$rowfechas[fecha_hasta];
mysql_free_result($qryfechas);

//Leer la tabla de Estad�sticas los datos de la Estad�stica deseada.
$qrystr="SELECT * FROM estadisticas WHERE id_estadistica=$i  ";
$qry = mysql_db_query($c_database,$qrystr,$link);
$row = mysql_fetch_array($qry);
$qrystr0=$row[select];
$titulo=$row[titulo];
$subtitulo=$row[subtitulo];
$comentario=$row[comentario];

if ($row[limite]<>0) {$qrystr0.=" limit $row[limite]";}

//Reemplazo en la consulta a fdesde y fhasta con los valres por defecto o los que hayan
//venido como par�metro.
$qrystr0 = ereg_replace('fdesde', $desde, $qrystr0);
$qrystr0 = ereg_replace('fhasta', $hasta, $qrystr0);

//echo $qrystr0;

$qry0 = mysql_db_query($c_database,$qrystr0,$link);
//echo $qrystr0;

$colours[1] = "ff0000";
$colours[2] = "00ff00";
$colours[3] = "0000ff";
$colours[4] = "ffff00";
$colours[5] = "00ffff";
$colours[6] = "ff00ff";
$colours[7] = "990000";
$colours[8] = "009900";
$colours[9] = "000099";
$colours[10] = "999900";
$colours[11] = "009999";
$colours[12] = "990099";
$colours[13] = "ff0000";
$colours[14] = "00ff00";
$colours[15] = "0000ff";
$colours[16] = "ffff00";
$colours[17] = "00ffff";
$colours[18] = "ff00ff";
$colours[19] = "990000";
$colours[20] = "009900";


$ff=0;

$parampie="";
$val=array();
$tit=array();
 while ($row0 = mysql_fetch_array($qry0))
 { $ff=$ff+1;
  $parampie.="c$ff=0x$colours[$ff]&t$ff=$row0[ref]&v$ff=$row0[tot]&";
  $val[$ff]=$row0[tot];
  $tit[$ff]=$row0[ref];
 }
 $parampie.="can=$ff";
//echo $parampie;

echo"
<table border='0' width='100%'>
  <tr>
    <td width='100%' align='center'><font face=Arial size=4>$titulo</font></td>
  </tr>";
	//Si encuentra la cadena fdesde en la consulta final, es porque se filtra por fechas,
	//entonces muestro las fechas del filtrado.
	if (substr_count($row[select],'fdesde')>0)
  	{echo"
    <tr>
      <td width='100%' align='center'><font face=Arial size=2>
			Fecha Inicial: ". date("d/m/Y",strtotime($desde)) .", Fecha Final (no inclusive): ". date("d/m/Y",strtotime($hasta)) ."  		
  		</font>
  		</td>
    </tr>";}

echo"
  <tr>
    <td width='100%' align='center'><font face=Arial size=3>$subtitulo</font></td>
  </tr>
     <tr>
    <td width='100%' colspan='3' height='1' bgcolor=C85858></td>
  </tr>
  <tr>
    <td width='100%'>
      <table border='0' width='100%' cellspacing='0' cellpadding='0'>
        <tr>
          <td width='30%' align='center'>
            <table border='0' width='100%'>
               ";
               
for ($i = 1; $i <= $ff; $i++) {
   echo"      <tr>
               <td width='85%'><font face=Arial size=2><b>$tit[$i]</b> ($val[$i])</font></td>
                <td width='15%'>
                  <table border='1' width='100%'>
                    <tr>
                      <td width='100%' bgcolor='$colours[$i]' align='center'><b>$i</b></td>
                    </tr>
                  </table>
                </td>
               </tr>";
}



echo"

            </table>
          </td>

    <td width='70%' align='center'>
    <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' WIDTH='440' HEIGHT='320' id='barchart' ALIGN=''>
    <PARAM NAME=movie VALUE='flash/barchart.swf?$parampie'> <PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#FFFFFF> <EMBED src='flash/barchart.swf' quality=high bgcolor=#FFFFFF  WIDTH='440' HEIGHT='320' NAME='piechart' ALIGN=''
      TYPE='application/x-shockwave-flash' PLUGINSPAGE='http://www.macromedia.com/go/getflashplayer'></EMBED>
     </OBJECT></td>
  </tr>
         </table>
    </td>
  </tr>
  <tr>
    <td width='100%' colspan='3' height='1' bgcolor=C85858></td>
  </tr>
   <tr>
    <td width='100%'>
      <table border='0' width='100%' cellspacing='0' cellpadding='0'>
        <tr>
          <td width='65%' align='center'>
            <table border='0' width='100%'>
              <tr>
               <td width='100%' align='center'>
               <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' WIDTH='440' HEIGHT='320' id='piechart' ALIGN=''>
               <PARAM NAME=movie VALUE='flash/piechart.swf?$parampie'> <PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#FFFFFF> <EMBED src='flash/piechart.swf' quality=high bgcolor=#FFFFFF  WIDTH='440' HEIGHT='320' NAME='piechart' ALIGN=''
                TYPE='application/x-shockwave-flash' PLUGINSPAGE='http://www.macromedia.com/go/getflashplayer'></EMBED>
               </OBJECT>
               </td>
              </tr>
             </table>
           </td>
           <td width='35%'>
                  <table border='0' width='100%'>
                    <tr>
                      <td width='100%' align='left'><font face=Arial size=2>$comentario</font></td>
                    </tr>
                  </table>
                </td>
        </tr>
      </table>
      </td>
      <td>
      </td>
    </table>
  <tr>
    <td width='100%' colspan='3' height='1' bgcolor=C85858></td>
  </tr>";
?>
