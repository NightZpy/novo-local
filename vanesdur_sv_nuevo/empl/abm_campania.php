<?
//***********************************************************
//********** ESTO VA A IR EN OTRO ARCHIVO .inc **************
// Require Class Library
// =====================
 require('class.forms_ale5.php');

 //####### OBLIGATORIOS
 $tablasuper = "campanias";          //La tabla del Supertipo.
 $preftablasub = "";    //El prefijo de las tablas subtipo. Es por si se usa en el subtipo productos_auto, productos_hela,etc.
 $estapagina = "empl/abm_campania.php";   //El nombre de la p�gina de ABM. Esto es porque al grabar debe llamarse a s� misma.
 $par_pag_sig = "empl/listado_campanias.php";//La p�gina a mostrar si graba exitosamente.
 $titulo = "Gesti�n de Campa�as";   //El t�tulo a mostrar en el html.
 
 $conf_tablas = array();             //Array que contiene las opciones para las tablas a mostrar.

 //####### OPCIONALES
 //T�tulos de los campos.
 //****** Primer elemento del array de opciones de tablas.
 $conf_tablas["$tablasuper"] = new conf_tabla();   //La clase conf_tabla est� definida en el abm.
 //Campos usados de la BD.
 $conf_tablas["$tablasuper"]->campos_usar = array("id_campania","id_pedido");
 //T�tulos de los Campos A MOSTRAR. Se ponen t�tulos a todos los que se usan aunque algunos no se muestren.
 $conf_tablas["$tablasuper"]->titulos_campos = array("id_campania","Id Pedido Desde");
 //Campos ocultos de la tabla supertipo.
 $conf_tablas["$tablasuper"]->campos_hidden = array("id_campania");
 //Las validaciones de la tabla supertipo.
 // array_push($conf_tablas["$tablasuper"]->validaciones, "if (StringVacio('nombre_producto',Form.nombre_producto)) return false;");
 //Los COMBOS de la tabla supertipo.
 // array_push($conf_tablas["$tablasuper"]->combos, new ConfCombo("id_origen","localidades","localidad","id_loc"));

//***********************************************************
//******** FIN ESTO VA A IR EN OTRO ARCHIVO .inc ************



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
#           DB Layer Example Page - Inserting             #
#           Tobie van der Spuy - 2001                     #
#           glow@gamersinc.co.za                          #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


 /*Variables de entrada (de control)
 par_campania              -- El Registro a mostrar en el ABM. Si es "" es una ALTA.
 par_pag_sig                -- La p�gina a mostrar (header, dentro del Administrador) si el ABM es exitoso.
 accion_abm                -- La p�gina a mostrar (header, dentro del Administrador) si el ABM es exitoso.
 */


   // Prepare Form
   // ============
   $campania = $par_campania; //Esto se decid�a en un if m�s arriba, pero como ahora mando el id_campania que quiero aunque sea un alta, lo seteo ac�.
   //************* Ac� se declara el objeto Base de Datos con sus campos ******************
   $db1 = new MySQLDB("$c_database","$c_usuario","$c_password","$c_conexion");
   $db1->select();

   //************* Ac� se declara el objeto formulario con sus campos ******************
   $form = new Form("$tablasuper",$titulo,"administracion.php","","","","",$usuario,$sesion);
   $form->addHiddenForm("pagina",$estapagina); //As� se agregan todos los hidden que se necesiten. �ste es para que cargue la misma p�gina de abm, es OBLIGATORIO. Dejarlo.

   if ($accion_abm == 1)         //1 = ALTA.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar));//
     $form->addHiddenForm("id_campania",$campania);      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("par_campania",$campania);      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("accion_abm",$accion_abm);      //As� se pasa al momento de grabar el tipo de acci�n a realizar.
    }
   else                      //MODIFICACI�N.
    {
     $form->addtable($db1,"$tablasuper","","",implode(":",$conf_tablas["$tablasuper"]->campos_hidden),$accion_abm,"id_campania = '$campania'","","",implode(":",$conf_tablas["$tablasuper"]->campos_usar));//
     $form->addHiddenForm("par_campania",$campania);      //As� deber�a setearse el ID principal. Recordar poner como hidden al campo de la tabla.
     $form->addHiddenForm("accion_abm",$accion_abm);      //As� se pasa al momento de grabar el tipo de acci�n a realizar.
    }

   //Los t�tulos de los campos separados con /:/
   $form->describe(implode("/:/",array_merge($conf_tablas["$tablasuper"]->titulos_campos,$conf_tablas["$preftablasub".$tipo]->titulos_campos)));
//   $form->addHiddenForm("nombre_producto","de pecho");      //As� se setea un campo de pecho. No importa si se modifica por el usuario.

   //Los combos de la tabla Supertipo.
   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->combos);$v++)
    {
     $temp = $conf_tablas["$tablasuper"]->combos[$v];
     $temp->ponerEnForm($db1,$form);
    }

   //FABRICAR LOS CAMPOS.
   $form->makefields();

   for($v=0;$v<sizeof($conf_tablas["$tablasuper"]->validaciones);$v++)
     $form->addValidaciones($conf_tablas["$tablasuper"]->validaciones[$v]);


   //************* Ac� se define si hay que mostrar los input **************************
   //************* o si hay que actualizar los valores en la bd. ***********************
   // Si se apret� Enviar, hace un Submit y graba, si no muestra los campos para ingresar.
   // ===================================================



   if ($hacerenvio=='S')
    {
     if (sesion_ok($usuario,$sesion)=='0')
      {
       echo "La sesi�n ha caducado";
       //header("location:error.php");
      }
     else
      {
       if ($form->submit())
        {
		     if($renueva_sesion == 'S')
           $sesion = sesion_ok_ns($usuario,$sesion,4000);

         if($cierra_al_grabar == 'S')
           echo "<script language='javascript'>close();</script>";
         else
           include ($par_pag_sig);
 	    	}
	     else
        {
		     echo $error;
		     echo mysql_error();
		     echo "\n<br><a href=$PHP_SELF>Back</a>";
		    }
	    }
    }
   else
    {
   	 echo $form->build("200","10","200","3","40");
	  }

?>



