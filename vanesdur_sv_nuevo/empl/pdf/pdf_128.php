<?php
include ("../../conexion.php");
include("pdf_set1.php");
require_once('pdfbarcode128.inc');

//Iniciando PDF
$pdf=new PDF();
$pdf->Open();  	
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');



$nota="<br>$row[texto]<br>
<B>Gener�:  <U>$usuario</U></B> <br>
Generador autom�tica de reportes de <A href='http://www.vanesaduran.com'>
www.vanesaduran.com</A> | Consultas en <A href='mailto:soporte@iddelsur.com.ar'>
soporte@iddelsur.com.ar</A>,<br>";

$alto=5;
$header=array('Ob','D�a','Horas','1er I/O','2da I/O','3ra I/O','Comentario'); //encabezados de columnas (en todos)
//$anchos=array(9,30,20,42,42,42,92); //anchos de cada celda procurar que sumen aprox -
$anchos=array(5,20,15,28,28,28,70); //anchos de cada celda procurar que sumen aprox -
$alig=array('C','L','C','C','C','C','L'); //L,R,C
$total=array('','','','','','',''); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Reporte: Reloj de personal: $empleado ($usuarioreloj).";//nombre del reporte (en todos)
$notaalpie="  Gracias por seguir confiando en Vanesa Duran !!"; //nota al pie
// aca van los select del load data


$qrystr = "SELECT concat(ent2) as c
           FROM reporte
           WHERE clave_ses='-1'
           ORDER BY ent1 ASC";
// ---------------- fin variables ---------

$data=$pdf->LoadData($qrystr);
//print_r($data);

// ----------------------------------------
$alto = 4;
$pdf->SetFont('Arial','',10);
$pdf->AddPage('P'); 	
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$data,0);

//************* Impresi�n Total de Tiempo Trabajado.
$pdf->Ln();
$pdf->Ln();
//************* Fin Impresi�n  Total de Tiempo Trabajado.

$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);


    $code = new pdfbarcode128('abcjdfghfhfdskjhfkjdshfkjsdhdfkjghfd', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 2, 5);

    $code = new pdfbarcode128('!1234usuarioreloj!', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 22, 5);

    $code = new pdfbarcode128('1234', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 42, 5);

    $code = new pdfbarcode128('!1234!', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 62, 5);

    $code = new pdfbarcode128('abcjdfghfhfdskjhfkjdshfkjsdhdfkjghfd', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 82, 5);

    $code = new pdfbarcode128('|234�~', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 102, 5);


    $code = new pdfbarcode128('abcjdfghfhfdskjhfkjdshfkjsdhdfkjghfd', 2,"true" );
    $code->set_pdf_document($pdf);
    $width = $code->get_width();
    $code->draw_barcode(100, 122, 5);

$pdf->Output();
?>

