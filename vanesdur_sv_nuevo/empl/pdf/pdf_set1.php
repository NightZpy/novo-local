<?php
define('FPDF_FONTPATH','font/');
require('pdf_fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;

function PDF($orientation='P',$unit='mm',$format='A4')
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{   global $anchos;
    global $notaalpie;
	$this->Cell(array_sum($anchos),0,'','T');
    //Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	//Arial italic 8
	$this->SetFont('Arial','I',8);
	//N�mero de p�gina
	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
}

function header()
{global $header;
 global $anchos;
 global $reporte;
 $this->Image('vd_srl.jpg',10,8,65);
 //Arial bold 1510
 $this->SetFont('Arial','B',9);
 //Movernos a la derecha
 $this->Cell(80);
 //T�tulo
 $this->Cell(110,7,'DOCUMENTACION DE USO INTERNO',0,8,'L');
 $this->SetFont('Arial','B',8);
 $this->Cell(110,6,$reporte,0,8,'L');
 $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
 $this->Ln(10);
 $this->SetFont('Arial','B',9);
 for($i=0;$i<count($header);$i++)
        $this->Cell($anchos[$i],7,$header[$i],1,0,'C');
    $this->Ln();

}

function LoadData($qrystr)

{ global $link;
  global $c_database;
  $qry = mysql_db_query($c_database,$qrystr,$link);
  $data=array();
  while ($row = mysql_fetch_array($qry))
  {$line=$row[c];
   $data[]=explode(';',chop($line));}
  return $data;
}

//Tabla simple
function BasicTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    global $alto;
    //Datos
    $totales=array();
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
            {if ($tot==1) {
             switch ($total[$i]) {
                    case 1:
                          $totales[$i]=$totales[$i]+$col;
                    break;
                    case 2:
                          $totales[$i]=$totales[$i]+1;
                    break;
                    case 3:
                          $totales[$i]=$col;
                    break;
                    case 4:
                          $totales[$i]=$totales[$i]+$col;
                          $col=$totales[$i];
                    break;
                                   }
                             }
             $this->Cell($anchos[$i],$alto,$col,1,0,$alig[$i]);
             $i=$i+1;
            }
        $this->Ln();
        
    }
    if ($tot==1)
    { for($i=0;$i<count($header);$i++)
      {  switch ($total[$i]) {
                    case 1:
                          $this->Cell($anchos[$i],$alto,$totales[$i],1,0,$alig[$i]);
                    break;
                    case 4:
                          $this->Cell($anchos[$i],$alto,$col,1,0,$alig[$i]);
                    break;
                    default:
                          $this->Cell($anchos[$i],$alto,$total[$i],1,0,$alig[$i]);
                    break;
                               }
       }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //datos
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],$alto,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=1;
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],$alto,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
         }
     $this->Ln();
     $fill=!$fill;
    }
}

}

?>
