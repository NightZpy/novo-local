<?php

define('FPDF_FONTPATH','font/');
//echo $paso_raiz;

if($paso_raiz=='')
   $paso_raiz = "../../";
//echo $paso_raiz;
require('pdf_fpdf.php');
require_once $paso_raiz."config.php";
require ($paso_raiz."../clases/int_graf/jpgraph.php");
include ($paso_raiz."../clases/int_graf/jpgraph_pie.php");
include ($paso_raiz."../clases/int_graf/jpgraph_line.php");
include ($paso_raiz."../clases/int_graf/jpgraph_bar.php");

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;
var $altoFila;     //La Altura de la fila de datos
var $MaxRengFila;  //El tama�o m�ximo al que puede crecer un rengl�n.
var $header_si;
var $tipoHeader;
var $usuarioGenera;
var $tipoFooter;
var $cambiaEnGrupo;               //Campos en los que se hace cambio de grupo.
var $grupoActual;                 //Grupos para hacer cambio de grupo.
var $usarGrupos;                  //[True|False].
var $camposHidden;                //Columnas que no se muestran.
var $fuenteTitulos;               //Tama�o de la fuente en los t�tulos.
var $totales;                     //Array que contiene los totales.
var $encabezarGrupos;             //[True|False]. Muestra u oculta los t�tulos de columna al cambiar de grupo.
var $imprimirGrupos;              //[True|False]. Imprime los t�tulos del grupo que cambia.
var $subTotales;                     //Array de arrays que contienen los subtotales por cada columna agrupable.
var $mostrarSubtotales;           //[True|False]. Muestra u oculta los subtotales al cambiar de grupo.

  var $anchos_def;
  var $totales_def;
  var $alineacion_def;

function PDF($orientation='P',$unit='mm',$format='A4',$parAltoFila=5,$parMaxRengFila=0)
{
	//Llama al constructor de la clase padre
	$this->FPDF($orientation,$unit,$format);
	//Iniciaci�n de variables
	$this->B=0;
	$this->I=0;
	$this->U=0;
	$this->HREF='';
	$this->setAltoFila($parAltoFila);
	$this->MaxRengFila =  $parMaxRengFila;    //0=Ilimitado.
	$this->header_si = 1;
	$this->tipoFooter = 0;
  $this->cambiaEnGrupo=array();
  $this->camposHidden=array();
  $this->usarGrupos = false;
  $this->grupoActual = array();
  $this->fuenteTitulos=10;
  $this->totales=array();
  $this->encabezarGrupos=true;
  $this->imprimirGrupos=true;
  $this->subTotales=array();
  $this->mostrarSubtotales=true;
}

function setAltoFila($parAltoFila)
{
 $this->altoFila = $parAltoFila;
}

function getAltoFila()
{
 return $this->altoFila;
}

function WriteHTML($html)
{
	//Int�rprete de HTML
	$html=str_replace("\n",' ',$html);
	$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			//Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			//Etiqueta
			if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				//Extraer atributos
				$a2=explode(' ',$e);
				$tag=strtoupper(array_shift($a2));
				$attr=array();
				foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
						$attr[strtoupper($a3[1])]=$a3[2];
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag,$attr)
{
	//Etiqueta de apertura
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF=$attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	//Etiqueta de cierre
	if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF='';
}

function SetStyle($tag,$enable)
{
	//Modificar estilo y escoger la fuente correspondiente
	$this->$tag+=($enable ? 1 : -1);
	$style='';
	foreach(array('B','I','U') as $s)
		if($this->$s>0)
			$style.=$s;
	$this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
	//Escribir un hiper-enlace
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
function Footer()
{
 switch($this->tipoFooter)
 {
 	case 0:
	 	global $anchos;
    	global $notaalpie;
    	global $usuario;
    	global $reporte;
		//Posici�n: a 1,7 cm del final
		$this->SetY(-17);
		$this->Cell($this->getAnchoTabla(),0,'','T');
		$this->Ln();
		//Arial italic 8
		$this->SetFont('Arial','',5);
		//N�mero de p�gina
		$this->Cell(0,10,'Usuario:'.$usuario.'   Datos Generados el:'.date("d/m/Y H:i").'    Reporte:'.$reporte.'   Pagina '.$this->PageNo().'/{nb}'.$notaalpie,0,0,'C');
	break;
	case 1:
	break;
 }
}

function header()
{global $header;
 global $anchos;
 global $reporte;
 global $reporte1;
 global $renglones;
 global $cfg;
 global $paso_raiz;
  switch($this->tipoHeader)
 {
     case 0:
            $this->SetFillColor(255);
            $this->RoundedRect(2, 5, $this->w-4, 26, 3, 'FD', '1234');
            $this->SetFillColor(255);
            $this->RoundedRect(2, 31, $this->w-4, $this->h-38, 2, 'FD', '1234');
            $this->Image('pdf/vd_srl.jpg',10,8,65);
            $this->SetFont('Arial','B',9);
            $this->Cell(80);
            $this->Cell(110,$this->altoFila,$cfg['empresa_direccion'],0,8,'L');
            $this->Cell(110,$this->altoFila,$cfg['empresa_te'],0,8,'L');
            $this->SetFont('Arial','B',8);
            $this->Cell(110,6,$reporte,0,8,'L');
            $this->Cell(110,6,$reporte1,0,8,'L');
            $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
            $this->Ln(0);
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            $this->SetFont('Arial','B',$this->fuenteTitulos);
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }
     break;
     case 1:

            $this->Image('pdf/vd_srl.jpg',10,4,30);
            $this->SetFont('Arial','B',9);
            $this->Cell(80);
            $this->Cell(110,$this->altoFila,$cfg['empresa_direccion'],0,8,'L');
            $this->Cell(110,$this->altoFila,$cfg['empresa_te'],0,8,'L');
            $this->SetFont('Arial','B',8);
            $this->Cell(110,6,$reporte,0,8,'L');
            $this->Cell(110,6,$reporte1,0,8,'L');
            $this->Cell(110,6,'Datos Generados el:'.date("d/m/Y H:i"),0,8,'R');
            $this->Ln(0);
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            $this->SetFont('Arial','B',8);
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;
     case 2:
            $this->SetFont('Arial','B',$this->fuenteTitulos);
            $this->Cell(190,3,$reporte,0,3,'C');
            $this->Cell(190,3,$reporte1,0,3,'C');
            $this->SetFont('Arial','',$this->fuenteTitulos);
            $this->Cell(190,3,'Fecha:'.date("d/m/Y"),0,3,'R');
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;

    case 3:
            for($i=0;$i<count($renglones);$i++)
               {$this->Cell(190,6,$renglones[$i],0,8,'L');}
            if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;
     case 4:
            $this->SetFont('Arial','B',$this->fuenteTitulos);
             if($this->header_si==1)
              {
               $this->encabezarTabla();
              }

     break;
 }

}

function encabezarTabla()
{
 global $header;
 global $anchos;
 $this->OnEncabezarTabla();
 for($i=0;$i<count($header);$i++)
   if(!in_array($header[$i],$this->camposHidden))
     $this->Cell($anchos[$i],$this->altoFila,$header[$i],1,0,'C');
 $this->Ln();
}


function limpiar_consulta($qrystr){

$qrystr = str_replace(",';',",",",$qrystr);
$qrystr =str_replace("SELECT CONCAT(","SELECT ",$qrystr);
$qrystr =str_replace("SELECT concat(","SELECT ",$qrystr);
$qrystr =str_replace(") AS c","",$qrystr);
$qrystr =str_replace(") as c","",$qrystr);
$qrystr =str_replace(") As c","",$qrystr);

return $qrystr;
}


function LoadData($qrystr)

{ global $link;
  global $c_database;
  global $header;
  global $anchos;
  global $align;
  global $anchos_def;
  global $totales_def;
  global $alineacion_def;

  $qrystr = $this->limpiar_consulta($qrystr);
  //  echo"$qrystr<br>";

  $qry = mysql_db_query($c_database,$qrystr,$link);
  $err=mysql_error();
  if($err<>'')
    echo"$qrystr<br>$err";
$num_campos = mysql_num_fields($qry);
//echo $num_campos;
//--------------------------IMPRIME LAS COLUMNAS CON EL ANCHO DEPENDIENDO DEL ANCHO DE LA PAGINA--------------------
$anchopg=$this->w;
$margen_i=$this->lMargin;
$margen_d=$this->rMargin;
$ancho_impr = $anchopg - ($margen_d + $margen_i);
$ancho_col = $ancho_impr / $num_campos;

  for($i=0;$i<$num_campos;$i++)
  {
    $header[] = mysql_field_name($qry,$i);

    if(sizeof($anchos_def)==0){
        $anchos[] = $ancho_col;
    }

//    else
//        $anchos[] = $anchos_def;

    if(sizeof($alineacion_def)==0)
        $align[] = 'L';
//    else
//        $align[] = $alineacion_def;
  }

  //var_dump($header);

  $data=array();
  while ($row = mysql_fetch_row($qry))
  {
   $data[]=$row;
  }
  //var_dump($data);
  return $data;
}

//Tabla simple
function BasicTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    global $subtotal;
    if(!$subtotal)
      $subtotal=array();
    //Datos
    $this->totales=array();
    foreach($data as $row)
    {
       $this->OnEnterRow($row);
       // ****************** Manejo de Cambio de Grupos *************************** //
       if($this->usarGrupos)
        {
         $col_cambiadas=$this->CambioGrupo($row);
         if(sizeof($col_cambiadas)>0)
          {
           //Lanzar eventoOnGroupChange.
           $this->OnGroupChange($col_cambiadas);

           $header_si_temp=$this->header_si;
           $this->header_si=0;
           if($this->mostrarSubtotales)
            {
              if($vez++>0)//Para que no imprima subtotales en el inicio del informe.
               {
                $col_cambiadas_inv=array_reverse($col_cambiadas,true);
                foreach($col_cambiadas_inv as $col_cambiada)
                 {
                  if(sizeof($subtotal[$header[$col_cambiada]])<>0)
                   {
                    $this->MultiCell(100,$this->altoFila,'Subtotales ' . $header[$col_cambiada] ,0,'L');
                    $this->Ln();
                    $this->imprimirSubtotal($header[$col_cambiada]);
                   }
                 }
               }
             }

           if($this->imprimirGrupos)
            {
             foreach($col_cambiadas as $col_cambiada) //Si Cambia el grupo, mostrar una celda del ancho de la tabla.
              {
               $this->Ln();
               $this->MultiCell($this->getAnchoTabla(),$this->altoFila,$header[$col_cambiada] . ": " . $this->grupoActual[$col_cambiada],1,'C');
              }
             $this->Ln();
            }
           if($this->encabezarGrupos)
            {
             $font=$this->FontFamily;
             $est=$this->FontStyle;
             $tam=$this->FontSizePt;
             $this->SetFont('Arial','B',$this->fuenteTitulos);
             $this->encabezarTabla();
             $this->SetFont($font,$est,$tam);
            }
           $this->header_si=$header_si_temp;
          }
        }
       // ****************** Fin Manejo de Cambio de Grupos *************************** //

        $i=0;
        $paginaoriginal = $this->page;
        $max_renglones = 0;
        $columnas = array();
        foreach($row as $col)
            {if ($tot==1) {
             switch ($total[$i]) {
                    case 1:
                          $this->totales[$i]=$this->totales[$i]+$col;
                    break;
                    case 2:
                          $this->totales[$i]=$this->totales[$i]+1;
                    break;
                    case 3:
                          $this->totales[$i]=$col;
                    break;
                    case 4:
                          $this->totales[$i]=$this->totales[$i]+$col;
                          $col=$this->totales[$i];
                    break;
                                   }
                             }

             // ******** C�lculo de subtotales *********** //
             //Cada elemento de $subtotal es un array con banderas de totalizaci�n de columnas.
             foreach ($subtotal as $colagrupable=>$sub)
              {
               switch ($sub[$i])
                {
                  case 1:
                        $this->subTotales[$colagrupable][$i]=number_format($this->subTotales[$colagrupable][$i]+$col,2, '.', '');
                  break;
                  case 2:
                        $this->subTotales[$colagrupable][$i]=$this->subTotales[$colagrupable][$i]+1;
                  break;
                  case 3:
                        $this->subTotales[$colagrupable][$i]=$col;
                  break;
                  case 4:
                        $this->subTotales[$colagrupable][$i]=$this->subTotales[$colagrupable][$i]+$col;
                        $col=$this->subTotales[$colagrupable][$i];
                  break;
                }
              }
             // ****** Fin C�lculo de subtotales ********* //



           //Lanzar eventoOnPrintCell
           $this->OnPrintCell($i,$col);


	     //$col=number_format($col,2,',','.');
             $yoriginal = $this->y;
             //Parte a una celda en renglones que se almacenan en cada elemento de la matriz columnas.
             if(!in_array($header[$i],$this->camposHidden))
               $columnas[$i]=$this->ParteCell($anchos[$i],$col);
             //var_dump($columnas);exit;
             //Almaceno la cantidad m�xima de renglones en la que se haya desglosado una celda.
             if($max_renglones < sizeof($columnas[$i]))
               $max_renglones = sizeof($columnas[$i]);
             if($max_renglones > $this->MaxRengFila and $this->MaxRengFila > 0 )
               $max_renglones = $this->MaxRengFila;

             $i=$i+1;
            }
        //Ya se construy� la fila, ahora recorro las columnas imprimiendo de a un rengl�n a la vez.

        //Primero me fijo cu�ntos renglones entran en cada p�gina si es que hay un salto de p�gina.
        $renglones_p1 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 1.
        $renglones_p2 = 0;       //Ac� quedar� la cantidad de renglones a mostrar en la p�gina 2 si hay salto de p�gina.
        $yfinal = $this->y;
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          if($yfinal + $this->altoFila < $this->PageBreakTrigger)
            $renglones_p1++;
          else
            {
             $renglones_p2 = $max_renglones - $renglones_p1;
             break;
            }
          $yfinal += $this->altoFila;
         }

        //Ahora imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //S�lo los de la p�gina 1.
        $xact = $this->lMargin;
        for($colu=0;$colu<sizeof($row);$colu++)
         {
          if(!in_array($header[$colu],$this->camposHidden))
           {
            $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$this->y) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p1))* $this->k,'S'));
            $xact += $anchos[$colu];
           }
         }

        //Imprimo un registro de la BD en los renglones que se requieran.
        for($reng=0;$reng < $max_renglones;$reng++)
         {
          //Imprimo todas las columnas del rengl�n $reng del registro actual.
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            if(!in_array($header[$colu],$this->camposHidden))
             {
              $rengtemp = $columnas[$colu];// echo "<br>".var_dump($rengtemp);
              $this->MultiCell($anchos[$colu],$this->altoFila,$rengtemp[$reng],0,$alig[$colu]);
             }
           }
          $this->Ln();
         }

        //Imprimo un conjunto de marcos que ocupan el alto de todos los renglones que se requieran.
        //Los de la p�gina 2, si hubo salto de p�gina.
        $xact = $this->lMargin;
        if($renglones_p2>0)
          for($colu=0;$colu<sizeof($row);$colu++)
           {
            if(!in_array($header[$colu],$this->camposHidden))
             {
              $ydesde = $this->y - $renglones_p2 * $this->altoFila;//Le resto el alto de los renglones de la primera fila de celdas, ya que ya fueron impresas y se les hizo Ln() en el ciclo de arriba.
              $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$xact*$this->k,($this->h-$ydesde) * $this->k,$anchos[$colu] * $this->k,(-($this->altoFila * $renglones_p2))* $this->k,'S'));
              $xact += $anchos[$colu];
             }
           }

    }
    if($this->usarGrupos)
        {
         foreach($this->cambiaEnGrupo as $agrupable)
          {
           $colAgrupable = array_search($agrupable,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
           //Si el contenido de la columna agrupable de la fila actual cambi� respecto al �ltimo grupo registrado, mostrar una celda todo a lo ancho.
           if($this->grupoActual[$colAgrupable] <> $parArrFila[$colAgrupable])
            {
             $this->grupoActual[$colAgrupable] = $parArrFila[$colAgrupable];
             $col_cambiadas[]=$colAgrupable;
            }
          }
         //var_dump($col_cambiadas);
         $header_si_temp=$this->header_si;
         $this->header_si=0;
          if($this->mostrarSubtotales)
            {
             if($vez++>0)//Para que no imprima subtotales en el inicio del informe.
               {
                $col_cambiadas_inv=array_reverse($col_cambiadas,true);
                foreach($col_cambiadas_inv as $col_cambiada)
                {
                 if(sizeof($subtotal[$header[$col_cambiada]])<>0)
                   {
                    $this->MultiCell(100,$this->altoFila,'Subtotales ' . $header[$col_cambiada],0,'L');
                    $this->Ln();
                    $this->imprimirSubtotal($header[$col_cambiada]);
                   }
                }
              }
            }
         $this->header_si=$header_si_temp;
        }
    if ($tot==1)
    {
      if(sizeof($subtotal)>0)
      {
	    	$this->MultiCell(100,$this->altoFila,'Totales ',0,'L');
	      $this->Ln();

      }

    	for($i=0;$i<count($header);$i++)
      {
        if(!in_array($header[$i],$this->camposHidden))
         {

          switch ($total[$i]) {
                      case 1:
                            $valor_imprimir=$this->totales[$i];
		      break;
                      case 2:
                            $valor_imprimir=$this->totales[$i];
		      break;
                      case 4:
                            $valor_imprimir=$col;
		      break;
                      default:
                            $valor_imprimir=$total[$i];
		      break;
                                 }
          $this->OnPrintTotal($i,$valor_imprimir);
          $this->Cell($anchos[$i],$this->altoFila,$valor_imprimir,1,0,$alig[$i]);

         }
       }
     $this->Ln();
     }

}

//Una tabla m�s completa
function ImprovedTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //datos
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i]);
          $i=$i+1;
         }
     $this->Ln();
    }
    //L�nea de cierre

}

//Tabla coloreada
function FancyTable($header,$data,$tot)
{   global $anchos;
    global $alig;
    global $total;
    //Colores, ancho de l�nea y fuente en negrita
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.6);
    $this->SetFont('','B');
    //Restauraci�n de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Datos
    $fill=1;
    foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        {$this->Cell($anchos[$i],6,$row[$i],'LR',0,$alig[$i],$fill);
          $i=$i+1;
         }
     $this->Ln();
     $fill=!$fill;
    }
}

function CambioGrupo($parArrFila)
{
 global $header;
 $col_cambiadas=array();

 foreach($this->cambiaEnGrupo as $agrupable)
  {
   $colAgrupable = array_search($agrupable,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
   //Si el contenido de la columna agrupable de la fila actual cambi� respecto al �ltimo grupo registrado, mostrar una celda todo a lo ancho.
   if($this->grupoActual[$colAgrupable] <> $parArrFila[$colAgrupable])
    {
     $this->grupoActual[$colAgrupable] = $parArrFila[$colAgrupable];
     $col_cambiadas[]=$colAgrupable;

    }
  }
 return $col_cambiadas;
}
function getAnchoTabla()
{
 global $header;
 global $anchos;
 $ancho=array_sum($anchos);
 foreach ($this->camposHidden as $co)
  {
   $col = array_search($co,$header);  //Recupera la key (nro de columna) de la columna que agrupa.
   $ancho -= $anchos[$col];
  }
 return $ancho;
}

function imprimirSubtotal($colCambiada)
 {
  global $header;
  global $anchos;
  global $alig;
  global $subtotal;
  if(!$subtotal)
      $subtotal=array();

  for($i=0;$i<count($header);$i++)
   {
    if(!in_array($header[$i],$this->camposHidden))
     {
      switch ($subtotal[$colCambiada][$i])
       {
        case 1:
              $this->Cell($anchos[$i],$this->altoFila,$this->subTotales[$colCambiada][$i],1,0,$alig[$i]);
        break;
        case 2:
              $this->Cell($anchos[$i],$this->altoFila,$this->subTotales[$colCambiada][$i],1,0,$alig[$i]);
        break;
        case 4:
              $this->Cell($anchos[$i],$this->altoFila,$col,1,0,$alig[$i]);
        break;
        default:
              $this->Cell($anchos[$i],$this->altoFila,$subtotal[$colCambiada][$i],1,0,$alig[$i]);
        break;
       }
     }
    unset($this->subTotales[$colCambiada][$i]);
   }
  $this->Ln();
 }

//************************** M�todos Abstractos *************************************
//M�todo Abstracto a implementar en la clase hija.
function OnEnterMultiCell(&$contenido,&$columna,&$color_fondo,&$color_texto)
 {
 }

//M�todo Abstracto a implementar en la clase hija.
function OnEnterRow(&$parFila)
 {
 }
function OnEncabezarTabla()
{
}

//M�todo Abstracto a implementar en la clase hija.
function OnGroupChange(&$col_cambiadas)
 {

 }
 function OnPrintCell($num_col,&$valor)
 {
 }

function OnPrintTotal($num_col,&$valor)
 {
 }
function MEM_IMAGE($orientation='P',$unit='mm',$format='A4')
	{
		$this->FPDF($orientation, $unit, $format);
		//Register var stream protocol (requires PHP>=4.3.2)
		if(function_exists('stream_wrapper_register'))
			stream_wrapper_register('var','VariableStream');
	}

	//
	// PRIVATE FUNCTIONS
	//
	function _readstr($var, &$pos, $n)
	{
		//Read some bytes from string
		$string = substr($var, $pos, $n);
		$pos += $n;
		return $string;
	}

	function _readstr_int($var, &$pos)
	{
		//Read a 4-byte integer from string
		$i =ord($this->_readstr($var, $pos, 1))<<24;
		$i+=ord($this->_readstr($var, $pos, 1))<<16;
		$i+=ord($this->_readstr($var, $pos, 1))<<8;
		$i+=ord($this->_readstr($var, $pos, 1));
		return $i;
	}

	function _parsemempng($var)
	{
		$pos=0;
		//Check signature
		$sig = $this->_readstr($var,$pos, 8);
		if($sig != chr(137).'PNG'.chr(13).chr(10).chr(26).chr(10))
			$this->Error('Not a PNG image');
		//Read header chunk
		$this->_readstr($var,$pos,4);
		$ihdr = $this->_readstr($var,$pos,4);
		if( $ihdr != 'IHDR')
			$this->Error('Incorrect PNG Image');
		$w=$this->_readstr_int($var,$pos);
		$h=$this->_readstr_int($var,$pos);
		$bpc=ord($this->_readstr($var,$pos,1));
		if($bpc>8)
			$this->Error('16-bit depth not supported: '.$file);
		$ct=ord($this->_readstr($var,$pos,1));
		if($ct==0)
			$colspace='DeviceGray';
		elseif($ct==2)
			$colspace='DeviceRGB';
		elseif($ct==3)
			$colspace='Indexed';
		else
			$this->Error('Alpha channel not supported: '.$file);
		if(ord($this->_readstr($var,$pos,1))!=0)
			$this->Error('Unknown compression method: '.$file);
		if(ord($this->_readstr($var,$pos,1))!=0)
			$this->Error('Unknown filter method: '.$file);
		if(ord($this->_readstr($var,$pos,1))!=0)
			$this->Error('Interlacing not supported: '.$file);
		$this->_readstr($var,$pos,4);
		$parms='/DecodeParms <</Predictor 15 /Colors '.($ct==2 ? 3 : 1).' /BitsPerComponent '.$bpc.' /Columns '.$w.'>>';
		//Scan chunks looking for palette, transparency and image data
		$pal='';
		$trns='';
		$data='';
		do
		{
			$n=$this->_readstr_int($var,$pos);
			$type=$this->_readstr($var,$pos,4);
			if($type=='PLTE')
			{
				//Read palette
				$pal=$this->_readstr($var,$pos,$n);
				$this->_readstr($var,$pos,4);
			}
			elseif($type=='tRNS')
			{
				//Read transparency info
				$t=$this->_readstr($var,$pos,$n);
				if($ct==0)
					$trns=array(ord(substr($t,1,1)));
				elseif($ct==2)
					$trns=array(ord(substr($t,1,1)),ord(substr($t,3,1)),ord(substr($t,5,1)));
				else
				{
					$pos=strpos($t,chr(0));
					if(is_int($pos))
						$trns=array($pos);
				}
				$this->_readstr($var,$pos,4);
			}
			elseif($type=='IDAT')
			{
				//Read image data block
				$data.=$this->_readstr($var,$pos,$n);
				$this->_readstr($var,$pos,4);
			}
			elseif($type=='IEND')
				break;
			else
				$this->_readstr($var,$pos,$n+4);
		}
		while($n);
		if($colspace=='Indexed' and empty($pal))
			$this->Error('Missing palette in '.$file);
		return array('w'=>$w,
					 'h'=>$h,
					 'cs'=>$colspace,
					 'bpc'=>$bpc,
					 'f'=>'FlateDecode',
					 'parms'=>$parms,
					 'pal'=>$pal,
					 'trns'=>$trns,
					 'data'=>$data);
	}

	/********************/
	/* PUBLIC FUNCTIONS */
	/********************/
	function MemImage($data, $x, $y, $w=0, $h=0, $link='')
	{
		//Put the PNG image stored in $data
		$id = md5($data);
		if(!isset($this->images[$id]))
		{
			$info = $this->_parsemempng( $data );
			$info['i'] = count($this->images)+1;
			$this->images[$id]=$info;
		}
		else
			$info=$this->images[$id];

		//Automatic width and height calculation if needed
		if($w==0 and $h==0)
		{
			//Put image at 72 dpi
			$w=$info['w']/$this->k;
			$h=$info['h']/$this->k;
		}
		if($w==0)
			$w=$h*$info['w']/$info['h'];
		if($h==0)
			$h=$w*$info['h']/$info['w'];
		$this->_out(sprintf('q %.2f 0 0 %.2f %.2f %.2f cm /I%d Do Q',$w*$this->k,$h*$this->k,$x*$this->k,($this->h-($y+$h))*$this->k,$info['i']));
		if($link)
			$this->Link($x,$y,$w,$h,$link);
	}

	function GDImage($im, $x, $y, $w=0, $h=0, $link='')
	{
		//Put the GD image $im
		ob_start();
		imagepng($im);
		$data = ob_get_contents();
		ob_end_clean();
		$this->MemImage($data, $x, $y, $w, $h, $link);
	}
//0000000000000000000000000000000
 function bar($series_leg,$series_g,$titulo,$data,$ancho=190,$align='J')
 {


/// tama�o ubicacion
  $anchopx=700;
	$altopx=500;
	$relasp=$anchopx/$altopx;
  $margen=$this->lMargin;
  $anchopg=$this->w;
  //if($align=='L') {$margen=10;}
  if($align=='J') {$ancho=$anchopg-$margen-$this->rMargin;}
  if($align=='C') {$margen=$margen+($anchopg-$ancho)/2;}
  if($align=='R') {$margen=$margen+($anchopg-$ancho);}
  $espacio=intval($ancho/$relasp);
  $ydesde = 5 + $this->y;
  if ($espacio+$ydesde>=$this->h) {
                                    $this->AddPage();
                                    $ydesde =5+ $this->y;
                                   }
 	$this->Ln($espacio);
//////////////////////////
/// Create the graph.
$graph = new Graph(600,400);
$graph->SetScale("textlin");
$graph->SetMarginColor('white');

// Adjust the margin slightly so that we use the
// entire area (since we don't use a frame)
$graph->SetMargin(50,1,20,50);

// Box around plotarea
$graph->SetBox();

// No frame around the image
$graph->SetFrame(false);

// Setup the tab title
$graph->tabtitle->Set($titulo);
$graph->tabtitle->SetFont(FF_ARIAL,FS_BOLD,10);

// Setup the X and Y grid
$graph->ygrid->SetFill(true,'#DDDDDD@0.5','#BBBBBB@0.5');
$graph->ygrid->SetLineStyle('dashed');
$graph->ygrid->SetColor('gray');
$graph->xgrid->Show();
$graph->xgrid->SetLineStyle('dashed');
$graph->xgrid->SetColor('gray');

// Setup month as labels on the X-axis

$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
$graph->xaxis->SetLabelAngle(30);
$graph->legend->Pos(0.03,0.5,"right","center");


/////////////////// date
 //print_r($data);
 //echo " DATA <br>";
 $graficos=array();



for($colu=0;$colu<sizeof($series_g);$colu++)
{ $legend_x=array();
  $databar=array();
  $dataline=array();
  //ECHO "$colu >>>";
  $fcol_1=rand(20,200);
  $fcol_2=rand(20,200);
  $fcol_3=rand(20,200);

  $fcol=hexdec($fcol_1).hexdec($fcol_2).hexdec($fcol_3);
  $tcol=dechex(hexdec($fcol));
	$fcol='#'.$fcol;$tcol='#'.$tcol;
//	echo "$fcol $tcol <br>";
  foreach($data as $row)
      {
      switch ($series_g[$colu]) {
                      case 'P':
                           {array_push ($dataline,$row[$colu]);
                            //echo "$row[$colu] in p <br>";
                            }
                      break;
                      case 'B':
                          {array_push ($databar,$row[$colu]);
                           //echo "$row[$colu] in  b <br>";
                           }
		                  break;
                      case 'L':
                           {array_push ($legend_x,$row[$colu]);
                            //echo "$row[$colu] in l <br>";
                           }
                      break;
                      default:
		                  break;
                           }
         }
      switch ($series_g[$colu]) {
                      case 'P':
                           { // Create filled line plot
                            $lplot = new LinePlot($dataline);
                            $lplot->SetFillColor('skyblue@0.5');
                            $lplot->SetColor('navy@0.7');
                            $lplot->SetBarCenter();
                            $lplot->SetLegend($series_leg[$colu]);
                            $lplot->mark->SetType(MARK_SQUARE);
	         	                $lplot->mark->SetColor('blue@0.5');
                            $lplot->mark->SetFillColor('lightblue');
                            $lplot->mark->SetSize(6);
                            $graph->Add($lplot);
                            //echo "dibuja";PRINT_R($dataline);
                            }
                      break;
                      case 'B':
                           { $bplot = new BarPlot($databar);
                             $bplot->SetWidth(0.6);
                             $bplot->SetLegend($series_leg[$colu]);
                             $bplot->SetFillGradient($fcol,$tcol,GRAD_LEFT_REFLECTION);
                             $bplot->SetWeight(0);
                             array_push ($graficos,$bplot);
                             }
		                  break;
                      case "L":
                           { $graph->xaxis->SetTickLabels($legend_x);}
                      break;
                      default:
		                  break;
                           }


    }

  $gbplot = new GroupBarPlot($graficos);
  $gbplot->SetWidth(0.9);
  $graph->Add($gbplot);
  $img0=$graph->Stroke(_IMG_HANDLER);
  $this->GDImage($img0,$margen,$ydesde,$ancho);
 }


function pie($headerpie,$data,$titulo,$ancho=190,$align='J')
 {

	$pos_leg=0;
  $pos_serie=1;
//
  $legend=array();
  $datagr=array();
	foreach($data as $row)
    {   $i=0;
        foreach($row as $col)
        { if ($pos_leg==$i) array_push ($legend,$row[$i]);
          if ($pos_serie==$i) array_push ($datagr,$row[$i]);
          $i=$i+1;
         }
    }

  $anchopx=600;
	$altopx=400;
	$relasp=$anchopx/$altopx;
  $margen=$this->lMargin;
  $anchopg=$this->w;
  //if($align=='L') {$margen=10;}
  if($align=='J') {$ancho=$anchopg-$margen-$this->rMargin;}
  if($align=='C') {$margen=$margen+($anchopg-$ancho)/2;}
  if($align=='R') {$margen=$margen+($anchopg-$ancho);}
  $espacio=intval($ancho/$relasp);
  $ydesde = 5 + $this->y;
  if ($espacio+$ydesde>=$this->h) {
                                    $this->AddPage();
                                    $ydesde =5+ $this->y;
                                   }
 	$this->Ln($espacio);
  // Create the Pie Graph.
	$graph = new PieGraph($anchopx,$altopx,"auto");
	$graph->SetShadow();
	// Set A title for the plot
	$graph->title->Set("$titulo");
	$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);
	$graph->title->SetColor("brown");

	// Create pie plot
	//$datagr=asort($datagr);
	$p1 = new PiePlot($datagr);
  $p1->SetLegends($legend);
	//$p1->SetSliceColors(array("red","blue","yellow","green"));
	$p1->SetTheme("earth");
	$p1->value->SetFont(FF_ARIAL,FS_NORMAL,10);
	// Set how many pixels each slice should explode
  $p1->Explode(array(10,10,12));
	$graph->Add($p1);
  $img0=$graph->Stroke(_IMG_HANDLER);
  $this->GDImage($img0,$margen,$ydesde,$ancho);
 }

function barCode($x,$y,$barnumber,$code_height=30,$code_scale=1,$encode="I25",$colorFondo='#000000',$colorFrente='#FFFFFF',$tamanio_fuente=7)
{
  global $_im_barcode;
	$barra= new BARCODE();
	if($barra==false)
		die($barra->error());

	//$barnumber='91810122090012909062';
	$barra->setSymblogy($encode);
	$barra->setHeight($code_height);
	//$bar->setFont("arial");
	$barra->setScale($code_scale);
	$barra->setHexColor($colorFondo,$colorFrente);

	$_im_barcode='';      //Contendr� la imagen de las barras. EN la clase de las barras se declara como global.
	$return = $barra->genBarCode($barnumber,'png','');
	if($return==false)
		$barra->error(true);
	$this->GDImage($_im_barcode,$x,$y);
	@imagedestroy($_im_barcode);
  $tamanio_fuente_temp = $this->FontSize;
  $this->SetFontSize($tamanio_fuente);
	$this->Text($x, $y + $code_height/3, $barnumber);
  $this->FontSize = $tamanio_fuente_temp;

}

function RoundedRect($x, $y, $w, $h, $r, $style = '', $angle = '1234')
{
	$k = $this->k;
	$hp = $this->h;
	if($style=='F')
		$op='f';
	elseif($style=='FD' or $style=='DF')
		$op='B';
	else
		$op='S';
	$MyArc = 4/3 * (sqrt(2) - 1);
	$this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));

	$xc = $x+$w-$r;
	$yc = $y+$r;
	$this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));
	if (strpos($angle, '2')===false)
		$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k,($hp-$y)*$k ));
	else
		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

	$xc = $x+$w-$r;
	$yc = $y+$h-$r;
	$this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
	if (strpos($angle, '3')===false)
		$this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-($y+$h))*$k));
	else
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

	$xc = $x+$r;
	$yc = $y+$h-$r;
	$this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
	if (strpos($angle, '4')===false)
		$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-($y+$h))*$k));
	else
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

	$xc = $x+$r ;
	$yc = $y+$r;
	$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
	if (strpos($angle, '1')===false)
	{
		$this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$y)*$k ));
		$this->_out(sprintf('%.2f %.2f l',($x+$r)*$k,($hp-$y)*$k ));
	}
	else
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
	$this->_out($op);
}

function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
{
	$h = $this->h;
	$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
		$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
}
}//Fin Clase.
?>
