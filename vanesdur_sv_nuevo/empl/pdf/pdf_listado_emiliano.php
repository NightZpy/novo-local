<?php
include ("../../conexion.php");
include("pdf_set5.php");

//Recopilacion de Datos
$sesion=crear_clave_sesion();
$usuario = $_GET['usuario'];
$semilla = $_GET['semilla'];
$alto=4;
$nota="<br>Informe de Prueba<br>
<b>Generado por:  <u>$usuario</u></u><br>";

$header=array('Nombre','Edad','E-mail','Deposito','Fecha'); //encabezados de columnas (en todos)
$anchos=array(40,20,60,35,35); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','L','L','L'); //L,R,C
$total=array('Total',2,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$reporte="Reporte: Informe de Emiliano";//nombre del reporte (en todos)
$notaalpie="  Documentacion de Emiliano.!!"; //nota al pie
// aca van los select del load data
$qrystr = "SELECT concat(IFNULL(emiliano.nombre, ''),';',IFNULL(emiliano.edad, ''),';',IFNULL(emiliano.email, ''),';',IFNULL(deposito_tipos.nombre_tipo_deposito, ''),';',IFNULL(emiliano.fecha, '')) as c
           FROM emiliano
           LEFT JOIN deposito_tipos ON emiliano.deposito=deposito_tipos.id_tipo_deposito
           WHERE emiliano.nombre LIKE '%$semilla%'";
// ---------------- fin variables ---------
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$data=$pdf->LoadData($qrystr);
//print_r($data);
// ----------------------------------------

$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$data,1);
//$pdf->ImprovedTable($header,$data);
//$pdf->FancyTable($header,$data);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
?>
