<?php
include ("../../conexion.php");
include("pdf_set5.php");
//Recopilacion de Datos
$sesion=crear_clave_sesion();
$alto=4;
if($opcion==1)
 {
  $reporte="Reporte: Movimientos Interdepósitos Acumulado x Productos";//nombre del reporte (en todos)
  $campo = "SUM(mov.cantidad)";
  $filtro = "GROUP BY mov.id_vd";
 }
else
 {
  $reporte="Reporte: Movimientos Interdepósitos Detallado";//nombre del reporte (en todos)
  $campo = "mov.cantidad";
  $filtro = "";
 } 
 // Se le agrego un campo en la consulta - ent3 en el INSERT y prod.preciounidad en el SELECT
$qrystr = " INSERT INTO reporte (clave_ses,date1,ref1,ent1,ent2,ent3,ref2,ref3,ref4)
            SELECT '$sesion',mov.fecha_mov,mov.id_vd,mov.id_producto,$campo,prod.preciounidad,d_in.nombre_deposito,d_out.nombre_deposito,prod.nombreproducto
            FROM mov_stock AS mov
            INNER JOIN productos AS prod ON mov.id_vd = prod.id_vd AND mov.id_producto = prod.id_producto
            INNER JOIN depositos AS d_in ON mov.dep_in = d_in.nombre_columna
            INNER JOIN depositos AS d_out ON mov.dep_out = d_out.nombre_columna
            WHERE mov.fecha_mov >= '$fecha_d'
                  AND mov.fecha_mov <= '$fecha_h'
                  AND mov.dep_out = '$dep_desde'
                  AND mov.dep_in = '$dep_hasta'
             $filtro     
            ";
$qry = mysql_db_query($c_database,$qrystr,$link);

$err=mysql_error();
if($err<>'')
  {echo"$qrystr<br>$err";exit;}
//echo $qrystr;exit;
//instanciando... las variables
// $nota="<br><B>Generó:  <U>$usuario</U></B> <br>
// Generador automática de reportes de <A href='http://www.vanesaduran.com'>
// www.vanesaduran.com</A> | Consultas en <A href='mailto:gdemiguel@iddelsur.com.ar'>
// soporte@iddelsur.com.ar</A>,<br>";
$header=array('Fecha','Descripción','Cant.','P.Unit.','Dep. Desde','Dep. Hasta'); //encabezados de columnas (en todos)
$anchos=array(20,90,10,15,25,30); //anchos de cada celda procurar que sumen aprox 190-
$alig=array('L','L','R','R','L','L'); //L,R,C
$total=array('Total',2,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
$notaalpie="Documentacion exclusiva de AREA STOCK.!!"; //nota al pie
$reporte1 = "entre el $fecha_d y el $fecha_h";
// aca van los select del load data
if($opcion==1)
  $col1="'---'";// Fecha
else  
  $col1='date1';
  
$qrystr = "SELECT concat($col1,';',concat(ref4,' (',ref1,')'),';',ent2,';',ent3,';',ref3,';',ref2) as c
           FROM reporte
           WHERE clave_ses='$sesion'
           ORDER BY ref1 asc";
// ---------------- fin variables ---------
//Iniciando PDF
$pdf=new PDF();
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetTitle($reporte);
$pdf->SetAuthor($usuario.' (Adm) - Vanesa Duran');
$pdf->Setcreator('IDDelSur para VD');
$data=$pdf->LoadData($qrystr);
// ----------- borramos recopilacion de datos ----------
$qrystr = "DELETE FROM reporte WHERE clave_ses='$sesion'";
$qry = mysql_db_query($c_database,$qrystr,$link);
// ----------------------------------------
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$data,1);
$pdf->Cell(array_sum($anchos),0,'','T');//linea del todo el ancho de la tabla-
$pdf->Ln();
$pdf->WriteHTML($nota);
$pdf->Output();
?>
