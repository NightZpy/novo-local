<?php
//******************************************* CLASE MARCO *************************************************
//*********************************************************************************************************
class Marco 
{
 var $imagenes;
 var $tamanio;
 var $anchoMarco = "100%";
 var $alineamientoMarco = "center";
 var $directorio = ".";
 var $nombreArchivos;
 var $bgcolor='#990000';
 
 /**
  * Marco::Marco()
  * 
  * @param $parDirectorio
  * @return 
  */
 function Marco($parDirectorio="",$parNombreArchivos="",$parBgColor="")
 {
	if($parDirectorio<>"")
	 {
  	  $this->setDirectorio($parDirectorio);
      if($parNombreArchivos<>"")
       {
	    $this->nombreArchivos = $parNombreArchivos;
	    $this->cargarImagenes();
       }
      else
        $this->nombreArchivos = $parDirectorio."_";
	  $this->cargarImagenes();
	 }

    if ($parBgColor<>"")
      $this->setBgColor($parBgColor); 	
 }
 
 /**
  * Marco::setDirectorio()
  * 
  * @param $parDirectorio
  * @return 
  */
 function setDirectorio($parDirectorio)
 {
	$this->directorio = $parDirectorio;
	if($parDirectorio<>"" and $this->nombreArchivos == "")
	  $this->nombreArchivos = $parDirectorio."_";
 }
 
 function setBgColor($parBgColor)
 {
  if($parBgColor<>"" )
    $this->bgcolor = $parBgColor;
 }

 function getBgColor()
 {
    return $this->bgcolor;
 }
  /**
  * Marco::setNombreArchivos()
  * 
  * @param $parNombreArchivos
  * @return 
  */
 function setNombreArchivos($parNombreArchivos)
 {
	$this->nombreArchivos = $parNombreArchivos;
 }

 /**
  * Marco::cargarImagenes()
  * 
  * @return 
  */
 function cargarImagenes()
 {
	for($i=1;$i<10;$i++)
   {
	  if($i<>5)
	    {
       $this->imagenes[$i]=$this->directorio."/".$this->nombreArchivos."0$i.gif";
       $this->tamanio[$i] = GetImageSize($this->imagenes[$i],&$info);	
	    }
   }
 }
 
 /**
  * Marco :: abrirMarco()
  * 
  * @return 
  */
 function abrirMarco()
 {
  echo "<table width ='".$this->anchoMarco."' border='0' cellpadding='0' cellspacing='0' align='".$this->alineamientoMarco."'>
          <tr>
            <td >
              <img  src='".$this->imagenes[1]."' ".$this->tamanio[1][3]."></td>
            <td background='".$this->imagenes[2]."' ></td>
            <td >
              <img src='".$this->imagenes[3]."' ".$this->tamanio[3][3]."></td>
          </tr>
          
					<tr>
            <td background='".$this->imagenes[4]."' ></td>
            <td bgcolor=".$this->bgcolor." width=100% align=center>";
 }

 /**
  * Marco :: cerrarMarco()
  * 
  * @return 
  */
 function cerrarMarco()
 {
  echo "</TD>
            <td background='".$this->imagenes[6]."' ></td>
          </tr>
          <tr>
            <td>
              <img src='".$this->imagenes[7]."'  ".$this->tamanio[7][3]."></td>
            <td background='".$this->imagenes[8]."' ".$this->tamanio[8][3]."></td>
            <td >
              <img src='".$this->imagenes[9]."' ".$this->tamanio[9][3]."></td>
          </tr>
        </table>";
 }

 /**
  * Marco :: setAnchoMarco()
  * 
  * @return 
  */
 function setAnchoMarco($parAnchoMarco)
 {
	//Primero valido la entrada.
  if((!is_numeric($parAnchoMarco) and !is_numeric(substr($parAnchoMarco,0,strlen($parAnchoMarco)-1)))or(substr($parAnchoMarco,strlen($parAnchoMarco)-1,1)<>'%' and !is_numeric(substr($parAnchoMarco,strlen($parAnchoMarco)-1,1))))
		{
		 $this->mostrarError($parAnchoMarco." No es un Ancho v�lido","setAnchoMarco");
		 return;
		} 
	//Todo OK, sigo.
  $this->anchoMarco = $parAnchoMarco;
 }

 /**
  * Marco :: setAlineamientoMarco()
  * 
  * @return 
  */
 function setAlineamientoMarco($parAlineamientoMarco)
 {
	//Primero valido la entrada.
	$validos = array("CENTER","LEFT","RIGHT","JUSIFY");
  if(!in_array(strtoupper($parAlineamientoMarco),$validos))
		{
		 $this->mostrarError($parAlineamientoMarco." No es un alineamiento v�lido","setAlineamientoMarco");
		 return;
		} 
	//Todo OK, sigo.
	$this->alineamientoMarco = $parAlineamientoMarco;
 }

 /**
  * Marco::mostrarError()
  * 
  * @param $parError
  * @param $fuente
  * @return 
  */
 function mostrarError($parError,$fuente="Indeterminado")
 {
  echo "<br>ERROR: Clase Marco->$fuente: ".$parError."<br>";
 }
}
//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************

//Texto de Prueba:
/*$a=new Marco("tabla_bordeaux");
//$a=new Marco("");
$a->setAnchoMarco("220000");
$a->setAlineamientoMarco("center");
//$a->setDirectorio("tabla_bordeaux");
//$a->setNombreArchivos("tabla_bordeaux_");
$a->cargarImagenes();

$a->abrirMarco();
echo " <span class=cremita align=center>
      <br> <br> Ingrese Un Comentario Para este Turno <br> <br> <br></span>";
$a->cerrarMarco();*/

//******************************************* CLASE PAGINADOR *********************************************
//*********************************************************************************************************

class Paginador
 {
  var $pagina;
  var $cantidadPaginacion;

  function Paginador($parPagina,$parCantPag)
   {
    $this->pagina = $parPagina;
    $this->cantidadPaginacion = $parCantPag;
   }

  function escribirAccesos($parDesde)
   {
    $accesos="";
    $desde = $parDesde;
    $desdeant = $desde - $this->cantidadPaginacion;
    if($desdeant>=0)
      $dirant = "<a href=".$this->pagina."&desde=$desdeant>Anterior</a>";
    else
      $dirant = "Anterior";

    $desdeprox = $desde + $this->cantidadPaginacion;

    $accesos.= "
          <table width=760 CELLSPACING=0 >
            <tr height='19'>
            </tr>
            <tr height='19'>";
    $accesos.= "
              <td width='7%'>$dirant</td>";
    $accesos.= "
              <td width='10%'><a href=".$this->pagina."&desde=$desdeprox>Siguiente</a></td>";
    $accesos.= "
              <td width='83%' >";
    for($i=0;$i<10;$i++)
     {
      if (($i*$this->cantidadPaginacion) == $desde)
        $accesos.= "
             <a href=".$this->pagina."&desde=".$i*$this->cantidadPaginacion."><b> ".($i+1)." </b></a>";
      else
        $accesos.= "
             <a href=".$this->pagina."&desde=".$i*$this->cantidadPaginacion."> ".($i+1)." </a>";
     }
    $accesos.= "
             </td>";

    $accesos.= "
            <tr height='19'>
          </table>";
    return $accesos;
   }

  function limitar($parDesde)
   {
    $desde = $parDesde;
    return " limit $desde,".$this->cantidadPaginacion." ";
   }
 }

//******************************************* FIN CLASE ***************************************************
//*********************************************************************************************************

/*Ejemplo de uso

$paginador = new Paginador("administracion.php?usuario=$usuario&sesion=$sesion&pagina=empl/alta_prov_ven.php",$cantidad_paginacion);
$qrystr = "select * from usuario
           where psw = 'pswpswpsw' ".$paginador->limitar($desde);
dfhghglkhjfghjgjh
fghfgh
ddhdh
echo $paginador->escribirAccesos($desde);*/

?>
