<?php
/*
�ndice de Lugares en lo que hay que realizar modificaciones
-----------------------------------------------------------

 - Seteos generales de panel
 - on enter row
 - Preparaci�n de filtros
 - Encabezamiento y filtros
 - switch acciones ejecutadas en el mismo panel
 - paginaci�n
 - select principal
 - Definici�n de campos
 - Pie

FIN �ndice de Lugares en lo que hay que realizar modificaciones
*/

// ***************************************************************************************************************************** //
//Seteos generales de panel
// ***************************************************************************************************************************** //
//$no_incluir_fecha=true;
require_once "empl/ajax_lib3.inc";
require_once "empl/panel_func.inc";
require_once "../clases/interfaz/acciones2.php";


//$paso_images_gif="images_gif";
$t=$g_bgolor;
// *1
if($panel=='')
  $panel=1;
$datos_panel = obtener_datos_panel($panel);

//var_dump($datos_panel);
if($ent=='')
  $ent=$datos_panel['entidad'];//'me';

// ******* Buscar �reas del panel ************* //
$_arr_restricciones['area_trabajo'] = obtener_areas_panel($panel);
// ***** FIN Buscar �reas del panel *********** //

// *2
if($id_area_trabajo=='')
  $id_area_trabajo='SA';


$arr_objetos_marcados=obtener_objetos_marcados($datos_panel['tipo_objeto']);

// ***************************************************************************************************************************** //
//FIN Seteos generales de panel
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//on enter row
// ***************************************************************************************************************************** //

  //lanzadores de acciones

  //acciones sueltas

  //otros
class ListasImp extends Listas
{
	function OnEnterRow(&$parFila)
	{

		global $usuario;
		global $sesion;
		global $c_database;
		global $link;
		global $paso_images_gif;
		global $class_name;
		global $id_area_trabajo;
		global $usuver;
		global $ent;
		global $icon_previa;
		global $recien_eliminada;
		global $ga;
    global $datos_panel;
		global $arr_objetos_marcados;
		global $id_solicitud;
		global $gif;

    $arr_botones = array();
    $p_registro=$parFila['id'];
    $p_tipo_tomado='tomado_x_usu';
    $p_estado=$parFila['estado'];//.$estado_anexo;
    $p_idhtml=$this->idHtml . "_row". $this->fila;
    $p_inicio_conteo_fila=$this->fila;
    $p_parametros_extra='';
    $p_tipo_objeto=$datos_panel['tipo_objeto'];

    //Decidir la est�tica de la fila.
    // aca ver de poner los colores en la tabla o marcas en la tabla objeto estado agregar objeto a estetica
    $arr_estetica_fila = estetica_x_estado($parFila['id'],$parFila['estado'],$datos_panel['tipo_objeto']);
    $this->clasePar=$arr_estetica_fila['clasePar'];
    $this->claseImpar=$arr_estetica_fila['claseImpar'];
    //FIN Decidir la est�tica de la fila.

    //Decidir qu� hacer si est� marcado o no. **************************************************************
    if(in_array($parFila['id'], $arr_objetos_marcados) and $parFila['id']<>$recien_eliminada)
      $imagen_marca="$paso_images_gif/bandera_encendida.gif";
    else
      $imagen_marca="$paso_images_gif/bandera_apagada.gif";
    //FIN Decidir qu� hacer si est� marcado o no. **********************************************************

    //Banderita de marcar/desmarcar ************************************************************************
    $paso_marcar="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/solicitudes_comp_panel.php&id_reserva=$id_reserva&id_comp_res=$parFila[id]&no_incluir_ajax=true";
    $arr_botones[] ="<a ONCLICK=\"javascript: setContenido('" . $this->idHtml . "_row". $this->fila ."','$paso_marcar&id_html=" . $this->idHtml . "_row". $this->fila ."&fila_unica=$parFila[id]&inicio_conteo_fila=". $this->fila ."&id_contenido=$id_contenido&no_incluir_ajax=true&bandera=44');\"><img border=0 src=$imagen_marca></a>";
    //FIN Banderita de marcar/desmarcar ********************************************************************
    $arr_botones[] ="<img src='$paso_images_gif/borde_izq.gif' border=0>";

        //Determinar el gr�fico acorde a tipo de prioridad que tiene . *****************************************
    switch ($parFila['prio'])
     {
      case 0:
        $imagen_prior="$paso_images_gif/advertencia_gris.gif";
    	break;
      case 1:
        $imagen_prior="$paso_images_gif/advertencia_amarillo.gif";
    	break;
      case 2:
        $imagen_prior="$paso_images_gif/advertencia_anaranjado.gif";
    	break;
      case 3:
        $imagen_prior="$paso_images_gif/advertencia_rojo.gif";
    	break;
     }
    //FIN Determinar el gr�fico acorde a tipo de prioridad que tiene . *************************************

    //BOT�N de prioridad ***********************************************************************************
    $paso_prior="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/solicitudes_comp_panel.php&id_solicitud=$parFila[id]&no_incluir_ajax=true";
    $arr_botones[] ="<a ONCLICK=\"javascript: setContenido('" . $this->idHtml . "_row". $this->fila ."','$paso_prior&id_html=" . $this->idHtml . "_row". $this->fila ."&fila_unica=$parFila[id]&inicio_conteo_fila=". $this->fila ."&id_contenido=$id_contenido&no_incluir_ajax=true&bandera=45');\"><img border=0 src=$imagen_prior></a>";
    //FIN BOT�N de prioridad *******************************************************************************




    //Decidir el bot�n lanzador del men� en funci�n del estado_tomado. *************************************
    if($p_tipo_tomado=='tomado_x_usu')
      $icono_opc="opc_tomado.gif";
    else
      $icono_opc="opc_no_tomado.gif";
    //FIN Decidir el bot�n lanzador del men� en funci�n del estado_tomado. *********************************
//
    //Bot�n del mensajero **********************************************************************************
    $gif="$paso_images_gif/gifs/el.gif";

    if ($parFila['em_a_cl']==1) {$gif="$paso_images_gif/gifs/enl.gif";}
    if ($parFila['cl_a_em']==1) {$gif="$paso_images_gif/gifs/snl_ease.gif";}
//       if ($ent=="ce")
    if ($ent=="me")
    {
      if ($parFila['em_a_cl']==1) {$gif="$paso_images_gif/gifs/snl_ease.gif";}
      if ($parFila['cl_a_em']==1) {$gif="$paso_images_gif/gifs/enl.gif";}
    }
    $paso_revisualizar="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/solicitudes_comp_panel.php&id_solicitud=$parFila[id]";
    $paso_mensaje="admin_vacia.php?pagina=empl/mensaje_lista_solicitud_ajx.php&usuario=$usuario&sesion=$sesion&id_solicitud=$parFila[id]&ent=$ent&no_incluir_ajax=true";
    $lanzador_mensaje = "<a style='cursor: pointer;' ONCLICK=\"javascript: var vent1 =new Ventana({titulo:'Mensajes Solicitud $parFila[id]',anchoInicial: '700px',altoInicial: '100%',
    onClose: function(){recargarFilaTabla('" . $this->idHtml . "_row". $this->fila ."',$this->fila,'$parFila[id]','&id_solicitud=$parFila[id]');},
    className: '$class_name'});vent1.setAjaxContent('$paso_mensaje&id_contenido='+vent1.capaContenido.div.id, {method: 'POST',asynchronous:false , evalScripts: true});vent1.capaContenedora.div.style.top=window.scrollY;\"><img  border=0 src='$gif' ></a>";
    $arr_botones[] =$lanzador_mensaje;
    //FIN Bot�n del mensajero ******************************************************************************


     //LANZADOR DE ACCIONES *********************************************************************************
     $arr_botones[] = "<img id='gb$parFila[id]' onmouseover=\"menu_tooltip1.mostrar_opc(this,{ido:$p_registro,tt:'$p_tipo_tomado',e:'$p_estado',se:'$p_subestado',idh:'$p_idhtml',icf:$p_inicio_conteo_fila,to:$p_tipo_objeto,ag:'',p:'$p_parametros_extra'});\" onmouseout=\"menu_tooltip1.prog_ocultar_opc();\" border=0 src=$paso_images_gif/$icono_opc>";
     //FIN LANZADOR DE ACCIONES *****************************************************************************
    $arr_botones[] ="<img src='$paso_images_gif/borde_der.gif' border=0>";
    $parFila['opc'] =arma_botonera($arr_botones);

//    $arr_bot_suelt = $ga->cargar_acciones_sueltas(3,$this->idHtml . "_row". $this->fila,$this->fila,$p_tipo_tomado,$p_estado,$subestado,1,'0');
//    foreach ($arr_bot_suelt as $accion_suelta)
//      $arr_botones[] = $accion_suelta;
    //FIN LANZADOR DE ACCIONES *****************************************************************************

	}
}

  // **************************************************************************************** //
  // **************************************************************************************** //
// ***************************************************************************************************************************** //
//FIN on enter row
// ***************************************************************************************************************************** //


// ***************************************************************************************************************************** //
//Preparaci�n de filtros
// ***************************************************************************************************************************** //
// *****************************FILTROS *********************************************************** //


  if($_ampliando<>'')
    $fila_unica='';

  //Obtener los estados en los que se pueden mostrar las reservas.
  if($id_area_trabajo=='' or $fila_unica<>'')
    $fil_estado= " ";//$fil_estado= " AND ped.estado in (20,21,22,25,28,29,30,31) ";
  else
  {
    $qrystr_at = "SELECT estado
                  FROM `pnl_area_estado`
                  WHERE id_area='$id_area_trabajo'
                    AND tipo_objeto='". $datos_panel['tipo_objeto'] ."'
                  GROUP BY estado
                 ";
    $qry_at= mysql_db_query($c_database,$qrystr_at,$link);
//     echo"$qrystr_at<br />";
    verificar('',$qrystr_at);
    $arr_fil_estado=array();
    while($row_at = mysql_fetch_array($qry_at))
      $arr_fil_estado[]=$row_at['estado'];

    $fil_estado= " AND ped.estado in ('" . implode("','", $arr_fil_estado) . "') ";
  }
  //FIN Obtener los estados en los que se pueden mostrar las reservas.

  if($fila_unica<>'')
  {
    $_modo_muestra = 'fila_unica';
    if($id_solicitud=='')
      $id_solicitud=$fila_unica;
    $filtro_unico = " AND s.id_solicitud='$id_solicitud' ";
  }
  if($filtro_txt<>'')
    $filtro_txt1 = " AND ((ped.pax LIKE('%$filtro_txt%')) OR (ped.id_reserva_ver='$filtro_txt')) ";

  if($_arr_restricciones['agencia']<>'')
    $usuver_hidden=$_arr_restricciones['agencia'];
  $usuver=$usuver_hidden;
  if($usuver<>'')
    $filtro_usu = " AND usu.id_lider='$usuver' ";

// end Filtros********************************************************************************** //
// ***************************************************************************************************************************** //
//FIN Preparaci�n de filtros
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//Encabezamiento y filtros
// ***************************************************************************************************************************** //

//Inicio de buffer para que no salga por pantalla a�n
ob_start();

//if($_ampliando<>'')
//{
//  $qrystr_r = "SELECT * FROM `reservas` WHERE id_reserva='$id_reserva'";
//  $qry_r= mysql_db_query($c_database,$qrystr_r,$link);
//  verificar('',$qrystr_ru);
//  $row_r = mysql_fetch_array($qry_r);
//  $_ampliando_arr_estetica_fila = estetica_x_estado($id_reserva,$row_r['estado']);
//
//  $_ampliando_mostrar=$row_r['id_reserva_ver'];
//  $_ampliando_minimizar="<a ONCLICK=\"javascript: recargarFilaTabla('$id_html',$inicio_conteo_fila,$id_reserva,'');\"><img border=0 src=$paso_images_gif/arbol_menos.gif></a>";
//}

echo "
<table class=list border=0 cellspacing=0 cellpadding=0><tr>
<td class=encabezado >";

echo"
</td>
</tr></table>
<script language='JavaScript'>
function ValidarDatos(Form)
{
//       if(Form.param_hidden.value=='' || Form.param_hidden.value=='NINGUNO')
//        {
//         alert('Ingrese el Usuario.');
//         return false;
//        }
//        Form.cod.value=Form.param_hidden.value;
  return true;
}
buscar_eg2 = function (pag,formu)
{
 formu.pagina.value = pag;
 formu._modo_muestra.value = 'contenido';
 protoSendSimple(formu,'".$id_contenido."_t_r');
}
";
echo"
EnterPulsado = function(e)
{
     var characterCode;
    if(e && e.which)
    {
      e = e;
      characterCode = e.which;
    }
     else
     {
       characterCode = e.keyCode;
     }
   if(characterCode == 13 )
  {
     return true;
  }
    else
  {
    return false;
  }
}
reiniciarBusqueda = function(tipo)
{
  if(tipo==1)//Si no hay restricci�n de agencia.
  {
    $('usuver').value='';
    $('usuver_hidden').value='';
    $('filtro_txt').value='';
    $('usuver').focus();
  }
  if(tipo==2)//Si hay restricci�n de agencia.
  {
    $('filtro_txt').value='';
    $('filtro_txt').focus();
  }
}
recargarFilaTabla = function(par_idHtmlFila,par_fila,par_id,par_extra)
{
  var idHtmlTabla=$(par_idHtmlFila).parentNode.id;
  setContenido(par_idHtmlFila,$(idHtmlTabla).paso_paginacion + '&id_html=' + par_idHtmlFila +'&fila_unica=' + par_id + '&inicio_conteo_fila=' + par_fila + '&id_contenido=$id_contenido&no_incluir_ajax=true' + par_extra,par_idHtmlFila + '_col0');
//   $(par_idHtmlFila + '_col0').visualEffect('highlight',{duration:4.5});
}
recargarTabla = function(par_idHtmlFila)
{
  var idHtmlTabla=$(par_idHtmlFila).parentNode.id;
  setContenido(idHtmlTabla,$(idHtmlTabla).paso_paginacion + '&id_contenido=$id_contenido&no_incluir_ajax=true&_modo_muestra=contenido');
//   $(par_idHtmlFila + '_col0').visualEffect('highlight',{duration:4.5});
}
</script>
";
    echo"
    <script type='text/javascript' src='".$paso_js."menu_tooltip_idd.js'></script>
    <script language='JavaScript'>

    var menu_tooltip1 = new menu_tooltip('_mostrador_opc','_mostrador_opc_cont');

    menu_tooltip1.rellenarOpciones =function ()
    {
      var buf='';
      var id_objeto = this.objeto_ref.id;
      var tipo_tomado = this.parametros.tt;
      var estado = this.parametros.e;
      //tipo_tomado = 'no_tomado';

      mis_acciones = cargar_acciones(tipo_tomado,estado);
      	num_acciones = mis_acciones.length;
    	//para cada acci�n del array, la introduzco en el men�
    	for(i=0;i<num_acciones;i++)
      {
          buf  +='<span width=100% onmouseover=\"this.style.color=\\'AA0000\\'\" onmouseout=\"this.style.color=\\'5555FF\\'\" ONCLICK=\"ejecuta_accion(' + mis_acciones[i] + ',' + this.parametros.res + ',' + this.parametros.idh + ',' + this.parametros.icf + ')\" style=\"cursor:pointer\"><b> � ' + obtener_detalle_accion(mis_acciones[i],'descri_accion') + ' </b></span><br>';
    	}


      return buf;
    };
    ";

    echo "
      </script>
      ";
//Almacaner el buffer en una variable que se usa despu�s
$_pnl_encabezamiento_y_filtros = ob_get_contents();

//Fin de buffer
ob_end_clean();

// ***************************************************************************************************************************** //
//FIN Encabezamiento y filtros
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//switch acciones ejecutadas en el mismo panel
// ***************************************************************************************************************************** //
switch($bandera)
{
 case 44:      //Cambiar marca
   $bandera=0;
   marcar_objeto($id_solicitud,$arr_objetos_marcados,$datos_panel['tipo_objeto']);

   $_modo_muestra = 'fila_unica';

 break;
  case 45:      //Cambiar prioridad
   $bandera=0;
   $qrystrd = " SELECT det FROM solicitudes WHERE id_solicitud='$id_solicitud'";
//      echo "$qrystrd<br />";
   $qryd= mysql_db_query($c_database,$qrystrd,$link);
   verificar($qrystrd);
   $rowd=mysql_fetch_array($qryd);
   if($rowd[0]==3)
     $proxprior=0;
   elseif($rowd[0]==2)
     $proxprior=3;
   elseif($rowd[0]==1)
     $proxprior=2;
   else
     $proxprior=1;

   $qrystrd1 = " UPDATE solicitudes SET det='$proxprior' WHERE id_solicitud='$id_solicitud'";
//      echo "$qrystrd1<br />";
   $qryd1= mysql_db_query($c_database,$qrystrd1,$link);
   verificar($qrystrd1);

   $_modo_muestra = 'fila_unica';

 break;
}
// ***************************************************************************************************************************** //
//FIN switch acciones ejecutadas en el mismo panel
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//paginaci�n
// ***************************************************************************************************************************** //
$paso_paginacion = "admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/solicitudes_comp_panel.php&id_reserva=$id_reserva&id_contenido=$id_contenido&id_area_trabajo=$id_area_trabajo&ent=$ent";
$cantidad_paginacion=30;
// ***************************************************************************************************************************** //
//FIN paginaci�n
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//select principal
// ***************************************************************************************************************************** //
  // ******************************** SELECCION ************************************************************* //
//*8

$qrystr = " SELECT s.id_solicitud AS id, s.id_solicitud AS opc, concat(p.nombreproducto,' (',p.id_vd,')') AS repuesto,
                   s.cantidad_p AS cant, s.usu_pedido AS usu_ped ,s.obs AS obs, s.estado AS estado, s.det AS prio, s.em_a_cl, s.cl_a_em
            FROM `solicitudes` as s
            INNER JOIN productos AS p ON s.id_repuesto = p.id_vd
            WHERE 1
              $filtro_unico
            ORDER BY s.id_solicitud
          ";
// echo $qrystr;
// ****************************** FIN SELECCION ********************************************************** //

// ***************************************************************************************************************************** //
//FIN select principal
// ***************************************************************************************************************************** //

// ***************************************************************************************************************************** //
//Definici�n de campos
// ***************************************************************************************************************************** //
$lista = new ListasImp();
$lista->mostrarNombreGrupos=false;
// ******************************** DATOS LISTA DEL PANEL ************************************************************* //
$lista->titulo = "<b>COMPONENTES DE SOLICITUDES ENCONTRADOS</b>";

// $paso_nuevo="admin_vacia.php?usuario=$usuario&sesion=$sesion&pagina=empl/abm_grupos.php&id_tipo_grupo=$id_tipo_grupo&desde=$desde&id_contenido=$id_contenido";
$lista->pasoNuevo = "";
//*7 verificar sufijo de id_dontenido en el resto del script
$lista->idHtml = $id_contenido."_".$id_reserva."_t_cr";

$lista->addCol("id","id","R",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("opc","opc","L",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("repuesto","repuesto","L",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("cant","cantidad","R",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("usu_ped","usuario","L",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("obs","obs","L",0);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("estado","estado","L",1);//$nombre,$titulo,$alineacion,$hidden)
$lista->addCol("prio","prio","L",1);//$nombre,$titulo,$alineacion,$hidden)

//$lista->cambiaEnGrupo=array("tipo_comp");
//$lista->usarGrupos = true;

// $lista->subTotal['tipo_comp']=array('Cantidad','',2,1,1,1,1,1,1,1,1,1,1,1); // texto, 1 (suma), 2 (cuenta), 3 (ultimo reg), 4 (saldo) va acumulando renglon por renglon funciona si se llama la tabla con 1 en tot
// $lista->mostrarSubtotales=true;           //[True|False]. Muestra u oculta los subtotales al cambiar de grupo.
// $lista->encabezarGrupos=true;             //[True|False]. Muestra u oculta los t�tulos de columna al cambiar de grupo.
// $lista->imprimirGrupos=true;              //[True|False]. Imprime los t�tulos del grupo que cambia.
// $lista->mostrarSubtotales=true;           //[True|False]. Muestra u oculta los subtotales al cambiar de grupo.

// ****************************** FIN DATOS LISTA DEL PANEL *********************************************************** //

// ***************************************************************************************************************************** //
//FIN campos
// ***************************************************************************************************************************** //

include "empl/panel.php";
// ***************************************************************************************************************************** //
// ***************************************************************************************************************************** //
//Pie
// ***************************************************************************************************************************** //
//Inicio de buffer para que no salga por pantalla a�n
ob_start();

//Almacaner el buffer en una variable que se usa despu�s
$_pnl_pie = ob_get_contents();
//Fin de buffer
ob_end_clean();
// ***************************************************************************************************************************** //
//FIN Pie
// ***************************************************************************************************************************** //

?>
