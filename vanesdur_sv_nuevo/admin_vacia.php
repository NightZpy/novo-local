<?php
require_once "config.php";
require_once ("conexion.php");

header('Content-Type: text/html; charset=ISO-8859-1');

if(sesion_ok($usuario,$sesion)<>1)
{
  echo "<script language=javascript>alert('SESI�N NO V�LIDA');</script>";
}
else
{
  //header('Content-Type: text/html; charset=UTF-8');
  require_once ("lib_php.php");
  require_once "../clases/interfaz/clases_interfaz2.php";
  $marcofondo = new Marco($paso_fondo,"tr_",$t,"png");
  $marcofrente = new Marco($paso_frente,"tf_",$t,"png");
  $___validada=true;

  if($___id_accion<>'')
  {
    $___valid_accion= validar_accion($panel,$area,$___id_accion,$tipo_objeto,$id_objeto,$usuario);
    if($___valid_accion<>'')
      $___validada=false;
  }

  if($___validada==true)
    include($pagina);
  else
    echo $___valid_accion;  
}
?>
