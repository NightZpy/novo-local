	<title>Vanesa Duran</title>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
	<link rel="stylesheet" href="css/estilos.css"/>
	<!--[if lt IE 9]>
	<script>
	  var e = ("abbr,article,aside,audio,canvas,datalist,details," +
		"figure,footer,header,hgroup,mark,menu,meter,nav,output," +
		"progress,section,time,video").split(',');
	  for (var i = 0; i < e.length; i++) {
		document.createElement(e[i]);
	  }
	</script>
	<![endif]-->

	<!--menu-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){ $("#navmenu-h li,#navmenu-v li").hover( function() { $(this).addClass("iehover"); }, function() { $(this).removeClass("iehover"); } ); });
	</script>
	<!--end menu-->

	<!--scroll banner -->
	<script src="slide/jquery.tools.min.js"></script>
	<link rel="stylesheet" type="text/css" href="slide/scrollable-navigator.css" /> 
    <link rel="stylesheet" type="text/css" href="slide/scrollable-buttons.css" /> 
    <link rel="stylesheet" type="text/css" href="slide/scrollable-horizontal.css" /> 
    <script type="text/javascript">
		$(document).ready(function() {
			$(".scroll").scrollable({size: 1, loop: true}).autoscroll({
			autoplay: true,
			api: true,
			steps:2
			});;
		
		});
    </script>
	<!--end scroll banner -->
	
	<!--menu derecho-->
	<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
	<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css"></script>
	<script>
	$(function() {
		$("#noticias").accordion({
			collapsible: true
		});
	});
	$(function() {
		$("#filtro").accordion({
			collapsible: true
		});
	});
	</script>
	<link rel="stylesheet" type="text/css" href="css/view.css" media="all">
	<script type="text/javascript" src="js/view.js"></script>
	<!--Multi file-->
	<script src="js/jquery.MultiFile.js" type="text/javascript"></script>
	
	<!-- lightbox 0.5 -->
  <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->
    
    <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
          $('a.open').lightBox();
    });
    </script>
