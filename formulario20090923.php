<script language="JavaScript">
function visibilidad(nombre,imgnom)
{

  // ocultamos o mostramos segun la variable valor
  if(document.getElementById(nombre).style.visibility!='visible')
    {
     document.getElementById(imgnom).src='images_gif/carpeta_a.gif';
     //document.getElementById(imgnom).width=20;
     document.getElementById(nombre).style.visibility='visible';
     document.getElementById(nombre).style.display='';
    }
  else
    {
     document.getElementById(imgnom).src='images_gif/carpeta_c.gif';
     //document.getElementById(imgnom).width=20;
     document.getElementById(nombre).style.visibility='hidden';
     document.getElementById(nombre).style.display='none';
    }
}
function comparacodigo(Campo,Nro)
{
  if (Campo.value==Nro)
  {
      alert("ERROR: El " + Campo + " es igual");
      Campo.focus();
      return true;
   }
  else
   alert("ERROR: El " + Campo + " diferente a " + Campo);
   Campo.focus();
    return false;
}

function AbrirVentana(imagen)
    {
      ventana=open("","MiPropiaVentana","width=300, height=200, toolbar=no,directories=no,menubar=no,status=no");
      ventana.document.write("<HTML>");
      ventana.document.write('<HEAD><TITLE>Foto</TITLE></HEAD>');
      ventana.document.write('<BODY bgcolor=#b8b4d8 link="#cc6600" vlink="#66cc00" alink="#66cc00">');
      ventana.document.write('<p align=center><img border="0" src=' + imagen + '></p>');
      ventana.document.write('<p align="center"><font face="arial" size="3" color="#cc6600"><b><a href="javascript:close()"><b>cerrar</b></a></font></p>')
      ventana.document.write("</BODY>");
      ventana.document.write("</HTML>");
    }

// Funciones varias con formularios
  function llenar_campo(campo,valor)
  {
    campo = valor
  }

// Funciones de Validacion de datos

function esNumerico(chr)
{
        if (chr >= '0' && chr <= '9') return true;
        else return false;
}

function Stringmenor(CampoNom,Campo,largo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length < largo))
  {
      alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function Stringmenorovacio(CampoNom,Campo,largo)
{
  if ((Campo.value.length > 0) && (Campo.value.length < largo))
  {
      alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
      Campo.focus();
      return true;
   }
  else
    return false;
}
function StringVacio(CampoNom,Campo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length==0))
  {
      alert("ERROR: El " + CampoNom + " no puede estar vacio");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function AnioValido(CampoNom,Campo,Desde,Hasta)
{
        if (Campo.value.length!=4)
        {
                alert("ERROR: El a�o no es correcto");
                Campo.focus();
                return false;
        }
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if (!esNumerico(chr))
                {
                        alert("ERROR: El a�o no es correcto");
                        Campo.focus();
                        return false;
                }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El a�o no es correcto");
              Campo.focus();
              return false;
          }


return true;
}

function PrecioValido(CampoNom,Campo,Desde,Hasta)
{
        var puntosa=0;

        if (Campo.value.length==0)
        {
                alert("ERROR: Debe ingresar " + CampoNom);
                Campo.focus();
                return true;
        }


        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if ( (chr==".") )
                   puntosa=puntosa+1;

                if (puntosa > 1)
                  {
                   alert("ERROR:" + CampoNom + " No es un monto valido " );
                   Campo.focus();
                   return true;
                  }

                if (!((esNumerico(chr)) || (chr == ".")))
                  {
                   alert("ERROR: Debe ingresar un n�mero");
                   Campo.focus();
                   return true;
                  }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El n�mero debe estar entre " + Desde + " y " + Hasta);
              Campo.focus();
              return true;
          }


        return false;

}


function NumeroValido(CampoNom,Campo,Desde,Hasta)
{
        if (Campo.value.length==0)
        {
                alert("ERROR: Debe ingresar " + CampoNom);
                Campo.focus();
                return true;
        }
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if (!esNumerico(chr))
                {
                        alert("ERROR: Debe ingresar un n�mero");
                        Campo.focus();
                        return true;
                }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El n�mero debe estar entre " + Desde + " y " + Hasta);
              Campo.focus();
              return true;
          }


return false;
}

function MailValido(CampoNom,Campo)
{
var chr;
var arrobas=0;
var puntos=0;
var blancos=0;
if  ((Campo!=null) && (Campo.value.length!=0))
{
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if ( (chr=="@") )
                        arrobas=arrobas+1;
                if ( (chr==".") )
                        puntos=puntos+1;
                if ( (chr==" ") )
                        blancos=blancos+1;
        }

        if ( (blancos!=0) || (arrobas!=1) || (puntos<1) )
        {
                alert("ERROR: La direcci\363n de correo no es correcta.Recuerda que no se adminten espacios en blanco y que debe contener el car\341cter arroba (@) y el car\341cter punto (.)");
                Campo.focus();
                return false;
        }
}
return true;
}


//calcular la edad de una persona
//recibe la fecha como un string en formato espa�ol
//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual
function calcular_edad(fecha){

    //calculo la fecha de hoy
    hoy=new Date()
    //alert(hoy)

    //calculo la fecha que recibo
    //La descompongo en un array
    var array_fecha = fecha.split("-");
    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length!=3)
       return false;

    //compruebo que los ano, mes, dia son correctos
    var ano
    ano = parseInt(array_fecha[0]);
    if (isNaN(ano))
       return false

    var mes
    mes = parseInt(array_fecha[1]);
    if (isNaN(mes))
       return false

    var dia
    dia = parseInt(array_fecha[2]);
    if (isNaN(dia))
       return false

    //resto los a�os de las dos fechas
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o

    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
       return edad
    if (hoy.getMonth() + 1 - mes > 0)
       return edad+1

    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
       return edad + 1

    return edad
}



function EdadMenor(CampoNom,Campo,Anios)
{

  if (calcular_edad(Campo.value)<18)
  {
      alert("ERROR: " + CampoNom + " es menor de " + Anios + ". ");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function CaracterValido(control)
{
var chr;
var novalido=0;
if ((control.value!=null) && (control.value.length!=0))
  {
    for (var i=0;i<control.value.length;i++)
        {
         chr=control.value.substring(i,i+1);
         if ( (chr==';') || (chr==',') || (chr=='%') || (chr=='\"') || (chr=='\`') || (chr=='\�') || (chr=='\'') || (chr=='\\') )
            novalido=novalido+1;
        }
        if ( (novalido!=0) )
        {
          alert('ERROR: No se adminten los siguientes caracteres(  ;  ,  %  \`  \�  \\  \'  \"  ), Por favor reemplace por caracteres validos o quitelos ');
          control.focus();
          return false;
        }
  }
return true;
}
</script>

<?php
function advertencia($adv)
{ global $stylefield;
  global $link;
  global $c_database;
  global $agregadv;
  if (!$agregadv) $agregadv="";
  $qrystr = "select * from advertencia where adv = $adv";
  $qry = mysql_db_query($c_database,$qrystr ,$link);
  $row = mysql_fetch_array($qry);
  if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
				<tr height='19'>
                    <td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
					<td height='19' background='img/tabla_02.gif'><br>
					</td>
					<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
				</tr>
				<tr>
					<td width='23' background='img/tabla_04.gif'><br>
					</td>
					<td>
						<table border=0>
                          <tr>
                            <td width='4%' rowspan='2' align='center'>
                            <img border='0' width='20' height='94'src='images_gif/advertencia.gif'></td>
                            <td width='47%'><font size='4'>Informacion:</font></td>
                          </tr>
                          <tr>
                            <td width='47%' align='justify'><font face='Arial'>$row[texto] $agregadv</td>
                          </tr>
                        </table>
					</td>
					<td width='22' background='img/tabla_06.gif'><br>
					</td>
				</tr>
				<tr height='22'>
					<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
					<td height='22' background='img/tabla_08.gif'><br>
					</td>
					<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
     	            </tr>
                    </table>
                    ";}
}

function advertencia_informes($filtro1,$filtro2)
{ global $stylefield;
  global $link;
  global $c_database;
  global $agregadv;
  if (!$agregadv) $agregadv="";
  $qrystr = "select * from adv_filtro where filtro1 = '$filtro1' AND filtro2= '$filtro2'";
  $qry = mysql_db_query($c_database,$qrystr ,$link);
  $row = mysql_fetch_array($qry);
  if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
				<tr height='19'>
                    <td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
					<td height='19' background='img/tabla_02.gif'><br>
					</td>
					<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
				</tr>
				<tr>
					<td width='23' background='img/tabla_04.gif'><br>
					</td>
					<td>
						<table border=0>
                          <tr>
                            <td width='4%' rowspan='2' align='center'>
                            <img border='0' width='20' height='94'src='images_gif/advertencia.gif'></td>
                            <td width='47%'><font size='4'>Informacion:</font></td>
                          </tr>
                          <tr>
                            <td width='47%' align='justify'><font face='Arial'>$row[texto] $agregadv</td>
                          </tr>
                        </table>
					</td>
					<td width='22' background='img/tabla_06.gif'><br>
					</td>
				</tr>
				<tr height='22'>
					<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
					<td height='22' background='img/tabla_08.gif'><br>
					</td>
					<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
     	            </tr>
                    </table>
                    ";}
}

 function advertenciaswf($adv)
{ global $stylefield;
  global $link;
  global $c_database;
  global $agregadv;

  if (!$agregadv) $agregadv="";
  $qrystr = "select * from advertencia where adv = $adv";
  $qry = mysql_db_query($c_database,$qrystr ,$link);
  $row = mysql_fetch_array($qry);
  if ($row[activo]==1) {echo"<table border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFDF0'>
				<tr height='19'>
                    <td width='23' height='19'><img src='img/tabla_01.gif' alt='' height='19' width='23'></td>
					<td height='19' background='img/tabla_02.gif'><br>
					</td>
					<td width='22' height='19'><img src='img/tabla_03.gif' alt='' height='19' width='22'></td>
				</tr>
				<tr>
					<td width='23' background='img/tabla_04.gif'><br>
					</td>
					<td>
						<table border=0>
                          <tr>
                            <td width='4%' rowspan='2' align='center'>
                            <img border='0' width='20' height='94' src='images_gif/advertencia.gif'></td>
                            <td width='47%'><font size='4'>Informacion:</font></td>
                            <td width='300' HEIGHT='180' rowspan='2'>
                              <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' WIDTH='400' HEIGHT='180'>
                                <param NAME='movie' VALUE='flash3.swf'>
                                <param NAME='quality' VALUE='high'>
                                <param NAME='bgcolor' VALUE='#FFFDF2'><embed src='flash3.swf' quality='high' bgcolor='#FFFDF2' WIDTH='400' HEIGHT='180' TYPE='application/x-shockwave-flash' PLUGINSPAGE='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
                              </object>
                            </td>
                          </tr>
                          <tr>
                            <td width='47%' align='justify'><font face='Arial'>$row[texto]
                              </td>
                          </tr>
                          <tr>
                            <td width='51%' align='center' colspan='3'>
                            $agregadv</td>
                          </tr>
                        </table>
					</td>
					<td width='22' background='img/tabla_06.gif'><br>
					</td>
				</tr>
				<tr height='22'>
					<td width='23' height='22'><img src='img/tabla_07.gif' alt='' height='22' width='23'></td>
					<td height='22' background='img/tabla_08.gif'><br>
					</td>
					<td width='22' height='22'><img src='img/tabla_09.gif' alt='' height='22' width='22'></td>
     	            </tr>
                    </table>
                    ";}
}
function poner_combo_a_medida($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida)
{
   global $stylefield;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_a_medida_1blanco($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida)
{
   global $stylefield;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
    echo "<option value=0></option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_a_medida_1texto($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$_campo_id_html='')
{
   global $stylecombo;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   if($_campo_id_html<>'')
     $_campo_id_html1="id='$_campo_id_html'";
   echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
   echo "<option value=$valor>$texto</option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}

    function poner_combo_a_medida_3texto($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$_campo_id_html='',$texto1,$valor1,$texto2,$valor2)
{
   global $stylecombo;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   if($_campo_id_html<>'')
     $_campo_id_html1="id='$_campo_id_html'";
   echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
   echo "<option value=$valor>$texto</option>";
    echo "<option value=$valor1>$texto1</option>";
    echo "<option value=$valor2>$texto2</option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}


function poner_combo_1texto($nom_combo,$tabla,$campo_mostrar,$campo_clave,$texto,$valor)
{
   global $estilo;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
   echo "<option value=$valor>$texto</option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}




function poner_combo_1blanco($nom_combo,$tabla,$campo_mostrar,$campo_clave)
{
   global $estilo;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
   echo "<option value=0></option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_filtro_1texto($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro,$texto,$valor)
{
   global $estilo;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $estilo. ">";
   echo "<option value=$valor>$texto</option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_filtro_1blanco($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
   global $estilo;
   global $link;
   global $c_database;

   $qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $estilo . ">";
   echo "<option value=0></option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_a_medida_b($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$esp,$bus)
{
   global $stylefield;
   global $link;
   global $c_database;
   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   $espacio=str_pad($espacio,$esp, "_", STR_PAD_LEFT);
   if (mysql_affected_rows()==0) { echo " <INPUT TYPE=hidden name=$nom_combo   value='0'>
                                            <input  type='text' onfocus='this.blur()' name='pepe' size='50' value='No hay resultados en la busqueda:$bus'>";}
   if (mysql_affected_rows()==1) {$row = mysql_fetch_array($qry);
                                  echo " <INPUT TYPE=hidden name=$nom_combo   value='$row[$campo_clave]'>
                                         <input  type='text' onfocus='this.blur()' name='pepe' size='52' value='$row[$campo_mostrar]'>";}
   if (mysql_affected_rows()>1) {echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
                                  while ($row = mysql_fetch_array($qry))
                                          {$muestra=$row[$campo_mostrar];
                                           echo "<option value=$row[$campo_clave]>$muestra</option>";}
                                           echo "<option value='0'>$espacio</option>";
                                  echo "</select>";}
}
function poner_combo($nom_combo,$tabla,$campo_mostrar,$campo_clave,$_campo_id_html='')
{
   global $stylecombo;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   if($_campo_id_html<>'')
     $_campo_id_html1="id='$_campo_id_html'";

   echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_str($nom_combo,$tabla,$campo_mostrar,$campo_clave)
{
   global $stylefield;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
   while ($row = mysql_fetch_array($qry))
     echo "<option value='$row[$campo_clave]'>$row[$campo_mostrar]</option>";
   echo "</select>";
}


function poner_combo_filtro($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
   global $stylefield;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
   //echo $qrystr;
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
   while ($row = mysql_fetch_array($qry))
   echo "<option value=$row[$campo_clave]>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_filtro_str($nom_combo,$tabla,$campo_mostrar,$campo_clave,$filtro)
{
   global $stylefield;
   global $link;
   global $c_database;
   $qrystr = "select * from $tabla where $filtro order by $campo_mostrar";
   $qry = mysql_db_query($c_database,$qrystr ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
   while ($row = mysql_fetch_array($qry))
   echo "<option value='$row[$campo_clave]'>$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_a_medida_noeditable($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$editable)
{
   global $stylefield;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . " disabled='".$editable."'>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_a_medida_1texto_x($nom_combo,$campo_mostrar,$campo_clave,$qrystramedida,$texto,$valor,$extra)
{
   global $stylecombo;
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,$qrystramedida ,$link);
   echo "<select $extra size='1' name=" . $nom_combo . " " . $stylecombo . ">";
   echo "<option value=$valor>$texto</option>";
   while ($row = mysql_fetch_array($qry))
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}

function poner_combo_nro($nom_combo,$desde,$hasta)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom_combo . " " . $stylefield . ">";
   for ($i=$desde;$i<=$hasta;$i++)
     echo "<option value=$i>$i</option>";
   echo "</select>";
}

function poner_combo_array($nom_combo,$campo_mostrar,$campo_clave,$arr_param,$_campo_id_html='')
{
   global $stylecombo;

   if($_campo_id_html<>'')
     $_campo_id_html1="id='$_campo_id_html'";
   echo "<select $_campo_id_html1 size='1' name=" . $nom_combo . " " . $stylecombo . ">";
   foreach ($arr_param AS $row)
     echo "<option value=$row[$campo_clave] >$row[$campo_mostrar]</option>";
   echo "</select>";
}
// Funciones de fechas
 function poner_combo_hora($nom,$j)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=$j>$j</option>";
   $p='';
   for ($i=0;$i<=23;$i++)
       {
        $p='0'.$i;
        $p=substr($p,-2);
        echo "<option value=$p>$p</option>";
       }
   echo "</select>";
}
 function poner_combo_min_sec($nom,$j)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=$j>$j</option>";
   $p='';
   for ($i=0;$i<=59;$i++)
       {
        $p='0'.$i;
        $p=substr($p,-2);
        echo "<option value=$p>$p</option>";
       }
   echo "</select>";
}
function invertir_fecha($fecha)
{
    $anio = substr($fecha,0,4);
    $mes = substr($fecha,5,2);
    $dia = substr($fecha,8,2);
    return $dia . "/" . $mes . "/" . $anio;
}

function poner_combo_dia($nom)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   for ($i=1;$i<=31;$i++)
     echo "<option value=$i>$i</option>";
   echo "</select>";
}

function poner_combo_mes($nom)
{
   global $stylefield;
   global $ar_meses;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   for ($i=1;$i<=12;$i++)
     echo "<option value=$i>$ar_meses[$i]</option>";
   echo "</select>";
}

function poner_combo_anio($nom,$desde,$hasta)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   for ($i=$desde;$i<=$hasta;$i++)
     echo "<option value=$i>$i</option>";
   echo "</select>";
}
 function poner_combo_diah($nom,$j)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=$j>$j</option>";
   for ($i=1;$i<=31;$i++)
     echo "<option value=$i>$i</option>";
   echo "</select>";
}

function poner_combo_mesh($nom,$j)
{
   global $stylefield;
   global $ar_meses;
   $j=intval($j);
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=$j>$ar_meses[$j]</option>";
   for ($i=1;$i<=12;$i++)
     echo "<option value=$i>$ar_meses[$i]</option>";
   echo "</select>";
}

function poner_combo_anioh($nom,$desde,$hasta,$j)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=$j>$j</option>";
   for ($i=$desde;$i<=$hasta;$i++)
     echo "<option value=$i>$i</option>";
   echo "</select>";
}
function poner_combo_sino($nom)
{
   global $stylefield;
   echo "<select size='1' name=" . $nom . " " . $stylefield . ">";
   echo "<option value=1>Si</option>";
   echo "<option value=0>No</option>";
   echo "</select>";

}
  function llenar_campo($form,$campo,$valor)
  {
     echo "<script language='JavaScript'>";
     echo " $form.$campo.value = '$valor'; ";
     echo "</script>";
  }
  function llenar_area($form,$campo,$valor)
  {
     echo "<script language='JavaScript'>";
     echo " $form.$campo.text = '$valor'; ";
     echo "</script>";
  }
function poner_datos_lid($codigo)
{
   global $link;
   global $c_database;
   $qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_lider",$link);
   while ($row = mysql_fetch_array($qry))
   {
     return $row[paq];
   }
}
function poner_datos_dis($codigo)
{
   global $link;
   global $c_database;
   $qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_distrib",$link);
   while ($row = mysql_fetch_array($qry))
   {
     return $row[paq];
   }
}
function poner_datos_cod($nom_concat,$codigo)
{
   global $link;
   global $c_database;

   $qry = mysql_db_query($c_database,"select cod_us,concat(apellido,' ',nombres,' (',cod_us,')') as paq FROM usuario WHERE cod_us = '$codigo' ORDER by paq" ,$link);
   while ($row = mysql_fetch_array($qry))
   $nom_concat=$row[$paq];
}
function poner_datos_reg($codigo)
{
   global $link;
   global $c_database;
   $qry = mysql_db_query($c_database,"select concat(usu.apellido,' ',usu.nombres,' (',usu.cod_us,')') as paq FROM usuario as usu,usuario as usu1  WHERE usu1.cod_us='$codigo' and  usu.cod_us=usu1.id_regional",$link);
   while ($row = mysql_fetch_array($qry))
   return $row[paq];
}
function cliente_en_circuito($cliente,$nivel=6)
{
	global $c_database;
	global $link;
	switch ($nivel)
	{
		case 6:
			$campo1="comp_pedidos.id_vendedor";
		break;
		case 5:
			$campo1="comp_pedidos.id_lider";
		break;
		case 4:
			$campo1="comp_pedidos.id_distrib";
		break;
		case 3:
			$campo1="comp_pedidos.id_regional";
		break;
	}
	$qrystr = "SELECT id_pedido FROM campanias ORDER BY id_campania DESC LIMIT 2";
	$qry = mysql_db_query($c_database,$qrystr,$link);
	while ($row = mysql_fetch_array($qry))
	{
		$pedido=$row[id_pedido];
	}
	$qrystr1 = "SELECT comp_pedidos.id_distrib AS dis,comp_pedidos.id_lider AS lid,comp_pedidos.id_vendedor AS ven,id_pedido,estado_descrip
							FROM comp_pedidos
							INNER JOIN pedido ON id_pedidos=id_pedido
							INNER JOIN estado_pedido ON estado=cod_estado
							WHERE id_pedido >= $pedido
								AND estado IN(1,2,3,4,5,6,7,9,12,15,16,23)
								AND pedido.anexo NOT IN('cambio_cred','acc','grab_rein')
								AND $campo1='$cliente'
							GROUP BY id_pedido,comp_pedidos.id_vendedor";
	$qry1=mysql_db_query($c_database,$qrystr1,$link);
//	echo $qrystr1;
	verificar($qry,$qrystr);
	$arr_pedidos=array();
	while ($row1=mysql_fetch_array($qry1))
	{
	 $arr_pedidos[]	= $row1;
	}
	return $arr_pedidos;
}
function ayuda($par_texto,$snapx=0,$snapy=0,$bgcolor='#800000',$fgcolor='#C4D3DB',$textcolor='#800000')
{
// return " onmouseover=\"pmaTooltip('". $par_texto ."'); return false;\" onmouseout=\"swapTooltip('default'); return false;\" ";
  return " onmouseover=\"return overlib('$par_texto',SNAPX,$snapx,SNAPY,$snapy,BGCOLOR,'$bgcolor',FGCOLOR,'$fgcolor',TEXTCOLOR,'$textcolor');\" onmouseout=\"return nd();\" ";
}

function llenar_campo_id($id,$valor)
{
   echo "<script language='JavaScript'>";
   echo "document.getElementById('$id').value = '$valor'; ";
   echo "</script>";
}
?>
