/**
 * @author Javier Rios
 */
/**
*
*  
**/
 
upload = [];
	
//	$('#upload-progressbar').progressbar();

	$('#files').bind('change', function(e) {
		var files = e.target.files;
		var fileCount = 0;
		$.each(files, function(k, v) {
			fileCount++;
			reader = new FileReader();
			reader.onloadend = function(e) {
				upload.push({
					name : v.name,
					type : v.type || 'text/plain',
					bin : e.target.result // el contenido del archivo
				});
				if (upload.length == fileCount) uploadFiles();
			};
			reader.readAsBinaryString(v);
		});
	});
	
function uploadFiles() {
	var xhr = new XMLHttpRequest();

	$('#upload-progressbar').show();

	xhr.upload.addEventListener('progress', function(e) {
		if (e.lengthComputable) {
			var perc = Math.round((e.loaded * 100) / e.total);
			$('#upload-progressbar').progressbar('value', perc);
		}
	}, false);
	xhr.upload.addEventListener('load', function(e) {
		$('#upload-progressbar').progressbar('value', 100);
	}, false);

	xhr.open("POST", 'empl/compras_pagos_guardar.php');

	var BOUNDARY = '---------------------------1966284435497298061834782736';
	var rn = "\r\n";
	var req = '';
	$.each(upload, function(k, v) {
		req += "--" + BOUNDARY + rn + "Content-Disposition: form-data; name=\"files[]\"";
		req += '; filename="' + v.name + '"' + rn + 'Content-type: ' + v.type;
		req += rn + rn + v.bin + rn;
	});
	req += "--" + BOUNDARY + '--';

	xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
	xhr.sendAsBinary(req);
}