/**
 * @author Javier Rios
 */
/*
 *  Estas funciones fueron agregadas por mi 20120210 - Javier 
 */
// Drag & Drop files manager


// Uploads files Manager


// Send Files to Server
function sendFiles() {
  var archiv = document.querySelectorAll(".dropTarget");
  
  for (var i = 0; i < archiv.length; i++) {
    new FileUpload(archiv[i], archiv[i].file);
  }
}
// Manejar la subida
function FileUpload(vector, file) {
  var reader = new FileReader();  
  this.ctrl = createThrobber(vector);
  var xhr = new XMLHttpRequest();
  this.xhr = xhr;
  
  var self = this;
  this.xhr.upload.addEventListener("progress", function(e) {
        if (e.lengthComputable) {
          var percentage = Math.round((e.loaded * 100) / e.total);
          self.ctrl.update(percentage);
        }
      }, false);
  
  xhr.upload.addEventListener("load", function(e){
          self.ctrl.update(100);
          var canvas = self.ctrl.ctx.canvas;
          canvas.parentNode.removeChild(canvas);
      }, false);
  xhr.open("POST", "empl/compras_pagos_guardar.php");
  xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
  reader.onload = function(evt) {
    xhr.sendAsBinary(evt.target.result);
  };
  reader.readAsBinaryString(file);
}