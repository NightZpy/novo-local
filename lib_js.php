<script language="JavaScript">
function visibilidad(nombre,imgnom)
{

  // ocultamos o mostramos segun la variable valor
  if(document.getElementById(nombre).style.visibility!="visible")
    {
     document.getElementById(imgnom).src="skin/1/images_gif/carpeta_a.gif";
     //document.getElementById(imgnom).width=20;
     document.getElementById(nombre).style.visibility="visible";
     document.getElementById(nombre).style.display="";
    }
  else
    {
     document.getElementById(imgnom).src="skin/1/images_gif/carpeta_c.gif";
     //document.getElementById(imgnom).width=20;
     document.getElementById(nombre).style.visibility="hidden";
     document.getElementById(nombre).style.display="none";
    }
}
function comparacodigo(Campo,Nro)
{
  if (Campo.value==Nro)
  {
      alert("ERROR: El " + Campo + " es igual");
      Campo.focus();
      return true;
   }
  else
   alert("ERROR: El " + Campo + " diferente a " + Campo);
   Campo.focus();
    return false;
}

function AbrirVentana(imagen)
    {
      ventana=open("","MiPropiaVentana","width=300, height=200, toolbar=no,directories=no,menubar=no,status=no");
      ventana.document.write("<HTML>");
      ventana.document.write('<HEAD><TITLE>Foto</TITLE></HEAD>');
      ventana.document.write('<BODY bgcolor=#b8b4d8 link="#cc6600" vlink="#66cc00" alink="#66cc00">');
      ventana.document.write('<p align=center><img border="0" src=' + imagen + '></p>');
      ventana.document.write('<p align="center"><font face="arial" size="3" color="#cc6600"><b><a href="javascript:close()"><b>cerrar</b></a></font></p>')
      ventana.document.write("</BODY>");
      ventana.document.write("</HTML>");
    }

// Funciones varias con formularios
  function llenar_campo(campo,valor)
  {
    campo = valor
  }

// Funciones de Validacion de datos

function esNumerico(chr)
{
        if (chr >= '0' && chr <= '9') return true;
        else return false;
}

function Stringmenor(CampoNom,Campo,largo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length < largo))
  {
      alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function Stringmenorovacio(CampoNom,Campo,largo)
{
  if ((Campo.value.length > 0) && (Campo.value.length < largo))
  {
      alert("ERROR: El " + CampoNom + " no puede tener menos de " + largo + " letras");
      Campo.focus();
      return true;
   }
  else
    return false;
}
function StringVacio(CampoNom,Campo)
{
  if ((Campo==null) || (Campo.value==null) || (Campo.value.length==0))
  {
      alert("ERROR: El " + CampoNom + " no puede estar vacio");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function AnioValido(CampoNom,Campo,Desde,Hasta)
{
        if (Campo.value.length!=4)
        {
                alert("ERROR: El a�o no es correcto");
                Campo.focus();
                return false;
        }
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if (!esNumerico(chr))
                {
                        alert("ERROR: El a�o no es correcto");
                        Campo.focus();
                        return false;
                }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El a�o no es correcto");
              Campo.focus();
              return false;
          }


return true;
}

function PrecioValido(CampoNom,Campo,Desde,Hasta)
{
        var puntosa=0;

        if (Campo.value.length==0)
        {
                alert("ERROR: Debe ingresar " + CampoNom);
                Campo.focus();
                return true;
        }


        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if ( (chr==".") )
                   puntosa=puntosa+1;

                if (puntosa > 1)
                  {
                   alert("ERROR:" + CampoNom + " No es un monto valido " );
                   Campo.focus();
                   return true;
                  }

                if (!((esNumerico(chr)) || (chr == ".") || (chr=="-")))
                  {
                   alert("ERROR: Debe ingresar un n�mero");
                   Campo.focus();
                   return true;
                  }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El n�mero debe estar entre " + Desde + " y " + Hasta);
              Campo.focus();
              return true;
          }


        return false;

}


function NumeroValido(CampoNom,Campo,Desde,Hasta)
{
        if (Campo.value.length==0)
        {
                alert("ERROR: Debe ingresar " + CampoNom);
                Campo.focus();
                return true;
        }
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if (!esNumerico(chr))
                {
                        alert("ERROR: Debe ingresar un n�mero");
                        Campo.focus();
                        return true;
                }
        }
        if (Campo.value < Desde || Campo.value > Hasta)
          {
              alert("ERROR: El n�mero debe estar entre " + Desde + " y " + Hasta);
              Campo.focus();
              return true;
          }


return false;
}

function MailValido(CampoNom,Campo)
{
var chr;
var arrobas=0;
var puntos=0;
var blancos=0;
if  ((Campo!=null) && (Campo.value.length!=0))
{
        for (var i=0;i<Campo.value.length;i++)
        {
                chr=Campo.value.substring(i,i+1);
                if ( (chr=="@") )
                        arrobas=arrobas+1;
                if ( (chr==".") )
                        puntos=puntos+1;
                if ( (chr==" ") )
                        blancos=blancos+1;
        }

        if ( (blancos!=0) || (arrobas!=1) || (puntos<1) )
        {
                alert("ERROR: La direcci\363n de correo no es correcta.Recuerda que no se adminten espacios en blanco y que debe contener el car\341cter arroba (@) y el car\341cter punto (.)");
                Campo.focus();
                return false;
        }
}
return true;
}


//calcular la edad de una persona
//recibe la fecha como un string en formato espa�ol
//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual
function calcular_edad(fecha){

    //calculo la fecha de hoy
    hoy=new Date()
    //alert(hoy)

    //calculo la fecha que recibo
    //La descompongo en un array
    var array_fecha = fecha.split("-");
    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length!=3)
       return false;

    //compruebo que los ano, mes, dia son correctos
    var ano
    ano = parseInt(array_fecha[0]);
    if (isNaN(ano))
       return false

    var mes
    mes = parseInt(array_fecha[1]);
    if (isNaN(mes))
       return false

    var dia
    dia = parseInt(array_fecha[2]);
    if (isNaN(dia))
       return false

    //resto los a�os de las dos fechas
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o

    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
       return edad
    if (hoy.getMonth() + 1 - mes > 0)
       return edad+1

    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
       return edad + 1

    return edad
}



function EdadMenor(CampoNom,Campo,Anios)
{

  if (calcular_edad(Campo.value)<18)
  {
      alert("ERROR: " + CampoNom + " es menor de " + Anios + ". ");
      Campo.focus();
      return true;
   }
  else
    return false;
}

function CaracterValido(control)
{
var chr;
var novalido=0;
if ((control.value!=null) && (control.value.length!=0))
  {
    for (var i=0;i<control.value.length;i++)
        {
         chr=control.value.substring(i,i+1);
         if ( (chr==';') || (chr==',') || (chr=='%') || (chr=='\"') || (chr=='\`') || (chr=='\�') || (chr=='\'') || (chr=='\\') )
            novalido=novalido+1;
        }
        if ( (novalido!=0) )
        {
          alert('ERROR: No se adminten los siguientes caracteres(  ;  ,  %  \`  \�  \\  \'  \"  ), Por favor reemplace por caracteres validos o quitelos ');
          control.focus();
          return false;
        }
  }
return true;
}
</script>
