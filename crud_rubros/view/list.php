<!DOCTYPE HTML>
<html lang="es">
	<?php include_once 'head.php'; ?>
	<body>
		<form action="<?php echo $helper->url("rubro","create"); ?>" method="post" class="col-lg-5" id="new_rubro">
			<h3>Gestor de Precios por Rubros</h3>
			<a href="<?php echo $helper->url("rubro","index"); ?>" class="btn btn-info">Nuevo</a>
			<hr/>
			<div class="col-md-12" id="message">
			</div>
			<input type="hidden" id="id" name="id" value="<?php echo (isset($rubro->id)?$rubro->id: '') ?>">
			<div class="form-group">
				<label for="rubro_alfa">Rubro Alfa:</label> <input type="text" name="rubro_alfa" id="rubro_alfa" class="form-control" required="required" value="<?php echo (isset($rubro->rubro_alfa)?$rubro->rubro_alfa: '') ?>"/>
			</div>
			<div class="form-group">
				<label for="descrip_rubro">Descripción:</label> <input type="text" name="descrip_rubro" id="descrip_rubro" class="form-control" required="required" value="<?php echo (isset($rubro->descrip_rubro)?$rubro->descrip_rubro: '') ?>"/>
			</div>
			<div class="form-group">
				<label for="precio_gral">Precio:</label> <input type="text" name="precio_gral" id="precio_gral" class="form-control" required="required" value="<?php echo (isset($rubro->precio_gral)?$rubro->precio_gral: '') ?>"/>
			</div>
			<input type="submit" value="Guardar" class="btn btn-success"/>
		</form>
		<div class="col-lg-7">
			<h3>Rubros</h3>
			<hr/>
		</div>
		<div class="col-lg-7">
			<table class="table table-striped">
					<tr>
						<th class="col-lg-1">
							ID
						</th>
						<th class="col-lg-3">
							Codigo Rubro
						</th>
						<th class="col-lg-3">
							Descripción Rubro
						</th>
						<th class="col-lg-3">
							Precio General
						</th>
						<th class="col-lg-2">
							Acciones
						</th>
					</tr>
					<tbody  id="rubros">
				<?php if(count($rubros)>0){ ?>
					<?php foreach($rubros as $rubro) { //recorremos el array de objetos y obtenemos el valor de las propiedades ?>
						<tr>
							<td class="col-lg-1">
								<?php echo $rubro->id; ?>
							</td>
							<td class="col-lg-3">
								<?php echo $rubro->rubro_alfa; ?>
							</td>
							<td class="col-lg-3">
								<?php echo $rubro->descrip_rubro; ?>
							</td>
							<td class="col-lg-3">
								<?php echo $rubro->precio_gral; ?>
							</td>
							<td class="col-lg-2">
								<div class="right">
									<a href="<?php echo $helper->url("rubro","edit"); ?>&id=<?php echo $rubro->id; ?>" class="btn btn-warning">Edit</a>
									<a href="<?php echo $helper->url("rubro","destroy"); ?>&id=<?php echo $rubro->id; ?>" class="btn btn-danger">Borrar</a>
								</div>
							</td>
						</tr>
					<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="5" class="text-center">No hay precios de rubros registrados.</td>
					</tr>
				<?php } ?>
					</tbody>
			</table>
		</div>
		<?php include_once 'footer.php'; ?>
	</body>

</html>
