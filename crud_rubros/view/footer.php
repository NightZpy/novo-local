<footer class="col-lg-12">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="assets/js/additional-methods.min.js"></script>
	<script type="text/javascript" src="assets/js/localization/messages_es.min.js"></script>
	<script type="text/javascript" src="assets/js/function.js"></script>
	<hr/>
	<div class="text-center">
		Desarrollado por <a target="_black" href="http://www.presentatenlaweb.com/">Presentatenlaweb.com</a> - Copyright &copy; <?php echo  date("Y"); ?>
	</div>
</footer>
