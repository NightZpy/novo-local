<?php


class Rubro extends Illuminate\Database\Eloquent\Model
{
	protected $fillable = ['rubro_alfa','descrip_rubro','precio_gral'];
	public $timestamps = false;
}


