$(document).ready(function(){
	jQuery.validator.addMethod("sinnumero", function(value, element) {
		return this.optional(element) || /^[A-Z\s]+$/.test(value) || this.optional(element) || /[S/N]$/.test(value);
	});

	$("#new_rubro").validate({
		rules: {
			rubro_alfa: {
				required: true,
				maxlength: 3,
				sinnumero: true
			}
		},
		messages: {
			rubro_alfa:{
				sinnumero: "Solo mayusculas",
			}
		},
		submitHandler: function(form) {
			myurl = 'index.php?controller=rubro&action=create';
			console.dir(myurl);
			data = $("#new_rubro").serialize();
			$.ajax({
				url: myurl,
				type: "POST",
				dataType: 'json',
				data: data,
				success: function(data) {
					$('#message').show();
					if(data.success == false)
					{
						if( $.isArray(data.msg)){
							content = '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">';
							$.each(data.msg, function(key, value){
								content = content + '<strong>'+ value +'</strong><br>';
							});
							$('#message').html(content+'</div>');
							$('#message').delay(2000).slideToggle(400);
						}else{
							$('#message').html('<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert"><strong>'+ data.msg +'</strong></div>');
							$('#message').delay(2000).slideToggle(400);
						}
					} else {
						$('#message').html('<div class="alert alert-success col-md-6 col-md-offset-3" role="alert"><strong>'+ data.msg +'</strong></div>');
						$('#message').delay(2000).slideToggle(400);
						$('#new_rubro').trigger("reset");
						reload();
					}
				},
				error: function(xhr, textStatus, thrownError) {
					console.dir('Algo ha ido mal. Por favor, inténtelo de nuevo más tarde');
				}
			});
			event.preventDefault();
		}
	});

	function reload(e){
		$.ajax({
			url: 'index.php?controller=rubro&action=reload',
			type: "POST",
			dataType: 'json',
			success: function(data) {
				content = '';
				$.each(data, function(key, value){
					content = content + '<tr><td>'+ value.id +'</td><td>'+ value.rubro_alfa +'</td><td>'+ value.descrip_rubro +'</td><td>'+ value.precio_gral +'</td>'
											+ '<td><div class="right">'
											+ '<a href="index.php?controller=rubro&action=edit&id='+ value.id +'" class="btn btn-warning">Edit</a> '
											+ '<a href="index.php?controller=rubro&action=destroy&id='+ value.id +'" class="btn btn-danger">Borrar</a>'
											+ '</div></td> </tr>';
				});
				$('#rubros').html('');
				$('#rubros').html(content);
			},
			error: function(xhr, textStatus, thrownError) {
				console.dir('Algo ha ido mal. Por favor, inténtelo de nuevo más tarde');
			}
		});
		event.preventDefault();
	}

	$('#precio_gral').keyup(function () {
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
});
