<?php

class RubroController extends ControladorBase{

	protected $rules =  ['rubro_alfa'		=> 'required|max_len,3|alpha',
								'descrip_rubro'	=> 'required',
								'precio_gral'		=> 'required|numeric'];

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$rubros = Rubro::all();
		return $this->view("list",["rubros" => $rubros]);
	}

	public function create()
	{
		if( $_POST["id"] === ''){
			$find = Rubro::where( 'rubro_alfa', '=' ,$_POST["rubro_alfa"])->first();
		}else{
			$find = Rubro::where('id', '<>', $_POST["id"])->where( 'rubro_alfa', '=' ,$_POST["rubro_alfa"])->first();
		}
		if(is_object($find)){
			$response = (['success' => false, 'msg' => 'El codigo de rubro ya esta en uso.']);
		}else{
			if( $_POST["id"] === ''){
				$validated = GUMP::is_valid($_POST, $this->rules);
				if($validated === true)
				{
					$rubro = new Rubro();
					$rubro->rubro_alfa		= $_POST["rubro_alfa"];
					$rubro->descrip_rubro	= $_POST["descrip_rubro"];
					$rubro->precio_gral		= $_POST["precio_gral"];
					if($rubro->save()){
						$response = ['success' => true, 'msg' => 'El nuevo rubro fue guardado con éxito.'];
					}else{
						$response = ['success' => false, 'msg' => 'No se pudo guardar el nuevo rubro.'];
					}
				}else{
					$response = ['success' => true, 'msg' => $validated];
				}
			}else{
				$id=(int)$_POST["id"];
				$rubro = Rubro::find($id);
				$rubro->rubro_alfa		= $_POST["rubro_alfa"];
				$rubro->descrip_rubro	= $_POST["descrip_rubro"];
				$rubro->precio_gral		= $_POST["precio_gral"];
				if($rubro->save()){
					$response = ['success' => true, 'msg' => 'El rubro se modficado con éxito.'];
				}else{
					$response = ['success' => false, 'msg' => 'No se pudo modificar el rubro.'];
				}
			}
		}
		echo json_encode($response);
	}

	public function edit()
	{
		if(isset($_GET["id"])){
			$id=(int)$_GET["id"];
			$rubro = Rubro::find($id);
		}
		$rubros = Rubro::all();
		$this->view("list",["rubros" => $rubros, 'rubro' => $rubro]);
	}


	public function destroy()
	{
		if(isset($_GET["id"])){
			$id=(int)$_GET["id"];
			$rubro = Rubro::find($id);
			$rubro->delete($id);
		}
		$this->redirect();
	}

	public function reload()
	{
		$rubros = Rubro::all()->toArray();
		echo json_encode($rubros);
	}

}

