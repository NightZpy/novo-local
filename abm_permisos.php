<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title></title>
		<script type='application/javascript'>
		 $("#usuario").autocomplete({
				source: function(request,response){
					$.ajax({
						type: "POST",
						url: "empl/search.php",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response(data);
						}
					});
				 }  
			  });
		</script>
	</head>
<body>	
<div id="contenedor">

	<div class="form">
			<div class="demo">
				 <p><label class="forms" for="usuario">Usuario:</label>
				        <input class="ui-widget" type="usuario" placeholder="Ingrese usuario" id="usuario" name="usuario" size="40"/></p>
			</div><!-- End demo -->
		<?php include("menu_pedidos_data_n1.php");?>
	</div>
<!--end form-->
</div>
<!--end contenedor-->
</body>
</html>
