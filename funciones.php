<?php
$T_GIF = 1;
$T_JPG = 2;
$T_PNG = 3;
$porc_err_img = 1;
function tam_imagen_ok($imagen,$ancho,$alto,$tipo)
{
  $ancho_error = (int)($ancho * $porc_error_img / 100);
  $alto_error = (int)($alto * $porc_error_img / 100);
  $imagenok = file_exists($imagen);
  if ($imagenok)
    {
      $ar_img = GetImageSize($imagen);
      $imagenok = ($imagenok) and
                  (($ancho - $ancho_error) >= $ar_img[0]) and ($ar_img[0] <= ($ancho + 2));
      if ($imagenok)
           $imagenok = ($imagenok) and (($alto - $alto_error) >= $ar_img[1]) and ($ar_img[1] <= ($alto + 2));
      if ($imagenok)
          $imagenok = ($imagenok) and ($tipo == $ar_img[2]);
    }
   return $imagenok;
}
?>
