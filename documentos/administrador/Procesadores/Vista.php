<style type="text/css">
		
		h2{
			color:#333;
		}

		div#titulo{
			font-family: arial, verdana, sans-serif;
			font-weight:bold;
			width: 100%;
			color: #333;
		}

		div#pregunta{
			margin-left:10px;
			padding: 5px;
		}

		div#opcion{
			margin-top: 0px;
			margin-left:12px;
		}
		div#opcion input{
			widht:400px;
			height:150px;
		}

		div#botones{
			text-align:center;
			width:300px;
			margin:auto;
		}

		.error{
			color:red;
			font-size:0.8em;
		}

		.pregunta{
			text-align:left;
			margin: 15px 0 15px 0;
			padding: 8px;
			border:1px dashed #333333;
			position:relative;
		}
		.pregunta span{
			line-height:2em;
		}

		div.borrar_pregunta{
			position:absolute;
			right:0;
			top:0;
			margin:8px;
		}

		.opcion{
			margin: 2px 2px 2px 20px;
		}

		a.borrar_opcion, a.borrar_pregunta{
			color:red;
			text-decoration:none;
			font-weight:bold;
		}

		a.agregar_opcion, a#agregar_pregunta{
			color:green;
			text-decoration:none;

		}


		#dyn_elements{
			display:none;
		}
 </style>

        <?php
            include ("encuestas/clases/entidad/Encuesta.php");
            include ("encuestas/clases/DAO/EncuestaDAO.php");
            include ("encuestas/clases/entidad/Opcion.php");
            include ("encuestas/clases/entidad/Pregunta.php");

            $id = $_GET["idencuesta"];
            $encDao = new EncuestaDAO();
            $pregDao = new PreguntaDAO();
            $opcDao = new OpcionDAO();

            if(($encuesta = $encDao->traerPorHash($id))==null){
                    die("<h1>Encuesta Invalida</h1>");
            }
			//print_r($encuesta);
            if(isset($_COOKIE["voto"])){
                $cookie = $_COOKIE["voto"];
                $conCookie=" AND sesion = '$cookie'";
			}
			$id_encuesta_cookie = $encuesta->getId();
			
			$id_usuario=$_SESSION[id_usuario];
			if($_SESSION[logueado_user]!='ANAHI'){
					if($_SESSION[logueado_user]!='GUADALUPE'){
				
			$query = "SELECT * FROM respuestas_encuestaslu WHERE id_encuesta='$id_encuesta_cookie' AND id_usuario='$id_usuario' LIMIT 1";
			//echo $query."<br>";
			$sal = mysql_query($query) or die("Error de SQL: $query");

                if(mysql_num_rows($sal) != 0){
					
						?>
                        <form class="form_adv" action="administracion.php?pagina=encuestas/Procesadores/Procesar.php" method="POST">                  
                            <h1> Usted ya votó en esta encuesta. </h1>
                        </form>

						<?php
						die();
				}
			}
		}

   

		if($encuesta->getObligatorio()){
			echo "<div class='enc_obligatoria'>";
		}	
        ?>
			
         
		 <form style="position:relative;padding:25px;" class="form_n" action="administracion.php?pagina=encuestas/Procesadores/Procesar.php" method="POST">
				<div style="width:800px; overflow:hidden;">
					<img src="encuestas/Procesadores/imagen/<?php echo $encuesta->getImagen(); ?>" >
				</div>
                <h2>
                        <?php echo $encuesta->getTitulo(); ?>
                </h2>
                <p>
                        <?php echo $encuesta->getDescripcion(); ?>
                </p>
                <?php

                    foreach ($encuesta->preguntas as $pregunta){

                ?>

                <br><br>
                <div id="pregunta">

                    <div id="titulo">
                        <?php

                            echo $pregunta->getTitulo();
                        ?>

						
                    </div>
                    <br>


                    <?php

                        $exclusiva = $pregunta->getExclusiva();

                        if($exclusiva){
                            $tipo = "radio";
                            $array = "";
                        }else{
                            $tipo = "checkbox";
                            $array = "[]";
                        }

                        foreach($pregunta->opciones as $opcion){


                    ?>

                    <div id="opcion">

                        <input style="height:10px" type="<?php echo $tipo ?>" name="<?php echo $pregunta->getOrden().$array ?>" value="<?php echo $opcion->getId() ?>" <? if ($tipo=='radio') { ?>required="required"<? }?>><?php echo $opcion->getTitulo() ?>
                    </div>

                    <br>

                    <?php


                        }
                        if($pregunta->getAbierta()){
                    ?>


                    <div id="opcion">
                        <p>Otro:</p> 
                        <textarea name="<?php echo $pregunta->getOrden()."_comentario" ?>" style=" min-width: 75%; height:180px;"></textarea>
                    </div>

               

                <?php
                        } 	?>
					</div>
                 <?php   }

                ?>

                <br><br>

                <div align="center" id="botones">

                    <br><br>
                   
                    <input type="hidden" name="encuesta" value="<?php echo $encuesta->getId(); ?>" />

                    <p class="submit" style="height: 50px;">
                        <input type="submit" value="Enviar" class="botonvd marginright"/>
                    </p>

                </div>
        </form>
       <?
			if($encuesta->getObligatorio()){
				echo "</div>";
			}	
