<?php
    include_once("encuestas/clases/DAO/EncuestaDAO.php");
    include_once("encuestas/clases/entidad/Encuesta.php");
    include_once("encuestas/clases/entidad/Opcion.php");
    include_once("encuestas/clases/entidad/Pregunta.php");
      
    $action = $_REQUEST['action'];
    $id_encuesta = $_REQUEST['idencuesta'];
    $dao_enc = new EncuestaDAO();
	if ($_POST[asignar]=='on')
			$_POST[asignar]=1;
		else
			$_POST[asignar]=0;
    switch($action){
        case "edit":
            if(!is_numeric($id_encuesta))
                header("Location: index.php");
            $encuesta = $dao_enc->traerPorId($id_encuesta);
            if($encuesta->getTipo()=='privada')
				$ussu = $dao_enc->traerListaControladores($id_encuesta);
            $next_action = "update";
            $tituloenc=$encuesta->getTitulo();
            $id_encuesta=$encuesta->getId();    
            $asignar=$encuesta->getAsignar();   
            $imagen=$encuesta->getImagen();   
            $descripcion=$encuesta->getDescripcion(); 
            $habilitar=$encuesta->getHabilitar();
            $obligatorio=$encuesta->getObligatorio();
            
			$Module_hover  = Array("img"=>$imagen);
            $publicacion=$encuesta->getFecha_de_publicacion();
            $fecha=explode('-',$publicacion);
			$anio=$fecha[0];
			$mes=$fecha[1];
			$dia=$fecha[2];
			
            $caducidad=$encuesta->getFecha_de_caducidad();
            $fecha2=explode('-',$caducidad);
			$anio2=$fecha2[0];
			$mes2=$fecha2[1];
			$dia2=$fecha2[2];
            break;
        case "update":
			$_POST[fecha_de_publicacion]=$_POST[anio]."-".$_POST[mes]."-".$_POST[dia];
			$_POST[fecha_de_caducidad]=$_POST[anio2]."-".$_POST[mes2]."-".$_POST[dia2];
			$encuesta = Encuesta::crearEncuesta($_POST);
			$dao_enc->modificar($encuesta);
			?>
            <script type="text/javascript">
				window.location.href='administracion.php?pagina=encuestas/admin/lista_encuestas.php&msje=1';
			</script>
			<?
             break;
		case "add":
			//print_r($_POST);
			$_POST[fecha_de_publicacion]=$_POST[anio]."-".$_POST[mes]."-".$_POST[dia];
			$_POST[fecha_de_caducidad]=$_POST[anio2]."-".$_POST[mes2]."-".$_POST[dia2];
			
			$encuesta = Encuesta::crearEncuesta($_POST);
			$dao_enc->persistir($encuesta);
			?><script type="text/javascript">
				window.location.href='administracion.php?pagina=encuestas/admin/lista_encuestas.php&msje=2';
			</script><?		
		break;
		case "delete":	
			if(($encuesta = $dao_enc->traerPorHash($id_encuesta))!=null){
				$id_encuesta_elim = $encuesta->getId();
				$dao_enc->eliminar($id_encuesta_elim);
				?><script type="text/javascript">
				window.location.href='administracion.php?pagina=encuestas/admin/lista_encuestas.php&msje=3';
			</script><?	
			}
		break;
	
        default: //new
            $next_action = "add";
    }
    if($next_action == "update")
        $titulo = "Modificar Encuesta";
    else
        $titulo = "Nueva Encuesta";

$tipo = array(
    array("value" => "publica", "texto" => "P&uacute;blica"),
    array("value" => "privada", "texto" => "Privada")
);

$estructura = array(
    array("value" => "", "texto" => "Seleccione estructura"),
    array("value" => "REGIONAL", "texto" => "Regional"),
    array("value" => "DISTRIBUIDOR", "texto" => "Distribuidor"),
    array("value" => "LIDER", "texto" => "Lider"),
    array("value" => "REVENDEDOR", "texto" => "Revendedor")
);


if($next_action == "update"){

	foreach($tipo as &$t){
        if($t['value'] == $encuesta->getTipo())
            $t['selected'] = " selected=\"selected\"";
    }
    unset($t);
    
    foreach($estructura as &$es){
        if($es['value'] == $encuesta->getEstructura())
            $es['selected'] = " selected=\"selected\"";
    }
    
	unset($es);
	
}
?>
<script>
function select_estructura(){
	$.ajax({
		beforeSend: function(objeto) {
		$("#usuarios_no_asignados").html("<option>Cargando...</option>");},
		type: "POST",
		error: function (XMLHttpRequest, textStatus, errorThrown) {
		alert(this+" "+textStatus+" "+errorThrown)},
		url: "busca_ussu.php?estr="+$("#estructura").val(),
		success: function(dat){
		$("#usuarios_no_asignados").html(dat);
		}});
	}		

</script>
<html>
<head>
	
        <link href="encuestas/admin/css/general.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="encuestas/admin/js/jquery-1.3.2.min.js"></script>
		
		<script type="text/javascript" src="encuestas/admin/js/form_encuestas.js"></script>
		<script src="encuestas/admin/js/jquery.color.js"></script>
		<style type="text/css">
			div{
				text-align:left;
			}

			.red{
				color:red;
			}

			.green{
				color:green;
			}
			div#main{
				width:580px;
				margin:auto;
			}

			div.row{
				margin: 7px auto;
			}
			.error{
				color:red;
				font-size:0.8em;
			}

			.pregunta{
				text-align:left;
				margin: 15px 0 15px 0;
				padding: 8px;
				border:1px dashed #333333;
				position:relative;
				width: 670px;
			}
			.pregunta span{
				line-height:2em;
			}
			
			div.borrar_pregunta{
				position:absolute;
				right:0;
				top:0;
				margin:8px;
			}

			div#preguntas_box{
				width:80%;
			}

			.opcion{
				margin: 2px 2px 2px 20px;
			}

			a.borrar_opcion, a.borrar_pregunta{
				color:red;
				text-decoration:none;
				font-weight:bold;
			}

			a.agregar_opcion, a#agregar_pregunta{
				color:green;
				text-decoration:none;
			}

			a#desasignar_usuario, a#asignar_usuario{
				text-decoration:none;
			}

			#usuarios_no_asignados, #usuarios_asignados{
				width:200px;
			}

			#dyn_elements{
				display:none;
			}

			#asignacion_usuarios_wrapper{
				margin:50 0px;
				display:none;
			}
		</style>
<script>
	$(function() {
		$( "#datepicker" ).datepicker();
	});
</script>
</head>
<body>
<?php
    include("inc_menu.php");
    // Add Encuesta
?>
       <div id="main">
                <form method="POST" action="administracion.php?pagina=encuestas/admin/form_encuestas.php&action=<?php echo $next_action; ?>" class="form" name="form" enctype="multipart/form-data" >
					<input type="hidden" name="MAX_FILE_SIZE" value="900000" />
					<div class="row">
						<span> T&iacute;tulo: </span>
						<input type="text" name="titulo" size="80" required="required" value="<? echo $tituloenc;?>" >
					</div>		
					 <div class="row">
						<span> Descripci&oacute;n: </span>
						<textarea name="descripcion" style="width: 666px; height: 139px;"><? echo $descripcion;?></textarea>
					</div>	
					<div class="row">
						<span> Seleccionar imagen para encabezado (jpg|png|gif  - 900 px de ancho): </span>
						<input name="imagen" class="multi element text medium" type="file" size="16" accept="jpg|png|gif" value="<?=$imagen?>" maxlength="1" />	
						<?php
						   if($next_action == "update"){ 
								if($imagen){ ?>
								<ul class="hoverbox">
									<li>
										<a href="#"><img src="encuestas/Procesadores/imagen/<?=$imagen?>" alt="description">
										<img src="encuestas/Procesadores/imagen/<?=$imagen?>" alt="description" class="preview"></a>
									</li>
								</ul>
								
								<? }
						   }
						?>
					</div>	
					<div class='clear'></div>
					<div class="row">
						<span> Fecha de Publicaci&oacute;n: </span><br />
						<span>
							<input id="element_1_2" name="dia" class="element text" size="2" maxlength="2" value="<? if($dia) echo $dia;?>" type="number" required="required"> /
						</span>
						<span>
							<input id="element_1_1" name="mes" class="element text" size="2" maxlength="2" value="<? if($mes) echo $mes;?>" type="number" required="required"> /
						</span>
						<span>
							<input id="element_1_3" name="anio" class="element text" size="4" maxlength="4" value="<? if($anio) echo $anio;?>" type="number" required="required">
						</span>
					
						<span id="calendar_1">
							<img id="cal_img_1" class="datepicker" src="css/calendar.gif" alt="Pick a date.">	
						</span>
						<script type="text/javascript">
							Calendar.setup({
							inputField	 : "element_1_3",
							baseField    : "element_1",
							displayArea  : "calendar_1",
							button		 : "cal_img_1",
							ifFormat	 : "%B %e, %Y",
							onSelect	 : selectDate
							});
						</script>
					</div>	
				<div class="row">
					<span> Fecha de Caducidad: </span><br />
					<span>
						<input id="element_1a_2" name="dia2" class="element text" size="2" maxlength="2" value="<? if($dia2) echo $dia2;?>" type="number" required="required"> /
					</span>
					<span>
						<input id="element_1a_1" name="mes2" class="element text" size="2" maxlength="2" value="<? if($mes2) echo $mes2;?>" type="number" required="required"> /
					</span>
					<span>
						<input id="element_1a_3a" name="anio2" class="element text" size="4" maxlength="4" value="<? if($anio2) echo $anio2;?>" type="number" required="required">
					</span>
				
					<span id="calendar_2">
						<img id="cal_img_2" class="datepicker" src="css/calendar.gif" alt="Pick a date.">	
					</span>
					<script type="text/javascript">
						Calendar.setup({
						inputField	 : "element_1a_3a",
						baseField    : "element_1a",
						displayArea  : "calendar_2",
						button		 : "cal_img_2",
						ifFormat	 : "%B %e, %Y",
						onSelect	 : selectDate
						});
					</script>
				</div>	
				<div id="preguntas_box">
                <?php	// Edit Encuesta
                    if($next_action == "update"):
                    ?>
						<input type="hidden" name="usuario_modifico" value="<?php echo $_SESSION['id_usuario']; ?>">
						<input type="hidden" name="id_encuesta" size="80" value="<? echo $id_encuesta;?>" >
                    <?
                    $i=1;
						foreach ($encuesta->preguntas as $preg) { ?>

							<div class='pregunta'>
								<div class='borrar_pregunta'>
									<a href='#borrar_pregunta' class='borrar_pregunta'> X </a>
								</div>
								<span> Pregunta: </span>
								<input type='text' class='pregunta_titulo' size='70' name='preguntas[<?=$i?>][titulo]' value='<?=$preg->titulo?>'><br>
								<span> Exclusiva: </span>
								<input type="radio" class="pregunta_exclusiva" name="preguntas[<?=$i?>][exclusiva]" value="1" <? if ($preg->exclusiva=='1') echo 'checked="checked"'; ?> > Si
								<input type="radio" class="pregunta_exclusiva" name="preguntas[<?=$i?>][exclusiva]" value="0" <? if ($preg->exclusiva=='0') echo 'checked="checked"'; ?>  > No
								<br/>
								<span> Opciones: </span><a href='#agregar_opcion' class='agregar_opcion'>  (Agregar) </a><br/>
								<div class='opciones_box'>
									<? foreach ($preg->opciones as $opc){ ?>
											<div class='opcion'>
												<input type='text' size="50" value='<?=$opc->titulo?>'>
											</div>
									<? } ?>
         
								</div>
								 <input type="checkbox" class="comentario" value="1"  <? if ($preg->abierta=='1') echo 'checked="checked"'; ?>  >
								 <span> Permitir ingresar comentario </span>
							</div>
						<?
						$i++;
					}
                    else:
                    
                ?>
					<input type="hidden" name="id_usuario"  value="<?php echo $_SESSION['id_usuario']; ?>">
                    <div class="pregunta">
                        <span> Pregunta: </span>
                        <input type="text" class="pregunta_titulo" size="70" name="">
                        <br/>
                        <span> Exclusiva: </span>
                        <input type="radio" class="pregunta_exclusiva" name="exclusiva" value="1" checked="checked"> Si
                        <input type="radio" class="pregunta_exclusiva" name="exclusiva" value="0"> No
                        <br/>
                        <span> Opciones: </span>
                        <a href="#agregar_opcion" class="agregar_opcion"> (Agregar) </a>
                        <br/>
                        <div class="opciones_box">
                            <div class="opcion">
                                <input type="text" size="50">
                            </div>
                            <div class="opcion">
                                <input type="text" size="50">
                            </div>
                            <div class="opcion">
                                <input type="text" size="50">
                                <a href="#borrar_opcion" class="borrar_opcion"> X </a>
                            </div>
                        </div>
                        <input type="checkbox" class="comentario" value="1">
                        <span> Permitir ingresar comentario </span>

                    </div>   
                <?php
                    endif;
                ?>
                      </div>    
                
                <div style="text-align:right">
                    <a href="#agregar_pregunta" id="agregar_pregunta"> Agregar Pregunta </a>
                </div>
                <div class="row">
                    <span> Tipo: </span>
                    <select name="tipo" id="select_tipo"  style="width: 180px; margin-left: 45px;">
                    <?php
                        foreach($tipo as $t):
                    ?>
                        <option value="<?php echo $t['value'];?>" <?php echo $t['selected']; ?>>
                             <?php echo $t['texto'];?>
                        </option>
                    <?php
                        endforeach;
                    ?>
                    </select>
                </div>
   
             
                <div class="row">
                    <span> Dirigida a: </span>
                    <select name="estructura" id="estructura" style="width: 180px; margin-left: 15px;" onChange="select_estructura()" >
                    <?php
							foreach($estructura as $es):
						?>
							<option value="<?php echo $es['value'];?>" <?php echo $es['selected']; ?>>
                            <?php echo $es['texto'];?>
                        </option>
						<?php
							endforeach;
						?>
                    </select>
                </div>
              <div class="row">
                    <span> Obligatoria: </span>
                    <input type="radio" class="pregunta_exclusiva" name="obligatorio" value="1" <? if ($obligatorio==1){?> checked="checked" <? } ?> > Si
                    <input type="radio" class="pregunta_exclusiva" name="obligatorio" value="0" <? if ($obligatorio==0){?> checked="checked" <? } ?>> No
              </div>
               <div class="row">
				    <span> Habilitar  : </span>
                    <input type="radio" class="pregunta_exclusiva" name="habilitar" value="1" <? if ($habilitar==1){?> checked="checked" <? }?>> Si	 		
                    <input type="radio" class="pregunta_exclusiva" name="habilitar" value="0" <? if ($habilitar==0){?> checked="checked" <? }?>> No
              </div>
             <div id="asignacion_usuarios_wrapper" class="row">
					<p> <input type="checkbox" size="50" name="asignar" <? if($asignar) echo "checked"; ?> >Asignar a toda la estructura</p><br />
                    <table style="width: 682px;">
                        <tr>
                            <td> Lista Usuarios</td>
                            <td> &nbsp; </td>
                            <td> Usuarios Asignados <b style="color:red; font-size:11px;">(mantenga seleccionados todos los usuarios asignados)</b></td>
                        </tr>
                        <tr>
                            <td>
                                <select multiple size="15" style="width:290px;" id="usuarios_no_asignados" onchange="if (this.selectedIndex) doSomething();"> 
                                </select>
                            </td>
                            <td>
								<p><a class="green" href="#asignar" id="asignar_usuario">
                                    Asignar >>
                                </a></p>
                                <br/> <br/>
                                <p style="width: 80px;"><a class="red" href="#desasignar" id="desasignar_usuario">
                                    << Desasignar
                                </a></p>
                            </td>
                            <td>
                                <select multiple size="15" style="width:290px;" id="usuarios_asignados" name="controladores[]">
									<? echo $ussu;?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align:center">
                    <input id="submit_button" type="submit" value="Guardar Encuesta" >
                </div>
                </form>
            </div>
			<!-- PARA USO DE javascript -->
            <div id="dyn_elements">
                <div class="pregunta" style="display:none">
                    <div class="borrar_pregunta">
                        <a href="#borrar_pregunta" class="borrar_pregunta"> X </a>
                    </div>
                    <span> Pregunta: </span>
                    <input type="text" class="pregunta_titulo" size="70" name="">
                    <br/>
                    <span> Exclusiva: </span>
                    <input type="radio" class="pregunta_exclusiva" name="exclusiva" value="1" checked="checked"> Si
                    <input type="radio" class="pregunta_exclusiva" name="exclusiva" value="0"> No
                    <br/>
                    <div class="opciones_box">
                        <span> Opciones: </span>
                        <a href="#agregar_opcion" class="agregar_opcion"> (Agregar) </a><br/>
                        <div class="opcion">
                            <input type="text" size="50">
                        </div>
                        <div class="opcion">
                            <input type="text" size="50">
                        </div>
                        <div class="opcion">
                            <input type="text" size="50">
                            <a href="#borrar_opcion" class="borrar_opcion"> X </a>
                        </div>
                    </div>
                    <input type="checkbox" class="comentario" value="1">
                    <span> Permitir ingresar comentario </span>
                </div>
                <div class="opcion" style="display:none">
                            <input type="text" size="50">
                            <a href="#borrar_opcion" class="borrar_opcion"> X </a>
                </div>
            </div>
<!-- ------------------------------------------>

        </body>
</html>

