<?php
	include_once("encuestas/clases/Paginador.Class.php");
    include_once("encuestas/clases/entidad/Encuesta.php"); 
    include_once("encuestas/clases/entidad/Usuario.php");
    include_once("encuestas/clases/DAO/EncuestaDAO.php");
    include_once("encuestas/clases/entidad/Opcion.php");
    include_once("encuestas/clases/entidad/Pregunta.php");
    
	
	$enc_dao = new EncuestaDAO();

	$pagina_actual = $_GET['pagina'];
	
    $total_filas = $enc_dao->totalEncuestas();
    
    $paginador = new PaginadorEncuesta($total_filas, 20, $pagina_actual);
    
    $encuestas = $enc_dao->traerLista($paginador->getDesde(), $paginador->getTotalFilasPaginaActual());
    

?>

<link href="encuestas/admin/css/general.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	table#lista{
		margin: 0 auto 0 auto;
		width:100%;
	}

	#submenu{
		text-align:left;
		margin-bottom: 20px;
		padding-left:10%;
		float: right;
	}

	.service_list, .service_list2{
		width:99%; 
		overflow:hidden;
		border:1px solid;
	}
	.service_list div, .service_list2 div{
		padding:10px;
		width:66px;
		overflow:hidden;
		float:left;
		text-align:center;
	}
</style>
    
<?php
    include("inc_menu.php");
    if($msje){
		if($msje==1){
			$mostrar='<div style="margin-top:30px;width:96%" class="advertencia1">Se ha editado correctamente la encuesta</div>';
		}elseif($msje==2){
			$mostrar='<div style="margin-top:30px;width:96%" class="advertencia1">Se ha agregado correctamente la encuesta</div>';
		}elseif($msje==3){
			$mostrar='<div style="margin-top:30px;width:96%" class="advertencia1">Se ha eliminado correctamente la encuesta</div>';
		}
	}
?>
            <p id="submenu">
                    <a href="administracion.php?pagina=encuestas/admin/form_encuestas.php&action=new"> Crear Nueva Encuesta <img src="img/add.png"></a>
            </p>
                <p> <h2> Listado Encuestas </h2> </p>
                <div><?=$mostrar?></div>
                <div class="service_list2" style="background-color:#eee">
					<div> Titulo </div>
					<div> Creada Por </div>
					<div>Fecha de Publicaci&oacute;n </div>
					<div>Fecha de Caducidad </div>
					<div> Tipo </div>
					<div> URL </div>
					<div> Estadisticas </div>
					<div> Modificar </div>
					<div> Eliminar </div>
					<div> Habilitado </div>
					<div> Obligatorio </div>
					
                </div>        
                <?php foreach($encuestas as $e):
                         if($reng++%2==0)
						   $clase="style='background-color:#F8F8F8;'";
						 else
						   $clase="";?>	
						  <div class="service_list" id="service" data="<?php echo $e->getHash();?>">    
							 <div> <?php echo $e->getTitulo();?> </div>
                             <div> <?php echo $e->getUsuario()->getNombre(); ?> </div>
                             <div> <?php echo $e->getFecha_de_publicacion(); ?> </div>
                             <div> <?php echo $e->getFecha_de_caducidad(); ?> </div>
                             <div> <?php echo $e->getTipo();?> </div>
                             <div> <a href="administracion.php?pagina=encuestas/Procesadores/Vista.php&idencuesta=<?php echo $e->getHash(); ?>"> URL para votar </div>
                             <div> <a href="administracion.php?pagina=encuestas/clases/entidad/Estadisticas.php&idencuesta=<?php echo $e->getId(); ?>"> Estadisticas </a></div>

                             <div>
                              <? if($e->getEstado()=='abierta'){ ?>
                                <a href="administracion.php?pagina=encuestas/admin/form_encuestas.php&action=edit&idencuesta=<?php echo $e->getId(); ?>">
                                    Editar
                                </a>
                                <? }?>			
                             </div>
                             <div>
							 <?if($e->getEstado()=='abierta'){ ?>
								<a  class="delete" href="administracion.php?pagina=encuestas/admin/form_encuestas.php&action=delete&idencuesta=<? echo  $e->getHash();?>">&nbsp;</a>
							 <? }?>	
							 </div>	
							  <div> 
                              <? if($e->getHabilitar()==1){ echo "si";}else{ echo "no"; } ?>
                            </div>	
                            <div> 
                              <? if($e->getObligatorio()==1){ echo "si";}else{ echo "no "; } ?>
                            </div>	
						 </div>
                        <?php endforeach; ?>
                       <p>
                            <?php
                                if($paginador->getPaginaActual() > 1):
                            ?>
                                <a href="administracion.php?pagina=encuestas/admin/lista_encuestas.php&pagina=<? ($paginador->getPaginaActual()-1);?>">
                                        <<
                                    </a>

                            <?php
                                else:
                            ?>
                                <<
                            <?php
                                endif;
                                
                                echo "Pagina {$paginador->getPaginaActual()} de {$paginador->getPaginaUltima()}";

                                if($paginador->getPaginaActual() < $paginador->getPaginaUltima()):
                            ?>
                                    <a href="administracion.php?pagina=encuestas/admin/lista_encuestas.php&pagina=<? ($paginador->getPaginaActual()+1);?>">
                                        >>
                                    </a>
                            <?php
                                else:
                            ?>
                                >>
                            <?php
                                endif;
                            ?>
                          </p>

