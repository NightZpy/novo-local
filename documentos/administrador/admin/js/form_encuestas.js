$(document).ready(function(){

    $("#agregar_pregunta").click(agregarPregunta);
    $(".borrar_pregunta").click(borrarPregunta);
    $(".agregar_opcion").click(agregarOpcion);
    $(".borrar_opcion").click(borrarOpcion);
    $("#submit_button").click(validarForm);
    $("#asignar_usuario").click(asignarUsuario);
    $("#desasignar_usuario").click(desasignarUsuario);
    $("#select_tipo").change(changeTipoEncuesta).change();
    $("#select_estructura").change(changeTipoEstructura).change();
    
    nombrarForm();
})


    function getCantidadPreguntas(){
        return $("#main .pregunta").size();
    }

    function agregarPregunta(){
        $("#dyn_elements > .pregunta").clone(true).appendTo($("#preguntas_box")).slideDown();
        nombrarForm();
    }

    function borrarPregunta(){
        $(this).parents(".pregunta").slideUp(function(){$(this).remove();});
        nombrarForm();
    }

    function agregarOpcion(){
        var pregunta = $(this).parents(".pregunta");
        $("#dyn_elements > .opcion").clone(true).appendTo($(pregunta).find(".opciones_box")).slideDown();
    }

    function borrarOpcion(){
        $(this).parents(".opcion").slideUp(function(){$(this).remove();});
    }

    function validarForm(){
        nombrarForm();
        return true;
    }

    function nombrarForm(){
        $(".pregunta").each(function(index){
            $(this).find(".pregunta_titulo").attr("name", "preguntas[" + index + "][titulo]");
            $(this).find(".pregunta_exclusiva").attr("name", "preguntas[" + index + "][exclusiva]");
            $(this).find(".opcion input").attr("name", "preguntas[" + index + "][opciones][]");
            $(this).find(".comentario").attr("name", "preguntas[" + index + "][comentario]");
        });
    }

    function asignarUsuario(){
       $("#usuarios_no_asignados option:selected").remove().appendTo($("#usuarios_asignados"));
    }

    function desasignarUsuario(){
       $("#usuarios_asignados option:selected").remove().appendTo($("#usuarios_no_asignados"));
    }

    function changeTipoEncuesta(){
        if($(this).val() == "privada")
            $("#asignacion_usuarios_wrapper").slideDown();
        else
            $("#asignacion_usuarios_wrapper").slideUp();
    }

