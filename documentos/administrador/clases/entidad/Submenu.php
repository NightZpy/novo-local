<?php
class Submenu{
	var $id_menu;
    var $id;
    var $titulo;
    var $orden;

    function __construct($id="", $id_menu="", $titulo="",$orden=""){
        $this->titulo = $titulo;
        $this->id = $id;
        $this->orden = $orden;
        $this->id_menu = $id_menu;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getIdMenu(){
        return $this->id_menu;
    }

    public function setIdMenu($id){
        $this->id_menu = $id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }

    public function renderSubmenu(){

        return $this->titulo;

    }
     public function editarSubmenun(){
			$renderSubmenu="<div >";
			$renderSubmenu.="<input type='text' value='$this->titulo'>";
			$renderSubmenu.="</div>";
        return $renderSubmenu;

    }

}
?>
