<?php
class Pagina {

    var $id;
    var $titulo;
    var $subtitulo;
    var $descripcion;
    var $imagen;
    var $columnas = array();
    var $articulo = array();
    var $galeria;
    var $imagen;
    var $formulario;
	
   function  __construct($id= null, $titulo= null, $subtitulo = null, $descripcion = null, $usuario = null, $preguntas = array(),$hash="", $tipo = "", $estructura = "",$controladores = array(),$estado = "",$asignar,$habilitar= null, $descripcion=null,$imagen=null,$usuario_modifico=null,$obligatorio) {
        $this->id = $id;
        $this->fecha_de_caducidad = $fecha_de_caducidad;
        $this->fecha_de_publicacion = $fecha_de_publicacion;
        $this->titulo = $titulo;
        $this->usuario = $usuario;
        $this->hash = $hash;
        $this->preguntas = $preguntas;
        $this->tipo = $tipo;
        $this->estructura = $estructura;
        $this->controladores = $controladores;
        $this->estado = $estado;
        $this->asignar = $asignar;
        $this->habilitar = $habilitar;
        $this->descripcion = $descripcion;
        $this->imagen = $imagen;
        $this->obligatorio = $obligatorio;
        $this->usuario_modifico = $usuario_modifico;
    }

    static function crearEncuesta($params){	

        $id_usuario = $params['id_usuario'];
        $titulo = $params['titulo'];
        $descripcion = $params['descripcion'];
        $imagen = $_FILES['imagen'];
        $tipo = $params['tipo'];
        $estructura = $params['estructura'];
        $id_encuesta = $params['id_encuesta'];
        $fecha_de_publicacion = $params['fecha_de_publicacion'];
        $fecha_de_caducidad = $params['fecha_de_caducidad'];
        $asignar = $params['asignar'];
        $obligatorio = $params['obligatorio'];
        $habilitar = $params['habilitar'];
        $usuario_modifico = $params['usuario_modifico'];
         
        $preguntas = array();
        foreach($params['preguntas'] as $key=>$p){
            $titulo_p = $p['titulo'];
            $exclusiva = $p['exclusiva'];
            $comentario = $p['comentario'];
            if(!isset($comentario)) $comentario = "0";
            else $comentario = "1";

            $opciones = array();
            foreach($p['opciones'] as $key2=>$o){
                $opciones[] = new Opcion(null, $key2, $o);
            }

            $preguntas[] = new Pregunta(null, $titulo_p, $key, $comentario, $exclusiva, $opciones,$id_encuesta);
        }
        return new Encuesta($id_encuesta, $fecha_de_publicacion, $fecha_de_caducidad, $titulo, $id_usuario, $preguntas, "", $tipo, $estructura, $params['controladores'] ,"abierta",$asignar,$habilitar,$descripcion,$imagen,$usuario_modifico,$obligatorio);
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }
       public function getObligatorio() {
        return $this->obligatorio;
    }

    public function setObligatorio($titulo) {
        $this->obligatorio = $obligatorio;
    }
        public function getAsignar() {
        return $this->asignar;
    }

    public function setAsignar($asignar) {
        $this->asignar = $asignar;
    }


    public function getPreguntas() {
        return $this->preguntas;
    }

    public function setPreguntas($preguntas) {
        $this->preguntas = $preguntas;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
     public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->id = $descripcion;
    }
    public function getImagen() {
        return $this->imagen;
    }

    public function setImagen($hash) {
        $this->imagen = $imagen;
    }
     public function getUsuarioModifico() {
        return $this->usuario_modifico;
    }

    public function setUsuarioModifico($usuario_modifico) {
        $this->id = $usuario_modifico;
    }
    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setControladores($controladores){
        $this->controladores = $controladores;
    }

    public function getControladores(){
        return $this->controladores;
    }

    public function setTipo($tipo){
        $this->tipo = $tipo;
    }

    public function getTipo(){
        return $this->tipo;
    }
    
    public function setHabilitar($habilitar){
        $this->habilitar = $habilitar;
    }

    public function getHabilitar(){
        return $this->habilitar;
    }
    
    public function setEstructura($estructura){
        $this->tipo = $estructura;
    }

    public function getEstructura(){
        return $this->estructura;
    }

    function agregarPregunta($pregunta) {
        array_push($this->preguntas,$pregunta);
    }


    public function cantidadColumnas() {
        return sizeof($this->columnas);
    }

    
}
?>
