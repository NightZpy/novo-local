<?php
class Administrador {
    var $id;
    var $mail; 
    var $contraseña;
    var $nombre;
    var $apellido;
    var $telefono;
    var $permiso;
   
    function  __construct($mail, $contraseña, $nombre, $apellido, $telefono = null , $permiso){
        $this->mail = $mail;
        $this->contraseña = $contraseña;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->telefono = $telefono;
        $this->permiso = $permiso;
    }

    function getMail(){
        return $this->mail;
    }

    function getNombre(){
        return $this->nombre;
    }

	function getContraseña(){
        return $this->contraseña;
    }

	function getApellido(){
        return $this->apellido;
    }

    function getTelefono(){
        return $this->telefono;
    }
     public static function crearListaUsuarios($array){
        $usuarios = array();
       
        if(!isset($array)) return $usuarios;
        foreach($array as $a){
            $usuarios[] = new Usuario("", $a);
        }
        return $usuarios;
    }
}
?>

