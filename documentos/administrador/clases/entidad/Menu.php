<?php
class Menu {

    var $id;
    var $id_encuesta;
    var $titulo;
    var $orden;
    var $opciones = array();
    var $multiple;  //indica si esta pregunta tendra multiples respuestas
    var $exclusiva; //indica si puede haber mas de una respuesta (radio/option)
    var $abierta;

    public function __construct($id="",$titulo="",$orden="",$abierta=0, $exclusiva="", $opciones=array(),$id_encuesta=""){
        $this->titulo = $titulo;
        $this->id = $id;
        $this->orden = $orden;
        $this->opciones = $opciones;
        $this->abierta = $abierta;
        $this->exclusiva = $exclusiva;
        $this->id_encuesta = $id_encuesta;
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId_encuesta() {
        return $this->id_encuesta;
    }

    public function setId_encuesta($id_encuesta) {
        $this->id_encuesta = $id_encuesta;
    }

    public function getAbierta(){
        return $this->abierta;
    }

    public function setAbierta($abierta){
        $this->abierta = $abierta;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }

    public function getOpciones() {
        return $this->opciones;
    }

    public function setOpciones($opciones) {
        $this->opciones = $opciones;
    }

    public function getMultiple() {
        return $this->multiple;
    }

    public function setMultiple($multiple) {
        $this->multiple = $multiple;
    }

    public function getExclusiva() {
        return $this->exclusiva;
    }

    public function setExclusiva($exclusiva) {
        $this->exclusiva = $exclusiva;
    }


    function renderPregunta(){

        $renderPregunta = $this->titulo . "<br>\n";

        $renderPregunta .= "<input type='hidden' name='responses[]' value='$this->id'>\n";

        foreach ($this->opciones as $opc){
        
            $opcionActual = $opc->renderOpcion();

            if($this->exclusiva){
                $renderPregunta.="<input type='radio' name='$this->id[]' value='$opcionActual'>$opcionActual<br>\n";
            }else{
                $renderPregunta.="<input type='checkbox' name='$this->id[]' value='$opcionActual'>$opcionActual<br>\n";
            }

        }

        if($this->abierta!=0) { $renderPregunta.="<input type='text' name='$this->id-txt'>"; }

        return $renderPregunta;
    }

    function renderPregunta2(){

        $renderPregunta = $this->titulo . "<br>\n";

        $renderPregunta .= "<input type='hidden' name='responses[]' value='$this->id'>\n";

        foreach ($this->opciones as $opc){

            $opcionActual = $opc->renderOpcion();

            if($this->exclusiva){
                $renderPregunta.="<input type='radio' name='$this->id[]' value='$opcionActual'>$opcionActual<br>\n";
            }else{
                $renderPregunta.="<input type='checkbox' name='$this->id[]' value='$opcionActual'>$opcionActual<br>\n";
            }

        }

        if($this->abierta!=0) { $renderPregunta.="<br><input type='text' name='$this->id-txt'>"; }

        return $renderPregunta;
    }
              
                             //  <a href="#borrar_opcion" class="borrar_opcion"> X </a>
    function editarPregunta(){
		if ($this->exculsiva)
			$exclusiva1="checked='checked'";
		else
			$exclusiva0="checked='checked'";
		
		$renderPregunta = "<div class='pregunta'>
							<div class='borrar_pregunta'>
								<a href='#borrar_pregunta' class='borrar_pregunta'> X </a>
							</div>";
					$renderPregunta = "<span> Pregunta: </span>";
					$renderPregunta .= "<input type='text' class='pregunta_titulo' size='70' name='' value='".$this->titulo."'><br>\n";
					$renderPregunta .= "  <span> Exclusiva: </span>
									<input type='radio' class='pregunta_exclusiva' name='exclusiva' value='1' $exclusiva1 > Si
									<input type='radio' class='pregunta_exclusiva' name='exclusiva' value='0' $exclusiva0 > No
									<br/>";
					$renderPregunta .= "<span> Opciones: </span><a href='#agregar_opcion' class='agregar_opcion'>  (Agregar) </a><br/>";
					$renderPregunta .= "<div class='opciones_box'>";
					
						foreach ($this->opciones as $opc){
							$renderPregunta = $opc->editarOpcion();
						}

         $renderPregunta .= "</div></div>";
        
        return $renderPregunta;
    }

    function agregarOpcion($opcion){
        array_push($this->opciones, $opcion);
    }
}
?>
