<?php
class Articulo {

    var $sesion;
    var $id;
    var $id_encuesta;
    var $id_usuario;


    public function getId_encuesta() {
        return $this->id_encuesta;
    }

    public function setId_encuesta($id_encuesta) {
        $this->id_encuesta = $id_encuesta;
    }
   

    function __construct($id = "", $sesion = "", $id_usuario = ""){
        $this->id = $id;
        $this->sesion = $sesion;
        $this->id_usuario = $id_usuario;
    }

    public function getSesion() {
        return $this->sesion;
    }

    public function setSesion($sesion) {
        $this->sesion = $sesion;
    }
    public function getUsuario() {
        return $this->id_usuario;
    }

    public function setUsuario($id_usuario) {
        $this->sesion = $id_usuario;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }


}
?>
