<?php

class PaginadorEncuesta {
	private $filas_por_pagina;
	private $total_filas;
	private $total_paginas;
	private $pagina_actual;
	private $fila_desde;


	private $offset;
	/* Usado para el caso en que la primer pagina tenga distinto numero de elementos
	offset = cantidad de elementos menos que tiene la primer pagina */
	
	function __construct($total, $filas_por_pag, $pag_actual, $offs = 0)
	{	
		$this->total_filas = $total;
		$this->filas_por_pagina = $filas_por_pag;
		$this->offset = $offs;

                $this->setTotalPaginas();
		$this->setPaginaActual($pag_actual);
		$this->setFila_desde();


	}

        private function setTotalPaginas()
        {
                if($this->total_filas == 0)
                    $this->total_paginas = 1;
                else
                    $this->total_paginas = ceil(($this->total_filas + $this->offset) / $this->filas_por_pagina);

        }
	
	private function setPaginaActual($actual)
	{
		if((isset($actual)) && (is_numeric($actual)) && ($actual > 0) && ($actual <= $this->total_paginas))
			$this->pagina_actual = $actual;		
		else
			$this->pagina_actual = 1;
	}
	
	private function setFila_desde()
	{
		if($this->pagina_actual == 1)
			$this->fila_desde = 0;
		else
			$this->fila_desde = ($this->filas_por_pagina - $this->offset) + 
								($this->pagina_actual-2) * $this->filas_por_pagina;
	}
	
	
	function getDesde(){
		return $this->fila_desde;
	}
	
	function getTotalFilasPaginaActual(){
		//Error, la cantidad de filas de la ultima pagina puede ser menor
		if($this->pagina_actual == 1)
			return ($this->filas_por_pagina - $this->offset);
		else
			return $this->filas_por_pagina;
	}
	
	
	function getPaginaActual(){
		return $this->pagina_actual;
	}
	
	function getPaginaUltima(){
		return $this->total_paginas;
	}
	
	
	
	
	function mostrarTodo(){
		echo "filas por pagina " .  $this->filas_por_pagina . "<br> ";
		echo "offset " . $this->offset . "<br> ";
		echo "total_filas  " . $this->total_filas . "<br> ";
		echo "total paginas   " .  $this->total_paginas . "<br> ";
		echo "pagina actual   " . $this->pagina_actual . "<br> ";
		echo "fila_desde  " . $this->fila_desde . "<br> ";

		echo "total filas pagina actual " . $this->getTotal_filas_pagina();
		
	}

}

?>
