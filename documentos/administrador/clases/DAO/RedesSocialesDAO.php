<?php

class VotanteDAO {
    const NAME_MAX_LENGTH = 256;
    const EMAIL_MAX_LENGTH = 256;
    //ERRORES
    const FIELD_TO_LONG = -1;
    const INVALID_EMAIL = -2;
    const FORMAT_ERROR = -3;

    function importarListaTextoPlano($lista, $id_encuesta, $id_controlador){
        $delimitador = " - ";
        $votantes = array();
        $lista = explode("\n", $lista);
        
        foreach($lista as $line){
            $temp = explode($delimitador, $line);
            if(count($temp) != 2)
                return self::FORMAT_ERROR;

            $nombre = $temp[0];
            $email = $temp[1];

            if((strlen($nombre) > self::NAME_MAX_LENGTH) ||(strlen($email) > self::EMAIL_MAX_LENGTH))
                return self::FIELD_TO_LONG;

            if(!$this->validarEmail($email))
                return self::INVALID_EMAIL;

            $votantes[] = new Votante("", $nombre, $email, $id_encuesta, $id_controlador);
        }

        $this->persistirLista($votantes);

        return true;
    }

    public function persistirLista($votantes){
        
        foreach($votantes as $votante)
            $this->persistir($votante);
    }

    public function persistir($votante){
        
        //VERIFICACION SI EXISTE EL MAIL
        $query = "SELECT id FROM votanteslu WHERE email = '" . $votante->getEmail() . "'";
        $res = mysql_query($query) or die("ERR SQL");
        $fila = mysql_fetch_array($res);
        $flag_new = false;

        if(!$fila){
            //INSERT NUEVO VOTANTE
            $query = "INSERT INTO votanteslu (nombre, email)
                VALUES('{$votante->getNombre()}', '{$votante->getEmail()}')";
            mysql_query($query) or die("ERR SQL");
            $votante->setId(mysql_insert_id());
            $this->asociarAEncuesta($votante);
            $this->asociarAEncuestaControlador($votante);
            return $votante->getId();
        }
        
        $votante->setId($fila[0]);
            
        if(!$this->estaAsociadoAEncuesta($votante))
            $this->asociarAEncuesta($votante);

        if(!$this->estaAsociadoAEncuestaControlador($votante))
            $this->asociarAEncuestaControlador($votante);

        return $votante->getId();
    }

    public function estaAsociadoAEncuesta($votante){
        $query = "SELECT count(*) FROM votantes_encuestaslu WHERE id_encuesta = " . $votante->getIdEncuesta()
        . " AND id_votante = " . $votante->getId();

        $res = mysql_query($query) or die("Error SQL $query");

        $fila = mysql_fetch_assoc($res);
        if($fila[0] > 0)
            return true;
        else
            return false;
    }

    public function estaAsociadoAEncuestaControlador($votante){
       $query = "SELECT count(*) FROM votantes_encuestas_controladoreslu WHERE id_encuesta = " . $votante->getIdEncuesta()
        . " AND id_votante = " . $votante->getId() . " AND id_usuario = " . $votante->getIdControlador();
        $res = mysql_query($query) or die("E SQL $query");
        $fila = mysql_fetch_array($res);
        if($fila[0] > 0)
            return true;
        else
            return false;
    }
    
    public function asociarAEncuesta($votante){
        $hash = md5($votante->getId() . $votante->getIdEncuesta() . date('dms'));
        $query = "INSERT INTO votantes_encuestaslu (id_votante, id_encuesta, hash)
                VALUES (" . $votante->getId() . ", " . $votante->getIdEncuesta() . ", '$hash')";
        mysql_query($query) or die("ERR SQL $query");
    }
    
    public function asociarAEncuestaControlador($votante){
        $query = "INSERT INTO votantes_encuestas_controladoreslu (id_votante, id_encuesta, id_usuario)
                VALUES (" . $votante->getId() . ", " . $votante->getIdEncuesta() . ", "
                . $votante->getIdControlador() . ")";
        mysql_query($query) or die("ERR SQL");
    }

    private function validarEmail(){
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;

            return false;
    }
}
?>
