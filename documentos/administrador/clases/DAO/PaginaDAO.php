<?php
include ("encuestas/clases/DAO/PreguntaDAO.php");

class EncuestaDAO {

	var $pregDao;

    function __construct(){
        
        $this->pregDao = new PreguntaDAO();
        
    }


    function persistir($encuesta){

        $fecha_de_publicacion = mysql_real_escape_string($encuesta->getFecha_de_publicacion());
        $fecha_de_caducidad = mysql_real_escape_string($encuesta->getFecha_de_caducidad());
        $id_usuario = mysql_real_escape_string($encuesta->getUsuario());
        $titulo = mysql_real_escape_string($encuesta->getTitulo());
        $tipo = mysql_real_escape_string($encuesta->getTipo());
        $estructura = mysql_real_escape_string($encuesta->getEstructura());
        $estado = mysql_real_escape_string($encuesta->getEstado());
        $asignar = mysql_real_escape_string($encuesta->getAsignar());
        $habilitar = mysql_real_escape_string($encuesta->getHabilitar());
        $descripcion = mysql_real_escape_string($encuesta->getDescripcion());
        $imagen = $encuesta->getImagen();
        $obligatorio = mysql_real_escape_string($encuesta->getObligatorio());
		
		//print_r($imagen);
		$img="";
		if (isset($imagen)){
			$error=$imagen["error"];
			if ($error == UPLOAD_ERR_OK) {
					   echo "$error_codes[$error]";
					   $nbre=$imagen["name"];	
					   $directorio = './encuestas/Procesadores/imagen/';
					   $bu=move_uploaded_file($imagen["tmp_name"],$directorio.$nbre) or die("error, el archivo no se guardo");
					   $img="$nbre";
			}
		}
		
        //INSERT ENCUESTA
        $query = "INSERT INTO encuestaslu (fecha_de_publicacion, fecha_de_caducidad, id_usuario, titulo, tipo, estructura, estado, asignar, habilitar, descripcion, imagen, obligatorio)
                  VALUES  ('$fecha_de_publicacion','$fecha_de_caducidad','$id_usuario','$titulo', '$tipo', '$estructura', 'abierta',$asignar,$habilitar,'$descripcion','$img','$obligatorio')";
		//echo $query;
			
        $result = mysql_query($query) or die("Error de SQL encuesta insert $query");

        $id_encuesta = mysql_insert_id();

        $hash = md5($id_encuesta);

        //INSERT HASH
        $query = "UPDATE encuestaslu SET hash='$hash' WHERE id='$id_encuesta' LIMIT 1";
        $result = mysql_query($query) or die("Error de SQL  encuesta update 1 $query");

        //INSERT PREGUNTAS
        foreach ($encuesta->preguntas as $pregunta){
            $pregunta->setId_encuesta($id_encuesta);
            $this->pregDao->persistir($pregunta);
        }

        //INSERT CONTROLADORES DE USUARIOS
        if($encuesta->controladores){
			foreach($encuesta->controladores as $c){
				$query = "INSERT INTO encuesta_controladoreslu
						VALUES (NULL, $id_encuesta, '$c')";
				mysql_query($query) or die("Err SQL $query");
			}
		}
        return $id_encuesta;

    }

    function modificar($encuesta){
		//print_r($encuesta);
		$fecha_de_publicacion = mysql_real_escape_string($encuesta->getFecha_de_publicacion());
        $fecha_de_caducidad = mysql_real_escape_string($encuesta->getFecha_de_caducidad());
        $titulo = mysql_real_escape_string($encuesta->getTitulo());
        $tipo = mysql_real_escape_string($encuesta->getTipo());
        $estructura = mysql_real_escape_string($encuesta->getEstructura());
        $id = mysql_real_escape_string($encuesta->getId());
        $asignar = mysql_real_escape_string($encuesta->getAsignar());
        $descripcion = mysql_real_escape_string($encuesta->getDescripcion());
        $imagen = $encuesta->getImagen();
        $usuario_modifico = mysql_real_escape_string($encuesta->getUsuarioModifico());
        $habilitar = mysql_real_escape_string($encuesta->getHabilitar());
        $obligatorio = mysql_real_escape_string($encuesta->getObligatorio());
		
		if (isset($imagen)){
			$error=$imagen["error"];
			if ($error == UPLOAD_ERR_OK) {
					   echo "$error_codes[$error]";
					   $nbre=$imagen["name"];	
					   $directorio = './encuestas/Procesadores/imagen/';
					   $bu=move_uploaded_file($imagen["tmp_name"],$directorio.$nbre) or die("error, el archivo no se guardo");
					   $img=", imagen='$nbre'";
			}
		}
        
        $query = "UPDATE encuestaslu SET fecha_de_publicacion='$fecha_de_publicacion', fecha_de_caducidad='$fecha_de_caducidad', titulo='$titulo', estructura='$estructura', tipo='$tipo', asignar=$asignar, descripcion='$descripcion', usuario_modifico=$usuario_modifico, obligatorio=$obligatorio, habilitar=$habilitar $img 
                  WHERE id=$id
                  LIMIT 1";
	 //  echo $query;
		
        $result = mysql_query($query) or die("Error de SQL  encuesta update 2 $query");

        // Hacer drop de todas las preguntas y cargar las nuevas

       $this->pregDao->eliminarPorIdEncuesta($id);

        foreach($encuesta->preguntas as $pregunta){
            $this->pregDao->persistir($pregunta);
        }
        
        if($encuesta->controladores){
			if($tipo=='privada'){
			$query = "DELETE FROM  encuesta_controladoreslu WHERE id_encuesta='$id'";
			//echo $query;
			$result = mysql_query($query) or die("Error de SQL  encuesta select 2 $query");
			//print_r($encuesta->getControl);
				  //INSERT CONTROLADORES DE USUARIOS
				foreach($encuesta->controladores as $c){
					$query = "INSERT INTO encuesta_controladoreslu
									VALUES (NULL, $id, '$c')";
					//echo $query;
					mysql_query($query) or die("Err SQL $query");
				 }
			}
		}
    }

    function eliminar($id){
        $query = "DELETE FROM encuestaslu WHERE id='$id' LIMIT 1";
		//ECHO "$query";
       $result = mysql_query($query) or die("Error de SQL $query");
        
       $this->pregDao->eliminarPorIdEncuesta($id);
    }

    function traerPorId($idEncuesta){

        $query = "SELECT * FROM encuestaslu WHERE id='$idEncuesta' LIMIT 1";
       // echo "$query <br>";
        $result = mysql_query($query) or die("Error de SQL  encuesta select 1 $query");

        $temp=mysql_fetch_array($result);

        $id = $temp["id"];
        $fecha_de_publicacion = $temp["fecha_de_publicacion"];
        $fecha_de_caducidad = $temp["fecha_de_caducidad"];
        $id_usuario = $temp["id_usuario"];
        $titulo = $temp["titulo"];
        $tipo = $temp["tipo"];
        $estado = $temp["estado"];
        $estructura = $temp["estructura"];
        $asignar = $temp["asignar"];
        $habilitar = $temp["habilitar"];
        $usuario_modifico = $temp["usuario_modifico"];
        $descripcion = $temp["descripcion"];
        $imagen = $temp["imagen"];
        $obligatorio = $temp["obligatorio"];
        $arrayPreguntas = $this->pregDao->traerPorIdEncuesta($idEncuesta);
      //  print_r($arrayPreguntas);
      
      return new Encuesta($id, $fecha_de_publicacion, $fecha_de_caducidad,  $titulo, $id_usuario, $arrayPreguntas, "", $tipo, $estructura, "",$estado,$asignar,$habilitar,$descripcion,$imagen,$usuario_modifico,$obligatorio);


    }

    function traerPorHash($hash){

        $query = "SELECT * FROM encuestaslu WHERE hash='$hash' LIMIT 1";
       // echo $query;
        $result = mysql_query($query) or die("Error de SQL  encuesta select 2 $query");

        if(mysql_num_rows($result) == 0){
            return null;
        }
        
        $temp=mysql_fetch_array($result);

     
        $id = $temp["id"];
        $fecha_de_publicacion = $temp["fecha_de_publicacion"];
        $fecha_de_caducidad = $temp["fecha_de_caducidad"];
        $id_usuario = $temp["id_usuario"];
        $titulo = $temp["titulo"];
        $tipo = $temp["tipo"];
        $estado = $temp["estado"];
        $estructura = $temp["estructura"];
        $asignar = $temp["asignar"];
        $habilitar = $temp["habilitar"];
        $usuario_modifico = $temp["usuario_modifico"];
        $descripcion = $temp["descripcion"];
        $imagen = $temp["imagen"];
        $obligatorio = $temp["obligatorio"];
        $arrayPreguntas = $this->pregDao->traerPorIdEncuesta($id);
      // print_r($arrayPreguntas);
      
      return new Encuesta($id, $fecha_de_publicacion, $fecha_de_caducidad,  $titulo, $id_usuario, $arrayPreguntas, "", $tipo, $estructura, "",$estado,$asignar,$habilitar,$descripcion,$imagen,$usuario_modifico,$obligatorio);

}

    function traerLista($offset="0", $cantidad="1000"){

        $arrayEncuestas = array();
        $query = "SELECT enc.*, usu.* FROM encuestaslu AS enc 
				JOIN usuario AS usu ON(enc.id_usuario = usu.id_usuario)
                LIMIT $offset,$cantidad";
            //    echo $query;
        $resultSet = mysql_query($query) or die("Error de SQL encuesta select  3 $query");

        while($temp = mysql_fetch_assoc($resultSet)){

            $id = $temp["id"];
            $fecha_de_publicacion = $temp["fecha_de_publicacion"];
            $fecha_de_caducidad = $temp["fecha_de_caducidad"];
            $titulo = $temp["titulo"];
            $usuario = new Usuario($temp["cod_us"], $temp["id_usuario"], $temp["apellido"]." ".$temp["nombres"]);
            $hash = $temp["hash"];
            $estado = $temp["estado"];
			$asignar = $temp["asignar"];
            $tipo = $temp["tipo"];
            $estructura = $temp["estructura"];
            $imagen = $temp["imagen"];
            $usuario_modifico = $temp["usuario_modifico"];
            $obligatorio = $temp["obligatorio"];
            $habilitar= $temp["habilitar"];
  
            array_push($arrayEncuestas, new Encuesta($id, $fecha_de_publicacion, $fecha_de_caducidad, $titulo, $usuario, null, $hash,$tipo,$estructura,"",$estado,$asignar,$habilitar,$descripcion,$imagen,$usuario_modifico,$obligatorio));
        }

        return $arrayEncuestas;

    }

    public function totalEncuestas(){
        $query = "SELECT count(*) FROM encuestaslu";
        $res = mysql_query($query) or die("ERROR SQL");
        $fila = mysql_fetch_array($res);
        return $fila[0];
    }

    function traerListaControladores($id_encuesta){

        $query = "SELECT * FROM encuesta_controladoreslu 
		JOIN usuario ON (encuesta_controladoreslu.cod_us = usuario.cod_us) 
		WHERE encuesta_controladoreslu.id_encuesta=$id_encuesta";

        $resultSet = mysql_query($query) or die("Error de SQL select 4 $query");
		
		$controladores="";
		
        while($row = mysql_fetch_assoc($resultSet)){
			$controladores.="<option value='".$row[cod_us]."' selected='selected'>(".htmlentities($row[cod_us]).") ".htmlentities($row[apellido])." ".htmlentities($row[nombres])."</option>";
        }

        return $controladores;

    }

}
?>
