<?php
include("encuestas/clases/DAO/OpcionDAO.php");
class PreguntaDAO {

    var $opcDao;

    function __construct(){

        $this->opcDao = new OpcionDAO();
    }

    function persistir($pregunta){

        $id_encuesta = mysql_real_escape_string($pregunta->getId_encuesta());
        $orden = mysql_real_escape_string($pregunta->getOrden());
        $titulo = mysql_real_escape_string($pregunta->getTitulo());
        $exclusiva = mysql_real_escape_string($pregunta->getExclusiva());
        $abierta = mysql_real_escape_string($pregunta->getAbierta());

        $query = "INSERT INTO preguntaslu (id_encuesta, orden, titulo, exclusiva, abierta)
                  VALUES ('$id_encuesta', '$orden', '$titulo', '$exclusiva', '$abierta')";
     //   echo $query;
        $resultSet = mysql_query($query) or die("Error de SQL  $query");       

        $id_pregunta = mysql_insert_id();
        foreach($pregunta->opciones as $opcion){
            $opcion->setIdPregunta($id_pregunta);
            $this->opcDao->persistir($opcion);
        }

        return $id_pregunta;
    }
      
    function modificar($pregunta){
        $id = mysql_real_escape_string($pregunta->getId());
        #$id_pregunta = mysql_real_escape_string($pregunta->getId_encuesta());
        $titulo = mysql_real_escape_string($pregunta->getTitulo());
        $orden = mysql_real_escape_string($pregunta->getOrden());
        #$multiple = mysql_real_escape_string($pregunta->getMultiple());
        $exclusiva = mysql_real_escape_string($pregunta->getExclusiva());
        $abierta = mysql_real_escape_string($pregunta->getAbierta());

        $query = "UPDATE preguntaslu 
                  SET titulo='$titulo', exclusiva='$exclusiva', abierta='$abierta'
                  WHERE id='$id'
                  LIMIT 1";

        $result = mysql_query($query) or die("Error de SQL  $query");
        foreach($pregunta->opciones as $opcion){
            $opcion->setIdPregunta($id_pregunta);
            $this->opcDao->modificar($opcion);
        }

    }

    function eliminar($id){
        $query = "DELETE FROM preguntaslu WHERE id='$id' LIMIT 1";
        $result = mysql_query($query) or die("Error de SQL");

    }

    function eliminarPorIdEncuesta($idEncuesta){
		
        $this->opcDao->eliminarPorIdEncuesta($idEncuesta);
        
        $query =    "DELETE preguntaslu FROM preguntaslu
                    JOIN encuestaslu ON (preguntaslu.id_encuesta = encuestaslu.id)
                    WHERE encuestaslu.id = '$idEncuesta'";
		//echo $query."<br>";
        $result = mysql_query($query) or die("Error de SQL  $query");
       
    }

    function traerPorId($id){

        $query = "SELECT * FROM preguntaslu WHERE id='$id' LIMIT 1";
        $result = mysql_query($query) or die("Error de SQL  $query");

        $temp = mysql_fetch_array($result);

        $id_encuesta = $temp["id_encuesta"];
        $orden = $temp["orden"];
        $titulo = $temp["titulo"];
        $exclusiva = $temp["exclusiva"];
        $abierta = $temp["abierta"];

        $opciones = $this->opcDao->traerPorIdPregunta();


        return new Pregunta($id, $titulo, $orden, $abierta, $exclusiva);

    }

    function traerPorIdEncuesta($idEncuesta){

        $arrayPreguntas = array();
        $query = "SELECT * FROM preguntaslu WHERE id_encuesta = '$idEncuesta' ORDER BY orden";
		//echo $query;
        $resultSet = mysql_query($query) or die("Error de SQL  $query");

        While($temp = mysql_fetch_array($resultSet)){

            $id_pregunta = $temp["id"];
            $id_encuesta = $temp["id_encuesta"];
            $orden = $temp["orden"];
            $titulo = $temp["titulo"];
            $exclusiva = $temp["exclusiva"];
            $abierta = $temp["abierta"];            

            $arrayOpc = $this->opcDao->traerPorIdPregunta($id_pregunta);

            $currPreg = new Pregunta($id_pregunta, $titulo, $orden, $abierta, $exclusiva, $arrayOpc);
            array_push($arrayPreguntas, $currPreg);
        }

        return $arrayPreguntas;

    }

    function traerLista($offset="0", $cantidad="1000"){
        
        $arrayPreguntas = array();
        $query = "SELECT * FROM preguntaslu LIMIT $offset,$cantidad";
        $resultSet = mysql_query($query) or die("Error de SQL  $query");

        While($temp = mysql_fetch_array($resultSet)){

            $id_encuesta = $temp["id_encuesta"];
            $orden = $temp["orden"];
            $titulo = $temp["titulo"];
            $exclusiva = $temp["exclusiva"];
            $abierta = $temp["abierta"];

            array_push($arrayPreguntas, new Pregunta($id, $titulo, $orden, $abierta, $exclusiva));
        }

        return $arrayPreguntas;

    }
}
?>
