<?php
//corren en REMOTO.
//########################## CLASE RESPONDEDOR ##############################

class Respondedor
 {
  var $peticion_sql;
  var $respuestaMySql;
  var $separadorCampos = "##";
  var $separadorRegistros = "||";

  //-------------------------------------------------
  function Respondedor($parPeticion = "")
   {
    if($parPeticion <> "")
      $this->setPeticion_sql($parPeticion);
   }
   
  //-------------------------------------------------
  function setPeticion_sql($parPeticion_sql)
   {
    $this->peticion_sql = $parPeticion_sql;
   }

  //-------------------------------------------------
  function setSeparadorCampos($parSeparadorCampos)
   {
    if ($parSeparadorCampos <> "")
      $this->separadorCampos = $parSeparadorCampos;
   }

  //-------------------------------------------------
  function getSeparadorCampos()
   {
    return $this->separadorCampos;
   }

  //-------------------------------------------------
  function setSeparadorRegistros($parSeparadorRegistros)
   {
    if ($parSeparadorRegistros <> "")
      $this->separadorRegistros = $parSeparadorRegistros;
   }

  //-------------------------------------------------
  function getSeparadorRegistros()
   {
    return $this->separadorRegistros;
   }

  //-------------------------------------------------
  function hacerPeticion($parPeticion_sql = "")
   {
    global $c_database;
    global $link;
    if($parPeticion_sql=="")
      $qrystr = $this->peticion_sql;
    else
      $qrystr = $parPeticion_sql ;
    //echo "qrystr: $qrystr";
    $this->respuestaMySql = mysql_db_query($c_database,$qrystr ,$link);
   }

  //-------------------------------------------------
  function escribirTextoPlano()
   {
    $filas = 0;
    $campos = mysql_num_fields($this->respuestaMySql);
    while($row = mysql_fetch_row($this->respuestaMySql))
     {
      if($filas > 0)
        echo $this->getSeparadorRegistros();
      for($i=0;$i < $campos;$i++)
       {
        if($i > 0)
          echo $this->getSeparadorCampos();
          
        echo $this->normalizarCampo($row[$i]);
       }
      $filas++;
     }
   }

  //-------------------------------------------------
  function normalizarCampo($campo)
   {
    $campo = str_replace($this->getSeparadorCampos(),"",$campo);
    $campo = str_replace($this->getSeparadorRegistros(),"",$campo);
    return $campo;
   }

 }

//########################## FIN CLASE ######################################


//########################## EJEMPLO DE USO #################################
//Viene como parámetro por GET o POST la variable consulta.
/*include("conexion.php");
$link = mysql_connect($c_conexion,$c_usuario,$c_password);

$pru = new Respondedor("$consulta");
$pru->hacerPeticion();
$pru->escribirTextoPlano();*/

?>
